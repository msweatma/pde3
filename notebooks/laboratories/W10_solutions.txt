###W10_ex1_start_md
**Solution:**

We us the initial and boundary conditions to find the form of the solution from the four basic solution types 
\begin{align*}
u_1(x,t) &= \cos\lambda ct\sin\lambda x \\
u_2(x,t) &= \cos\lambda ct\cos\lambda x \\
u_3(x,t) &= \sin\lambda ct\sin\lambda x \\
u_4(x,t) &= \sin\lambda ct\cos\lambda x
\end{align*}
For $u(0,t)=0\ \forall t>0$ we require $\sin\lambda x$ in the solution; this removes the two basic solutions $u_2$ and $u_4$. 

Similarly, to fulfil $\frac{\partial}{\partial t}u(x,0)=0,\ 0\le x\le \pi,$ we require that the derivative with respect to $t$ contains $\sin\lambda ct$. Thus removing $u_3$ from the list of possible solutions. 

Therefore the solutions needs to have the form $u_1$  
$$
u_1(x,t) = \cos\lambda ct\sin\lambda x
$$

The boundary condition (b) gives a condition for the allowed values of $\lambda$
\begin{align*}
0 &= \cos\lambda ct\sin\lambda\pi, \quad \forall t \\
\implies 0 &= \sin\lambda\pi \\
\implies \lambda &= n\text{ where }n\text{ is any integer.}
\end{align*}

To match the initial condition (c) we form the superposition of these solutions 
$$
u(x,t) = \sum_{n=1}^\infty b_n\cos nct \sin nx
$$
We can do this because the wave equation is linear. We require that
$$
u(x,0) = \sum_{n=1}^\infty b_n\sin nx = x^2,\ 0\le x\le \pi
$$

To determine the $b_n$ we find the Fourier sine series expansion over the finite interval $0\le x\le\pi$ by using integration by parts twice 
\begin{align*}
b_n &= \frac2\pi \int_0^{\pi}x^2\sin n x\text{ d}x \\
&= \frac2\pi \left(\left[-\frac{x^2}{n}\cos nx\right]_0^\pi + \int_0^\pi \frac{2x}{n}\cos nx \text{ d}x \right) \\
&= \frac2\pi \left(\left[-\frac{x^2}{n}\cos nx\right]_0^\pi + \left[\frac{2x}{n^2} \sin nx\right]_0^\pi -  \int_0^\pi \frac{2}{n^2}\sin nx \text{ d}x \right) \\
&= \frac2\pi \left(\left[-\frac{x^2}{n}\cos nx\right]_0^\pi + \left[\frac{2x}{n^2} \sin nx\right]_0^\pi + \left[\frac{2}{n^3}\cos nx\right]_0^\pi\right) \\
&= \frac2\pi \left( \frac{-\pi^2}{n}(-1)^n + 0 + \frac{2}{n^3}\left((-1)^n-1\right)\right) \\
&= \frac{2\pi}{n}(-1)^{n+1} + \frac{4}{\pi n^3}\left((-1)^n-1\right)
\end{align*}
So the solution is 
$$
u(x,t) = \sum_{n=1}^\infty \left(\frac{2\pi}{n}(-1)^{n+1} + \frac{4}{\pi n^3}\left((-1)^n-1\right)\right) \cos nct \sin nx
$$
for $0\le x\le \pi$ and $t>0$.
###W10_ex1_end

###W10_ex2_start_md
**Solution:**

If $u(x,t)$ is the transverse displacement of the string at any point $x$ at any time $t$, then the boundary value problem is 
$$
\pdderiv{u}{t}=c^2\pdderiv{u}{x}
$$
with the initial and boundary conditions
\begin{align*}
u(x,0) &= 0, &\forall x, \qquad & (a) \\
\frac{\partial}{\partial t}u(x,0) &= 0, & \forall x \qquad  & (b) \\
u(0,t) &= A_0 \sin(\omega t), & t>0, \qquad & (c) \\
|u(x,t)| &< M, &\exists M>0, \forall x, \forall t. \qquad & (d)
\end{align*}
The initial conditions (a) and (b) ensure that the string is initially at rest. The boundary condition (c) describes the periodic displacement at $x=0$. We add the boundary condition (d) to make sure that the displacement is bounded. While this boundary condition isn't explicitly given, it makes sense physically.

Taking the Laplace transform with respect to $t$ of the differential equation and the boundary conditions gives
\begin{align*}
s^2 U(x,s) - s u(x,0) - u'(x,0) &= c^2 \pdderiv{U(x,s)}{x} & (e) \\
U(0,s) &= \frac{A_0\omega}{s^2+\omega^2} & (f) \\
U(x,s) &< K, \quad \exists K, \forall x, \forall s. & (g)
\end{align*}
where the last condition shows that the Laplace transform of the displacement is also bounded.

Inserting the initial conditions (a) and (b) we get an ordinary differential equation for $U(x,s)$
$$
\pdderiv{U}{x} - \frac{s^2}{c^2}U = 0
$$
The general solution of this differential equation is
$$
U(x,s) = a_1 e^{sx/c} + a_2 e^{-sx/c}
$$
This can be found from the characteristic polynomial and the ODE solution methods.

From condition (g) on the boundedness of the Laplace transform we can infer that $a_1=0$. This is the case because the exponential term $e^{sx/c}$ is unbounded. 

The coefficient $a_2$ can be found by using the condition (f) at $x=0$
$$
U(0,s) = a_2 e^{0} \overset{!}{=} \frac{A_0\omega}{s^2+\omega^2} 
$$
and thus
$$
a_2 = \frac{A_0\omega}{s^2+\omega^2} 
$$
and the solution in the Laplace domain is 
$$
U(x,s) = \frac{A_0\omega}{s^2+\omega^2} e^{-sx/c}
$$
We can invert this by using the time shift property and the Laplace transform of the sine to get
$$
u(x,t) = A_0 \sin\left[\omega (t-x/c)\right] H(t-x/c)
$$
for all $x$ and for $t>0$.

Physically, this means that a point $x$ of the string stays at rest until the time $t=x/c$. Thereafter it undergoes motion identical with that of the end $x=0$ but lags behind it in time by the amount $x/c$. The constant $c$ is the speed with which the wave travels.
###W10_ex2_end
