\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Elliptic PDE Part 2: Separated solution}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Find the three basic solutions for the method of separation of variabes for the Laplace equation
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Derive the Laplace equation
\item Show that a given equation is a solution to the Laplace equation
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Recap}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace Equation}
\begin{block}{The Laplace equation is given by}
\[
\grad^2u = \pdderiv{u}{x_1} + \pdderiv{u}{x_2} + \cdots + \pdderiv{u}{x_n} = 0
\]
\end{block}
\begin{itemize}
\item It is the most fundamental \textcolor{blue}{elliptic} PDE which models the steady state of a diffusion like process
\item Used to describe the pressure in an incompressible flow, steady state diffusion and heat conduction, stress and strain, hydrostatics, electrostatics, ground water flow, etc.
\item Unlike the other PDEs the solution only involves spatial derivatives -- \textcolor{red}{there is no time dependency}
\item It is easy to find \underline{\textbf{a}} solution -- finding \underline{\textbf{the}}  solution matching the boundary conditions is the hard part
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Separated solutions}
% -------------------------------------------------------- %
\subsection{Idea}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Separation of Variables}
\begin{itemize}
\item Remember the following solution of the Laplace equation from the example video
\[u(x,y) = e^{-y} \cos(x)
\]
\item The two independent variables $x$ and $y$ are separated into individual parts
\item Can we generalise this to find solutions of the Laplace equation of the form
\[u(x,y)=X(x)Y(y)\]
where $X(x)$ and $Y(y)$ are single functions of single variables?
\begin{itemize}
\item We can sometimes find $X$ and $Y$ as solutions of \textcolor{blue}{ordinary differential equations}
\item These are often much easier to solve than PDEs and it \textcolor{red}{may} be possible to build up full solutions of the PDE in terms of $X$ and $Y$
\item This can be applied to many types of PDE
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Application to Laplace equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Separated solutions}
\begin{itemize}
\item We seek solutions to the 2D Laplace equation of the form
\[u(x,y) =X(x)Y(y)\]
\item Substituting this into the Laplace equation and applying the product rule gives
\[Y\frac{\text{d}^2X}{\text{d}x^2}+X\frac{\text{d}^2Y}{\text{d}y^2}=0\]
\item Or by rearranging we get
\[
\frac1X\frac{\text{d}^2X}{\text{d}x^2}=-\frac1Y\frac{\text{d}^2Y}{\text{d}y^2}=\lambda
\]
\item Here $\lambda$ is independent of both $x$ and $y$
\item We will later pick a $\lambda$ so that the boundary conditions are fulfilled
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Two simple harmonic ODEs}
\begin{itemize}
\item Since the two equations are independent, we can split them into two ODEs of the simple harmonic type
\vskip-0.6cm
\begin{align*}
\frac{\text{d}^2X}{\text{d}x^2} &= \lambda X \\
\frac{\text{d}^2Y}{\text{d}y^2} &= -\lambda Y
\end{align*}
\item The type of the solution depends on the sign of $\lambda$ for which there are three possibilities
\item Let us first look at the solutions for $X$:
\begin{align*}
X(x) =& A \cos(\mu x) + B \sin(\mu x) & \lambda=-\mu^2<0,\\
X(x) =& A e^{\mu x} + B e^{-\mu x} & \lambda=\mu^2>0,\\
X(x) =& (Ax+B) & \lambda=0
\end{align*}
\item The order of the first and second solution is swapped for $Y$ due to the minus sign
\end{itemize}
\vskip1.1cm
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Three basic solutions}
\begin{itemize}
\item By combining the solutions for $X$ and $Y$ we get
\begin{align*}
u(x,y) =&(A\sin \mu x+B\cos \mu x)\left(Ce^{\mu y}+De^{-\mu y}\right)&\lambda=-\mu^2<0,\\
 =&(A\sin \mu x+B\cos \mu x)\left(\tilde{C}\cosh \mu y+\tilde{D}\sinh \mu y\right) & \\
u(x,y) =&\left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y)&\lambda=\mu^2>0,\\
 =&\left(\tilde{A}\cosh \mu x+\tilde{B}\sinh \mu x\right)(C\sin \mu y+ D\cos \mu y) & \\
u(x,y) =&(Ax+B)(Cy+D)&\lambda=0
\end{align*}
where $A,\, B,\, C,$ and $D$ are arbitrary constants
\item We will show in an example video how we can use these basic solutions to find the solution to the Laplace equation and its boundary conditions
\end{itemize}
\vskip2cm
\end{frame}

%-------------------------------------------------------%
\section{Summary}
% -------------------------------------------------------- %
\subsection{Separated solutions recipe}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Recipe for the method of separation of variables}
Consider the three possible solutions 
\begin{align*}
u_1(x,y) =& (A\sin \mu x+B\cos \mu x)\left(Ce^{\mu y}+De^{-\mu y}\right)&\lambda=-\mu^2<0, \\
 =& (A\sin \mu x+B\cos \mu x)\left(\tilde{C}\cosh \mu y+\tilde{D}\sinh \mu y\right) & \\
u_2(x,y) =& \left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y)&\lambda=\mu^2>0,\\
 =& \left(\tilde{A}\cosh \mu x+\tilde{B}\sinh \mu x\right)(C\sin \mu y+ D\cos \mu y) & \\
u_3(x,y) =& (Ax+B)(Cy+D)&\lambda=0
\end{align*}
and go through the boundary conditions to
\smallskip
\begin{enumerate}
\item Eliminate basic solutions
\item Specify the permissible values of $\mu$
\item Superpose solutions to fit the initial condition
\end{enumerate}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Boundary conditions for elliptic PDEs}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}