\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Hyperbolic PDE -- Part 7: Laplace transform method}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Use the Laplace transform method to find the solution of the wave equation
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Use the method of separation of variables to solve wave equation problems
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Recap}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Recipe for separated solutions}
\begin{itemize}
\item We developed the separated solution method based on the fundamental solution for the vibrating string
\[
  u(x,t) = u_0 \cos\frac{\pi c t}{L} \sin\frac{\pi x}{L}
\]
\item Consider the four basic solutions 
\begin{align*}
u_1(x,t) &= \cos\lambda ct\sin\lambda x \\
u_2(x,t) &= \cos\lambda ct\cos\lambda x \\
u_3(x,t) &= \sin\lambda ct\sin\lambda x \\
u_4(x,t) &= \sin\lambda ct\cos\lambda x 
\end{align*}
\item Go through the initial and boundary conditions to
\begin{enumerate}
\item Eliminate basic solutions
\item Specify the permissible values of $\lambda$
\item Superpose solutions to fit the initial condition
\end{enumerate}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Laplace transform method}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace transform method}
\begin{itemize}
\item For linear problems with $t=0\rightarrow\infty$, for example the wave equation, Laplace transforms provide a formal method of solution
\item Take the Laplace transform with respect to $t$ to reduce the PDE to an ODE in $x$
\item Following the procedure for solving ODEs we need the Laplace transforms of
\[
\pderiv{u}{x},\,\pderiv{u}{t},\,\pdderiv{u}{x},\text{ and }\pdderiv{u}{t}
\]
for the function $u(x,t),\ t\ge0$
\item The main difficulty is the final inversion
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Transform the derivatives}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Partial derivative transforms}
\begin{itemize}
\item Consider the function
\[
u(x,t),\,t\ge0
\]
\item The Laplace transforms of the partial derivatives are given by
\begin{align*}
\mathcal{L}\left\{\pderiv{u}{x}\right\} &= \int_0^\infty e^{-st}\pderiv{u}{x}\text{ d}t =\frac{\text{d}}{\text{d}x}\int_0^\infty e^{-st}u(x,t)\text{ d}t = \frac{\text{d}}{\text{d}x}U(x,s)\\
\mathcal{L}\left\{\pdderiv{u}{x}\right\} &= \frac{\text{d}^2U(x,s)}{\text{d}x^2} \\
\mathcal{L}\left\{\pderiv{u}{t}\right\} &= sU(x,s)-u(x,0)\\
\mathcal{L}\left\{\pdderiv{u}{t}\right\} &= s^2U(x,s)-su(x,0)-u_t(x,0)
\end{align*}
\item Here, $U(x,s)$ is the Laplace transform of $u(x,t)$ with respect to $t$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Wave equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace transform of the wave equation}
\begin{itemize}
\item Consider the 1D linear wave equation
\[
c^2\pdderiv{u(x,t)}{x} = \pdderiv{u(x,t)}{t}
\]
\item Subject to the initial conditions
\[
u(x,0) = f(x)\text{ and } \pderiv{u(x,0)}{t} = g(x)
\]
\item Apply the Laplace transform with respect to $t$ to both sides to get
\[
c^2\frac{\text{d}^2U(x,s)}{\text{d}x^2} = s^2U(x,s)-g(x)-sf(x)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Find the Laplace domain ordinary differential equation}
\begin{itemize}
\item We can rearrange the transformed equation to standard form to get
\[\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s^2}{c^2}U(x,s) = \frac{-1}{c^2} g(x) - \frac{s}{c^2} f(x)
\]
\item The characteristic polynomial of the ODE has the zeros $\lambda_{1,2}=\pm\frac{s}{c}$
\item Thus, the complementary function is
\[
U_c(x,s)=A(s) e^{\frac{sx}{c}} + B(s) e^{\frac{-sx}{c}}
\]
where $A(s)$ and $B(s)$ are arbitrary functions of $s$
\item The inhomogeneity can be handled with a particular integral
\item The parameters $A(s)$ and $B(s)$ can be found from the boundary conditions on $x$
\item The combined solution can be inverted to find $u(x,t)$
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Hyperbolic-AP7: Find Laplace domain ODE}
\vskip-0.cm
\begin{block}{Apply the Laplace transform  with respect to time to the 1D wave equation for a semi-infinite string, $0\le x<\infty$, with the following initial and boundary conditions}
\vskip-0.6cm
{\small
\begin{align}
u(x,0) &= 0 &\forall x\ge0 \label{eq:wave_916_IC1} \\
u'(x,0) = \pderiv{u(x,0)}{t} &= xe^{-\frac{x}{a}} & \forall x\ge0 \label{eq:wave_916_IC2}\\
u(0,t) &= 0 & \forall t\ge0 \label{eq:wave_916_BC1} \\
\lim_{x\rightarrow\infty}u(x,t) &= 0 & \forall t\ge0 \label{eq:wave_916_BC2}
\end{align}}
\vskip-0.5cm
\begin{itemize}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.2cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item \(\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s}{c^2}U(x,s) = \frac{-1}{c^2}xe^{-\frac{x}{a}}\)
\item \(\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s^2}{c^2}U(x,s) = \frac{-1}{c^2}xe^{-\frac{x}{a}}\)
\item \(\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s^2}{c^2}U(x,s) = \frac{-s}{c^2}xe^{-\frac{x}{a}}\)
\item \(\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s}{c^2}U(x,s) = \frac{-s}{c^2}xe^{-\frac{x}{a}}\)
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\begin{itemize}
\item Taking the Laplace transform with respect to time and substituting the initial conditions~\ref{eq:wave_916_IC1} and \ref{eq:wave_916_IC2} we get
{\small
\[
c^2\frac{\text{d}^2U(x,s)}{\text{d}x^2} = s^2U(x,s) - su(x,0) - u'(x,0) =s^2U(x,s)-xe^{-\frac{x}{a}}
\]}
\item Rearrange to standard form: \(\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s^2}{c^2}U(x,s) = \frac{-1}{c^2}xe^{-\frac{x}{a}}\)
\end{itemize}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
%-------------------------------------------------------%
\subsection{Laplace transform method}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Laplace transform method}
\begin{itemize}
\item The Laplace transform generates the following ODE from the 1D wave equation
\[\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s^2}{c^2}U(x,s) = \frac{-1}{c^2} u'(x,0) - \frac{s}{c^2} u(x,0)
\]
\item The Laplace domain solution is given from the complementary solution plus the particular integral
\[ U(x,s) = U_c(x,s) + U_p(x,s) = A(s) e^{\frac{sx}{c}} + B(s) e^{\frac{-sx}{c}} + U_p(x,s)
\]
\item The parameters $A(s)$ and $B(s)$ can be found from the boundary conditions
\item The resulting Laplace domain solution needs to be inverted back into the time domain
\end{itemize}
\vskip0.1cm
\begin{block}{Comments}
\begin{itemize}
\item The inverse transforms, if it exist, can often be very complicated so the use of a computer algebra system is recommended
\item Extension to other coordinate systems and other PDEs is possible
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Boundary conditions}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}