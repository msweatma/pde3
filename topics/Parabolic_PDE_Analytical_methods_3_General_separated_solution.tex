\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Parabolic PDE Part 3: General separated solution}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Derive the separated solution for the heat conduction or diffusion equation
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Nondimensionalise the heat conduction equation
\item Describe the heat conduction equation in terms of mass diffusion 
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Recap}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Nondimensionalisation and parameter values}
\begin{itemize}
\item Values of the thermal conductivity range between
\begin{itemize}
\item Vacuum insulation panels $k\approx 0.007$ in W$\,$m$^{-1}\,$K$^{-1}$
\item Silver $k\approx 429\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\item The nondimensionalisation of the heat conduction equation can remove the diffusivity parameter
\item The same partial differential equation can also describe other diffusion processes
\end{itemize}
\centerline{\includegraphics[width=0.75\columnwidth]{Blausen_0315_Diffusion.png}}
\end{frame}

% -------------------------------------------------------- %
\section{Parabolic PDE}
% -------------------------------------------------------- %
\subsection{General solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{General solution}
\centerline{\includegraphics[width=0.45\columnwidth]{AMEM_Example_9_20.png}}
\begin{itemize}
\item If there are no time varying inputs, i.e. boundary conditions or source terms, the solution will approach a steady state
\item In this case, we can write the general solution as
\vskip-0.3cm
\[
u = U + v
\]
\vskip-0.3cm
\begin{itemize}
\item $U(x)$ is the steady state solution
\item $v(x,t)$ is the transient (time-varying) part that satisfies both the PDE and the boundary conditions on $u-U$
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Transient solution}
\begin{itemize}
\item Because the solution approaches the steady state at $t\to\infty$
\vskip-0.3cm
\[
\lim_{t\rightarrow\infty} u = U
\]
it follows that the transient goes to zero
\vskip-0.3cm
\[
\lim_{t\rightarrow\infty}v=0
\]
\item The boundary and initial conditions of the transient problem are given by
\begin{align*}
v(x_0, t) &= u(x_0, t) - U(x_0) &\forall t>0 \\
\left.\pderiv{v}{x}\right|_{x=x_0} &= \left.\pderiv{u}{x}\right|_{x=x_0} - \left.\pderiv{U}{x}\right|_{x=x_0} &\forall t>0 \\
v(x, t=0) &= u(x,0) - U(x) & x_{min}\le x \le x_{max}
\end{align*}
\vskip-0.8cm
\item For example, for constant Dirichlet boundary conditions we always get
\[
v(x_0, t) = 0
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Separated solutions}
% -------------------------------------------------------- %
\subsection{Idea}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Separation of variables}
\begin{itemize}
\item Solve the PDE by seeking solutions of the form
\[
v(x,t) = r(t)w(x)
\]
\item Here $r(t)$ and $w(x)$ are single functions of single variables
\item We substitute this into the heat equation
\[
\frac{1}{\kappa}\pderiv{v}{t} = \pdderiv{v}{x}
\]
\item This give us two separated ordinary differential equations
gives
\[
\frac{1}{\kappa} \frac{dr(t)}{dt} w(x) = r(t) \frac{\text{d}^2w}{\text{d}x^2}
\]
or
\[
\frac{1}{\kappa}\frac1r\frac{\text{d}r}{\text{d}t} = \frac1w\frac{\text{d}^2w}{\text{d}x^2} = \mu
\]
\item Here $\mu$ is independent of both $x$ and $t$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Derivation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Two simple ODEs}
\begin{itemize}
\item Since the two equations are independent, we can split them into two ODEs 
\[
\frac{\text{d}r}{\text{d}t} = \mu\kappa r \quad\text{ and }\quad\frac{\text{d}^2w}{\text{d}x^2} = \mu w
\]
\item The type of the solution depends on the sign of $\mu$ which gives three possibilities
\item Let us first look at the solutions for $w(x)$:
\begin{align*}
w(x) =& A \cos(\lambda x) + B \sin(\lambda x) & \mu = -\lambda^2 < 0,\\
w(x) =& A e^{\lambda x} + B e^{-\lambda x} & \mu = \lambda^2>0,\\
w(x) =& (Ax+B) & \mu=0
\end{align*}
\item For $r(t)$ we get the solution
\[
r(t) = C e^{\mu\kappa t}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Combined solution}
\begin{itemize}
\item We get the combined solution
\[
v(x, t) = r(t) w(x) = C e^{\mu\kappa t} w(x)
\]
\item This fits with the physics which suggests a solution which decays in time if the exponent is negative
\item To achieve this we need $\mu\kappa<0$ from which it follows that $\mu < 0$
\item This is the case because 
\[
\kappa = k/c\rho > 0
\]
\item Thus, we get the following trial solution for the transient
\[
v(x,t) = e^{-\lambda^2\kappa t} \left(A\sin\lambda x + B\cos\lambda x\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{General form}
% -------------------------------------------------------- %
\begin{frame}[label=general_form]
\frametitle{General form of the separated solution}
\begin{itemize}
\item Setting $\alpha = \lambda^2\kappa$ we get
\[
v(x,t) = e^{-\alpha t} \left(A\sin\lambda x+B\cos\lambda x\right)
\]
\item This is a solution of the heat conduction equation for all values of $\lambda$, $A$ and $B$
\item As previously we can build up a solution to satisfy all the boundary conditions by superimposing the solution
\[
v(x, t) =\sum_{n=1}^\infty e^{-\alpha_n t} \left(A_n\sin\lambda_n x+B_n\cos\lambda_n x\right)
\]
\item See example videos for Examples 9.20 and 9.21 from the textbook
\end{itemize}
%\hyperlink{example_920}{\beamergotobutton{Example 9.20}} \\
%\hyperlink{example_921}{\beamergotobutton{Example 9.21}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Comments}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Concluding remarks}
\begin{itemize}
\item Separated solutions can provide a useful approach to solving the one dimensional heat conduction or diffusion equation.
\item As before finding separated solutions requires \textcolor{red}{judicious use of known solutions} and \textcolor{blue}{careful fitting of the boundary conditions}. 
\item The starting point must always be to find the \textcolor{red}{steady state solution} and to subtract this from the dependent variable.
\item Many of the Fourier series obtained can be slow to converge and a large number of terms may need to be evaluated.
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
%-------------------------------------------------------%
\subsection{General solution}
%-------------------------------------------------------%
\begin{frame}
\frametitle{General solution of the heat conduction or diffusion equation}
\begin{itemize}
\item For constant boundary conditions and source terms the solution to the heat conduction or diffusion equation is given by
\vskip-0.2cm
\[
u = U + v
\]
\vskip-0.1cm
\begin{itemize}
\item $U(x)$ is the steady state solution
\item $v(x,t)$ is the transient (time-varying) part that satisfies both the PDE and the boundary conditions on $u-U$
\end{itemize}
\item Solution procedure for the heat conduction or diffusion equation
\begin{enumerate}
\item Find the steady state solution $U(x)$
\item Calculate the boundary and initial conditions for $v(x,t) = u(x,t) - U(x)$
\item Find the parameters $\alpha_n$, $A_n$ and $B_n$ of the superposed trial solution
\[
v(x, t) =\sum_{n=1}^\infty e^{-\alpha_n t} \left(A_n\sin\lambda_n x+B_n\cos\lambda_n x\right)
\]
\item Combine the steady state and transient solution to the general solution
\end{enumerate}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Initial and boundary conditions}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}