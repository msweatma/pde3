\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Elliptic PDE Part 4: Nondimensionalisation}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Nondimensionalise the Laplace equation
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Differentiate the different boundary conditions and explain their physical meaning
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Recap}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Well-posed elliptic PDE}
\begin{itemize}
\item Boundary conditions are given on all sides of the domain
\vskip0.3cm
\centerline{\includegraphics[width=0.5\columnwidth]{LaplaceBC.pdf}}
\item The boundary conditions are either
\begin{itemize}
\item \textcolor{blue}{Dirichlet} conditions -- the edges of the sheet are held at a specified temperature
\item \textcolor{blue}{Neumann} conditions -- the heat flux at the edges is given 
\end{itemize}
\end{itemize}
\end{frame}

% ----------------------------------------------------- %
\section{Nondimensionalisation}
%-------------------------------------------------------%
\subsection{Motivation}
% ----------------------------------------------------- %
\begin{frame}{Do the equations make sense?}
Consider the last example in terms of heat conduction
\[
\nabla^2 u = 0
\]
where the variable $u$ is a temperature, i.e. $u=T$
\medskip
\begin{itemize}
\item There are no parameters
\item The solution depends only on the boundary conditions
\item Are the given boundary conditions reasonable for heat conduction?
\begin{align*}
T(x,0)&=0&0\le x\le 2 \\
T(x,1)&=0&0\le x\le 2 \\
T(0,y)&=0&0\le y \le 1\\
T(2,y)&=a\sin 2\pi y&0\le y \le 1
\end{align*}
\vskip-0.2cm
\pause\item A temperature of $0$ on the boundary is very low
\item Maybe the equation is given in nondimensional form!
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Procedure}
% ----------------------------------------------------- %
\begin{frame}{Procedure}
\begin{itemize}
\item Identify a characteristic value for each dependent and independent variable
\item Replace each dependent and independent variable with a variable scaled with the characteristic value
\item Rewrite equations in terms of the nondimensional variables
\end{itemize}
\vskip-0.1cm
\pause
\begin{block}{Example}
\begin{itemize}
\item In the dimensional form the temperature at the bottom, top and left boundary can be used as the characteristic value: call it $T_{ref}$
\item Set $\tilde{T}  = T T_{ref} + T_{ref}$ which is equivalent to
\[
T = \frac{\tilde{T} - T_{ref}}{T_{ref}}
\]
\item $T$ varies around $0$ 
\item $\tilde{T}$ varies around $T_{ref}$
\item Substitute this into the Laplace equation
\end{itemize}
\end{block}
\end{frame}

% ----------------------------------------------------- %
\begin{frame}{Nondimensionalisation continued}
Consider the derivatives $\pderiv{T}{x}$ and $\pdderiv{T}{x}$
\begin{itemize}
\item Set $T=\frac{\tilde{T} - T_{ref}}{T_{ref}}$ with $T_{ref}=const$
\item Then we get
\vskip-0.6cm
\begin{align*}
\pderiv{T}{x} &= \frac1{T_{ref}}\pderiv{\tilde{T}}{x} \\
\pdderiv{T}{x} &= \frac1{T_{ref}}\pdderiv{\tilde{T}}{x}
\end{align*}
\item Substitute into the Laplace equation
\begin{align*}
&\nabla^2 T = \pdderiv{T}{x} + \pdderiv{T}{y} = \frac1{T_{ref}} \pdderiv{\tilde{T}}{x} + \frac1{T_{ref}} \pdderiv{\tilde{T}}{y} = 0 \\
\Rightarrow & \pdderiv{\tilde{T}}{x} + \pdderiv{\tilde{T}}{y} = 0 
\end{align*}
\end{itemize}
\end{frame}

% ----------------------------------------------------- %
\begin{frame}{Nondimensionalisation continued}
\begin{itemize}
\item The boundary conditions in the dimensional form are
\begin{align*}
\tilde{T}(x,0) &= T_{ref} & 0\le x\le 2 \\
\tilde{T}(x,1) &= T_{ref} & 0\le x\le 2 \\
\tilde{T}(0,y) &= T_{ref} & 0\le y \le 1\\
\tilde{T}(2,y) &= T_{ref}a\sin 2\pi y + T_{ref} & 0\le y \le 1
\end{align*}
\end{itemize}
\smallskip
\begin{block}{Remarks}
\begin{itemize}
\item We didn't nondimensionalise the independent variables
\item Not applicable here but nondimensionalisation can reduce the number of parameters
\item This makes it easier to compare numerical experiments
\item We will come back to this later
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Elliptic-AP4: Nondimensionalisation}
\vskip-0.cm
\begin{block}{What are suitable characteristic values for the nondimensionalisation of the independent variables?}
\centering{
%\includegraphics[width=5.cm]{orthogonal_sine_cosine_3}
}
\vskip0.1cm
\begin{itemize}
\item Consider the Laplace equation for $0\le x\le 2$ and $0\le y \le 1$
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.2cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item $x_{ref} = 1$ and $y_{ref} = 1$
\item $x_{ref} = 2$ and $y_{ref} = 2$
\item $x_{ref} = 1$ and $y_{ref} = 2$
\item $x_{ref} = 2$ and $y_{ref} = 1$
\item Some other characteristic values
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\begin{block}{Solution}
\only<2>{
\begin{itemize}
\item All options are possible
\item Option 1. and 2. are the most suitable
\item Option 3. and 4. will result in a scale factor between the different terms in the Laplace equation
\end{itemize}
}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
% -------------------------------------------------------- %
\subsection{Nondimensionalisation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Nondimensionalisation procedure}
\begin{itemize}
\item Identify a characteristic value for each dependent and independent variable
\item Replace each dependent and independent variable with a variable scaled with the characteristic value
\item Rewrite equations in terms of the nondimensional variables
\end{itemize}
\begin{block}{Example}
\begin{itemize}
\item Scale temperature with the reference temperature
    \[T=\frac{\tilde{T} - T_{ref}}{T_{ref}}\]
\end{itemize}
\end{block}
\vskip-0.4cm
\begin{block}{Remarks:}
\begin{itemize}
\item Nondimensionalisation can reduce the number of parameters
\begin{itemize}
\item This makes it easier to compare numerical experiments
\end{itemize}
\item Scaling can make numerical calculations more stable
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Complex variable solution}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}