\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Hyperbolic PDE Example 2: Separation of variables example 9.14}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Example 9.14}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.14: Displacement of a string}
\textbf{Problem:} Solve the wave equation
\[
\frac1{c^2}\pdderiv{u}{t}=\pdderiv{u}{x}
\]
\begin{itemize}
\item For a string stretched between $x=0$ and $x=l$ 
\item Subject to the boundary conditions
\begin{align*}
u(0,t) &= u(l,t)=0,  &\forall t\ge0 \\
\pderiv{u(x,0)}{t} &= 0,  &0\le x\le l\\
u(x,0) &= F(x), &0\le x\le l
\end{align*}
\item Where $F(x)$ is an arbitrary function
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Separation of variables}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Recipe for separated solutions}
\begin{itemize}
\item Consider the four basic solutions 
\begin{align}
u_1(x,t) &= \cos\lambda ct\sin\lambda x \label{eq:u1}\\
u_2(x,t) &= \cos\lambda ct\cos\lambda x \label{eq:u2}\\
u_3(x,t) &= \sin\lambda ct\sin\lambda x \label{eq:u3}\\
u_4(x,t) &= \sin\lambda ct\cos\lambda x \label{eq:u4}
\end{align}
\item Go through the initial and boundary conditions to
\begin{enumerate}
\item Eliminate basic solutions
\item Specify the permissible values of $\lambda$
\item Superpose solutions to fit the initial condition
\end{enumerate}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Eliminate basic solutions}
\begin{itemize}
\item By inspection neither solutions~\ref{eq:u2} or \ref{eq:u4} will satisfy the first boundary condition, 
\[
u(0,t) = 0\quad \forall t\ge0
\] 
because $\cos(0)=1$
\item Solutions~\ref{eq:u1} and \ref{eq:u3} will satisfy the boundary condition,
\[
u(l,t) = 0\quad \forall t\ge0
\]
provided
\[
\sin \lambda l = 0 \implies\lambda l=n\pi,\ n=1,\, 2,\, 3,\, \dots 
\]
\item This means the solution takes the form of either
\begin{align*}
u_1(x,t) &= \cos\frac{nc\pi t}{l}\sin\frac{n\pi x}{l}, \quad\text{ or } \\
u_3(x,t) &= \sin\frac{nc\pi t}{l}\sin\frac{n\pi x}{l}, \quad \ n=1,\, 2,\, 3,\, \dots 
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Apply initial condition on the time derivative}
\begin{itemize}
\item Of these only
\[
u_1(x,t) = \cos\frac{nc\pi t}{l}\sin\frac{n\pi x}{l}, \quad n=1,\, 2,\, 3,\, \dots \]
will satisfy the initial condition
\[
\pderiv{u(x,0)}{t}=0
\]
\item But, this will not, in general, satisfy the initial condition
\[
u(x,0) = F(x)
\]
\item Fortunately we are dealing with the \textcolor{blue}{linear wave equation} so we can \textcolor{red}{superpose solutions}, i.e. the sum of these solutions is also a solution, to get
\[
u(x,t) = \sum_{n=1}^\infty b_n\cos\frac{nc\pi t}{l}\sin\frac{n\pi x}{l}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Case (i)}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Case (i): Problem and solution}
\begin{itemize}
\item Set the initial displacement to
\begin{align*}
u(x,0) &= F(x) = \sin\frac{\pi x}{l}+\frac14\sin\frac{3\pi x}{l}, &0\le x\le l
\end{align*}
\item The initial condition $u(x,0)=\sin\frac{\pi x}{l}+\frac14\sin\frac{3\pi x}{l}$ requires that the following holds
\[
\sum_{n=1}^\infty b_n\sin\frac{n\pi x}{l} \overset{!}{=} \sin\frac{\pi x}{l}+\frac14\sin\frac{3\pi x}{l}
\]
\item Clearly $b_1=1,\, b_2=0,\, b_3=\frac14,\, b_4=b_5=\dots=0$
\item Giving the solution
\[
u(x,t) = cos\frac{c\pi t}{l}\sin\frac{\pi x}{l}+\frac14cos\frac{3c\pi t}{l}\sin\frac{3\pi x}{l}, \quad 0\le x\le l
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution plot}
\begin{tikzpicture}[scale=0.95]
 \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black,domain=0:1, samples=101] {cos(180/3*0.0)*sin(180*x)+cos(180*0.0)*sin(3*180*x)/4};
  \addplot[blue, dotted, line width=1.5,domain=0:1, samples=101] {cos(180/3*0.4)*sin(180*x)+cos(180*0.4)*sin(3*180*x)/4};
  \addplot[cyan, dashed, line width=1.5,domain=0:1, samples=101] {cos(180/3*1.2)*sin(180*x)+cos(180*1.2)*sin(3*180*x)/4};
  \addplot[magenta, dashdotted, line width=1.5,domain=0:1, samples=101] {cos(180/3*2.0)*sin(180*x)+cos(180*2.0)*sin(3*180*x)/4};

  \legend{$t=0.0$,$t=0.4$,$t=1.2$,$t=2.0$}
 \end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\subsection{Case (ii)}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Case (ii): Same string with a different initial condition}
\begin{itemize}
\item Set the initial displacement to
\begin{align*}
u(x,0) &= F(x) = \begin{cases}
x & 0\le x \le \frac{l}{2} \\
l-x & \frac{l}{2} \le x \le l
\end{cases}
\end{align*} 
\item The superposed solution is given by
\[
u(x,t) = \sum_{n=1}^\infty b_n\cos\frac{nc\pi t}{l}\sin\frac{n\pi x}{l}
\]
\item The initial condition $u(x,0)=F(x)$ requires that the following holds
\[
\sum_{n=1}^\infty b_n\sin\frac{n\pi x}{l} \overset{!}{=}\begin{cases}
x & 0\le x \le \frac{l}{2} \\
l-x & \frac{l}{2} \le x \le l
\end{cases}
\]
\item The values of $b_n$ can be found with the methods from Fourier series 
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Calculate the Fourier coefficients}
\begin{itemize}
\item Using the Fourier series sine expansion
\begin{align*}
b_n &= \frac{2}{l}\int_0^l F(x)\sin\frac{n\pi x}{l}\text{d}x = \frac{2}{l}\int_0^\frac{l}{2}x\sin\frac{n\pi x}{l}\text{d}x+\frac{2}{l}\int_\frac{l}{2}^l (l-x)\sin\frac{n\pi x}{l}\text{d}x \\
&= \frac{4l}{\pi^2n^2}\sin \frac12n\pi, \quad n=1,2,3,\dots \\
&= \frac{4l}{\pi^2 (2k-1)^2} (-1)^{k+1}, \quad k=1,2,3,\dots
\end{align*}
\item Where we have replaced $n=2k-1$ to cover only the odd terms
\item The complete solution is therefore
\[
u(x,t) = \frac{4l}{\pi^2}\sum_{k=1}^\infty\frac{(-1)^{k+1}}{(2k-1)^2}\cos\left(\frac{(2k-1)c\pi t}{l}\right)\sin\left(\frac{(2k-1)\pi x}{l}\right)
\]
for $0\le x\le l$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution plot for different times}
\begin{itemize}
\item Six terms of the series solution for six different times
\end{itemize}
\vskip0.2cm
\begin{tikzpicture}[scale=0.95]
 \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black] coordinates {(0,0) (0.5,0.5) (1,0)};
%
  \addplot[blue, dotted, line width=1.5,domain=0:1, samples=101] {4/(pi*pi)*(cos(180/3*0.2)*sin(180*x)
  -cos(180*0.2)*sin(3*180*x)/9+cos(5*180/3*0.2)*sin(5*180*x)/25-cos(7*180/3*0.2)*sin(7*180*x)/49)
  +cos(9*180/3*0.2)*sin(9*180*x)/81-cos(11*180/3*0.2)*sin(11*180*x)/121};
%
  \addplot[cyan, dashed, line width=1.5,domain=0:1, samples=101] {4/(pi*pi)*(cos(180/3*0.4)*sin(180*x)
  -cos(180*0.4)*sin(3*180*x)/9+cos(5*180/3*0.4)*sin(5*180*x)/25-cos(7*180/3*0.4)*sin(7*180*x)/49)
  +cos(9*180/3*0.4)*sin(9*180*x)/81-cos(11*180/3*0.4)*sin(11*180*x)/121};
%
  \addplot[magenta, dashdotted, line width=1.5,domain=0:1, samples=101] {4/(pi*pi)*(cos(180/3*0.6)*sin(180*x)
  -cos(180*0.6)*sin(3*180*x)/9+cos(5*180/3*0.6)*sin(5*180*x)/25-cos(7*180/3*0.6)*sin(7*180*x)/49)
  +cos(9*180/3*0.6)*sin(9*180*x)/81-cos(11*180/3*0.6)*sin(11*180*x)/121};
%
 \addplot[red, loosely dotted, line width=1.5,domain=0:1, samples=101] {4/(pi*pi)*(cos(180/3*0.8)*sin(180*x)
  -cos(180*0.8)*sin(3*180*x)/9+cos(5*180/3*0.8)*sin(5*180*x)/25-cos(7*180/3*0.8)*sin(7*180*x)/49)
  +cos(9*180/3*0.8)*sin(9*180*x)/81-cos(11*180/3*0.8)*sin(11*180*x)/121};
%
 \addplot[green, densely dotted, line width=1.5,domain=0:1, samples=101] {4/(pi*pi)*(cos(180/3*1.0)*sin(180*x)
  -cos(180*1.0)*sin(3*180*x)/9+cos(5*180/3*1.0)*sin(5*180*x)/25-cos(7*180/3*1.0)*sin(7*180*x)/49)
  +cos(9*180/3*1.0)*sin(9*180*x)/81-cos(11*180/3*1.0)*sin(11*180*x)/121};

  \legend{$t=0.0$,$t=0.2$,$t=0.4$,$t=0.6$,$t=0.8$,$t=1.0$}
 \end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Convergence at $t=0$}
\begin{tikzpicture}[scale=0.95]
 \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black] coordinates {(0.3,0.3) (0.5,0.5) (0.7,0.3)};
%=0.3:0:7
  \addplot[blue, dotted, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)};
%
  \addplot[cyan, dashed, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9};
%
  \addplot[magenta, dashdotted, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9+cos(5*180/3*0.0)*sin(5*180*x)/25};
%
 \addplot[red, loosely dotted, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9+cos(5*180/3*0.0)*sin(5*180*x)/25-cos(7*180/3*0.0)*sin(7*180*x)/49)};
%
 \addplot[green, densely dotted, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9+cos(5*180/3*0.0)*sin(5*180*x)/25-cos(7*180/3*0.0)*sin(7*180*x)/49)
  +cos(9*180/3*0.0)*sin(9*180*x)/81};
%
\addplot[brown, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9+cos(5*180/3*0.0)*sin(5*180*x)/25-cos(7*180/3*0.0)*sin(7*180*x)/49)
  +cos(9*180/3*0.0)*sin(9*180*x)/81-cos(11*180/3*0.0)*sin(11*180*x)/121};

  \legend{Exact, 1 term, 2 terms, 3 terms, 4 terms, 5 terms, 6 terms}
 \end{axis}
\end{tikzpicture}
\begin{itemize}
\item The series solution at $t=0$ for different numbers of terms in the series solution
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Thank you}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}
