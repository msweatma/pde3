\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Hyperbolic PDE -- Part 6: Separation of variables}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Use the method of separation of variables to solve wave equation problems
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Characterise a general second-order PDE as either elliptic, parabolic or hyperbolic
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Recap}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Formal Classification of second order PDEs}
\begin{itemize}
\item Given the general second-order PDE
\[
Au_{xx}+2Bu_{xy}+Cu_{yy}+Du_x+Eu_y+F = 0
\]
where $A,\, B,\, C,\ldots$ are constants
\item We can make the change of variable to use characteristics $r=ax+y$ and $s=x+by$ to obtain
\[
(A+2bB+b^2C)\left[u_{ss}+\frac{AC-B^2}{(A+Bb)^2}u_{rr}\right]+\ldots = 0
\]
\item The behaviour of the PDE depends on the sign of $AC-B^2$
\begin{itemize}
\item If $AC-B^2<0$ the PDE is \textcolor{red}{hyperbolic} $\to$ real and distinct characteristics
\item If $AC-B^2=0$ the PDE is \textcolor{red}{parabolic} $\to$ real and simultaneous characteristics
\item If $AC-B^2>0$ the PDE is \textcolor{red}{elliptic} $\to$ complex conjugate characteristics
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Separated Solutions}
% -------------------------------------------------------- %
\subsection{Motivation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Vibrating string}
\includegraphics[width=\columnwidth]{VibratingString03-400x220}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Remember the fundamental mode of a vibrating string}
\[
u(x,t) = u_0 \cos\frac{\pi c t}{L} \sin\frac{\pi x}{L}
\]

\begin{tikzpicture}[scale=0.93]
 \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black,domain=0:1, samples=101] {sin(x*180)};
  \addplot[blue, dashed, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(45)};
  \addplot[cyan, loosely dotted, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(90)};
  \addplot[green, dashdotted, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(135)};
  \addplot[red, dotted, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(180)};
  \legend{$t=0.00$s,$t=0.25$s,$t=0.50$s,$t=0.75$s,$t=1.00$s}
 \end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\subsection{Procedure}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Separation of Variables}
\begin{block}{Solve the PDE by seeking solutions of the form}
\[
u(x,y) = X(x)Y(y)
\]
where $X(x)$ and $Y(y)$ are single functions of single variables
\end{block}
\begin{itemize}
\item We can sometimes find $X$ and $Y$ as solutions of \textcolor{blue}{ordinary differential equations}
\item These are often much easier to solve than PDEs and it \textcolor{red}{may} be possible to build up full solutions of the PDE in terms of $X$ and $Y$
\item This can be applied to many types of PDE and has already been used for
\begin{itemize}
\item Laplace equation
\item Heat conduction/diffusion equation
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Vibrating string}
\begin{itemize}
\item We are looking for solutions of the wave equation of the form of either 
\begin{align*}
u(x,t) &= v(x)\sin c\lambda t \\
u(x,t) &= v(x)\cos c\lambda t
\end{align*}
\item Substituting into the wave equation we obtain the ODE
\[
\frac{1}{v}\frac{\text{d}^2v}{\text{d}x^2}=-\lambda^2
\]
\item This is a simple harmonic equation with solutions
\begin{align*}
v(x) &= \sin\lambda x\\
v(x) &= \cos\lambda x
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Recipe}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Recipe for separated solutions for the wave equation}
\begin{itemize}
\item We are looking for solutions of the wave equation
\[
\frac1{c^2}\pdderiv{u}{t} = \pdderiv{u}{x}
\]
\item Consider the four basic solutions 
\begin{enumerate}
\item $u_1(x,t) = \cos\lambda ct\sin\lambda x$
\item $u_2(x,t) = \cos\lambda ct\cos\lambda x$
\item $u_3(x,t) = \sin\lambda ct\sin\lambda x$
\item $u_4(x,t) = \sin\lambda ct\cos\lambda x$
\end{enumerate}
\item Go through the initial and boundary conditions to
\begin{enumerate}
\item Eliminate basic solutions
\item Specify the permissible values of $\lambda$
\item Superpose solutions to fit the initial condition
\end{enumerate}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Hyperbolic-AP6: Separated solution}
\vskip-0.cm
\begin{block}{Which are the possible solutions for the wave equation with the following initial and boundary conditions?}
\begin{align*}
u(x,0)&=x & 0\le x\le 2\pi, \\
u_t(x,0)&=0 & 0\le x\le 2\pi,\\
u(0,t)&=0&t>0,\\
u(\pi,t)&=0&t>0.
\end{align*}
\vskip-0.1cm
\begin{itemize}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.2cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item $u_1(x,t) =\cos\lambda ct\sin\lambda x$
\item $u_2(x,t) =\cos\lambda ct\cos\lambda x$
\item $u_3(x,t) =\sin\lambda ct\sin\lambda x$
\item $u_4(x,t) =\sin\lambda ct\cos\lambda x$
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\begin{enumerate}
\item[1.] $u_1(x,t) =\cos\lambda ct\sin\lambda x$
\end{enumerate}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
% -------------------------------------------------------- %
\subsection{Separated solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Recipe for separated solutions for the wave equation}
\begin{itemize}
\item We developed the separated solution method based on the fundamental solution for the vibrating string
\[
  u(x,t) = u_0 \cos\frac{\pi c t}{L} \sin\frac{\pi x}{L}
\]
\item Consider the four basic solutions 
\begin{align*}
u_1(x,t) &= \cos\lambda ct\sin\lambda x \\
u_2(x,t) &= \cos\lambda ct\cos\lambda x \\
u_3(x,t) &= \sin\lambda ct\sin\lambda x \\
u_4(x,t) &= \sin\lambda ct\cos\lambda x 
\end{align*}
\item Go through the initial and boundary conditions to
\begin{enumerate}
\item Eliminate basic solutions
\item Specify the permissible values of $\lambda$
\item Superpose solutions to fit the initial condition
\end{enumerate}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Laplace transform method}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}