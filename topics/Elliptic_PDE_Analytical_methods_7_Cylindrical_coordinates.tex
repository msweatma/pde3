\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Elliptic PDE Part 7: Cylindrical coordinates}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Translate partial differential equations from Cartesian into cylindrical coordinates
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Solve the Laplace equation in Cartesian coordinates
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Recap}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Procedure to solve Poisson's equation}
\begin{itemize}
\item The Poisson equation is given by
\[ 
\nabla^2 u = f
\]
\item It is the Laplace equation with a scalar \textcolor{red}{Source} or \textcolor{blue}{Sink} term
\end{itemize}
\vskip0.1cm
\begin{block}{Procedure}
\begin{enumerate}
\item Find a particular integral that satisfies the right hand side
\begin{itemize}
\item Remember the rules from EM2A for particular integrals
\item Not always easy to find the particular solution
\item The resulting Laplace equation can have very difficult boundary conditions
\end{itemize}
\item Solve the resulting Laplace equation
\item Add the solution of the Laplace equation and the particular integral to get the solution of the Poisson's equation
\end{enumerate}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\section{Laplace equation in cylindrical coordinates}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Coordinate systems}
\begin{itemize}
\item It is often easier to express problems in coordinate systems different from the standard Cartesian coordinates
\item The integration problems in EM2B made extensive use of cylindrical and polar coordinates
\end{itemize}
\centerline{\includegraphics[height=5cm]{Glyn_Advanced_3_38}
\includegraphics[height=5cm]{Glyn_Advanced_3_39}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Derive cylindrical Laplace equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{2D cylindrical coordinates}
\begin{itemize}
\item The coordinates are given by
\begin{align*}
x &= r \cos(\theta) & r = \sqrt{x^2 + y^2}\\
y &= r \sin(\theta) & \theta = \tan^{-1}\left(\frac{y}{x}\right) 
\end{align*}
\item The Cartesian Laplace equation is
\[
\grad^2u = \pdderiv{u}{x} + \pdderiv{u}{y} = 0
\]
where $x= x(r,\theta)$ and $y = y(r, \theta)$
\item With \(u(x,y) = u(r, \theta)\) we get the Laplace equation in cylindrical coordinates
\[
\grad^2 u(r, \theta) = 0
\]
where $r= r(x, y)$ and $\theta = \theta(x, y)$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}[label=cylindrical_equations]
\frametitle{Laplace equation in cylindrical coordinates}
\begin{itemize}
\item We are using the identity \(r^2 = x^2 + y^2\) and the chain rule 
\[
\pderiv{u}{x} = \pderiv{u}{r}\pderiv{r}{x} + \pderiv{u}{\theta}\pderiv{\theta}{x}
\]
\item With these, we can calculate the partial derivatives involving the coordinates $x$, $y$, $r$ and $\theta$
\item For example the partial derivative with respect to $x$ of $r^2$ is given by
\[
\pderiv{r^2}{x} = \pderiv{(x^2+y^2)}{x} \Rightarrow 2r\pderiv{r}{x} = 2x \Rightarrow \pderiv{r}{x} = \frac{x}{r} 
\]
\item Combining everything we get the Laplace equation in cylindrical coordinates
\begin{align*}
\pdderiv{u}{x} + \pdderiv{u}{y} &= \pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
\end{itemize}
\vskip0.1cm
\hyperlink{derive_cylindrical_equations}{\beamergotobutton{Complete derivation of cylindrical equations}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Separated solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Separated solution in 2D cylindrical coordinates}
\begin{itemize}
\item Consider the Laplace equation in cylindrical coordinates
\begin{align*}
\pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
\item We are going to search for separated solutions
\[u(r,\theta) = R(r) \Theta(\theta)\]
\item We calculate the partial derivatives and insert them into the Laplace equation
\[\frac{d^2 R}{dr^2} \Theta + \frac{1}{r} \frac{dR}{dr}\Theta + \frac{R}{r^2}\frac{d^2\Theta}{d\theta^2} = 0 \]
\item We separate the terms depending on $r$ and $\theta$ to get
\[-\frac{\Theta''}{\Theta} = \frac{R'' + \frac{R'}{r}}{\frac{R}{r^2}} = \lambda
\]
\end{itemize}
\end{frame}
   
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Three basic solutions depending on $\lambda$}
\begin{itemize}
\item Consider three cases similar to the separated solutions in Cartesian coordinates
\item Gets more complicated if we have variation also in the $z$ direction
\item The three basic solutions are given by
\begin{align*}
u(r,\theta) &= (A\theta + B) (C\ln(r) + D) \quad & \lambda = 0 \\
u(r,\theta) &= \left(A\cosh(\mu\theta) + B\sinh(\mu\theta)\right) \left(C r^{-i\mu} + D r^{i\mu}\right) \quad & \lambda =-\mu^2 < 0 \\
u(r,\theta) &= \left(A\cos(\mu\theta) + B\sin(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right) \quad & \lambda = \mu^2> 0
\end{align*}
\item Follow the same steps in picking and eliminating solutions as in the Cartesian case
\item Use the boundary conditions to find the values of the parameters
\end{itemize}
\end{frame}
   
% -------------------------------------------------------- %
\section{Laplace equation}
% -------------------------------------------------------- %
\subsection{Laplace transform}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace transform method}
\begin{itemize}
\item Laplace transforms are not a natural solution method for the Laplace equation!
\item The Laplace equation has no time derivative, so unlike the wave equation and the heat/diffusion equation it cannot be solved using Laplace transforms.
\item Even for a semi-infinite domain where one space dimension is used as 'time' the Laplace transform method is unsuitable
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
%-------------------------------------------------------%
\subsection{Laplace equation}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Laplace equation summary}
\begin{itemize}
\item The Laplace equation in Cartesian and cylindrical coordinates are given by
\begin{align*}
\pdderiv{u}{x} + \pdderiv{u}{y} &= \pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
\item The Laplace equation is well-posed if we have Dirichlet or Neumann conditions on all boundaries
\item The maxima and minima of the solution are always on the boundary
\item The method of separation of variables is the main analytical solution method
\begin{enumerate}
\item Evaluate the basic solutions to find the ones which fulfil the boundary conditions
\item Specify the permissible values of $\mu$
\item Superpose solutions to fit the remaining boundary condition
\end{enumerate}
\item The Laplace transform is not a natural solution method for the Laplace equation
\item Poisson's equation adds an inhomogeneity
\begin{itemize}
\item Makes the problem more complicated
\item The solution maxima and minima can be anywhere   
\end{itemize}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next topic}
\begin{itemize}
\item Topic: \textbf{Parabolic partial differential equations}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

% -------------------------------------------------------- %
\section{Laplace equation in cylindrical coordinates}
% -------------------------------------------------------- %
\subsection{Complete derivation}
% -------------------------------------------------------- %
\begin{frame}[label=derive_cylindrical_equations]
\frametitle{Derivatives in cylindrical coordinates}
\begin{itemize}
\item With the chain rule we get the first partial derivative
\[
\pderiv{u}{x} = \pderiv{u}{r}\pderiv{r}{x} + \pderiv{u}{\theta}\pderiv{\theta}{x}
\]
\item Applying the chain rule again together with the product rule we get the second partial derivatives
\begin{align*}
\pdderiv{u}{x} =& \pderiv{u}{r}\pdderiv{r}{x} + \pderiv{}{x}\left(\pderiv{u}{r}\right)\pderiv{r}{x}
   +\pderiv{u}{\theta}\pdderiv{\theta}{x} + \pderiv{}{x}\left(\pderiv{u}{\theta}\right)\pderiv{\theta}{x}
\end{align*}
\item The terms with the brackets require again the application of the chain rule
\begin{align*}
\pderiv{}{x}\left(\pderiv{u}{r}\right) &= \pderiv{}{r}\left(\pderiv{u}{r}\right)\pderiv{r}{x} + \pderiv{}{\theta}\left(\pderiv{u}{r}\right)\pderiv{\theta}{x} = 
\pdderiv{u}{r}\pderiv{r}{x} + \frac{\partial^2 u}{\partial\theta\partial r} \pderiv{\theta}{x} \\
\pderiv{}{x}\left(\pderiv{u}{\theta}\right) &= \pderiv{}{r}\left(\pderiv{u}{\theta}\right)\pderiv{r}{x} + \pderiv{}{\theta}\left(\pderiv{u}{\theta}\right)\pderiv{\theta}{x} = \frac{\partial^2 u}{\partial r\partial \theta} \pderiv{r}{x} +  \pdderiv{u}{\theta}\pderiv{\theta}{x}
\end{align*}
\end{itemize}
\vskip0.2cm
\hyperlink{cylindrical_equations}{\beamergotobutton{Back to cylindrical equations}}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Partial derivatives of the different coordinates}
\begin{itemize}
\item We can calculate the partial derivatives involving the coordinates $x$, $y$, $r$ and $\theta$ by using the identity
\[
r^2 = x^2 + y^2 
\]
\item Using this we get the following partial derivatives
\vskip-0.5cm
\begin{align}
\pderiv{r^2}{x} = \pderiv{(x^2+y^2)}{x} \Rightarrow 2r\pderiv{r}{x} = 2x &\Rightarrow \pderiv{r}{x} = \frac{x}{r} \\
\pdderiv{r}{x} = \pderiv{}{x}\left(\frac{x}{r}\right) \Rightarrow \pdderiv{r}{x} = \pderiv{}{x}\left(\frac{x}{\sqrt{x^2 + y^2}}\right) &\Rightarrow \pdderiv{r}{x} = \frac{y^2}{r^3} \\
\pderiv{\theta}{x} = \pderiv{}{x} \left(\arctan\left(\frac{y}{x}\right)\right) \Rightarrow \pderiv{\theta}{x} = - \frac{1}{1+\left(\frac{y}{x}\right)^2} \frac{y}{x^2}
&\Rightarrow \pderiv{\theta}{x} = - \frac{y}{r^2} \\
\pdderiv{\theta}{x} = \pderiv{}{x}\left(- \frac{y}{r^2}\right) \Rightarrow \pdderiv{\theta}{x} = \pderiv{}{x}\left(\frac{-y}{x^2 + y^2}\right)
&\Rightarrow \pdderiv{\theta}{x} = \frac{2xy}{r^4}
\end{align}
\item Here we have used
\[
\frac{d}{dt} \arctan(t) = \frac{1}{1+t^2}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Combine all the parts}
\begin{itemize}
\item Combining all the parts we get
\begin{align*}
\pdderiv{u}{x} &= \pdderiv{u}{r} \frac{x^2}{r^2} + \pderiv{u}{r}\frac{y^2}{r^3} + \frac{\partial^2 u}{\partial r\partial \theta} \frac{-2xy}{r^3} + \pderiv{u}{\theta}\frac{2xy}{r^4} + \pdderiv{u}{\theta}\frac{y^2}{r^4} \\
\pdderiv{u}{y} &= \pdderiv{u}{r} \frac{y^2}{r^2} + \pderiv{u}{r}\frac{x^2}{r^3} + \frac{\partial^2 u}{\partial r\partial \theta} \frac{2xy}{r^3} - \pderiv{u}{\theta}\frac{2xy}{r^4} + \pdderiv{u}{\theta}\frac{x^2}{r^4}
\end{align*}
\item By adding the two equations we get
\begin{align}
\pdderiv{u}{x} + \pdderiv{u}{y} &= \pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0 \label{eq:LE_derivation}
\end{align}
\item Equation~\ref{eq:LE_derivation} is the Laplace equation in 2D cylindrical coordinates
\end{itemize}
\vskip0.2cm
\hyperlink{cylindrical_equations}{\beamergotobutton{Back to cylindrical equations}}
\end{frame}

\appendixend

\end{document}