\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Parabolic PDE Part 1: Introduction to parabolic PDE}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Derive the heat conduction equation in one spatial dimension 
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Solve the Laplace equation in Cartesian and cylindrical coordinates
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Recap}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Laplace equation summary}
\begin{itemize}
\item The Laplace equation in Cartesian and cylindrical coordinates are given by
\begin{align*}
\pdderiv{u}{x} + \pdderiv{u}{y} &= \pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
\item The Laplace equation is well-posed if we have Dirichlet or Neumann conditions on all boundaries
\item The maxima and minima of the solution are always on the boundary
\item The method of separation of variables is the main analytical solution method
\begin{enumerate}
\item Evaluate the basic solutions to find the ones which fulfil the boundary conditions
\item Specify the permissible values of $\mu$
\item Superpose solutions to fit the remaining boundary condition
\end{enumerate}
\item The Laplace transform is not a natural solution method for the Laplace equation
\item Poisson's equation adds an inhomogeneity
\begin{itemize}
\item Makes the problem more complicated
\item The solution maxima and minima can be anywhere   
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Heat equation}
% -------------------------------------------------------- %
\subsection{Introduction}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Heat conduction or diffusion equation}
\begin{columns}
\column{0.55\textwidth}
\centerline{\includegraphics[width=0.9\columnwidth]{heated_rod.jpg}}
\column{0.45\textwidth}
The heat conduction equation is given by 
\[
\pderiv{T}{t}=\kappa\pdderiv{T}{x}
\]
where $\kappa$ is a diffusivity
\end{columns}
\vskip0.1cm
\begin{block}{Examples}
\begin{itemize}
\item Calculate heat loss through a building envelope
\item Analyse experiments to find the thermal conductivity of a new insulating material
\item Evaluate the temperature profile in an internal combustion engine
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Example}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example solution of the heat conduction equation}
\begin{block}{Show that}
\[
T(x,t) = \frac1{\sqrt{t}}\exp\left(-\frac{x^2}{4\kappa t}\right)
\]
is a solution of the one dimensional heat conduction equation
\end{block}
\begin{itemize}
\item We calculate the partial derivatives
{\small
\begin{align*}
T_t(x,t) &= \frac{-1}{2t^{3/2}}\exp\left(\frac{-x^2}{4\kappa t}\right)
+\frac{1}{\sqrt{t}}\frac{x^2}{4\kappa t^2}\exp\left(\frac{-x^2}{4\kappa t}\right) \\
T_x(x,t) &= \frac{1}{\sqrt{t}}\frac{-2x}{4\kappa t}\exp\left(\frac{-x^2}{4\kappa t}\right) \\
T_{xx}(x,t) &= \frac{-1}{2\kappa t^{3/2}}\exp\left(\frac{-x^2}{4\kappa t}\right)
+ \frac{1}{\sqrt{t}} \frac{x^2}{4\kappa^2 t^2}\exp\left(\frac{-x^2}{4\kappa t}\right)
\end{align*}}
\item Inserting the partial derivatives into the heat equation we can show that
\[T_t = \kappa T_{xx}\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Reduced temperature plots at different, scaled times $4\kappa t = L^2$ }
\begin{tikzpicture}
  \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$\frac{T}{\sqrt{4\kappa}}$]
  \addplot[black,domain=-2:2, samples=101] {1/sqrt(0.1^2)*exp(-x^2/0.1^2)};
  \addplot[blue, dotted, line width=1.5,domain=-2:2, samples=101] {1/sqrt(0.2^2)*exp(-x^2/0.2^2)};
  \addplot[cyan, dashed, line width=1.5,domain=-2:2, samples=101] {1/sqrt(0.3^2)*exp(-x^2/0.3^2)};
  \addplot[magenta, dashdotted, line width=1.5,domain=-2:2, samples=101] {1/sqrt(0.5^2)*exp(-x^2/0.5^2)};
  \addplot[red, loosely dotted, line width=1.5,domain=-2:2, samples=101] {1/sqrt(1^2)*exp(-x^2/1^2)};

  \legend{$L=0.1$,$L=0.2$,$L=0.3$,$L=0.5$,$L=1.0$}
\end{axis}
\end{tikzpicture} 
\vskip-0.2cm
\[
\frac{T(x,t)}{\sqrt{4\kappa}} = \frac1{\sqrt{4\kappa t}}\exp\left(-\frac{x^2}{4\kappa t}\right) = \frac1{L}\exp\left(-\frac{x^2}{L^2}\right)
\]
\end{frame}

% -------------------------------------------------------- %
\subsection{Derivation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Problem statement}
\centering
\includegraphics[width=0.8\columnwidth]{HeatFlowElement.pdf}
\medskip
\begin{itemize}
\item Consider the heat balance in the element
\item $Q(x,t)$ comes in and $Q(x+\Delta x,t)$ leaves
\item The net increase at time $t$ is given by balancing the energy flows
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Energy balance}
\begin{minipage}[c]{0.3\columnwidth}
\includegraphics[height=0.9\columnwidth,angle=90]{HeatFlowElement.pdf}
\end{minipage}%
\begin{minipage}[c]{0.7\columnwidth}
\begin{itemize}
\item The net increase per unit time is
\[
A c\rho\pderiv{T}{t}\Delta x = A \left( Q(x,t)-Q(x+\Delta x,t) \right)
\]
\item Here the parameters and variables are given by
\begin{itemize}
\item $c$ is the specific heat capacity in J$\,$kg$^{-1}\,$K$^{-1}$
\item $\rho$ is the density in kg$\,$m$^{-3}$
\item $\Delta x$ is the length of the unit in m
\item $A$ is the cross section in m$^2$
\end{itemize}
\item Check the units
\begin{align*}
\text{m}^2\frac{\text{J}}{\text{kg}\,\text{K}} \frac{\text{kg}}{\text{m}^3}\frac{\text{K}}{\text{s}}\text{m} &= \text{m}^2  \left[Q\right] \\
\Rightarrow \frac{\text{J}}{\text{m}^2\,\text{s}} = \frac{\text{W}}{\text{m}^2} &=  \left[Q\right]
\end{align*}
\end{itemize}
\end{minipage}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Apply Fourier's law}
\begin{minipage}[c]{0.3\columnwidth}
\includegraphics[height=0.9\columnwidth,angle=90]{HeatFlowElement.pdf}
\end{minipage}%
\begin{minipage}[c]{0.7\columnwidth}
\begin{itemize}
\item Divide by $A$ and $\Delta x$, and let $\lim \Delta x\rightarrow 0$
\[
c\rho\pderiv{T}{t} =-\pderiv{Q}{x}.
\]
\item Apply Fourier's law 
\[
Q=-k\pderiv{T}{x}
\]
\item With the thermal conductivity $k$ in W$\,$m$^{-1}\,$K$^{-1}$, we obtain
\[
\pderiv{T}{t}=\kappa\pdderiv{T}{x}
\]
\item Here $\kappa=k/c\rho$ is the \textbf{thermal diffusivity} with units m$^{2}\,$s$^{-1}$
\end{itemize}
\end{minipage}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Parabolic-AP1: Heat equation solution}
\vskip-0.cm
\begin{block}{Consider a perfectly insulated rod with the conditions}
\begin{itemize}
\item Both ends are held at fixed temperatures: $T(0,t) = 1$ and $T(L,t) = -1$
\item Initially the wire is at a given temperature $T(x,0) = \cos(\pi x/L)$ for $0\le x\le L$
\end{itemize}
What is the solution of this problem for $t\to\infty$?
\vskip-0.1cm
\begin{itemize}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.2cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\centerline{\includegraphics[width=6cm]{heat_steady_state}}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\begin{enumerate}
\item Solution 3 is correct: at $t\to\infty$ the initial disturbance will have dispersed and the solution will be a linear temperature profile between the two boundary values
\end{enumerate}
\vskip0.2cm
\centerline{\includegraphics[width=4.2cm]{heat_steady_state}}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
% -------------------------------------------------------- %
\subsection{Parabolic PDE introduction}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Heat conduction or diffusion equation}
\begin{itemize}
\item The heat conduction equation in $n$ space dimensions is given by
\begin{equation}
\frac1\kappa\pderiv{T}{t} = \grad^2T = \pdderiv{T}{x_1} + \cdots + \pdderiv{T}{x_n}
\label{eq:heat_conduction_PDE}
\end{equation}
\item This equation is \textcolor{red}{parabolic} in nature and given enough time (and constant boundary conditions) the solution will converge to a steady state
\item If there are no time-varying inputs this has the steady state solution, $U$, satisfying the Laplace equation
\[ 
\nabla^2U = 0
\]
and the boundary conditions
\item Depending on the context, the parabolic partial differential equation~\ref{eq:heat_conduction_PDE} can describe many diffusion processes
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Nondimensionalisation and diffusion equation}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}