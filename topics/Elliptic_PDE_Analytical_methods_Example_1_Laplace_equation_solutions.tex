\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Elliptic PDE Example 1: Laplace equation solutions}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Laplace equation solutions}
% -------------------------------------------------------- %
\subsection{Laplace equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace Equation}
\begin{block}{The Laplace equation is given by}
\[
\grad^2u = \pdderiv{u}{x_1} + \pdderiv{u}{x_2} + \cdots + \pdderiv{u}{x_n} = 0
\]
\end{block}
\begin{itemize}
\item It is the most fundamental \textcolor{blue}{elliptic} PDE which models the steady state of a diffusion like process
\item Used to describe the pressure in an incompressible flow, steady state diffusion and heat conduction, stress and strain, hydrostatics, electrostatics, ground water flow, etc.
\item It is easy to find \underline{\textbf{a}} solution -- finding \underline{\textbf{the}}  solution matching the boundary conditions is the hard part
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Inviscid, irrotational flow}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Inviscid, irrotational flow}
\begin{itemize}
\item Used in subsonic aerodynamics
\item Can be used to calculate lift on aerofoils
\item One solution to the Laplace equation is the stream function
\[
\psi(x,y) = Uy\left(1-\frac{a^2}{x^2+y^2}\right)
\]
\item This functions satisfies the Laplace equation
\begin{enumerate}
\item Calculate the second derivatives with respect to \(x\) and \(y\)
\item Insert these into the Laplace equation
\item Check that they add up to zero
\end{enumerate}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Show the stream function satisfies the Laplace equation}
\begin{itemize}
\item Calculate the first and second partial derivatives with respect to both dimensions of
\[
\psi(x,y) = Uy\left(1-\frac{a^2}{x^2+y^2}\right)
\]
\item The derivatives are given by
\vskip-0.6cm
\pause
{\small
\begin{align*}
\psi_x(x,y) &= \frac{2xyUa^2}{(x^2+y^2)^2}\\
\psi_y(x,y) &= U - \frac{Ua^2}{x^2+y^2} + \frac{2y^2Ua^2}{(x^2+y^2)^2} \\
\psi_{xx}(x,y) &= \frac{2yUa^2}{(x^2+y^2)^2} - \frac{8x^2yUa^2}{(x^2+y^2)^3} \\
\psi_{yy}(x,y) &= \frac{6yUa^2}{(x^2+y^2)^2} - \frac{8y^3Ua^2}{(x^2+y^2)^3}
\end{align*}}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Show the stream function satisfies the Laplace equation}
\begin{itemize}
\item Take the second partial derivatives 
\vskip-0.6cm
{\small
\begin{align*}
\psi_{xx}(x,y) &= \frac{2yUa^2}{(x^2+y^2)^2} - \frac{8x^2yUa^2}{(x^2+y^2)^3} \\
\psi_{yy}(x,y) &= \frac{6yUa^2}{(x^2+y^2)^2} - \frac{8y^3Ua^2}{(x^2+y^2)^3}
\end{align*}}
\item Insert them into the Laplace equation
\[
\pdderiv{\psi}{x} + \pdderiv{\psi}{y} = 0 
\]
\item To get
\vskip-0.5cm
\pause
{\small
\begin{align*}
\grad^2\psi(x,y) &= \frac{2yUa^2}{(x^2+y^2)^2} - \frac{8x^2yUa^2}{(x^2+y^2)^3} + \frac{6yUa^2}{(x^2+y^2)^2} - \frac{8y^3Ua^2}{(x^2+y^2)^3} \\
&= \frac{8yUa^2}{(x^2+y^2)^2}-\frac{8y(x^2+y^2)Ua^2}{(x^2+y^2)^3} \\
&= \frac{8yUa^2}{(x^2+y^2)^2}-\frac{8yUa^2}{(x^2+y^2)^2}  = 0
\end{align*}}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Polynomial solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Multivariate polynomial}
\begin{itemize}
\item Consider the function
\vskip-0.2cm
\[
u(x,y) = a(x^2 - y^2) + bxy + c
\]
with constants $a, b, c$
\item The partial derivatives are given by
\pause
\begin{align*}
\pderiv{u}{x} &= 2ax + by \qquad &\pderiv{u}{y} &= -2ay + bx \\
\pdderiv{u}{x} &= 2a \qquad &\pdderiv{u}{y} &= -2a \\
\frac{\partial^2 u}{\partial x \partial y} &= b \qquad &\frac{\partial^2 u}{\partial y \partial x} &= b
\end{align*}
\item $u(x,y)$ is a solution to the Laplace equation
\item The order of integration makes no difference if the function is smooth enough
\item True for most functions in engineering
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Trigonometric-exponential solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Trigonometric-exponential solution}
\begin{itemize}
\item Consider the function
\[
v(x,y) = e^{-y} \cos(x)
\]
\item 
The partial derivatives are
\pause
\begin{align*}
\pderiv{v}{x} &= -e^{-y} \sin(x) \qquad &\pderiv{v}{y} &= -e^{-y} \cos(x) \\
\pdderiv{v}{x} &= -e^{-y} \cos(x) \qquad &\pdderiv{v}{y} &= e^{-y} \cos(x)
\end{align*}
\item $v(x,y)$ is also a solution of the Laplace equation
\item The superposition of $u$ and $v$ is also a solution
\[
w(x,y) = u(x,y) + v(x,y) = a(x^2 - y^2) + bxy + c + e^{-y} \cos(x)
\]
\item The particular solution for a given problem is determined by the boundary conditions
\end{itemize}
\end{frame}


%-------------------------------------------------------%
\section{Thank you}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}