\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Parabolic PDE Example 5: Radially symmetric heat conduction example 9.24}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Example 9.24b}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Problem definition}
Solve the radially symmetric heat conduction equation
\[\frac{1}{r^2}\frac{\partial}{\partial r}\left(r^2\pderiv{T}{r}\right)=\frac{1}{\kappa}\pderiv{T}{t}\]
\begin{itemize}
\item In the region $r\ge a$ 
\item Subject to the boundary conditions
\begin{align*}
T(a,t) &= T_0 & \forall t\ge0 \\
\lim_{r\rightarrow\infty} T(r,t) &= 0 & \forall t\ge0 \\
T(r,0) &= 0 & \forall r>a
\end{align*}
\item $T_0$ is a constant temperature
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Parabolic-AP6: Scaled boundary conditions}
\vskip-0.cm
\begin{block}{Find the boundary conditions for $\theta(r,t) = r T(r,t)$?}
\begin{itemize}
\item Consider $\theta(r,t) = r T(r,t)$ and solve the transformed equation
\item We need to adjust the boundary conditions for $\theta$
\begin{align*}
T(a,t) &= T_0 \implies & \theta(a,t) &= ? & \forall t\ge0 \\
\lim_{r\rightarrow\infty} T(r,t) &= 0 \implies & \lim_{r\rightarrow\infty} \theta(r,t) &<\text{const} & \forall t\ge0 \\
T(r,0) &= 0 \implies & \theta(r,0) &= 0 & \forall r>a
\end{align*}
\end{itemize}
\end{block}
\vskip-0.2cm
\begin{overprint}
\onslide<1>
\begin{block}{Solution}
\vskip0.2cm
\begin{enumerate}
\item The correct boundary condition is given by the following condition
\begin{align*}
\theta(a,t) &= a T(a,t) = a T_0 & \forall t\ge0
\end{align*}
\end{enumerate}
\end{block}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace transform solution}
\vskip-0.1cm
\begin{itemize}
\item The equation for the scaled temperature $\theta$ is the same as the heat conduction or diffusion equation in Cartesian coordinates
\item Therefore, we can directly write 
\[
\overline{\theta}(r,s) = A(s)\exp\left(r\sqrt{\frac{s}{\kappa}}\right) + B(s)\exp\left(-r\sqrt{\frac{s}{\kappa}}\right)
\]
\item Since $\lim_{r\rightarrow\infty} \theta(r,t)$ is bounded it follows that the Laplace transform also needs to be bounded
\item From this it directly follows that the positive exponential needs to be zero and thus
\[A(s) = 0\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Apply second boundary condition}
\vskip-0.1cm
\begin{itemize}
\item Transforming the boundary condition at $r=a$ gives
\[
\overline{\theta}(a,s) = a\frac{T_0}{s}
\]
\item The general solution $\overline{\theta}$ needs to match this value at the boundary
\[
\overline{\theta}(a,s) = a\frac{T_0}{s} \overset{!}{=} B(s)\exp\left(-a\sqrt{\frac{s}{\kappa}}\right)
\]
\item From this it follows that
\[
B(s) = \frac{aT_0}{s}\exp\left(a\sqrt{\frac{s}{\kappa}}\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Inverse Laplace transform}
\begin{itemize}
\item The solution of the transformed equation is therefore
\[
\overline{\theta}(r,s) = \frac{aT_0}{s}\exp\left(-(r-a)\sqrt{\frac{s}{\kappa}}\right)
\]
\item Finding the inverse transform for 
\[
\overline{\theta}(r,s) = \frac{aT_0}{s}\exp\left(-(r-a)\sqrt{\frac{s}{\kappa}}\right)
\]
is not easy
\item We need extensive tables of Laplace transforms or computer algebra tools.  
\item In this case the inverse is given by the complementary error function
\[
\theta(r,t) = aT_0\ \text{erfc}\left(\frac12\frac{r-a}{\sqrt{\kappa t}}\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Error function}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{The error function}
\begin{block}{The error function $\text{erf}(z)$ is given by}
\[\text{erf}(z)=\frac2{\sqrt{\pi}}\int_0^z e^{-\nu^2}\text{ d}\nu\]
\end{block}
\begin{block}{The complementary error function $\text{erfc}(z)$ is given by}
\[\text{erfc}(z)=1-\text{erf}(z)\]
\end{block}
\begin{itemize}
\item Both commonly occur in the solution of heat conduction and diffusion problems (and are widely used in statistics)
\item Evaluating $\text{erf}(z)$ is tricky but either tables or approximations can be used
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{The error function and the complementary error function for $x>0$}
\includegraphics[width=0.8\columnwidth]{error_function}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Unscale the solution}
\begin{itemize}
\item The solution of the scaled temperature $\theta$ is 
\[
\theta(r,t) = aT_0\ \text{erfc}\left(\frac12\frac{r-a}{\sqrt{\kappa t}}\right)
\]
\item Recall the scaling of the temperature
\[
\theta(r,t) = r T(r,t)
\]
\item We get the solution in terms of temperature
\[ 
T(r,t) = \frac{aT_0}{r}\ \text{erfc}\left(\frac12\frac{r-a}{\sqrt{\kappa t}}\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Plot solution at scaled times $\tau=\frac{4\kappa t}{a^2}$}
\includegraphics[width=0.8\columnwidth]{example_924_solution}
\end{frame}

%-------------------------------------------------------%
\section{Thank you}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}