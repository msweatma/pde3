\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Hyperbolic PDE -- Part 8: Initial and boundary conditions}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Give the appropriate boundary conditions for the wave equation
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Use the Laplace transform method to find the solution of the wave equation
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Recap}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Laplace transform method}
\begin{itemize}
\item The Laplace transform generates the following ODE from the 1D wave equation
\[\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s^2}{c^2}U(x,s) = \frac{-1}{c^2} u'(x,0) - \frac{s}{c^2} u(x,0)
\]
\item The Laplace domain solution is given from the complimentary solution plus the particular integral
\[ U(x,s) = U_c(x,s) + U_p(x,s) = A(s) e^{\frac{sx}{c}} + B(s) e^{\frac{-sx}{c}} + U_p(x,s)
\]
\item The parameters $A(s)$ and $B(s)$ can be found from the boundary conditions
\item The resulting Laplace domain solution needs to be inverted back into the time domain
\end{itemize}
\vskip0.1cm
\begin{block}{Comments}
\begin{itemize}
\item The inverse transforms, if it exist, can often be very complicated so the use of a computer algebra system is recommended
\item Extension to other coordinate systems and other PDEs is possible
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\section{Boundary conditions}
% -------------------------------------------------------- %
\subsection{Different types}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Boundary conditions}
If $\Omega$ is the domain on which the given equation is to be solved and $\partial\Omega$ denotes its boundary, we can have
\begin{itemize}
\item Cauchy
\[
\text{both }u\text{ and }\pderiv{u}{n}\text{ are specified on }\partial\Omega
\]
\item Dirichlet
\[
u\text{ is specified on } \partial\Omega
\]
\item Neumann
\[
\pderiv{u}{n}\text{ is specified on }\partial\Omega
\]
\item Robin
\[
au+B\pderiv{u}{n}=g\text{ on }\partial\Omega
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Mixed boundary conditions}
Two or more BC of different types are specified on $\partial\Omega$
\centerline{\includegraphics[height=0.7\textheight]{MixedBC}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Relation to reality}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Boundary conditions and physics}
\begin{itemize}
\item If we consider the wave equation being used to model a vibrating string,
\begin{itemize}
\item the ends of the string are likely to be fixed in place -- \textcolor{blue}{Dirichlet} conditions
\item it seems natural for the initial displacement and the velocity of the string  to be specified -- \textcolor{blue}{Cauchy condition}
\end{itemize}
\item There are alternatives
\begin{enumerate}
\item We could also fix the gradient at the ends of the string using \textcolor{blue}{Neumann} conditions
\item The initial position of the string could be known (from a photograph) and its position at a later time (from a second exposure) giving \textcolor{blue}{Dirichlet} conditions
\end{enumerate}
\item The important question is can a solution be found?
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Uniqueness}
\begin{itemize}
\item The vibrating string problem (Example 9.1) has the solution
\[
u = u_0\sin\frac{\pi x}{L}\cos\frac{\pi ct}{L}
\]
\item If we know its position at $t=L/2c$ and $t=3L/2c$ can we find a solution?
\[
\text{At }t = \frac{L}{2c},\  \cos\frac{\pi ct}{L} = \cos\frac{\pi}{2} = 0\implies u(x)=0\quad \forall x
\]
\[
\text{At }t = \frac{3L}{2c},\  \cos\frac{\pi ct}{L}=\cos\frac{3\pi}{2}=0\implies u(x)=0\quad \forall x
\]
\item So in both cases the string is horizontal
\item One solution is that $u_0=0$ and the string is stationary, but many other solutions are possible
\item Here the boundary conditions \textcolor{red}{do not provide a unique solution}
and so cannot be used
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Wave equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Wave equation boundary conditions}
\centerline{\includegraphics[width=0.9\columnwidth]{WaveBC}}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Appropriateness of Boundary Conditions}
\centerline{\includegraphics[width=0.95\columnwidth]{BCMatrix.pdf}}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
%-------------------------------------------------------%
\subsection{Hyperbolic partial differential equations}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Wave equation summary}
\begin{itemize}
\item The wave equation is given by 
\[\frac{1}{c^2}\pdderiv{u}{t}=\pdderiv{u}{x}+\pdderiv{u}{y}+\pdderiv{u}{z}=\grad^2u\]
\item The wave equation is well-posed if 
\begin{itemize}
\item The boundary is open
\item We are given Cauchy initial conditions
\item The two boundary conditions can be Dirichlet or Neumann
\end{itemize}
\item The wave equation can be solved with a number of different methods
\begin{enumerate}
\item D'Alembert solution
\item Method of characteristics
\item Separation of variables
\item Laplace transform method
\end{enumerate}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}