\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Parabolic PDE Part 5: Laplace transform method}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Apply the Laplace transform method to the heat conduction or diffusion equation
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Give the appropriate boundary conditions for the heat conduction or diffusion equation
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Recap}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Well-posed parabolic PDE}
\begin{itemize}
\item The boundary is open
\begin{itemize}
\item Not all boundaries have a condition attached
\end{itemize}
\item The other boundaries have Dirichlet or Neumann conditions
\end{itemize}
\vskip0.2cm
\centerline{\includegraphics[width=0.6\columnwidth]{DiffusionBC.pdf}}
\end{frame}

% -------------------------------------------------------- %
\section{Laplace transform method}
% -------------------------------------------------------- %
\subsection{Recap: Laplace transform}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Recap: Laplace transform definition}
\vskip-0.1cm
\begin{block}{Definition of the Laplace transform}
The Laplace transform of a function $f(t)$ is defined as
\[
 {\cal L} \left\{ f(t) \right\} = F(s) = \int_0^\infty f(t) e^{-st}dt
\]
for those $s\in\mathbb{C}$ for which the integral exists
\end{block}
\vskip0.3cm
\begin{block}{Remarks}
\begin{itemize}
\item Transforms a function $f(t)$ with a real argument $t\ge0$ to a complex valued function $F(s)$ with complex argument $s$
\item Laplace transform of the Dirac delta function $\delta(t)$ is
\[
\mathcal{L}\left\{T\delta(t)\right\} = T
\]
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Partial derivative transforms}
\begin{itemize}
\item Consider the function
\[
u(x,t),\,t\ge0
\]
\item The Laplace transforms of the partial derivatives are given by
\begin{align*}
\mathcal{L}\left\{\pderiv{u}{x}\right\} &= \frac{\text{d}}{\text{d}x}U(x,s) \\
\mathcal{L}\left\{\pdderiv{u}{x}\right\} &= \frac{\text{d}^2U(x,s)}{\text{d}x^2} \\
\mathcal{L}\left\{\pderiv{u}{t}\right\} &= sU(x,s)-u(x,0)\\
\mathcal{L}\left\{\pdderiv{u}{t}\right\} &= s^2U(x,s)-su(x,0)-u_t(x,0)
\end{align*}
\item Here, $U(x,s)$ is the Laplace transform of $u(x,t)$ with respect to $t$
\end{itemize}
\end{frame}

%----- Frame -------------------------------------------%
\begin{frame}\frametitle{Laplace transform to solve differential equations}
\vskip-0.1cm
\begin{block}{Laplace transform method}
\begin{enumerate}
\item Transform the differentiation in the time domain into an algebraic operation in the Laplace domain
\item The solution in the Laplace domain describes the Laplace transform of the system response
\item Get the time response by the inverse Laplace transform
\end{enumerate}
\end{block}
\vskip0.0cm
\begin{block}{Remarks}
\begin{itemize}
\item Ordinary differential equations are reduced to algebraic equations
\item The initial conditions are automatically included: complete solution to the differential equation
\item One of the derivatives of partial differential equations is removed
\end{itemize}
\end{block}
\vskip0.1cm
\end{frame}

% -------------------------------------------------------- %
\section{Laplace transform example}
% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution by Laplace transforms}
\begin{itemize}
\item Applying the Laplace transform to the diffusion equation we get
\[
sU(x,s)-u(x,0)=\kappa \frac{\text{d}^2U(x,s)}{\text{d}x^2}
\]
\item Rearrange the equation to get
\[
\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s}{\kappa}U = -\frac{u(x,t=0)}{\kappa} 
\]
\item We can find the complimentary function by finding the characteristic polynomial 
\[
m^2 - \frac{s}{\kappa} = 0
\]
and remembering the ODE solution methods
\[
U(x,s) = A(s)\exp\left(x\sqrt{\frac{s}{\kappa}}\right) + B(s)\exp\left(-x\sqrt{\frac{s}{\kappa}}\right)
\]
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Parabolic-AP5: Laplace transform}
\vskip-0.cm
\begin{block}{What is the Laplace transform with respect to $x$ of the 2D Laplace equation?}
\[
0 = \pdderiv{u}{x} + \pdderiv{u}{y} = \grad^2 u
\]
\vskip-0.1cm
\begin{itemize}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.2cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\vskip0.2cm
\begin{enumerate}
\item $\pdderiv{U(x,s)}{x} + s^2 U(x,s) - s u(0, y) - u'(0,y) = 0$
\item $s^2 U(s,y) - s u(0, y) - u'(0,y) + s^2 U(x,s) - s u(x, 0) - u'(x,0) = 0$
\item $s^2 U(s,y) - s u(0, y) - u'(0,y) + \pdderiv{U(s,y)}{y} = 0$
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\begin{block}{Solution}
\vskip0.2cm
\begin{enumerate}
\item The correct solution is
\[
s^2 U(s,y) - s u(0, y) - u'(0,y) + \pdderiv{U(s,y)}{y} = 0
\]
\end{enumerate}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
% -------------------------------------------------------- %
\subsection{Laplace transform method}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solving the heat conduction with the Laplace transform}
\begin{itemize}
\item Applying the Laplace transform with respect to $t$ to the parabolic equation
\[
sU(x,s)-u(x,0)=\kappa \frac{\text{d}^2U(x,s)}{\text{d}x^2}
\]
\item Insert the boundary condition at $t=0$ to get
\[
\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s}{\kappa}U = -\frac{u(x,t=0)}{\kappa} 
\]
\item Use the characteristic polynomial to find the complimentary function
\[
U_c(x,s) = A(s)\exp\left(x\sqrt{\frac{s}{\kappa}}\right) + B(s)\exp\left(-x\sqrt{\frac{s}{\kappa}}\right)
\]
\item Find a particular integral satisfying the inhomogeneity
\item Use the inverse Laplace transform to find the time domain solution from the particular solution
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Heat conduction or diffusion equation in spherical coordinates}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}