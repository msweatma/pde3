\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Hyperbolic PDE Example 3: Laplace transform method example 9.16}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Example 9.16}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Problem definition}
\textbf{Problem:} Use the Laplace transform method to find the solution to the wave equation for a semi-infinite string with the following initial and boundary conditions
\begin{align}
u(x,0) &= 0 &\forall x\ge0 \label{eq:wave_916_IC1} \\
u'(x,0) = \pderiv{u(x,0)}{t} &= xe^{-\frac{x}{a}} & \forall x\ge0 \label{eq:wave_916_IC2}\\
u(0,t) &= 0 & \forall t\ge0 \label{eq:wave_916_BC1} \\
\lim_{x\rightarrow\infty}u(x,t) &= 0 & \forall t\ge0 \label{eq:wave_916_BC2}
\end{align}
\vskip0.1cm
\begin{enumerate}
\item The string is initially undisturbed
\item It has an initial velocity, i.e. it is moving at $t=0$
\item The string is fixed at $x=0$
\item It is held at infinity
\end{enumerate}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Find the Laplace domain ordinary differential equation}
\begin{itemize}
\item Taking the Laplace transform with respect to time and substituting the initial conditions~\ref{eq:wave_916_IC1} and \ref{eq:wave_916_IC2} we get
{\small
\[
c^2\frac{\text{d}^2U(x,s)}{\text{d}x^2} = s^2U(x,s) - su(x,0) - u'(x,0) =s^2U(x,s)-xe^{-\frac{x}{a}}
\]}
\item Rearrange to standard form to get
\[\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s^2}{c^2}U(x,s) = \frac{-1}{c^2}xe^{-\frac{x}{a}}\]
\item The characteristic polynomial of the ODE has the zeros $\lambda_{1,2}=\pm\frac{s}{c}$
\item Thus, the complementary function is
\[
U_c(x,s)=A(s) e^{\frac{sx}{c}} + B(s) e^{\frac{-sx}{c}}
\]
where $A(s)$ and $B(s)$ are arbitrary functions of $s$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Define a trial function and insert it into the Laplace domain ODE}
\begin{itemize}
\item We seek a particular integral of the ODE of the form
\[
U_p(x,s) = \alpha xe^{-\frac{x}{a}} + \beta e^{-\frac{x}{a}}
\]
\item Calculate the second derivative
\begin{align*}
\frac{dU_p}{dx} &= \alpha e^{-\frac{x}{a}} - \frac{\alpha}{a} xe^{-\frac{x}{a}} - \frac{\beta}{a}e^{-\frac{x}{a}} \\
\frac{d^2U_p}{dx^2 } &= -\frac{\alpha}{a} e^{-\frac{x}{a}} - \frac{\alpha}{a} e^{-\frac{x}{a}} + \frac{\alpha}{a^2} xe^{-\frac{x}{a}} + \frac{\beta}{a^2}e^{-\frac{x}{a}}
\end{align*}
\item Insert this into the differential equation in the Laplace domain
\begin{align*}
e^{-\frac{x}{a}}\left[\frac{\beta}{a^2} - \frac{2\alpha}{a}\right] + \frac{\alpha}{a^2} x e^{-\frac{x}{a}} - \frac{s^2}{c^2} \left[\alpha xe^{-\frac{x}{a}} + \beta e^{-\frac{x}{a}}\right] &= \frac{-1}{c^2}x e^{-\frac{x}{a}}
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Find the parameters of the trial function}
\begin{itemize}
\item Collect the terms
\begin{align*}
e^{-\frac{x}{a}}\left[\frac{\beta}{a^2} - \frac{2\alpha}{a} - \frac{s^2}{c^2}\beta\right] + x e^{-\frac{x}{a}}\left[\frac{\alpha}{a^2} - \frac{\alpha s^2}{c^2} + \frac{1}{c^2}\right] &= 0
\end{align*}
\item Solve for $\alpha$
\[
\frac{\alpha}{a^2} - \frac{\alpha s^2}{c^2} = -\frac{1}{c^2} \implies \alpha = \frac{1}{s^2 - \frac{c^2}{a^2}}
\]
\item Use $\alpha$ to solve for $\beta$
\begin{align*}
\beta \left(\frac{1}{a^2} - \frac{s^2}{c^2}\right) - \frac{2\alpha}{a} &= 0 \\
\implies \beta \frac{c^2 - s^2 a^2}{a^2 c^2} &= \frac{2}{a}\frac{1}{s^2 - \frac{c^2}{a^2}} \\
\implies \beta &= \frac{2}{a}\frac{1}{s^2 - \frac{c^2}{a^2}} \frac{a^2 c^2}{c^2 - s^2 a^2} = -\frac{\frac{2 c^2}{a}}{\left(s^2 - \frac{c^2}{a^2}\right)^2}
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Use the right boundary condition}
\begin{itemize}
\item With the parameters $\alpha$ and $\beta$ we obtain the general solution
\[
U(x,s)=A(s) e^{\frac{sx}{c}} + B(s) e^{\frac{-sx}{c}} + \frac{e^{-\frac{x}{a}}}{s^2 - \frac{c^2}{a^2}}
\left[x - \frac{2\frac{c^2}{a}}{s^2 - \frac{c^2}{a^2}}\right]
\]
\item $A(s)$ and $B(s)$ are arbitrary functions which we need to find from the boundary conditions
\item We can find $A(s)$ and $B(s)$ by using the boundary conditions~\ref{eq:wave_916_BC1} and \ref{eq:wave_916_BC2}
\item For this, we have to transform the boundary conditions~\ref{eq:wave_916_BC1} and \ref{eq:wave_916_BC2} into the Laplace domain
\begin{align*}
u(0,t)=0&\implies U(0,s)=0 \\
\lim_{x\rightarrow\infty}u(x,t)=0&\implies\lim_{x\rightarrow\infty}U(x,s)=0
\end{align*}
\item Clearly $\lim_{x\rightarrow\infty}U(x,s) = 0 \implies A(s) =0$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Use the left boundary condition}
\begin{itemize}
\item Considering the boundary condition at $x=0$, we get
\[
U(0,s) = B(s)e^{\frac{-s0}{c}} + \frac{e^{-\frac{0}{a}}}{s^2 - \frac{c^2}{a^2}}
\left[0 - \frac{2\frac{c^2}{a}}{s^2 - \frac{c^2}{a^2}}\right] \overset{!}{=} 0
\]
\item Solving this equation for $B(s)$ it follows that
\begin{align*}
0  &= B(s) - \frac{2\frac{c^2}{a}}{\left(s^2 - \frac{c^2}{a^2}\right)^2} 
\implies B(s) = \frac{2\frac{c^2}{a^2}}{\left(s^2 - \frac{c^2}{a}\right)^2}
\end{align*}
\item The complete solution in the Laplace domain is given by
\[
U(x,s) = \frac{2\frac{c^2}{a}}{\left(s^2 - \frac{c^2}{a^2}\right)^2}e^{-\frac{sx}{c}} + \frac{e^{-\frac{x}{a}}}{s^2 - \frac{c^2}{a^2}}\left[x - \frac{2\frac{c^2}{a}}{s^2 - \frac{c^2}{a^2}}\right]
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Invert the Laplace domain solution}
\begin{itemize}
\item Fortunately the Laplace domain solution can be inverted with the time shift theorem and the following functions transforms
{\small
\begin{align*}
\mathcal{L}\left\{f(t-\alpha) H(t-\alpha)\right\} &= \exp(-\alpha s)F(s) \\
\mathcal{L}\left\{\sinh\omega t\right\}&=\frac{\omega}{s^2-\omega^2} \\
\mathcal{L}\left\{\cosh\omega t\right\}&=\frac{s}{s^2-\omega^2} \\
\mathcal{L}\left\{\frac{\omega t\cosh\omega t-\sinh\omega t}{2\omega^3}\right\}&=\frac{1}{(s^2-\omega^2)^2}
\end{align*}}
\item The time domain solution is given by
\begin{align*}
u(x,t) &= \frac{a}{c}\left[(ct-x)\cosh\left(\frac{ct-x}{a}\right)H(ct-x)-cte^{-\frac{x}{a}}\cosh\left(\frac{ct}{a}\right)\right] \\
 &+ \frac{a}{c}\left[(e^{-\frac{x}{a}}(x+a)\sinh\left(\frac{ct}{a}\right) - a\sinh\left(\frac{ct-x}{a}\right)H(ct-x)\right]
\end{align*}
for $t\ge0$ and with the Heaviside step function $H(t)$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution plot for $a=1$ and $c=1$}
\begin{tikzpicture}
\begin{axis}[legend style={draw=none},
legend pos=outer north east,
xlabel=$x$,ylabel=$u$]
% t=0.0
\addplot[black,domain=0:10, samples=201] {0.0};
% t=0.5
\addplot[cyan, dotted, line width=1.5,domain=0:10, samples=201] {(0.5-x)*cosh(0.5-x)*(0.5+0.5*tanh(0.5-x*10))-0.5*exp(-x)*cosh(0.5)
  +exp(-x)*(x+1)*sinh(0.5)-sinh(0.5-x)*(0.5+0.5*tanh(0.5-x*10))};
% t=1.0
\addplot[blue, dashed, line width=1.5,domain=0:10, samples=201] {(1.0-x)*cosh(1.0-x)*(0.5+0.5*tanh(1.0-x*10))-1.0*exp(-x)*cosh(1.0)
  +exp(-x)*(x+1)*sinh(1.0)-sinh(1.0-x)*(0.5+0.5*tanh(1.0-x*10))};
% t=2.0
\addplot[red, dashdotted, line width=1.5,domain=0:10, samples=201] {(2.0-x)*cosh(2.0-x)*(0.5+0.5*tanh(2.0-x*10))-2.0*exp(-x)*cosh(2.0)
  +exp(-x)*(x+1)*sinh(2.0)-sinh(2.0-x)*(0.5+0.5*tanh(2.0-x*10))};

\legend{$t=0.0$,$t=0.5$,$t=1.0$,$t=2.0$}
\end{axis}
\end{tikzpicture}
\end{frame}


%-------------------------------------------------------%
\section{Thank you}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}
