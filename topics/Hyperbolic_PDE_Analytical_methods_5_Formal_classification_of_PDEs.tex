\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Hyperbolic PDE -- Part 5: Formal classification of PDEs}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Characterise a general second-order PDE as either elliptic, parabolic or hyperbolic
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Use the method of characteristics to calculate the solution to the wave equation
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Recap}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Method of characteristics}
\begin{columns}
\column{0.55\textwidth}    
\begin{itemize}
\item We can find the solution at the point $P$ as
\[
u(P) = f(B)+g(A)
\]
\vskip-0.2cm
\item Since $F$ and $G$ are given we can compute
\begin{align*}
f^\prime(x) &= \frac12\left[F^\prime(x)+\frac{G(x)}{c}\right] \\
g^\prime(x) &= \frac12\left[F^\prime(x)-\frac{G(x)}{c}\right] 
\end{align*}
\end{itemize}
\column{0.5\textwidth}
\centerline{\includegraphics[width=0.91\columnwidth]{domainofdependence_left}}
\end{columns}
\begin{block}{Comments}
\begin{itemize}
\item For non-constant coefficient equations the characteristics lines will be curved
\item Intersecting characteristics may lead to shocks in the solution
\item The method of characteristics is the basis of both space marching schemes for solving hyperbolic equations and for semi-Lagrangian methods\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\section{Formal classification}
% -------------------------------------------------------- %
\subsection{Procedure}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Formal Classification of second order PDEs}
\begin{itemize}
\item Given the general second-order PDE
\[
Au_{xx}+2Bu_{xy}+Cu_{yy}+Du_x+Eu_y+F = 0
\]
where $A,\, B,\, C,\ldots$ are constants
\item We can make the change of variable to use characteristics 
\begin{align*}
r &= ax + y \\
s &= x + by
\end{align*}
\item We obtain (see section 9.8.1)
\[
(A+2bB+b^2C)\left[u_{ss}+\frac{AC-B^2}{(A+Bb)^2}u_{rr}\right]+\ldots = 0
\]
\item The $\ldots$ indicate low order derivatives
\item The behaviour of the PDE depends on the sign of $AC-B^2$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Elliptic equations}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Classification: Elliptic equation}
\begin{block}{Elliptic equation}
\begin{itemize}
\item $AC-B^2>0$
\item Writing $\lambda^2=(AC-B)^2/(A+bB)^2$ we obtain
\[
\alpha(u_{ss}+\lambda^2u_{rr})+\ldots = 0
\] 
\item By substituting $q=\frac{r}\lambda$ we get
\[
u_{ss} + u_{qq} + \ldots = 0
\]
\item This is similar to Laplace's Equation
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Parabolic equations}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Classification: Parabolic equation}
\begin{block}{Parabolic equation}
\begin{itemize}
\item $AC-B^2=0$ 
\item Since $AC-B^2=0$ we have 
\[
u_{ss}+\ldots = 0
\]
\item The heat conduction or diffusion equation has only a first order partial derivative in the temporal variable
\item This is very similar to the heat conduction/diffusion equation
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Hyperbolic equations}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Classification: Hyperbolic equation}
\begin{block}{Hyperbolic equation}
\begin{itemize}
\item  $AC-B^2<0$
\item Writing $-\mu^2=(AC-B)^2/(A+bB)^2$ we obtain
\[
\alpha(u_{ss}-\mu^2u_{rr})+\ldots = 0
\]
\item This can be expressed as
\[
u_{ss}-u_{tt} +\ldots= 0
\]
where $t=\frac{r}\mu$
\item This is similar to the wave equation
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Example 9.45}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Evaluate the problem classification for different $\alpha$}
\begin{itemize}
\item Evaluate the behaviour of the following PDE for different values of $\alpha$
\[
0 = u_{xx}+2u_{xt}+2\alpha u_{tt}
\]
\item With the notation from previous slides this can be transformed into
\[ u_{ss} + \frac{2\alpha - 1}{(1+b)^2} u_{rr} = 0
\]
\begin{itemize}
\item For $\alpha > 0.5$ we get an elliptic equation
\item For $\alpha = 0.5$ we get a parabolic equation
\item For $\alpha < 0.5$ we get a hyperbolic equation
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Evaluate the problem characteristics}
\begin{itemize}
\item Define $u(z) = u(x+at)$ and writing $u^\prime=\frac{\text{d}u}{\text{d}z}$, etc. we obtain
\[
0 = u_{xx}+2u_{xt}+2\alpha u_{tt}=(1+2a+2a^2\alpha)u^{\prime\prime}
\]
\item The character of the PDE is determined by the roots of the polynomial
\[
1+2a+2a^2\alpha = 0
\] 
\item From this we get 
\[
a_{1,2} = \frac{-1\pm\sqrt{1-2\alpha}}{2\alpha}
\]
\item The PDE can be in one of the following three classes
\begin{enumerate}
\item If the roots are real and distinct the PDE is \textcolor{red}{hyperbolic}
\item If the roots are real and simultaneous the PDE is \textcolor{red}{parabolic}
\item If the roots are complex the PDE is \textcolor{red}{elliptic}
\end{enumerate}
\item Only the hyperbolic case with two distinct and real characteristics is useful for the solution
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
% -------------------------------------------------------- %
\subsection{Classification}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Formal Classification of second order PDEs}
\begin{itemize}
\item Given the general second-order PDE
\[
Au_{xx}+2Bu_{xy}+Cu_{yy}+Du_x+Eu_y+F = 0
\]
where $A,\, B,\, C,\ldots$ are constants
\item We can make the change of variable to use characteristics $r=ax+y$ and $s=x+by$ to obtain
\[
(A+2bB+b^2C)\left[u_{ss}+\frac{AC-B^2}{(A+Bb)^2}u_{rr}\right]+\ldots = 0
\]
\item The behaviour of the PDE depends on the sign of $AC-B^2$
\begin{itemize}
\item If $AC-B^2<0$ the PDE is \textcolor{red}{hyperbolic}
\item If $AC-B^2=0$ the PDE is \textcolor{red}{parabolic}
\item If $AC-B^2>0$ the PDE is \textcolor{red}{elliptic}
\end{itemize}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Separation of variables}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}