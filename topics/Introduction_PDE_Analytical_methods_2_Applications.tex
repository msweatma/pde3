\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Introduction Part 2: Applications in Mechanical Engineering}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Give a few examples of PDEs and describe their solution behaviour
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Distinguish between ordinary and partial differential equations
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Recap}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Attributes of PDEs}
\begin{itemize}
\item More than one independent variable
\item Partial derivatives with respect to the independent variables
\item Existence and uniqueness of the solution not guaranteed
\item Solution behaviour for many PDEs depends on the conditions at the boundaries and how the system is initialised 
\item It is often easy to find a solution of the PDE but it is much harder to find a solution fitting the initial and boundary conditions
\item Illustration of the solution is much harder compared to ODEs
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Basic PDE types}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Three basic PDE types}
In this part of the course we will consider analytical solutions for three basic types of PDE
\begin{enumerate}
\item The Laplace equation which is elliptic
\item The heat conduction or diffusion equation which is parabolic
\item The wave equation which is hyperbolic
\end{enumerate}
\bigskip
These three different types of equations
\begin{itemize}
\item Appear in many areas of engineering
\item Have different solution properties and behaviours
\item Require different boundary conditions
\item Require different numerical solution methods
\item Any second order linear PDE can be reduced to one of these types
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Mechanical engineering}
% -------------------------------------------------------- %
\subsection{Examples}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{PDEs in mechanical engineering}
Many mechanical engineering problems are governed by partial differential equations (PDEs), i.e. equations involving partial derivatives. In my research work I use advection-diffusion-reaction equations.
\begin{columns}
\column{.6\textwidth}
\includegraphics[width=6.4cm]{columnPlusPellet.pdf} 
\column{.43\textwidth}
\includegraphics[width=4.3cm]{CCSi_CO2_concentration.pdf} 
\end{columns}
\medskip
Other examples are
\begin{itemize}
\item Equations of fluid flow
\item Heat conduction
\item Wave propagation
\item Stress analysis
\item Mass, energy and momentum balances
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Wave equation}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Wave equation}
\vskip0.1cm
\centerline{\includegraphics[width=0.6\columnwidth]{VibratingString03-400x220.jpg}}
\smallskip
\begin{block}{The general form of the wave equation is}
\vskip-0.1cm
\[\frac{1}{c^2}\pdderiv{u}{t}=\pdderiv{u}{x}+\pdderiv{u}{y}+\pdderiv{u}{z}=\grad^2u.\]
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Example}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Megatsunami propagation in a fjord}

\includegraphics[width=0.45\columnwidth]{coastline-60.pdf}
\includegraphics[width=0.45\columnwidth]{coastline-120.pdf}

Landslide generated megatsunami in a fjord similar to Lituya Bay in Alaska. Where in 1958 a landslide in the Gilbert inlet caused a wave of up to 91m high.  Based on a survivors evidence the wave travelled at about 120 mph!
\end{frame}

% -------------------------------------------------------- %
\section{Heat conduction or diffusion equation}
%-------------------------------------------------------%
\subsection{Introduction}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Heat-conduction or diffusion equation}
\centerline{\includegraphics[width=0.45\columnwidth]{heated_rod.jpg}}
\begin{itemize}
\item General equation
\[
\frac1\kappa\pderiv{T}{t}=\grad^2T
\]
\item This equation is \textcolor{red}{parabolic} in nature and given enough time (and constant boundary conditions) the solution will converge to a steady state
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Example}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example: point heat source}
\begin{itemize}
\item Assume that we have a rod extending to plus and minus infinity
\item At time zero this is heated to infinity at the origin
\item The temperature in the rod can be described by
\[
T(x,t) = \frac1{\sqrt{t}}\exp\left(-\frac{x^2}{4\kappa t}\right)
\]
\item This functions satisfies the heat conduction equation
\begin{itemize}
\item Calculate the second derivatives with respect to \(x\) and the first derivative with respect to \(t\)
\item Insert these into the heat conduction equation
\item Check that the equation is fulfilled
\end{itemize}
\item This solution is approximately valid for many point heat sources in sufficiently long rods
\end{itemize}

\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Diffusion -- Reduced temperature at $t=\frac{L^2}{4\kappa}$ }
\begin{tikzpicture}
  \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$\frac{T}{\sqrt{4\kappa}}$]
  \addplot[black,domain=-2:2, samples=101] {1/sqrt(0.1^2)*exp(-x^2/0.1^2)};
  \addplot[blue, dotted, line width=1.5,domain=-2:2, samples=101] {1/sqrt(0.2^2)*exp(-x^2/0.2^2)};
  \addplot[cyan, dashed, line width=1.5,domain=-2:2, samples=101] {1/sqrt(0.3^2)*exp(-x^2/0.3^2)};
  \addplot[magenta, dashdotted, line width=1.5,domain=-2:2, samples=101] {1/sqrt(0.5^2)*exp(-x^2/0.5^2)};
  \addplot[red, loosely dotted, line width=1.5,domain=-2:2, samples=101] {1/sqrt(1^2)*exp(-x^2/1^2)};

  \legend{$L=0.1$,$L=0.2$,$L=0.3$,$L=0.5$,$L=1.0$}
\end{axis}
   \end{tikzpicture} 
\end{frame}

% -------------------------------------------------------- %
\section{Laplace Equation}
% -------------------------------------------------------- %
\subsection{General form}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace Equation}
\vskip0.1cm
\begin{itemize}
\item The general form of the Laplace equation is given by
\[\grad^2u=0\]
\item It doesn't have any parameters
\item It was first studied by Pierre-Simon Laplace (1749--1827), of whom you have already heard!
\item It is an \textcolor{red}{elliptic} equation which models the steady state of a diffusion like process.  
We can use it to describe the pressure in an incompressible flow, steady state diffusion and heat conduction, stress and strain, etc.
\item It is closely related to the Poisson equation \[\grad^2u=f(x,y)\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Example}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Inviscid, irrotational flow}
\begin{itemize}
\item Used in subsonic aerodynamics
\item Can be used to calculate lift on aerofoils
\item One solution to the Laplace equation is the stream function
\[
\psi(x,y) = Uy\left(1-\frac{a^2}{x^2+y^2}\right)
\]
\item This functions satisfies the Laplace equation
\begin{itemize}
\item Calculate the second derivatives with respect to \(x\) and \(y\)
\item Insert these into the Laplace equation
\item Check that they add up to zero
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Contours of $\psi(x,y) =Uy\left(1-\frac{a^2}{x^2+y^2}\right)$}
\includegraphics[width=0.9\columnwidth,]{StreamLines.pdf}
\begin{itemize}
\item The streamlines are contours of $\psi$
\item The streamlines show the flow of an inviscid, irrotational fluid past a cylinder placed in a uniform stream
\item In this plot, for clarity, $\psi$ has been clipped below $-0.2$ so the singularity inside the cylinder, at (0,0), is not visible
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Fun with PDEs}
% -------------------------------------------------------- %
\subsection{Fluid flow}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fluid flow equations}
\begin{block}{Incompressible Euler equations}
\begin{align*}
\nabla.\vect{u}&=0 \\
\pderiv{\vect{u}}{t}+ \underbrace{\vect{u}.\grad\vect{u}}_\text{advection}&=-\grad s
\end{align*}
where $\vect{u}$ is the velocity and $s=p/\rho_0$ is the specific pressure.
\end{block}
in 1755 Euler wrote ``\emph{If difficulties remain they will not be in the field of mechanics,
but entirely in the field of analysis}''
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fluid flow equations 2}
He was wrong and didn't know about viscosity in fluids, so in 1822 Claude Louis Marie Henri Navier and Sir George Gabriel Stokes derived the 
\begin{block}{Incompressible Navier-Stokes equations}
\vskip-0.4cm
\begin{align*}
\nabla.\vect{u}&=0 \\
\pderiv{\vect{u}}{t}+
\underbrace{\vect{u}.\grad\vect{u}}_\text{advection}
-\underbrace{\nu\grad^2\vect{u}}_\text{diffusion}
&=-\grad s+\vect{g}
\end{align*}
where $\nu=\mu/\rho_0$ is the kinematic viscosity.
\end{block}
Analytical solutions can only be found for simple cases such as laminar flow in infinitely long straight pipes:

\centering
\includegraphics[width=5.9cm]{PhD_transportEffects.png}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Applications of the Navier-Stokes equations}
Analytical solutions cannot be found for real problems (we can for laminar flow in an infinitely long straight pipe),
so we use numerical methods and massive computers to obtain...

\centering\includegraphics[width=0.7\columnwidth]{weather} \\
 
Iso-bars from the BBC Weather forecast 3-Jan-2015
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Applications of the Navier-Stokes equations}
\centering
\includegraphics[width=0.8\columnwidth]{SeaGenSim.jpg} \\
 
Flow passed the SeaGen turbine 
 
(CFD simulation by Dr Angus Creech, IES)
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fun with PDEs}
\centerline{\includegraphics[width=\columnwidth]{DeLaval.pdf}}
Solution of the Navier-Stokes equations with commercial software in Computational Fluid Dynamics 5 (MECE11004).
\end{frame}

%-------------------------------------------------------%
\subsection{Structural modelling}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fun with PDEs}
\centerline{\includegraphics[width=0.95\columnwidth]{FEM.pdf}}
Stress and strain analysis  in Finite Element Methods for Solids and Structures 4 (CIVE10022) and The Finite Element Method 5 (CIVE11029).
\end{frame}

%-------------------------------------------------------%
\subsection{Summary}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Application examples}
\begin{itemize}
\item Equations of fluid flow
\item Heat conduction
\item Wave propagation
\item Stress analysis
\item Mass, energy and momentum balances
\end{itemize}
\vskip0.2cm
\centering{
\includegraphics[width=0.5\columnwidth, height=4cm]{VibratingString03-400x220.jpg}
\hskip0.2cm
\includegraphics[width=0.38\columnwidth, height=4cm]{heated_rod.jpg}}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next analytical part}
\begin{itemize}
\item Topic: \textbf{Introduction to elliptic PDEs}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}