\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Elliptic PDE Example 6: Separated solution example 9.31}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Example 9.31}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}[label=example931]
\frametitle{Problem definition}
\begin{block}{Solve the Laplace equation $\nabla^2 u = 0$ subject to the following boundary conditions}
\begin{align}
u(x,0)&=x&0\le x\le 1 \label{eq:bottom_BC} \\
u(x,2)&=0&0\le x\le 1 \label{eq:top_BC} \\
u(0,y)&=0&0\le y\le 2 \label{eq:left_BC} \\
\pderiv{u(1,y)}{x}&=0&0\le y \le 2 \label{eq:right_BC}
\end{align}
\end{block}
\begin{itemize}
\item This problem requires \textcolor{red}{zeros} on $x=0$ and a \textcolor{blue}{zero derivative} on $x=1$
\item This suggests that trigonometric solutions in $x$ and exponential solutions in $y$ may be appropriate
\item We try
\[
u(x,y) = (A\sin\mu x+B\cos\mu x)(C\cosh \mu y+D\sinh \mu y)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Apply the left and right boundary conditions}
\begin{itemize}
\item We consider the trial solution given by
\[
u(x,y) =(A\sin\mu x+B\cos\mu x)(C\cosh \mu y+D\sinh \mu y)
\]
\item From the left boundary condition~\ref{eq:left_BC}, $u(0,y)=0$, it follows that $B=0$ 
\item This means we get
\[u(x,y)=(C^*\cosh \mu y+D^*\sinh \mu y)\sin\mu x\]
with $C^* = AC$ and $C^* = AD$
\item The right boundary condition~\ref{eq:right_BC}, $u_x(1,y)=0$ implies $\cos\mu=0$
\item So we get $\mu=(n+\frac12)\pi$ for $n=0,1,2,\dots$ 
\item Giving to following solution
{\small
\begin{align*}
u(x,y) =& \left(C^*\cosh\left(n+\frac12\right)\pi y +D^*\sinh    \left(n+\frac12\right)\pi y \right) \sin\left(n+\frac12\right)\pi x, \quad n=0,1,2,\dots
\end{align*}}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Apply the top boundary condition}
\begin{itemize}
\item To satisfy the top boundary condition~\ref{eq:top_BC}, $u(x,2)=0$ it is better to write the solution as
{\small
\begin{align*}
u(x,y) &= \sin\left(n+\frac12\right)\pi x \left[E\cosh \left(\left(n+\frac12\right)\pi \left(2-y\right)\right)
+F\sinh\left(\left(n+\frac12\right)\pi \left(2-y\right)\right)\right]
\end{align*}}
\item Here we have shifted and mirrored the part of the solution in $y$
\item  It is easy to show that this is also a solution to the Laplace equation
\item To satisfy the top boundary condition we require $E=0$
\item Giving, using superposition, the basic solution
\[
u(x,y)=\sum_{n=0}^\infty F_n\sin\left(\left(n+\frac12\right)\pi x\right)
\sinh\left(\left(n+\frac12\right)\pi \left(2-y\right)\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Transform the hyperbolic cosine and sine}
\begin{itemize}
\item I transformed the solution to make the search for the parameters easier 
\item By changing $y$ to $2-y$ we directly get that $E=0$ which removes the cosh part from the solution
\item We can do this type of transformation because, in fact, we haven't changed the equation, as shown in the following steps
{\small
\begin{align*}
C\cosh(y) + D\sinh(y) &= \frac{1}{2} \left(C (e^y + e^{-y}) + D (e^y - e^{-y})\right) = \frac{C+D}{2} e^y + \frac{C-D}{2} e^{-y} \\
 &= e^2 \frac{C+D}{2} e^{y-2} + e^{-2} \frac{C-D}{2} e^{2-y} \\
 &= \frac{E}{2} \left(e^{2-y} + e^{y-2}\right) + \frac{F}{2} \left(e^{2-y} - e^{y-2})\right) \\
 &= E\cosh(2-y) + F \sinh(2-y)
\end{align*}}
\item We get two relationships linking $C$, $D$, $E$ and $F$
\begin{align*}
E + F &= e^{-2} (C - D) \\
E - F &= e^2 (C + D)
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Apply the bottom boundary condition}
\begin{itemize}
\item Finally, the bottom boundary condition~\ref{eq:bottom_BC}, $u(x,0)=x$, gives the Fourier problem
\[
x = \sum_{n=0}^\infty F_n\sinh\left(\left(2n+1\right)\pi \right)
\sin\left(\left(n+\frac12\right)\pi x\right)
\]
\item So that 
\begin{align*}
\frac12F_n\sinh(2n+1)\pi &= \int_0^1x\sin\left(n+\frac12\right)\pi x\text{ d}x \\
&= \frac{\sin\left(\left(n+\frac12\right)\pi\right)}{\pi^2\left(n+\frac12\right)^2} = (-1)^n \frac{4}{\pi^2 (2n+1)^2}
\end{align*}
\item The complete solution is given by
\textcolor{red}{
\[
u(x,y) = \frac8{\pi^2}\sum_{n=0}^\infty (-1)^n
\frac{\sin\left(\frac{(2n+1)}{2}\pi x\right) \sinh\left(\frac{(2n+1)}{2}\pi(2-y)\right)}{(2n+1)^2\sinh\left((2n+1)\pi\right)}
\]
}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Plot the first 4 terms of the sum}
\begin{tikzpicture}[scale=0.95]
\begin{axis}[
title={$\frac8{\pi^2}\sum_{n=0}^\infty (-1)^n
\frac{\sin\left(\frac{(2n+1)}{2}\pi x\right) \sinh\left(\frac{(2n+1)}{2}\pi(2-y)\right)}{(2n+1)^2\sinh\left((2n+1)\pi\right)}$},
xlabel=$x$, ylabel=$y$, zlabel=$u$,
]
\addplot3[
surf,
domain=0:1, 
domain y=0:2,
]
{8/pi*(sin(90*x)*sinh(pi*(2-y)/2)/sinh(pi)
-sin(180*3/2*x)*sinh(3*pi/2*(2-y))/(9*sinh(3*pi))
+sin(180*5/2*x)*sinh(5*pi/2*(2-y))/(25*sinh(5*pi))
-sin(180*7/2*x)*sinh(7*pi/2*(2-y))/(49*sinh(7*pi))
)};
\end{axis}
\end{tikzpicture}
%\vskip-0.2cm
%\hyperlink{overview}{\beamergotobutton{Back}}
\end{frame}

%-------------------------------------------------------%
\section{Thank you}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}