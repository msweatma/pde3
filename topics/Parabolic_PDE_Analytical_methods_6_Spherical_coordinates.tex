\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Parabolic PDE Part 6: Spherical coordinates}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Derive the heat conduction or diffusion equation for a radially symmetric problem in spherical coordinates
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Use the Laplace transform method to solve the heat conduction or diffusion equation
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Recap}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solving the heat conduction equation with the Laplace transform}
\begin{itemize}
\item Applying the Laplace transform with respect to $t$ to the parabolic equation
\[
sU(x,s)-u(x,0)=\kappa \frac{\text{d}^2U(x,s)}{\text{d}x^2}
\]
\item Insert the boundary condition at $t=0$ to get
\[
\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s}{\kappa}U = \frac{u(x,t=0)}{\kappa} 
\]
\item Use the characteristic polynomial to find the complimentary function
\[
U_c(x,s) = A(s)\exp\left(x\sqrt{\frac{s}{\kappa}}\right) + B(s)\exp\left(-x\sqrt{\frac{s}{\kappa}}\right)
\]
\item Find a particular integral satisfying the inhomogeneity
\item Use the inverse Laplace transform to find the time domain solution from the particular solution
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Spherical coordinates}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Spherical coordinates}
\begin{itemize}
\item So far we have restricted solutions to one dimensional rods and pipes
\item We can rewrite the PDEs in spherical $(r, \varphi,\nu)$ and cylindrical polar $(r,\varphi, z)$ form and find the solution in other shaped regions
\end{itemize}
\centerline{\includegraphics[width=0.6\columnwidth]{coordinates-spherical.png}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Symmetry}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Spherical symmetry}
\begin{itemize}
\item Let us consider a radially symmetric, spherical region and solution
\item The solution is the same for all values of $\varphi\in[0, 2\pi]$ and $\nu\in[0,\pi]$
\item The solution now depends only on \textcolor{blue}{time} and \textcolor{red}{distance from the origin} 
\item So we are solving for $u(r,t)$
\item Using Pythagoras (the radius is the Euclidean distance from the origin) we know
\[
r^2=x^2+y^2+z^2
\]
\item We can use this relationship to calculate the partial derivatives of $r$
\[
2r\pderiv{r}{x}=2x
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Spherical coordinates}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Transform from Cartesian to spherical coordinates}
\begin{itemize}
\item We want to find the Laplacian with no variation due to $\theta$ and $\phi$
\[
\nabla^2 f=\pdderiv{f}{x}+\pdderiv{f}{y}+\pdderiv{f}{z},\text{ where }f=f(r,t)
\]
\item Use the chain rule with $x = r \cos(\theta)$ and the result from the last slide
\[
\pderiv{f}{x}=\pderiv{f}{r}\pderiv{r}{x}=\frac{x}{r}\pderiv{f}{r}
\]
\item Apply the chain rule again for the 2nd derivative
\begin{align*}
\pdderiv{f}{x}&=\frac{\partial}{\partial x}\left(\frac{x}{r}\pderiv{f}{r}\right) \\
&=\frac{1}{r}\pderiv{f}{r}-\frac{x}{r^3}\left(x\pderiv{f}{r}\right)+\frac{x^2}{r^2}\pdderiv{f}{r}
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Radially symmetric heat conduction or diffusion equation}
\begin{itemize}
\item A similar process gives the $y$ and $z$ derivatives so the Laplacian becomes
\begin{align*}
\nabla^2f&=\frac{3}{r}\pderiv{f}{r}-\frac{x^2+y^2+z^2}{r^3}\pderiv{f}{r}+\frac{x^2+y^2+z^2}{r^2}\pdderiv{f}{r}\\
&=\frac{2}{r}\pderiv{f}{r}+\pdderiv{f}{r}=\frac{1}{r^2}\frac{\partial}{\partial r}\left(r^2\pderiv{f}{r}\right)
\end{align*}
\item In the previous step we have used $r^2 = x^2+y^2+z^2$
\item The time derivative stays the same as it is not affected by the change of coordinate system
\item The radially symmetric heat conduction/diffusion equation is
\[
\frac{1}{r^2}\frac{\partial}{\partial r}\left(r^2\pderiv{T}{r}\right) = \frac{1}{\kappa}\pderiv{T}{t}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Scaled temperature}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Scale the temperature with the radius}
\begin{itemize}
\item Scale the temperature with the radius 
\[T = \frac{\theta(r,t)}{r}\]
\item Use the product rule to find the space derivative of the scaled temperature
\[
\pderiv{T}{r} = \pderiv{\frac{\theta}{r}}{r} = \frac{1}{r}\pderiv{\theta}{r} - \frac{\theta}{r^2}
\]
\item Insert this into the heat conduction equation in spherical coordinates to get
\[
\frac{1}{r^2}\frac{\partial}{\partial r}\left(-\theta+r\pderiv{\theta}{r}\right) = \frac{1}{r\kappa}\pderiv{\theta}{t}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Equation for the scaled temperature}
\begin{itemize}
\item Differentiating once more gives,
\[
\frac{1}{r}\left(-\pderiv{\theta}{r}+\pderiv{\theta}{r}+ r\pdderiv{\theta}{r}\right) = \frac{1}{\kappa}\pderiv{\theta}{t}
\]
\item So the equation for $\theta(r,t)$ is
\[
\pdderiv{\theta}{r}=\frac{1}{\kappa}\pderiv{\theta}{t}
\]
\item We can solve this using the methods previously described 
\item Care must be taken when \textcolor{red}{$r=0$}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
% -------------------------------------------------------- %
\subsection{Spherically symmetric heat conduction equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Scaled temperature for spherically symmetric systems }
\begin{itemize}
\item The radially symmetric heat conduction or diffusion equation is
\[
\frac{1}{\kappa}\pderiv{T}{t} = \frac{1}{r^2}\frac{\partial}{\partial r}\left(r^2\pderiv{T}{r}\right)
\]
\item Scale the temperature with the radius
\[
T(r,t) = \frac{\theta(r,t)}{r}
\]
\item This gives the equation for $\theta(r,t)$ as
\[
\frac{1}{\kappa}\pderiv{\theta}{t} = \pdderiv{\theta}{r}
\]
\item We can solve this using the methods previously described but care must be taken when $r=0$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Problem definition}
Solve the radially symmetric heat conduction equation
\[\frac{1}{r^2}\frac{\partial}{\partial r}\left(r^2\pderiv{T}{r}\right)=\frac{1}{\kappa}\pderiv{T}{t}\]
\begin{itemize}
\item In the region $r\ge a$ 
\item Subject to the boundary conditions
\begin{align*}
T(a,t) &= T_0 & \forall t\ge0 \\
\lim_{r\rightarrow\infty} T(r,t) &= 0 & \forall t\ge0 \\
T(r,0) &= 0 & \forall r>a
\end{align*}
\item $T_0$ is a constant temperature
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\begin{frame}{ConcepTest Parabolic-AP6: Scaled boundary conditions}
\vskip-0.cm
\begin{block}{Find the boundary conditions for $\theta(r,t) = r T(r,t)$?}
\begin{itemize}
\item Consider $\theta(r,t) = r T(r,t)$ and solve the transformed equation
\item We need to adjust the boundary conditions for $\theta$
\begin{align*}
T(a,t) &= T_0 \implies & \theta(a,t) &= ? & \forall t\ge0 \\
\lim_{r\rightarrow\infty} T(r,t) &= 0 \implies & \lim_{r\rightarrow\infty} \theta(r,t) &<\text{const} & \forall t\ge0 \\
T(r,0) &= 0 \implies & \theta(r,0) &= 0 & \forall r>a
\end{align*}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.2cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\vskip0.cm
\begin{enumerate}
\item \(\theta(a,t) = \frac{T_0}{a}, \quad \forall t\ge0\)
\item \(\theta(a,t) = T_0, \quad \forall t\ge0\)
\item \(\theta(a,t) = a^2T_0, \quad \forall t\ge0\)
\item \(\theta(a,t) = aT_0, \quad \forall t\ge0\)
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\begin{block}{Solution}
\vskip0.2cm
\begin{enumerate}
\item Solution 4 is correct: 
\begin{align*}
\theta(r,t) &= r T(r,t) & \\
\theta(a,t) &= a T(a,t) = a T_0 & \forall t\ge0
\end{align*}
\end{enumerate}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\subsection{Heat conduction or diffusion equation}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Heat conduction or diffusion equation summary}
\begin{itemize}
\item The heat conduction or diffusion equation in Cartesian coordinates is given by
\begin{align*}
\frac{1}{\kappa}\pderiv{u}{t} &= \pdderiv{u}{x} + \pdderiv{u}{y} 
\end{align*}
\item The heat conduction or diffusion equation is well-posed if the boundary is open and we have Dirichlet or Neumann conditions on the closed boundaries 
\item The method of separation of variables is the main analytical solution method
\begin{enumerate}
\item Specify the permissible values of $\mu$
\item Superpose solutions to fit the remaining boundary condition
\end{enumerate}
\item The Laplace transform method is a further solution method
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next topic}
\begin{itemize}
\item Topic: \textbf{Hyperbolic partial differential equations}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}