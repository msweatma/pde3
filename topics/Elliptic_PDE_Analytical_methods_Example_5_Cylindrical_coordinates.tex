\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Elliptic PDE Example 5: Cylindrical coordinates}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Laplace equation in cylindrical coordinates}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Problem definition}
Consider the Laplace equation in cylindrical coordinates
\begin{align*}
\pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
with the boundary conditions
\begin{align*}
u(a,\theta) &= 1, \quad 0\le\theta<\pi \\
u(a,\theta) &= 0, \quad \pi\le\theta<2\pi
\end{align*}
\begin{itemize}
\item The domain is a circle with radius $a$
\item The temperature at the top boundary is set to 1
\item The temperature at the bottom boundary is set to 0
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Separated solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Separated solution in 2D cylindrical coordinates}
\begin{itemize}
\item Consider the Laplace equation in cylindrical coordinates
\begin{align*}
\pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
\item Search for separated solutions
\begin{align*}
u(r,\theta) &= R(r) \Theta(\theta) \\
\frac{d^2 R}{dr^2} \Theta + \frac{1}{r} \frac{dR}{dr}\Theta + \frac{R}{r^2}\frac{d^2\Theta}{d\theta^2} &= 0 \\
\intertext{We separate the terms depending on $r$ and $\theta$ to get}
-\frac{\Theta''}{\Theta} &= \frac{R'' + \frac{R'}{r}}{\frac{R}{r^2}} = \lambda
\end{align*}
\end{itemize}
\vskip-0.3cm
\begin{itemize}
\item Consider three cases similar to the separated solutions in Cartesian coordinates
\item Gets more complicated if we have variation also in the $z$ direction
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Case $\lambda=0$}
\begin{block}{Basic solution for $\lambda=0$}
\begin{align*}
\Theta(\theta) &= A\theta + B \\
R(r) &= C\ln(r) + D \\
u_1(r,\theta) &= (A\theta + B) (C\ln(r) + D)
\end{align*}
\end{block}
\vskip0.0cm
\begin{block}{Use boundary conditions to find the parameters}
\begin{itemize}
\pause
\item The solution has to be periodic in $2\pi$ $\Rightarrow A=0$
\item The solution needs to be finite for $r\to0\Rightarrow C=0$
\item From this it follows that the solution is a constant
\[
u_1(r,\theta) = BD = k = \text{constant}
\]
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Case $\lambda<0$}
\begin{block}{Basic solution for $\lambda = -\mu^2<0$}
\begin{align*}
\Theta(\theta) &= A\cosh(\mu\theta) + B\sinh(\mu\theta) \\
R(r) &= C r^{-\mu} + D r^{\mu} \\
u_2(r,\theta) &= \left(A\cosh(\mu\theta) + B\sinh(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
\end{block}
\vskip0.0cm
\begin{block}{Use boundary conditions to find the parameters}
\begin{itemize}
\pause
\item The solution has to be periodic in $\theta $ with period $2\pi$ 
\item From this it follows that $A=B=0$ and thus
\[
u_2(r,\theta) = 0
\]
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Case $\lambda>0$}
\begin{block}{Basic solution for $\lambda = \mu^2 > 0$}
\begin{align*}
\Theta(\theta) &= A\cos(\mu\theta) + B\sin(\mu\theta) \\
R(r) &= C r^{-\mu} + D r^{\mu} \\
u_3(r,\theta) &= \left(A\cos(\mu\theta) + B\sin(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
\end{block}
\vskip0.0cm
\begin{block}{Use boundary conditions to find the parameters}
\begin{itemize}
\pause
\item This is periodic in $\theta $ with period $2\pi$ for $\mu=n$ with $n\in\mathbb{N}$
\item To remain finite as $r\to0$ we need $C=0$
\item With $A_n=A D$ and $B_n=B D$ we get
\[
u_3(r,\theta) = r^n \left(A_n\cos(\mu\theta) + B_n\sin(\mu\theta)\right), \quad n=1,2, \dots
\]
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Combined solution}
\begin{itemize}
\item Thus the solution is
\[
u(r,\theta) = k + \sum_{n=1}^\infty r^{n}\left(A_n\cos(n\theta) + B_n\sin(n\theta)\right)
\]
\item Apply the boundary condition
\[
u(a,\theta) = k + \sum_{n=1}^\infty a^{n}\left(A_n\cos(n\theta) + B_n\sin(n\theta)\right) = \begin{cases}
1, \quad & 0\le\theta<\pi \\
0, \quad & \pi\le\theta<2\pi
\end{cases}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fourier series coefficients}
\begin{itemize}
\item Solve for the coefficients with the Fourier series formulas
\begin{align*}
a_0 &= \frac{2}{2\pi}\int_0^{2\pi} u(a, \theta)d\theta = \frac{1}{\pi} \int_{0}^{\pi} 1 d\theta = 1 \Rightarrow k = \frac{1}{2} a_0 = \frac{1}{2} \\
A_n &= \frac{1}{a^n \pi} \int_{0}^{\pi} \cos\left(n\theta\right) d\theta = 0\\
B_n &= \frac{1}{a^n \pi} \int_{0}^{\pi} \sin\left(n\theta\right) d\theta = \begin{cases}
\frac{2}{a^n \pi n}, \quad & n \text{ odd} \\
0, \quad & n \text{ even}
\end{cases}
\end{align*}
\pause
\item Thus the solution is
\[
u(r,\theta) = \frac{1}{2} + \sum_{k=1}^\infty \frac{2}{(2k-1)\pi a^{2k-1}} r^{2k-1}\sin((2k-1)\theta)
\]
where we used $n=2k-1$ to cover only the odd coefficients
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Plot}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Surface and contour plot of the solution}
\centerline{\includegraphics[width=0.5\columnwidth]{Elliptic_cylindrical_example_surf.png}
\includegraphics[width=0.5\columnwidth]{Elliptic_cylindrical_example_contour.png}}
\begin{itemize}
\item Both plots show the first 20 terms of the Fourier series solution
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Thank you}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}