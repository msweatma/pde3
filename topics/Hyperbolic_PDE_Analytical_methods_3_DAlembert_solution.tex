\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Hyperbolic PDE -- Part 3: D'Alembert solution}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Derive and use the d'Alembert solution for the wave equation
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Show that a given expression is or isn't a solution to the wave equation
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Recap}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Wave equation examples}
\begin{itemize}
\item The fundamental mode of a vibrating string is given by
\[u(x,t)=u_0\sin\frac{\pi x}{L}\cos\frac{\pi c t}{L}\]
\item A linear advection solution is given by
\[
u(x,t) = a\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right]
\]
\end{itemize}
\begin{tikzpicture}
 \begin{groupplot}[group style={group size=1 by 2},
             height=0.4\textheight,width=0.95\columnwidth,
             axis lines=left,xlabel=$x$]
  \nextgroupplot[ylabel=$t$]
  \addplot[blue, domain=0:8] {x};
  \addplot[magenta,loosely dotted, line width=1.5, domain=2:10] {x-2};
  \addplot[magenta,loosely dotted, line width=1.5, domain=-2:6] {x+2};
  \addplot[black, domain=-2:10]{6};
  \addplot[magenta,loosely dotted, line width=1.5] coordinates {(4,0) (4,6)};
  \addplot[magenta,loosely dotted, line width=1.5] coordinates {(8,0) (8,6)};
  \addplot[blue] coordinates {(6,0) (6,6)};
 \end{groupplot}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\section{Characteristics}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Characteristics}
\begin{itemize}
\item The linear advection problem shows that \textcolor{red}{information} propagates at the speed $c$
\item Clearly $c=\pm\sqrt{c^2}$ so we can have left or right travelling waves!
\end{itemize}
\vskip0.2cm
\centerline{\resizebox{0.8\columnwidth}{!}{
\begin{tikzpicture}
  \begin{axis}[legend style={draw=none},
     legend pos=outer north east,
     xlabel=$x/c$,ylabel=$t$]
  \addplot[magenta, dashed, line width=1.5, domain=0:5] {x};
  \addplot[cyan, line width=1.5, domain=-5:0] {-x};
  \legend{$t=0+x/c$,$t=0-x/c$}
\end{axis}
\end{tikzpicture}
}}
\end{frame}

% -------------------------------------------------------- %
\subsection{General solution}
% -------------------------------------------------------- %
\begin{frame}[label=general_solution]
\frametitle{Convert to characteristics based coordinates}
\begin{itemize}
\item We derive a classical solution of the one-dimensional wave equation by changing the coordinate system to one based on the \textcolor{red}{characteristics}, namely,
\begin{align*}
r&=x+ct \text{, }\quad s=x-ct
\end{align*}
\item We apply the multivariable chain rule to get
\begin{align*}
u_{xx} &= u_{rr}+2u_{rs}+u_{ss} \\
u_{tt} &= c^2\left(u_{rr}-2u_{rs}+u_{ss}\right)
\end{align*}
\item So the wave equation becomes
\[
4c^2u_{rs} = 0 \Rightarrow u_{rs} = 0
\]
\end{itemize}
\hyperlink{general_solution_chain_rule}{\beamergotobutton{Chain rule details}}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{General solution of the wave equation}
\begin{itemize}
\item Integrating, once, with respect to $s$ gives
\[
u_r = \int u_{rs} ds = \int 0 ds = \theta(r)
\]
where $\theta(r)$ is an arbitrary function of $r$
\item Now, integrating with respect to $r$ we obtain
\[
u = f(r)+g(s)
\]
\item Substituting for $r$ and $s$ gives the general solution of the wave equation,
\[
u = f(x+ct)+g(x-ct)
\]
where $f$ and $g$ are arbitrary functions
\item The functions $f$ and $g$ depend only on one parameter
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Example 9.11}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.11}
\begin{block}{Solve the 1D wave equation}
\[
\frac{1}{c^2}\pdderiv{u}{t} = \pdderiv{u}{x}
\]
\begin{itemize}
\item Subject to the initial conditions
\begin{align*}
\pderiv{u(x,0)}{t} &= 0 \qquad  \forall x, \\
u(x,0) = F(x) &=
\begin{cases}
1-x & 0\le x \le 1 \\
1+x & -1\le x \le 0 \\
0 & \text{otherwise}
\end{cases}
\end{align*}
\end{itemize}
\end{block}
\begin{itemize}
\item The PDE represents, for example, an infinite string
\item Initially the string is displaced according to $F(x)$ but is at rest
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Apply the derivative initial condition}
\begin{itemize}
\item We know the general solution is
\[
u(x,t) = f(x+ct)+g(x-ct)
\]
\item Take the derivative with respect to $t$
\[
\pderiv{u(x,t)}{t} = c f'(x+ct) - c g'(x-ct)
\]
\item Set $t=0$ and apply the derivative initial condition
\[
0 = \pderiv{u(x,0)}{t} = cf^\prime(x)-cg^\prime(x), \quad \forall x
\]
\item Integrate to get
\[
f(x)-g(x) = K,
\]
where $K$ is an arbitrary constant
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Apply the Dirichlet initial condition}
\begin{itemize}
\item Rearrange for $g(x)$ to get
\[
g(x) = f(x) - K
\]
\item Substitute $g(x)$ in the general solution to get
\[
u(x,t) = f(x+ct)+f(x-ct) - K
\]
\item Similarly, applying the Dirichlet condition gives
\[
F(x) = u(x,0) = 2f(x) - K
\]
\item Clearly, we get
\begin{align*}
u(x,t) &= \frac{1}{2} F(x+ct) + \frac{1}{2}K + \frac{1}{2} F(x-ct) + \frac{1}{2}K - K \\
&= \frac12F(x+ct) + \frac12F(x-ct)
\end{align*}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Hyperbolic-AP3a: Solution shape}
\vskip-0.cm
\begin{block}{Which plot shows the correct solution behaviour?}
\vskip-0.1cm
\begin{itemize}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.6cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\includegraphics[width=0.33\columnwidth]{example_911_decreasing}
\includegraphics[width=0.33\columnwidth]{example_911_correct}\\
\includegraphics[width=0.33\columnwidth]{example_911_f}
\includegraphics[width=0.33\columnwidth]{example_911_f_plus_g}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\begin{block}{Solution}
\includegraphics[width=0.45\columnwidth]{example_911_correct}
\begin{itemize}
\item This solution has \textbf{two} travelling waves, one propagating to the left and one to the right
\item The waves propagate without loss of amplitude or distortion and shape discontinuities are not smoothed out
\end{itemize}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}<0>[noframenumbering]
\frametitle{Solution as an $x-t$ diagram}
\begin{tikzpicture}
  \begin{axis}[xlabel=$x$,ylabel=$t$]
  \addplot[cyan, dotted, line width=1.5, domain=0:5] {x};
  \addplot[cyan, dotted, line width=1.5, domain=-5:0] {-x};
  \addplot[blue, line width=1.5, domain=-1:4]{x+1};
  \addplot[red, dashed, line width=1.5, domain=1:6]{x-1};
  \addplot[blue, line width=1.5, domain=-6:-1]{-x-1};
  \addplot[red, dashed, line width=1.5, domain=-4:1]{-x+1};
\end{axis}
\end{tikzpicture}
\begin{itemize}
\item This plot is for $c=1$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{D'Alembert solution}
% -------------------------------------------------------- %
\subsection{Derivation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{d'Alembert solution}
Consider the solution of the wave equation
\[
u(x,t) = f(x+ct) + g(x-ct)
\]
\vskip-0.2cm
\begin{enumerate}
\item with an initial velocity,
\[
\pderiv{u(x,0)}{t} = G(x), \quad\forall x \text{ and}
\]
\item and an initial displacement,
\[
u(x,0) = F(x),\quad \forall x
\]
\end{enumerate}
\textbf{Solution:}
\begin{itemize}
\item Calculate the derivative of $u(x,t)$ and set $t=0$ so that condition 1 gives
\(c\left[f^\prime(x)-g^\prime(x)\right] = G(x)\)
\item Integrate this to get \(c\left[f(x)-g(x)\right] =\int_0^x G(z)\,\text{d}z+Kc\)
\item Replace $u(x,t)$ in condition 2 to get \(f(x) + g(x) = F(x)\)
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{d'Alembert solution derivation}
\begin{itemize}
\item This gives the following linear equation system
\begin{align*}
c\left[f(x) - g(x)\right] &= \int_0^x G(z)\,\text{d}z+Kc \\
f(x) + g(x) &= F(x)
\end{align*}
\item This can be solved for $f(x)$ and $g(x)$ to get
\begin{align*}
f(x) &= \frac12F(x)+\frac{1}{2c}\int_0^xG(z)\,\text{d}z+\frac12K \\
g(x) &= \frac12F(x)-\frac{1}{2c}\int_0^xG(z)\,\text{d}z-\frac12K
\end{align*}
\vskip-0.8cm
\item Combining these gives the
\begin{block}{d'Alembert solution}
\[
u(x,t) = \frac12\left[F(x+ct)+F(x-ct)\right] + \frac1{2c}\int_{x-ct}^{x+ct} G(z)\,\text{d}z
\]
\end{block}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Hyperbolic-AP3b: D'Alembert solution}
\vskip-0.cm
\begin{block}{Pick all true statements}
\centering{
%\includegraphics[width=5.cm]{orthogonal_sine_cosine_3}
}
\vskip-0.1cm
\begin{itemize}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.2cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item The d'Alembert solution can be used for any $x$ interval
\item The d'Alembert solution can only be used for $-\infty<x<\infty$
\item The d'Alembert solution can be used for $0\le x<\infty$
\item The wave equation needs only initial conditions
\item The wave equation needs initial and boundary conditions
\item The wave equation needs only initial conditions in unbounded spatial domains
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\begin{enumerate}
\item[2.] The d'Alembert solution can only be used for $-\infty<x<\infty$
\item[6.] The wave equation needs only initial conditions in unbounded spatial domains
\end{enumerate}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
% -------------------------------------------------------- %
\subsection{D'Alembert solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{D'Alembert solution}
The solution of the wave equation
\begin{itemize}
\item For an initial velocity, $\pderiv{u(x,0)}{t}=G(x),\quad \forall x,$ 
\item And an initial displacement, $u(x,0)=F(x),\quad \forall x,$ 
\item Is given by
\[
u(x,t) =\frac12\left[F(x+ct)+F(x-ct)\right]+\frac1{2c}\int_{x-ct}^{x+ct} G(z)\,\text{d}z
\]
\end{itemize}
\centering
\includegraphics[width=7cm]{Ex9-11}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Method of characteristics}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin
% -------------------------------------------------------- %
\section{Characteristics}
% -------------------------------------------------------- %
\subsection{Chain rule}
% -------------------------------------------------------- %
\begin{frame}[label=general_solution_chain_rule]
\frametitle[squezze]{General solution chain rule}
\begin{itemize}
\item Remember the new coordinates $r=x+ct \text{, }\quad s=x-ct$
\item Applying the multivariable chain rule to the first derivatives
\begin{align*}
\pderiv{u}{x} &= \pderiv{u}{r}\pderiv{r}{x} + \pderiv{u}{s}\pderiv{s}{x} = \pderiv{u}{r} + \pderiv{u}{s} \\
\pderiv{u}{t} &= \pderiv{u}{r}\pderiv{r}{t} + \pderiv{u}{s}\pderiv{s}{t} = c\pderiv{u}{r} - c\pderiv{u}{s}
\end{align*}
\item Apply the chain rule for the second derivatives
{\small
\begin{align*}
\pdderiv{u}{x} &= \pderiv{}{x}\left(\pderiv{u}{r} + \pderiv{u}{s}\right) = \pdderiv{u}{r} \pderiv{r}{x} + \frac{\partial^2 u}{\partial r \partial s}\pderiv{s}{x} + \frac{\partial^2 u}{\partial s \partial r}\pderiv{r}{x} + \pdderiv{u}{s}\pderiv{s}{x} \\
&= \pdderiv{u}{r} + 2\frac{\partial^2 u}{\partial r \partial s} + \pdderiv{u}{s} = u_{rr}+2u_{rs}+u_{ss} \\
\pdderiv{u}{t} &= c\pderiv{}{t}\left(\pderiv{u}{r} - \pderiv{u}{s}\right)
 = c\left(\pdderiv{u}{r} \pderiv{r}{t} + \frac{\partial^2 u}{\partial r \partial s}\pderiv{s}{t} - \frac{\partial^2 u}{\partial s \partial r}\pderiv{r}{t} - \pdderiv{u}{s}\pderiv{s}{t}\right) \\
 &= c^2\left(\pdderiv{u}{r} - 2\frac{\partial^2 u}{\partial r \partial s} + \pdderiv{u}{s}\right) = c^2\left(u_{rr} - 2u_{rs} + u_{ss} \right)
\end{align*}}
\end{itemize}
\vskip-0.2cm
\hyperlink{general_solution}{\beamergotobutton{Back}}
\end{frame}

\appendixend

\end{document}