\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Parabolic PDE Example 4: Laplace transform method example 9.23}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Example 9.23}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Diffusion equation}
\begin{block}{Consider the one dimensional diffusion equation given by}
\[ 
\pderiv{u}{t} = \kappa \pdderiv{u}{x}, \qquad \forall x,\, t\ge 0
\]
\begin{itemize}
\item With the following initial and boundary conditions
\begin{itemize}
\item The concentration $u(x,t)$ needs to remain bounded
\item The boundary and initial conditions are given by
\begin{align*}
u(x,0) &= 0, \quad & \forall x \\
u(a,t) &= T\delta(t), & \forall t
\end{align*}
\end{itemize}
\item Here $\delta(t)$ is the Dirac delta function which can be thought of as
\[
\delta(t) = \begin{cases}
\infty, & t=0 \\
0, & \text{else}
\end{cases}
\]
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution by Laplace transforms}
\begin{itemize}
\item Applying the Laplace transform to the diffusion equation we get
\pause
\[
sU(x,s)-u(x,0)=\kappa \frac{\text{d}^2U(x,s)}{\text{d}x^2}
\]
\item Insert the initial condition to get
\[
\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s}{\kappa}U=0
\]
\item This ODE can be solved by finding the characteristic polynomial 
\[
m^2 - \frac{s}{\kappa} = 0
\]
and remembering the ODE solution methods
\[
U(x,s) = A(s)\exp\left(x\sqrt{\frac{s}{\kappa}}\right) + B(s)\exp\left(-x\sqrt{\frac{s}{\kappa}}\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Use the boundedness of the solution}
\begin{itemize}
\item The Laplace domain solution needs to remain bounded because the time domain solution remains bounded
\item To fulfil this we need to split the Laplace domain solution into two parts: one for $x<a$ and one for $x>a$
\item Since these two solutions need to be bounded we need to choose
\begin{align*}
U(x,s) &= B(s)\exp\left(-x\sqrt{\frac{s}{\kappa}}\right) &\forall x\ge a \\
U(x,s) &= A(s)\exp\left(x\sqrt{\frac{s}{\kappa}}\right) & \forall x<a
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Apply the boundary condition at $x=a$}
\begin{itemize}
\item Using the second boundary condition
\pause
\[
U(a,s) = B(s)\exp\left(-a\sqrt{\frac{s}{\kappa}}\right) \overset{!}{=} \mathcal{L}\left\{T\delta(t)\right\} = T
\]
we can find $B(s)$
\[
B(s) = T\exp\left(a\sqrt{\frac{s}{\kappa}}\right)
\]
\item And similar to find $A(s)$ we use
\[
U(a,s) = A(s)\exp\left(a\sqrt{\frac{s}{\kappa}}\right) \overset{!}{=} \mathcal{L}\left\{T\delta(t)\right\} = T
\]
so
\[
A(s) = T\exp\left(-a\sqrt{\frac{s}{\kappa}}\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Complete Laplace domain solution}
\begin{itemize}
\item Using the functions of $A(s)$ and $B(s)$ we get
\begin{align*}
U(x,s) &= \begin{cases}
    T\exp\left(-(x-a)\sqrt{\frac{s}{\kappa}}\right), &\forall x\ge a \\
    T\exp\left(-(a-x)\sqrt{\frac{s}{\kappa}}\right), &\forall x<a
\end{cases}
\end{align*}
\item This can be inverted by looking at extensive tables of Laplace transforms
\item From these we find the following inverse Laplace transform 
\[
\mathcal{L}^{-1}\left\{e^{-b\sqrt{s}}\right\} = \frac{b}{2\sqrt{\pi}}t^{-\frac32}e^{-\frac{b^2}{4t}}
\]
\item The parameter is given by $b= \frac{x-a}{\sqrt{\kappa}}$ or $b= \frac{a-x}{\sqrt{\kappa}}$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Invert the Laplace domain solution}
\begin{itemize}
\item With $b= \frac{x-a}{\sqrt{\kappa}}$ we get
\[
u(x,t) = \frac{T\left(x-a\right)}{2\sqrt{\pi\kappa}}t^{-\frac32}e^{-\frac{(x-a)^2}{4\kappa t}},\, t>0, \forall x\ge a
\]
\item And for $b= \frac{a-x}{\sqrt{\kappa}}$ we get
\[
u(x,t) = \frac{T\left(a-x\right)}{2\sqrt{\pi\kappa}}t^{-\frac32}e^{-\frac{(x-a)^2}{4\kappa t}},\, t>0, \forall x< a
\]
\item So that the full solution is
\[
u(x,t) = \frac{T\left|x-a\right|}{2\sqrt{\pi\kappa}}t^{-\frac32}e^{-\frac{(x-a)^2}{4\kappa t}},\, t>0
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Plot the solution for $\kappa=1$ and $a=1$}
\begin{tikzpicture}
    \begin{axis}[legend style={draw=none}, 
    xlabel=$x$,ylabel=$u/T$]
% t=0.5
    \addplot[black,domain=-3:10, samples=201] {abs(x-1)/(2*sqrt(3.14159))*0.5^(-1.5)*exp(-(x-1)^2/(4*0.5)};
% t=1.0
    \addplot[blue, dotted, line width=1.5, domain=-3:10, samples=201] {abs(x-1)/(2*sqrt(3.14159))*1.0^(-1.5)*exp(-(x-1)^2/(4*1.0)};
% t=2.0
    \addplot[cyan, dashed, line width=1.5, domain=-3:10, samples=201] {abs(x-1)/(2*sqrt(3.14159))*2.0^(-1.5)*exp(-(x-1)^2/(4*2.0)};
% t=5.0
    \addplot[red, dashdotted, line width=1.5, domain=-3:10, samples=201] {abs(x-1)/(2*sqrt(3.14159))*5.0^(-1.5)*exp(-(x-1)^2/(4*5.0)};

    \legend{$t=0.5$,$t=1.0$,$t=2.0$,$t=5.0$}
\end{axis}
    \end{tikzpicture}
\end{frame}
    

%-------------------------------------------------------%
\section{Thank you}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}