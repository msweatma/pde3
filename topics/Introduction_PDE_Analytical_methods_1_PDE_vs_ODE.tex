\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Introduction Part 1: PDE vs ODE}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Distinguish between ordinary and partial differential equations
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Know about and solve ordinary differential equations
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\section{Recap}
% -------------------------------------------------------- %
\subsection{ODE}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Ordinary differential equations}
\centerline{\includegraphics[angle=180,width=0.56\columnwidth]{HydraulicDamper.jpg}} \medskip
 For this hydraulic damper, the resistance is proportional to the velocity of compression, $v$.  If a motion of a mass, $m$, is being arrested by the damper, we can write $$\frac{\mathrm{d}v(t)}{\mathrm{d}t}=-\lambda v(t),\,v(0)=U,$$ where  $\lambda=k/m$, $v(t)$ is the speed of the mass at time $t$, $U$ is its initial velocity and $k$ is the damping coefficient. \\ [6pt]
\begin{itemize}
\item What is the order of the ODE?
\item What are the independent and dependent variables?
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Analytical solution}
\begin{itemize}
\item Let us calculate the analytical solution for the hydraulic damper
\item This was covered in Engineering Mathematics 2A
\item Please revise the material from EM2A if you are not familiar with this
\end{itemize}
\begin{eqnarray*}
\mbox{Find the general solution of} && \frac{\mathrm{d}v}{\mathrm{d}t}=-\lambda v\\
\mbox{Separating variables} && \int\frac{\mathrm{d}v}{v}=\int-\lambda\,\mathrm{d}t,\\
\mbox{Integrating} && \ln{v}+D=-\lambda t,\\
\mbox{Take antilogs} && ve^D=e^{-\lambda t},\\
\mbox{So we get the general solution} && v=Ce^{-\lambda t}
\end{eqnarray*}
With the initial condition $v(0)=U$, we get the particular solution
\[
v=Ue^{-\lambda t}
\]
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Plot of the solution}
\begin{tikzpicture}
 \begin{axis}[ 
    axis lines=left,
    scaled ticks=false,
    xlabel={$t$ [s]},
    ylabel={$v/U$ [--]}
 ] 
 \addplot[green, dashed, domain=0:5, samples=201, line width=1.5] {e^(-0.5*x)};
 \addplot[red, domain=0:5, samples=201, line width=1.5] {e^(-x)}; 
 \addplot[blue, dotted, domain=0:5, samples=201, line width=1.5] {e^(-2*x)};
 \legend{$\lambda=0.5$,$\lambda=1.0$, $\lambda=2.0$}
 \end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\section{PDE introduction}
% -------------------------------------------------------- %
\subsection{Wave equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Vibrating string}
\centerline{\includegraphics[width=0.85\columnwidth]{VibratingString03-400x220.jpg}}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{The wave equation}
\begin{block}{One dimensional wave equation}
\[\frac{1}{c^2}\pdderiv{u}{t}=\pdderiv{u}{x}\]
where $c^2=T/\rho$ where $T$ is the tension and $\rho$ is the mass per unit length.
\end{block}
\begin{itemize}
\item It can model sound waves, shock waves, electromagnetic waves, water waves, a plucked string, etc.
\item The one dimensional form was discovered by the French scientist Jean-Baptiste le Rond d'Alembert (1717--1783)
\item The equation is said to be \textcolor{red}{hyperbolic} in character and has the nasty property that discontinuities in $u$, or its derivative, $\frac{\partial u}{\partial x}$, will propagate in time
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Linear advection}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Linear advection}
\begin{itemize}
\item We can show that the following equation
\[u(x,t)=a\exp\left[-\left(\frac{x}{h}-\frac{c t}{h}\right)^2\right]\]
satisfies the wave equation for any parameters $a$ and $h$
\item $u(x,t)$ describes the transport (or advection) of an initial disturbance with a flow
\item Show that $u(x,t)$ fulfils the wave equation
\begin{itemize}
\item Differentiating $u(x,t)$ twice with respect to $x$ and twice with respect to $t$
{\small
\begin{align*}
u_{xx}&=\frac{-2a}{h^2}\exp\left[-\left(\frac{x}{h}-\frac{c t}{h}\right)^2\right]
+\frac{4a(x-ct)^2}{h^4}\exp\left[-\left(\frac{x}{h}-\frac{c t}{h}\right)^2\right] \\
u_{tt}&=\frac{-2a c^2}{h^2}\exp\left[-\left(\frac{x}{h}-\frac{c t}{h}\right)^2\right]
+\frac{4a(x-ct)^2c^2}{h^4}\exp\left[-\left(\frac{x}{h}-\frac{c t}{h}\right)^2\right]
\end{align*}
}
\item Insert $u_{xx}$ and $u_{tt}$ into the wave equation to show that it is fulfilled
\item Check that I correctly calculated the derivatives
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Plot of the linear advection solution}
\begin{tikzpicture}[scale=1]
 \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black,domain=-2:10, samples=101] {exp(-x^2)};
  \addplot[blue, dotted, line width=1.5,domain=-2:10, samples=101] {exp(-(x-2)^2)};
  \addplot[cyan,dashed, line width=1.5,domain=-2:10, samples=101] {exp(-(x-4)^2)};
  \addplot[magenta,loosely dotted, line width=1.5,domain=-2:10, samples=101] {exp(-(x-8)^2)};

  \legend{$t=0$,$t=\frac{2h}{c}$,$t=\frac{4h}{c}$,$t=\frac{8h}{c}$}
 \end{axis}
\end{tikzpicture}
\begin{itemize}
\item Shape and magnitude of the disturbance stay constant
\item The wave equation doesn't contain dispersive terms
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Linear Advection with a discontinuity}
\centerline{\includegraphics[width=0.58\columnwidth]{LW-tvd.pdf}}
\vskip-0.2cm
\begin{itemize}
\item Shocks or discontinuities pose problems for numerical solvers
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Solution methods}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution methods: analytical vs. numerical}
\begin{block}{Analytical solutions}
\begin{itemize}
\item Exact and explicit solution
\item Fast to evaluate the solution behaviour for changes in the parameters: e.g. changes in tension
\item Gives insight into the solution behaviour
\item Only available for special and often simple cases
\end{itemize}
\end{block}
\vskip0.0cm
\begin{block}{Numerical solutions}
\begin{itemize}
\item Available for any case (limited by available computational resources)
\item Might take significant time to compute
\item Only gives the solution for one set of parameters and boundary conditions
\item Needs to be evaluated for every set of parameters
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\section{PDE vs ODE}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{PDE vs ODE}
\vskip-0.1cm
\begin{block}{More than one independent variable}
\begin{itemize}
\item The wave equation depends on time and space
\item Stationary problems with multiple space dimensions 
\end{itemize}
\end{block}
\vskip-0.1cm
\begin{block}{Partial derivatives}
\begin{itemize}
\item \textbf{ODE:} Derivative 
\[
\frac{\mathrm{d}f}{\mathrm{d}t} = f' = \dot{f} 
\]
\item \textbf{PDE:} Partial derivative
\begin{align*}
\pderiv{f(t,x)}{t} &= f_{t}(t,x) = \partial_{t} f(t,x) \\
\pderiv{f(t,x)}{x} &= f_{x}(t,x) = \partial_{x} f(t,x) \\
\pdderiv{f(t,x)}{x} &= f_{xx}(t,x) = \partial_{xx} f(t,x)
\end{align*}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{PDE vs ODE continued}
\vskip-0.1cm
\begin{block}{Existence of a solution}
\begin{itemize}
\item \textbf{ODE:}  Existence and uniqueness theorems exist
\item \textbf{PDE:}  No guarantees of existence
\end{itemize}
\end{block}
\vskip-0.1cm
\begin{block}{Initial and boundary conditions}
\begin{itemize}
\item Solution behaviour for many PDEs depends on the conditions at the boundaries and how the system is initialised 
\item It is often easy to find a solution of the PDE
\item Much harder to find a solution fitting the initial and boundary conditions
\end{itemize}
\end{block}
\begin{block}{Illustration of the solution much harder}
\begin{enumerate}
\item \textbf{ODE:}  Time-value plot
\item \textbf{PDE:}  Multiple space plus time dimensions
\end{enumerate}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Introduction-AP1: Order and independent variables}
\vskip-0.cm
\begin{block}{What is the order and what are the independent variables of the following PDE?}
\[
\pdderiv{x}{y} = \pderiv{x}{t} + \sin(t)
\]
\centering{
%\includegraphics[width=5.cm]{orthogonal_sine_cosine_3}
}
\vskip-0.1cm
\begin{itemize}
\item Discuss on the course discussion board
\end{itemize}
\end{block}
\vskip-0.5cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item Order 1, $x$ and $t$
\item Order 1, $x$ and $y$
\item Order 1 and $t$
\item Order 2, $x$ and $t$
\item Order 2, $x$ and $y$
\item Order 2, $y$ and $t$
\item Order 2 and $t$
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\begin{block}{Solution}
\begin{enumerate}
\item[6.] Order 2, $y$ and $t$
\begin{itemize}
\item The order is given by the highest partial derivative
\item The independent variables are $y$ and $t$ because they are not dependent on any other variable
\end{itemize}
\end{enumerate}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\subsection{Summary}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Attributes of PDEs}
\begin{itemize}
\item More than one independent variable
\item Partial derivatives with respect to the independent variables
\item Existence and uniqueness of the solution not guaranteed
\item Solution behaviour for many PDEs depends on the conditions at the boundaries and how the system is initialised 
\item It is often easy to find a solution of the PDE but it is much harder to find a solution fitting the initial and boundary conditions
\item Illustration of the solution is much harder compared to ODEs
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next analytical part}
\begin{itemize}
\item Topic: \textbf{Applications of PDEs in Mechanical Engineering}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}