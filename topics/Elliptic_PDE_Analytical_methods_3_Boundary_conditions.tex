\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Elliptic PDE Part 3: Boundary conditions}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Differentiate the different boundary conditions and explain their physical meaning
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Solve the Laplace equation with the method of separation of variables
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Recap}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Recipe for separated solutions}
Consider the three possible solutions 
\begin{align*}
u_1(x,y) =& (A\sin \mu x+B\cos \mu x)\left(Ce^{\mu y}+De^{-\mu y}\right)&\lambda=-\mu^2<0, \\
 =& (A\sin \mu x+B\cos \mu x)\left(\tilde{C}\cosh \mu y+\tilde{D}\sinh \mu y\right) & \\
u_2(x,y) =& \left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y)&\lambda=\mu^2>0,\\
 =& \left(\tilde{A}\cosh \mu x+\tilde{B}\sinh \mu x\right)(C\sin \mu y+ D\cos \mu y) & \\
u_3(x,y) =& (Ax+B)(Cy+D)&\lambda=0
\end{align*}
and go through the boundary conditions to
\smallskip
\begin{enumerate}
\item Eliminate basic solutions
\item Specify the permissible values of $\mu$
\item Superpose solutions to fit the initial condition
\end{enumerate}
\end{frame}

% -------------------------------------------------------- %
\section{Boundary conditions}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Types of boundary conditions}
\begin{itemize}
\item So far we have only looked at the boundary conditions on the left
\item What do these conditions signify?
\item Can you think of any other boundary conditions?
\end{itemize}
\centerline{\includegraphics[height=0.5\textheight]{Laplace_derivation}
\pause
\includegraphics[height=0.5\textheight]{Laplace_BC}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Definitions}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Types of boundary conditions}
If $\Omega$ is the domain on which the given equation is to be solved and $\partial\Omega$ denotes its boundary, we can have
\begin{itemize}
\item Dirichlet 
\[u\text{ is specified on } \partial\Omega.\]
\item Neumann 
\[\pderiv{u}{n}\text{ is specified on }\partial\Omega.\]
\item Cauchy  
\[\text{both }u\text{ and }\pderiv{u}{n}\text{ are specified on }\partial\Omega.\]
\item Robin 
\[au+B\pderiv{u}{n}=g\text{ on }\partial\Omega.\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Mixed boundary conditions}
Two or more BC of different types are specified on $\partial\Omega$

\centerline{\includegraphics[height=0.75\textheight]{MixedBC.pdf}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Practical meaning}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Boundary conditions and physics}
Consider that the Laplace equation is used to model heat conduction in a sheet of metal, 
\begin{itemize}
\item \textcolor{blue}{Dirichlet} conditions -- the edges of the sheet are held at a specified temperature
\item \textcolor{blue}{Neumann} conditions -- the heat flux at the edges is given 
\end{itemize}
The important question is: Can a solution be found?
\vskip0.2cm
\centerline{\includegraphics[width=0.5\columnwidth]{LaplaceBC.pdf}}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest Elliptic-AP3a: Solution shape}
\vskip0.cm
\begin{block}{Look at the possible solutions and find the one with the correct solution shape for the Laplace equation with following boundary conditions?}
\vskip-0.5cm
\begin{tiny}
\begin{align}
u(x,0) &= 1 + x & 0\le x\le 2 \\
u(x,1) &= 1 - x & 0\le x\le 2 \\
\pderiv{u}{x}(2, y) &= 0, &0\le y\le 1 \label{eq:BC3}\\
u(0,y) &= 1 & 0\le y \le 1
\end{align}
\end{tiny}
\vskip-0.9cm
\begin{itemize}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\begin{overprint}
\onslide<1>
\vskip-0.6cm
\begin{block}{ConcepTest: possible solutions}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_1contour}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_5contour}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_3contour}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_4contour} \\
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_1surf}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_5surf}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_3surf}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_4surf}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\begin{itemize}
\item Second from the left
\end{itemize}
\vskip0.cm
\begin{columns}
\column{.6\textwidth}
\includegraphics[width=0.48\columnwidth]{Laplace_concepTest_5contour}
\includegraphics[width=0.48\columnwidth]{Laplace_concepTest_5surf}
\column{.4\textwidth}
\begin{itemize}
\item The x derivative at $x=2$ is zero for this solution
\item This is required for boundary condition~\ref{eq:BC3}
\end{itemize}
\end{columns}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\subsection{Dependence on data}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Is the PDE well-posed?}
\vskip-0.1cm
\begin{block}{The behaviour of PDEs is defined by}
\vskip-0.1cm
\begin{itemize}
\item Initial and boundary conditions
\item Coefficient functions
\item Inhomogeneous terms
\item These define the data on which the solution depends
\end{itemize}
\end{block}
\vskip-0.2cm
\begin{block}{Well-posed problem}
\vskip-0.1cm
\begin{enumerate}
\item A solution exists
\item The solution is unique
\item The solution depends continuously on the data: small changes in the data produce small changes in the solution
\end{enumerate}
\end{block}
\vskip-0.2cm
\begin{block}{Ill-posed problem}
\vskip-0.1cm
\begin{itemize}
\item Any of the requirements of well-posed problems is not fulfilled
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Appropriateness of Boundary Conditions}
\includegraphics[width=0.8\columnwidth]{BCMatrix.pdf}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest Elliptic-AP3b: Extreme points of the solution}
\begin{block}{Where can the extreme points, e.g. maximum or minimum, of the solution to the Laplace equation be located for a non-constant solution?}
\vskip-0.1cm
\begin{itemize}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.2cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item On the boundary
\item Inside the solution domain
\item Anywhere
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\begin{block}{Solution}
\begin{enumerate}
\item On the boundary
\begin{itemize}
\item Assume there is a maximum temperature on the inside
\item Heat flows from hot to cold places
\item This heat maximum would dissipate
\item This is called the maximum principle which holds for all non-constant solutions of the Laplace equation
\end{itemize}
\end{enumerate}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
%-------------------------------------------------------%
\subsection{Well-posed elliptic PDE}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Well-posed elliptic PDE}
\begin{itemize}
\item Boundary conditions are given on all sides of the domain
\vskip0.3cm
\centerline{\includegraphics[width=0.5\columnwidth]{LaplaceBC.pdf}}
\item The boundary conditions are either
\begin{itemize}
\item \textcolor{blue}{Dirichlet} conditions -- the edges of the sheet are held at a specified temperature
\item \textcolor{blue}{Neumann} conditions -- the heat flux at the edges is given 
\end{itemize}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Nondimensionalisation}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}