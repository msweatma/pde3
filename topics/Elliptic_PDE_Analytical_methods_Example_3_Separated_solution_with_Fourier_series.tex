\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Elliptic PDE Example 3: Separated solution with Fourier series}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Fourier series}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Recap of Fourier series}
Was covered extensively in Engineering Mathematics 2A
\vskip0.2cm
\begin{enumerate}
\item For any periodic function $f(x)$ with period $T$ we can find coefficients $a_n$ and $b_n$ so that
\[
f(x) = \frac{1}{2} a_0 + \sum_{n=1}^\infty a_n \cos\left(\frac{2n\pi x}{T}\right) + \sum_{n=1}^\infty b_n \sin\left(\frac{2n\pi x}{T}\right)
\]
\item The coefficients of the full-range extension can be calculated by
\begin{align*}
a_0 &= \frac{2}{T} \int_{0}^{T} f(x) dx \\
a_n &= \frac{2}{T} \int_{0}^{T} f(x) \cos\left(\frac{2n\pi x}{T}\right) dx \\
b_n &= \frac{2}{T} \int_{0}^{T} f(x) \sin\left(\frac{2n\pi x}{T}\right) dx
\end{align*}
\end{enumerate}
\end{frame}

% -------------------------------------------------------- %
\subsection{Periodic extension}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Periodic extensions}
\begin{itemize}
\item Full-range periodic extension
\begin{flushright}
\includegraphics[width=7.3cm]{Glyn_Advanced_7_17}
\end{flushright}
\item Even periodic extension
\begin{flushright}
\includegraphics[width=7.3cm]{Glyn_Advanced_7_18}
\end{flushright}
\item Odd periodic extension
\begin{flushright}
\includegraphics[width=7.3cm]{Glyn_Advanced_7_19}
\end{flushright}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Half-range extension}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Half-range extensions}
\begin{itemize}
\item The half-range extensions have half the frequency: the factor $2$ is removed from the cosine/sine
\item Even periodic extension: cosine series
\[
a_n = \frac{2}{T} \int_{0}^{T} f(x) \cos\left(\frac{n\pi x}{T}\right) dx
\]
\item Odd periodic extension: sine series
\[
b_n = \frac{2}{T} \int_{0}^{T} f(x) \sin\left(\frac{n\pi x}{T}\right) dx
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.30}
% -------------------------------------------------------- %
\subsection{Problem}
% Check: x>=0???
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.30}
\begin{block}{Solve the Laplace equation $\nabla^2 u = 0$ subject to the following boundary conditions}
\begin{align}
u(x,0) &= 0 & x\ge 0 \\
u(x,1) &= 0 & x\ge 0 \\
u(x,y) &\rightarrow 0 & x\rightarrow\infty, 0\le y\le 1 \\
u(0,y) &= 1 & 0\le y \le 1
\end{align}
\end{block}
To satisfy the third condition we need a solution which is exponential in x, so we need to use $u_2$
\[
u_2(x,y) = \left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y)
\]
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.30}
Remove the subscript and step through the boundary conditions to find the parameters.
\[
u(x,y) = \left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y)
\]
\begin{enumerate}
\item The condition $\lim_{x\rightarrow\infty}u(x,y)=0$ is only satisfied if $A=0$ because the positive exponential is unbounded. Thus the solution simplifies to

\[
u(x,y) = e^{-\mu x}(C^*\sin \mu y+D^*\cos \mu y)
\]
where $C^*=BC$ and $D^*=BD$.

\item Now, consider the first boundary condition, $u(x,0)=0$ \pause
\begin{align*}
u(x,y=0) &= e^{-\mu x}(C^*\sin(\mu 0)+D^* \cos(\mu 0))  \\
  &= e^{-\mu x}D^* \overset{!}{=} 0
\end{align*}
which implies that $D^*=0$. 
\end{enumerate}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.30, continued}
\begin{enumerate}
\item[3.] We are left with the following solution
\[u(x,y) = C^* e^{-\mu x} \sin \mu y \] 
\item[4.] The second boundary condition, $u(x,1)=0$ gives \pause
\begin{align*}
u(x,y=1) &= C^* e^{-\mu x}C^*\sin(\mu 1) \overset{!}{=} 0 \\
 & \implies\ \sin\mu=0
\end{align*}
so $\mu=n\pi,\,n=1,2,\ldots$
\item[5.] Because the Laplace equation is linear we can superpose the solutions, to get
\[
u(x,y) = \sum_{n=1}^\infty C^*_ne^{-n\pi x}\sin n\pi y,\quad 0\le y \le 1.
\]
\end{enumerate}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.30, continued}
\begin{enumerate}
\item[6.] From the final boundary condition, $u(0,y)=1,\ 0\le y\le1$ it follows that 
\pause 
\[
1 \overset{!}{=} \sum_{n=1}^\infty C^*_n\sin n\pi y,\quad 0\le y \le 1.
\]
which is a classical Fourier series problem. This is a half-range Fourier sine series with period $T=1$ , so that with the formula from earlier we get
\[
C^*_n=2\int_0^1\sin n\pi y\ \text{d}y=
\begin{cases}
4/n\pi&n\text{ odd}\\
0&n \text{ even}
\end{cases}
\]
\end{enumerate}

The complete solution is therefore
\textcolor{red}{
\[u(x,y) = \frac{4}{\pi}\sum_{k=1}^\infty \frac{1}{2k-1}e^{-(2k-1)\pi x}\sin\left((2k-1)\pi y\right)\]}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.30 - plot the first 4 terms of the sum}
\begin{tikzpicture}
\begin{axis}[
title={$u(x,y) = \frac{4}{\pi}\sum_{k=1}^\infty \frac{1}{2k-1}e^{-(2k-1)\pi x}\sin\left((2k-1)\pi y\right)$},
xlabel=$x$, ylabel=$y$,
]
\addplot3[
surf,
domain=0:1, 
domain y=0:1,
]
{4/pi*(exp(-pi*x)*sin(180*y)
+exp(-3*pi*x)*sin(3*180*y)/3
+exp(-5*pi*x)*sin(5*180*y)/5
+exp(-7*pi*x)*sin(7*180*y)/7
)};
\end{axis}
\end{tikzpicture}
\begin{itemize}
\item Evaluating this series at $x=0$ requires more than 30 terms to be evaluated, while at $x=1$ only one or two terms are needed.
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Thank you}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}