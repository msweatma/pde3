\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Parabolic PDE Part 2: Nondimensionalisation and diffusion equation}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Nondimensionalise the heat conduction equation
\item Describe the heat conduction equation in terms of mass diffusion 
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Derive the heat conduction equation in one spatial dimension 
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Recap}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Heat conduction or diffusion equation}
\begin{itemize}
\item The heat conduction equation in $n$ space dimensions is given by
\begin{equation}
\frac1\kappa\pderiv{T}{t} = \grad^2T = \pdderiv{T}{x_1} + \cdots + \pdderiv{T}{x_n}
\label{eq:heat_conduction_PDE}
\end{equation}
\item This equation is \textcolor{red}{parabolic} in nature and given enough time (and constant boundary conditions) the solution will converge to a steady state
\item If there are no time-varying inputs this has the steady state solution, $U$, satisfying the Laplace equation
\[ 
\nabla^2U = 0
\]
and the boundary conditions
\item Depending on the context, the parabolic partial differential equation~\ref{eq:heat_conduction_PDE} can describe many diffusion processes
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Parabolic PDE}
% -------------------------------------------------------- %
\subsection{Thermal properties}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Thermal conductivity}
\begin{itemize}
\item Vacuum insulation panels $k\approx0.007$ in W$\,$m$^{-1}\,$K$^{-1}$
\item Gases: $k\approx0.005 - 0.2$ in W$\,$m$^{-1}\,$K$^{-1}$
\begin{itemize}
\item CO$_2$ $0.0146\,$W$\,$m$^{-1}\,$K$^{-1}$
\item Hydrogen  $0.168\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\item Insulators: $k\approx0.02 - 0.7$ in W$\,$m$^{-1}\,$K$^{-1}$
\begin{itemize}
\item Styrofoam $0.033\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\item Liquids: $k\approx0.2 - 8$ in W$\,$m$^{-1}\,$K$^{-1}$
\begin{itemize}
\item Water $0.58\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\item Non-metallic solids: $k\approx0.4 - 60$ in W$\,$m$^{-1}\,$K$^{-1}$
\begin{itemize}
\item Ice $2.18\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\item Alloys: $k\approx15 - 150$ in W$\,$m$^{-1}\,$K$^{-1}$
\begin{itemize}
\item Chrome nickel steel $16.3\,$W$\,$m$^{-1}\,$K$^{-1}$
\item Aluminium bronze  $121\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\item Metals: $k\approx50 - 429$ in W$\,$m$^{-1}\,$K$^{-1}$
\begin{itemize}
\item Silver $429\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Nondimensionalisation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Nondimensionalisation}
\vskip0.cm
We pick characteristic length and time to nondimensionalise:
\begin{itemize}
\item Characteristic length: pick the length of the domain $L$ \[x = L \chi\]
\item Characteristic time: pick the domoin length squared divided by the thermal diffusivity
\[
t = \frac{L^2}{\kappa}\tau
\]
\item From this it follows
\begin{align*}
\frac{\kappa}{L^2}\pderiv{T}{\tau} = \pderiv{T}{t} &= \kappa\pdderiv{T}{x} = \frac{\kappa}{L^2}\pdderiv{T}{\chi} \\
\pderiv{T}{\tau} &= \pdderiv{T}{\chi}
\end{align*}
\item This nondimensionalisation removed the only parameter
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Diffusion equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Diffusion equation}
\centerline{\includegraphics[width=0.75\columnwidth]{Blausen_0315_Diffusion.png}}
\begin{itemize}
\item A similar argument using \textbf{Fick's law} for diffusion instead of \textbf{Fourier's law} leads to an identical results with a different definition of $\kappa$
\item Here $\kappa$ is the mass diffusivity again with units m$^{2}\,$s$^{-1}$
\item Application examples
\begin{itemize}
\item Diffusion of dopants in microfabrication
\item Mixing of two fluids
\end{itemize}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
% -------------------------------------------------------- %
\subsection{Nondimensionalisation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Nondimensionalisation and parameter values}
\begin{itemize}
\item Values of the thermal conductivity range between
\begin{itemize}
\item Vacuum insulation panels $k\approx 0.007$ in W$\,$m$^{-1}\,$K$^{-1}$
\item Silver $k\approx 429\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\item The nondimensionalisation of the heat conduction equation can remove the diffusivity parameter
\item The same partial differential equation can also describe other diffusion processes
\end{itemize}
\centerline{\includegraphics[width=0.75\columnwidth]{Blausen_0315_Diffusion.png}}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{General separated solution}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}