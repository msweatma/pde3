\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Elliptic PDE Part 5: Complex variable solutions}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Describe the Joukowski transform
\item Verify if a complex variable function is a solution to the Laplace equation
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Nondimensionalise the Laplace equation
\item Calculate the real valued solution to the Laplace equation with the method of separation of variables
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Recap}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Nondimensionalisation procedure}
\begin{block}{Procedure}
\begin{enumerate}
\item Identify a characteristic value for each dependent and independent variable
\item Replace each dependent and independent variable with a variable scaled with the characteristic value
\item Rewrite equations in terms of the nondimensional variables
\end{enumerate}
\end{block}
\begin{block}{Example}
\begin{itemize}
\item Scale temperature with the reference temperature
    \[T=\frac{\tilde{T} - T_{ref}}{T_{ref}}\]
\end{itemize}
\end{block}
\vskip-0.6cm
\begin{block}{Remarks:}
\begin{itemize}
\item Nondimensionalisation can reduce the number of parameters
\begin{itemize}
\item This makes it easier to compare numerical experiments
\end{itemize}
\item Scaling can make numerical calculations more stable
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\section{Complex variable solution}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Complex variable solutions}
\begin{itemize}
\item The real and imaginary parts of a \textcolor{blue}{differentiable} function, $f(z)$, of the complex variable $z=x+iy$ automatically satisfy the Laplace equation
\item Remember the solution
\vskip0.0cm
\centering\includegraphics[width=0.5\columnwidth,]{StreamLines.pdf}
\vskip0.0cm
\item The Joukowski transformation enables us to map the solution around a circle onto an airfoil surface (see \url{www.grc.nasa.gov/WWW/k-12/airplane/map.html})\\
\vskip0.05cm
\centerline{\includegraphics[width=0.28\columnwidth, height=2.8cm]{Joukowsky_transform1}
\includegraphics[width=0.55\columnwidth, height=2.8cm]{Joukowsky_transform2}}
\vskip-0.0cm
\item This was used to find potential flow solutions to design airfoils without computers
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Joukowski transform}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Joukowski transform}
\centerline{\includegraphics[width=0.3\columnwidth]{Joukowsky_transform1}
\includegraphics[width=0.6\columnwidth]{Joukowsky_transform2}}
\begin{itemize}
\item The Joukowski transform is given by $z=\zeta+\frac1\zeta$
\item It maps a circle $\zeta=\chi + i\eta$ onto an airfoil $z=x+iy$
\begin{align*}
 x+iy = z &=  \zeta + \frac1\zeta = \chi + i \eta + \frac{1}{\chi + i \eta}  \\
 & = \chi + i \eta + \frac{\chi - i \eta}{\chi^2 + \eta^2} \\
 & = \frac{\chi(\chi^2 + \eta^2 + 1)}{\chi^2 + \eta^2} + i \frac{\eta(\chi^2 + \eta^2 - 1)}{\chi^2 + \eta^2} 
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.32}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.32}
\begin{block}{Consider the complex function $f(z)=\phi(x,y) + i\psi(x,y)$}
\begin{itemize}
\item The function depends on the complex variable $z = x + iy$
\item Verify that $\phi$ and $\psi$ satisfy the Laplace equation for the case where $f(z)=z^2$
\item Sketch the contours of $\phi$ and $\psi$
\end{itemize}
\end{block}
\vskip0.1cm
\begin{block}{Solution}
\begin{itemize}
\item We can write $f(z)$ in terms of the real and imaginary part to get
\[f(z) = z^2 = x^2-y^2 + i2xy\]
\item Here, the real and imaginary parts are given by
\begin{align*}
\phi &= \Re(f(z))=x^2-y^2 \\
\psi & =\Im(f(z))=2xy
\end{align*}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Check that $\phi$ is a solution of the Laplace equation}
\begin{itemize}
\item We differentiate $\phi$ w.r.t. $x$  to get
\[\pderiv{\phi}{x}=2x\text{ and }\pdderiv{\phi}{x}=2\]
\item Differentiating $\phi$ w.r.t. $y$  gives
\[\pderiv{\phi}{y}=-2y\text{ and }\pdderiv{\phi}{y}=-2\]
\item Inserting the previous two terms into the Laplacian shows that $\phi$ is a solution
\[\nabla^2\phi=\pdderiv{\phi}{x}+\pdderiv{\phi}{y}=2-2=0\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Check that $\psi$ is a solution of the Laplace equation}
\begin{itemize}
\item Differentiating $\psi$ w.r.t. $x$  gives
\[\pderiv{\psi}{x}=2y\text{ and }\pdderiv{\psi}{x}=0\]
\item Differentiating $\phi$ w.r.t. $y$  gives
\[\pderiv{\psi}{y}=2x\text{ and }\pdderiv{\psi}{y}=0\]
\item The Laplacian is
\[\nabla^2\psi=\pdderiv{\psi}{x}+\pdderiv{\psi}{y}=0+0=0\]
\item So the Laplace equation is also satisfied for $\psi$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Contour plot}
\vskip-0.3cm
\centerline{\includegraphics[width=0.8\columnwidth]{Ex9-32.pdf}}
\vskip-0.5cm
\begin{itemize}
\item $\psi = 2xy = \text{const}$ give streamlines of flow into a corner
\item $\phi = x^2-y^2 = \text{const}$ intersect with the streamlines at right angles
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
%-------------------------------------------------------%
\subsection{Complex variable solution}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Complex variable solution}
\begin{itemize}
\item The Joukowski transform can be used to map a circle onto an airfoil
\centerline{\includegraphics[width=0.28\columnwidth]{Joukowsky_transform1}
\includegraphics[width=0.55\columnwidth]{Joukowsky_transform2}}
\item The real and imaginary parts of a \textcolor{blue}{differentiable} function, $f(z)$, of the complex variable $z=x+iy$ automatically satisfy the Laplace equation
\item It is this method that was used to find the potential flow solutions needed to design airfoils before the advent of the digital computer
\vskip0.0cm
\centering\includegraphics[width=0.5\columnwidth,]{StreamLines.pdf}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Poisson's equation}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}