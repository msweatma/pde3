\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Hyperbolic PDE -- Part 4: Method of characteristics}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Use the method of characteristics to calculate the solution to the wave equation
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Derive and use the d'Alembert solution for the wave equation
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Recap}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{D'Alembert solution}
\begin{itemize}
\item For an initial velocity, $\pderiv{u(x,0)}{t}=G(x),\quad \forall x,$ 
\item And an initial displacement, $u(x,0)=F(x),\quad \forall x,$ 
\item The d'Alembert solution of the wave equation is given by
\[
u(x,t) =\frac12\left[F(x+ct)+F(x-ct)\right]+\frac1{2c}\int_{x-ct}^{x+ct} G(z)\,\text{d}z
\]
\end{itemize}
\centering
\includegraphics[width=7cm]{Ex9-11}
\end{frame}

% -------------------------------------------------------- %
\section{Method of characteristics}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Domains of dependence and influence}
\centerline{\includegraphics[width=0.91\columnwidth]{domainofdependence}}
\vskip0.2cm
\begin{itemize}
\item Dependence --- the solution at $P$ is determined by the initial conditions on the boundary $AB$
\item Influence --- the initial conditions on $AB$ influence the shaded area
\begin{itemize}
\item \textcolor{red}{Data outside of this region cannot be influenced}
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Procedure}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Idea for the method of characteristics}
\begin{itemize}
\item We can find the solution at the point $P$ as
\[
u(P) = f(B)+g(A)
\]
\vskip-0.2cm
\begin{itemize}
\item $f$ is constant along the \textcolor{red}{characteristic} $BP$ 
\item $g$ is constant along the \textcolor{red}{characteristic} $AP$
\end{itemize}
\item The problem is now to compute $f(B)$ and $g(A)$ on the $t=0$ boundary
\item Typical conditions along $t=0$ are $u(x,0)=F(x)$ and $\pderiv{u(x,0)}{t}=G(x)$ 
\item Use $u(x,t) = f(x+ct) + g(x-ct)$ and calculate
{\small\begin{align*}
c\pderiv{u}{x}+\pderiv{u}{t} &= cf^\prime(x+ct) + cg^\prime(x-ct) + cf^\prime(x+ct) - cg^\prime(x-ct) \\
&= 2cf^\prime(x+ct) \text{ and similarly} \\
c\pderiv{u}{x} - \pderiv{u}{t} &= 2cg^\prime(x-ct)
\end{align*}}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Method of characteristics}
\begin{itemize}
\item On $t=0$ we know that $\pderiv{u}{x}=F^\prime(x)$ and $\pderiv{u}{t}=G(x)$, so
\begin{align*}
cF^\prime(x)+G(x) &= 2cf^\prime(x) \\
cF^\prime(x)-G(x) &= 2cg^\prime(x)
\end{align*}
\item Since $F$ and $G$ are given we can compute
\begin{align*}
f^\prime(x) &= \frac12\left[F^\prime(x)+\frac{G(x)}{c}\right] \\
g^\prime(x) &= \frac12\left[F^\prime(x)-\frac{G(x)}{c}\right] 
\end{align*}
\item So $f(x)$ and $g(x)$ can be found by integration
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.12}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Wave equation with $c=1$}
\begin{center}
\includegraphics[width=5.8cm]{GlynJames_9_14}
\end{center}
\begin{align*}
u(x,0) &= V(x) =1, \quad \forall x > 0 \\
\pderiv{u(x,0)}{t} &= 0, \quad\forall x > 0 \\
u(0, t) &= 0, \quad t>0
\end{align*}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution}
\begin{itemize}
\item Find $f$ and $g$ for the general solution
\[
u(x,t) = f(x-t) + g(x+t)
\]
where $c=1$ due to the slope of the characteristics
\item For $x>t$ we get
\begin{align*}
f(x) = g(x) &= \frac{1}{2} V(x) \\
u(x,t) &= \frac{1}{2}\left[V(x+t) + V(x-t)\right] 
\end{align*}
\item For $x<t$ we need to take the boundary condition at $x=0$ into account
\begin{align*}
0 = u(0,t) &= f(-t) + g(t) \Rightarrow f(-z) = -g(z) = -\frac{1}{2} V(z)\\
u(x,t) &= \frac{1}{2}\left[V(x+t) - V(t-x)\right]
\end{align*}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
% -------------------------------------------------------- %
\subsection{Method of characteristics}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Method of characteristics}
\begin{columns}
\column{0.55\textwidth}    
\begin{itemize}
\item We can find the solution at the point $P$ as
\[
u(P) = f(B)+g(A)
\]
\vskip-0.2cm
\item Since $F$ and $G$ are given we can compute
\begin{align*}
f^\prime(x) &= \frac12\left[F^\prime(x)+\frac{G(x)}{c}\right] \\
g^\prime(x) &= \frac12\left[F^\prime(x)-\frac{G(x)}{c}\right] 
\end{align*}
\end{itemize}
\column{0.5\textwidth}
\centerline{\includegraphics[width=0.91\columnwidth]{domainofdependence_left}}
\end{columns}
\begin{block}{Comments}
\begin{itemize}
\item For non-constant coefficient equations the characteristics lines will be curved
\item Intersecting characteristics may lead to shocks in the solution
\item The method of characteristics is the basis of both space marching schemes for solving hyperbolic equations and for semi-Lagrangian methods\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Formal classification of PDEs}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}
