\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\title{SCEE09004:  Partial Differential Equations 3}
\subtitle{Course overview}
\author[Daniel Friedrich]{David Ingram and Daniel Friedrich}
\date{Semester 2, 2020-21\\[12pt]
\includegraphics[width=0.75\columnwidth]{1Line2ColCMYK_CS3.pdf}} 

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{PDE introduction}
% -------------------------------------------------------- %
\subsection{Why study partial differential equations?}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{PDEs are used in most real-world challenges}
\centerline{\includegraphics[width=0.9\columnwidth]{SeaGenSim} }
\vskip0.1cm
Simulation of the SeaGen 1.2MW turbine in a channel with very turbulent flow, created by Dr Angus Creech using the Fluidity computational fluid dynamics package with a mathematical model of the flow over the rotor blades
\end{frame}

% -------------------------------------------------------- %
\subsection{Wave equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Wave equation}
\begin{itemize}
\item The 1D wave equation is given by
\vskip-0.3cm
\[\frac{1}{c^2}\pdderiv{u}{t}=\pdderiv{u}{x}\]
with $c^2=T/\rho$ where $T$ is the tension and $\rho$ is the mass per unit length
\item It can model sound waves, shock waves, electromagnetic waves, water waves, a plucked string, etc.
\item The equation is said to be \textcolor{red}{hyperbolic} in character and has the nasty property that discontinuities in $u$, or its derivative, $\frac{\partial u}{\partial x}$, will propagate in time
\end{itemize}
\centerline{\includegraphics[width=0.5\columnwidth]{VibratingString03-400x220.jpg}}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}<0>[noframenumbering]
\frametitle{Plot of the linear advection solution}
\begin{tikzpicture}[scale=1]
 \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black,domain=-2:10, samples=101] {exp(-x^2)};
  \addplot[blue, dotted, line width=1.5,domain=-2:10, samples=101] {exp(-(x-2)^2)};
  \addplot[cyan,dashed, line width=1.5,domain=-2:10, samples=101] {exp(-(x-4)^2)};
  \addplot[magenta,loosely dotted, line width=1.5,domain=-2:10, samples=101] {exp(-(x-8)^2)};

  \legend{$t=0$,$t=\frac{2h}{c}$,$t=\frac{4h}{c}$,$t=\frac{8h}{c}$}
 \end{axis}
\end{tikzpicture}
\begin{itemize}
\item Shape and magnitude of the disturbance stay constant
\item The wave equation doesn't contain dispersive terms
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Linear Advection with a discontinuity}
\centerline{\includegraphics[width=0.58\columnwidth]{LW-tvd.pdf}}
\vskip-0.2cm
\begin{itemize}
\item Shocks or discontinuities pose problems for numerical solvers
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Solution methods}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution methods: analytical vs. numerical}
\begin{block}{Analytical solutions}
\begin{itemize}
\item Exact and explicit solution
\item Fast to evaluate the solution behaviour for changes in the parameters: e.g. changes in tension
\item Gives insight into the solution behaviour
\item Only available for special and often simple cases
\end{itemize}
\end{block}
\vskip0.0cm
\begin{block}{Numerical solutions}
\begin{itemize}
\item Available for any case (limited by available computational resources)
\item Might take significant time to compute
\item Only gives the solution for one set of parameters and boundary conditions
\item Needs to be evaluated for every set of parameters
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Heat conduction or diffusion equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Heat-conduction or diffusion equation}
\centerline{\includegraphics[width=0.45\columnwidth]{heated_rod.jpg}}
\begin{itemize}
\item General equation
\[
\frac1\kappa\pderiv{T}{t}=\grad^2T
\]
\item This equation is \textcolor{red}{parabolic} in nature and given enough time (and constant boundary conditions) the solution will converge to a steady state
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Laplace Equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace Equation}
\vskip0.1cm
\begin{itemize}
\item The general form of the Laplace equation is given by
\[\grad^2u=0\]
\item It is an \textcolor{red}{elliptic} equation which models the steady state of a diffusion like process.  
We can use it to describe the pressure in an incompressible flow, steady state diffusion and heat conduction, stress and strain, etc.
\end{itemize}
\centering
\includegraphics[width=0.8\columnwidth,]{StreamLines.pdf}
\begin{itemize}
\item The streamlines show the flow of an inviscid, irrotational fluid past a cylinder placed in a uniform stream
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Navier-Stokes equations}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fun with PDEs}
\centerline{\includegraphics[width=\columnwidth]{DeLaval.pdf}}
\begin{itemize}
\item Solution of the Navier-Stokes equations with commercial software in Computational Fluid Dynamics 5 (MECE11004)
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Course overview}
% -------------------------------------------------------- %
\subsection{Learning outcomes}
% -------------------------------------------------------- %
\begin{frame}{Learning outcomes}
This course will introduce you to mathematical and numerical tools and techniques for analysing and solving of 2$^{\text{nd}}$ order partial differential equations which are invaluable to engineers building models of real dynamic systems and processes.
\vskip0.1cm
\begin{enumerate}
\item Distinguish between the three different types of second order partial differential equations; this includes their properties and general solution behaviour
\item Calculate the analytical solution of engineering problems described by the three types of linear, constant coefficient second order partial differential equations
\item Use Matlab to simulate the numerical solution of engineering problems described by second order partial differential equations
\item Evaluate the performance and suitability of the numerical methods for the three different types of partial differential equations 
\end{enumerate}
\end{frame}

% -------------------------------------------------------- %
\subsection{Syllabus}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Course syllabus}
\begin{itemize}
\item The material is split into one block for each of the three fundamental types of partial differential equations plus an introduction block
\item Each of these blocks contains two learning modules: analytical methods taught by Dr Daniel Friedrich and numerical methods taught by Prof David Ingram. 
\item While the analytical and numerical methods learning modules for each block are designed to be independent, it is recommended that you start with the analytical methods learning module which introduces the PDE type. 
\end{itemize}
\vskip0.0cm
\begin{enumerate}
    \item Week 1: Introduction to partial differential equations
    \item Weeks 1-4: Elliptic partial differential equations
    \begin{enumerate}
    \item Analytical methods
    \item Numerical methods
    \end{enumerate}
    \item Weeks 5-7: Parabolic partial differential equations
    \begin{enumerate}
    \item Analytical methods
    \item Numerical methods
    \end{enumerate}
    \item Weeks 8-10: Hyperbolic partial differential equations
    \begin{enumerate}
    \item Analytical methods
    \item Numerical methods
    \end{enumerate}
\end{enumerate}
\end{frame}

%-------------------------------------------------------%
\section{Course delivery}
%-------------------------------------------------------%
\subsection{Overview}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle{Overview of the new course delivery}
The teaching and learning for PDE3 are split into a number of different activities
\begin{enumerate}
\item Learning modules
\item Exercise sheets
\item Hybrid seminars
\item Computer labs
\item Coursework
\end{enumerate}
\vskip0.3cm
\begin{itemize}
\item All of these are supported by discussions on the discussion board
\item Familiarity with material from Engineering Mathematics 2A and 2B is expected
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Learning modules}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle{Learning modules}
The course material is split into Learning modules that contain different activities:
\begin{itemize}
\item Short lecture style videos which introduce and discuss the theory
\item Example videos in which we apply the theory to examples and provide step by step solutions
\item The corresponding sections and pages in the textbooks
\begin{itemize}
\item These will introduce and handle the material slightly differently
\item By comparing the presentation in the textbooks to the videos you will deepen your understanding of the topics
\end{itemize}
\item Class discussions on the discussion board
\begin{itemize}
\item Please open a thread on the discussion board if you have any questions about the content
\item We will monitor these threads, endorsing students' solutions and providing solutions ourselves
\end{itemize}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Exercise sheets}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle{Exercise sheets}
Following along the solutions is a good way to get started but it is essential that you try to solve similar problems yourself.
\begin{itemize}
\item Exercise sheets for each topic provide practice questions for you
\item You are strongly encouraged to work through these exercise sheets by yourself or in small groups (subject to physical distancing rules)
\item Ask questions on the discussion board if you are stuck
\item The threads for the exercise sheets will be monitored by us
\item The book sections provide additional problems for you to work through and solve
\begin{itemize}
\item Ask on the discussion board if you get stuck with these
\end{itemize}
\item We will show step by step solutions in the hybrid seminars and discuss your open questions
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Hybrid seminars}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle{Hybrid seminar structure}
\begin{itemize}
\item Synchronous sessions take place every Monday afternoon
\item You work through the corresponding exercise sheet questions before the hybrid seminar
\item During the seminar one tutor will solve some of the question on the visualiser
\item The other tutor will monitor the live chat as well as the in-person participants so that any questions about the problems can be covered
%\item We will run a poll before the hybrid seminar to establish the problems which most students find difficult
\end{itemize}
\begin{block}{Information about hybrid seminars}
\begin{itemize}
\item Starting Semester 1 video \url{https://media.ed.ac.uk/media/1_gbt2h4aq}
\item Archive of emails - at the bottom of our Teaching2020 web page \url{https://www.eng.ed.ac.uk/students/teaching-2020/}
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Computer lab}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle{Computer lab structure}
\begin{itemize}
\item Synchronous sessions take place every Wednesday afternoon
\item You are going to solve partial differential equations in Matlab using Finite Difference methods
\item You will work in pairs or small groups
\item Tutors will support you programming in Matlab and help you to locate and fix bugs in your code
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Discussion board}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle{Discussion board}
\begin{itemize}
\item We are going to use the discussion board on Learn for all questions about the course
\item All$^*$ questions should be asked on the discussion board
\begin{itemize}
\item All students benefit from the questions and answers
\item Collaborative spaces for all students to develop the solutions yourself
\item This active learning helps you to gain a deeper understanding of the material
\end{itemize}
\item We will also use the discussion board for follow-on discussions for Top Hat polls
\item[*] Please email the lecturers directly if you have a personal question
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Study guide}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle{Workload}
\begin{itemize}
\item PDE3 is a 10 credit course
\item This should correspond to around 100 hours of student effort over the semester
\item The semester has 11 teaching weeks and 1 revision week
\item The University expects you to spend \textcolor{red}{7.5 hours per week} studying PDEs
\item We provide \textcolor{blue}{1 hour per week}
\item So, you need to do \textcolor{red}{6.5 hours per week} of \textcolor{red}{independent self study}
\end{itemize}
\end{frame}

%----- Frame -------------------------------------------%
\begin{frame}
\frametitle{Independent learning}
\begin{itemize}
\item We have prepared all of the course material in a format that is suitable for self-study, which means that you can cover the material when best for you. 
\item Because of this change, and the lack of regular lectures to pace you through the course material, it is even more important than in the past to manage your study time carefully.
\item We strongly suggest you spend 1 hour a day doing mathematics (do five problems every day!!)
\begin{itemize}
\item Use examples from exercise sheets
\item Use examples and questions from the text books
\item More examples can be found in Schaum's Outline series
%\item The continuous assessment in Stack provide detailed, formative, feedback
\item There are videos with worked examples and advice on sitting exams
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Weekly targets}
% -------------------------------------------------------- %
\begin{frame}<0>[noframenumbering]
\frametitle{Weekly course preparation}
The \WeeklyTarget give
\begin{itemize}
\item Learning aims for each week
\item Pre-class preparation
\begin{itemize}
\item Sections in the text books
\item Additional sources
\end{itemize}
\item An weekly, open feedback question
\item I will look through the feedback to address difficult, interesting, challenging topics
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}<0>[noframenumbering]
\frametitle{Be an active participant}
\begin{columns}
\column{.46\textwidth}
\includegraphics[width=5.2cm]{teachermemes_silent}\\
\tiny{www.teachermemes.com/participation.html}
\column{.54\textwidth}
\begin{itemize}
\item Engage in the discussion on the forum
\item Ask and answer some questions
\item There is no shame in giving a wrong answer
\item Let me know if something is not clear
\end{itemize}
\includegraphics[width=5.5cm]{minions}
\end{columns}
\end{frame}

%-------------------------------------------------------%
\subsection{Textbooks}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle{Books}
You must have a copy of
\begin{itemize}
\item Advanced Modern Engineering Mathematics by Glyn James et al., Fifth Edition
\item Other editions of the books are fine but the page numbers and sections will be given for the 5$^\text{th}$ Editions
\item The library has a small number of these books available but not enough for all students in the class
\end{itemize}
\begin{block}{Comments}
\begin{itemize}
\item The Learning modules will provide detailed page numbers and sections covered in each module
\item You are strongly recommended to work through the exercises in the book and to read and practice the material as it is taught. 
\item We will deal with the analytical solution of partial differential equations (PDEs) and with finite difference methods. 
\item The extended reading list can be found \href{https://eu01.alma.exlibrisgroup.com/leganto/readinglist/lists/28605598920002466}{here}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Top Hat}
% -------------------------------------------------------- %
\begin{frame}{Top Hat}
\begin{itemize}
\item We will use the Top Hat voting system
\item The voting system works with any personal electronic device which can access the internet, i.e. smartphone, tablet or laptop
\begin{itemize}
\item To configure your device to access the University's wireless network see the information about the \href{http://www.ed.ac.uk/information-services/computing/desktop-personal/wireless-networking}{eduroam wireless network}
\item To set up your Top Hat acccount, follow \href{http://www.ed.ac.uk/information-services/learning-technology/electronic-voting-system/students/account}{the instructions here}
\item Once you have a Top Hat account, follow \href{http://www.ed.ac.uk/information-services/learning-technology/electronic-voting-system/students/adding}{these instructions} to join the Partial Differential Equations 3 course. The code is \textbf{191427} and here is a link to \TopHat
\item If you are using an Android or iOS device, we recommend using the \href{http://www.ed.ac.uk/information-services/learning-technology/electronic-voting-system/students/mobile-apps}{custom application} as it will make it quicker and easier for you to log in 
%\item Remember to bring your device to each class
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Matlab}
% -------------------------------------------------------- %
\begin{frame}{Matlab refresh}
\begin{itemize}
\item We will use Matlab throughout this course
\item You need to be comfortable using it
\item The School of Engineering has an interactive Matlab introduction
\begin{itemize}
\item Available at \href{http://www.matlab.eng.ed.ac.uk/}{\beamergotobutton{www.matlab.eng.ed.ac.uk}}
\end{itemize}
\item Matlab is available on the computers in the teaching labs
\item Alternatively you can use Octave 
\begin{itemize}
\item Most Matlab scripts run with no or only small modifications in Octave
\item Available for free at \href{https://www.gnu.org/software/octave/}{\beamergotobutton{www.gnu.org/software/octave}}
\item Also available for most Android devices
\end{itemize}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Introduction-CO2: Matlab proficiency}
\vskip-0.cm
\begin{block}{What is your Matlab experience level?}
\vskip-0.1cm
\begin{itemize}
\item Answer on \TopHat
\end{itemize}
\end{block}
\vskip-0.5cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item Expert
\item Proficient
\item Intermediate
\item Novice
\item I know what it is but have not used it myself
\item Never heard about it
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\begin{block}{Solution}
\begin{enumerate}
\item[6.] Order 2, $y$ and $t$
\begin{itemize}
\item The order is given by the highest partial derivative
\item The independent variables are $y$ and $t$ because they are not dependent on any other variable
\end{itemize}
\end{enumerate}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\section{Assessment}
% -------------------------------------------------------- %
\subsection{Coursework}
% -------------------------------------------------------- %
\begin{frame}{Coursework}
\begin{block}{Coursework 40$\%$}
\begin{itemize}
\item Two mechanical engineering based problems to be submitted electronically to the ETO
\item Each coursework is worth 20$\%$
\item Coursework topics
\begin{enumerate}
\item Static PDE, due in week 5
\item Transient PDE, due in week 9
\end{enumerate}
\item Marks will be awarded for a correct solution and for mathematical style and presentation
\item Failing to submit a problem on time will have serious consequences and could lead to you failing the course.
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Exam}
% -------------------------------------------------------- %
\begin{frame}{Exam}
\begin{block}{Exam 60$\%$}
\begin{itemize}
\item Most likely a 24 hours exam but exact details will be confirmed later
\item Two questions each for the analytical and numerical part of the course
\item You need to solve 3 of these 4 questions
\item The questions for the analytical part will be similar to those from previous years 
\item Previous exams from Engineering Mathematics 2A and 2B, Mathematics for Science and Engineering 2B and from Mathematics for Electrical and Mechanical Engineers and Mathematics for Chemical Engineers will be useful.
\item Example exam questions for the numerical part will be made available on Learn
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
% -- Appendix -------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}