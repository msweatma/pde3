\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Parabolic PDE Part 4: Initial and boundary conditions}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Give the appropriate boundary conditions for the heat conduction or diffusion equation
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Derive the separated solution for the heat conduction or diffusion equation
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Recap}
%-------------------------------------------------------%
\begin{frame}
\frametitle{General solution of the heat conduction or diffusion equation}
\begin{itemize}
\item For constant boundary conditions and source terms the solution to the heat conduction or diffusion equation is given by
\vskip-0.2cm
\[
u = U + v
\]
\vskip-0.1cm
\begin{itemize}
\item $U(x)$ is the steady state solution
\item $v(x,t)$ is the transient (time-varying) part that satisfies both the PDE and the boundary conditions on $u-U$
\end{itemize}
\item Solution procedure for the heat conduction or diffusion equation
\begin{enumerate}
\item Find the steady state solution $U(x)$
\item Calculate the boundary and initial conditions for $v(x,t) = u(x,t) - U(x)$
\item Find the parameters $\alpha_n$, $A_n$ and $B_n$ of the superposed trial solution
\[
v(x, t) =\sum_{n=1}^\infty e^{-\alpha_n t} \left(A_n\sin\lambda_n x+B_n\cos\lambda_n x\right)
\]
\item Combine the steady state and transient solution to the general solution
\end{enumerate}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Boundary conditions}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Types of boundary conditions}
If $\Omega$ is the domain on which the given equation is to be solved and $\partial\Omega$ denotes its boundary, we can have
\begin{itemize}
\item Dirichlet 
\[u\text{ is specified on } \partial\Omega\]
\item Neumann 
\[\pderiv{u}{n}\text{ is specified on }\partial\Omega\]
\item Cauchy  
\[\text{both }u\text{ and }\pderiv{u}{n}\text{ are specified on }\partial\Omega\]
\item Robin 
\[au+B\pderiv{u}{n}=g\text{ on }\partial\Omega\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Heat conduction equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Heat conduction equation boundary conditions}
\centerline{\includegraphics[width=0.9\columnwidth]{DiffusionBC.pdf}}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Appropriateness of boundary conditions}
\centerline{\includegraphics[width=0.9\columnwidth]{BCMatrix.pdf}}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.22}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}[label=example_922]
\frametitle{Example 9.22}
Solve the one dimensional heat conduction equation
\[
\pderiv{u}{t}=\kappa\pdderiv{u}{x}
\]
\begin{itemize}
\item Subject to the boundary and initial conditions
\begin{align}
u(0,t) &= 0 & \forall t\ge0 \label{eq:left_BC} \\
u(1,t) &= 1 & \forall t\ge0 \label{eq:right_BC} \\
u(x,0) &= x(2-x) & 0\le x \le 1 \label{eq:IC}
\end{align}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Parabolic-AP4a: Steady state solution}
\vskip-0.cm
\begin{block}{What is the steady state solution of problem 9.22 for $t\to\infty$?}
\centering{
%\includegraphics[width=5.cm]{orthogonal_sine_cosine_3}
}
\vskip-0.1cm
\begin{itemize}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.2cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
  \centerline{\includegraphics[width=6cm]{heat_steady_state2}}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\begin{itemize}
\item Solution 2 is correct: at $t\to\infty$ the initial disturbance will have dispersed and the solution will be a linear temperature profile between the two boundary values
\end{itemize}
\vskip-0.2cm
\centerline{\includegraphics[width=4.2cm]{heat_steady_state2}}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Parabolic-AP4b: Transient solution initial condition}
\vskip-0.cm
\begin{block}{What is the initial condition for the transient solution of example 9.22?}
\begin{itemize}
\item The steady state solution is $U(x) = x$ 
\item This satisfies the first two boundary conditions and also $\nabla^2 U=0$
\item The transient solution $v=u - U$ needs to satisfy the new boundary conditions
\vskip-0.7cm
\begin{align*}
u(0,t) &=0 &\implies v(0,t) &= u(0,t) - U(0) = 0 \\
u(1,t) &= 1 &\implies v(1,t) &= u(1,t) - U(1) = 0 \\
u(x,0) &= x(2-x) &\implies v(x,0) &= ?
\end{align*}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.5cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item \(v(x,0) = x - 2x^2\)
\item \(v(x,0) = 2x\)
\item \(v(x,0) = x - x^2\)
\item \(v(x,0) = - x^2\)
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\begin{block}{Solution}
\skip-0.3cm
\begin{enumerate}
\item Solution 3 is correct: 
\begin{align*}
u(x,0) &= x(2-x) &\implies v(x,0) &= x(2-x) - U(x) \\
& & &= 2x-x^2 - x = x - x^2
\end{align*}
\end{enumerate}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
%-------------------------------------------------------%
\subsection{Well-posed parabolic PDE}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Well-posed parabolic PDE}
\begin{itemize}
\item The boundary is open
\begin{itemize}
\item Not all boundaries have a condition attached
\end{itemize}
\item The other boundaries have Dirichlet or Neumann conditions
\end{itemize}
\vskip0.2cm
\centerline{\includegraphics[width=0.6\columnwidth]{DiffusionBC.pdf}}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Laplace transform method}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}