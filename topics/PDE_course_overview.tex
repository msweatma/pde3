\documentclass{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Course overview}
\author[Daniel Friedrich]{David Ingram and Daniel Friedrich}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Learning outcomes}
% -------------------------------------------------------- %
\begin{frame}{Learning outcomes}
\begin{enumerate}
\item A sound basis in partial differential equations to gain understanding of the properties of partial differential equations and their solutions 
\item Practice in applying analytical methods for the solution of partial differential equations
\item Basic understanding of the workings and limitations of numerical methods for the solution of partial differential equations
\item Practice in the numerical solution of the three different types of partial differential equations using Matlab
\end{enumerate}
\end{frame}

% -------------------------------------------------------- %
\section{Organisation}
% -------------------------------------------------------- %
\begin{frame}{Course organisation}
\begin{itemize}
\item Class times
\begin{itemize}
\item Lecture 1, Monday 15:10, LT B, JCMB
\item Lecture 2, Wednesday 12:10, LT B, JCMB
\end{itemize}
\item Tutorials
\begin{itemize}
\item You will be assigned to one of two tutorial slots which start in week 2
\item Tutorial group 1, Monday 12:10-13:00, TLC, Alrick
\item Tutorial group 2, Tuesday 12:10-13:00, TLC, Alrick
\end{itemize}
\item Recorded lectures will be avaiable through Media Hopper
\item Preparation for each week: see \WeeklyTarget
\item Familiarity with material from Engineering Mathematics 2A and 2B is expected
\item Learning by doing and active participation is expected
\end{itemize}

\begin{block}{Lecturers}
\begin{itemize}
\item \href{https://www.eng.ed.ac.uk/about/people/prof-david-m-ingram}{Prof David Ingram}: Numerical methods on Wednesdays
\item \href{https://www.eng.ed.ac.uk/about/people/dr-daniel-friedrich}{Dr Daniel Friedrich}: Analytical methods on Mondays
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\section{Weekly targets}
% -------------------------------------------------------- %
\begin{frame}{Weekly course preparation}
The \WeeklyTarget give
\begin{itemize}
\item Learning aims for each week
\item Pre-class preparation
\begin{itemize}
\item Sections in the text books
\item Additional sources
\end{itemize}
\item An weekly, open feedback question
\item I will look through the feedback to address difficult, interesting, challenging topics
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}{Be an active participant}
\begin{columns}
\column{.46\textwidth}
\includegraphics[width=5.2cm]{teachermemes_silent}\\
\tiny{www.teachermemes.com/participation.html}
\column{.54\textwidth}
\begin{itemize}
\item Do the pre-class assignment
\item Engage in the discussion
\item Ask and answer some questions
\item There is no shame in giving a wrong answer
\item Let me know if something is not clear
\end{itemize}
\includegraphics[width=5.5cm]{minions}
\end{columns}
\end{frame}

% -------------------------------------------------------- %
\section{Assessment}
% -------------------------------------------------------- %
\begin{frame}{Assessment - coursework}
\begin{block}{Coursework 40$\%$}
\begin{itemize}
\item Two mechanical engineering based problems to be submitted electronically to the ETO
\item Each coursework is worth 20$\%$
\item Coursework topics
\begin{enumerate}
\item Static PDE, due in week 6
\item Transient PDE, due in week 9
\end{enumerate}
\item Marks will be awarded for a correct solution and for mathematical style and presentation
\item Failing to submit a problem on time will have serious consequences and could lead to you failing the course.
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}{Assessment - exam}
\begin{block}{Exam 60$\%$}
\begin{itemize}
\item 2 hour exam sat in December
\item Two questions each for the analytical and numerical part of the course
\item You need to solve 3 of these 4 questions
\item The questions for the analytical part will be similar to those from previous years 
\item Previous exams from Engineering Mathematics 2A and 2B, Mathematics for Science and Engineering 2B and from Mathematics for Electrical and Mechanical Engineers and Mathematics for Chemical Engineers will be useful.
\item Example exam questions for the numerical part will be made available on Learn
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\section{Syllabus}
% -------------------------------------------------------- %
\begin{frame}{Syllabus}
\begin{columns}
\column{.15\textwidth}
\column{.85\textwidth}
\begin{enumerate}
\item[Week 1] Introduction to PDEs
\item[Week 2-4] Elliptic PDEs
\item[Week 5-7] Parabolic PDEs
\item[Week 8-10] Hyperbolic PDEs
\end{enumerate}
\end{columns}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Books}
\begin{block}{Textbooks}
\begin{itemize}
\item Gyln James: Advanced Modern Engineering Mathematics
\end{itemize}
\end{block}
\vskip0.1cm
\begin{block}{Comments}
\begin{itemize}
\item The material for the analytical solution follows Chapter 9 (pages 724--802) of Advanced Modern Engineering Mathematics, by Gyln James. You are strongly recommended to work through the exercises in this section of the book and to read and practice the material as it is taught. 
\item We will deal with the analytical solution of partial differential equations (PDEs) and with finite difference methods. 
\item The extended reading list can be found \href{https://www.learn.ed.ac.uk/webapps/blackboard/content/listContentEditable.jsp?content_id=_3880561_1&course_id=_73805_1&mode=reset}{here}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\section{TopHat}
% -------------------------------------------------------- %
\begin{frame}{TopHat}
During the lectures we will make use of the TopHat voting system. The voting system works with any personal electronic device which can access the internet, i.e. smartphone, tablet or laptop.

\begin{itemize}
\item To configure your device to access the University's wireless network see the information about the \href{http://www.ed.ac.uk/information-services/computing/desktop-personal/wireless-networking}{eduroam wireless network}
\item To set up your TopHat acccount, follow \href{http://www.ed.ac.uk/information-services/learning-technology/electronic-voting-system/students/account}{the instructions here}
\item Once you have a TopHat account, follow \href{http://www.ed.ac.uk/information-services/learning-technology/electronic-voting-system/students/adding}{these instructions} to join the Partial Differential Equations 3 course. The code is \textbf{640923} and here is a link to \TopHat
\item If you are using an Android or iOS device, we recommend using the \href{http://www.ed.ac.uk/information-services/learning-technology/electronic-voting-system/students/mobile-apps}{custom application} as it will make it quicker and easier for you to log in during class
\item Remember to bring your device to each class
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Matlab}
% -------------------------------------------------------- %
\begin{frame}{Matlab refresh}
\begin{itemize}
\item We will use Matlab throughout this course
\item You need to be comfortable using it
\item The School of Engineering has an interactive Matlab introduction
\begin{itemize}
\item Available at \href{http://www.matlab.eng.ed.ac.uk/}{\beamergotobutton{www.matlab.eng.ed.ac.uk}}
\end{itemize}
\item Matlab is available on the computers in the teaching labs
\item Alternatively you can use Octave 
\begin{itemize}
\item Most Matlab scripts run with no or only small modifications in Octave
\item Available for free at \href{https://www.gnu.org/software/octave/}{\beamergotobutton{www.gnu.org/software/octave}}
\item Also available for most Android devices
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
% -- Appendix -------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}