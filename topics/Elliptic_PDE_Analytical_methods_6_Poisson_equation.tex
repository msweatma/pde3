\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Elliptic PDE Part 6: Poisson's equation}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Solve Poisson's equation for easy right hand sides
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Solve the Laplace equation with the method of separation of variables
\end{itemize}
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Recap}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Complex variable solution}
\begin{itemize}
\item The real and imaginary parts of a \textcolor{blue}{differentiable} function, $f(z)$, of the complex variable $z=x+iy$ automatically satisfy the Laplace equation
\item The Joukowski transform can be used to map a circle onto an airfoil
\centerline{\includegraphics[width=0.28\columnwidth]{Joukowsky_transform1}
\includegraphics[width=0.55\columnwidth]{Joukowsky_transform2}}
\item It is this method that was used to find the potential flow solutions needed to design airfoils before the advent of the digital computer
\vskip0.0cm
\centering\includegraphics[width=0.5\columnwidth,]{StreamLines.pdf}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Poisson's equation}
% -------------------------------------------------------- %
\subsection{Introduction}
% -------------------------------------------------------- %
\begin{frame}\frametitle{Poisson's equation}
\begin{block}{The Poisson equation is}
\[ 
\nabla^2 u = f
\]
where \(u=u(x,y,z)\) and \(f=f(x,y,z)\) for the 3D case.
\end{block}
\vskip0.2cm
\begin{itemize}
\item It is the Laplace equation with a scalar \textcolor{red}{Source} or \textcolor{blue}{Sink} term
\item Source terms allow the addition or removal of $u$ inside the solution domain and are a very important part of modelling real engineering problems
\item Source terms can occur in all types of PDE, but normally make the problem much harder to solve
\item We will develop the solution procedure by going step-by-step through an example
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.33}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}\frametitle{Example 9.33}
\begin{block}{Solve the Poisson equation}
\[\pdderiv{u}{x}+\pdderiv{u}{y}=-\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}\]
over the rectangle $0\le x\le a,\,0\le y\le b$ given the boundary conditions 
\begin{align*}
u(0,y)&=0 \quad &0\le y\le b\\
u(a,y)&=f(y)&0\le y\le b\\
\pderiv{u(x,0)}{y}&=0&0\le x\le a\\
\pderiv{u(x,b)}{y}&=0&0\le x\le a
\end{align*}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}\frametitle{Find a particular integral}
\begin{itemize}
\item The approach here is to find a \textcolor{red}{particular integral} to eliminate the RHS, then to compute new boundary conditions and then solve the residual Laplace equation
\item The right hand side of Poisson's equation is given by trigonometric functions
\[\pdderiv{u}{x}+\pdderiv{u}{y}=-\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}\]
\item This suggests a trigonometric particular integral and we choose
\[
U(x,y) = K\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}
\]
\item Calculate the second partial derivatives
\begin{align*}
    \pdderiv{U}{x} & = -K \frac{\pi^2}{a^2}\sin\frac{\pi x}{a}\cos\frac{\pi y}{b} \\
    \pdderiv{U}{y} & = -K \frac{\pi^2}{b^2}\sin\frac{\pi x}{a}\cos\frac{\pi y}{b} 
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}\frametitle{Fix the parameters of the particular integral}
\begin{itemize}
\item Substituting into the Poisson equation
\[\grad^2U=-K\left(\frac{\pi^2}{a^2}+\frac{\pi^2}{b^2}\right)\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}\]
\item By comparing to the right hand side we get
\[K^{-1} = \left(\frac{\pi^2}{a^2}+\frac{\pi^2}{b^2}\right)\]
\item This gives the particular integral
\[
U(x,y) = \left(\frac{\pi^2}{a^2}+\frac{\pi^2}{b^2}\right)^{-1}\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}
\]
\item We now write $u=U+v$ so that $v$ satisfies 
\[\grad^2v = \grad^2 u - \grad^2 U = 0\]
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Elliptic-AP6a: Boundary conditions of the Poisson equation}
\vskip-0.cm
\begin{block}{What are the boundary conditions for $\grad^2v=0$?}
\centering{
%\includegraphics[width=5.cm]{orthogonal_sine_cosine_3}
}
\vskip-0.1cm
\begin{itemize}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item The same as for $\grad^2u=0$
\item Boundary conditions for $u$ multiplied by $K$
\item Boundary conditions for $u$ minus $K$
\item No idea
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\skip-0.5cm
\begin{itemize}
\item In this case the modified boundary conditions remain the same
\begin{align*}
v(0,y)&=0&0\le y\le b\\
v(a,y)&=f(y)&0\le y\le b\\
\pderiv{v(x,0)}{y}&=0&0\le x\le a\\
\pderiv{v(x,b)}{y}&=0&0\le x\le a
\end{align*}
\item Why are the boundary conditions the same?
\end{itemize}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Elliptic-AP6b: Extreme point of the Poisson equation}
\vskip-0.cm
\begin{block}{Where can the extreme points, e.g. maximum or minimum, of the solution to the Poisson equation be located?}
\centering{
%\includegraphics[width=5.cm]{orthogonal_sine_cosine_3}
}
\vskip-0.1cm
\begin{itemize}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item On the boundary
\item Inside the solution domain
\item Anywhere
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\skip-0.5cm
\begin{enumerate}
\item[3.] Anywhere
\begin{itemize}
\item A heat source can lead to higher temperatures on the inside
\item A heat sink can lead to lower temperatures on the inside
\end{itemize}
\end{enumerate}
\end{block}
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
%-------------------------------------------------------%
\subsection{Poisson's equation}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Procedure to solve Poisson's equation}
\begin{itemize}
\item The Poisson equation is given by
\[ 
\nabla^2 u = f
\]
\item It is the Laplace equation with a scalar \textcolor{red}{Source} or \textcolor{blue}{Sink} term
\end{itemize}
\vskip0.1cm
\begin{block}{Procedure}
\begin{enumerate}
\item Find a particular integral that satisfies the right hand side
\begin{itemize}
\item Remember the rules from EM2A for particular integrals
\item Not always easy to find the particular solution
\item The resulting Laplace equation can have very difficult boundary conditions
\end{itemize}
\item Solve the resulting Laplace equation
\item Add the solution of the Laplace equation and the particular integral to get the solution of the Poisson's equation
\end{enumerate}
\end{block}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Cylindrical coordinates}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}