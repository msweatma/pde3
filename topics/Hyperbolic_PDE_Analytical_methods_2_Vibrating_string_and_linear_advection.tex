\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Hyperbolic PDE -- Part 2: Vibrating string and linear advection}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Show that a given expression is a solution to the wave equation
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Derive the wave equation in one spatial dimension
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Recap}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{The wave equation}
\vskip0.1cm
\centerline{\includegraphics[width=0.8\columnwidth]{StringElement}}
\vskip-0.2cm
\[\frac{1}{c^2}\pdderiv{u}{t}=\pdderiv{u}{x}+\pdderiv{u}{y}+\pdderiv{u}{z}=\grad^2u\]
\vskip-0.1cm
\begin{itemize}
\item It can model sound waves, shock wave, electromagnetic waves, water waves, a plucked string, etc.
\item The one dimensional form was discovered by the French scientist Jean-Baptiste le Rond d'Alembert (1717--1783)
\item The equation is said to be \textcolor{red}{hyperbolic} in character and has the nasty property that discontinuities in $u$, or its derivative, $\frac{\partial u}{\partial x}$,  will propagate in time
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Vibrating string}
% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fundamental mode of a vibrating string 1/2}
\textbf{Problem:} Show that
\[u(x,t)=u_0\sin\frac{\pi x}{L}\cos\frac{\pi c t}{L}\]
satisfies both the wave equation and the initial conditions
\[
u(x,0)=u_0\sin\frac{\pi x}{L}\text{ and }
\left.\pderiv{u}{t}\right|_{t=0}=0
\]
\begin{itemize}
\item Differentiate $u(x,t)$ with respect to $t$ to get
\[u_t(x,t)=\pderiv{u}{t}=-\frac{u_0\pi c}{L}\sin\frac{\pi x}{L}\sin\frac{\pi c t}{L}\]
\item Clearly $u_t(x,0)=0$ and $u(x,0)$ satisfy the initial conditions
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fundamental mode of a vibrating string 2/2}
\begin{itemize}
\item We compute the 2nd partial derivatives of $u(x,t)$,
\[
u_{xx}(x,t)=\pdderiv{u}{x} = -\frac{u_0\pi^2}{L^2}\sin\frac{\pi x}{L}\cos\frac{\pi c t}{L}
\]
\[
u_{tt}(x,t)=\pdderiv{u}{t} = -\frac{u_0\pi^2c^2}{L^2}\sin\frac{\pi x}{L}\cos\frac{\pi c t}{L}
\]
\item Clearly we have
\[
u_{tt} = c^2u_{xx}
\]
so the one dimensional wave equation is satisfied
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Plot of the fundamental mode of a vibrating string with $c=1,\,L=1,u_0=1$}
\begin{tikzpicture}[scale=0.93]
 \begin{axis}[legend style={draw=none},
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black,domain=0:1, samples=101] {sin(x*180)};
  \addplot[blue, dotted, line width=1.5, domain=0:1, samples=101]{sin(180*x)*cos(45)};
  \addplot[cyan, dashed, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(90)};
  \addplot[green,dashdotted, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(135)};
  \addplot[red,loosely dotted, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(180)};
  \legend{$t=0.00$s,$t=0.25$s,$t=0.50$s,$t=0.75$s,$t=1.00$s}
 \end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\section{Linear advection}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Linear advection problem}
\textbf{Problem:} Show that
\[
u(x,t) = a\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right]
\]
satisfies the wave equation
\vskip0.1cm
\begin{itemize}
\item Differentiate $u(x,t)$ twice with respect to $x$
{\small
\begin{align*}
u_x(x,t) &= \frac{-2a(x-ct)}{h^2}\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right] \\
u_{xx}(x,t) &= \frac{-2a}{h^2}\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right]
+\frac{4a(x-ct)^2}{h^4}\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right]
\end{align*}
}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution for linear advection problem}
\begin{itemize}
\item Twice differentiating $u(x,t)$ with respect to $t$ gives us
{\small
\begin{align*}
u_t(x,t) &= \frac{2a(x-ct)c}{h^2}\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right] \\
u_{tt}(x,t) &= \frac{-2a c^2}{h^2}\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right]
+\frac{4a(x-ct)^2c^2}{h^4}\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right]
\end{align*}
}
\item Comparing $u_{tt}$ and $u_{xx}$ it is clear that
\[
u_{tt} = c^2u_{xx}
\]
\item Thus, this is also a solution of the one dimensional wave equation
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Plot of the solution}
\begin{tikzpicture}[scale=0.95]
 \begin{axis}[legend style={draw=none},
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black,domain=-2:10, samples=101] {exp(-x^2)};
  \addplot[blue,dotted, line width=1.5,domain=-2:10, samples=101] {exp(-(x-2)^2)};
  \addplot[cyan,dashed, line width=1.5,domain=-2:10, samples=101] {exp(-(x-4)^2)};
  \addplot[magenta,loosely dotted, line width=1.5,domain=-2:10, samples=101] {exp(-(x-8)^2)};

  \legend{$t=0$,$t=\frac{2h}{c}$,$t=\frac{4h}{c}$,$t=\frac{8h}{c}$}
 \end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{A geometric solution using the $x-t$ diagram}
Instead of plotting the solution $u(x,t)$, we can plot the location of the start, end and peak of the curve, against time.
\vskip0.2cm
\begin{tikzpicture}
 \begin{groupplot}[group style={group size=1 by 2},
             height=0.4\textheight,width=0.95\columnwidth,
             axis lines=left,xlabel=$x$]
  \nextgroupplot[ylabel=$t$]
  \addplot[blue, domain=0:8] {x};
  \addplot[magenta,loosely dotted, line width=1.5, domain=2:10] {x-2};
  \addplot[magenta,loosely dotted, line width=1.5, domain=-2:6] {x+2};
  \addplot[black, domain=-2:10]{6};
  \addplot[magenta,loosely dotted, line width=1.5] coordinates {(4,0) (4,6)};
  \addplot[magenta,loosely dotted, line width=1.5] coordinates {(8,0) (8,6)};
  \addplot[blue] coordinates {(6,0) (6,6)};

   \nextgroupplot[ylabel=$u$, ymax=1]
  \addplot[red,domain=-2:10, samples=101, ylabel=$u$] {exp(-(x-6)^2)};
  \addplot[magenta,loosely dotted, line width=1.5] coordinates {(4,0) (4,1)};
  \addplot[magenta,loosely dotted, line width=1.5] coordinates {(8,0) (8,1)};
  \addplot[blue] coordinates {(6,0) (6,1)};
 \end{groupplot}
\end{tikzpicture}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
%-------------------------------------------------------%
\subsection{Wave equation examples}
%-------------------------------------------------------%
\begin{frame}
\frametitle{Wave equation examples}
\begin{itemize}
\item The fundamental mode of a vibrating string is given by
\[u(x,t)=u_0\sin\frac{\pi x}{L}\cos\frac{\pi c t}{L}\]
\item A linear advection solution is given by
\[
u(x,t) = a\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right]
\]
\end{itemize}
\begin{tikzpicture}
 \begin{groupplot}[group style={group size=1 by 2},
             height=0.4\textheight,width=0.95\columnwidth,
             axis lines=left,xlabel=$x$]
  \nextgroupplot[ylabel=$t$]
  \addplot[blue, domain=0:8] {x};
  \addplot[magenta,loosely dotted, line width=1.5, domain=2:10] {x-2};
  \addplot[magenta,loosely dotted, line width=1.5, domain=-2:6] {x+2};
  \addplot[black, domain=-2:10]{6};
  \addplot[magenta,loosely dotted, line width=1.5] coordinates {(4,0) (4,6)};
  \addplot[magenta,loosely dotted, line width=1.5] coordinates {(8,0) (8,6)};
  \addplot[blue] coordinates {(6,0) (6,6)};
 \end{groupplot}
\end{tikzpicture}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{D'Alembert solution}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}