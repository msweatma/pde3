\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Elliptic PDE Example 4: Poisson's equation}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Example 9.33}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}\frametitle{Example 9.33}
\begin{block}{Solve the Poisson equation}
\[\pdderiv{u}{x}+\pdderiv{u}{y}=-\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}\]
over the rectangle $0\le x\le a,\,0\le y\le b$ given the boundary conditions 
\begin{align*}
u(0,y)&=0&0\le y\le b\\
u(a,y)&=f(y)&0\le y\le b\\
\pderiv{u(x,0)}{y}&=0&0\le x\le a\\
\pderiv{u(x,b)}{y}&=0&0\le x\le a
\end{align*}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}\frametitle{Find a particular integral}
\begin{itemize}
\item The approach here is to find a \textcolor{red}{particular integral} to eliminate the RHS, then to compute new boundary conditions and then solve the residual Laplace equation
\item In the present case we choose
\[
U(x,y) = K\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}
\]
\item Calculate the second partial derivatives
\begin{align*}
    \pdderiv{U}{x} & = -K \frac{\pi^2}{a^2}\sin\frac{\pi x}{a}\cos\frac{\pi y}{b} \\
    \pdderiv{U}{y} & = -K \frac{\pi^2}{b^2}\sin\frac{\pi x}{a}\cos\frac{\pi y}{b} 
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}\frametitle{Fix the parameters of the particular integral}
\begin{itemize}
\item Substituting into the Poisson equation
\[\grad^2U=-K\left(\frac{\pi^2}{a^2}+\frac{\pi^2}{b^2}\right)\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}\]
\item By comparing to the right hand side we get
\[K^{-1} = \left(\frac{\pi^2}{a^2}+\frac{\pi^2}{b^2}\right)\]
\item This gives the particular integral
\[
U(x,y) = \left(\frac{\pi^2}{a^2}+\frac{\pi^2}{b^2}\right)^{-1}\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}\frametitle{Define the remaining Laplace equation}
\begin{itemize}
\item With the particular integral
\[
U(x,y) = \left(\frac{\pi^2}{a^2}+\frac{\pi^2}{b^2}\right)^{-1}\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}
\]
\item We now write $u=U+v$ so that $v$ satisfies \pause
\[\grad^2v = \grad^2 u - \grad^2 U = 0\]
\item In this case the modified boundary conditions remain the same
\begin{align*}
v(0,y) &= u(0,y) - U(0,y) = 0 & 0\le y\le b\\
v(a,y) &= u(a,y) - U(a,y) = f(y) & 0\le y\le b\\
\pderiv{v(x,0)}{y} &= \pderiv{u(x,0)}{y} - \pderiv{U(x,0)}{y} = 0 & 0\le x\le a\\
\pderiv{v(x,b)}{y} &= \pderiv{u(x,b)}{y} - \pderiv{U(x,b)}{y} = 0 & 0\le x\le a
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solve the resulting Laplace equation}
\begin{itemize}
\item We now have a \textsl{standard} Laplace equation problem which can be solved by separation of variables
\item The derivative of the solution with respect to $y$ is zero for two values of $y$ which can be fulfilled by the following basic solutions
\vskip-0.7cm
\begin{align*}
v_0(x,y) &= (A_0 x + B_0) (C_0 y + D_0) \\
v_n(x,y) &=\left(A_n\cosh \mu_n x + B_n\sinh \mu_n x\right)(C_n\sin \mu_n y+D_n\cos \mu_n y), \quad n=1,2,\dots 
\end{align*}
\item Applying the first three boundary conditions we obtain the usual series solution \pause
\[v(x,y) = \frac12 A_0x+\sum_{n=1}^\infty A_n\sinh\frac{n\pi x}{b}\cos\frac{n\pi y}{b}\]
with parameters $A_n$ for the different $\mu_n = \frac{n\pi}{b}$
\end{itemize}
\vskip2cm
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Calculate the Fourier series coefficients}
\begin{itemize}
\item Applying $v(a,y)=f(y)$ we have \pause
\[f(y) = \frac12A_0 a + \sum_{n=1}^\infty A_n\sinh\left(\frac{a n \pi}{b}\right)\cos\frac{n\pi y}{b}\]
\item We must now solve the general Fourier problem to find the unknown coefficients, using the usual methods we have
\begin{align*}
a_0 &= A_0a=\frac2b\int_0^bf(y)\text{ d}y\\
a_n &= A_n\sinh\frac{a}{b}n\pi=\frac2b\int_0^bf(y)\cos\frac{n\pi y}{b}\text{ d}y&n=1,2,3,\ldots
\end{align*}
\item We can calculate $a_0$ and $a_n$ for any function $f(y)$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Assemble complete solution}
\begin{itemize}
\item Once we have calculated the $a_0$ and $a_n$ we get
\[v(x,y) = \frac{a_0 x}{a} + \sum_{n=1}^\infty \frac{a_n}{\sinh\left(\frac{a}{b}n \pi\right)}\sinh\frac{n\pi x}{b}\cos\frac{n\pi y}{b}
\]
\item The complete solution is given by adding $v$ and the particular solution
\textcolor{red}{
\[
u(x,y) = U(x,y) + v(x,y) = \frac{\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}}{\left(\frac{\pi^2}{a^2}+\frac{\pi^2}{b^2}\right)}
+\frac{a_0x}{a}
+\sum_{n=1}^\infty \frac{a_n}{\sinh\frac{a}{b}n\pi}\sinh\frac{n\pi x}{b}\cos\frac{n\pi y}{b}
\]}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Thank you}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}