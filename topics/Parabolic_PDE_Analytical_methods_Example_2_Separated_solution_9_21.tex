\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Parabolic PDE Example 2: Separated solution example 9.21}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Example 9.21}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}[label=example_921]
\frametitle{Problem definition}
Solve the one dimensional heat conduction equation
\[
\pderiv{T}{t} = \kappa\pdderiv{T}{x}
\]
\begin{itemize}
\item Subject to the boundary and initial conditions
\begin{align}
T(0,t)&=0&\forall t > 0 \label{eq:left_BC} \\
T(l,t)&=0&\forall t > 0 \label{eq:right_BC} \\
T(x,0)&=T_0\left(\frac12-\frac{x}{l}\right)&0\le x \le l \label{eq:IC} 
\end{align}
\item There is a discontinuity in the initial and boundary conditions
\item The boundary conditions are constant and there are no source terms so the system approaches a steady state solution
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Using the left and right boundary conditions}
\begin{itemize}
\item As before we are seeking solutions of the form
\[
T(x,t) = e^{-\alpha t} \left(A\sin\lambda x+B\cos\lambda x\right)
\]
\item The left boundary condition~\ref{eq:left_BC} is
\[
T(0,t) = e^{-\alpha t} \left(A\sin(\lambda 0) + B\cos(\lambda 0)\right) \overset{!}{=} 0
\]
which is only fulfilled for $B=0$
\item The right boundary condition~\ref{eq:right_BC} is
\[
T(l,t) =  e^{-\alpha t} A\sin(\lambda l) \overset{!}{=} 0
\]
\pause
which is only fulfilled for
\begin{align*}
\sin\lambda l=0,\text{ or }\lambda l&=n\pi,\quad n=1,2,3,\ldots \\
\lambda &= \frac{n\pi}{l}
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fit the initial condition}
\begin{itemize}
\item Recalling that $\lambda^2=\alpha/\kappa$ we are seeking solutions of the form
\[
T(x,t) = A\exp\left(-\frac{\kappa n^2\pi^2 t}{l^2}\right)\sin\frac{n\pi x}{l},\ n=1,2,3,\ldots
\]
\item This time we cannot satisfy the initial conditions from a single solution, so we need to use superposition
\[
T(x,t) = \sum_{n=1}^\infty A_n\exp\left(-\frac{\kappa n^2\pi^2 t}{l^2}\right)\sin\frac{n\pi x}{l}
\]
\item Applying the initial condition we get
\[
T(x, t=0) = \sum_{n=1}^\infty A_n\sin\frac{n\pi x}{l} \overset{!}{=} T_0\left(\frac12-\frac{x}{l}\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Calculate the Fourier series coefficients}
\begin{itemize}
\item Using the half-range Fourier sine series we find the coefficients $A_n$
\[
A_n = \frac{2T_0}{l}\int_0^l \left(\frac12-\frac{x}{l}\right)\sin\frac{n\pi x}{l}\text{ d}x=
\begin{cases}
0 & \text{odd }n \\
\frac{2T_0}{n\pi} & \text{even }n
\end{cases}
\]
\item The following integrals can be useful in calculating Fourier series coefficients
\begin{align*}
\int x\sin(ax)dx &= \frac{1}{a^2}\left(\sin(ax) - ax \cos(ax)\right) \\
\int x\cos(ax)dx &= \frac{1}{a^2}\left(\cos(ax) + ax \sin(ax)\right) 
\end{align*}
\item Similar relations exist for $x^2 \sin(ax)$ and $x^2 \cos(ax)$
\end{itemize}
\vskip1.5cm
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Complete solution}
\begin{itemize}
\item Substituting the $A_n$ back into the solution and using $2m=n$ we get the solution
\[
T(x,t) = \frac{T_0}{\pi} \sum_{m=1}^\infty \frac1{m}\exp\left(-m^2\frac{4\kappa \pi^2}{l^2} t\right)\sin\left(\frac{2m\pi x}{l}\right)
\]
\item We can plot the solution at the scaled time
\[
\tau = \frac{4\kappa\pi^2}{l^2} t
\] 
using the dimensionless temperature $T/T_0$ and the dimensionless length $x/l$
\item We are only interested in the solution over the interval, $0\le x \le l$ and although $T$ can be computed for values of $x$ outside this range it has no physical meaning
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Plot solution at different, scaled times $\tau = \frac{4\kappa\pi^2}{l^2}t$}
\begin{tikzpicture}
  \begin{axis}[legend style={draw=none}, 
  xlabel=$x/l$,ylabel=$T/T_0$]
% t=0.0
 \addplot[black,domain=0:1, samples=3] {0.5-x};
% t=0.5
  \addplot[cyan,dashed, domain=0:1, samples=201] 
  {(exp(-0.5)*sin(360*x)+exp(-4*0.5)*sin(2*360*x)/2+exp(-9*0.5)*sin(3*360*x)/3+exp(-16*0.5)*sin(4*360*x)/4)
  /3.141592};
% t=1.0
   \addplot[blue,dotted, line width=1.5,domain=0:1, samples=201] 
  {(exp(-1.0)*sin(360*x)+exp(-4*1.0)*sin(2*360*x)/2+exp(-9*1.0)*sin(3*360*x)/3+exp(-16*1.0)*sin(4*360*x)/4)
  /3.141592};
% t=1.5
  \addplot[red,dashdotted, line width=1.5,domain=0:1, samples=201] 
  {(exp(-1.5)*sin(360*x)+exp(-4*1.5)*sin(2*360*x)/2+exp(-9*1.5)*sin(3*360*x)/3+exp(-16*1.5)*sin(4*360*x)/4)
  /3.141592};

  \legend{$\tau=0.0$,$\tau=0.5$,$\tau=1.0$,$\tau=1.5$}
\end{axis}
   \end{tikzpicture}
\begin{itemize}
\item Showing 4 terms of the Fourier series
\end{itemize}
%\vskip0.1cm
%\hyperlink{general_form}{\beamergotobutton{Back to general solution form}}
\end{frame}

%-------------------------------------------------------%
\section{Thank you}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}