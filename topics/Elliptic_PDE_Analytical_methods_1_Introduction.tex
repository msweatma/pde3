\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Elliptic PDE Part 1: Introduction}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

%-------------------------------------------------------%
\section{Part overview}
%-------------------------------------------------------%
\subsection{Learning outcomes}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Learning outcomes and prerequisites}
\vskip-0.1cm
\begin{block}{Learning outcomes}
After this part, you should be able to:
\begin{itemize}
\item Derive the Laplace equation in 2D
\end{itemize}
\end{block}
\vskip0.3cm
\begin{block}{Prerequisites}
Before this part, you should be able to:
\begin{itemize}
\item Distinguish ordinary and partial differential equations and their properties
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\section{Laplace equation}
% -------------------------------------------------------- %
\subsection{General form}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace Equation}
The Laplace equation is defined by
\vskip-0.2cm
\[
\grad^2 u(x,y) =0
\]

\begin{itemize}
\item It was first studied by Pierre-Simon Laplace (1749--1827), of whom you have already heard!
\item It is an \textcolor{red}{elliptic} equation which models the steady state of a diffusion like process.  
\begin{itemize}
\item We will discuss the formal classification of PDEs later in this course
\end{itemize}
\item We can use it to describe the pressure in an incompressible flow, steady state diffusion and heat conduction, stress and strain, etc.
\item In 2D the Laplace operator $\Delta$ is given by
\vskip-0.2cm
\[
\Delta = \grad \cdot \grad = \grad^2 = \pdderiv{ }{x} + \pdderiv{ }{y} 
\]
\item It is closely related to the Poisson equation 
\vskip-0.2cm
\[\grad^2u=f(x,y)\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Derivation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Problem statement}
\begin{itemize}
\item Consider a flat, rectangular sheet of thermally conductive material
\item The left side is heated while the other sides are keep cold
\item There is no heat transfer in the z direction
\item There is no source or sink inside the sheet
\item Find the steady state temperature distribution in the sheet
\end{itemize}
\centerline{\includegraphics[width=0.45\columnwidth]{Laplace_derivation.png}}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Temperature in a small element}
\centerline{\includegraphics[width=0.5\columnwidth]{Laplace_derivation3.png}}
\begin{itemize}
\item Consider the small element between $x_0$, $x_1$, $y_0$ and $y_1$
\item There are four heat flow terms at its boundaries
\item The temperature inside is not changing over time, i.e. $\pderiv{T}{t}=0$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Assumptions}
\begin{itemize}
\item Heat flows from hot regions to cold regions
\item The rate at which heat flows through a plane section drawn in a body is proportional to its area and to the temperature gradient normal to the section
\item The quantity of heat in a body is proportional to its mass, specific heat capacity and to its temperature
\item Fourier's law for the conduction of heat is
\[
Q = -k \pderiv{T}{x}
\]
\end{itemize}
\vskip3cm
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Derivation 1/2}
\centerline{\includegraphics[width=0.38\columnwidth]{Laplace_derivation3.png}}
\begin{itemize}
\item The heat flows across the boundaries need to balance because $T$ doesn't change with time
\vskip-0.2cm
\[
0 = \left[Q(x_0,y) - Q(x_1,y)\right] (y_1 - y_0) + \left[Q(x,y_0) - Q(x,y_1)\right] (x_1 - x_0)
\]
\item Divide by $(x_1 - x_0)(y_1 - y_0)$
\vskip-0.2cm
\[
0 = -\frac{Q(x_1,y) - Q(x_0,y)}{x_1 - x_0} - \frac{Q(x,y_1) - Q(x,y_0)}{y_1 - y_0}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Derivation 2/2}
\vskip-0.2cm
\centerline{\includegraphics[width=0.38\columnwidth]{Laplace_derivation3.png}}
\vskip0.1cm
\begin{itemize}
\item Reduce the square size so that $x_1 - x_0\to0$ and $y_1 - y_0\to0$
\vskip-0.2cm
\[
0 = -\pderiv{Q}{x} - \pderiv{Q}{y}
\]
\item Apply Fourier's law and divide by $k$ to get 
\vskip-0.2cm
\[
0 = \pdderiv{T}{x} + \pdderiv{T}{y} = \grad^2 T
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Contour plot of the solutionLaplace equation}
\centerline{\includegraphics[width=0.7\columnwidth]{laplace_solution.png}}
\begin{itemize}
\item Contour plot for the case with heated left side while the other sides are keep cold
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
%----- Frame -------------------------------------------%
\begin{frame}{ConcepTest Elliptic-AP1: Boundary conditions}
\begin{block}{Which boundary condition does the solution}
\[
u(x,y) = e^{-y} \cos(x)
\]
of the Laplace equation on $0\le x\le 1$ and $0\le y \le 1$ fulfil?
\vskip-0.1cm
\begin{itemize}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.1cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item $u(1,y) = e^{-y}\cos(x)$ on $0\le y\le 1$
\item $u(1,y) = e^{-y}\cos(1)$ on $0\le y\le 1$
\item $u(1,y) = e^{-1}\cos(x)$ on $0\le y\le 1$
\item $u(1,y) = e^{-1}\cos(1)$ on $0\le y\le 1$
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\begin{block}{Solution}
\begin{itemize}
\item[2.] $u(1,y) = e^{-y}\cos(1)$ on $0\le y\le 1$
\begin{itemize}
\item Insert $x=1$ in the solution
\item Notice that $y$ is given between $0$ and $1$
\end{itemize} 
\end{itemize}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\section{Summary}
% -------------------------------------------------------- %
\subsection{Laplace equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace Equation}
\begin{block}{The Laplace equation is given by}
\[
\grad^2u = \pdderiv{u}{x_1} + \pdderiv{u}{x_2} + \cdots + \pdderiv{u}{x_n} = 0
\]
\end{block}
\begin{itemize}
\item It is the most fundamental \textcolor{blue}{elliptic} PDE which models the steady state of a diffusion like process
\item Used to describe the pressure in an incompressible flow, steady state diffusion and heat conduction, stress and strain, hydrostatics, electrostatics, ground water flow, etc.
\item Unlike the other PDEs the solution only involves spatial derivatives -- \textcolor{red}{there is no time dependency}
\item It is easy to find \underline{\textbf{a}} solution -- finding \underline{\textbf{the}}  solution matching the boundary conditions is the hard part
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Next part}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about
\end{itemize}
\end{itemize}

\vskip0.2cm
\begin{block}{Next part}
\begin{itemize}
\item Topic: \textbf{Method of separation of variables}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}