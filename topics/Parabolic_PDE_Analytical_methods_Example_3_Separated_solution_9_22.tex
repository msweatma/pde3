\documentclass[aspectratio=141]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import PDE3 style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Parabolic PDE Example 3: Separated solution example 9.22}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Example 9.22}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}[label=example_922]
\frametitle{Problem definition}
Solve the one dimensional heat conduction equation
\[
\pderiv{u}{t}=\kappa\pdderiv{u}{x}
\]
\begin{itemize}
\item Subject to the boundary and initial conditions
\begin{align}
u(0,t) &= 0 & \forall t\ge0 \label{eq:left_BC} \\
u(1,t) &= 1 & \forall t\ge0 \label{eq:right_BC} \\
u(x,0) &= x(2-x) & 0\le x \le 1 \label{eq:IC}
\end{align}
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Parabolic-AP4a: Steady state solution}
\vskip-0.cm
\begin{block}{What is the steady state solution of problem 9.22 for $t\to\infty$?}
\centering{
%\includegraphics[width=5.cm]{orthogonal_sine_cosine_3}
}
\vskip-0.1cm
\begin{itemize}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.2cm
\begin{overprint}
\onslide<1>
\begin{block}{Solution}
\begin{itemize}
\item Solution 2 is correct: at $t\to\infty$ the initial disturbance (Solution 3) will have dispersed and the solution will be a linear temperature profile between the two boundary values
\end{itemize}
\vskip-0.cm
\centerline{\includegraphics[width=6.2cm]{heat_steady_state2}}
\end{block}
\iftoggle{conceptest}{% Show solution
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

%-------------------------------------------------------%
\subsection{ConcepTest}
%-------------------------------------------------------%
\begin{frame}{ConcepTest Parabolic-AP4b: Transient solution initial condition}
\vskip-0.cm
\begin{block}{What is the initial condition for the transient solution of example 9.22?}
\begin{itemize}
\item The steady state solution is $U(x) = x$ 
\item This satisfies the first two boundary conditions and also $\nabla^2 U=0$
\item The transient solution $v=u - U$ needs to satisfy the new boundary conditions
\vskip-0.7cm
\begin{align*}
u(0,t) &=0 &\implies v(0,t) &= u(0,t) - U(0) = 0 \\
u(1,t) &= 1 &\implies v(1,t) &= u(1,t) - U(1) = 0 \\
u(x,0) &= x(2-x) &\implies v(x,0) &= x - x^2
\end{align*}
\item Answer and discuss on the discussion board
\end{itemize}
\end{block}
\vskip-0.5cm
\begin{overprint}
\onslide<1>
\begin{block}{Solution}
\skip-0.3cm
\begin{enumerate}
\item Solution 3 is correct: 
\begin{align*}
u(x,0) &= x(2-x) &\implies v(x,0) &= x(2-x) - U(x) \\
& & &= 2x-x^2 - x = x - x^2
\end{align*}
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show solution
\vskip-0.1cm
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Apply left and right boundary conditions}
\begin{itemize}
\item As before we are seeking solutions of the form
\[
v(x,t) = e^{-\alpha t} \left(A\sin\lambda x+B\cos\lambda x\right)
\]
\item The first boundary condition
\[
v(x=0, t) = e^{-\alpha t} \left(A\sin(\lambda 0) + B\cos(\lambda 0)\right) \overset{!}{=} 0
\]
is only fulfilled for $B=0$
\item The second boundary condition 
\[
v(x=1, t) = e^{-\alpha t} A\sin(\lambda 1)  \overset{!}{=} 0
\]
is only fulfilled for $\sin\lambda=0$ which gives
\[
\lambda=n\pi,\quad n=1,2,3,\ldots
\]
\end{itemize}
\end{frame}


% -------------------------------------------------------- %
\begin{frame}
\frametitle{Superpose and apply initial condition}
\begin{itemize}
\item To fulfil the initial condition we need to superpose the solution for all $n=1,2,3,\dots$
\[
v(x,t) = \sum_{n=1}^\infty a_n e^{-\alpha_n t} \sin(n \pi x)
\]
\item The initial condition requires
\[
v(x,t=0) = \sum_{n=1}^\infty a_n e^{-\alpha_n 0}\sin( n\pi x) \overset{!}{=} x - x^2
\]
\item Determining the Fourier coefficients in the normal 
\[
a_n = 2\int_0^1\left(x-x^2\right)\sin( n\pi x) \text{ d}x
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solve Fourier coefficient integral}
\begin{itemize}
\item The following integrals help in calculating Fourier series coefficients
\begin{align*}
\int x^2\sin(ax)dx &= \frac{2x}{a^2}\sin(ax) - \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \cos(ax) \\
\int x^2\cos(ax)dx &=  \frac{2x}{a^2}\cos(ax) + \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \sin(ax)
\end{align*}  
\item With this we can determine the Fourier coefficients 
\begin{align*}
a_n &= 2\int_0^1\left(x-x^2\right)\sin( n\pi x) \text{ d}x \\
&= 2\int_0^1 x\sin( n\pi x) \text{ d}x - 2\int_0^1 x^2\sin( n\pi x) \text{ d}x \\
&= \begin{cases}
\frac{8}{n^3 \pi^3}, & \text{odd } n \\
0, &\text{even }n
\end{cases}
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{General solution}
\begin{itemize}
\item Using $\alpha_n = \kappa\lambda_n^2$ and substituting $n=2k-1$ we get the following transient solution
\begin{align*}
v(x,t) &= \sum_{n=1}^\infty a_n e^{-\alpha_n t} \sin(n \pi x) \\
 &= \frac8{\pi^3}\sum_{k=1}^\infty\frac1{(2k-1)^3}e^{-(2k-1)^2 \kappa\pi^2t}\sin\left((2k-1)\pi x\right)
\end{align*}
\item The general solution is given by the sum of the steady state and transient solutions
\begin{align*}
u(x,t) &= U(x) + v(x,t) \\
  &= x + \frac8{\pi^3}\sum_{k=1}^\infty\frac1{(2k-1)^3}e^{-(2k-1)^2 \kappa\pi^2t}\sin\left((2k-1)\pi x\right)
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Plot of the solution at different, scaled times $\tau=\kappa\pi^2t$}

\begin{tikzpicture}
  \begin{axis}[legend style={draw=none}, 
  legend pos=south east,
  xlabel=$x$,ylabel=$u$]
% t=0.0
 \addplot[black,domain=0:1, samples=201] {x*(2-x)};
% t=0.2
  \addplot[cyan, dotted, line width=1.5, domain=0:1, samples=201] 
  {x+8*(exp(-0.2)*sin(180*x)+exp(-9*0.2)*sin(3*180*x)/27+exp(-25*0.2)*sin(5*180*x)/125)/3.141592^3};
% t=0.6
   \addplot[blue, dashed, line width=1.5, domain=0:1, samples=201] 
  {x+8*(exp(-0.6)*sin(180*x)+exp(-9*0.6)*sin(3*180*x)/27+exp(-25*0.6)*sin(5*180*x)/125)/3.141592^3};
% t=1.0
   \addplot[red, dashdotted, line width=1.5, domain=0:1, samples=201] 
  {x+8*(exp(-1.0)*sin(180*x) + exp(-9*1.0)*sin(3*180*x)/27 + exp(-25*1.0)*sin(5*180*x)/125)/3.141592^3};

  \legend{$\tau=0.0$,$\tau=0.2$,$\tau=0.6$,$\tau=1.0$}
\end{axis}
\end{tikzpicture}
\vskip0.1cm
\begin{itemize}
\item Plot of the first three terms of the general solution
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\section{Thank you}
%-------------------------------------------------------%
\subsection{Questions?}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Any questions?}
\begin{itemize}
\item Go to the Discussion Board
\begin{itemize}
\item Ask questions about this part or any other part on the forum
\item What did you find confusing or difficult about this part?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}