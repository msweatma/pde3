
% -----------------------------------------------------
% QUESTION
% -----------------------------------------------------
 \question 
 Figure QA\ref{fig:Polar} is a sketch of the regular Cartesian polar grid to be used for solving the two dimensional circular polar Laplacian,  
 \[\frac{\partial^2u}{\partial r^2}+\frac{1}{r}\frac{\partial u}{\partial r}+\frac{1}{r^2}\frac{\partial^2u}{\partial \theta^2}=0,\]
subject to the boundary conditions \(u(0,\theta)=0\), \(u(5,\theta)=5\cos\theta\) and \(\frac{\partial u}{\partial\theta}=0\) when \(\theta=0\) and \(\theta=\frac\pi2\).

 \begin{figure}[!ht]  
   \centering
   \includegraphics[width=0.7\textwidth]{Images/PolarGrid.pdf}
   \caption{$90^\circ$ circular polar grid for the solution of \(\nabla^2u(r,\theta)=0\) showing boundary conditions, \(u(0,\theta)=0\),
 \(u(5,\theta)=5\cos\theta\) and \(\frac{\partial u}{\partial\theta}=0\) when \(\theta=0\) and \(\theta=\frac\pi2\).}
   \label{fig:Polar}                             % EDIT - Create
   \end{figure}

 \begin{parts}
 \part [5] Using the Taylor series expansion of $u(x-\delta r,\theta)$  and  $u(x+\delta r,\theta)$ derive the $2^\text{nd}$ order central difference approximation to  $\frac{\partial^2 u}{\partial r^2}$.
 \droppoints
 \begin{solution}
Using Taylor series we have
 \begin{align*}
 u(r-\Delta r)&=h(r)-\Delta r\frac{\partial u}{\partial r}
+\frac{\Delta r^2}{2!}\frac{\partial^2u}{\partial r^2}
-\frac{\Delta r^2}{3!}\frac{\partial^3u}{\partial r^3}
+O(\Delta r^4)\text{ and,}\\
u(r+\Delta r)&=u(r)+\Delta r\frac{\partial u}{\partial r}
+\frac{\Delta r^2}{2!}\frac{\partial^2u}{\partial r^2}
+\frac{\Delta r^2}{3!}\frac{\partial^3u}{\partial r^3}
+O(\Delta r^4)
\end{align*}
\hfill [2]

Adding these gives
\[u(r-\Delta r)+h(r+\Delta r)=2u(r)+\Delta r^2\frac{\partial^2u}{\partial r^2}
+O(\Delta r^4)\]
\hfill[1]

Rearranging
\[\frac{\partial^2u}{\partial r^2}=\frac{u(r-\Delta r)-2u(r)+u(r+\Delta r)}{\Delta r^2}+O(\Delta r^2)\]
\hfill[2]

(one mark lost for not including the higher order terms)
 \end{solution}
\part [9] Using the appropriate $2^\text{nd}$ order accurate central difference approximations show the Gauss-Seidel update for $u_{ij}$ is,
\[u^{m+1}_{ij}=\frac{1}{2(1+\beta^2)}\left[u^{m+1}_{i-1,j}+u^{m}_{i+1,j}
+\frac{\Delta r}{2r}\left(u^m_{ij+1}-u^{m+1}_{ij-1}\right)
 +\beta^2\left(u^{m+1}_{i,j-1}+u^{m}_{i,j+1}\right)\right]\]
 where
\[\beta=\frac{\Delta r}{r\Delta \theta}\]
and $m$ is the iteration count.
\droppoints
\begin{solution}
 The PDE is
  \[\frac{\partial^2u}{\partial r^2}+\frac{1}{r}\frac{\partial u}{\partial r}+\frac{1}{r^2}\frac{\partial^2u}{\partial \theta^2}=0,\]
so we need finite difference approximations for $u_{rr},\ u_{r}$ and $u_{\theta\theta}$. 
 We have
 \begin{align*}
 0=&\frac{u_{ij-1}-2u_{ij}+u_{ij+1}}{\Delta r^2}, \\
 0=&\frac{u_{i-1j}-2u_{ij}+u_{i+1j}}{\Delta \theta^2},\text{ and}\\
 0=&\frac{u_{ij+1}-u_{ij-1}}{2\Delta r}.
 \end{align*} 
 \hfill[1]
 
 So the approximation to the PDE is
  \[\frac{u_{ij-1}-2u_{ij}+u_{ij+1}}{\Delta r^2}
  +\frac{1}{r}\frac{u_{ij+1}-u_{ij-1}}{2\Delta r}
  +\frac{1}{r^2}\frac{u_{i-1j}-2u_{ij}+u_{i+1j}}{\Delta \theta^2}=0,\]
 \hfill[2]
 
 Rearranging
 \[ \frac{2u_{ij}}{\Delta r^2}+\frac{1}{r^2}\frac{2u_{ij}}{\Delta \theta^2}=
 \frac{u_{ij-1}+u_{ij+1}}{\Delta r^2}
 +\frac{1}{r}\frac{u_{ij+1}-u_{ij-1}}{2\Delta r}
 +\frac{1}{r^2}\frac{u_{i-1j}+u_{i+1j}}{\Delta \theta^2}
 \]
\hfill[1]

Multiplying by $\Delta r^2$ b.s.
\[ 2h_{ij}+\frac{2\Delta r^2}{r^2\Delta \theta^2}h_{ij}=
 u_{ij-1}+u_{ij+1}
 +\frac{\Delta r}{2r}\left(u_{ij+1}-u_{ij-1}\right)
 +\frac{\Delta r^2}{r^2\Delta \theta^2}\left(u_{i-1j}+u_{i+1j}\right)
\]
 \hfill[1]
 
Letting \(\beta=\frac{\Delta r}{r\Delta \theta}\)
\[ 2u_{ij}+2\beta^2u_{ij}=
 u_{ij-1}+u_{ij+1}
 +\frac{\Delta r}{2r}\left(u_{ij+1}-u_{ij-1}\right)
 +{\beta^2}\left(u_{i-1j}+u_{i+1j}\right)
\]
\[ 2u_{ij}(1+\beta^2)= u_{ij-1}+u_{ij+1}
 +\frac{\Delta r}{2r}\left(u_{ij+1}-u_{ij-1}\right)
 +\beta^2\left(u_{i-1j}+u_{i+1j}\right)
\]
 \hfill[1]

or
\[ u_{ij}=\frac{1}{2(1+\beta^2)}\left[u_{i-1j}+u_{i+1j}
+\frac{\Delta r}{2r}\left(u_{ij+1}-u_{ij-1}\right)
+\beta^2\left(u_{ij-1}+u_{ij+1}\right)\right]\]
 \hfill[2]
 
Now when \(u_{ij}\) is being calculated, \(u_{i-1j}\) and \(u_{ij-1}\) are known. Introducing the iteration count $m$ we have
\[u^{m+1}_{ij}=\frac{1}{2(1+\beta^2)}\left[u^{m+1}_{i-1,j}+u^{m}_{i+1,j}
+\frac{\Delta r}{2r}\left(u^m_{ij+1}-u^{m+1}_{ij-1}\right)
 +\beta^2\left(u^{m+1}_{i,j-1}+u^{m}_{i,j+1}\right)\right]\]
as required. \hfill[1]
\end{solution}

\newpage
\part[6] In order to test the performance of the scheme a mesh refinement study has been conducted.  The solution has been computed on a sequence of grids that are $9\times5$, $17\times9$, $33\times17$, $65\times33$, and $129\times65$ nodes in the $\theta$ and $r$ directions respectively and recorded at $(60^\circ,3.0)$.  The results of the \texttt{RefinementAnalysis.m} script are given in the Code Listing~\ref{code:PDE3_2019_grid_convergence}. Using the grid convergence index test to see if (and if so, state on which grid) a converged solution has been obtained.

\begin{minipage}[h]{\linewidth}
\begin{Verbatim}[frame=single,framerule=0.5mm,rulecolor=\color{black}]
>> RefinementAnalysis(dx',fx')
5 grids have been used.

Grid  Spacing  Function
   1  0.698      2.585549
   2  1.385      2.576612
   3  2.727      2.547178
   4  5.294      2.562340
   5 10.000      2.561414
   
Order of convergence using the first three finest grids
and assuming constant grid refinement.
Order of Convergence, p = 1.739034

Grid Convergence Index on fine grids. Using p=1.739 
Factor of Safety = 1.25

  Grid      Refinement
  Step      Ratio, r      GCI
  1  2      2.000000      0.188374
  2  3      2.000000      0.634454
  3  4      2.000000      0.342993
  4  5      2.000000      0.022332
\end{Verbatim}
\captionof{code}{Output of the \texttt{RefinementAnalysis.m} script.}
\label{code:PDE3_2019_grid_convergence}
\end{minipage}

\droppoints
\begin{solution}
For a mesh independent solution the ratio,
\[\frac{r^pGCI_{12}}{GCI_{32}}= 1\]

Using the provided data, 
\begin{verbatim}
  Grid      Refinement
  Step      Ratio, r      GCI
  1  2      2.000000      0.188374
  2  3      2.000000      0.634454
  3  4      2.000000      0.342993
  4  5      2.000000      0.022332
\end{verbatim}
we can calculate the ratios
\begin{align*}
\frac{4GCI_{12}}{GCI_{23}}&=\frac{4\times0.188374}{0.634454}=0.977889\\
\frac{4GCI_{23}}{GCI_{34}}&=\frac{4\times0.634454}{0.342993}=6.012912\\
\frac{4GCI_{34}}{GCI_{45}}&=\frac{4\times0.342993}{0.022332}=48.675737
\end{align*}\hfill[2]

Since $\frac{4GCI_{12}}{GCI_{23}}\approx1$ and while the remaining GCI ratios are $>>1$ a grid converged solution has been obtained on only the first three grids.\hfill[2]

This implies that a grid spacing of no more that $\Delta \theta=2.7^\circ$ should be used for an accurate solution to be obtained.\hfill[2]
\end{solution}
\end{parts}
