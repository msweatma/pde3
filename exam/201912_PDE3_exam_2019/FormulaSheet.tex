% -------------------------------------------------------------
% Formula sheet
% -------------------------------------------------------------
\headingFormulaSheet

% ---------------------------------------------
\subsection*{Laplace transform tables}
%----- Table -------------------------------------------------------%
\begin{table}[ht]
\caption{Properties of the unilateral Laplace transform} % title of Table
\centering % used for centering table
\begin{tabular}{|c|c|c|} % centered columns (4 columns)
\hline %inserts double horizontal lines
Property & $f(t), t\ge0$ & $F(s)$ \\ 
%heading
\hline 
Definition & $f(t)$ & $\int_0^\infty\exp(-st)f(t)dt$ \\ [0.3ex]
Inversion & $\frac{1}{j2\pi}\int_{c-j\infty}^{c+j\infty} \exp(st)F(s)ds$ & $F(s)$ \\ [0.3ex]
Linearity & $\alpha f(t) + \beta g(t)$ & $\alpha F(s) + \beta G(s)$ \\ [0.3ex]
Scaling & $f(\alpha t)$ & $\frac{1}{\alpha}F\left(\frac{s}{\alpha}\right)$ \\ [0.3ex]
Frequency shifting & $\exp(\alpha t)f(t)$ & $F(s-\alpha)$ \\ [0.3ex]
Time shifting & $f(t-\alpha) H(t-\alpha)$ & $\exp(-\alpha s)F(s)$ \\ [0.3ex]
First derivative & $f'(t)$ & $sF(s) - f(0)$ \\ [0.3ex]
nth derivative & $f^{(n)}(t)$ & $s^nF(s)-s^{n-1}f(0)-s^{n-2}f'(0)$ \\
      & & $-\dots-s f^{(n-2)}(0) - f^{(n-1)}(0)$  \\ [0.3ex]
Transform derivative & $t^n f(t)$ & $(-1)^n \frac{d^n F(s)}{ds^n}$ \\ [0.3ex]        
Integration & $\int_0^t f(\tau)d\tau$ & $\frac{1}{s}F(s)$  \\ [0.3ex]
\hline
\end{tabular}
\label{table:properties} % is used to refer this table in the text
\end{table}

%----- Table -------------------------------------------------------%
\begin{table}[ht]
\caption{Function transforms} % title of Table
\centering % used for centering table
\begin{tabular}{|c|c|c|c|} 
\hline %inserts double horizontal lines
Name & $f(t), t\ge0$ & $F(s)$ & Convergence region \\ [0.3ex] % inserts table
%heading
\hline % inserts single horizontal line
Unit impulse & $\delta(t)$ & $1$ & all $s$ \\ [0.3ex]
Ideal delay& $\delta(t-\alpha)$ & $\exp(-\alpha s)$ & $\Re(s)\ge\alpha$ \\ [0.3ex]
Unit step & $H(t)$ & $\frac{1}{s}$ & $\Re(s)>0$ \\ [0.3ex]
Ramp  & $t$ & $\frac{1}{s^2}$ & $\Re(s)>0$ \\ [0.3ex]
nth power  & $t^n, n=1,2,\dots$ & $\frac{n!}{s^{n+1}}$ & $\Re(s)>0$ \\ [0.3ex]
Sine & $\sin(\alpha t)$ & $\frac{\alpha}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
Cosine & $\cos(\alpha t)$ & $\frac{s}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
Hyperbolic sine & $\sinh(\alpha t)$ & $\frac{\alpha}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
Hyperbolic cosine & $\cosh(\alpha t)$ & $\frac{s}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
Exponential decay & $\exp(-\alpha t)$ & $\frac{1}{s + \alpha}$ & $\Re(s)>-\alpha$ \\ [0.3ex]
Ramp hyperbolic sine& $t\sinh(\alpha t)$ & $\frac{2\alpha s}{(s^2 - \alpha^2)^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
Complementary error function  & $\text{erfc}\left(\frac{k}{2\sqrt{t}}\right)$ & $\frac{1}{s}\exp\left(-k\sqrt{s}\right)$ & $\Re(s)\ge0$ \\ [0.3ex]
\hline %inserts single line
\end{tabular}
\label{table:functions}
\end{table}

% ---------------------------------------------
\subsection*{Useful integrals}
\begin{align*}
\int x \sin ax \, \mathrm{d}x &= \frac{1}{a^2} \bigl ( \sin a x - a x \cos ax \bigr ) \\
\int x \cos ax \, \mathrm{d}x &= \frac{1}{a^2} \bigl ( \cos a x + a x \sin ax \bigr ) \\
\int x^2 \sin ax \, \mathrm{d}x &= \frac{2x}{a^2} \sin ax - \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \cos ax \\
\int x^2 \cos ax \, \mathrm{d}x &= \frac{2x}{a^2} \cos ax + \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \sin ax
\end{align*}

% ---------------------------------------------
\subsection*{Finite difference approximations}
\begin{align*}
\left.\frac{\partial u}{\partial x}\right|_{x_i} &= \frac{u_{i+1}-u_i}{\Delta x}+O(\Delta x)\\
\left.\frac{\partial u}{\partial x}\right|_{x_i} &= \frac{u_{i}-u_{i-1}}{\Delta x}+O(\Delta x)\\
\left.\frac{\partial u}{\partial x}\right|_{x_i} &= \frac{u_{i+1}-u_{i-1}}{2\Delta x}+O(\Delta x^2)\\
\left.\frac{\partial^2 u}{\partial x^2}\right|_{x_i} &= \frac{u_{i+1}-2u_i+u_{i-1}}{\Delta x^2}+O(\Delta x^2)
\end{align*}

% ---------------------------------------------
\subsection*{Euler formula}
\[e^{{ix}}=\cos x+i\sin x,\text{ so } \cos x=\frac{e^{ix}+e^{-ix}}{2}\text{ and} \sin x=\frac{e^{ix}-e^{-ix}}{2i}\]

% ---------------------------------------------
\subsection*{Fourier constituents for Von Neumann stability analysis}
\begin{align*}
 u^{n}_{j} &= g^{n}e^{ij\theta}, \\
 u^{n}_{j\pm1} &= g^{n}e^{i(j\pm1)\theta},\\
 u^{n+1}_j &= g^{n+1}e^{ij\theta}, \\
 u^{n+1}_{j\pm1} &= g^{n+1}e^{i(j\pm1)\theta}, 
\end{align*}
where \(i=\sqrt{-1}\), $j$ is the grid index and $n$ is the time step. There are substituted into the scheme and the resulting equation is rearranged into the form
\[g^{n+1}=g^{n}G(\theta)\]
The scheme is stable provided \(\left| G(\theta)\right|\le 1\ \forall\theta\).
