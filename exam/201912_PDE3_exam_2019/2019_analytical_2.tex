% -----------------------------------------------------
% QUESTION
%------------------------------------------------------------------------%
% Analytical questons set by Daniel Friedrich
\question 
\setcounter{equation}{0}
Consider the one-dimensional wave equation
\begin{align}
\frac{\partial^2u}{\partial t^2} &= c^2 \frac{\partial^2u}{\partial x^2} & \quad  \label{eq:wave_equation_201912}\\
\intertext{on the interval $x\in[0, l]$ with the initial and boundary conditions }
 u(x, 0) &= f(x), \quad \forall x\in[0, l], \label{eq:we_IC1_201912} \\
\left. \frac{\partial u(x, t)}{\partial t}\right|_{t=0} &= 0, \quad \forall x\in[0, l], \label{eq:we_IC2_201912} \\
u(0, t) &= 0, \quad \forall t. \label{eq:we_BC1_201912} \\
\left. \frac{\partial u(x, t)}{\partial x}\right|_{x=l} &= 0, \quad \forall t, \label{eq:we_BC2_201912}
\end{align}
where $l$ is a positive constant and $f(x)$ is an arbitrary function.

\begin{parts}
%----- Part -----------------------------------------------------------%
\part[5]
The partial differential equation~(\ref{eq:wave_equation_201912}) and the boundary and initial conditions~(\ref{eq:we_IC1_201912}-\ref{eq:we_BC2_201912}) model the behaviour of a string. 

Sketch the domain on which the given problem is to be solved and indicate the given initial and boundary conditions. Clearly label the sketch. Comment on the physical quantity calculated by the model as well as the initial state and boundary conditions.
\droppoints
\begin{solution}
\begin{itemize}
\item Sketch of the problem with proper axis and initial/boundary condition labelling \hfill[2]
\item The PDE models the displacement of a string, which has an initial displacement given by $f(x)$. The initial velocity is zero. \hfill[2]
\item The left side of the string is held at zero and the spatial derivative at the right side is held at zero. \hfill[1]
\end{itemize}
\end{solution}

%----- Part ------------------------------------------%
\part[8] Using the method of separated solutions, derive the solution of the wave equation~(\ref{eq:wave_equation_201912}) subject to the above initial and boundary conditions~(\ref{eq:we_IC1_201912}-\ref{eq:we_BC2_201912}).
\droppoints
\begin{solution}
The four separated solution types are 
\begin{align*}
u_1(x,t) &= \cos \lambda c t\sin\lambda x \\
u_2(x,t) &= \cos \lambda c t\cos\lambda x \\
u_3(x,t) &= \sin \lambda c t\sin\lambda x \\
u_4(x,t) &= \sin \lambda c t\cos\lambda x  
\end{align*}
We are stepping through the initial and boundary conditions to find a solution which fits all conditions. The first boundary condition~\ref{eq:we_BC1_201912}, $u(0,t) = 0$, is fulfilled by the solutions $u_1$ and $u_3$. \hfill[2]

From the initial condition~\ref{eq:we_IC2_201912} we know that $u'(x,0)=0$ which indicates a cosine term in $t$. This leaves only solution $u_1$. \hfill[2]

The second boundary condition~\ref{eq:we_BC2_201912}, $\left. \frac{\partial u(x, t)}{\partial x}\right|_{x=l} = 0$, is only fulfilled for solution $u_1$ if
\[
\cos(\lambda l) = 0
\]
and thus $\lambda_n = \frac{(2n-1)\pi}{2l}$ for $n\in\mathbb{N}$. \hfill[2]

Thus we get solutions of the form
\[
u_n(x,t) = b_n \cos(\lambda_n c t) \sin(\lambda_n x), \quad n=1,2,\dots
\]
where we kept the $\lambda_n$ for readability.

To fulfil the second initial condition~\ref{eq:we_IC2_201912} for an arbitrary $f(x)$ we need to superimpose the solution \hfill[2]
\begin{align*}
u(x,t) = \sum_{n=1}^\infty b_n \cos(\lambda_n c t) \sin(\lambda_n x)
\end{align*}
\end{solution}

%----- Part ------------------------------------------%
\part[7] Now consider the case where the initial displacement is given by
\begin{equation}
u(x,0) = f_c(x) = (l-x)x, \quad \forall x\in[0, l]. \label{eq:we_IC1c_201912} 
\end{equation}
Find the solution to the PDE~\ref{eq:wave_equation_201912} which fulfils the initial and boundary conditions~\ref{eq:we_IC2_201912}, \ref{eq:we_BC1_201912}, \ref{eq:we_BC2_201912} and the initial displacement \ref{eq:we_IC1c_201912}.
\droppoints
\begin{solution}
To fulfil initial condition~\ref{eq:we_IC1c_201912}, we need to equate the superimposed solution with the displacement at the initial time $t=0$ \hfill[2]
\begin{align*}
u(x,0) &= \sum_{n=1}^\infty b_n \cos(\lambda_n c 0) \sin(\lambda_n x) \overset{!}{=} (l-x)x
\end{align*}

The parameters $b_n$ can be found with the usual Fourier series formulas \hfill[3]
\begin{align*}
b_n =& \frac{2}{l} \int_0^l (l-x)x \sin(\lambda_n x) dx \\
 =& \frac{2}{l} \left[\frac{l}{\lambda_n^2} \bigl (\sin(\lambda_n x) - \lambda_n x \cos(\lambda_n x) \bigr ) \right. \\
 & \quad \left. - \frac{2x}{\lambda_n^2} \sin(\lambda_n x) + \left(\frac{x^2}{\lambda_n} - \frac{2}{\lambda_n^3}\right) \cos(\lambda_n x) \right]_0^l \\
 =& \frac{2}{l} \left[\frac{l}{\lambda_n^2} \sin(\lambda_n l) - \frac{2l}{\lambda_n^2} \sin(\lambda_n l) + \frac{2}{\lambda_n^3} \right] \\ 
  =& \frac{2}{l} \left[-\frac{l}{\lambda_n^2} \sin(\lambda_n l) + \frac{2}{\lambda_n^3} \right] \\ 
 &= \frac{2}{\lambda_n^2}(-1)^n + \frac{4}{l\lambda_n^3} \\
 &= \frac{8l^2}{(2n-1)^2\pi^2}(-1)^n + \frac{32 l^2}{(2n-1)^3\pi^3}
\end{align*}

Thus, the final solution is \hfill[2]
\begin{align*}
u(x,t) &= \sum_{n=1}^\infty \left(\frac{8l^2}{(2n-1)^2\pi^2}(-1)^n + \frac{32 l^2}{(2n-1)^3\pi^3}\right) \cos(\frac{(2n-1)\pi}{2l} c t) \sin(\frac{(2n-1)\pi}{2l} x)
\end{align*}
\end{solution}

\end{parts}
