
% -----------------------------------------------------
% QUESTION
% -----------------------------------------------------
\question 
The governing equation for the advection of a scalar quantity, $c$, at a velocity $u$ is:	
\[\frac{\partial c}{\partial t}+u\frac{\partial c}{\partial x}=0.\]

A suitable scheme for the solution of this equation may be derived by considering the differential marching operator,
\[c(x,t+\Delta t)=L_x(\Delta t).c(x,t)+O(\Delta t^3),\]
\[\text{where }L_x(\Delta t)=1-u\Delta t\partial_x+\frac{u^2\Delta t^2}{2}\partial{xx}.\]

Using this approach the following time marching method can be obtained:
\begin{equation}
c_i^{n+1}=c^n_i-\frac\nu2\left(c^n_{i+1}-c^n_{i-1}\right)+\frac{\nu^2}2\left(c^n_{i+1}-2c^n_i+c^n_{i-1}\right)\text{ where }\nu=\frac{u\Delta t}{\Delta x}.
\label{e:QN2}
\end{equation}
\begin{parts}	
% -----------------------------------------------------
\part[6] Show that using an appropriate choice of finite difference approximations for $\partial_x$ and $\partial_{xx}$ the time marching scheme (\ref{e:QN2}) can be derived. State the formal accuracy of the scheme in both time and space.
\droppoints

\begin{solution}
This is the well known Lax-Wendroff scheme.  It is derived by substituting the 2nd order central difference approximations for $\partial_x$ and $\partial_{xx}$. \hfill[1]
\[\delta_x.c_i^n=\frac{c_{i+1}^n-c_{i-1}^n}{2\Delta x}\mbox{ and }
\delta_{xx}.c_i^n=\frac{c_{i+1}^n-2c_i^n+c_{i-1}^n}{\Delta x^2}.\] \hfill[1]

Substituting into the differential marching operator,
\[
c_i^{n+1}=c_i^n-\frac{u\Delta t}{2\Delta x}\left(c_{i+1}^n-c_{i-1}^n\right)
+\frac{u^2\Delta t^2}{2\Delta x^2}\left(c_{i+1}^n-2c_i^n+c_{i-1}^n\right).
\]
Substituting $\nu=\frac{u\Delta t}{\Delta x}$,
\[
c_i^{n+1}=c^n_i-\frac\nu2\left(c^n_{i+1}-c^n_{i-1}\right)+\frac{\nu^2}2\left(c^n_{i+1}-2c^n_i+c^n_{i-1}\right)
\] \hfill[2]

Because the 2nd order central finite difference approximations have been used the scheme is 2nd order in space. \hfill[1]

The scheme is 2nd order in time because both terms of the space marching opperator have been approximated.
\hfill[1]

\end{solution}

% -----------------------------------------------------
\part[10] Use von Neumann stability analysis to show that (\ref{e:QN2}) is stable if and only if $\nu\le1$.
\droppoints

\begin{solution}
Substitute the Fourier modes 
\begin{align*}
 c^{n}_{j} &= g^{n}e^{ij\theta}, \\
 c^{n}_{j\pm1} &= g^{n}e^{i(j\pm1)\theta},\\
 c^{n+1}_j &= g^{n+1}e^{ij\theta}, \\
 c^{n+1}_{j\pm1} &= g^{n+1}e^{i(j\pm1)\theta}, 
\end{align*}
where $j$ is the cell index and $i=\sqrt{-1}$, into
\[
c_j^{n+1}=
c^n_j
-\frac\nu2\left(c^n_{j+1}-c^n_{j-1}\right)
+\frac{\nu^2}2\left(c^n_{j+1}-2c^n_j+c^n_{j-1}\right)
\]
gives
\begin{align*}
g^{n+1}e^{ij\theta}&=
g^{n}e^{ij\theta}
-\frac\nu2\left(g^{n}e^{i(j+1)\theta}-g^{n}e^{i(j-1)\theta}\right)\\
&+\frac{\nu^2}2\left(g^{n}e^{i(j+1)\theta}-2g^{n}e^{ij\theta}+g^{n}e^{i(j-1)\theta}\right).
\end{align*} \hfill[2]

Dividing through by $e^{ij\theta}$ both sides
\[
g^{n+1}=
g^{n}
-g^n\nu\frac{e^{i\theta}-e^{-i\theta}}{2}
+\frac{g^{n}\nu^2}2\left(e^{i\theta}-2+e^{-i\theta}\right).
\] \hfill[2]

Factorising
\[
g^{n+1}=
g^{n}\left[1
-\nu\frac{e^{i\theta}-e^{-i\theta}}{2}
+\nu^2\left(\frac{e^{i\theta}+e^{-i\theta}}{2}-1\right)\right].
\] 

Using the Euler formula
\[
g^{n+1}=
g^{n}\left[1
-\nu i\sin\theta
+\nu^2\left(\cos\theta-1\right)\right].
\] \hfill[1]

Now $G(\theta)=g^{n+1}/g^n$, so
\[G(\theta)=1
-\nu i\sin\theta
+\nu^2\left(\cos\theta-1\right)\] 

Squaring both sides we have
\begin{align*}
|G(\theta)|^2&=(1-\nu^2(1-\cos\theta))^2+\nu^2(1-\cos^2\theta)\\
&=1-2\nu^2(1-\cos\theta)+\nu^2(1-\cos^2\theta)+\nu^4(1-\cos\theta)^2\\
&=1-\nu^2(1-\nu^2)(1-\cos\theta)^2\\
&=1-4\nu^2(1-\nu^2)\sin^4\frac\theta2.
\end{align*}
\hfill[4]

For stability \[|G(\theta)|\le1\implies|G(\theta)|^2\le1\implies\nu\le1.\]
\hfill[1]
\end{solution}

% -----------------------------------------------------
\part[4] Explain why the use of a factored sequence of one-dimensional operators would be advantageous, in terms of computational effort, when attempting to solve the two dimensional advection equation, 
\[\frac{\partial c}{\partial t}+u\frac{\partial c}{\partial x}+v\frac{\partial c}{\partial y}=0,\]
and what properties the chosen sequence needs in order to preserve a second order accurate solution.
\droppoints

\begin{solution}
\begin{itemize}
\item Using one dimensional operators makes the implementation of the numerical efficient as the time step is restricted by \[\Delta t<\nu\min\left(\frac{\Delta x}{u},\frac{\Delta y}{v}\right)\] \hfill[1]
\item Implementation of the one-dimensional operators is more computationally efficient as they can be done using long parallisable loops. \hfill[1]
\item To preserve 2nd order accuracy (max 2 marks)
\begin{itemize}
\item The 1D operator must be 2nd order \hfill[1]
\item The sequence must be symmetric, i.e. $u^{n+2}=L_xL_yL_yL_x(\Delta t).u^n$ \hfill[1]
\item The same time step must be used for all operators. \hfill[1]
\end{itemize}
\end{itemize}
\end{solution}

\end{parts}