
% -----------------------------------------------------
% QUESTION
%------------------------------------------------------------------------%
% First analytical questions set by Daniel Friedrich
% Heat equation with Laplace transform method
\question
\pointformat{(\bfseries\boldmath\themarginpoints)}

Consider the one-dimensional heat conduction equation
\begin{align}
\frac{\partial u}{\partial t} &= \kappa \frac{\partial^2u}{\partial x^2} & \quad  \label{eq:heat_equation_201912}\\
\intertext{on the interval $x\in[0, \infty)$ with the initial and boundary conditions }
u(x, 0) &= x\exp(-\alpha x), \quad \forall x\in[0, \infty), \label{eq:heat_IC1_201912} \\
u(0, t) &= \frac{2}{\alpha}\sinh(\kappa \alpha^2 t) + H(t), \quad \forall t. \label{eq:heat_BC1_201912} \\
\lim_{x\to\infty}u(x,t) &= 0, \quad \forall t. \label{eq:heat_BC2_201912}
\end{align}
where $\kappa$ is the thermal diffusivity, $\alpha$ is a positive parameter and $H(t)$ is the Heaviside unit step function.

\begin{parts}
%----- Part -----------------------------------------------------------%
\part[12] Apply the Laplace transform with respect to $t$ to the PDE~(\ref{eq:heat_equation_201912}) and find the general solution for $U(x,s)$ in the Laplace domain.
\droppoints
\begin{solution}
The Laplace transform of the left hand side of the partial differential equation can be found using the derivative property and on the right hand side we can swap the order of integration and differentiation. This gives us the transformed equation
\begin{align}
s U(x, s) - u(x, 0) &= \kappa {\cal L} \left\{\frac{\partial^2 u(x,t)}{\partial x^2} \right\} = \kappa\frac{\partial^2 U(x,s)}{\partial x^2} 
\label{eq:heat_transformed_201912}
\end{align}

Insert the initial conditions into Eq.~(\ref{eq:heat_transformed_201912}) to get \hfill[2]
\begin{align}
sU(x, s) - x\exp(-\alpha x) &= \kappa \frac{\partial^2 U(x,s)}{\partial x^2} \nonumber\\
\Rightarrow \frac{\partial^2 U(x,s)}{\partial x^2} - \frac{s}{\kappa} U(x, s) &= \frac{-1}{\kappa}x\exp(-\alpha x) \label{eq:heat_laplace_201912}
\end{align}

The differential equation~(\ref{eq:heat_laplace_201912}) has the characteristic equation
\begin{align*}
m^2 - \frac{s}{\kappa} &= 0 \\
\intertext{which has the zeros}
 m_{1,2} &= \pm \sqrt{\frac{s}{\kappa}} \\
\end{align*}
Thus the complimentary function is
\begin{align}
U_g(x,s) &= A(s) e^{x\sqrt{\frac{s}{\kappa}}} + B(s) e^{-x\sqrt{\frac{s}{\kappa}}} \label{eq:heat_general_solution_201912}
\end{align}
where $A(s)$ and $B(s)$ are functions in the Laplace domain which we need to determine from the boundary conditions. \hfill[2]

Before we find the two functions we need to take care of the inhomogeneity of the differential equation. The trial solution for the particular integral for the inhomogeneous differential equation~(\ref{eq:heat_laplace_201912}) is \hfill[2]
\begin{align*}
U_p(x,s) &= Px\exp(-\alpha x) + Q\exp(-\alpha x)
\end{align*}
where the coefficients $P$ and $Q$ need to be determined. We calculate the first and second derivatives of $U_p$
\begin{align*}
\frac{\partial U_p(x,s)}{\partial x} &= P\exp(-\alpha x) - P\alpha x\exp(-\alpha x) - Q\alpha\exp(-\alpha x) \\
\frac{\partial^2 U_p(x,s)}{\partial x^2} &= -2P\alpha\exp(-\alpha x) + P\alpha^2 x \exp(-\alpha x) + Q\alpha^2\exp(-\alpha x) 
\end{align*}

We substitute $U_p$ and its second derivative with respect to $x$ into Eq.~(\ref{eq:heat_laplace_201912}) and get \hfill[2]
\begin{align*}
-2P\alpha\exp(-\alpha x) + P\alpha^2 x \exp(-\alpha x) + Q\alpha^2\exp(-\alpha x) & \\
- \frac{s}{\kappa}\left( Px\exp(-\alpha x) + Q\exp(-\alpha x)\right) &= \frac{-1}{\kappa}x\exp(-\alpha x)
\end{align*}

\textbf{There has been an error in the solution. First we give the original wrong solution:}

We can arrange this in terms of $\exp(-\alpha x)$ and $x\exp(-\alpha x)$
\[
\exp(-\alpha x) \left[-2P\alpha + Q\alpha^2 + \frac{Qs}{\kappa}\right] + x\exp(-\alpha x) \left[P\alpha^2 -\frac{Ps}{\kappa} + \frac{1}{\kappa}\right] = 0
\]
From this we get two equations 
\begin{align*}
-2P\alpha + Q\alpha^2 + \frac{Qs}{\kappa} &= 0 \\
 P\alpha^2 -\frac{Ps}{\kappa} + \frac{1}{\kappa} &= 0
\end{align*}

From this it follows that
\begin{align*}
P &= \frac{1}{s - \kappa\alpha^2} \\
Q &= \frac{2\alpha\kappa}{(s - \kappa\alpha^2)(s+\alpha^2\kappa)} = \frac{2\alpha\kappa}{s^2 - \kappa^2\alpha^4} 
\end{align*}
for $\Re(s)>|\kappa^2\alpha^4|$ .\hfill[2]

Thus, the general solution of the differential equation is of the form
\begin{align*}
U(x,s) &= A(s) e^{x\sqrt{\frac{s}{\kappa}}} + B(s) e^{-x\sqrt{\frac{s}{\kappa}}} + \frac{1}{s - \kappa\alpha^2} x \exp(-\alpha x) + \frac{2\alpha\kappa}{s^2 - \kappa^2\alpha^4} \exp(-\alpha x)
\end{align*}
\hfill[2]

\textbf{Corrected solution where the highlighted sign is changed:}

We can arrange this in terms of $\exp(-\alpha x)$ and $x\exp(-\alpha x)$
\[
\exp(-\alpha x) \left[-2P\alpha + Q\alpha^2 {\color{red}-} \frac{Qs}{\kappa}\right] + x\exp(-\alpha x) \left[P\alpha^2 -\frac{Ps}{\kappa} + \frac{1}{\kappa}\right] = 0
\]
From this we get two equations 
\begin{align*}
-2P\alpha + Q\alpha^2 - \frac{Qs}{\kappa} &= 0 \\
 P\alpha^2 -\frac{Ps}{\kappa} + \frac{1}{\kappa} &= 0
\end{align*}

From this it follows that
\begin{align*}
P &= \frac{1}{s - \kappa\alpha^2} \\
Q &= \frac{-2\alpha\kappa}{(s - \kappa\alpha^2)(s - \kappa\alpha^2)} = \frac{-2\alpha\kappa}{\left(s - \kappa\alpha^2\right)^2} 
\end{align*}
for $\Re(s)>|\kappa^2\alpha^4|$ .\hfill[2]

Thus, the general solution of the differential equation is of the form
\begin{align*}
U(x,s) &= A(s) e^{x\sqrt{\frac{s}{\kappa}}} + B(s) e^{-x\sqrt{\frac{s}{\kappa}}} + \frac{1}{s - \kappa\alpha^2} x \exp(-\alpha x) + \frac{-2\alpha\kappa}{\left(s - \kappa\alpha^2\right)^2} \exp(-\alpha x)
\end{align*}
\hfill[2]

\end{solution}

%----- Part -----------------------------------------------------------%
\part[8] Given that the general solution to the PDE~(\ref{eq:heat_equation_201912}) is
\begin{align*}
U(x,s) &= A(s) e^{x\sqrt{\frac{s}{\kappa}}} + B(s) e^{-x\sqrt{\frac{s}{\kappa}}} + \frac{1}{s - \kappa\alpha^2} x \exp(-\alpha x) + \frac{2\alpha\kappa}{s^2 - \kappa^2\alpha^4} \exp(-\alpha x)
\end{align*}
use the boundary conditions~(\ref{eq:heat_BC1_201912}-\ref{eq:heat_BC2_201912}) to find $A(s)$ and $B(s)$. Use the inverse Laplace Transform on the resulting solution to find the solution of the heat conduction equation~(\ref{eq:heat_equation_201912}-\ref{eq:heat_BC2_201912}) in the time domain.
\droppoints
\begin{solution}
From the boundary condition~(\ref{eq:heat_BC2_201912}) we know that the solution is bounded. Thus we can swap the limit and the integration to get \hfill[2]
\begin{align*}
\lim_{x\to-\infty} U(x,s) &= \lim_{x\to\infty} \int_0^\infty e^{-st} u(x,t) dt = \int_0^\infty  e^{-st} \lim_{x\to\infty} u(x,t) dt = 0
\end{align*}
From this it follows that
\begin{align*}
A(s) &= 0, 
\end{align*}
because otherwise the first term on the right hand side of Eq.~(\ref{eq:heat_general_solution_201912}) would go to infinity for $x\to\infty$.
\hfill[2]

The boundary condition~(\ref{eq:heat_BC1_201912}) requires that
\begin{align*}
U(0,s) &= B(s) + \frac{2\alpha\kappa}{s^2 - \kappa^2\alpha^4}\overset{!}{=} {\cal L} \left\{u(0,t) \right\} = \frac{2}{\alpha}\frac{\kappa\alpha^2}{s^2 - (\kappa\alpha^2)^2} + \frac{1}{s}, \quad \Re(s)>0,
\end{align*}
and thus \hfill[2]
\begin{align*}
 B(s) &= \frac{1}{s}
\end{align*}

With $B(s)$ we get the solution in the Laplace domain 
\begin{align*}
U(x,s) &= \frac{1}{s} e^{-x\sqrt{\frac{s}{\kappa}}} + \frac{1}{s - \kappa\alpha^2} x \exp(-\alpha x) + \frac{2\alpha\kappa}{s^2 - \kappa^2\alpha^4} \exp(-\alpha x)
\end{align*}
and can find the solution in the time domain by using the Laplace transforms given in Table~\ref{table:functions}. \hfill[2]
\begin{align*}
u(x,t) &= \text{erfc}\left(\frac{x}{2\sqrt{s\kappa}} \right) + \exp(\kappa\alpha^2 t) x \exp(-\alpha x) + \frac{2}{\alpha} \sinh(\kappa\alpha^2 t) \exp(-\alpha x), t\ge 0.
\end{align*}
\end{solution}
\end{parts}
