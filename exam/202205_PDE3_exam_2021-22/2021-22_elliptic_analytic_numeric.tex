%------------------------------------------------------------------------%
% Analytical and numerical question set by Daniel Friedrich and David Ingram
\newpage
\question \quad\\
\setcounter{equation}{0}
We consider steady-state heat conduction in a circle with radius 2 which can be described by the following 2D Laplace equation in cylindrical coordinates
\begin{align}
\frac{\partial^2 u}{\partial r^2} + \frac{1}{r}\frac{\partial u}{\partial r}  + \frac{1}{r^2}\frac{\partial^2 u}{\partial \theta^2}  = 0
\label{eq:Laplace_equation} 
\end{align}
for $0\le r \le 2$.

The boundary conditions are given by
\begin{align}
 \left.\frac{\partial u (r, \theta)}{\partial r}\right|_{r=2} &= 1, \quad 0\le \theta < \pi, \label{eq:Laplace_equation_BC1} \\
 \left.\frac{\partial u (r, \theta)}{\partial r}\right|_{r=2} &= 0, \quad \pi\le \theta < 2\pi. \label{eq:Laplace_equation_BC2}
\end{align}

\begin{parts}
%----- Part ------------------------------------------%
\part[10] Using the method of separated solutions, derive the solution of the Laplace equation~(\ref{eq:Laplace_equation}) subject to the above boundary conditions~(\ref{eq:Laplace_equation_BC1} -- \ref{eq:Laplace_equation_BC2}).
\droppoints

\begin{solution}
Using separation of variables we have to consider three cases for negative, zero and positive $\lambda$.
For $\lambda<0$ we get the solution
\begin{align*}
u_-(r,\theta) &= \left(A\cosh(\mu\theta) + B\sinh(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
with parameters $A$, $B$, $C$ and $D$, and where we set $\lambda = -\mu^2$. 
This needs to be periodic in $2\pi$ which is only fulfilled for $A=B=0$.
Thus the solution for $\lambda<0$ is zero. \hfill[1]

For $\lambda=0$ we get the solution
\[
u_0(r, \theta) = (a\theta + b)(C\ln(r) + D)
\]
with parameters $a$, $b$, $C$ and $D$. We know that the solution needs to be periodic in $2\pi$ and this is only the case for $a=0$. 
The solution needs to be finite for $r\to0$ from which it follows that $C=0$ so that we get
\begin{align*}
u_0(r,\theta) = b D = c
\end{align*}
where $c=bD$ is a constant which needs to be determined. \hfill[2]

For $\lambda = \mu^2 > 0$ we get the combined solution
\begin{align*}
u_+(r,\theta) &= \left(A\cos(\mu\theta) + B\sin(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
with parameters $A$, $B$, $C$ and $D$. Again this needs to be periodic in $2\pi$ and this is the case only for $\mu=n$ with $n\in\mathbb{N}$. 
The parameter $C$ needs to be zero to ensure that the solution remains finite as $r\to 0$.
By setting $A_n=AD$ and $B_n=BD$ we get
\[
u_3(r,\theta) = r^n \left(A_n\cos(n\theta) + B_n\sin(n\theta)\right), \quad n=1,2, \dots
\]
and for $n\in\mathbb{N}$. \hfill[2]

To fulfil the outer boundary condition we need to superimpose the solutions for $\lambda=0$ and $\lambda>0$. 
This is possible because we are dealing with a linear PDE. 
The complete solution is \hfill[1]
\[
u(r,\theta) = c + \sum_{n=1}^\infty r^{n}\left(A_n\cos(n\theta) + B_n\sin(n\theta)\right)
\]

We calculate the derivative with respect to $r$ of the superposed solution and set it equal to the outer boundary condition~\ref{eq:Laplace_equation_BC2} at $r=2$
\[
\left.\frac{\partial u (r, \theta)}{\partial r}\right|_{r=2} = \sum_{n=1}^\infty n2^{n-1}\left(A_n\cos(n\theta) + B_n\sin(n\theta)\right) = \begin{cases}
1, \quad & 0\le\theta<\pi \\
0, \quad & \pi\le\theta<2\pi
\end{cases}
\]
The parameters $A_n$ and $B_n$ can be found with the usual method of the Fourier series.
\begin{align*}
A_n &= \frac{1}{n2^{n-1} \pi} \int_{0}^{\pi} \cos\left(n\theta\right) d\theta = 0\\
B_n &= \frac{1}{n2^{n-1} \pi} \int_{0}^{\pi} \sin\left(n\theta\right) d\theta = \begin{cases}
\frac{2}{n^2 2^{n-1} \pi}, \quad & n \text{ odd} \\
0, \quad & n \text{ even}
\end{cases}
\end{align*}
\hfill[2]

Thus, the complete solution is given by 
\[
u(r,\theta) = c + \sum_{k=1}^\infty \frac{2}{(2k-1)^2 2^{2k-2} \pi} r^{2k-1}\sin((2k-1)\theta)
\]
for $0\le r \le 2$ and $0\le\theta\le 2\pi$ and with $n=2k-1$ to cover only the odd coefficients.
The solution is not unique because we only have Neumann boundary conditions so that the parameter $c$ can take any finite value.
\hfill[2]
\end{solution}

%% ---- Numerical part
\vskip0.4cm
Now the circle is distorted into an ellipse and we want to numerically handle the steady-state heat conduction problem on the domain shown by Figure QA\ref{f:elliptical-coords}.
\begin{figure}[h]
\setcounter{figure}{\thequestion-1}
\center{\includegraphics[width=0.75\columnwidth]{Images/Elliptical_coordinates_grid.pdf}}
\caption{Elliptical $(\mu,\nu)$ coordinates on the plane.}
\label{f:elliptical-coords}
\end{figure}

%----- Part ------------------------------------------%
\part[6]  Elliptical coordinates on the plane $(x,\,y)$ are:
\[\left.\begin{matrix}
x=c\cosh\mu\cos\nu \\
y=c\sinh\mu\sin\nu
\end{matrix}\right\}\]
Figure QA\ref{f:elliptical-coords} shows the grid for such a coordinate system. 
Lines of constant $\mu$ are ellipses with foci at $(-c,\,0)$ and $(c,\,0)$, while lines of constant $\nu$ are hyperbolas with the same foci. 
The Laplace equation transformed into this coordinate system is:
\begin{equation}
    0=\frac{1}{c^2\left(\sinh\mu+\sin^2\nu\right)}
    \left[\frac{\partial^2 u}{\partial\mu^2} +
    \frac{\partial^2 u}{\partial\nu^2}\right].
    \label{eq:elliptic-laplace}
\end{equation}
Using appropriate finite difference approximations, discretise Equation \ref{eq:elliptic-laplace} and hence determine the coefficients associated with the five points in the stencil $u_N,\, u_E,\, u_S,\, u_W$ and $u_O$.
\droppoints

\begin{solution}
Using the $2^{nd}$ order central finite difference approximations,
\begin{align*}
    \frac{\partial^2 u}{\partial\mu^2} &\approx \frac{u_W-2u_o+u_E}{\Delta\mu^2},\text{ and} \\
    \frac{\partial^2 u}{\partial\nu^2} &\approx \frac{u_S-2u_o+u_N}{\Delta\nu^2}
\end{align*}
\hfill[1]

We have 
\[0=\frac{1}{c^2\left(\sinh\mu+\sin^2\nu\right)}
 \left[\frac{u_W-2u_o+u_E}{\Delta\nu^2} +
 \frac{u_S-2u_o+u_N}{\Delta\mu^2}\right] \]
Collecting terms:
\[0=\frac{1}{c^2\left(\sinh\mu+\sin^2\nu\right)}
\frac{2(\Delta\nu^2+\Delta\mu^2)u_o-\Delta\nu^2u_W-\Delta\nu^2u_E
-\Delta\mu^2u_N-\Delta\mu^2u_S}{\Delta\mu^2\Delta\nu^2}\]
Writing $\beta=\Delta\mu/\Delta\nu$ and simplifying
\[0=\frac{1}{c^2\left(\sinh\mu+\sin^2\nu\right)}
\frac{(2\beta^2+2)u_o-\beta^2(u_W+u_E)-(u_N+u_S)}{\Delta\nu^2}\]
\hfill[1]

Now the coefficient $1/[c^2\left(\sinh\mu+\sin^2\nu\right)]$ is a function of both $\nu$ and $\mu$ so the value at the points $N,\, E,\, S\, W$ and $O$ depends on their respective $(\nu,\,\mu)$ coordinates.  The coefficients are therefore:
\begin{align*}
    u_E, u_W :& \frac{-\beta^2}{\Delta\nu^2c^2\left(\sinh\mu_E+\sin^2\nu_E\right)}, \\
    u_N, u_S :& \frac{-1}{\Delta\nu^2c^2\left(\sinh\mu_N+\sin^2\nu_N\right)},\\
    u_O :& \frac{2\beta^2+2}
    {\Delta\nu^2c^2\left(\sinh\mu_O+\sin^2\nu_O\right)}.
\end{align*}
\hfill[4]

Note: Multiplying by $c^2\left(\sinh\mu+\sin^2\nu\right)$ b.s. gives the normal Cartesian components,
\begin{align*}
    u_E, u_W :& \frac{-\beta^2}{\Delta\nu^2}, \\
    u_N, u_S :& \frac{-1}{\Delta\nu^2},\\
    u_O :& \frac{2\beta^2+2}{\Delta\nu^2}.
\end{align*}
and is awarded \hfill[2]

\end{solution}

%----- Part ------------------------------------------%
\part[4] In the computational domain you are solving the equations on a uniform grid with mesh spacing $\Delta\nu$ in the $i$ direction and $\Delta\mu$ in the $j$ direction.
Sketch the computational domain and briefly explain what type of boundary condition is needed on the  West, $\nu=0$, and East, $\nu=2\pi$ boundaries and how you would implement it.
\droppoints

\begin{solution}
\centerline{\includegraphics[width=0.5\columnwidth]{Images/Computational-domain.pdf}}
\hfill [2]

The $\nu=0$ and $\nu=2\pi$ boundaries are periodic.  On the $\nu=0$ boundary the stencil must pick up values from the opposite end of the domain at $\nu=2\pi$ and vica-versa.
\hfill[2]

\end{solution}
\end{parts}