% -----------------------------------------------------
% QUESTION --- Hyperbolic partial differential equations
% Numerical methods only.
% -----------------------------------------------------
\newpage
\question 
\setcounter{equation}{0}

Professor Ingram is trying to solve the inviscid, one dimensional form of the hyperbolic Burger's equation:
\[\frac{\partial u}{\partial t}+u\frac{\partial u}{\partial x}=0.\]
He knows this equation is similar to the linear advection equation,
\[\frac{\partial u}{\partial t}+a\frac{\partial u}{\partial x}=0,\] 
but that in the case of Burger's equation the speed of advection, $a=u$, varies in time and space. 

\begin{parts}
%----- Part -----------------------------------------------------------%
\part[5] Choosing appropriate finite-difference approximations derive the Lax-Friedrichs method for Burger's equation (given below), stating the formal accuracy of the scheme.
\[
u_i^{n+1}=\frac{u_{i+1}^{n}+u_{i-1}^{n}}{2}
-\frac{u_i\Delta t}{2\Delta x}\left(u_{i+1}^n-u_{i-1}^n\right).
\]
\droppoints

\begin{solution}
Choosing the $2^{nd}$ central difference for $u_x$ and the $1^{st}$ order forward difference approximation for $u_t$
\begin{align*}
    \frac{\partial u}{\partial t} &= \frac{u_i^{n+1}-u_i^{n}}{\Delta t}+O(\Delta t) \\
    \frac{\partial u}{\partial x} &= \frac{u_{i+1}^n-u_{i-1}^n}{2\Delta x}+O(\Delta t^2)
\end{align*}
\hfill[1]

In the time derivative we replace $u^n_i$ with the average of the left and right values,
\[\frac{\partial u}{\partial t} \approx \frac{u_i^{n+1} - \frac12(u_{i+1}^n+u_{i-1}^n)}{\Delta t}\]
\hfill[1]

The governing equation becomes
\[\frac{\partial u}{\partial t} \approx \frac{u_i^{n+1} - \frac12(u_{i+1}^n+u_{i-1}^n)}{\Delta t}+a\frac{u_{i+1}^n-u_{i-1}^n}{2\Delta x}=0\]
\hfill[1]

Rearranging,
\[u_i^{n+1}=\frac{u_{i+1}^{n}+u_{i-1}^{n}}{2}
-\frac{a\Delta t}{2\Delta x}\left(u_{i+1}^n-u_{i-1}^n\right).\]
\hfill[1]

Which is first order in time and second order in space. \hfill[1]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[10] Using von Neumann stability analysis determine the maximum stable time step which can be used with the classical Lax-Friedrichs scheme for the linear advection equation. 
\droppoints

\begin{solution}
The Fourier modes are:
\[u_i^{n+1}=g^{n+1}e^{ij\theta},\ u_i^n=g^ne^{ij\theta},\text{ and }  
u_{i\pm1}^n=g^ne^{(i\pm1)j\theta}\]
where \(j=\sqrt{-1}\). \hfill[1]

substituting into the Lax-Friedrichs scheme
\[g^{n+1}e^{ij\theta} = \frac{g^ne^{(i+1)j\theta} + g^ne^{(i-1)j\theta}}{2}
- \frac{a\Delta t}{2\Delta x}
  \left(g^ne^{(i+1)j\theta}-g^ne^{(i-1)j\theta}\right)\]
\hfill[1]

taking $g^n$ out as a factor,
\[g^{n+1}e^{ij\theta} = g^n\left[\frac{e^{(i+1)j\theta} + e^{(i-1)j\theta}}{2}
- \frac{a\Delta t}{2\Delta x}
  \left(e^{(i+1)j\theta}-e^{(i-1)j\theta}\right)\right].\]
\hfill[1]

dividing by $e^{ij\theta}$ both sides
\[g^{n+1} = g^n\left[\frac{e^{j\theta} + e^{-j\theta}}{2}
- \frac{a\Delta t}{\Delta x}
  \frac{e^{j\theta}-e^{-j\theta}}{2}\right].\]
\hfill[1]

using the complex trig identity
\[g^{n+1} = g^n\left[\cos\theta-\frac{a\Delta t}{\Delta x}j\sin\theta\right].\]
\hfill[1]

Writing $g^{n+1} = g^nG(\theta)$ we can see that
\[G(\theta)=\cos\theta-\frac{a\Delta t}{\Delta x}j\sin\theta\]
For the scheme to be stable we require that
$\left|G(\theta)\right| \le 1$ \hfill[1]

Since $G$ is a complex number, $|G|=\sqrt{GG^*}$, which implies $GG*\le1.$

\begin{align*}
G(\theta)G^*(\theta) &=\left(\cos\theta-\frac{a\Delta t}{\Delta x}j\sin\theta\right)
\left(\cos\theta+\frac{a\Delta t}{\Delta x}j\sin\theta\right) \\
&= \cos^2\theta + \left(\frac{a\Delta t}{\Delta x}\right)^2\sin^2\theta 
\end{align*}
\hfill [1]

Since $\cos\theta$ and $\sin\theta$ are out of phase the largest value will occur when $\cos\theta=0$ and $\sin\theta=1$, therefore
\[\left(\frac{a\Delta t}{\Delta x}\right)^2\le 1\]
So the scheme will be stable if and only if, 
\[\frac{a\Delta t}{\Delta x}\le 1\]
\hfill[2]

So the maximum stable time step is
\[\Delta t\le \frac{\Delta x}{a}.\]
\hfill[1]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[5] Briefly explain how the stable time step would be calculated if the non-linear Burger's equation,
\[\frac{\partial u}{\partial t}+u\frac{\partial u}{\partial x}=0,\]
is being solved instead of the linear advection equation.  
\droppoints

\begin{solution}
Since the advection speed depends on $u$,
\[\Delta t \le \min_i\frac{\Delta x}{|u_i|}\]
\hfill[2]

The stable time step can be calculated as
\[\Delta t =\nu\min_i\frac{\Delta x}{|u_i|}\text{ where }\nu\le1.\]
\hfill[1]

Because the equation is \textbf{non linear} we would choose a value of $\nu<1$, for example $\nu=0.9$.
\hfill [1]

Numerical experiments will be needed to test that this choice Courant number, $\nu$, is appropriate.
\hfill[1]

\end{solution}
\end{parts}

