
% -----------------------------------------------------
% QUESTION
%-----------------------------------------------------%
% First analytical questions set by Daniel Friedrich
% Heat equation with Laplace transform method
\question
\pointformat{(\bfseries\boldmath\themarginpoints)}

We consider the heat conduction in a very long rod for which we want to calculate the temperature profile over time.

You are given the following, non-dimensional, 1D heat conduction equation
\begin{align}
\frac{\partial u}{\partial t} &= \kappa \frac{\partial^2u}{\partial x^2} & \quad  \label{eq:heat_conduction_equation}\\
\intertext{on the interval $x\in[0, \infty)$ with the initial and boundary conditions }
u(x, 0) &= \sin(\alpha x), \quad \forall x\in[0, \infty), \label{eq:heat_IC} \\
u(0, t) &= \frac{1}{\sqrt{\pi t}}, \quad  t>0, \label{eq:heat_BC_left} \\
\lim_{x\to\infty}u(x,t) &< C, \quad \forall t. \label{eq:heat_BC_right}
\end{align}
where $\kappa$ is the thermal diffusivity, $\alpha$ is a positive parameter and $C$ is a positive and finite constant.

\begin{parts}
%----- Part ------------------------------------------%
\part[4] You are asked to describe the problem which is given by this partial differential equation (PDE) problem, i.e the PDE Eq.~\ref{eq:heat_conduction_equation} with the initial and boundary conditions Eqs.~\ref{eq:heat_IC} -- \ref{eq:heat_BC_right}, in plain English. 
In particular, you should discuss the choice of spatial domain.
\droppoints

\begin{solution}
The 1D heat conduction equation describes the heat conduction along the rod.
We approximate the very long rod with a semi-infinite domain where we force the right hand side to be bounded.
This will significantly simplify the calculation of the solution while having only a negligible impact on the temperature profile. \hfill[2]

Initially the rod has a temperature profile given by a sine function with angular frequency $\alpha$.
The temperature at the left hand side of the rod will jump to a large value once $t>0$ before slowly decreasing. \hfill[2]
\end{solution}

%----- Part ------------------------------------------%
\part[6] Apply the Laplace transform with respect to $t$ to the PDE~(\ref{eq:heat_conduction_equation}) and find the general solution for $U(x,s)$ in the Laplace domain.
\droppoints

\begin{solution}
The Laplace transform of the left hand side of the partial differential equation can be found using the derivative property and on the right hand side we can swap the order of integration and differentiation. This gives us the transformed equation
\begin{align}
s U(x, s) - u(x, 0) &= \kappa {\cal L} \left\{\frac{\partial^2 u(x,t)}{\partial x^2} \right\} = \kappa\frac{\partial^2 U(x,s)}{\partial x^2} 
\label{eq:heat_transformed}
\end{align}

Insert the initial condition~(\ref{eq:heat_IC}) into Eq.~(\ref{eq:heat_transformed}) to get 
\begin{align}
sU(x, s) - \sin(\alpha x) &= \kappa \frac{\partial^2 U(x,s)}{\partial x^2} \nonumber\\
\Rightarrow \frac{\partial^2 U(x,s)}{\partial x^2} - \frac{s}{\kappa} U(x, s) &= \frac{-1}{\kappa}\sin(\alpha x) \label{eq:heat_laplace}
\end{align}

The differential equation~(\ref{eq:heat_laplace}) has the characteristic equation
\begin{align*}
m^2 - \frac{s}{\kappa} &= 0 \\
\intertext{which has the zeros}
 m_{1,2} &= \pm \sqrt{\frac{s}{\kappa}}
\end{align*}
Thus, the complimentary function is
\begin{align}
U_g(x,s) &= A(s) e^{x\sqrt{\frac{s}{\kappa}}} + B(s) e^{-x\sqrt{\frac{s}{\kappa}}} \label{eq:heat_general_solution}
\end{align}
where $A(s)$ and $B(s)$ are functions in the Laplace domain which we need to determine from the boundary conditions. \hfill[2]

Before we find the two functions we need to take care of the inhomogeneity of the differential equation. 
The trial solution for the particular integral for the inhomogeneous differential equation~(\ref{eq:heat_laplace}) is
\begin{align*}
U_p(x,s) &= P\sin(\alpha x) + Q \cos(\alpha x)
\end{align*}
where the coefficients $P$ and $Q$ need to be determined. We substitute $U_p$ and its second derivative with respect to $x$ into Eq.~(\ref{eq:heat_laplace}) and get \hfill[2]
\begin{align*}
-P \alpha^2 \sin(\alpha x) - Q\alpha^2\cos( \alpha x) - \frac{s}{\kappa} \left( P\sin(\alpha x) + Q\cos(\alpha x)\right) &= -\frac{1}{\kappa} \sin(\alpha x)
\end{align*}
We can arrange this in terms of sine and cosine
\[
\sin(x) \left(-P\alpha^2 - \frac{s}{\kappa} P + \frac{1}{\kappa}\right) + \cos(x) \left(-Q\alpha^2 - \frac{s}{\kappa} Q\right) = 0
\]
From this we get two equations 
\begin{align*}
-P\alpha^2 - \frac{s}{\kappa} P + \frac{1}{\kappa} &= 0 \\
-Q\alpha^2 - \frac{s}{\kappa} Q &= 0
\end{align*}

From this it follows that
\begin{align*}
P &= \frac{1}{s + \kappa \alpha^2} \\
Q &= 0
\end{align*}
for $\Re(s)>-\kappa \alpha^2$.
Thus, the general solution of the differential equation is of the form
\begin{align*}
U(x,s) &= A(s) e^{x\sqrt{\frac{s}{\kappa}}} + B(s) e^{-x\sqrt{\frac{s}{\kappa}}} + \frac{1}{s + \kappa \alpha^2} \sin(\alpha x)
\end{align*}
\hfill[2]
\end{solution}

%----- Part ------------------------------------------%
\part[6] Given that the general solution to the PDE~(\ref{eq:heat_conduction_equation}) is
\begin{align*}
U(x,s) &= A(s) e^{x\sqrt{\frac{s}{\kappa}}} + B(s) e^{-x\sqrt{\frac{s}{\kappa}}} + \frac{1}{s + \kappa \alpha^2} \sin(\alpha x)
\end{align*}
use the boundary conditions~(\ref{eq:heat_BC_left}-\ref{eq:heat_BC_right}) to find $A(s)$ and $B(s)$. 
Use the inverse Laplace transform on the resulting solution to find the solution of the heat conduction equation~(\ref{eq:heat_conduction_equation} -- \ref{eq:heat_BC_right}) in the time domain.
\droppoints

\begin{solution}
From the boundary condition~(\ref{eq:heat_BC_right}) we know that the solution is bounded. Thus we can swap the limit and the integration to get \begin{align*}
\lim_{x\to\infty} U(x,s) &= \lim_{x\to\infty} \int_0^\infty e^{-st} u(x,t) dt = \int_0^\infty  e^{-st} \lim_{x\to\infty} u(x,t) dt = \frac{C}{s}
\end{align*}
From this it follows that
\begin{align*}
A(s) &= 0, 
\end{align*}
because otherwise the first term on the right hand side of Eq.~(\ref{eq:heat_general_solution}) would go to infinity for $x\to\infty$.
\hfill[2]

We calculate the Laplace transform of the boundary condition~(\ref{eq:heat_BC_left}) and set it equal to $U(0,s)$ to get
\begin{align*}
U(0,s) &= B(s) \overset{!}{=} {\cal L} \left\{u(0,t) \right\} = \frac{1}{\sqrt{s}}, \quad \Re(s)>0,
\end{align*}
and thus \hfill[2]
\begin{align*}
 B(s) &= \frac{1}{\sqrt{s}}
\end{align*}

With $B(s)$ we get the solution in the Laplace domain 
\begin{align*}
U(x,s) &= \frac{1}{\sqrt{s}} e^{-x\sqrt{\frac{s}{\kappa}}} + \frac{1}{s + \kappa \alpha^2} \sin(\alpha x)
\end{align*}
and can find the solution in the time domain by using the Laplace transforms given in Table~\ref{table:functions}. \hfill[2]
\begin{align*}
u(x,t) &= \frac{1}{\sqrt{\pi t}} \exp\left(-\frac{x^2}{4t\kappa}\right) + \exp( -\kappa \alpha^2 t) \sin(\alpha x), t > 0.
\end{align*}
\end{solution}

%----- Part ------------------------------------------%
\part[4] You are asked why you used the Laplace transform method for the solution of this problem.
Briefly (in less than 200 words), discuss which solution methods for the heat conduction equation are or aren’t applicable in this case.
\droppoints

\begin{solution}
\begin{itemize}
\item Laplace transform method is obviously applicable. In fact, the bounded boundary condition~\ref{eq:heat_BC_right} simplifies the solution significantly by removing the term with $A(s)$ \hfill[2]
\item The method of separation of variables is not directly applicable \hfill[2]
\begin{itemize}
\item The boundary condition varies with time so we can't just subtract the steady state solution
\item We would need to split the problem into two: the first one with zero boundary conditions and the non-zero initial condition; and the second with a zero initial condition and a non-zero left boundary condition
\end{itemize}
\end{itemize}

\end{solution}
\end{parts}
