%% This is typeset using the EXAM LaTeX2e class by Philip Hirschhorn. 
%%
%% Exam options - The points for each question will be located on the right hand margin and will be enclosed in round braces and there will be no header on the top of each page and a footer and a footer with the course code, course name and the month and year of the exam.  Each question should start on a new page. The final question should say END OF PAPER
%\pointsinrightmargin
\documentclass[answers,addpoints,12pt]{exam}
\usepackage[a4paper,top=4cm,left=2.5cm,right=2.5cm,bottom=4cm]{geometry}
%
% Exam options - The points for each question will be located on the right hand margin and will be enclosed in square braces and there will be a header on the top of each page and a footer with a page count at the bottom,.  On the last page of this exam this will say END.
\bracketedpoints
\pointsinrightmargin
% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\footer{}{}{SCEE09004 Partial Differential Equations 3 - December 2018}

% solution header
\renewcommand{\solutiontitle}{\noindent\textbf{Solution:}\par\noindent}
% we also use graphics for diagrams and pgfplots for graphs etc.
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{caption} %in order to use \caption* (i.e. prevent captions)
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
%\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\graphicspath{{./figures_exam/}}
% plotting
\usepackage{pgfplots}
\pgfplotsset{compat=1.8}
% AMS symbols
\usepackage{amsmath}
\usepackage{amssymb}

% system fonts
%\usepackage{fontspec,xltxtra,xunicode}
%\defaultfontfeatures{Mapping=tex-text}
%%setromanfont[Mapping=tex-text]{Times}
%\setsansfont[Mapping=tex-text]{Helvetica}
%\setmonofont[Scale=MatchLowercase]{Courier}

% font to san-serif
\usepackage[scaled]{helvet}
\renewcommand{\familydefault}{\sfdefault} 
\usepackage[T1]{fontenc}

% Custom commands
\newcommand{\pderiv}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\pdderiv}[2]{\frac{\partial^2 #1}{\partial #2^2}}
\renewcommand{\vec}[1]{\mathbf{#1}}
% Change exam class formats
\renewcommand{\subpartlabel}{\thesubpart}
\renewcommand{\thesubpart}{\textbf{(\roman{subpart})}}
\renewcommand{\thepartno}{\textbf{\alph{partno}}}
\renewcommand{\thequestion}{\textbf{Question \arabic{question}}}
\pointsdroppedatright 

\begin{document}
%
% Cover page
\begin{coverpages}
\begin{flushleft}
\textbf{\Large SCEE09004 --- Partial Differential Equations 3}\\
\textsl{\large School of Engineering \\
College of Science and Engineering\\
The University of Edinburgh}

\vfill
Last edited \today

\begin{tabular}{|r|cccc|} \hline
&Q1&Q2&Q3&Q4\\ \hline \hline
Set by & DF & DF & DI & DI \\ 
Checked by& DI & DI & DF & DF  \\ \hline
\end{tabular}

\vfill
\textit{Temporary cover page, with full rubric, to be replaced by ETO.}
\vfill
\textsl{Answer 3 of the 4 questions}
\vfill
\end{flushleft}

\begin{minipage}[c]{0.7\columnwidth}
\begin{flushleft}
Solutions will not receive full marks unless supported by an appropriate explanation\\ [12pt]

\underline{In the examination it is permitted to have:}

\textbf{Calculators approved by the College of Science and Engineering for use in examinations:} Casio fx85 (any version), Casio fx83 (any version) and Casio fx82 (any version). \\ [12pt]
\end{flushleft}
\end{minipage}
\hfill\pointtable[v][questions]

\end{coverpages}
%
% This is the start of the questions
%Please follow the template (a) - "I am not a robot" type question 5/20, (b) - Assessment question for 15 minutes work 12/20, and (c) - Sting in the tail question 3/20. If the students will need to utilise any particular formulas from the help given at the back of the script please add a relevant hint in the question. 

\begin{questions}
% reset the figure label to contain the Question number and  then reset the figure counter for each question
\renewcommand{\thefigure}{Q\arabic{question}-\arabic{figure}}
\setcounter{figure}{0}

%------------------------------------------------------------------------%
% Analytical questions set by Daniel Friedrich
\question\quad
\pointformat{(\bfseries\boldmath\themarginpoints)}

Consider the steady-state heat conduction through the wall of a cylindrical pipe. This can be described by the Laplace equation in cyclindrical coordinates
\begin{align}
\pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\label{eq:Laplace_equation_201812}
\end{align}
where $u$ is the non-dimensional temperature.

The inner and outer radii of the pipe are given by $r=1$ and $r=2$, respectively. The inside surface of the pipe is held at 
\begin{equation}
u(1,\theta) = 0, \text{for } 0\le\theta<2\pi
\label{eq:LE_BC1_201812}
\end{equation}
while the non-dimensional temperature at the outer surface is given by 
\begin{equation}
u(2,\theta) = f(\theta), \text{for } 0\le\theta<2\pi
\label{eq:LE_BC2_201812}
\end{equation}

\begin{parts}
%----- Part ------------------------------------------%
\part[10] Find the separated solution of the PDE~\ref{eq:Laplace_equation_201812} which fulfils boundary conditions~\ref{eq:LE_BC1_201812} and \ref{eq:LE_BC2_201812} for an arbitrary function $f(\theta)$. Show that it is
\[
u(r, \theta) = k\log(r) + \sum_{n=1}^\infty \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) \left(r^{n} - r^{-n}\right) 
\]

\droppoints
\begin{solution}
We are looking for separated solutions of the Laplace equation~\ref{eq:Laplace_equation_201812} of the form $u(r,\theta) = R(r) \Theta(\theta)$. This gives us two ordinary differential equations
\begin{align*}
\frac{d^2 R}{dr^2} \Theta + \frac{1}{r} \frac{dR}{dr}\Theta + \frac{1}{r^2}\frac{d^2\Theta}{d\theta^2} &= 0 \\
-\frac{\Theta''}{\Theta} &= \frac{R'' + \frac{R'}{r}}{\frac{R}{r^2}} = \lambda
\end{align*}
The solutions of these two equations will depend on the value of $\lambda$ and we need to consider three cases. \hfill[1]

For $\lambda=0$ we get the solution
\begin{align*}
u_0(r, \theta) &= (a\theta + b)(C\ln(r) + D)
\end{align*}
with parameters $a$, $b$, $C$ and $D$. We know that the solution needs to be periodic in $2\pi$ and this is only the case for $a=0$. At the inner boundary we need to have $u_0(1,\theta)=0$ so that
\begin{align*}
u_0(1, \theta) &= C \ln(1) + D \overset{!}{=} 0 
\end{align*}
and from this it follows that $D=0$. Thus, we are left with
\begin{align*}
u_0(r,\theta) = k \ln(r)
\end{align*}
where $k=bC$ is a constant which needs to be determined later. \hfill[2]

For $\lambda<0$ we get the solution
\begin{align*}
u_-(r,\theta) &= \left(A\cosh(\mu\theta) + B\sinh(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
with parameters $A$, $B$, $C$ and $D$, and where we set $\lambda = -\mu^2$. Again, this needs to be periodic in $2\pi$ which is only fulfilled for $A=B=0$. Thus the solution for $\lambda<0$ is zero. \hfill[1]

For $\lambda = \mu^2 > 0$ we get the solution
\begin{align*}
u_+(r,\theta) &= \left(A\cos(\mu\theta) + B\sin(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
with parameters $A$, $B$, $C$ and $D$. Again this needs to be periodic in $2\pi$ and this is the case only for $\mu=n$ with $n\in\mathbb{N}$.\hfill[2]

To fulfil the inner boundary condition it is necessary to have
\[
u_+(1, \theta) = \left(A\cos(n\theta) + B\sin(n\theta)\right) \left(C 1^{-n} + D 1^{n}\right) \overset{!}{=} 0, \quad 0\le\theta\le2\pi
\]
Since the first part is not zero for all $\theta$, it follows that $C=-D$. Thus the solution is 
\[
u_+(r, \theta) = \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) \left(r^{n} - r^{-n}\right) 
\]
where we have set $a_n = AD$ and $b_n = BD$. \hfill[2]

To fulfil the outer boundary condition we need to superimpose the solutions for $\lambda=0$ and $\lambda>0$. This is possible because we are dealing with a linear PDE. The complete solution for an arbitrary $f(\theta)$ is \hfill[2]
\[
u(r, \theta) = k\log(r) + \sum_{n=1}^\infty \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) \left(r^{n} - r^{-n}\right) 
\]
\end{solution}

%----- Part ------------------------------------------%
\part[8] Now consider the case where the condition at the outer pipe boundary is given by
\begin{equation}
u(2,\theta) = f_b(\theta) = \begin{cases}
\frac{\pi^2}{4} - \theta^2, \quad & -\pi/2\le\theta<\pi/2 \\
0, \quad & \pi/2\le\theta<3\pi/2
\end{cases}
\label{eq:LE_BC2b_201812}
\end{equation}
Derive the solution to the PDE~\ref{eq:Laplace_equation_201812} which fulfils the boundary conditions~\ref{eq:LE_BC1_201812} and \ref{eq:LE_BC2b_201812} with $f_b(\theta)$.

\droppoints
\begin{solution}
To match the outer boundary condition, we need to find the coefficients $a_n$ and $b_n$. At the outer boundary we need to have \hfill[1]
\begin{align*}
u(2, \theta) &= k\log(2) + \sum_{n=1}^\infty \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) \left(2^{n} - 2^{-n}\right) \\
 &\overset{!}{=} \begin{cases}
\frac{\pi^2}{4} - \theta^2, \quad & -\pi/2\le\theta<\pi/2 \\
0, \quad & \pi/2\le\theta<3\pi/2
\end{cases}
\end{align*}

The parameters $k$, $a_n$ and $b_n$ can be found with the usual method of the Fourier series.
The parameter $a_0 = k \log(2) 2$ is given by
\begin{align*}
a_0 &= \frac{2}{2\pi} \int_{0}^{2\pi} u(2, \theta) d\theta \\
  &= \frac{1}{\pi} \int_{-\pi/2}^{\pi/2}\left(\frac{\pi^2}{4} - \theta^2\right) d\theta = \frac{1}{\pi} \left[\frac{\pi^2\theta}{4} - \frac{\theta^3}{3} \right]_{-\pi/2}^{\pi/2} \\
  &= \frac{1}{\pi}\left(\frac{\pi^3}{8} - \frac{\pi^3}{24} - \left(-\frac{\pi^3}{8} + \frac{\pi^3}{24}\right)\right) = \frac{\pi^2}{4} - \frac{\pi^2}{12} = \frac{\pi^2}{6}
\end{align*}
From this it follows that $k$ is given by \hfill[2]
\begin{align*}
k &= \frac{a_0}{2\log(2)} = \frac{\pi^2}{12\log(2)}
\end{align*}

The parameters $a_n$ are given by \hfill[2]
\begin{align*}
a_n &= \frac{1}{\left(2^{n} - 2^{-n}\right)} \frac{2}{2\pi} \int_{0}^{2\pi} u(2, \theta) \cos(n\theta) d\theta \\
 &= \frac{1}{\left(2^{n} - 2^{-n}\right)} \frac{1}{\pi} \int_{-\pi/2}^{\pi/2}\left(\frac{\pi^2}{4} - \theta^2\right) \cos(n\theta) d\theta \\
  &= \frac{2}{\pi\left(2^{n} - 2^{-n}\right)}\int_{0}^{\pi/2}\left(\frac{\pi^2}{4} - \theta^2\right) \cos(n\theta) d\theta \\
&=  \frac{2}{\pi\left(2^{n} - 2^{-n}\right)} \left[\frac{\pi^2}{4n} \sin(n\theta) - \frac{2\theta}{n^2} \cos(n\theta) - \left(\frac{\theta^2}{n} - \frac{2}{n^3}\right) \sin(n\theta) \right]_{0}^{\pi/2} \\
 &=  \frac{2}{\pi\left(2^{n} - 2^{-n}\right)} \left[\frac{\pi^2}{4n} \sin(n\pi/2) - \frac{\pi}{n^2} \cos(n\pi/2) - \left(\frac{\pi^2}{4n} - \frac{2}{n^3}\right) \sin(n\pi/2) \right] \\
 &=  \frac{2}{\pi\left(2^{n} - 2^{-n}\right)} \left[\frac{2}{n^3} \sin(n\pi/2) - \frac{\pi}{n^2} \cos(n\pi/2) \right] \\
 &= \begin{cases}
 \frac{2}{\pi\left(2^{n} - 2^{-n}\right)} \frac{2}{n^3} (-1)^{(n-1)/2}, \quad & n \text{ odd} \\
 \frac{2}{\pi\left(2^{n} - 2^{-n}\right)} \frac{\pi}{n^2} (-1)^{n/2 + 1}, \quad & n \text{ even} 
 \end{cases} 
\end{align*}

The parameters $b_n$ are given by \hfill[2]
\begin{align*}
b_n &= \frac{1}{\left(2^{n} - 2^{-n}\right)} \frac{2}{2\pi} \int_{0}^{2\pi} u(2, \theta) \sin(n\theta) d\theta \\
 &= \frac{1}{\left(2^{n} - 2^{-n}\right)} \frac{1}{\pi} \int_{-\pi/2}^{\pi/2}\left(\frac{\pi^2}{4} - \theta^2\right) \sin(n\theta) d\theta \\
 &= 0
\end{align*}
because the boundary condition $f(\theta)$ is an even function in $\theta$.

Thus, the complete solution is \hfill[1]
\begin{align*}
u(r,\theta) =& \frac{\pi^2}{12\log(2)}\log(r) + \sum_{i=1}^\infty \frac{2(r^{2i-1} - r^{-(2i-1)})}{\pi\left(2^{2i-1} - 2^{-(2i-1)}\right)} \frac{2}{(2i-1)^3} (-1)^{i-1} \cos((2i-1)\theta) \\
&+ \sum_{i=1}^\infty \frac{2(r^{2i} - r^{-2i})}{\left(2^{2i} - 2^{-2i}\right)} \frac{1}{(2i)^2} (-1)^{i-1} \cos(2i\theta)
\end{align*}
\end{solution}

%----- Part ------------------------------------------%
\part[2] How would the solution change qualitatively compared to part (b) if the outer boundary condition is given by
\[
u(2, \theta) = f_c(\theta) = \begin{cases}
\theta, \quad & 0\le\theta<\pi \\
\theta^2, \quad & \pi\le\theta<2\pi
\end{cases}
\]

\droppoints
\begin{solution}
The function $f_c(\theta)$ is neither odd nor even and thus the solution would contain both sine and cosine terms. \hfill[1]

In addition, the function $f_c(\theta)$ has jump discontinuities and thus the solution would have lower convergence order. \hfill[1]
\end{solution}
\end{parts}

\newpage
%------------------------------------------------------------------------%
% Analytical questons set by Daniel Friedrich
\newpage
\question \quad\\
Consider the one-dimensional wave equation
\begin{align}
\frac{\partial^2u}{\partial t^2} &= 9 \frac{\partial^2u}{\partial x^2} & \quad  \label{eq:wave_equation_201812}\\
\intertext{on the interval $x\in(-\infty,2\pi]$ with the initial and boundary conditions }
 u(x, 0) &= \cos(x), \quad \forall x\in(-\infty,2\pi], \label{eq:we_IC1_201812} \\
\left. \frac{\partial u(x, t)}{\partial t}\right|_{t=0} &= 0, \quad \forall x\in(-\infty,2\pi], \label{eq:we_IC2_201808} \\
 \lim_{x\to-\infty}u(x,t) &= 0, \quad \forall t. \label{eq:we_BC1_201812} \\
 u(2\pi, t) &= \exp(-t), \quad \forall t, \label{eq:we_BC2_201812}
\end{align}

\begin{parts}
%----- Part -----------------------------------------------------------%
\part[5] The partial differential equation~(\ref{eq:wave_equation_201812}) and the boundary and initial conditions~(\ref{eq:we_IC1_201812}-\ref{eq:we_BC2_201812}) model the behaviour of a string. 

Sketch the domain on which the given problem is to be solved and indicate the given initial and boundary conditions. Clearly label the sketch. Comment on the physical quantity calculated by the model as well as the initial state and boundary conditions.

\droppoints
\begin{solution}
\begin{itemize}
\item Sketch of the problem with proper axis and initial/boundary condition labelling \hfill[2]
\item The PDE models the displacement of a string, which is initially displaced by $\cos(x)$ and its initial velocity is zero. \hfill[2]
\item The right side of the string at $x=1$ is moved from $1$ to zero while for $x\to-\infty$ the string is at rest. \hfill[1]
\end{itemize}
\end{solution}

%----- Part -----------------------------------------------------------%
\part[9] Apply the Laplace transform with respect to $t$ to the PDE~(\ref{eq:wave_equation_201812}) and find the general solution for $U(x,s)$ in the Laplace domain.

\droppoints
\begin{solution}
The Laplace transform of the left hand side of the partial differential equation can be found using the derivative property and on the right hand side we can swap the order of integration and differentiation. This gives us the transformed equation
\begin{align}
s^2 U(x, s) - s u(x, 0) - u'(x,0) &= 9 {\cal L} \left\{\frac{\partial^2 u(x,t)}{\partial x^2} \right\} = 9\frac{\partial^2 U(x,s)}{\partial x^2} 
\label{eq:we_transformed_201812}
\end{align}

Insert the initial conditions into Eq.~(\ref{eq:we_transformed_201812}) to get
\begin{align}
s^2 U(x, s) - s\cos(x) &= 9 \frac{\partial^2 U(x,s)}{\partial x^2} \nonumber\\
\Rightarrow \frac{\partial^2 U(x,s)}{\partial x^2} - \frac{s^2}{9} U(x, s) &= \frac{-s}{9}\cos(x) \label{eq:we_laplace_201812}
\end{align}
\hfill[2]

The differential equation~(\ref{eq:we_laplace_201812}) has the characteristic equation
\begin{align*}
m^2 - \frac{s^2}{9} &= 0 \\
\intertext{which has the zeros}
 m_{1,2} &= \pm \frac{s}{3} \\
\end{align*}
Thus the complimentary function is
\begin{align}
U_g(x,s) &= A(s) e^{sx/3} + B(s) e^{-sx/3} \label{eq:we_general_solution_201812}
\end{align}
where $A(s)$ and $B(s)$ are functions in the Laplace domain which we need to determine from the boundary conditions. \hfill[2]

Before we find the two functions we need to take care of the inhomogeneity of the differential equation. The trial solution for the particular integral for the inhomogeneous differential equation~(\ref{eq:we_laplace_201812}) is
\begin{align*}
U_p(x,s) &= P\sin(x) + Q \cos(x)
\end{align*}
where the coefficients $P$ and $Q$ need to be determined. We substitute $U_p$ and its second derivative with respect to $x$ into Eq.~(\ref{eq:we_laplace_201812}) and get \hfill[2]
\begin{align*}
-P \sin(x) - Q\cos(x) - \frac{s^2}{9} \left( P\sin(x) + Q\cos(x)\right) &= -\frac{s}{9} \cos(x)
\end{align*}
We can arrange this in terms of sine and cosine
\[
\sin(x) \left(-P - \frac{s^2}{9} P\right) + \cos(x) \left(-Q - \frac{s^2}{9} Q + \frac{s}{9}\right) = 0
\]
From this we get two equations 
\begin{align*}
-P - \frac{s^2}{9} P &= 0 \\
-Q - \frac{s^2}{9} Q + \frac{s}{9} &= 0
\end{align*}

From this it follows that
\begin{align*}
P &= 0 \\
Q &= \frac{s}{s^2 + 9}
\end{align*}
for $\Re(s)>0$.\hfill[2]

Thus, the general solution of the differential equation is of the form
\begin{align*}
U(x,s) &= A(s) e^{sx/3} + B(s) e^{-sx/3} + \frac{s}{s^2 + 9} \cos(x)
\end{align*}
\hfill[1]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[6] Given that the general solution to the PDE~(\ref{eq:wave_equation_201812}) is
\begin{align*}
U(x,s) &= A(s) e^{sx/3} + B(s) e^{-sx/3} + \frac{s}{s^2 + 9} \cos(x)
\end{align*}
use the boundary conditions~(\ref{eq:we_BC1_201812}-\ref{eq:we_BC2_201812}) to find the particular solution. Use the inverse Laplace Transform on the particular solution to get the solution of the wave equation~(\ref{eq:wave_equation_201812}-\ref{eq:we_BC2_201812}) in the time domain.

\droppoints
\begin{solution}
From the boundary condition~(\ref{eq:we_BC1_201812}) we know that the solution is bounded. Thus we can swap the limit and the integration to get
\begin{align*}
\lim_{x\to-\infty} U(x,s) &= \lim_{x\to-\infty} \int_0^\infty e^{-st} u(x,t) dt = \int_0^\infty  e^{-st} \lim_{x\to-\infty} u(x,t) dt = 0
\end{align*}
From this it follows that
\begin{align*}
B(s) &= 0, 
\end{align*}
because otherwise the second term on the right hand side of Eq.~(\ref{eq:we_general_solution_201812}) would go to infinity for $x\to-\infty$.
\hfill[2]

The boundary condition~(\ref{eq:we_BC2_201812}) requires that
\begin{align*}
U(2\pi,s) &= A(s)e^{2\pi s/3} + \frac{s}{s^2 + 9} \overset{!}{=} {\cal L} \left\{u(2\pi,t) \right\} =  \frac{1}{s + 1}, \quad \Re(s)>-1,
\end{align*}
and thus
\begin{align*}
 A(s) &= \left(\frac{1}{s + 1} - \frac{s}{s^2 + 9}\right) e^{-2\pi s/3}
\end{align*}
\hfill[2]

From the solution in the Laplace domain 
\begin{align*}
U(x,s) &= \left(\frac{1}{s + 1} - \frac{s}{s^2 + 9}\right) e^{-2\pi s/3} e^{sx/3} + \frac{s}{s^2 + 9} \cos(x) \\
 &= \left(\frac{1}{s + 1} - \frac{s}{s^2 + 9}\right) e^{s(x-2\pi)/3} + \frac{s}{s^2 + 9} \cos(x)
\end{align*}
we can find the solution in the time domain by using the time shifting property and the function transforms of the exponential and cosine functions.
\begin{align*}
u(x,t) =& \left(\exp[-(t+ (x-2\pi)/3)] - \cos[3(t + (x-2\pi)/3)] \right) H(t+(x-2\pi)/3) \\
   & + \cos(3t)\cos(x) H(t)
\end{align*}
\hfill[2]
\end{solution}

\end{parts}

\newpage
%------------------------------------------------------------------------%
% First Numerical questions set by David Ingram
\question\quad\\
The two dimensional heat conduction equation is
\[\pderiv{T}{t}=\alpha\left(\pdderiv{T}{x}+\pdderiv{T}{y}\right)\]
where $T$ is the temperature and $\alpha=\frac{k}{C_p\rho}$ is the thermal diffusivity, which depends on the thermal conductivity $k$, the mass density $\rho$, and the specific heat capacity $C_p$.
\begin{parts}

%----- Part -----------------------------------------------------------%
\part [6] Show than an explicit scheme for discretising the governing equation is; 
\[T^{n+1}_{i,j}=T^n_{i,j}+\frac{\alpha\Delta t}{\Delta x^2}\left(T^n_{i+1,j}-2T^n_{i,j}+T^n_{i-1,j}\right)
+\frac{\alpha\Delta t}{\Delta y^2}\left(T^n_{i,j+1}-2T^n_{i,j}+T^n_{i,j-1}\right).\]
State the formal accuracy of the scheme in both time and space.
\droppoints
\begin{solution}
Substituting the first order forward approximation, \[\pderiv{T}{t}\approx\frac{T^{n+1}_{i,j}-T^{n}_{i,j}}{\Delta t},\]
and the second order central approximations,
\[\pdderiv{T}{x}\approx\frac{T^n_{i+1,j}-2T^n_{i,j}+T^n_{i-1,j}}{\Delta x^2}\text{ and }
\pdderiv{T}{y}\approx\frac{T^n_{i,j+1}-2T^n_{i,j}+T^n_{i,j-1}}{\Delta y^2}\]
into the PDE we obtain \hfill[2]
\[\frac{T^{n+1}_{i,j}-T^{n}_{i,j}}{\Delta t}=\alpha\left(
\frac{T^n_{i+1,j}-2T^n_{i,j}+T^n_{i-1,j}}{\Delta x^2}+
\frac{T^n_{i,j+1}-2T^n_{i,j}+T^n_{i,j-1}}{\Delta y^2}\right)\]
\hfill[1]

multiplying by $\Delta t$ b.s.
\[{T^{n+1}_{i,j}-T^{n}_{i,j}}=\frac{\alpha\Delta t}{\Delta x^2}\left(T^n_{i+1,j}-2T^n_{i,j}+T^n_{i-1,j}\right)+
\frac{\alpha\Delta t}{\Delta y^2}\left(T^n_{i,j+1}-2T^n_{i,j}+T^n_{i,j-1}\right)\]
adding $T^{n}_{i,j}$ b.s.
\[T^{n+1}_{i,j}=T^n_{i,j}+\frac{\alpha\Delta t}{\Delta x^2}\left(T^n_{i+1,j}-2T^n_{i,j}+T^n_{i-1,j}\right)
+\frac{\alpha\Delta t}{\Delta y^2}\left(T^n_{i,j+1}-2T^n_{i,j}+T^n_{i,j-1}\right).\]
\hfill[1]

The solution is 2nd order in time and 1st order in space \hfill[2]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[8]By using the following locally one dimensional splitting:
\[\left.\begin{matrix}\pderiv{T}{t}=\alpha\pdderiv{T}{x}\\
\pderiv{T}{t}=\alpha\pdderiv{T}{y}\end{matrix}\right\}.\]
show that the factored operator sequence, \(T^{n+1}=L_x(\Delta t)L_y(\Delta t)\cdot T^n,\)
has the same formal accuracy as the un-split method.
\droppoints
\begin{solution}
The two dimensional operator, $L_{xy}$ is
\begin{equation*}
L_{xy}(\Delta t)=1+\alpha\Delta t\left[\frac{\partial^2 }{\partial x^2}+\frac{\partial^2 }{\partial y^2}\right].
\label{e:2Dop}
\end{equation*} \hfill[2]

Consider,
\[T(x,y,t+\Delta t)=L_x(\Delta t)\cdot L_y(\Delta t)\cdot T(x,y,t).\]
\begin{align*}
T(t+\Delta t)&=\left[1+\alpha\Delta t\frac{\partial^2}{\partial x^2} \right]\cdot
\left[1+\alpha\Delta t\frac{\partial^2}{\partial y^2}\right]\cdot T(x,y,t)\\
&=\left(1+\alpha\Delta t\left[\frac{\partial^2}{\partial x^2}+\frac{\partial^2}{\partial y^2}\right]\right)
\cdot T(x,y,t)+O(\Delta t^2)\\
&=L_{xy}(\Delta t)\cdot T(x,y,t)
\end{align*}\hfill[4]

So the operator sequence is equivalent to the 2D operator and both will be 2nd order accurate.\hfill[2]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[6] Write down difference scheme representations of the differential operators $L_x(\Delta t)$ and $L_y(\Delta t)$  showing how the above operator sequence would be implemented and indicating the advantages of the splitting scheme.
\droppoints
\begin{solution}
\[L_x(\Delta t).T=T+\kappa\Delta t\pdderiv{T}{x}\]
replacing \(\pdderiv{T}{x}\) with the central 2nd order finite difference approximation we have
\[\tilde{T}_{i,j}^{n+1}=T_{i,j}^n+\alpha\frac{T^n_{i-1,j}-2T^n_{i,j}+T^n_{i+1,j}}{\Delta x^2}.\] \hfill[1]

\[L_y(\Delta t).T=T+\kappa\Delta t\pdderiv{T}{y}\]
replacing \(\pdderiv{T}{y}\) with the central 2nd order finite difference approximation we have
\[{T_{i,j}}^{n+1}=\tilde{T}_{i,j}^n+\alpha\frac{\tilde{T}^n_{i,j-1}-2\tilde{T}^n_{i,j}+\tilde{T}^n_{i,j-1}}{\Delta y^2}.\] \hfill[1]

The advantage of this scheme is that the maximum time step is
\[\Delta t=\min\left(\frac{\Delta x^2}{2\alpha},\,\frac{\Delta y^2}{2\alpha}\right)\]
which is much less restrictive that the 2D time step.\hfill[1]

The implementation would be as follows:
\begin{verse}
set the initial conditions, $T^0_{ij}\ \forall\ i,\,j$ \\
compute $$\Delta t=\min\left(\frac{\Delta x^2}{2\kappa},\,\frac{\Delta y^2}{2\kappa}\right).$$\\
set $n=0$ and $t=0.0$\\
repeat \\
~~~apply boundary conditions \\
~~~compute $$\widetilde{T_{ij}^{n+1}}=L_y(\Delta t).T_{ij}^n\ \forall\ i,\,j$$ \\
~~~compute $$T_{ij}^{n+1}=L_x(\Delta t).\widetilde{T_{ij}^{n+1}}\ \forall\ i,\,j$$ \\
~~~increment $n$\\
~~~set $t=t+\Delta t$\\
until $t\ge t_{stop}$.
\end{verse} \hfill[3]

\end{solution}
\end{parts}

\newpage
%------------------------------------------------------------------------%
% Second Numerical questions set by David Ingram
% based on 0506 Q3 - St Venant Equations

\question\quad\\
The homogeneous form of the Saint-Venant equations is,
\begin{equation*}
\frac{\partial\vec{U}}{\partial t}+\frac{\vec{F}(\vec{U})}{\partial x}=0\text{ where }
\vec{U}=\left(\begin{matrix}\phi\\ \phi u\end{matrix}\right)\text{ and }
\vec{F}(\vec{U})=\left(\begin{matrix}\phi u\\ \phi u^2+\frac{\phi^2}{2}\end{matrix}\right),
\end{equation*}
$u$ is the local fluid velocity, $\phi=gh$ is the geopoential, $g$ is the acceleration due to gravity and $h$ is the local water depth.  This system of equations can be used to model flow in an open channel with constant cross section and constant bathymetry.

%----- Part -----------------------------------------------------------%
\begin{parts}
\part[5] Write this system of equations in quasi-linear form \(\vec{U}_t+J\vec{U}_x=0\) and hence find the Jacobian matrix, $J$.
\droppoints
\begin{solution}
For the quasi-linear form \(\vec{U}_t+J\vec{U}_x=0\), $J$ is given by
\[J=\frac{\partial\vec{F}}{\partial\vec{U}}=
\begin{pmatrix}\frac{\partial f_1}{\partial u_1}&\frac{\partial f_1}{\partial u_2}\\
\frac{\partial f_2}{\partial u_1}&\frac{\partial f_2}{\partial u_2}\end{pmatrix}.\]
\hfill[1]

Let $p=\phi$ and $q=\phi u$\\
Now $F_1=q$ and $F_2=q^2/p+p^2/2$ \hfill[1]\\

Differentiating w.r.t $p$
\[
\frac{\partial}{\partial p}q=0\mbox{ and }
\frac{\partial}{\partial p}q^2/p+p^2/2=-q^2/p^2+p
\] \hfill[1]

Differentiating w.r.t. $q$
\[
\frac{\partial}{\partial q}q=1\mbox{ and }
\frac{\partial}{\partial q}q^2/p+p^2/2=2q/p
\]\hfill[1]

So
\[
J=\begin{pmatrix}0 & 1 \\-q^2/p^2+p & 2q/p\end{pmatrix}=\begin{pmatrix}0 & 1 \\ -u^2+\phi & 2u\end{pmatrix}
\] \hfill[1]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[5] Find the two eigenvalues of J and hence determine if the system is hyperbolic, parabolic or elliptic.
\droppoints
\begin{solution}
We solve the characteristic equation \(J-\lambda I=0\), so
\[ \left|\begin{matrix}-\lambda & 1 \\ -u^2+\phi & 2u-\lambda\end{matrix}\right|=0 \] \hfill[1]

Expanding the determinant we have
\begin{align*}
0&=-\lambda\times(2u-\lambda)-(-u^2+\phi)\times 1\\
&=\lambda^2-2u\lambda+u^2-\phi\\
&=(\lambda-u)^2-\phi\\
&\lambda=u\pm\sqrt{\phi}.
\end{align*}\hfill[2]

Since \(\phi\ge0,\  u\pm\sqrt{\phi}\) will always be real so the Jacobian has two distinct real roots.

Therefore the system is hyperbolic.\hfill[2]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[10] A dam break problem is to be simulated in a 2m long channel, \(-1\le x\le 1\), using 5 grid points, \(\Delta x=0.5\). The initial conditions, at $t=0$, are given by
\[\phi=\begin{cases}1&x\le0\\ 4&x>0\end{cases}\text{ and }u=1.0\]
Perform \textbf{ONE} time step (working to 2 d.p.) using the Lax-Friedrichs scheme 
\[\vec{U}_i^{n+1}=\frac{1}{2}\left(\vec{U}_{i+1}^n+\vec{U}_{i-1}^n\right)-\frac{\Delta t}{2\Delta x}\left(\vec{F}_{i+1}^n-\vec{F}_{i-1}^n\right)\]
using a Courant number $\nu=0.9$. You may assume Dirichlet boundary conditions at the ends of the domain, i.e. the geopotential at $x=\pm1$ is constant and that the associated velocity is unity.

\textbf{Remark:} It can be shown that the local wave speeds ($S_L$ and $S_R$) are \(S_{L,R}=u\pm\sqrt{\phi}\) and thus that for any explicit time marching method,
\[\Delta t=\nu\frac{\Delta x}{\max(|u-\sqrt{\phi}|,|u+\sqrt{\phi}|)}\]
 where $\nu<1$ is the Courant number.

\droppoints
\begin{solution}
\textbf{Time step}: 
\[\Delta t=\nu\frac{\Delta x}{\max(|u-\sqrt{\phi}|,|u+\sqrt{\phi}|)}\]
We have $u=1\ \forall x$, so
\[\Delta t=\nu\frac{0.5}{\max(|1-\sqrt{\phi}|,|1+\sqrt{\phi}|)}\]
The largest value of $\phi=4\rightarrow\sqrt\phi=2$ so we have
\[\Delta t=\nu\frac{0.5}{\max(|1-2|,|1+2|)}=\nu\frac{0.5}{\max(1,3)}=\frac{0.5}{3}\nu=\frac{\nu}{6}=0.15\text{ seconds}\]
\hfill[3]

\textbf{U vector}:
The $\vec{U}$ is \[\vec{U}=\left(\begin{matrix}\phi\\ \phi u\end{matrix}\right)\]

We have
\[\vec{U}=\begin{cases}\left(\begin{matrix}1\\ 1\end{matrix}\right)&x\le0 \\
\left(\begin{matrix}4\\ 1\end{matrix}\right)&x>0\end{cases}\]
\hfill[1]

\textbf{Fluxes}:
The flux vector is  \[\vec{F}(\vec{U})=\left(\begin{matrix}\phi u\\ \phi u^2+\frac{\phi^2}{2}\end{matrix}\right)\]

We have
\[\vec{F}(\vec{U})=\begin{cases}\left(\begin{matrix}1\\ 1.5\end{matrix}\right)&x\le0 \\
\left(\begin{matrix}4\\ 12\end{matrix}\right)&x>0\end{cases}\]
\hfill[1]

We now compute the parts of the Lax Friedrichs scheme:
\[\frac{\Delta t}{2\Delta x}=\frac{0.15}{2\times0.5}=0.15.\]
\hfill[1]

\begin{tabular}{lrrrrr} \hline
$i$ & 1 & 2 & 3 & 4 & 5 \\ 
$x_i$ & -1.00 & -0.50 & 0.00 & 0.50 & 1.00 \\ \hline  

$\phi$ & 1.00 & 1.00 & 1.00 & 4.00 & 4.00 \\
$u$ & 1.00 & 1.00 & 1.00 & 1.00 & 1.00 \\ \hline \hline

\multicolumn{6}{c}{$\vec{U}^n_i$}\\ \hline
$\phi$ & 1.00 & 1.00 & 1.00 & 4.00 & 4.00 \\
$\phi u$ & 1.00 & 1.00 & 1.00 & 4.00 & 4.00 \\ \hline

\multicolumn{6}{c}{$\vec{F}^n_i$}\\ \hline
$\phi u$ & 1.00 & 1.00 & 1.00 & 4.00 & 4.00 \\ 
$\phi u^2+\frac{\phi^2}{2}$ & 1.50 & 1.50 & 1.50 & 12.00 & 12.00 \\ \hline \hline 

\multicolumn{6}{c}{$\frac{1}{2}\left(\vec{U}^n_{i+1}+\vec{U}^n_{i-1}\right)$}\\ \hline
$\phi$ & 1.00 & 1.00 & 2.50 & 2.50 & 4.00 \\ 
$\phi u$ & 1.00 & 1.00 & 2.50 & 2.50 & 4.00 \\ \hline

\multicolumn{6}{c}{$\vec{F}^n_{i+1}-\vec{F}^n_{i-1}$}\\ \hline
$\phi$ & 0.00 & 0.00 & 3.00 & 3.00 & 0.00 \\
$\phi u$ & 0.00 & 0.00 & 10.50 & 10.50 & 0.00 \\ \hline

\multicolumn{6}{c}{$\vec{U}^{n+1}_i$}\\ \hline
$\phi$ & 1.00 & 1.00 & 2.05 & 2.05 & 4.00 \\ 
$\phi u$ & 1.00 & 1.00 & 0.93 & 0.93 & 4.00 \\ \hline \hline
\end{tabular}\hfill[4]
\end{solution}
\end{parts}

\end{questions}
\medskip\hfill\textbf{END OF EXAM}
%
% end of paper marker
%
\newpage
Useful integrals
\begin{align*}
\int x \sin ax \, \mathrm{d}x &= \frac{1}{a^2} \bigl ( \sin a x - a x \cos ax \bigr ) \\
\int x \cos ax \, \mathrm{d}x &= \frac{1}{a^2} \bigl ( \cos a x + a x \sin ax \bigr ) \\
\int x^2 \sin ax \, \mathrm{d}x &= \frac{2x}{a^2} \sin ax - \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \cos ax \\
\int x^2 \cos ax \, \mathrm{d}x &= \frac{2x}{a^2} \cos ax + \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \sin ax
\end{align*}

Finite difference approximations
\begin{align*}
\left.\frac{\partial u}{\partial x}\right|_{x_i}=\frac{u_{i+1}-u_i}{\Delta x}+O(\Delta x)\\
\left.\frac{\partial u}{\partial x}\right|_{x_i}=\frac{u_{i}-u_{i-1}}{\Delta x}+O(\Delta x)\\
\left.\frac{\partial u}{\partial x}\right|_{x_i}=\frac{u_{i+1}-u_{i-1}}{\Delta 2x}+O(\Delta x^2)\\
\left.\frac{\partial^2 u}{\partial x^2}\right|_{x_i}=\frac{u_{i+1}-2u_i+u_{i-1}}{\Delta x^2}+O(\Delta x^2)
\end{align*}

Euler formula
\[e^{{ix}}=\cos x+i\sin x,\text{ so } \cos x=\frac{e^{ix}+e^{-ix}}{2}\text{ and} \sin x=\frac{e^{ix}-e^{-ix}}{2i}\]

Fourier constituents for Von Neumann analysis
\begin{align*}
 u^{n+1}_j&=g^{n+1}e^{ij\theta}, \\
 u^{n}_{j}&=g^{n}e^{ij\theta}, \\
 u^{n}_{j\pm1}&=g^{n}e^{i(j\pm1)\theta},
\end{align*}

%----- Table -------------------------------------------------------%
\begin{table}[ht]
\caption{Properties of the unilateral Laplace transform} % title of Table
\centering % used for centering table
\begin{tabular}{|c|c|c|} % centered columns (4 columns)
\hline %inserts double horizontal lines
Property & $f(t), t\ge0$ & $F(s)$ \\ 
%heading
\hline 
Definition & $f(t)$ & $\int_0^\infty\exp(-st)f(t)dt$ \\ [0.3ex]
Inversion & $\frac{1}{j2\pi}\int_{c-j\infty}^{c+j\infty} \exp(st)F(s)ds$ & $F(s)$ \\ [0.3ex]
Linearity & $\alpha f(t) + \beta g(t)$ & $\alpha F(s) + \beta G(s)$ \\ [0.3ex]
Scaling & $f(\alpha t)$ & $\frac{1}{\alpha}F\left(\frac{s}{\alpha}\right)$ \\ [0.3ex]
Frequency shifting & $\exp(\alpha t)f(t)$ & $F(s-\alpha)$ \\ [0.3ex]
Time shifting & $f(t-\alpha) H(t-\alpha)$ & $\exp(-\alpha s)F(s)$ \\ [0.3ex]
First derivative & $f'(t)$ & $sF(s) - f(0)$ \\ [0.3ex]
nth derivative & $f^{(n)}(t)$ & $s^nF(s)-s^{n-1}f(0)-s^{n-2}f'(0)$ \\
      & & $-\dots-s f^{(n-2)}(0) - f^{(n-1)}(0)$  \\ [0.3ex]
Transform derivative & $t^n f(t)$ & $(-1)^n \frac{d^n F(s)}{ds^n}$ \\ [0.3ex]        
Integration & $\int_0^t f(\tau)d\tau$ & $\frac{1}{s}F(s)$  \\ [0.3ex]
\hline
\end{tabular}
\label{table:properties} % is used to refer this table in the text
\end{table}

%----- Table -------------------------------------------------------%
\begin{table}[ht]
\caption{Function transforms} % title of Table
\centering % used for centering table
\begin{tabular}{|c|c|c|c|} 
\hline %inserts double horizontal lines
Name & $f(t), t\ge0$ & $F(s)$ & Convergence region \\ [0.3ex] % inserts table
%heading
\hline % inserts single horizontal line
Unit impulse & $\delta(t)$ & $1$ & all $s$ \\ [0.3ex]
Ideal delay& $\delta(t-\alpha)$ & $\exp(-\alpha s)$ & $\Re(s)\ge\alpha$ \\ [0.3ex]
Unit step & $H(t)$ & $\frac{1}{s}$ & $\Re(s)>0$ \\ [0.3ex]
Ramp  & $t$ & $\frac{1}{s^2}$ & $\Re(s)>0$ \\ [0.3ex]
nth power  & $t^n, n=1,2,\dots$ & $\frac{n!}{s^{n+1}}$ & $\Re(s)>0$ \\ [0.3ex]
Sine & $\sin(\alpha t)$ & $\frac{\alpha}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
Cosine & $\cos(\alpha t)$ & $\frac{s}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
Hyperbolic sine & $\sinh(\alpha t)$ & $\frac{\alpha}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
Hyperbolic cosine & $\cosh(\alpha t)$ & $\frac{s}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
Exponential decay & $\exp(-\alpha t)$ & $\frac{1}{s + \alpha}$ & $\Re(s)>-\alpha$ \\ [0.3ex]
Ramp hyperbolic sine& $t\sinh(\alpha t)$ & $\frac{2\alpha s}{(s^2 - \alpha^2)^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
\hline %inserts single line
\end{tabular}
\label{table:functions}
\end{table}

\end{document}