% ----------------------------------------------------------
% Fourier Series set by Daniel Friedrich
% ----------------------------------------------------------
% Integrals of odd/even functions
% Q13
\question[4] Consider the periodic function $f(x)$ with period $2\pi$ given by
\[
f(x) = -x^2, \quad -\pi\le x\le \pi
\]

If $n=1$, which of the following options is true?
\begin{choices}
\CorrectChoice{\[\int_{-\pi}^{\pi} f(x) \cos(nx) dx = 4\pi\]}
\choice{\[\int_{-\pi}^{\pi} f(x) \sin(nx) dx =-4\pi\]}
\choice{\[\int_{-\pi}^{\pi} f(x) \cos(nx) dx = -\pi\]}
\choice{\[\int_{-\pi}^{\pi} f(x) \sin(nx) dx =2\pi\]}
\end{choices}
\droppoints
\begin{solution}
The integral with cosine is given by
\begin{align*}
-\int_{-\pi}^{\pi} x^2 \cos(nx) dx &= -\left[\frac{2x}{n^2} \cos nx + \left(\frac{x^2}{n} - \frac{2}{n^3}\right) \sin nx\right]_{-\pi}^{\pi} \\
&= \frac{4\pi}{n^2}(-1)^{n+1}
\end{align*}
and the integral with the sine is given by
\[
-\int_{-\pi}^{\pi} x^2 \sin(nx) dx = -\left[\frac{2x}{n^2} \sin nx + \left(\frac{x^2}{n} - \frac{2}{n^3}\right) \cos nx\right]_{-\pi}^{\pi} = 0
\]
\end{solution}

% ----------------------------------------------------------
% Harmonic oscillator
% Q14
\newpage
\question[5] The mass-spring-damper system in figure \ref{fig:mass-spring-damper_system} can be described by the differential equation
\begin{align*}
\frac{\mathrm{d}^2 x}{\mathrm{d}t^2} &+ 2\zeta\omega\frac{\mathrm{d}x}{\mathrm{d}t} + \omega^2 x = f(t)
\end{align*}
\vskip-0.2cm
with the forcing term $f(t)$, the damping parameter $\zeta=\frac{c}{2}\sqrt{(km)^{-1}}\ge0$ and the radian frequency of free oscillation $\omega=\sqrt{\frac{k}{m}}>0$.
\begin{figure}[h]
\centering
\includegraphics[width=9.0cm]{EC_damped_trolley}
\caption{Mass-spring-damper system}
\label{fig:mass-spring-damper_system}
\end{figure}

%For the forcing term
%\[f(t) = a\cos(\theta t)\]
%with amplitude $a$ and frequency $\theta\neq \omega$ the periodic solution of the system is
%\begin{align*}
%x &= \frac{a}{\sqrt{(\omega^2-\theta^2)^2 + 4\zeta^2\omega^2\theta^2}} \cos(\theta t - \varphi) \\
%& \qquad\text{with } \varphi=\tan^{-1}\left(\frac{2\zeta\omega\theta}{\omega^2 - \theta^2}\right)
%\end{align*}
%
If the forcing term is a periodic function $f(t)$ with Fourier series
\[
FS\left\{f(t)\right\} = \frac{9}{\pi^2} - \sum_{n=1}^\infty \frac{5}{(3n - 1)^3} \cos((3n-2)\Omega t)
\]
and the damping parameter is set to $\zeta=0$ which of the following options is true?
\droppoints
\begin{choices}
\CorrectChoice{A periodic solution does not exist for $\Omega=\frac{\omega}{4}$.}
\choice{The periodic solution has a finite number of harmonics.}
\choice{A periodic solution does not exist for $\Omega=\frac{\omega}{3}$.}
\choice{The periodic solution exists for all values of $\Omega$.}
\end{choices}

\begin{solution}
The periodic solution under the forcing is
\begin{align*}
x(t) &=\frac{a}{\omega^2-((3n-2)\Omega)^2} \cos(3n-2)\Omega t)
\end{align*}
The first harmonic attains infinite amplitude due to resonance at $\Omega=\frac{\omega}{4}$.

\hrule
The choice "A periodic solution does not exist for $\Omega=\frac{\omega}{3}$." is gained for a miscalculation of the frequency.

It should be awarded 1 mark.
\end{solution}

\newpage
% ----------------------------------------------------------
% Identify plot of periodic extension
% Q15
\question[4] Consider the function
\[ 
f(t) =   \begin{cases}
    t^2, & \text{for } 0 < t < 1 \\
    2 - t, & \text{for } 1 < t < 2
  \end{cases}
 \] 

Which of the following options shows the even periodic extension of $f(t)$ in the interval $-2< t < 2$?

\begin{choices}
\choice{$\quad$\\ \includegraphics[width=8cm,height=4.5cm]{Fourier_202208_odd_extension0}}
\CorrectChoice{$\quad$\\ \includegraphics[width=8cm,height=4.5cm]{Fourier_202208_odd_extension1}}
\choice{$\quad$\\ % Fudge to move the graphic below the choice label
	\includegraphics[width=8cm,height=4.5cm]{Fourier_202208_odd_extension2}}
\choice{$\quad$\\ \includegraphics[width=8cm,height=4.5cm]{Fourier_202208_odd_extension3}}
\end{choices}
\droppoints
\begin{solution}
The solution is the green, dashed function. 
The function needs to be mirrored on the y axis.
\end{solution}

\newpage
% ----------------------------------------------------------
% Convergence of the Fourier series
% Q16
\question[4] Consider the function
\[
f(t) = \frac{t^2}{2\pi} - t + 1 - \cos(t)
\]
valid in the finite interval $0 < t < 2\pi$.

Which of the following options is true?
\droppoints
\begin{choices}
\choice{The coefficients of the half-range sine series of $f(t)$ decrease as $1/n^2$.}
\choice{The coefficients of the half-range cosine series of $f(t)$ decrease as $1/n^3$.}
\choice{The coefficients of the full-range series of $f(t)$ decrease as $1/n$.}
\CorrectChoice{The coefficients of the half-range sine series of $f(t)$ decrease as $1/n^3$.}
\end{choices}
\begin{solution}
The coefficients of the half-range sine series decrease as $1/n^3$ because the odd extension is continuous with continuous first derivative.

The coefficients of the half-range cosine series decrease as $1/n^2$ because the even extension is continuous with discontinuous first derivative.

The coefficients of the full-range Fourier series decrease as $1/n^2$ because the full-range extension is continuous with discontinuous first derivative.
\end{solution}

% ----------------------------------------------------------
% Compute the value of pi from a Fourier series
% Q17
\question[5] Consider the function
\[ 
f(t) =   \begin{cases}
    -1, & \text{for } -\pi < t \le 0 \\
    0, & \text{for } 0 < t \le \pi
  \end{cases}
 \] 
which has the following full-range Fourier series
\[
\text{FS}\{f(t)\} &= -\frac{1}{2} + \sum_{n=1}^\infty \frac{2}{(2n-1)\pi}\sin((2n-1) t)  
\]
on the interval $-\pi < t \le \pi$.

Which of the following expressions is true?
\begin{choices}
\choice{\[\frac{\pi}{4} = \sum_{n=1}^\infty\frac{(-1)^{n}}{2n-1}\]}
\choice{\[\frac{\pi^2}{4} = \sum_{n=1}^\infty\frac{(-1)^{n}}{2n-1}\]}
\CorrectChoice{\[\frac{\pi}{4} = \sum_{n=1}^\infty\frac{(-1)^{n-1}}{2n-1}\]}
\choice{\[\frac{\pi}{4} = \sum_{n=1}^\infty\frac{(-1)^{n-1}}{(2n-1)^2}\]}
\end{choices}

\droppoints
\begin{solution}
Insert the $t=\frac{\pi}{2}$ into the expression
\begin{align*}
0 = -\frac{1}{2} + \sum_{n=1}^\infty \frac{2}{(2n-1)\pi}\sin\left((2n-1) \frac{\pi}{2}\right)  
\end{align*}

Rearrange to get
\[
\frac{\pi}{4} = \sum_{n=1}^\infty \frac{\sin\left((2n-1)\frac{\pi}{2}\right)  }{(2n-1)} = 
\sum_{n=1}^\infty\frac{(-1)^{n-1}}{2n-1}
\]
\hrule
The solution:
\[
\frac{\pi}{4} = \sum_{n=1}^\infty\frac{(-1)^{n}}{2n-1}
\]
has just one sign error.

It should be awarded 1 mark.
\end{solution}

\newpage
% ----------------------------------------------------------
% Full range Fourier series
% Q18
\question[6] Consider the function
\[ 
f(t) = \begin{cases}
    2+t, & \text{for } -2 < t < -1 \\
    -t, & \text{for } -1 < t < 1 \\
    -2+t, & \text{for } 1 < t < 2
  \end{cases}
 \] 

Which of the following options is the full-range Fourier series of $f(t)$?


%shuffled answers
\begin{choices}
\choice{ \[ FS\left\{f(t)\right\} = \frac{3}{2} + \sum_{k=1}^\infty \frac{2(-1)^{k+1}}{((k-0.5)\pi)^2} \sin\left((k-0.5)\pi t\right) + \sum_{k=1}^\infty \frac{2(-1)^{k}}{(k\pi)^2} \cos\left(k\pi t\right) \]  }
\choice{ \[ FS\left\{f(t)\right\} = \sum_{k=1}^\infty \frac{2(-1)^{k}}{((k-0.5)\pi)^3} \sin\left((k-0.5)\pi t\right) \]  }
\CorrectChoice{ \[ FS\left\{f(t)\right\} = \sum_{k=1}^\infty \frac{2(-1)^{k}}{((k-0.5)\pi)^2} \sin\left((k-0.5)\pi t\right) \]  }
\choice{ \[ FS\left\{f(t)\right\} = \sum_{k=1}^\infty \frac{2(-1)^{k-1}}{((k-0.5)\pi)^2} \sin\left((k-0.5)\pi t\right) \]  }
\end{choices}
\droppoints

\begin{solution}
The function $f(t)$ is odd and thus the Fourier series will have only sine terms. In addition, the function is continuous but has a discontinuous first derivative which will lead to the Fourier coefficients decreasing as $1/n^2$.

The Fourier series is given by
\begin{align*}
FS\left\{f(t)\right\} &= \frac{a_0}{2} + \sum_{n=1}^\infty b_n \sin\left(0.5n\pi t\right)
\intertext{with the Fourier coefficients}
b_n &= 0.5 \int_{-2}^2 f(t) \sin(0.5n\pi t) dt, \quad n=1,2,\dots \\
&= -\int_0^1 t \sin(0.5n\pi t) dt + \int_1^2 (t - 2) \sin(0.5n\pi t) dt
\end{align*}
The coefficients can be calculated by
\begin{align*}
b_n &= -\int_0^1 t \sin(0.5n\pi t) dt - \int_1^2 2\sin(0.5n\pi t) dt + \int_1^2 t\sin(0.5n\pi t) dt\\
 &= -\frac{1}{(0.5n\pi)^2}\left[\sin(0.5n\pi t) - 0.5n\pi t\cos(0.5n\pi t)\right]_0^1 \\
 & \quad + \left[2\frac{\cos(0.5n\pi t)}{0.5n\pi}\right]_1^2 
+ \frac{1}{(0.5n\pi)^2}\left[\sin(0.5n\pi t) + 0.5n\pi t\cos(0.5n\pi t)\right]_1^2 \\
&= -\frac{2}{(0.5n\pi)^2}\left[\sin(0.5n\pi) - 0.5n\pi \cos(0.5n\pi)\right] \\
& + \frac{2}{0.5n\pi}[\cos(n\pi) - \cos(0.5n\pi)] - \frac{1}{(0.5n\pi)^2}n\pi\cos(n\pi) \\
&= -\frac{2}{(0.5n\pi)^2}\sin(0.5n\pi) 
 \end{align*}
The Fourier series is then given by
\begin{align*}
FS\left\{f(t)\right\} &= \sum_{k=1}^\infty \frac{2(-1)^{k}}{((k-0.5)\pi)^2} \sin\left((k-0.5)\pi t\right) 
\end{align*}

\hrule
The solution
\[ FS\left\{f(t)\right\} = \sum_{k=1}^\infty \frac{2(-1)^{k-1}}{((k-0.5)\pi)^2} \sin\left((k-0.5)\pi t\right) \] 
has only a sign error in the second term.

It should be awarded 1 mark.
\end{solution}