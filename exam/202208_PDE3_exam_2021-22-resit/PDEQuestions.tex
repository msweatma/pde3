\question[4] A partial differential equation is defined by the wave equation, 
\[ \frac1{c^2}\frac{\partial^2u}{\partial t^2}=\frac{\partial^2u}{\partial x^2} \] 
together with the conditions
\begin{align*}
u(0,t) &= 0 &t>0,\\
u(0.5\pi,t) &= 1 &t>0,\\
\frac{\partial}{\partial t}u(x,0) &= 0 & 0\le x\le\pi&\text{, and}\\
u(x,0) &= 1-\frac{x}{\pi} & 0\le x\le\pi.
\end{align*}

If you were to solve this using the method of separated solutions, 
which of the following basic solution types would you use?

%shuffled answers
\begin{choices}
\CorrectChoice{  \(u=\cos\lambda ct\sin \lambda x\)}
\choice{ \(u=\cos\lambda ct\cos \lambda x\)}
\choice{  \(u=\sin\lambda ct\cos \lambda x\)}
\choice{  \(u=\sin\lambda ct\sin \lambda x\)}
\end{choices}\droppoints
\begin{solution}
To satisfy \(u(0,t)=0\) we require \(\sin\lambda x\) in the solutions.

To satisfy \(\frac{\partial}{\partial t}u(x,0)=0\) we need 
\[ \frac{\partial u}{\partial t}=\pm\sin\lambda ct\mid_{t=0}. \] 
Thus \[ u=\cos\lambda ct\sin \lambda x \] 
is the correct basic form of the solution.
\end{solution}

\newpage
\question[4] You are trying to solve the wave equation
together with the conditions,
\begin{align*}
u(0,t) &= 0 &t>0,\\
u(\pi,t) &= 0 &t>0,\\
\frac{\partial}{\partial t}u(x,0) &= 0 & 0\le x\le\pi&\text{, and}\\
u(x,0) &= x^2 - x, & 0\le x\le\pi.
\end{align*}
using the method of separated solutions. 

%Assuming that \(u(x,t)=\cos\lambda ct\sin\lambda x\) is the appropriate form of the solution,
Which of the following solutions is obtained using linear superposition?

%shuffled answers
\begin{choices}
\choice{ \[ u(x,t)=\sum_{n=1}^\infty b_n\cos nct \]  }
\choice{  \[ u(x,t)=\sum_{n=1}^\infty b_n\sin nx \]  }
\choice{  \[ u(x,t)=\sum_{n=1}^\infty b_n\sin nct \cos nx \]  }
\CorrectChoice{ \[ u(x,t) = \sum_{n=1}^\infty b_n\cos nct \sin nx\]  }
\end{choices}\droppoints
\begin{solution}
We have \[ u=\cos\lambda ct\sin \lambda x \] 
To match the initial condition \(\frac{\partial}{\partial t}u(x,0)=0\) we form the superposition of these solutions 
\[
u(x,t) = \sum_{n=1}^\infty b_n\cos nct \sin nx
\] 
We can do this because the wave equation is linear. We require that
\[
u(x,0) = \sum_{n=1}^\infty b_n\sin nx = x^2 - x,\ 0\le x\le \pi
\]
\end{solution}

\newpage
\question[4] Which of the following solutions to the wave equation, 
\[
\frac{1}{4}\frac{\partial^2u}{\partial t^2}=\frac{\partial^2 u}{\partial x^2}
\] 
which satisfies the initial conditions 
\begin{align*}
u(x,0) &= \sin(x), \quad \forall x\\
\frac{\partial u(x,0)}{\partial t} &= x \exp(-x^2), \quad \forall x.
\end{align*}is obtained using the d'Alembert solution?

You are reminded that the d'Alembert solution is
\[ u(x,t) = \frac 1 2 \Bigl[ F(x+2t) + F(x-2t)\Bigr ] + \frac{1}{4} \int_{x-2t}^{x+2t} G(z) \mathrm{d}z, \] 
where 
\[ F(x) = u(x,0), \quad G(x) = \frac{\partial u}{\partial x}(x,0). \] 

%shuffled answers
\begin{choices}
\choice{ \[ u(x,t) = u(x,t) = \frac12\left[\sin(x+2t)+\sin(x-2t)\right] \]  }
\CorrectChoice{ \[ u(x,t) = \frac12\left[\sin(x+2t)+\sin(x-2t)\right]+\frac{1}{8} (\exp(-(x-2t)^2) - \exp(-(x+2t)^2)\]  }
\choice{ \[ u(x,t) = \frac12\left[\sin(x+2t)+\sin(x-2t)\right]+\frac{1}{4} (\exp(-(x-2t)^2) - \exp(-(x+2t)^2)\]  }
\choice{ \[ u(x,t) = \frac12\left[\cos(x+2t)+\cos(x-2t)\right]+\frac{1}{16} (\exp(-(x-2t)^2) - \exp(-(x+2t)^2)\]  }
\end{choices}\droppoints
\begin{solution}
The d'Alembert solution (equation 9.19) is given by 
\[
u(x,t) = \frac12\left[F(x+2t)+F(x-2t)\right] +\frac{1}{2c}\int_{x-2t}^{x+2t} G(z)\ \text{d}z
\]
where 
\[
F(x) = u(x,0)\text{ and }G(x)=\frac{\partial u(x,0)}{\partial t}.
\]
We can directly insert $F(x)=\cos(x)$ into the solution but need to calculate the integral of $G(x)$. For 
\[
G(x) = x\exp(-x^2)
\] 
we get 
\begin{align*}
 \frac{1}{2c}\int_{x-2t}^{x+2t} G(z)\ \text{d}z &= \frac{1}{2c}\int_{x-2t}^{x+2t} z\exp(-z^2)\ \text{d}z = \frac{1}{2c}\left[\frac{-1}{2} \exp(-z^2)\right]_{x-2t}^{x+2t} \\
 &= \frac{1}{4c} (\exp(-(x-2t)^2) - \exp(-(x+2t)^2)
\end{align*}
In the integral we used the variable $z$ to distinguish the independent variable of the function $G$ from the limits of the integration.

After solving the integral we get the following solution
\begin{align*}
u(x,t) &= \frac12\left[\cos(x+2t)+\cos(x-2t)\right]+\frac{1}{4\times2} (\exp(-(x-2t)^2) - \exp(-(x+2t)^2) \\
&=\frac12\left[\cos(x+2t)+\cos(x-2t)\right]+\frac{1}{8} (\exp(-(x-2t)^2) - \exp(-(x+2t)^2)
\end{align*}
\end{solution}
 
\newpage
\question[4] Figure \ref{f:WEinitial} shows the initial concentration, $\rho$, of a pollutant being advected in a uniform steady flow. 
\begin{figure}[ht]
\setcounter{figure}{\thequestion}
\begin{center}
\begin{tikzpicture}
  \begin{axis}[xlabel={$x\text{ [m]}$}, ylabel={$\rho$},
    samples=500,
    width=0.5\textwidth,
    height=0.2\textwidth] 
    \addplot[black, thick, domain=-2:8] ({x},{exp(-x^2)});
 \end{axis}
 \end{tikzpicture}
 \end{center}
\caption{Partial density, $\rho$, of a pollutant being advected in a uniform steady flow, at $t=0$s.}
\label{f:WEinitial}
\end{figure}

This is being modelled using the wave equation \[ \frac{1}{c^2}\frac{\partial^2\rho}{\partial t^2}=\frac{\partial^2\rho}{\partial x^2} \]  where $c=2.5\text{ ms}^{-1}$.  Which of the following plots shows the solution of this equation at $t=2.0\text{ s}$?

%shuffled answers
\begin{multicols}{2}
\begin{choices}
\choice{\begin{tikzpicture}[baseline=(current bounding box.north)]
  \begin{axis}[xlabel={$x$}, ylabel={$\rho$},
    samples=500,
    width=0.45\textwidth,
    height=0.2\textwidth] 
    \addplot[black, thick, domain=-2:8] ({x},{exp(-(x-2.5)^2)});
 \end{axis}
 \end{tikzpicture}}
\choice{\begin{tikzpicture}[baseline=(current bounding box.north)]
  \begin{axis}[xlabel={$x$}, ylabel={$\rho$},
    samples=500,
    width=0.45\textwidth,
    height=0.2\textwidth] 
    \addplot[black, thick, domain=-2:8] ({x},{exp(-(x-3.5)^2)});
 \end{axis}
 \end{tikzpicture}}
\choice{\begin{tikzpicture}[baseline=(current bounding box.north)]
  \begin{axis}[xlabel={$x$}, ylabel={$\rho$},
    samples=500,
    width=0.45\textwidth,
    height=0.2\textwidth] 
    \addplot[black, thick, domain=-2:8] ({x},{exp(-(x-1)^2)});
 \end{axis}
 \end{tikzpicture}}
\CorrectChoice{\begin{tikzpicture}[baseline=(current bounding box.north)]
  \begin{axis}[xlabel={$x$}, ylabel={$\rho$},
    samples=500,
    width=0.45\textwidth,
    height=0.2\textwidth] 
    \addplot[black, thick, domain=-2:8] ({x},{exp(-(x-5)^2)});
 \end{axis}
 \end{tikzpicture}}
\end{choices}
\droppoints
\end{multicols}

\begin{solution}
The speed of propagation is $2\text{ ms}^{-1}$ so the wave will have propagated $2\times2.5=5\text{ m}$.
\end{solution}