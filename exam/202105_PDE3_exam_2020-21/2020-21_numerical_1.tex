
% -----------------------------------------------------
% QUESTION
% -----------------------------------------------------
\question 
\setcounter{equation}{0}
The conservation law form of the Euler equations, which model compressible gas dynamics, is

\begin{equation}
\frac{\partial\vec{U}}{\partial t}+\frac{\vec{F}(\vec{U})}{\partial x}=0\text{ where }
\vec{U}=\left(\begin{matrix}\rho\\ \rho u \\ e\end{matrix}\right)\text{ and }
\vec{F}(\vec{U})=\left(\begin{matrix}\rho u\\ \rho u^2+p \\ (e+p)u\end{matrix}\right),
\end{equation}
$u$ is the local fluid velocity, $\rho$ is the density, $e$ is internal energy and $p$ is the static pressure.  The system is closed using the equation of state for pressure in an ideal gas, \[p=(\gamma-1)\left(e-\frac\rho2u^2\right),\] where $\gamma=C_v/C_p$ is the ratio of specific heats. For dry air the ratio is given by $\gamma=1.4$.

%----- Part -----------------------------------------------------------%
\begin{parts}
\part[5] By substituting $m=\rho u$, show that the flux vector $\vec{F}$ can be written  in terms of only the variables $\rho$, $m$ and $e$ and the constant $\gamma$ as:

\[\vec{F}=\left(\begin{matrix}m \\
\frac{3-\gamma}{2}\frac{m^2}{\rho}+(\gamma -1)e \\
\gamma\frac{em}{\rho}-\frac{\gamma-1}{2}\frac{m^3}{\rho^2}
\end{matrix}\right)\]
\droppoints
\begin{solution}
Substituting $m=\rho u$ info $\vec{F}$ we have
\[\vec{F}(\vec{U})=\left(\begin{matrix}m \\ \frac{m^2}{\rho}+p \\ (e+p)\frac{m}{\rho}\end{matrix}\right).\] \hfill[1]

We now need to eliminate $p$. Substituting $m=\rho u$ into the pressure equation:
\[p=(\gamma -1)\left(e-\frac{m^2}{2\rho}\right)\] \hfill[1]

Consider the momentum flux,\(\frac{m^2}{\rho}+p\):
\begin{align*}
\frac{m^2}{\rho}+p &= \frac{m^2}{\rho}+(\gamma -1)\left(e-\frac{m^2}{2\rho}\right)\\
&=\frac{m^2}{\rho}+(\gamma -1)e+(\gamma -1)\frac{m^2}{2\rho}\\
&=\frac{3-\gamma}{2}\frac{m^2}{\rho}+(\gamma -1)e\\
\end{align*} \hfill[1]

Consider the energy flux,\((e+p)\frac{m}{\rho}\):
\begin{align*}
(e+p)\frac{m}{\rho} &= \frac{m}{\rho}\left[e+(\gamma -1)\left(e-\frac{m^2}{2\rho}\right)\right] \\
&= \frac{m}{\rho}\left[\gamma e -(\gamma -1 )\frac{m^2}{2\rho}\right]\\
&= \gamma\frac{em}{\rho}-\frac{\gamma-1}{2}\frac{m^3}{\rho^2}.
\end{align*} \hfill [1]
\[\text{So }\vec{F}=\left(\begin{matrix}m \\
\frac{3-\gamma}{2}\frac{m^2}{\rho}+(\gamma -1)e \\
\gamma\frac{em}{\rho}-\frac{\gamma-1}{2}\frac{m^3}{\rho^2}
\end{matrix}\right)\]
as required. \hfill[1]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[4] Find the Jacobian $J=\partial \vec{F}/\partial \vec{U}$ where \(\vec{U}=(\rho, m, e)^T\).

\droppoints
\begin{solution}
To find the Jacobian matrix we differentiate every term in $\vec F$ by every term in $\vec U$. 

This gives
\[J=\left(\begin{matrix}0 & 1 & 0 \\
\frac{\gamma-3}{2}\frac{m^2}{\rho^2} & \frac{3-\gamma}{2}\frac{m}{\rho} 
& \gamma -1 \\
-\frac{\gamma e m}{\rho^2}+(\gamma-1)\frac{m^3}{\rho^3} &
\frac{\gamma e}{\rho}+(1-\gamma)\frac{3m^2}{2\rho^2} & \frac{m\gamma}{\rho}
\end{matrix}\right).
\] \hfill[4]
\end{solution}
%----- Part -----------------------------------------------------------%
\part [5] It can be shown that the eigenvalues of the Jacobian matrix, $J$ are
\begin{align*}
\lambda_1 &= \frac{m}{\rho}-\sqrt{\frac{\gamma p}{\rho}}, \\
\lambda_2 &= \frac{m}{\rho}, \\
\lambda_3 &= \frac{m}{\rho}+\sqrt{\frac{\gamma p}{\rho}}.
\end{align*}

Briefly explain why this means that the Euler equations are a hyperbolic system of conservation laws. Explain how you would calculate the time step when solving the equations, assuming that the numerical method being used is stable if $\nu\le1$. 
\droppoints
\begin{solution}
The system has three equations and three real eigenvalues, the system must therefore by hyperbolic. \hfill[1]

The stable time step is given by
\[\Delta t = \nu\frac{\Delta x}{\max_i |\lambda|}\] \hfill[1]

Now
\[\max \lambda = \left|\frac{m}{\rho}-\sqrt{\frac{\gamma p}{\rho}}\right|\text{ or }
\left|\frac{m}{\rho}+\sqrt{\frac{\gamma p}{\rho}}\right|\]
using the triangle inequality
\[\max \lambda = \left|\frac{m}{\rho}\right|+\sqrt{\frac{\gamma p}{\rho}}.\]

The stable time step is therefore
\[\Delta t = \nu\frac{\Delta x}{\left|\frac{m}{\rho}\right|+\sqrt{\frac{\gamma p}{\rho}}}.\]
\hfill[2]

Since $\nu\le 1$, the maximum stable time step is
\[\Delta t = \frac{\Delta x}{\left|\frac{m}{\rho}\right|+\sqrt{\frac{\gamma p}{\rho}}}.\]
\hfill[1]
\end{solution}
%----- Part -----------------------------------------------------------%
\part[6] You have written a program to solve the compressible Euler equations using the unmodified MacCormack scheme and are testing it by applying it to the well know Sod shock tube problem for the following initial conditions

\begin{center}
    \begin{tabular}{rll}\hline
    & $x<0.5$  & $x\ge0.5$ \\ \hline \hline
    Density, $\rho$ & 1.0 & 0.125 \\
    Pressure, $p$ & 1.0 & 0.1 \\
    velocity, $u$ & 0.0 & 0.0 \\ \hline
    \end{tabular}
\end{center}

The analytical solution for density (with $\gamma=1.4$) is shown in Figure QA\ref{f:sod}.

\begin{figure}[h]
\setcounter{figure}{\thequestion-1}
\center{\includegraphics[width=0.5\columnwidth]{SodShock.pdf}}
\caption{Density profile from the analytical solution to Sod's shock tube problem at $t=0.2$}
\label{f:sod}
\end{figure}
Briefly explain (in no more than 150 words) what difficulties you may encounter using this unmodified MacCormack scheme and what alternative approach could be used.  You may refer to the solution regions (I to V) shown in Figure QA\ref{f:sod} in your answer where appropriate.
\droppoints
\begin{solution}
One mark for each of the following points
\begin{itemize}
    \item The MacCormack scheme is a 2nd order method based on the Lax-Wendroff scheme.
    \item Godunov's theorem tells us such a scheme will exhibit Gibbs oscillations where the solution is steep.
    \item Such oscillations are expected at the boundaries of regions III and IV and regions IV and V.
    \item Density and pressure are low in region V and could become -ve. 
    \item This would cause the calculation of $\sqrt{\gamma p/\rho}$ to fail as we would be attempting to take the square root of a -ve number.
    \item Using a TVD scheme would prevent this from occurring, since it will suppress the Gibbs oscillations.
\end{itemize}
\end{solution}
\end{parts}

