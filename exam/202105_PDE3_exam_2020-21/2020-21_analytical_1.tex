%------------------------------------------------------------------------%
\question
\pointformat{(\bfseries\boldmath\themarginpoints)}

We consider the heat conduction through the wall of a building and want to calculate the temperature profile in the wall over time. 

Your colleague has given you the following non-dimensionalised, 1D heat conduction equation
\begin{align}
\frac{\partial u(x,t)}{\partial t} &= \kappa \frac{\partial^2 u(x,t)}{\partial x^2} \label{eq:AQ1_PDE} \\
\intertext{on the interval $0< x\le1$ with the initial condition}
u(x,0) &= x - 1, \quad 0 \le x \le 1, \label{eq:AQ1_IC}\\
\intertext{and the boundary conditions} 
  u(0,t) &= 1, \quad  t > 0, \label{eq:AQ1_BC_left} \\
  u(1, t)&= 0, \quad t \ge 0, \label{eq:AQ1_BC_right}
\end{align}
for this case.

\begin{parts}
\part[3] Your boss asks you to describe the problem which is given by this partial differential equation (PDE) problem, i.e the PDE Eq.~\ref{eq:AQ1_PDE} with the initial and boundary conditions Eqs.~\ref{eq:AQ1_IC} -- \ref{eq:AQ1_BC_right}, in plain English.
\droppoints

\begin{solution}
The 1D heat conduction equation describes the heat conduction through the wall of a building.
The right side of the wall is held at the fixed reference temperature while the left side changes instantly from $-1$ to $1$ at times just larger than 0.
Initially the wall has a temperature profile that is linear from the left side temperature of $-1$ to the right side temperature of $0$. \hfill[3]

\end{solution}

\part[5] Derive the steady-state solution $U(x)$ without solving the heat conduction equation and use the steady-state solution to reformulate the problem given by Eqs.~\ref{eq:AQ1_PDE} -- \ref{eq:AQ1_BC_right} into a problem for the transient solution $v(x,t)$. \\
You are reminded that $u(x,t) = U(x) + v(x,t)$.
\droppoints

\begin{solution}
The right boundary is kept at $0$ and the left boundary is held at $1$ for positive times, thus the function
\[
U(x) = 1 - x
\]
fulfils conditions (c) and (d) and also $\nabla^2 U = 0$; thus is the steady state solution.  \hfill[2]

With the steady-state solution the initial and boundary conditions for the transient problem are
\begin{align}
v(x,0) &= u(x,0) - U(x) = 2x - 2, \quad 0\le x \le 1, \tag{e}\\
 v(0, t) &= u(0,t) - U(0) = 0, \quad t > 0, \tag{f} \\
 v(1, t) &= u(1,t) - U(1) =  0, \quad t \ge 0. \tag{g}
\end{align}
One mark for each condition. \hfill[3]
\end{solution}

\part[6] Use the method of separation of variables to obtain the transient solution $v(x,t)$ of the heat conduction equation. Find the general solution of the problem given by Eqs.~\ref{eq:AQ1_PDE} -- \ref{eq:AQ1_BC_right}.
\droppoints

\begin{solution}
We seek a solution of the form
\begin{align*}
v(x,t) &= e^{-\alpha t} \left(A\sin(\lambda x) + B \cos(\lambda x)\right)
\end{align*}
subject to the initial and boundary conditions~(e-g) and with $\alpha=\kappa \lambda^2$. The parameters $A$ and $B$ need to be found to fulfil these conditions. \hfill[1]

The first boundary condition (f), $v(0, t)=0$, requires that
\begin{align*}
A\sin(\lambda 0) + B\cos(\lambda 0) \overset{!}{=} 0
\end{align*}
which is only fulfilled if $B=0$. \hfill[1]

To fulfil the second boundary condition (g), $v(1, t) = 0$, it is required that
\begin{align*}
\sin(\lambda) &= 0 \\
\intertext{which is fulfilled for}
\lambda_n &= n\pi, \quad n=1,2,\dots
\end{align*}
This give solutions of the form
\[
v_n(x,t) = e^{-\kappa \lambda_n^2 t} A_n\sin(n\pi x), \quad n=1,2,\dots
\]
where we have used $\alpha_n = \kappa \lambda_n^2$. \hfill[1]

To fulfil the initial condition (e) we can superimpose these solutions 
\[
v(x,t) = \sum_{n=1}^\infty e^{-\kappa \lambda_n^2 t} A_n\sin(n\pi x)
\]
and require that \hfill[1]
\[
\sum_{n=1}^\infty A_n\sin(n\pi x) \overset{!}{=} 2x - 2
\]

Using the Fourier sine series expansion over the interval $0\le x \le 1$, we get two integrals which can be solved using the list of integrals given at the end of the exam paper \hfill[1]
\begin{align*}
A_n &= \frac{2}{1} \int_0^1 (2x - 2) \sin(n\pi x dx \\
     &= 4\int_0^1 x \sin(n\pi x) dx - 4\int_0^1 \sin(n\pi x) dx \\
     &= \frac{4}{n^2\pi^2} \left[\sin(n\pi x) - n\pi x \cos(n\pi x)\right]_0^1 + \frac{4}{n\pi} \left[\cos(n\pi x)\right]_0^1 \\
     &= -\frac{4}{n\pi} \cos(n\pi) + \frac{4}{n\pi} \cos(n\pi) - \frac{4}{n\pi} = - \frac{4}{n\pi}
\end{align*}

Thus the transient and general solutions are \hfill[1]
\begin{align*}
 v(x,t) &= -\sum_{n=1}^\infty e^{-\kappa n^2\pi^2 t} \frac{4}{n\pi} \sin(n\pi x) \\
 u(x,t) &= 1 - x -\sum_{n=1}^\infty e^{-\kappa n^2\pi^2 t} \frac{4}{n\pi} \sin(n\pi x)
\end{align*}
for $0\le x \le 1$.
\end{solution}

\part[2] Once the solution of the PDE problem given in Eqs.~\ref{eq:AQ1_PDE} -- \ref{eq:AQ1_BC_right} has reached the steady-state temperature profile, the temperature on the left side of the wall drops to $-1$. Describe how you can use the results from parts~(b) and (c) to calculate the solution for this case and give the general solution for this case. \\
Remark: You should not repeat the process from part (c) but think about the transformation between the two problems. 
\droppoints

\begin{solution}
For the new problem of the drop in temperature, the role of the initial condition and steady-state solution from parts~(b) and (c) are reversed and are given by \hfill[1]
\begin{align*}
    u_n(x, 0) &= 1 - x, \quad 0\le x \le 1, \\
    U_n(x) &= x - 1, \quad 0\le x \le 1.
\end{align*}
This is equivalent to multiplying both with $-1$.

The resulting initial condition for the transient problem is the initial condition from part (b) multiplied by $-1$ and thus the solution is given by

\begin{align*}
 u_n(x,t) &= x - 1 + \sum_{n=1}^\infty e^{-\kappa n^2\pi^2 t} \frac{4}{n\pi} \sin(n\pi x) = - u(x,t)
\end{align*}
for $0\le x \le 1$. \hfill[1]
\end{solution} 

\part[4] Once you plot the solution from part~(c) you realise that the insulation of the wall is not sufficient for the proposed Passivhaus standard. You suggest to add an insulation layer with a lower thermal diffusivity $\kappa_i$ on the left side of the wall. 

What conditions do you need to impose on the interface of the insulation layer with the original wall when you try to calculate the temperature profile?

Assume that the insulation layer has a non-dimensional thickness of $0.5$ and that its thermal diffusivity $\kappa_i$ is half the thermal diffusivity $\kappa$ of the original wall. 
Sketch the steady-state temperature profile across the composite wall for the case where the left hand side is held at $1$ and the right hand side at $0$.
\droppoints

\begin{solution}
Both the solution value as well as the heat flux across the boundary need to be consistent across the boundary. This can be written as \hfill[2]
\begin{align*}
    u(0^-,t) &= u(0^+,t) \\
    \kappa_i\left.\frac{\partial u}{\partial x}\right|_{(0^-,t)} &= \kappa\left.\frac{\partial u}{\partial x}\right|_{(0^+,t)}
\end{align*}

The flux at the interface can only be the same if he slope on the left is twice as steep as the one on the right hand side. \hfill[2]

\end{solution} 

\end{parts}