
% -----------------------------------------------------
% QUESTION
% -----------------------------------------------------
\newpage
\question The governing equation for  one dimensional, transient,  heat transfer in a rod of uniform cross-section is:
\begin{equation}
\frac{\partial^2 T}{\partial x^2}=\frac{1}{\kappa}\frac{\partial T}{\partial t},
\end{equation}
 where $T$ is the temperature (in Kelvin), and $\kappa$ is the thermal diffusivity (in $\text{m}^2\text{s}^{-1}$) of the material from which the rod is made.
 \begin{parts}
 \part [5] Using appropriate finite difference approximations for the derivatives show that the Crank-Nicholson scheme can be written as a system of linear equations \[-R_x T^{n+1}_{i-1} + \alpha T^{n+1}_i -R_x T^{n+1}_{i+1} = R_x T^{n}_{i-1} + \beta T^{n}_i +R_x T^{n}_{i+1}\] and hence find the coefficients $R_x$, $\alpha$ and $\beta$.
 State the formal order of accuracy of the scheme in time and space.
\droppoints
\begin{solution}
We derive the Crank-Nicholson method using a weighted average of the old and new time level data. 
\[\frac{T^n_{i-1}-2T^n_i+T^n_{i+1}}{2\Delta x^2}+\frac{T^{n+1}_{i-1}-2T^{n+1}_i+T^{n+1}_{i+1}}{2\Delta x^2}=\frac{h_{i}^{n+1}-h_{i}^n}{\kappa\Delta t}.\] \hfill[1]

Multiplying B.S. by ${\kappa\Delta t}$
\[\frac{\kappa\Delta t}{2\Delta x^2}\left(T^n_{i-1}-2T^n_i+T^n_{i+1}\right) 
+ \frac{\kappa\Delta t}{2\Delta x^2}\left(T^{n+1}_{i-1}-2T^{n+1}_i+T^{n+1}_{i+1}\right) = h_{i}^{n+1}-h_{i}^n\]
Putting all the $T^{n+1}$ values on the LHS and $T^{n}$ on the RHS (and multiplying by -1 BS)
\[h_{i}^{n+1}-\frac{\kappa\Delta t}{2\Delta x^2}\left(T^{n+1}_{i-1}-2T^{n+1}_i+T^{n+1}_{i+1}\right) = h_{i}^n+\frac{\kappa\Delta t}{2\Delta x^2}\left(T^n_{i-1}-2T^n_i+T^n_{i+1}\right) \] \hfill[1]

Gathering terms (and writing $r_x=\frac{\kappa\Delta t}{2\Delta x^2}$), we have
\[-r_x T^{n+1}_{i-1} + (2+2r_x)T^{n+1}_i-r_x T^{n+1}_{i+1} = r_xT^n_{i-1}+(2-2r_x)T^n_i+r_xT^n_{i+1}\] \hfill[1]

So \[r_x= \frac{\kappa\Delta t}{2\Delta x^2},\, \alpha =  2+2r_x \text{ and }\beta=2-2r_x.\]
\hfill[1]

The scheme is first order in time and second order in space.
\hfill[1]
\end{solution} 
\part[10] Use von Neumann stability analysis to show that the Crank-Nicholson scheme is unconditionally stable.
\droppoints
\begin{solution}
The Fourier modes are:
\[T_i^{n+1}=g^{n+1}e^{ij\theta},\ T_i^n=g^ne^{ij\theta},\  
T_{i\pm1}^n=g^ne^{(i\pm1)j\theta} \text{ and } T_{i\pm1}^{n+1}=g^{n+1}e^{(i\pm1)j\theta}\]
where \(j=\sqrt{-1}\). \hfill[2]

substituting into the FD scheme
\begin{align*}
  &-r_x g^{n+1}e^{(i-1)j\theta} 
  +(2+2r_x)g^{n+1}e^{ij\theta}
  - r_x g^{n+1}e^{(i+1)j\theta} \\
  &= r_xg^ne^{(i-1)j\theta} +(2-2r_x)g^ne^{ij\theta}+r_xg^ne^{(i+1)j\theta} 
\end{align*} \hfill[1]

dividing through by $e^{ij\theta}$
\begin{align*}
  &-r_x g^{n+1}e^{-j\theta} 
  +(2+2r_x)g^{n+1}
  - r_x g^{n+1}e^{j\theta} 
  = r_xg^ne^{-j\theta} +(2-2r_x)g^n+r_xg^ne^{j\theta} 
\end{align*} \hfill[1]

or 
\begin{align*}
  &g^{n+1}\left[-r_x e^{-j\theta} 
  +(2+2r_x)
  - r_x e^{j\theta}\right]
  = g^n\left[r_xe^{-j\theta} +(2-2r_x)+r_xe^{j\theta} \right]
\end{align*} \hfill[1]

taking $r_x$ out as a factor  
\begin{align*}
  &g^{n+1}\left[2-r_x\left(e^{-j\theta}+ e^{j\theta}-2\right)\right]
  = g^n\left[2+r_x\left(e^{-j\theta}+e^{j\theta}-2\right)\right]
\end{align*} \hfill[1]

Now \(e^{j\theta}+e^{-j\theta}-2=2(\cos\theta-1)\), so
\begin{align*}
  &g^{n+1}\left[2-2r_x(\cos\theta-1)\right]
  = g^n\left[2+2r_x(\cos\theta-1)\right]
\end{align*} \hfill[1]

Writing $g^{n+1}=G(\theta)g^n$ we find
\[G(\theta)=\frac{1-r_x(1-\cos\theta)}{1+r_x(1+\cos\theta)}\] \hfill[1]

Since \(G(\theta)<1\ \forall\theta\) the Crank-Nicholson scheme is unconditionally stable.
\hfill[1]
\end{solution}
\part[5] The two dimensional variant of the Crank-Nicholson scheme is to be applied to the computational domain shown in Figure QA\ref{f:grid}.  It should be noted that the North and South boundaries of the domain are periodic, while the East and West boundaries are Dirichlet boundaries with specified values of $u_{0,j}$ and $u_{6,j}$.

\begin{figure}[b]
    \setcounter{figure}{\thequestion-1}
    \centering
    \includegraphics[width=0.6\columnwidth]{CrankGrid.pdf}
    \caption{Grid to be used for the two dimensional heat conduction equation showing periodic North-South boundaries and East-West Dirichlet boundary conditions.}
    \label{f:grid}
\end{figure}

With reference to the sparsity pattern of the coefficient matrix explain (in less than 200 words) why the use of a specialist, iterative, sparse matrix solver (for example the stabilised Bi-conjugate gradient method) would be preferable to the use of the Alternating Direction Implicit (ADI) scheme for this problem.

\droppoints
\begin{solution}
\begin{itemize}
    \item For all internal points the $A$ matrix will be penta-diagonal, with coefficients at $k-5, k-1, k, k+1$ and $k+5$. \hfill[1]
    \item At the North and South boundaries this pattern will be broken with coefficients at $k-30$ on the North boundary and $k+30$ on the south. \hfill[2]
    \item The ADI method relies on the use of fast tri-diagonal matrix solvers which cannot be used with periodic boundaries of this kind. \hfill[1]
    \item A sparse solver will not have this restriction and will be computationally efficient. \hfill[1]
\end{itemize}
\end{solution}
\end{parts}
