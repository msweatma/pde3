\setcounter{equation}{0}
\question We are tasked to calculate the displacement of a long string over time. Before the experiment the string is not moving but has an initial sine-shaped displacement.
At the start of the experiment, the left side of the string is quickly moved upwards and then slowly returned to the zero position.

After some consideration it was decided to model the string with the semi-infinite, 1D wave equation
\begin{align}
\frac{\partial^2u}{\partial t^2} &= 9 \frac{\partial^2u}{\partial x^2} & \quad &  \label{eq:wave_equation_052021}\\
\intertext{for $x\in[0,\infty)$ with the initial and boundary conditions }
 u(x, 0) &= \sin(x),  &\forall x, \label{eq:we_IC1_052021} \\
\left. \frac{\partial u(x,t)}{\partial t}\right|_{t=0} &= 0, &\forall x, \label{eq:we_IC2_052021} \\
 u(0, t) &= t\exp(-t),  &\forall t, \label{eq:we_BC1_052021} \\
 \lim_{x\to\infty}|u(x,t)| &< C, &\forall t. \label{eq:we_BC2_052021}
\end{align}
for a positive constant $C$.

\begin{parts}
%----- Part -----------------------------------------------------------%
\part[8] Apply the Laplace transform with respect to $t$ to the PDE~(\ref{eq:wave_equation_052021}) and find the general solution for $U(x,s)$ in the Laplace domain.
\droppoints

\begin{solution}
The Laplace transform of the left hand side of the partial differential equation can be found using the derivative property and on the right hand side we can swap the order of integration and differentiation. This gives us the transformed equation
\begin{align}
s^2 U(x, s) - s u(x, 0) - u'(x,0) &= 9 {\cal L} \left\{\frac{\partial^2 u(x,t)}{\partial x^2} \right\} = 9\frac{\partial^2 U(x,s)}{\partial x^2} 
\label{eq:we_transformed_052021}
\end{align}

Insert the initial conditions into Eq.~(\ref{eq:we_transformed_052021}) to get
\begin{align}
s^2 U(x, s) - s\sin(x) &= 9 \frac{\partial^2 U(x,s)}{\partial x^2} \nonumber\\
\Rightarrow \frac{\partial^2 U(x,s)}{\partial x^2} - \frac{s^2}{9} U(x, s) &= \frac{-s}{9}\sin(x) \label{eq:we_laplace_052021}
\end{align}
\hfill[2]

The differential equation~(\ref{eq:we_laplace_052021}) has the characteristic equation
\begin{align*}
m^2 - \frac{s^2}{9} &= 0 \\
\intertext{which has the zeros}
 m_{1,2} &= \pm \frac{s}{3} \\
\end{align*}
Thus the complimentary function is
\begin{align}
U_g(x,s) &= A(s) e^{sx/3} + B(s) e^{-sx/3} \label{eq:we_general_solution_052021}
\end{align}
where $A(s)$ and $B(s)$ are functions in the Laplace domain which we need to determine from the boundary conditions. \hfill[2]

Before we find the two functions we need to take care of the inhomogeneity of the differential equation. The trial solution for the particular integral for the inhomogeneous differential equation~(\ref{eq:we_laplace_052021}) is
\begin{align*}
U_p(x,s) &= P \sin(x) + Q \cos(x)
\end{align*}
where the coefficients $P$ and $Q$ need to be determined. We substitute $U_p$ and its second derivative with respect to $x$ into Eq.~(\ref{eq:we_laplace_052021}) and get \hfill[2]
\begin{align*}
-P \sin(x) - Q\cos(x) - \frac{s^2}{9} \left( P\sin(x) + Q\cos(x)\right) &= -\frac{s}{9} \sin(x)
\end{align*}

We can arrange this in terms of sine and cosine
\[
\sin(x) \left(-P - \frac{s^2}{9} P + \frac{s}{9}\right) + \cos(x) \left(-Q - \frac{s^2}{9} Q\right) = 0
\]
From this it follows that
\[
Q =0
\]
and that
\begin{align*}
P &= \frac{s}{s^2 + 9}
\end{align*}
for $\Re(s)>0$.
Thus, the general solution of the differential equation is of the form
\begin{align*}
U(x,s) &= A(s) e^{sx/3} + B(s) e^{-sx/3} + \frac{s}{s^2 + 9}\sin(x)
\end{align*}
\hfill[2]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[6] Given that the general solution to the PDE~(\ref{eq:wave_equation_052021}) is
\begin{align*}
U(x,s) &= A(s) e^{sx/3} + B(s) e^{-sx/3} + \frac{s}{s^2 + 9}\sin(x)
\end{align*}
use the boundary conditions~(\ref{eq:we_BC1_052021} -- \ref{eq:we_BC2_052021}) to find the particular solution. Use the inverse Laplace Transform on the particular solution to get the solution of the wave equation~(\ref{eq:wave_equation_052021} -- \ref{eq:we_BC2_052021}) in the time domain.
\droppoints

\begin{solution}
From the boundary condition~(\ref{eq:we_BC2_052021}) we know that the solution is bounded. Thus we can swap the limit and the integration to get
\begin{align*}
\lim_{x\to\infty} U(x,s) &= \lim_{x\to\infty} \int_0^\infty e^{-st} u(x,t) dt = \int_0^\infty  e^{-st} \lim_{x\to\infty} u(x,t) dt = 0
\end{align*}
From this it follows that
\begin{align*}
A(s) &= 0, 
\end{align*}
because otherwise the first term on the right hand side of Eq.~(\ref{eq:we_general_solution_052021}) would go to infinity for $x\to\infty$.
\hfill[2]

The boundary condition~(\ref{eq:we_BC1_052021}) requires that
\begin{align*}
U(0,s) &= B(s) \overset{!}{=} {\cal L} \left\{u(0,t) \right\} =  \frac{1}{(s + 1)^2}, \quad \Re(s)>0,
\end{align*}
and thus
\begin{align*}
 B(s) &= \frac{1}{(s + 1)^2}
\end{align*}
\hfill[2]

From the solution in the Laplace domain 
\begin{align*}
U(x,s) &= \frac{1}{(s + 1)^2} \exp(-sx/3) + \frac{s}{s^2 + 9} \sin(x)
\end{align*}
we can find the solution in the time domain by using the time shifting property and the Laplace transform tables
\begin{align*}
 u(x,t) =& (t-x/3)\exp\left(-(t-x/3)\right) H(t-x/3) + \cos(3t) \sin(x)
\end{align*}
for $t\ge0$. \hfill[2]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[6] You are asked why you used the Laplace transform method for the solution of this problem.

Briefly (in less than 200 words), discuss which solution methods for the wave equation are or aren't applicable in this case.
\droppoints

\begin{solution}
\begin{itemize}
\item d'Alembert method is not applicable because the domain is bounded on the left \hfill[1]
\item Separation of variables is not applicable because the initial conditions~\ref{eq:we_IC1_052021} and \ref{eq:we_IC2_052021} require a solution of the form $u(x,t) = \sin(\lambda x) \cos(3\lambda t)$ which won't be able to match the boundary condition~\ref{eq:we_BC1_052021} \hfill[2]
\item The method of characteristics can be applied with $u(x,t) = f(x-3t) + g(x+3t)$. For the solution we need to split the domain into a part for $x>3t$ and one for $x<3t$. \hfill[2]
\item Laplace transform method is obviously applicable. In fact, the bounded boundary condition~\ref{eq:we_BC2_052021} simplifies the solution significantly by removing the term with $A(s)$ \hfill[1]
\end{itemize}

\end{solution}

\end{parts}
