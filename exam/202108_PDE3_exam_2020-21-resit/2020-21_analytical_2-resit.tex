%------------------------------------------------------------------------%
% Analytical questons set by Daniel Friedrich
\question 
\setcounter{equation}{0}
Consider the one-dimensional wave equation
\begin{align}
\frac{\partial^2u}{\partial t^2} &= c^2 \frac{\partial^2u}{\partial x^2} & \quad  \label{eq:wave_equation_201908}\\
\intertext{on the interval $x\in[0, l]$ with the initial and boundary conditions }
 u(x, 0) &= 0, \quad \forall x\in[0, l], \label{eq:we_IC1_201908} \\
\left. \frac{\partial u(x, t)}{\partial t}\right|_{t=0} &= f(x), \quad \forall x\in[0, l], \label{eq:we_IC2_201808} \\
u(0, t) &= 0, \quad \forall t. \label{eq:we_BC1_201908} \\
u(l, t) &= 0, \quad \forall t, \label{eq:we_BC2_201908}
\end{align}
where $l$ is positive and $f(x)$ an arbitrary function.

\begin{parts}
%----- Part -----------------------------------------------------------%
\part[3]
The partial differential equation~(\ref{eq:wave_equation_201908}) and the boundary and initial conditions~(\ref{eq:we_IC1_201908} -- \ref{eq:we_BC2_201908}) model the behaviour of a string. 

Briefly (in less than 150 words) and in plain English, describe the physical quantity calculated by the model as well as the initial state and boundary conditions.

\droppoints
\begin{solution}
\begin{itemize}
\item The PDE models the displacement of a string, which has an initial velocity given by $f(x)$. The initial displacement is zero. \hfill[2]
\item The left and right side of the string are held at zero. \hfill[1]
\end{itemize}
\end{solution}

%----- Part ------------------------------------------%
\part[7] Using the method of separated solutions, derive the solution of the wave equation~(\ref{eq:wave_equation_201908}) subject to the above initial and boundary conditions~(\ref{eq:we_IC1_201908}-\ref{eq:we_BC2_201908}).

\droppoints
\begin{solution}
The four separated solution types are 
\begin{align*}
u_1(x,t) &= \cos \lambda c t\sin\lambda x \\
u_2(x,t) &= \cos \lambda c t\cos\lambda x \\
u_3(x,t) &= \sin \lambda c t\sin\lambda x \\
u_4(x,t) &= \sin \lambda c t\cos\lambda x  
\end{align*}
We are stepping through the initial and boundary conditions to find a solution which fits all conditions. The first boundary condition~\ref{eq:we_BC1_201908}, $u(0,t) = 0$, is fulfilled by the solutions $u_1$ and $u_3$. \hfill[2]

From the initial condition~\ref{eq:we_IC1_201908} we know that $u(x,0)=0$ which indicates a sine term in $t$. This leaves only solution $u_3$. \hfill[2]

The second boundary condition~\ref{eq:we_BC2_201908}, $u(l, t) = 0$, is only fulfilled for solution $u_3$ if
\[
\sin(\lambda l) = 0
\]
and thus $\lambda_n = \frac{n\pi}{l}$ for $n\in\mathbb{N}$. \hfill[2]

Thus we get solutions of the form
\[
u_n(x,t) = b_n \sin(n\pi ct/l) \sin(n\pi x/l), \quad n=1,2,\dots
\]

To fulfil the second initial condition~\ref{eq:we_IC2_201808} for an arbitrary $f(x)$ we need to superimpose the solution \hfill[1]
\begin{align*}
u(x,t) = \sum_{n=1}^\infty b_n \sin(n\pi ct/l) \sin(n\pi x/l)
\end{align*}
\end{solution}

%----- Part ------------------------------------------%
\part[6] Now consider the case where the initial velocity is given by
\begin{equation}
\left. \frac{\partial u(x, t)}{\partial t}\right|_{t=0} = f_c(x) = (l-x)x, \quad \forall x\in[0, l]. \label{eq:we_IC2c_201808} 
\end{equation}
Find the solution to the PDE~\ref{eq:wave_equation_201908} which fulfils the initial and boundary conditions~\ref{eq:we_IC1_201908}, \ref{eq:we_BC1_201908}, \ref{eq:we_BC2_201908} and the initial velocity \ref{eq:we_IC2c_201808}.

\droppoints
\begin{solution}
To fulfil initial condition~\ref{eq:we_IC2c_201808} we need to calculate the time derivative of the superimposed solution 
\begin{align*}
\frac{\partial u(x, t)}{\partial t} &= \sum_{n=1}^\infty \frac{b_n n\pi c}{l} \cos(n\pi ct/l) \sin(n\pi x/l)
\end{align*}
At the initial time $t=0$ this derivative needs to be equal to $f_c(x)$ \hfill[2]
\begin{align*}
\left. \frac{\partial u(x, t)}{\partial t}\right|_{t=0} &= \sum_{n=1}^\infty \frac{b_n n\pi c}{l} \sin(n\pi x/l) \overset{!}{=} (l-x)x
\end{align*}

The parameters $b_n$ can be found with the usual Fourier series formulas \hfill[2]
\begin{align*}
\frac{b_n n\pi c}{l} =& \frac{2}{l} \int_0^l (l-x)x \sin(n\pi x/l) dx \\
b_n =& \frac{2}{n\pi c}\int_0^l (l-x)x \sin(n\pi x/l) dx \\
 =& \frac{2}{n\pi c} \left[\frac{l^3}{n^2\pi^2} \bigl ( \sin(n\pi x/l) - \frac{n\pi x}{l} \cos(n\pi x/l) \bigr ) \right. \\
 & \quad \left. - \frac{2xl^2}{n^2\pi^2} \sin(n\pi x/l) + \left(\frac{x^2l}{n\pi} - \frac{2l^3}{n^3\pi^3}\right) \cos(n\pi x/l) \right]_0^l \\
 =& \frac{2}{n\pi c} \left[\frac{l^3}{n^2\pi^2} \bigl ( -n\pi \cos(n\pi)\bigr ) + \left(\frac{l^3}{n\pi} - \frac{2l^3}{n^3\pi^3}\right) \cos(n\pi) + \frac{2l^3}{n^3\pi^3} \right] \\
 =& \frac{2}{n\pi c} \left[\frac{2l^3}{n^3\pi^3} - \frac{2l^3}{n^3\pi^3} \cos(n\pi) \right] = \frac{4l^3}{n^4\pi^4 c} \left[1 - \cos(n\pi) \right] \\
 &= \begin{cases}
 \frac{8l^3}{n^4\pi^4 c}, \quad & n \text{ odd} \\
 0, \quad & n \text{ even}
 \end{cases} 
\end{align*}

Thus, the final solution is \hfill[2]
\begin{align*}
u(x,t) &= \sum_{k=1}^\infty \frac{8l^3}{(2k-1)^4\pi^4 c} \sin((2k-1)\pi ct/l) \sin((2k-1)\pi x/l)
\end{align*}
for all $t$ and for $x\in[0, l]$.
\end{solution}

%----- Part ------------------------------------------%
\part[4] You are asked why you used the method of separation of variables for the solution of this problem. 

Briefly (in less than 200 words), discuss which solution methods for the wave equation are or aren’t applicable in this case.

\droppoints
\begin{solution}
\begin{itemize}
\item d'Alembert method is not applicable because the domain is bounded on the left and right \hfill[1]
\item Separation of variables is obviously applicable and easy to use due to the three initial and boundary conditions that are zero \hfill[1]
\item The method of characteristics is applicable but requires careful application of the two boundary conditions which complicates the solution \hfill[1]
\item The Laplace transform method is applicable but the two boundary conditions at $x=0$ and $x=l$ make it difficult to find the two functions $A(s)$ and $B(s)$ \hfill[1]
\end{itemize}
\end{solution}

\end{parts}