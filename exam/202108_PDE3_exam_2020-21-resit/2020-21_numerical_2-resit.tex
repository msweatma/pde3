
% -----------------------------------------------------
% QUESTION
% -----------------------------------------------------
\question 
\setcounter{equation}{0}
The homogeneous form of the Saint-Venant equations is,

\begin{equation}
\frac{\partial\vec{U}}{\partial t}+\frac{\vec{F}(\vec{U})}{\partial x}=0\text{ where }
\vec{U}=\left(\begin{matrix}\phi\\ \phi u\end{matrix}\right)\text{ and }
\vec{F}(\vec{U})=\left(\begin{matrix}\phi u\\ \phi u^2+\frac{\phi^2}{2}\end{matrix}\right),
\end{equation}
$u$ is the local fluid velocity, $\phi=gh$ is the geopotential, $g$ is the acceleration due to gravity and $h$ is the local water depth.  This system of equations can be used to model flow in an open channel with constant cross section and constant depth.

%----- Part -----------------------------------------------------------%
\begin{parts}
\part[5] Write this system of equations in quasi-linear form \(\vec{U}_t+J\vec{U}_x=0\) and hence find the Jacobian matrix, $J$.
\droppoints
\begin{solution}
For the quasi-linear form \(\vec{U}_t+J\vec{U}_x=0\), $J$ is given by
\[J=\frac{\partial\vec{F}}{\partial\vec{U}}=
\begin{pmatrix}\frac{\partial f_1}{\partial u_1}&\frac{\partial f_1}{\partial u_2}\\
\frac{\partial f_2}{\partial u_1}&\frac{\partial f_2}{\partial u_2}\end{pmatrix}.\]
\hfill[1]

Let $p=\phi$ and $q=\phi u$\\
Now $F_1=q$ and $F_2=q^2/p+p^2/2$ \hfill[1]\\

Differentiating w.r.t $p$
\[
\frac{\partial}{\partial p}q=0\mbox{ and }
\frac{\partial}{\partial p}q^2/p+p^2/2=-q^2/p^2+p
\] \hfill[1]

Differentiating w.r.t. $q$
\[
\frac{\partial}{\partial q}q=1\mbox{ and }
\frac{\partial}{\partial q}q^2/p+p^2/2=2q/p
\]\hfill[1]

So
\[
J=\begin{pmatrix}0 & 1 \\-q^2/p^2+p & 2q/p\end{pmatrix}=\begin{pmatrix}0 & 1 \\ -u^2+\phi & 2u\end{pmatrix}
\] \hfill[1]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[5] Find the two eigenvalues of J and hence determine if the system is hyperbolic, parabolic or elliptic.
\droppoints
\begin{solution}
We solve the characteristic equation \(\left|J-\lambda I\right|=0\), so
\[ \left|\begin{matrix}-\lambda & 1 \\ -u^2+\phi & 2u-\lambda\end{matrix}\right|=0 \] \hfill[1]

Expanding the determinant we have
\begin{align*}
0&=-\lambda\times(2u-\lambda)-(-u^2+\phi)\times 1\\
&=\lambda^2-2u\lambda+u^2-\phi\\
&=(\lambda-u)^2-\phi\\
&\lambda=u\pm\sqrt{\phi}.
\end{align*}\hfill[2]

Since \(\phi\ge0,\  u\pm\sqrt{\phi}\) will always be real so the Jacobian has two distinct real roots.

Therefore the system is hyperbolic.\hfill[2]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[10] A dam break problem is to be simulated in a 2m long channel, \(-1\le x\le 1\), using 5 grid points, \(\Delta x=0.5\). The initial conditions, at $t=0$, are given by
\[
\phi=\begin{cases}1&x\le0\\ 3&x>0\end{cases}\text{ and }u=\begin{cases}1&x\le0\\ 2&x>0\end{cases}
\]
Perform \textbf{ONE} time step (working to 2 d.p.) using the Lax-Friedrichs scheme 
\[\vec{U}_i^{n+1}=\frac{1}{2}\left(\vec{U}_{i+1}^n+\vec{U}_{i-1}^n\right)-\frac{\Delta t}{2\Delta x}\left(\vec{F}_{i+1}^n-\vec{F}_{i-1}^n\right)\]
using a Courant number $\nu=0.9$. You may assume Dirichlet boundary conditions at the ends of the domain, i.e. both the geopotential, $\phi$, and the veclocity, $u$ at $x=\pm1$ are constant.

\textbf{Remark:} It can be shown that the local wave speeds ($S_L$ and $S_R$) are \(S_{L,R}=u\pm\sqrt{\phi}\) and thus that for any explicit time marching method,
\[\Delta t=\nu\frac{\Delta x}{\max(|u-\sqrt{\phi}|,|u+\sqrt{\phi}|)}\]
 where $\nu<1$ is the Courant number.

\droppoints
\begin{solution}
\textbf{Time step}: 
\[\Delta t=\nu\frac{\Delta x}{\max(|u-\sqrt{\phi}|,|u+\sqrt{\phi}|)}\]
The largest value of $u\pm\sqrt{phi}$ will be either $1+1=2$ or $2+\sqrt3\approx3.7$. So we have
\[\Delta t=\frac{\nu\Delta x}{3.7}=\frac{0.45}{3.7}=0.12\text{ seconds}\]
\hfill[3]

\textbf{U vector}:
The $\vec{U}$ is \[\vec{U}=\left(\begin{matrix}\phi\\ \phi u\end{matrix}\right)\]

We have
\[\vec{U}=\begin{cases}\left(\begin{matrix}1\\ 1\end{matrix}\right)&x\le0 \\
\left(\begin{matrix}2\\ 6\end{matrix}\right)&x>0\end{cases}\]
\hfill[1]

\textbf{Fluxes}:
The flux vector is  \[\vec{F}(\vec{U})=\left(\begin{matrix}\phi u\\ \phi u^2+\frac{\phi^2}{2}\end{matrix}\right)\]

We have
\[\vec{F}(\vec{U})=\begin{cases}\left(\begin{matrix}1\\ 1.5\end{matrix}\right)&x\le0 \\
\left(\begin{matrix}6\\ 16.5 \end{matrix}\right)&x>0\end{cases}\]
\hfill[1]

We now compute the parts of the Lax Friedrichs scheme:
\[\frac{\Delta t}{2\Delta x}=\frac{0.12}{2\times0.5}=0.12.\]
\hfill[1]

\begin{tabular}{lrrrrr} \hline
$i$ & 1 & 2 & 3 & 4 & 5 \\ 
$x_i$ & -1.00 & -0.50 & 0.00 & 0.50 & 1.00 \\ \hline  

$\phi$ & 1.00 & 1.00 & 1.00 & 3.00 & 3.00 \\
$u$ & 1.00 & 1.00 & 1.00 & 2.00 & 2.00 \\ \hline \hline

\multicolumn{6}{c}{$\vec{U}^n_i$}\\ \hline
$\phi$ & 1.00 & 1.00 & 1.00 & 3.00 & 3.00 \\
$\phi u$ & 1.00 & 1.00 & 1.00 & 6.00 & 6.00 \\ \hline

\multicolumn{6}{c}{$\vec{F}^n_i$}\\ \hline
$\phi u$ & 1.00 & 1.00 & 1.00 & 6.00 & 6.00\\ 
$\phi u^2+\frac{\phi^2}{2}$ & 1.50 & 1.50 & 1.50 & 16.50 & 16.50 \\ \hline \hline 

\multicolumn{6}{c}{$\frac{1}{2}\left(\vec{U}^n_{i+1}+\vec{U}^n_{i-1}\right)$}\\ \hline
$\phi$ & 1.00 & 1.00 & 2.00 & 2.00 & 3.00 \\ 
$\phi u$ & 1.00 & 1.00 & 3.50 & 3.50 & 6.00\\ \hline

\multicolumn{6}{c}{$\vec{F}^n_{i+1}-\vec{F}^n_{i-1}$}\\ \hline
$\phi$ & 0.00 & 0.00 & 5.00 & 5.00 & 0.00 \\
$\phi u$ & 0.00 & 0.00 & 15.00 & 15.00 & 0.00 \\ \hline

\multicolumn{6}{c}{$\vec{U}^{n+1}_i$}\\ \hline
$\phi$ & 1.00 & 1.00 & 1.40 & 1.40 & 3.00 \\
$\phi u$ & 1.00 & 1.00 & 1.69 & 1.69 & 6.00 \\ \hline \hline
\end{tabular}\hfill[4]
\end{solution}
\end{parts}