% -----------------------------------------------------
% QUESTION
%----------------------------------------------------------%
% First analytical questions set by Daniel Friedrich
% Same as 2018-19 resit but with different boundary conditions
% Heat equation with Laplace transform method
\question
\pointformat{(\bfseries\boldmath\themarginpoints)}

Consider the one-dimensional heat conduction equation
\begin{align}
\frac{\partial u}{\partial t} &= \kappa \frac{\partial^2u}{\partial x^2} & \quad  \label{eq:heat_equation_201908}\\
\intertext{on the interval $x\in[0, \infty)$ with the initial and boundary conditions }
u(x, 0) &= 2\sin(x), \quad \forall x\in(0, \infty), \label{eq:heat_IC1_201908} \\
u(0, t) &= 3 H(t), \quad \forall t. \label{eq:heat_BC1_201908} \\
\lim_{x\to\infty}u(x,t) &= 0, \quad \forall t. \label{eq:heat_BC2_201908}
\end{align}
where $\kappa$ is the thermal diffusivity and $H(t)$ is the Heaviside unit step function.

\begin{parts}
%----- Part -----------------------------------------------------------%
\part[10] Apply the Laplace transform with respect to $t$ to the PDE~(\ref{eq:heat_equation_201908}) and find the general solution for $U(x,s)$ in the Laplace domain.

\droppoints
\begin{solution}
The Laplace transform of the left hand side of the partial differential equation can be found using the derivative property and on the right hand side we can swap the order of integration and differentiation. This gives us the transformed equation
\begin{align}
s U(x, s) - u(x, 0) &= \kappa {\cal L} \left\{\frac{\partial^2 u(x,t)}{\partial x^2} \right\} = \kappa\frac{\partial^2 U(x,s)}{\partial x^2} 
\label{eq:heat_transformed_201908}
\end{align}

Insert the initial conditions into Eq.~(\ref{eq:heat_transformed_201908}) to get \hfill[2]
\begin{align}
sU(x, s) - 2\sin(\alpha x) &= \kappa \frac{\partial^2 U(x,s)}{\partial x^2} \nonumber\\
\Rightarrow \frac{\partial^2 U(x,s)}{\partial x^2} - \frac{s}{\kappa} U(x, s) &= \frac{-2}{\kappa}\sin(x) \label{eq:heat_laplace_201908}
\end{align}

The differential equation~(\ref{eq:heat_laplace_201908}) has the characteristic equation
\begin{align*}
m^2 - \frac{s}{\kappa} &= 0 \\
\intertext{which has the zeros}
 m_{1,2} &= \pm \sqrt{\frac{s}{\kappa}} \\
\end{align*}
Thus the complimentary function is
\begin{align}
U_g(x,s) &= A(s) e^{x\sqrt{\frac{s}{\kappa}}} + B(s) e^{-x\sqrt{\frac{s}{\kappa}}} \label{eq:heat_general_solution_201908}
\end{align}
where $A(s)$ and $B(s)$ are functions in the Laplace domain which we need to determine from the boundary conditions. \hfill[2]

Before we find the two functions we need to take care of the inhomogeneity of the differential equation. The trial solution for the particular integral for the inhomogeneous differential equation~(\ref{eq:heat_laplace_201908}) is
\begin{align*}
U_p(x,s) &= P\sin(x) + Q \cos(x)
\end{align*}
where the coefficients $P$ and $Q$ need to be determined. We substitute $U_p$ and its second derivative with respect to $x$ into Eq.~(\ref{eq:heat_laplace_201908}) and get \hfill[2]
\begin{align*}
-P \sin(x) - Q\cos(x) - \frac{s}{\kappa} \left( P\sin(x) + Q\cos(x)\right) &= -\frac{2}{\kappa} \sin(x)
\end{align*}
We can arrange this in terms of sine and cosine
\[
\sin(x) \left(-P - \frac{s}{\kappa} P + \frac{2}{\kappa}\right) + \cos(x) \left(-Q - \frac{s}{\kappa} Q\right) = 0
\]
From this we get two equations 
\begin{align*}
-P - \frac{s}{\kappa} P + \frac{2}{\kappa} &= 0 \\
-Q - \frac{s}{\kappa} Q &= 0
\end{align*}

From this it follows that
\begin{align*}
P &= \frac{2}{s + \kappa} \\
Q &= 0
\end{align*}
for $\Re(s)>-\kappa$.\hfill[2]

Thus, the general solution of the differential equation is of the form
\begin{align*}
U(x,s) &= A(s) e^{x\sqrt{\frac{s}{\kappa}}} + B(s) e^{-x\sqrt{\frac{s}{\kappa}}} + \frac{2}{s + \kappa} \sin(x)
\end{align*}
\hfill[2]
\end{solution}

%----- Part -----------------------------------------------------%
\part[6] Assume that the general solution to the PDE~(\ref{eq:heat_equation_201908}) is
\begin{align*}
U(x,s) &= A(s) e^{x\sqrt{\frac{s}{\kappa}}} + B(s) e^{-x\sqrt{\frac{s}{\kappa}}} + \frac{2}{s + \kappa} \sin(x)
\end{align*}
use the boundary conditions~(\ref{eq:heat_BC1_201908} -- \ref{eq:heat_BC2_201908}) to find the particular solution. Use the inverse Laplace Transform on the particular solution to get the solution of the heat conduction equation~(\ref{eq:heat_equation_201908} -- \ref{eq:heat_BC2_201908}) in the time domain.

\droppoints
\begin{solution}
From the boundary condition~(\ref{eq:heat_BC2_201908}) we know that the solution is bounded. Thus we can swap the limit and the integration to get 
\begin{align*}
\lim_{x\to\infty} U(x,s) &= \lim_{x\to\infty} \int_0^\infty e^{-st} u(x,t) dt = \int_0^\infty  e^{-st} \lim_{x\to\infty} u(x,t) dt = 0
\end{align*}
From this it follows that
\begin{align*}
A(s) &= 0, 
\end{align*}
because otherwise the first term on the right hand side of Eq.~(\ref{eq:heat_general_solution_201908}) would go to infinity for $x\to\infty$.
\hfill[2]

The boundary condition~(\ref{eq:heat_BC1_201908}) requires that
\begin{align*}
U(0,s) &= B(s) \overset{!}{=} {\cal L} \left\{u(0,t) \right\} =  \frac{3}{s}, \quad \Re(s)>-1,
\end{align*}
and thus \hfill[2]
\begin{align*}
 B(s) &= \frac{3}{s}
\end{align*}

From the solution in the Laplace domain
\begin{align*}
U(x,s) &= \frac{3}{s} e^{-x\sqrt{\frac{s}{\kappa}}} + \frac{2}{s + \kappa} \sin(x)
\end{align*}
we can find the solution in the time domain by using the Laplace transforms given in Table~\ref{table:functions}. \hfill[2]
\begin{align*}
u(x,t) &= 3\text{erfc}\left(\frac{x}{2\sqrt{s\kappa}} \right) + 2\exp(-\alpha t) \sin(x), t\ge 0.
\end{align*}
\end{solution}

%----- Part -----------------------------------------------------%
\part[4] Briefly (in less than 150 words), discuss the applicability of the analytical solution methods for the heat equation for this case.

\droppoints
\begin{solution}
\begin{itemize}
\item The Laplace transform method is obviously applicable as shown by the solution in parts (a) and (b). \hfill[1]
\item The zero boundary condition for $x\to\infty$ simplifies the Laplace domain solution significantly \hfill[1]
\item The semi-infinite spatial domain prevents the direct application of the method of separation of variables. We can't define the steady state solution which is required to generate the transient solution with zero boundaries. \hfill[2]
\end{itemize}
\end{solution}
\end{parts}
