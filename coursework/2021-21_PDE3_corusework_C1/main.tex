 % Add 'answers' to options to get the solutions
\documentclass[answers,addpoints,12pt]{exam}	

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Coursework 1}
\footer{}{Page \thepage\ of \numpages}{\iflastpage{\textbf{END}}{\textbf{Continued}}}

% Import PDE3 coursework style 
\usepackage{PDE3_coursework_style}
\usepackage{minted}

%-------------------------------------------------------%
%-------------------------------------------------------%
\begin{document}

The following coursework is to be handed in to the ETO online dropbox by \textbf{16:00 on Thursday the 17$^{th}$ of February 2022}. This is \textbf{coursework number 1 of 2}. Each coursework is worth $20\%$ of the final mark.
\vskip0.1cm
% ---- Modify
You should submit the solution to this coursework in the form of a short report on
Learn. This report must be typed, with at least 2cm margins, a font size of 11pt and line
spacing of at least 1, and must not be longer than 8 pages. The title page does not count
towards the page limit.

The marks for this coursework will be awarded for the correctness of the solution
as well as for the clarity and logical progression of the solution. Details on laying out
mathematical calculations and general guidelines for the coursework are given on Learn.

\vskip0.1cm

%----- Question ----------------------------------------%
% This is the start of the questions
\begin{questions}
\question[50] Consider an insulated pipe whose cross section is given by the following figure.
\centerline{\includegraphics[width=10cm]{Figures/Cw2021-22_C1_annulus.png}}

The temperature distribution in the red pipe can be described by the nondimensional Laplace equation in cylindrical coordinates which is given by
\begin{align*}
\pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}

The boundary conditions on the inside and outside of the pipe are given by
\begin{align}
    u(1, \theta) &= 0, \quad 0\le\theta\le 2\pi \label{eq:inner_BC}\\
    \pderiv{u}{r}(3, \theta) &= \begin{cases}
        0, \quad & 0\le\theta<\pi \\
        1, \quad & \pi\le\theta< 2\pi
        \end{cases} \label{eq:outer_BC}
\end{align}

\begin{parts}
%-------------------------------------------------------%
\part[8] Describe the heat transfer situation given by the boundary conditions~\ref{eq:inner_BC} and \ref{eq:outer_BC} and bring it into the context of a physical problem.

\begin{solution}
The inner boundary condition~\ref{eq:inner_BC} describes a case where the nondimensional temperature at the inner boundary of the annulus is fixed at the value of $0$. This can be achieved if the convective time constant is much smaller than the conductive time constant.

The outer boundary condition~\ref{eq:outer_BC} describes a case where the heat flux at the outer boundary of the annulus is $0$ on the top and $1$ at the bottom. 
This means that the top is perfectly insulating.
The positive slope at the bottom represents a heat flux in the negative $r$ direction which is into the insulated pipe.

\hfill[4 points each for the description of the inner and outer boundary]
\end{solution}

%-------------------------------------------------------%
\part[32] Calculate the analytical solution of the Laplace equation on the annulus with the given boundary conditions.

\begin{solution}
The Laplace equation in cyclindrical coordinates is given by
\begin{align*}
\pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
and we are looking for separated solutions of the form $u(r,\theta) = R(r) \Theta(\theta)$ which give us two ordinary differential equations
\begin{align*}
-\frac{\Theta''}{\Theta} &= \frac{R'' + \frac{R'}{r}}{\frac{R}{r^2}} = \lambda
\end{align*}
The solutions of these two equations will depend on the value of $\lambda$ and we need to consider three cases for negative, zero and positive $\lambda$.
For $\lambda<0$ we get the combined solution
\begin{align*}
u_-(r,\theta) &= \left(A\cosh(\mu\theta) + B\sinh(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
with parameters $A$, $B$, $C$ and $D$, and where we set $\lambda = -\mu^2$. 
Again, this needs to be periodic in $2\pi$ which is only fulfilled for $A=B=0$.
Thus the solution for $\lambda<0$ is zero. \hfill[4]

For $\lambda=0$ we get the solution
\[
u_0(r, \theta) = (a\theta + b)(C\ln(r) + D)
\]
with parameters $a$, $b$, $C$ and $D$. We know that the solution needs to be periodic in $2\pi$ and this is only the case for $a=0$. We set $b=1$ without loss of generality so that it can be combined with $C$ and $D$.

Let us know look at the boundary conditions. At the inner boundary we need to have $u_0(1,\theta)=0$ so that
\begin{align*}
u_0(1, \theta) &= C \ln(1) + D \overset{!}{=} 0
\end{align*}
and from this it follows that $D=0$. 
At the outer boundary we have the Neumann condition and we leave this until we have build the complete solution. 
Thus, we are left with
\begin{align*}
u_0(r,\theta) = C\ln(r) 
\end{align*}
where $C$ is a constant which needs to be determined. \hfill[4]

For $\lambda = \mu^2 > 0$ we get the combined solution
\begin{align*}
u_+(r,\theta) &= \left(A\cos(\mu\theta) + B\sin(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
with parameters $A$, $B$, $C$ and $D$. Again this needs to be periodic in $2\pi$ and this is the case only for $\mu=n$ with $n\in\mathbb{N}$. 
The inner boundary condition is already fulfilled by solution $u_0$ therefore $u_+$ needs to be zero at the inter boundary.
\[
u_+(1, \theta) = \left(A\cos(n\theta) + B\sin(n\theta)\right) \left(C 1^{-n} + D 1^{n}\right) \overset{!}{=} 0, \quad 0\le\theta\le2\pi
\]
and for $n\in\mathbb{N}$. 
Since the first part is not zero for all $\theta$, it follows that $C=-D$. 
Thus, the solution is given by
\[
u_+(r, \theta) = \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) \left(r^{n} - r^{-n}\right), \quad n=1,2,\dots
\]
where we have set $a_n = AD$ and $b_n = BD$. \hfill[4]

To fulfil the outer boundary condition we need to superimpose the solutions for $\lambda=0$ and $\lambda>0$. This is possible because we are dealing with a linear PDE. 
The complete solution is \hfill[4]
\begin{align}
u(r, \theta) &= C\ln(r) + \sum_{n=1}^\infty \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) \left(r^{n} - r^{-n}\right) 
\label{eq:superposed_solution}
\end{align}

To find the coefficients $C$, $a_n$ and $b_n$ we need to set it equal to the boundary condition. 
We calculate the derivative with respect to $r$ of the superposed solution~\ref{eq:superposed_solution} and set it equal to the outer boundary condition~\ref{eq:outer_BC} at $r=3$ \hfill[4]
\begin{align*}
\pderiv{u}{r}(3, \theta) &= \frac{C}{3} + \sum_{n=1}^\infty \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) \left(n3^{n-1} + n3^{-n-1}\right) \\
 &\overset{!}{=} \begin{cases}
        0, \quad & 0\le\theta<\pi \\
        1, \quad & \pi\le\theta\le 2\pi
        \end{cases} 
\end{align*}
The parameters $D$, $a_n$ and $b_n$ can be found with the usual method of the Fourier series.

The expression $\frac{C}{3}$ is equivalent to $\frac{a_0}{2}$ in the Fourier series theory so that we get
\begin{align*}
 \frac{2C}{3} = a_0 &= \frac{2}{2\pi} \int_{-\pi}^{\pi} \pderiv{u}{r}(3, \theta) d\theta = \frac{1}{\pi} \int_{\pi}^{2\pi} d\theta = 1
\end{align*}
from which it follows that $C = \frac{3}{2}$.
The parameter $a_n$ is given by \hfill[4]
\begin{align*}
a_n &= \frac{1}{n\left(3^{n-1} + 3^{-n-1}\right)} \frac{2}{2\pi} \int_{-\pi}^{\pi} \pderiv{u}{r}(3, \theta) \cos(n\theta) d\theta \\
 &= \frac{1}{n\left(3^{n-1} + 3^{-n-1}\right)} \frac{1}{\pi} \int_{\pi}^{2\pi} \cos(n\theta) d\theta = 0
\end{align*}
The parameter $b_n$ is zero because the boundary condition is even.\hfill[4]
\begin{align*}
b_n &= \frac{1}{n\left(3^{n-1} + 3^{-n-1}\right)} \frac{2}{2\pi} \int_{-\pi}^{\pi} \pderiv{u}{r}(3, \theta) \sin(n\theta) d\theta \\
 &=  \frac{1}{n\left(3^{n-1} + 3^{-n-1}\right)} \frac{1}{\pi} \int_{\pi}^{2\pi} \sin(n\theta) d\theta \\
 &= \frac{-1}{\pi n^2\left(3^{n-1} + 3^{-n-1}\right)} \left[\cos(n\theta)\right]_{\pi}^{2\pi} \\
 &= \begin{cases}
    \frac{-2}{\pi n^2\left(3^{n-1} + 3^{-n-1}\right)}, \quad & n \text{ odd} \\
    0, \quad & n \text{ even} 
    \end{cases} 
\end{align*}

Thus, the complete solution is given by 
\begin{align*}
u(r, \theta) =&  C\ln(r) + \sum_{n=1}^\infty b_n \sin(n\theta) \left(r^{n} - r^{-n}\right) \\
 =& \frac{3}{2}\ln(r) + \sum_{k=1}^\infty \frac{-2}{\pi (2k-1)^2\left(3^{2k-2} + 3^{-2k}\right)}  \sin((2k-1)\theta) \left(r^{2k-1} - r^{-2k+1}\right) 
\end{align*}
for $1\le r \le 3$ and $0\le\theta\le 2\pi$ and with $n=2k-1$.\hfill[4]
\end{solution}

%-------------------------------------------------------%
\part[10] Plot the analytical solution and discuss the convergence of the Fourier series. 

\vskip0.2cm
\textbf{Remarks:}
\begin{itemize}
\item Have a look at the exercise sheets and examples to get started.
\item The combined solution needs to fulfil the boundary conditions.
\end{itemize}

\begin{solution}
The Fourier series has second order convergence. Before we plot the analytical solution, we compare the analytical solution for different cut-off points of the Fourier series.
We compare a cut-off of 10, 20, 40, and 80 with a larger cut-off of 300. 
The maximum difference between these solutions on an 40x40 grid are $1.7\times10^{-2}$, $5.6\times10^{-3}$, $1.8\times10^{-3}$ and $4.8\times10^{-4}$, respectively. 
The sum of the square of the differences are $2.5\times10^{-2}$, $1.5\times10^{-4}$, $1.2\times10^{-5}$ and $6.7\times10^{-7}$, respectively. 

From this we choose a cut-off of 20 for the rest of this coursework. \hfill[4]

The solution on a 40x40 grid with a cut-off of 20 is given in the following plots. \hfill[6]

\includegraphics[width=0.5\columnwidth]{Figures/CW1_PDE3_2021-22_Analytical_solution_surf.png}
\includegraphics[width=0.5\columnwidth]{Figures/CW1_PDE3_2021-22_Analytical_solution_contour.png}
\end{solution}
\end{parts}

%----- Question ----------------------------------------%
\question[50] Using the \texttt{Grid} class defined in the \textsc{Laplace-polar} Jupyter notebook, complete the function \texttt{PolarLaplaceSolver} to produce a numerical solution to the coursework PDE using the bi-conjugate gradient solver from the \textsc{scipy} library.  

You are reminded that your \textsc{Python} code will need to assemble both the $A$ matrix and the $\vec b$ RHS vector so that the coefficients are those for the discretised version of the nondimensional Laplace equation in cylindrical coordinates.  You are also reminded that there is a periodic boundary condition $u(r,0)=u(r,2\pi)$ which has implications for the structure of the $A$ matrix.

Write a short report in which you need to explain what you are doing, compare the solution against the analytical solution, perform a grid convergence analysis and plot the solution.  

Your report should include the derivation of the stencil.

It should not include the whole \textsc{Python} code, but must include code fragments implementing the periodic boundary, the Neumann boundary condition at $r=3$ and showing how the coefficients are calculated for the computational stencil needed for the cylindrical coordinate version of the Laplace equation.


\begin{solution}
In implementing the solver we are using the solver from the BiCGStab workbook as a template, remembering that in this case we know that the $r=1$ boundary is a Dirichlet BC with $u=(1,\theta)=0$.  The $r=3$ boundary condition is a Neumann condition where

\begin{align*}
    u(1, \theta) &= 0, \quad 0\le\theta\le 2\pi \label{eq:inner_BC}\\
    \frac{\partial u}{\partial r}(3, \theta) &= \begin{cases}
        0, \quad & 0\le\theta<\pi \\
        1, \quad & \pi\le\theta\le 2\pi
        \end{cases}
\end{align*}

We also need to enforce the periodic boundary condition $u(r,0)=u(r,2\pi)$ and to modify the coefficients in the coefficient matrix to include both the additional derivative and the $1/r$ and $1/r^2$ coefficients.

\textbf{Matrix coefficients}

The first step is discretise the governing equation.  The cylindrical polar form of the Laplace equation
\[\frac{\partial^2 u}{\partial r^2}+\frac{1}{r}\frac{\partial u}{\partial \theta} + \frac{1}{r^2}\frac{\partial^2 u}{\partial \theta^2}=0
\]
is discretised using the centred, 2nd order, finite difference equations, 
 \begin{align*}
 \frac{\partial^2 u}{\partial r^2} &= \frac{u_N - 2u_O + u_S}{\Delta r^2} +O(\Delta r^2),\\
 \frac{\partial u}{\partial \theta} &= \frac{u_N - u_S}{2\Delta r} +O(\Delta \theta^2), \text{ and}\\
 \frac{\partial^2 u}{\partial \theta^2} &= \frac{u_E - 2u_O + u_W}{\Delta \theta^2}+O(\Delta \theta^2)
 \end{align*}
\hfill[Stating FD approx 3 marks]

Giving:
\[\frac{u_N - 2u_O + u_S}{\Delta r^2} + \frac{1}{r_j} \frac{u_N - u_S}{2\Delta r} + \frac{1}{r_j^2} \frac{u_E - 2u_O + u_W}{\Delta \theta^2} = 0\]
\hfill[1]

Collecting terms \hfill[2]
\begin{align*}
\frac{2\Delta r^2 u_W+2\Delta r^2 u_E}{2\Delta r^2\Delta\theta^2r^2}
+\frac{(2\Delta\theta^2 r^2-\Delta r\Delta\theta^2r) u_s}
{2\Delta r^2\Delta\theta^2r^2} & + \\
\frac{(2\Delta\theta^2 r^2+\Delta r\Delta\theta^2r) u_N}{2\Delta r^2\Delta\theta^2r^2}
-\frac{(4\Delta\theta^2r^2-4\Delta r^2) u_O}{2\Delta r^2\Delta\theta^2r^2}
&=0
\end{align*}

So the coefficients for the $A$ matrix are: \hfill[5]
\begin{align*}
u_N &:\ \frac{2r+\Delta r}{2r\Delta r^2}\\
u_E &: \ \frac{1}{r^2\Delta\theta^2} \\
u_S &: \ \frac{2r-\Delta r}{2r\Delta r^2} \\ 
u_W &:\ \frac{1}{r^2\Delta\theta^2} \\
u_O &:\ -2\frac{r^2\Delta\theta^2+\Delta r^2}{r^2\Delta r^2\Delta\theta^2} \\
\end{align*}

Since these coefficients depend on $r$ they need to be calculated  inside the matrix assembly loop using values from \texttt{grid.r}. So we need something like
\begin{minted}[fontsize=\footnotesize]{python}
    d_r = grid.Delta_r()  
    d_theta = grid.Delta_theta()
    
    for j in range(grid.Nj): # radial direction
            for i in range(grid.Ni): # circumferential direction

                # calculate the coefficients
                north = (2 * grid.r[j,i] + d_r) / (2*grid.r[j,i] * d_r**2)
                c_theta = 1 / (grid.r[j,i]**2 * d_theta**2)  # east and west
                south = (2 * grid.r[j,i] - d_r)/(2 * grid.r[j,i] * d_r**2)
                centre = - 2 * (d_theta**2 * grid.r[j,i]**2 + d_r**2) \
                    / (grid.r[j,i]**2 * d_r**2 * d_theta**2)
\end{minted}
\hfill(appropriate python code [5])

\textbf{Boundary Conditions}

It is useful to think about a small grid and to workout what points are needed in the stencil to deal with the periodic conditions.  Here is a cartoon of a 5x5 grid and the associated sparsity pattern of the $A$ matrix.  Periodic boundary conditions are in yellow.  Because we are directly assembling the matrix, \textbf{no grid cells are being used to hold boundary condition information}, so we won't need 1 cell on each side of the grid to hold ghost cell information.

This means the solver uses the whole grid.

\centerline{\includegraphics[width=0.5\columnwidth]{Figures/LaplaceGrid-k.png}}

\textbf{Periodic Boundaries}

On the left and right boundaries (where $\theta=0$ or $\theta=2\pi$ we have a periodic condition, this means that when $j=0$ the west node is at $j=N_j-1$ and when $j=N_j-1$ the east node is at $j=0$.  We need to do this in the matrix assembly routine.
\hfill[2]

\begin{minted}[fontsize=\footnotesize]{python}
    for j in range(grid.Nj): # radial direction
            for i in range(grid.Ni): # circumferential direction
               # calculate coefficients ...
               
	       # calculate the equation number
                k = i + grid.Ni*j

                # East boundary (periodic)
                if i<test.Ni-1:
                    ke=k+1
                else:
                    ke=k-grid.Ni+1
                A_mat[k,ke] += c_theta
            
                # West boundary (periodic)
                if i>0:
                    kw=k-1
                else:
                    kw=k+grid.Ni-1
                A_mat[k,kw] += c_theta
\end{minted}
\hfill(appropriate python code [3])

\textbf{$r=3$ Boundary}

The  $r=3$ boundary has a Neumann boundary condition whose gradient depends on the value of $\theta$. When the gradient is zero we have

$$\frac{u_N-u_S}{2\Delta r}=0 \implies u_N = u_s.$$
\hfill[1]

substituting into the stencil 

$$
\frac{2u_S - 2u_O}{\Delta r^2} + \frac{1}{r_j^2} \frac{u_E - 2u_O + u_W}{\Delta \theta^2} = 0
$$
\hfill[1]

and collecting terms we find

$$\frac{\Delta r^2u_W + 2r^2\Delta\theta^2u_S -2(r^2\Delta\theta^2 + \Delta r^2)u_O+\Delta r^2U_E}
{r^2\Delta r^2\Delta\theta^2 }=0.$$
\hfill[2]

We can see that the coefficients of $u_W$, $u_E$ and $u_O$ remain unchanged, the coefficient of $u_N$ is zero, but the coefficient of $u_S$ becomes

$$C_s=\frac{2}{\Delta r^2}.$$
\hfill[1]

When $\theta>\pi$ and the gradient is $1$ we have:

\begin{align*}
\frac{u_N-u_S}{2\Delta r}&=1 \\
u_N-u_S&=2\Delta r \\
u_N &= u_S+2\Delta r
\end{align*}
\hfill[2]

substituting into the stencil
$$\frac{u_W-2u_O+u_E}{r^2\Delta \theta^2}+\frac{2u_S-2u_O+2\Delta r}{\Delta r^2}+\frac{1}{r}=0$$
\hfill[1]

and collecting terms we find that $u_s$ coefficient becomes

$$C_s=\frac{2}{\Delta r^2}.$$
\hfill[1]

There is also a constant term on the LHS of the stencil, $\frac{2r+\Delta r}{r\Delta r}$.
So we also need to set $b_k=-\frac{2r+\Delta r}{r\Delta r}$ at the North boundary when $\theta>\pi$.
\hfill[2]

\begin{minted}[fontsize=\footnotesize]{python}
                # North boundary
                if j < (grid.Nj-1):
                    A_mat[k,k+grid.Ni] = north
                    kn=k+grid.Ni
                else:
                    kn=k-grid.Ni
                    # Neumann boundary (depends on theta)
                    if grid.theta[j,i]<np.pi:
                        A_mat[k,k-grid.Ni] = 2/d_r**2
                    else:
                        A_mat[k,k-grid.Ni] = 2/d_r**2
                        b_vec[k] +=  - (2*grid.r[j,i]+d_r)/(grid.r[j,i]*d_r)
\end{minted}
\hfill(Appropriate python code [4])

\textbf{$r=1$ Boundary}

On the $r=1$ boundary $u(1,\theta)=0$, this means that $u_S$ is known and must appear on the RHS.  Consequently $0\times(2r-\Delta r)/(2r\Delta r^2)=0$ must be subtracted from $b_k$, so there is nothing to be done.
\hfill[Explanation with appropriate Python code 2 marks]

\begin{minted}[fontsize=\footnotesize]{python}
                # South boundary, North boundary also sets this
                if j>0 and j<(grid.Nj-1):
                    ks=k-grid.Ni
                    A_mat[k,ks]=south
\end{minted}

\emph{Remark:}

It is useful to think about the shape of the matrix

\centerline{\includegraphics[width=0.5\columnwidth]{Figures/SparsityPattern-filled.png}}

This can be checked against then result from the Python code for a  $5\times5$ grid matrix:

\centerline{\includegraphics[width=0.5\columnwidth]{Figures/Matrix-structure.png}}

Once we are happy the code is correct, then the solver can be applied to a realistic grid, obtaining the solution:

\textbf{Solution}

The solution can now be computed. I'm using a $361\times101$ grid so we have $1^\circ$ resolution in the $\theta$ direction.  

\centerline{\includegraphics[width=0.5\columnwidth]{Figures/Coursework1.pdf}}

Marks are:
\begin{enumerate}
\item Plot heading including mesh size information [1]
\item Equal axis-polar plot showing a nice circle [1]
\item Colour bar or numbered contours showing u values [1]
\item 1 sentence comparison with with the analytical solution [2]
\end{enumerate}

\textbf{Mesh refinement study}
The solution should be computed on a series of grids with an appropriate integrated metric being computed. The code from the other examples can be reused.  I'm going to use a very fine series of grids so that we have good circular resolution -- but this is only for an example.  You must quote the results from the refinement analysis tool and interpret them.

The metric I've chose is \[U=\int_{\frac{\pi}{2}}^{\frac{3\pi}{2}}u(2.0,\theta) d\theta.\]  Any reasonable choice is worth \hfill[1]

Here are some results from running the tool. [3 marks] for using 4 or more grids, [2 marks] for using three grids and [1 mark] for using 2 grids.

\begin{minted}{text}
Refinement Analysis on 5 grids.

Grid Delta x ratio   U
----------------------------------------------------------
   1 0.006538    2   3.24976
   2 0.01306     2   3.24289
   3 0.02607     2   3.21926
   4 0.05193     2   3.17159
   5   0.103   ---   3.00397
---------------------------------------------------------- 

order of convergence, p =   1.78

Grids       GCI
---------------
 1  2  0.001084
 2  3  0.003734
 3  4  0.007589
 4  5   0.02709
---------------

 Grid Step  GCI Ratio Converged
-------------------------------
 1 2  2 3      0.9979 True
 2 3  3 4       1.692 False
 3 4  4 5      0.9636 False
-------------------------------
\end{minted}

\centerline{\includegraphics[width=0.7\columnwidth]{Figures/CW1-refinement.pdf}}

This shows the formal order of accuracy is $1.78$ which is close to the formal order of accuracy of 2. \hfill[1]

The first three grids seem to be converged, with a Ratio is close to 1, the last set of grids also have a ratio close to 1, but the middle grids (nos 2, 3 and 4) do not. \hfill[1]

The graph shows the expected convergence properties with consistent behaviour. The estimate on a mesh with zero seems consistent with the finer grid values \hfill [1]

\emph{Remark:}

Students may see good refinement behaviour, especially if the the integral quantity does not go all the way round!  They should interpret the order, the plot and the GCI ratios for [1] mark each.

\end{solution}

\end{questions}
\end{document}

