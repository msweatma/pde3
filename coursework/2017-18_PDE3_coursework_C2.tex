 % Add 'answers' to options to get the solutions
\documentclass[answers,addpoints,12pt]{exam}	

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Coursework 2}
\footer{}{Page \thepage\ of \numpages}{\iflastpage{\textbf{END}}{\textbf{Continued}}}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_coursework_style}

%-------------------------------------------------------%
\begin{document}

The following coursework is to be handed in to the ETO dropbox by \textbf{16:00 on Friday the 17$^{th}$ of November 2017}. This is \textbf{coursework number 2 of 2}. Each coursework is worth $20\%$ of the final mark.
\vskip0.1cm
You should submit the solution to this coursework in the form of a short report on Learn. This report must not be longer than 7 pages with at least 2cm margins and a font size of 11pt. The marks for this coursework will be awarded for the correctness of the solution as well as for the clarity and logical progression of the solution. Details on laying out mathematical calculations and general guidelines for the coursework are given on Learn. 

\vskip0.2cm
\textbf{Remarks:}
\begin{itemize}
\item The differential equation
\[
\frac{\text{d}^2 u(x)}{\text{d}x^2} - \sigma^2 u(x) = 0
\]
has the equivalent solutions
\begin{align*}
u_1(x) &= A e^{\sigma x} + B e^{-\sigma x} \\
u_2(x) &= \tilde{A~} \sinh(\sigma x) + \tilde{B} \sinh(\sigma(l-x))
\end{align*}
Depending on the problem one or the other leads to an easier solution.
\item The Laplace domain function
\[
F(s) = \frac{\sinh(b\sqrt{s})}{s^2\sinh(a\sqrt{s})}
\]
has the inverse transform
\begin{align*}
f(t) =& \frac{bt}{a} + \frac{2a^2}{\pi^3} \sum_{n=1}^\infty \frac{(-1)^n}{n^3}\left(1 - e^{-n^2\pi^2 t/a^2}\right) \sin\left(\frac{n\pi b}{a}\right)
\end{align*}
\end{itemize}

\vskip0.1cm

%----- Question ----------------------------------------%
% This is the start of the questions
\begin{questions}
\question Consider the one-dimensional heat conduction equation
\begin{align}
\frac{\partial u(x,t)}{\partial t} &= \kappa \frac{\partial^2 u(x,t)}{\partial x^2} \tag{a} \\
\intertext{on the interval $0<x<\pi$ for the initial condition}
u(x,0) &= \sin(x), \quad 0\le x< \pi, \tag{b} \\
\intertext{and the boundary conditions} 
u(0,t) &= g_0(t), \quad \forall t, \tag{c} \\
 u(\pi, t)&= g_1(t), \quad \forall t. \tag{d} 
\end{align}

\begin{parts}
%-------------------------------------------------------%
\part[35] By using the Laplace transform method with respect to time $t$ show that the Laplace domain solution is
\begin{align*}
U(x,s) =& \frac{G_1(s)}{\sinh\left(\pi\sqrt{\frac{s}{\kappa}}\right)}\sinh\left(x\sqrt{\frac{s}{\kappa}}\right) + \frac{G_0(s)}{\sinh\left(\pi\sqrt{\frac{s}{\kappa}}\right)}\sinh\left((\pi-x)\sqrt{\frac{s}{\kappa}}\right) \\
&\quad + \frac{1}{s + \kappa}\sin(x)
\end{align*}

\begin{solution}
We apply the Laplace transform with respect to $t$ to the heat conduction equation to get \hfill[4]
\[
sU(x,s) - u(x,0) = \kappa \frac{\text{d}^2U(x,s)}{\text{d}x^2}
\]
We insert the initial condition into the transformed equation to get
\[
sU(x,s) - \sin(x) = \kappa \frac{\text{d}^2U(x,s)}{\text{d}x^2}
\]
This is rearrange so that only the inhomogeneity is on the right hand side
\begin{equation}
\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s}{\kappa}U = \frac{-\sin(x)}{\kappa}
\label{eq:2017_C2_transformed_PDE}
\end{equation}

This ODE can be solved by finding the characteristic polynomial \hfill[4]
\[
m^2 - \frac{s}{\kappa} = 0
\]
which has roots $m_{1,2} = \pm\sqrt{\frac{s}{\kappa}}$. For these roots we get the complementary solution 
\[
U(x,s) = A(s)\sinh\left(x\sqrt{\frac{s}{\kappa}}\right) + B(s)\sinh\left((\pi-x)\sqrt{\frac{s}{\kappa}}\right)
\]

Before we find the two, unknown functions, $A(s)$ and $B(s)$, we need to take care of the inhomogeneity of the differential equation. The right hand side is a sine function thus a suitable trial solution for the particular integral for the inhomogeneous differential equation is \hfill[4]
\begin{align*}
U_p(x,s) &= P \sin(x) + Q \cos(x)
\end{align*}
where the coefficients $P$ and $Q$ need to be determined. We substitute $U_p$ and its second derivative with respect to $x$ into Eq.~\ref{eq:2017_C2_transformed_PDE} and get 
\begin{align*}
-P \sin(x) - Q\cos(x) - \frac{s}{\kappa} \left( P\sin(x) + Q\cos(x)\right) &= -\frac{\sin(x)}{\kappa}
\end{align*}

We arrange this in terms of sine and cosine \hfill[4]
\[
\sin(x) \left(-P - \frac{s}{\kappa} P + \frac{1}{\kappa}\right) + \cos(x) \left(-Q - \frac{s}{\kappa} Q\right) = 0
\]
This gives us two equations which can be solved for $P$ and $Q$
\begin{align*}
0 &= -P\left(1 + \frac{s}{\kappa}\right) + \frac{1}{\kappa} &\Rightarrow \qquad & P = \frac{1}{s + \kappa} \\
0 &= -Q - \frac{s}{\kappa} Q &\Rightarrow\qquad & Q=0
\end{align*}
Thus the general solution is 
\begin{equation}
U(x,s) = A(s)\sinh\left(x\sqrt{\frac{s}{\kappa}}\right) + B(s)\sinh\left((\pi-x)\sqrt{\frac{s}{\kappa}}\right) + \frac{1}{s + \kappa}\sin(x)
\label{eq:2017_C2_general_solution}
\end{equation}

Now we use the boundary conditions to find the two functions, $A(s)$ and $B(s)$. The Laplace transform of the two boundary conditions are given by $G_0(s)$ and $G_1(s)$. By setting the general solution~\ref{eq:2017_C2_general_solution} to the boundary conditions we get \hfill[4]
\begin{align*}
U(0,s) &=  A(s)\sinh\left(0\sqrt{\frac{s}{\kappa}}\right) + B(s)\sinh\left((\pi-0)\sqrt{\frac{s}{\kappa}}\right) + \frac{1}{s + \kappa}\sin(0) \overset{!}{=} G_0(s) \\
U(\pi,s) &=  A(s)\sinh\left(\pi\sqrt{\frac{s}{\kappa}}\right) + B(s)\sinh\left((\pi-\pi)\sqrt{\frac{s}{\kappa}})\right) + \frac{1}{s + \kappa}\sin(\pi) \overset{!}{=} G_1(s)
\end{align*}
Simplifying these equations we get expressions for $A(s)$ and $B(s)$ \hfill[4]
\begin{align*}
B(s)\sinh\left(\pi\sqrt{\frac{s}{\kappa}}\right) &= G_0(s) &\Rightarrow\qquad & A(s) = \frac{G_1(s)}{\sinh\left(\pi\sqrt{\frac{s}{\kappa}}\right)} \\
A(s)\sinh\left(\pi\sqrt{\frac{s}{\kappa}}\right) &= G_1(s) &\Rightarrow\qquad & B(s) = \frac{G_0(s)}{\sinh\left(\pi\sqrt{\frac{s}{\kappa}}\right)} 
\end{align*}

Thus the Laplace domain solution is \hfill[4]
\begin{align*}
U(x,s) =& \frac{G_1(s)}{\sinh\left(\pi\sqrt{\frac{s}{\kappa}}\right)}\sinh\left(x\sqrt{\frac{s}{\kappa}}\right) + \frac{G_0(s)}{\sinh\left(\pi\sqrt{\frac{s}{\kappa}}\right)}\sinh\left((\pi-x)\sqrt{\frac{s}{\kappa}}\right) \\
&\quad + \frac{1}{s + \kappa}\sin(x)
\end{align*}

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[7]
\end{solution}

%-------------------------------------------------------%
\part[30] Now consider the specific boundary conditions
\begin{align*}
u(0,t) &= g_0(t) = 0, \quad &\forall t,\\
u(\pi, t)&= g_1(t) = t, \quad &\forall t.
\end{align*}
and find the time domain solution. Use Matlab to plot the solution for $\kappa=0.1$ between $0\le t\le 4$ and investigate the convergence of the series solution.

\begin{solution}
Apply the Laplace transform to the boundary conditions to get \hfill[4]
\begin{align*}
G_0(s) &= 0 \\
G_1(s) &= \frac{1}{s^2}
\end{align*}
So the Laplace domain solution is given by
\begin{align*}
U(x,s) =& \frac{1}{s^2\sinh\left(\pi\sqrt{\frac{s}{\kappa}}\right)}\sinh\left(x\sqrt{\frac{s}{\kappa}}\right) + \frac{1}{s + \kappa}\sin(x)
\end{align*}

We can bring this into the form required for the inverse given in the remarks above. \hfill[4]
\begin{align*}
U(x,s) =& \frac{\sinh\left(\frac{x}{\sqrt{\kappa}}\sqrt{s}\right)}{s^2\sinh\left(\frac{\pi}{\sqrt{\kappa}} \sqrt{s}\right)} + \frac{1}{s + \kappa}\sin(x)
\end{align*}
with the parameters
\begin{align*}
a &= \frac{\pi}{\sqrt{\kappa}} \\
b &= \frac{x}{\sqrt{\kappa}}
\end{align*}

Apply the given formula to get \hfill[4]
\begin{align*}
u(x,t) =& \frac{bt}{a} + \frac{2a^2}{\pi^3} \sum_{n=1}^\infty \frac{(-1)^n}{n^3}\left(1 - e^{-n^2\pi^2 t/a^2}\right) \sin\left(\frac{n\pi b}{a}\right) + \sin(x) e^{-\kappa t}
\end{align*}
We replace $a$ and $b$ to get
\begin{align*}
u(x,t) =& \frac{xt}{\pi} + \frac{2}{\pi\kappa} \sum_{n=1}^\infty \frac{(-1)^n}{n^3}\left(1 - e^{-n^2\kappa t}\right) \sin\left(n x\right) + \sin(x) e^{-\kappa t}
\end{align*}

The following plots show the contour and surface of the analytical solution for 40 terms of the infinite series.\hfill[4]

\includegraphics[width=0.5\columnwidth]{C2_analytical_contour.png}
\includegraphics[width=0.5\columnwidth]{C2_analytical_solution.png}

Looking at the series solution we see that the magnitude of the terms of the series reduces with the third power of $n$. Thus it is conceivable that only a small number of terms is required. To check the convergence we calculate the solution $u_N(x,t)$ for different number $N$ of terms of the series. We use these solutions to calculate the difference between the solutions for different numbers of terms \hfill[4]
\[
I = \int_0^\pi \int_0^4 (u_{N_1}(x,t) - u_{N_2}(x,t))^2 dt dx
\]

For different numbers of terms we get the following table of results. \hfill[4]

\begin{tabular}{c|c|c}
$N_1$ & $N_2$ & $I$ \\
\hline
2 & 5 & $2.8\times10^{-1}$ \\ 
5 & 10 & $8.6\times10^{-3}$ \\ 
10 & 20 & $3.7\times10^{-4}$ \\
20 & 40 & $1.3\times10^{-5}$ \\
40 & 80 & $2.1\times10^{-7}$ \\
80 & 160 & $4.1\times10^{-9}$ \\
160 & 10000 & $9.1\times10^{-11}$ 
\end{tabular}

We can see that between 5 and 10 terms the solution changes by less than $1\%$ and between 10 and 20 terms by less than $0.05\%$. Thus 10 terms of the solution should be sufficient for this case. 

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[6]
\end{solution}

%-------------------------------------------------------%
\part[35] Develop a script in Matlab to solve the problem from part (b) numerically. Explain what you are doing, compare the solution against the analytical solution and discuss the accuracy of your solution. Plot the solution.

\begin{solution}
We need to modify one of the codes provided for the solution of the heat conduction equation.  In this case I will use the Crank-Nicholson method though the FTCS scheme can also be used. Two marks will be awarded for choosing an appropriate scheme and justifying the choice. \hfill[2]

The solution algorithm is as follows:
\begin{enumerate}
\item We define a grid in the $x$ direction with a mesh spacing of \hfill[4]
\[
\Delta x=\pi/(N-1)
\]
where we have
\[
x_i = i \Delta x, \quad i = 1, 2, \dots, N
\]
This can be done in Matlab as
{\small\begin{verbatim}
N=401; 
dx=pi/(N-1); % mesh spacing
x=0.0:dx:pi; %generate the grid
\end{verbatim}}
\item Next we calculate $\Delta t=\nu\Delta x^2/(2\kappa)$, for the FTCS scheme $\nu\le1$ while for Crank-Nicholson there is no limit on $\nu$ but a choice of $\nu\le50$ is reasonable. \hfill[4]

This can be done in Matlab as
{\small\begin{verbatim}
kappa=0.1; 		% Thermal diffusion coefficient
nu=50.0; 		% Courant number
dt=nu*dx^2/(2*kappa);
\end{verbatim}}
\item Boundary and initial conditions must be set so that \hfill[4	]
\begin{align*}
u(0,t) &= 0, \quad \forall t\ge0 \\
u(\pi,t) &= t, \quad \forall t\ge0 \\
u(x,0) &= \sin(x), \quad 0\le x\le\pi
\end{align*}

This can be done in Matlab as
{\small\begin{verbatim}
U(1,:) = sin(x); % set inital conditions

% set the boundary condtions
U(:,1) = 0.0;
U(:,N) = t;
\end{verbatim}}

\item To solve the problem we need to discretise the equations. For the Crank-Nicholson method the discretisation of the heat conduction equation is
\[
-r_x U_{i-1}^{n+1} + (2 + 2r_x) T_i^{n+1} - r_x T_{i+1}^{n+1} = r_x U_{i-1}^{n} + (2 - 2r_x) T_i^{n} + r_x T_{i+1}^{n}
\]
where the spatial and temporal indices are $i=1,2,\dots, N$ and $n=1,2,\dots$, respectively. Here the parameter $r_x$ is given by
\[
r_x = \kappa \frac{\Delta t}{\Delta x^2}
\]

The equation system is tridiagonal and can be solved with the Thomas algorithm. This can be done in Matlab as
{\small\begin{verbatim}
Rx=kappa*dt/(dx^2);
for time=dt:dt:4.0
    % compile the RHS (from the old time level data)
    RHS(1:N-2)=Rx*U(i,1:N-2)+2*(1-Rx)*U(i,2:N-1)+Rx*U(i,3:N);
    
    % now assemble the A matrix, as an n x 3 matrix 
    Amat(1:N-2,1)=-ones(N-2,1)*(Rx); % sub diagonal
    Amat(1:N-2,2)=ones(N-2,1)*2*(1+Rx); %diagonal
    Amat(1:N-2,3)=-ones(N-2,1)*(Rx); % super diagonal

    % boundary conditions on LHS
    Amat(1,1)=0.0; % u(1) is known and zero
    
    % boundary condition on RHS
    amat(N-2,3)=0.0; %u(N) is known and non-zero
    RHS(N-2)=RHS(N-2)+Rx*U(i+1,N);

    % solve the system
    U(i+1,2:N-1)=thomas(N-2,Amat,RHS);
    i=i+1;
end
\end{verbatim}}

The key point is that the LHS and RHS boundary conditions are Dirichlet BC and the RHS vector and coefficients of the $A$ matrix need to be amended. 
\hfill[8]
\end{enumerate}

Finally the solution should be plotted \hfill[4]

\includegraphics[width=0.5\columnwidth]{C2_numerical_contour.png}
\includegraphics[width=0.5\columnwidth]{C2_numerical_solution.png}

There should be a discussion of the necessary grid size for a good solution and comparisons with the analytical solution. 
The plots look very similar to the analytical solutions in part (b) which is confirmed by the contour and surface plots of the difference between the numerical and analytical solution for a 200 by 260 grid and 80 terms in the analytical solution. These plots show that the maximum difference is about $0.004$. \hfill[5]

\includegraphics[width=0.5\columnwidth]{C2_difference_contour.png}
\includegraphics[width=0.5\columnwidth]{C2_difference_solution.png}

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[4]
\end{solution}

\end{parts}
\end{questions}

\newpage
%----- Table -------------------------------------------------------%
\begin{table}[ht]
\caption{Properties of the unilateral Laplace transform} % title of Table
\centering % used for centering table
\begin{tabular}{|c|c|c|} % centered columns (4 columns)
\hline %inserts double horizontal lines
Property & $f(t), t\ge0$ & $F(s)$ \\ 
%heading
\hline 
Definition & $f(t)$ & $\int_0^\infty\exp(-st)f(t)dt$ \\ [0.3ex]
Inversion & $\frac{1}{j2\pi}\int_{c-j\infty}^{c+j\infty} \exp(st)F(s)ds$ & $F(s)$ \\ [0.3ex]
Linearity & $\alpha f(t) + \beta g(t)$ & $\alpha F(s) + \beta G(s)$ \\ [0.3ex]
Scaling & $f(\alpha t)$ & $\frac{1}{\alpha}F\left(\frac{s}{\alpha}\right)$ \\ [0.3ex]
Frequency shifting & $\exp(\alpha t)f(t)$ & $F(s-\alpha)$ \\ [0.3ex]
Time shifting & $f(t-\alpha) H(t-\alpha)$ & $\exp(-\alpha s)F(s)$ \\ [0.3ex]
First derivative & $f'(t)$ & $sF(s) - f(0)$ \\ [0.3ex]
nth derivative & $f^{(n)}(t)$ & $s^nF(s)-s^{n-1}f(0)-s^{n-2}f'(0)$ \\
      & & $-\dots-s f^{(n-2)}(0) - f^{(n-1)}(0)$  \\ [0.3ex]
Transform derivative & $t^n f(t)$ & $(-1)^n \frac{d^n F(s)}{ds^n}$ \\ [0.3ex]        
Integration & $\int_0^t f(\tau)d\tau$ & $\frac{1}{s}F(s)$  \\ [0.3ex]
\hline
\end{tabular}
\label{table:properties} % is used to refer this table in the text
\end{table}

%----- Table -------------------------------------------------------%
\begin{table}[ht]
\caption{Function transforms} % title of Table
\centering % used for centering table
\begin{tabular}{|c|c|c|c|} 
\hline %inserts double horizontal lines
Name & $f(t), t\ge0$ & $F(s)$ & Convergence region \\ [0.3ex] % inserts table
%heading
\hline % inserts single horizontal line
Unit impulse & $\delta(t)$ & $1$ & all $s$ \\ [0.3ex]
Ideal delay& $\delta(t-\alpha)$ & $\exp(-\alpha s)$ & $\Re(s)\ge\alpha$ \\ [0.3ex]
Unit step & $H(t)$ & $\frac{1}{s}$ & $\Re(s)>0$ \\ [0.3ex]
Ramp  & $t$ & $\frac{1}{s^2}$ & $\Re(s)>0$ \\ [0.3ex]
nth power  & $t^n, n=1,2,\dots$ & $\frac{n!}{s^{n+1}}$ & $\Re(s)>0$ \\ [0.3ex]
Sine & $\sin(\alpha t)$ & $\frac{\alpha}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
Cosine & $\cos(\alpha t)$ & $\frac{s}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
Hyperbolic sine & $\sinh(\alpha t)$ & $\frac{\alpha}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
Hyperbolic cosine & $\cosh(\alpha t)$ & $\frac{s}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
Exponential decay & $\exp(-\alpha t)$ & $\frac{1}{s + \alpha}$ & $\Re(s)>-\alpha$ \\ [0.3ex]
\hline %inserts single line
\end{tabular}
\label{table:functions}
\end{table}
\end{document}
