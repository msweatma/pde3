\documentclass[12pt]{amsart}
\usepackage[a4paper,top=2cm,left=2cm,right=2cm,bottom=2cm]{geometry}
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{color, colortbl}
\usepackage{bm}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{epstopdf}
\usepackage{framed}
\usepackage{tabularx}
\usepackage{color, colortbl}
\usepackage[yyyymmdd]{datetime}

\definecolor{Gray}{gray}{0.9}

\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\renewcommand{\vec}[1]{\mathbf{#1}}

\usepackage{pgfplots}
\pgfplotsset{}
\usepgfplotslibrary{groupplots}
%\newcommand\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;} 
\newcommand*\circled[1]{\tikz[baseline=(char.base)]{
            \node[shape=circle,draw,inner sep=2pt] (char) {#1};}}

\newcommand{\emptybox}[1]{\boxed{\phantom{#1}}}
% uncomment the following line to produce the solutions
%\renewcommand{\emptybox}[1]{\boxed{#1}}

\title{Partial Differential Equations 3 (SCEE09004)}
\author{Laying out of solutions}
\date{\today}

\begin{document}
\maketitle
The proper laying out of mathematical arguments and solutions to problems is critical to ensuring
\begin{enumerate}
\item You can follow your own working later and check to ensure there are no mistakes
\item Others can follow your calculations and argument
\end{enumerate}
Presentation is very important in mathematics, just as it is in other fields. The main reason for this is clarity. 
Good presentation allows you to communicate more clearly the content of your work.
A secondary reason is the appearance of competence. Careful presentation makes the reader think you were also careful with the content of your work. Although good presentation is rarely successful at bluffing past poor content, poor presentation can easily ruin good content.


\section{Flow}
Written mathematics must be readable. This may seem trivial, but it is an important point. You
should be able to read your work aloud to a classmate and have them understand your solution. If
you need to add any explanations, these should be included in your written work. The most common
mistake is to write mathematics without using enough words. All writing, even mathematics, should
consist of complete sentences. These should explain the problem by providing both the method and
justification for each step of the solution.

Although sometimes it seems hard to read textbooks, it would be much harder to understand if
they only had equations and no sentences. The situation is similar in a lecture: if your lecturers just
list formulas on the data projector without talking about them, leaving you to figure out what was
being done in each step, how much could you understand from the lecture? Neither of these would
be a good way for most of you to learn, since sentences are necessary to explain the mathematics.

You should explain coursework solutions using complete sentences. In other words you must link together the thoughts underlying the solution with words and embedding equations into sentences. Going through
the extra work to do this will benefit you in several ways:
\begin{itemize}
\item Writing down your thoughts and organising them into complete sentences will help you to
understand the method of solution better.
\item When you look back on homework to study for a test, or later on in another class, you will
understand what you were doing on each problem and the mathematics behind it.
\item Other people (teacher, classmates, tutor,...) will understand what you are doing at each step,
and why you are doing it. This way, you won’t lose points for skipping steps or solving the
problem in an unusual way.
\item Communicating your work will be essential in whatever field you choose. Even though the
fields are stereotypically weak on writing, engineers and scientists spend a surprising amount
of time writing reports and giving oral presentations.
\end{itemize}

It is customary to write mathematics using ``we'' instead of ``I'' and ``you''. Think of yourself as the
tour guide, showing the sights to your reader. You and the reader together make ``we''.

Finally keep your audience in mind, in preparing solutions for PDE3 you should imagine that you are writing for a classmate who has not seen the solution before.  Write with enough detail so that they can follow
your explanations. A good way to test if you have written enough is to read your work aloud to
another person. If you need to add any words to make it make sense, then these words should be
included in your written work.

\section{ ``$=$''}
The equality symbol, $=$, is often misused. It is important to remember that $=$ means that both sides of the equation are the same, but written differently.  Students often write $=$ to show they have done something, for example:

\begin{framed}
\centerline{\Large Bad}
Calculate $(3+5)/2$

\[3+5=8/2=4.\]
\end{framed}

This claims that $3+5=4$ which is clearly false.  The correct way of writing this is

\begin{framed}
\centerline{\Large Good}
Calculate $(3+5)/2$
\[\frac{3+5}{2}=\frac{8}{2}=4.\]
\end{framed}

An alternative would be to use implies, $\implies$,

\begin{framed}
\centerline{\Large Good}
Calculate $(3+5)/2$
\[3+5=8\ \implies\ \frac{3+5}{2}=4.\]
\end{framed}

Sometimes the problem is not stated and the solution states,

\begin{framed}
\centerline{\Large Bad}
\[ = \sin(2x),\]
\end{framed}

rising the question \textsl{equal to what?}.

\newpage
\section{Symbols}
\begin{framed}
\centerline{\Large Bad}
\[\lim_{x\rightarrow x_0}f(x)=L\implies \forall \epsilon>0\ \exists\delta>0\mbox{ s.t. } \forall x\]
\[0<\left|x-x_0\right|<\delta\implies \left|f(x)-L\right|<\epsilon.\] 
\end{framed}

Although this is mathematically correct, it's almost impossible to read. So whilst symbols are useful very compact notation can be challenging to say the least.  It would be better to write:

\begin{framed}
\centerline{\Large Good}
Let $f(x)$ be a function defined on an open interval about the point, $x_0$, noting that $f(x_0)$ may be undefined.  We define the limit, $L$, as 
\[L=\lim_{x\rightarrow x_0}f(x).\]
The limit, $L$ is said to exist if $\forall \epsilon>0$, there exists a corresponding number, $\delta$ such that $\forall x$ 
\[0<\left|x-x_0\right|<\delta\implies \left|f(x)-L\right|<\epsilon.\] 
\end{framed}

\section{Logical connectives}
Mathematics  has  its  own language.    As with  any  language,  effective communication  depends
on logically connecting  components.   Even  the simplest  ``real''  mathematical  problems  require  at least  a small  amount  of reasoning,  so it  is very  important  that you  develop  a feeling for \emph{formal (mathematical) logic.}

Consider,  for example,  the two  sentences  
\begin{quote}There  are 10 people waiting  for the bus\end{quote}  and  \begin{quote}The bus  is late.\end{quote}
What, if anything,  is the logical connection  between  these  two  sentences?   Does one logically imply the other?  
Similarly,  the two mathematical statements \[r^2 + r - 2 = 0\] \[r = 1\mbox{ or }r = -2\] need to be connected,  otherwise they are merely two random  statements that convey no useful information.  Warning:  when mathematicians talk about implication, it means that one thing \textbf{must} be true as a consequence of another; not that it can be true, or might be true sometimes.

Words  and  symbols  that tie  statements  together  logically are  called  \emph{logical connectives}.   They allow you to communicate  the reasoning  that has  led you to your  conclusion.   Possibly  the most important of these is \emph{implication} --- the idea that the next statement is a logical consequence of the previous  one.  This  concept  can be conveyed by the use of words such as:  therefore, hence, and  so, thus, since, if . . .  then . . . , this  implies,  etc.   In the middle  of mathematical  calculations,  we can represent these by the implication symbol ($\implies$). For example
\begin{equation}
x+7y^2=3\implies y=\pm\sqrt{\frac{3-x}{7}};\mbox {or}
\label{e:roots}
\end{equation}
\begin{equation}
x\in(0,\infty)\implies\cos x\in[-1,1].
\label{e:cosine}
\end{equation}

\subsection{Converse}
Note that ``statement A $\implies$ statement B'' does not necessarily mean that the logical converse ---
``statement B $\implies$ statement A'' --- is also true. Logical implication is a matter of cause and effect;
the logical converse is simply the reverse cause-effect situation (which may not be true). Consider
the following, everyday example:
\begin{quote}
I am running to class because I am late
\end{quote}
It should be clear that the logical converse of these is not true:
\begin{quote}
I am late to class because I am running.
\end{quote}
For examples (\ref{e:roots}) and (\ref{e:cosine}) above:
\[x+7y^2=3\Leftarrow y=\pm\sqrt{\frac{3-x}{7}}\]
but
\[ x\in(0,\infty) \not\Leftarrow \cos x\in[-1,1] \mbox{ since for }x<0\  \cos x \in[-1,1]\mbox{ as well.}\] 

\subsection{Contrapositive}
We have seen that simply reversing a logical statement can lead to problems. There is a way,
however, to invert an implication so that the inverted statement is also true. This is known as the
\emph{contrapositive}. Consider the statement ``A $\implies$ B''; this means ``if A is true, then B is true''. The
contrapositive is ``if B is false, then A must be false''.

For example
\begin{itemize}
\item if $X$ is a badger, it is an animal. \begin{framed}A true assertion\end{framed}
\item if $X$ is an animal, it is a badger.  \begin{framed}FALSE (converse)\end{framed}
\item if $X$ is not an animal, it is not a badger.  \begin{framed}TRUE (contrapositive)\end{framed}
\item if $X$ is not a badger, it is not an animal.  \begin{framed}FALSE. This is the contrapositive of the second statement\end{framed}
\end{itemize}

\subsection{Equivalence}
When the implication works both ways, we say that the two statements are equivalent and we may
use the equivalence symbol ($\Leftrightarrow$); in words we may say \emph{A is equivalent to B} or \emph{A if and only if B}. If two statements are equivalent, we may use any of the implication symbols ($Leftarrow$, $Rightarrow$, or $\Leftrightarrow$). Which connective we use depends on what we are trying to show. In (\ref{e:roots}) we are trying to find an equation for $y$, so we would normally just use $Leftarrow$ even though $\Leftrightarrow$ is a stronger statement and also true.  If, however, we wanted to show that the two statements about $x$ and $y$ were equivalent we would write
\[x+7y^2=3\Leftrightarrow y=\pm\sqrt{\frac{3-x}{7}}.\]
\section{Annotation}
Some of the steps in a logical deduction or argument can be omitted if they are well know or standard results, in this case it is important to annotate your work so the reader can follow the steps.

Writing statements like
\begin{itemize}
\item dividing both sides by $x^2$,
\item factorising,
\item integrating with respect to $x$,
\item differentiating w.r.t $t$,
\item using the chain rule, and 
\item by the double angle formula, $\sin(2x)=2\sin x\cos x,$ we have, etc.
\end{itemize}
can be very useful to explain the steps to the reader.

\section{Marking}
\begin{itemize}
\item The coursework will be marked on Learn.
\item Positive marking will be used with each mark earned.
\item The coursework will be marked with \textit{quick comments} such as \textcolor{red}{Good}, \textcolor{red}{Correct} and \textcolor{red}{Wrong}.
\item These \textit{quick comments} will be supplemented with a short comment explaining the error, e.g. \textcolor{red}{\emph{x should be  -ve}} should be present.
\item If a solution is completely wrong then you will see \textcolor{red}{\emph{nothing useful here}} or  \textcolor{red}{\emph{wrong approach}}, written. 
\item Where a small error has been made and the marks deducted the error should be carried forward into the rest of the solution. The marker will check the correct method has been used and marks will still be awarded for method. This will be indicated by annotation \textcolor{red}{e.c.f.} or \textcolor{red}{carried forward}.  
\item If the error is catastrophic making the rest of the solution meaningless no further marks will be awarded, for that part of the question. A comment will be made to this effect.
\item If the solution is divided into parts the total mark for the part at the end of that section again using the out-of format, e.g. \textcolor{red}{2/4}.
\item At the end of the question the total mark will be recorded again using the out-of format, e.g. \textcolor{red}{68/100}.
\end{itemize}
\subsection{Style}
Coursework questions  also have marks awarded for style of presentation. Your work will be graded in terms of clarity and layout as expressed by
\begin{enumerate}
\item Flow --- This category assesses the logical progression of the presented solution.  An excellent solution should be easy to follow and presented in a clear and logical manner. A poor one will be chaotic and badly organised.
\item Annotation ---  This category assess the commentary provided by the student to explain the work.  Excellent solutions present equations as part of complete grammatical sentences (including punctuation in the equations) and contain annotation that allows the reader to easily follow the solution, symbols used are properly definined.  Poor solutions are unannotated.
\end{enumerate}
The script should contain brief comments on the solution to indicate good or bad flow, use of symbols or annotation. For example \textcolor{red}{\emph{Acronym not defined}},  \textcolor{red}{\emph{Equation should be numbered}}, \textcolor{red}{\emph{You have assumed and not stated $A=1$}}, and \textcolor{red}{\emph{Use of the product rule not stated}}.

% ------------------------------- %
% Flow and annotation rubric
\subsection{Rubric}

The following rubric will be used as a guideline for the marking of flow and annotation of your solution. 

\begin{tabularx}{\columnwidth}{cX} 
\hline Grade & Flow and annotation  \\ \hline \hline
F & The solution is incomplete or incomprehensible, and the solution is incompletely annotated. \\

\rowcolor{Gray}
E & The solution makes inconsistent or illogical leaps between steps and/or the annotation is inconsistent or incorrect which fails to help the reader follow the solution. \\

D & Complete solution which fails to demonstrate logical connectivity between the stages. Equations do not form part of a grammatical sentences. And/or: Minor errors in use of symbols and symbols introduced without explanation.  \\

\rowcolor{Gray}
C & Complete solution (possibly containing minor errors), with equations forming part of sentences,  which demonstrates limited logical connectivity between the stages. And/or: The solution is partially annotated with a commentary that describes the solution method. \\

B & A clear, complete, logical solution is presented, demonstrating a firm understanding of the topic assessed, and the solution makes correct use of symbols and is completely annotated. \\

\rowcolor{Gray}
A3 & Solution is of a professional standard with all steps explained in a clear consistent manner demonstrating logical, ordered thought.  The solution is completely annotated with a professional commentary that assists the reader in following the solution method and shows critical awareness of the methods used \\

A2 & In addition to A3 the solution makes use of more advanced techniques  (beyond those taught) which provide a mathematically elegant solution.\\

\rowcolor{Gray}
A1 &  An outstanding solution  (in addition to A3 and A2) typical of those found in the best publications and text books. \\

\hline
\end{tabularx}

\end{document}  