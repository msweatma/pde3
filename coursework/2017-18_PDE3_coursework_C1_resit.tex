 % Add 'answers' to options to get the solutions
\documentclass[answers,addpoints,12pt]{exam}	

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Resit Coursework 1}
\footer{}{Page \thepage\ of \numpages}{\iflastpage{\textbf{END}}{\textbf{Continued}}}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_coursework_style}

%-------------------------------------------------------%
%-------------------------------------------------------%
\begin{document}

The following resit coursework is to be handed in to the ETO online dropbox by \textbf{15:00 on Friday the 3$^{rd}$ of August 2018}. This is \textbf{resit coursework number 1 of 2}. Each coursework is worth $20\%$ of the final mark.
\vskip0.1cm
% ---- Modify
You should submit the solution to this coursework in the form of a short report on Learn. This report must not be longer than 7 pages with at least 2cm margins and a font size of 11pt. The marks for this coursework will be awarded for the correctness of the solution as well as for the clarity and logical progression of the solution. Details on laying out mathematical calculations and general guidelines for the coursework are given on Learn. 

\vskip0.1cm

%----- Question ----------------------------------------%
% This is the start of the questions
\begin{questions}
\question 

Consider the Laplace equation on an annulus with inner and outer radii $r=1$ and $r=3$ as shown in the following figure.

\centerline{\includegraphics[width=10cm]{C1_annulus_resit.png}}

The inside boundary of the annulus is held at $u(1,\theta) = 0$ ($0\le\theta<2\pi$) while the outer boundary is given by
\[
\pderiv{u}{r}(3, \theta) = \begin{cases}
1, \quad & 0\le\theta<\pi \\
0, \quad & \pi\le\theta<2\pi
\end{cases}
\]

\begin{parts}
%-------------------------------------------------------%
\part[40] Calculate the analytical solution of the Laplace equation on the annulus with the given boundary conditions.

\begin{solution}
The Laplace equation in cyclindrical coordinates is given by
\begin{align*}
\pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
and we are looking for separated solutions of the form $u(r,\theta) = R(r) \Theta(\theta)$ which give us two ordinary differential equations
\begin{align*}
\frac{d^2 R}{dr^2} \Theta + \frac{1}{r} \frac{dR}{dr}\Theta + \frac{1}{r^2}\frac{d^2\Theta}{d\theta^2} &= 0 \\
-\frac{\Theta''}{\Theta} &= \frac{R'' + \frac{R'}{r}}{\frac{R}{r^2}} = \lambda
\end{align*}
The solutions of these two equations will depend on the value of $\lambda$ and we need to consider three cases. \hfill[2]

For $\lambda=0$ we get the two solutions
\begin{align*}
\Theta(\theta) &= a\theta + b \\
R(r) &= C\ln(r) + D
\end{align*}
with parameters $a$, $b$, $C$ and $D$. The solutions can be combined to a solution for the original problem \hfill[2]
\[
u_0(r, \theta) = (a\theta + b)(C\ln(r) + D)
\]
We know that the solution needs to be periodic in $2\pi$ and this is only the case for $a=0$. \hfill[2]

Let us know look at the boundary conditions. At the inner boundary we need to have $u_0(1,\theta)=0$ so that
\begin{align*}
u_0(1, \theta) &= C \ln(1) + D \overset{!}{=} 0 
\end{align*}
and from this it follows that $D=0$. At the outer boundary we have the Neumann condition and we leave this until we have build the complete solution. Thus we are left with
\begin{align*}
u_0(r,\theta) = k \ln(r)
\end{align*}
where $k=bC$ is a constant which needs to be determined. \hfill[2]

For $\lambda<0$ we get the two individual and the combined solution
\begin{align*}
\Theta(\theta) &= A\cosh(\mu\theta) + B\sinh(\mu\theta) \\
R(r) &= C r^{-\mu} + D r^{\mu} \\
u_-(r,\theta) &= \left(A\cosh(\mu\theta) + B\sinh(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
with parameters $A$, $B$, $C$ and $D$, and where we set $\lambda = -\mu^2$. Again, this needs to be periodic in $2\pi$ which is only fulfilled for $A=B=0$. Thus the solution for $\lambda<0$ is zero. \hfill[2]

For $\lambda = \mu^2 > 0$ we get the two individual and the combined solution\hfill[2]
\begin{align*}
\Theta(\theta) &= A\cos(\mu\theta) + B\sin(\mu\theta) \\
R(r) &= C r^{-\mu} + D r^{\mu} \\
u_+(r,\theta) &= \left(A\cos(\mu\theta) + B\sin(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
with parameters $A$, $B$, $C$ and $D$. Again this needs to be periodic in $2\pi$ and this is the case only for $\mu=n$ with $n\in\mathbb{N}$.\hfill[2]

To fulfil the inner boundary condition it is necessary to have
\[
u_+(1, \theta) = \left(A\cos(n\theta) + B\sin(n\theta)\right) \left(C 1^{-n} + D 1^{n}\right) \overset{!}{=} 0, \quad 0\le\theta\le2\pi
\]
Since the first part is not zero for all $\theta$, it follows that $C=-D$. Thus the solution is 
\[
u_+(r, \theta) = \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) \left(r^{n} - r^{-n}\right) 
\]
where we have set $a_n = AD$ and $b_n = BD$. \hfill[2]

To fulfil the outer boundary condition we need to superimpose the solutions for $\lambda=0$ and $\lambda>0$. This is possible because we are dealing with a linear PDE. The complete solution is \hfill[2]
\[
u(r, \theta) = k\log(r) + \sum_{n=1}^\infty \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) \left(r^{n} - r^{-n}\right) 
\]

To find the coefficients $a_n$ and $b_n$ we need to calculate the derivative of $u$ with respect to $r$ and set it equal to the boundary condition. The derivative is given by
\[
\pderiv{u}{r}(r, \theta) = \frac{k}{r} + \sum_{n=1}^\infty \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) \left(nr^{n-1} + nr^{-n-1}\right) 
\]
and at the outer boundary we need to have \hfill[2]
\begin{align*}
\pderiv{u}{r}(2, \theta) &= \frac{k}{2} + \sum_{n=1}^\infty \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) n \left(2^{n-1} + 2^{-n-1}\right) \\
 &\overset{!}{=} \begin{cases}
-1, \quad & 0\le\theta<\pi \\
0, \quad & \pi\le\theta<2\pi
\end{cases}
\end{align*}

The parameters $k$, $a_n$ and $b_n$ can be found with the usual method of the Fourier series.
The parameter $k$ is given by \hfill[2]
\begin{align*}
k &= \frac{2}{2\pi} \int_{0}^{2\pi} \pderiv{u}{r}(2, \theta) d\theta \\
  &= -\int_{0}^{\pi} d\theta = -1
\end{align*}
the parameter $a_n$ is given by \hfill[2]
\begin{align*}
a_n &= \frac{1}{n \left(2^{n-1} + 2^{-n-1}\right)} \frac{2}{2\pi} \int_{0}^{2\pi} \pderiv{u}{r}(2, \theta) \cos(n\theta) d\theta \\
 &= \frac{-1}{n\pi \left(2^{n-1} + 2^{-n-1}\right)} \int_{0}^{\pi} \cos(n\theta) d\theta = 0
\end{align*}
and $b_n$ is given by \hfill[2]
\begin{align*}
b_n &= \frac{1}{n \left(2^{n-1} + 2^{-n-1}\right)} \frac{2}{2\pi} \int_{0}^{2\pi} \pderiv{u}{r}(2, \theta) \sin(n\theta) d\theta \\
 &= \frac{-1}{n\pi \left(2^{n-1} + 2^{-n-1}\right)} \int_{0}^{\pi} \sin(n\theta) d\theta \\
 &= \begin{cases}
 \frac{-2}{n^2\pi \left(2^{n-1} + 2^{-n-1}\right)}, \quad & n \text{ odd} \\
 0, \quad & n \text{ even} 
 \end{cases} 
\end{align*}

Thus the complete solution is \hfill[2]
\begin{align*}
u(r,\theta) &= -\log(r) - \sum_{i=1}^\infty \frac{2}{(2i-1)^2\pi \left(2^{2i-2} + 2^{-2i}\right)} \sin((2i-1)\theta) \left(r^{2i-1} - r^{-2i+1}\right) 
\end{align*}
which can be plotted as: \hfill[4]

\includegraphics[width=0.5\columnwidth]{C1_analytical_contour.png}
\includegraphics[width=0.5\columnwidth]{C1_analytical_solution.png}

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[8]
\end{solution}

%-------------------------------------------------------%
\part[35] Develop a script in Matlab to solve the problem numerically. Explain what you are doing, compare the solution against the analytical solution and perform a grid convergence analysis. Plot the solution.

\vskip0.2cm
\textbf{Remarks:}
\begin{itemize}
\item While the Matlab scripts on Learn can be used as a starting point, you need to add some modifications
\item It will be beneficial to work in cylindrical coordinates
\end{itemize}

\begin{solution}
The Laplace equation in cylindrical coordinates is given by \hfill[2]
\begin{align*}
\pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
To solve the PDE with Matlab we need to discretise the equation. The derivatives can be discretised with the finite difference formulas for first and second derivatives from the lectures. We define the grid spacings
\begin{align*}
r_j &= 1 + (j-1)\Delta r, \qquad & j=1,2,\dots,n_r \\
\theta_i &= (j-1)\Delta \theta, \qquad & i=1,2,\dots,n_\theta 
\end{align*}
where $\Delta r$ and $\Delta \theta$ are the grid spacing in the $r$ and $\theta$ direction, respectively. At the grid point $(i,j)$ the radius and angle are $r_j$ and $\theta_i$, respectively, and we define $u(\theta_i, r_j) = u_{i,j}$. With the finite difference formulas we get \hfill[2]
\begin{align*}
\frac{u_{i,j+1} - 2u_{i,j} + u_{i,j-1}}{(\Delta r)^2} + \frac{1}{r_j} \frac{u_{i,j+1} - u_{i,j}}{\Delta r} + \frac{1}{r_j^2} \frac{u_{i+1,j} - 2u_{i,j} + u_{i-1,j}}{(\Delta \theta)^2} = 0
\end{align*}

Multiply this with $(\Delta r)^2$ and rearrange for $u_{i,j}$ similar to the case in Cartesian coordinates to get
\begin{align*}
u_{i,j} \left(2 + \frac{\Delta r}{r_j} + 2\frac{(\Delta r)^2}{(\Delta\theta)^2 r_j^2}\right) =& u_{i,j+1}\left(1 + \frac{\Delta r}{r_j}\right) + u_{i,j-1} \\
    &+ \frac{(\Delta r)^2}{(\Delta\theta)^2}\frac{1}{r_j^2} (u_{i+1,j} + u_{i-1,j}) 
\end{align*}
Multiply both sides of the equation with the factor
\[
Ch = \frac{1}{\left(2 + \frac{\Delta r}{r_j} + 2\frac{(\Delta r)^2}{(\Delta\theta)^2 r_j^2}\right)}
\]
and bring it into the form required for the SOR method \hfill[2]
\begin{align}
u_{i,j}^{k+1}  = (1-\omega)u_{i,j}^{k} &+ \omega Ch \left(u_{i,j+1}^{k}\left(1 + \frac{\Delta r}{r_j}\right) + u_{i,j-1}^{k}\right. \nonumber\\
    & \quad\qquad\quad\left.+ \beta^2\frac{1}{r_j^2} (u_{i+1,j}^{k} + u_{i-1,j}^{k}) \right)
\label{eq:uij}
\end{align}
where we have used $\beta = \frac{\Delta r}{\Delta\theta}$ and with $\omega$ the relaxation factor. The superscript $k$ indicates the iteration number of the iterative SOR method.

The Eq.~\ref{eq:uij} for $u_{i,j}^{k+1}$ is added into the SOR method for all internal values of $r$, i.e. all values of $r$ except $r=1$ and $r=2$, and all value of $\theta$. For the values $\theta=0$ and $\theta=2\pi$ we need to adjust the formula slightly so that the neighbouring angles are used, i.e. for $\theta=0$ the neighbouring angles are $\theta_+=\Delta \theta$ and $\theta_- = 2\pi-\Delta\theta$. \hfill[2]

At the inside boundary, i.e. for $r=1$, the Dirichlet boundary condition is given by \hfill[2]
\[
u_{i,1} = 0, \qquad i=1,2,\dots, n_\theta
\]
At the outside boundary the Neumann boundary condition is discretised with the backward difference formula and rearrange to \hfill[2]
\[
u_{i,n_r} = u_{i,n_r-1} - 0.5(1 + \textit{sign}(\pi-\theta_i)) \Delta r, \qquad i=1,2,\dots, n_\theta
\]
where $\textit{sign}(x)$ returns the sign of $x$.

We run the SOR method with a tolerance of $10^{-6}$. This gave us a maximum difference of $\sim4\times10^{-5}$ to a run with a much tighter tolerance which was deemed to be sufficient. \hfill[2]

The following plots show the contour and surface plots for the numerical solution on a 40 by 40 grid.\hfill[4]

\includegraphics[width=0.5\columnwidth]{C1_numerical_contour.png}
\includegraphics[width=0.5\columnwidth]{C1_numerical_solution.png}

The plots look very similar to the analytical solutions in part (a) which is confirmed by the contour and surface plots of the difference between the numerical and analytical solution for a 40 by 40 grid and 20 terms in the analytical solution. These plots show that the maximum difference is about $0.01$. \hfill[4]

\includegraphics[width=0.5\columnwidth]{C1_difference_contour.png}
\includegraphics[width=0.5\columnwidth]{C1_difference_solution.png}

The Matlab script RefinementAnalysis.m from the numerical lectures is used to perform the grid convergence analysis. We use the same function value as for the Cartesian grid. While this is an approximation of the integral of the solution over the domain in Cartesian coordinates, in cylindrical coordinates it will overestimate the influence of the solution values on the inside of the annulus. \hfill[2]

The script is run for four grid sizes, i.e. 10x10, 20x20, 40x40 and 80x80. \hfill[2]

The order of convergence is given as $p=0.9$. This is smaller than one which is expected because of the first order approximation of the first derivative in $r$. \hfill[2]

The grid convergence ratio is $1.01$ which is close to $1$ on the three finest grids which indicates that the solution is grid independent for a 40x40 grid.\hfill[2]

\textbf{Remark:} Different values for the order of convergence and grid convergence can be reached for different spatial discretisations of the first spatial derivative and the Neumann boundary condition.

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[5]
\end{solution}

%-------------------------------------------------------%
\part[25] The physical system is modified by a source term in the lower half of the annulus. Over the entire annulus the source term is given by
\[
f(r, \theta) = \begin{cases}
0, \quad & 0\le\theta<\pi \\
-(r-1)\sin(\theta), \quad & \pi\le\theta<2\pi
\end{cases}
\]
Add the source term as an inhomogeneity to the Laplace equation and develop a Matlab script to solve the inhomogeneous problem numerically.

Explain what you are doing, compare the solution against the solution of the homogeneous system and perform a grid convergence analysis. Plot the solution.

\begin{solution}
In the derivation of the numerical formula in part (b) we need to replace the $0$ on the right hand side with the inhomogeneity. With the definition $f(\theta_i, r_j) = f_{i,j}$ and the results from part (b) we get \hfill[2]
\begin{align*}
\frac{u_{i,j+1} - 2u_{i,j} + u_{i,j-1}}{\Delta r^2} + \frac{1}{r_j} \frac{u_{i,j+1} - u_{i,j}}{\Delta r} + \frac{1}{r_j^2} \frac{u_{i+1,j} - 2u_{i,j} + u_{i-1,j}}{\Delta \theta^2} = f_{i,j}
\end{align*}

Multiply this with $(\Delta r)^2$ and rearrange for $u_{i,j}$ similar to the case in part (b) to get 
\begin{align*}
u_{i,j} \left(2 + \frac{\Delta r}{r_j} + 2\frac{(\Delta r)^2}{(\Delta\theta)^2 r_j^2}\right) =& u_{i,j+1}\left(1 + \frac{\Delta r}{r_j}\right) + u_{i,j-1} \\
    &+ \frac{(\Delta r)^2}{(\Delta\theta)^2}\frac{1}{r_j^2} (u_{i+1,j} + u_{i-1,j}) \\
    &- (\Delta r)^2 f_{i,j}
\end{align*}
Multiply both sides of the equation with the factor
\[
Ch = \frac{1}{\left(2 + \frac{\Delta r}{r_j} + 2\frac{(\Delta r)^2}{(\Delta\theta)^2 r_j^2}\right)}
\]
and bring it into the form required for the SOR method \hfill[2]
\begin{align}
u_{i,j}^{k+1}  = (1-\omega)u_{i,j}^{k} &+ \omega Ch \left(u_{i,j+1}^{k}\left(1 + \frac{\Delta r}{r_j}\right) + u_{i,j-1}^{k}\right. \nonumber\\
    & \quad\qquad\quad\left.+ \beta^2\frac{1}{r_j^2} (u_{i+1,j}^{k} + u_{i-1,j}^{k}) \right. \nonumber\\
    &\quad\qquad\quad\left. - (\Delta r)^2 f_{i,j} \vphantom{\frac12}\right) \nonumber
\label{eq:uij}
\end{align}
The boundary conditions of the problem are the same as for part (b) so the only changes in the Matlab script are required for the internal values of $r$, i.e. $1<r<2$.\hfill[2]

For these values we need to add the following term to the right hand side of the update equation for $u_{i,j}^{k+1}$ \hfill[2]
\[
(\Delta r)^2 \omega Ch (r_j - 1) \sin(\theta_j) (1 - \textit{sign}(\pi - \theta_j))0.5
\]
where $dr$ is the grid spacing in the $r$ direction, $\omega$ the relaxation factor and $\textit{sign}(x)$ returns the sign of $x$. Alternative formulations with the Heaviside unit step function are possible.

We run the SOR method with the same tolerance of $10^{-6}$ as used in part (b). This was deemed to be sufficient. \hfill[2]

The following plots show the contour and surface plots for the numerical solution on a 40 by 40 grid.\hfill[4]

\includegraphics[width=0.5\columnwidth]{C1_inhomogeneous_contour.png}
\includegraphics[width=0.5\columnwidth]{C1_inhomogeneous_solution.png}

In comparison to the homogeneous system we have a positive forcing term for negative values of $y$ or, in other words, for $\pi\le\theta<2\pi$. This term leads to a reduced solution value for negative values of $y$ but also a small change for positive $y$ in comparison to the homogeneous solution as seen in the plots. \hfill[4]

\includegraphics[width=0.5\columnwidth]{C1_difference_inhomogeneous_contour}
\includegraphics[width=0.5\columnwidth]{C1_difference_inhomogeneous}

The grid convergence analysis gives an order of convergence of $p=0.78$ which is smaller than for the homogeneous case. The grid convergence ratio is $1.01$ which is close to $1$ on the three finest grids which indicates that the solution is grid independent for a 40x40 grid.\hfill[2]

\textbf{Remark:} Different values for the order of convergence and grid convergence can be reached for different spatial discretisations of the first spatial derivative and the Neumann boundary condition.

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[5]
\end{solution}

\end{parts}


\end{questions}
\end{document}
