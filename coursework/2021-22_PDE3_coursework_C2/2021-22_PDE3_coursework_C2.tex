 % Add 'answers' to options to get the solutions
\documentclass[answers,addpoints,12pt]{exam}	

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Coursework 2}
\footer{}{Page \thepage\ of \numpages}{\iflastpage{\textbf{END}}{\textbf{Continued}}}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_coursework_style}
\usepackage[ruled,vlined]{algorithm2e}
\usepackage{minted}
\usepackage{amssymb}

\renewcommand{\theequation}{Q\arabic{question}.\arabic{equation}}

\pointsdroppedatright 

%-------------------------------------------------------%
\begin{document}

The following coursework is to be handed in to the online dropbox by \textbf{16:00 on Thursday the 24$^{th}$ of March 2022}. This is \textbf{coursework number 2 of 2}. Each coursework is worth $20\%$ of the final mark.
\vskip0.1cm
You should submit the solution to this coursework in the form of a short report on Learn. This report must be typed, with at least 2cm margins, a font size of 11pt and line spacing of at least 1, and must not be longer than 9 pages. The title page does not count towards the page limit and you don't have to repeat the question text.

The marks for this coursework will be awarded for the correctness of the solution as well as for the clarity and logical progression of the solution. Details on laying out mathematical calculations and general guidelines for the coursework are given on Learn. 

%----- Question ----------------------------------------%
% This is the start of the questions
\begin{questions}
\question[50] Consider the one-dimensional heat conduction equation
\begin{align}
\frac{\partial u(x,t)}{\partial t} &= \kappa \frac{\partial^2 u(x,t)}{\partial x^2} \label{eq:1D_PDE}\\
\intertext{on the interval $0\le x\le 0.5\pi$ for the initial condition}
u(x,0) &= 0.5 \cos(x), \quad 0\le x \le 0.5\pi, \\
\intertext{and the boundary conditions} 
u(0,t) &= g_0(t), \quad \forall t\ge0, \\
 u(0.5\pi, t) &= g_1(t), \quad \forall t\ge0. \label{eq:1D_right_boundary}
\end{align}

\begin{parts}
%-------------------------------------------------------%
\part[26] By using the Laplace transform method with respect to time $t$ derive the Laplace domain solution of the heat conduction problem \ref{eq:1D_PDE}--\ref{eq:1D_right_boundary}, i.e. find the solution $U(x,s)$ in the Laplace domain.

\droppoints
\begin{solution}
We apply the Laplace transform with respect to $t$ to the heat conduction equation to get \hfill[4]
\[
sU(x,s) - u(x,0) = \kappa \frac{\text{d}^2U(x,s)}{\text{d}x^2}
\]
where $U(x,s)$ is the Laplace transform of $u(x,t)$ with respect to $t$. We insert the initial condition into the transformed equation and rearranged it so that only the inhomogeneity is on the right hand side which gives us
\[
sU(x,s) - 0.5\cos(x) = \kappa \frac{\text{d}^2U(x,s)}{\text{d}x^2}
\]
This is rearranged so that only the inhomogeneity is on the right hand side
\begin{equation}
\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s}{\kappa}U = \frac{-0.5\cos(x)}{\kappa}
\label{eq:C2_transformed_PDE}
\end{equation}

This ODE can be solved by finding the characteristic polynomial \(m^2 - \frac{s}{\kappa} = 0\) which has roots $m_{1,2} = \pm\sqrt{\frac{s}{\kappa}}$. 
For these roots we get the complementary solution \hfill[4]
\[
U(x,s) = A(s)\sinh\left(x\sqrt{\frac{s}{\kappa}}\right) + B(s)\sinh\left((0.5\pi-x)\sqrt{\frac{s}{\kappa}}\right)
\]
where $A(s)$ and $B(s)$ are functions of the Laplace parameter $s$. 
Here we used the second equivalent solution from the remarks because it will make the handling of the boundary conditions easier.

Before we find the two, unknown functions, $A(s)$ and $B(s)$, we need to take care of the inhomogeneity of the differential equation. The right hand side is a cosine function thus a suitable trial solution for the particular integral for the inhomogeneous differential equation is \hfill[4]
\begin{align*}
U_p(x,s) &= P \sin(x) + Q \cos(x)
\end{align*}
where the coefficients $P$ and $Q$ need to be determined. We substitute $U_p$ and its second derivative with respect to $x$ into Eq.~\ref{eq:C2_transformed_PDE} and get 
\begin{align*}
-P \sin(x) - Q\cos(x) - \frac{s}{\kappa} \left( P\sin(x) + Q\cos(x)\right) &= -\frac{0.5 \cos(x)}{\kappa}
\end{align*}

We arrange this in terms of sine and cosine \hfill[4]
\[
\sin(x) \left(-P - \frac{s}{\kappa} P\right) + \cos(x) \left(-Q - \frac{s}{\kappa} Q + \frac{0.5}{\kappa}\right) = 0
\]
This gives us two equations which can be solved for $P$ and $Q$
\begin{align*}
0 &= -P\left(1 + \frac{s}{\kappa}\right) &\Rightarrow \qquad & P = 0 \\
0 &= -Q - \frac{s}{\kappa} Q + \frac{0.5}{\kappa} &\Rightarrow\qquad & Q=\frac{0.5}{s + \kappa}
\end{align*}
Thus, the general solution in the Laplace domain is 
\begin{equation}
U(x,s) = A(s)\sinh\left(x\sqrt{\frac{s}{\kappa}}\right) + B(s)\sinh\left((0.5\pi-x)\sqrt{\frac{s}{\kappa}}\right) + \frac{0.5}{s + \kappa}\cos(x)
\label{eq:C2_general_solution}
\end{equation}

We use the boundary conditions to find the two functions, $A(s)$ and $B(s)$. 
We define the Laplace transform of the two boundary conditions as $G_0(s)$ and $G_1(s)$. By setting the general solution~\ref{eq:C2_general_solution} to the boundary conditions we get \hfill[4]
\begin{align*}
U(0,s) =&  A(s)\sinh\left(0\sqrt{\frac{s}{\kappa}}\right) + B(s)\sinh\left((0.5\pi-0)\sqrt{\frac{s}{\kappa}}\right) + \frac{0.5}{s + \kappa}\cos(0) \overset{!}{=} G_0(s) \\
U\left(\frac{\pi}{2},s\right) =&  A(s)\sinh\left(\frac{\pi}{2}\sqrt{\frac{s}{\kappa}}\right) + B(s)\sinh\left(\left(\frac{\pi}{2}-\frac{\pi}{2}\right)\sqrt{\frac{s}{\kappa}})\right) + \frac{0.5}{s + \kappa}\cos\left(\frac{\pi}{2}\right) \overset{!}{=} G_1(s)
\end{align*}
Simplifying these equations we get expressions for $A(s)$ and $B(s)$ \hfill[4]
\begin{align*}
B(s)\sinh\left(0.5\pi\sqrt{\frac{s}{\kappa}}\right) &= G_0(s) - \frac{0.5}{s+\kappa} &\Rightarrow\qquad & 
B(s) = \frac{G_0(s) - \frac{0.5}{s+\kappa}}{\sinh\left(0.5\pi\sqrt{\frac{s}{\kappa}}\right)} \\
A(s)\sinh\left(0.5\pi\sqrt{\frac{s}{\kappa}}\right) &= G_1(s) &\Rightarrow\qquad & 
A(s) = \frac{G_1(s)}{\sinh\left(0.5\pi\sqrt{\frac{s}{\kappa}}\right)} 
\end{align*}

Thus, the Laplace domain solution for $0\le x \le 0.5\pi$ is \hfill[2]
\begin{align*}
U(x,s) =& \frac{G_1(s)}{\sinh\left(0.5\pi\sqrt{\frac{s}{\kappa}}\right)}\sinh\left(x\sqrt{\frac{s}{\kappa}}\right) + \frac{G_0(s) - \frac{0.5}{s+\kappa}}{\sinh\left(0.5\pi\sqrt{\frac{s}{\kappa}}\right)}\sinh\left((0.5\pi-x)\sqrt{\frac{s}{\kappa}}\right) \\
&\quad + \frac{0.5}{s + \kappa}\cos(x)
\end{align*}
\end{solution}

%-------------------------------------------------------%
\part[12] Now consider the specific boundary conditions
\begin{align*}
u(0,t) &= g_0(t) = 0.5\exp(-\kappa t), \quad &\forall t\ge0,\\
u(0.5\pi, t)&= g_1(t) = t, \quad &\forall t\ge0.
\end{align*}
and find the time domain solution. 

\droppoints
\begin{solution}
We apply the Laplace transform to the boundary conditions and with the Laplace transform tables we get \hfill[4]
\begin{align*}
G_0(s) &= \frac{0.5}{s + \kappa} \\
G_1(s) &= \frac{1}{s^2}
\end{align*}
Insert these into the Laplace domain solution to get
\begin{align*}
U(x,s) =& \frac{1}{s^2\sinh\left(0.5\pi\sqrt{\frac{s}{\kappa}}\right)}\sinh\left(x\sqrt{\frac{s}{\kappa}}\right) \\
 &+ \frac{\frac{0.5}{s + \kappa} - \frac{0.5}{s+\kappa}}{\sinh\left(0.5\pi\sqrt{\frac{s}{\kappa}}\right)}\sinh\left((0.5\pi-x)\sqrt{\frac{s}{\kappa}}\right) + \frac{0.5}{s + \kappa}\cos(x) \\
 =& \frac{1}{s^2\sinh\left(0.5\pi\sqrt{\frac{s}{\kappa}}\right)}\sinh\left(x\sqrt{\frac{s}{\kappa}}\right) + \frac{0.5}{s + \kappa}\cos(x) 
\end{align*}

We can bring this into the following form required for the inverse given in the remarks below \hfill[4]
\begin{align*}
U(x,s) =& \frac{\sinh\left(\frac{x}{\sqrt{\kappa}}\sqrt{s}\right)}{s^2\sinh\left(\frac{0.5\pi}{\sqrt{\kappa}} \sqrt{s}\right)} + \frac{0.5}{s + \kappa}\cos(x)
\end{align*}
with the parameters
\begin{align*}
a = \frac{0.5\pi}{\sqrt{\kappa}} & \quad\text{and} \quad 
b = \frac{x}{\sqrt{\kappa}} 
\end{align*}

Apply the given formula to get \hfill[4]
\begin{align*}
u(x,t) =& \frac{bt}{a} + \frac{2a^2}{\pi^3} \sum_{n=1}^\infty \frac{(-1)^n}{n^3}\left(1 - e^{-n^2\pi^2 t/a^2}\right) \sin\left(\frac{n\pi b}{a}\right) + 0.5\cos(x) e^{-\kappa t}
\end{align*}
We replace $a$ and $b$ to get
\begin{align*}
u(x,t) =& \frac{2xt}{\pi} + \frac{0.5}{\pi\kappa} \sum_{n=1}^\infty \frac{(-1)^n}{n^3}\left(1 - e^{-4n^2\kappa t}\right) \sin\left(2n x\right) + 0.5\cos(x) e^{-\kappa t}
\end{align*}
for $0\le x \le 0.5\pi$ and for $t\ge0$.
\end{solution}

%-------------------------------------------------------%
\part[12] Investigate the convergence of the series solution. Plot the solution for $\kappa=2$ and for a suitable time range.

\droppoints
\begin{solution}
Looking at the series solution we see that the magnitude of the terms of the series reduces with the third power of $n$. Thus, it is conceivable that only a small number of terms is required. To check the convergence we calculate the solution $u_N(x,t)$ for different number $N$ of terms of the series. We use these solutions to calculate the maximum difference
\[
E = \max(|u_{N_1} - u_{N_2}|)
\]
as well as the relative difference between the solutions for different numbers of terms 
\[
I = \int_0^{\pi/2} \int_0^2 \frac{(u_{N_1}(x,t) - u_{N_2}(x,t))^2}{u_{N_2}(x,t)^2} dt dx
\]
The relative difference is approximated as a double summation. 
For different numbers of terms and a 40 by 40 grid we get the following table of relative deviations. \hfill[Suitable metric and values: 4]

\begin{tabular}{c|c|c|c}
$N_1$ & $N_2$ & $E$ & $I$ \\
\hline
 5 & 10 & 1.426452e-03 & 4.690332e-03 \\
10 & 20 & 2.808873e-04 & 1.223675e-04 \\
20 & 40 & 6.251136e-05 & 3.614391e-06 \\
40 & 80 & 1.125423e-05 & 9.973780e-08 \\
80 & 160 & 1.433183e-06 & 1.556672e-09
\end{tabular}

We can see that between 5 and 10 terms the relative error change in the solution is less than $1\%$ and between 20 and 40 terms less than $0.001\%$. 
The maximum difference is on the same order for small number of terms but doesn't decrease as fast.
From this we recommend the use of 40 terms of the series to get a maximum error below $0.01\%$. 

\hfill[Discussion and choice of cut-off: 4]

The following plots show the contour and surface of the analytical solution for 40 terms of the infinite series on a 40 by 40 grid. 

\includegraphics[width=0.5\columnwidth]{CW2_PDE3_2021-22_Analytical_solution_surf.png}
\includegraphics[width=0.5\columnwidth]{CW2_PDE3_2021-22_Analytical_solution_contour.png}

\hfill[Surface or contour plot with proper labelling: 4]
\end{solution}
\end{parts}
%-------------------------------------------------------%

%-------------------------------------------------------%
\newpage
\question[50] The 1D problem from question 1 is extended to the following similar 2D problem
\setcounter{equation}{0}
\begin{align}
\frac{\partial u(x,y,t)}{\partial t} &= \kappa \left(\frac{\partial^2 u(x,y,t)}{\partial x^2}+\frac{\partial^2 u(x,y,t)}{\partial y^2}\right) \label{eq:2D_PDE}
\end{align}
on the interval $0\le x\le\frac\pi2, -\frac\pi2\le y\le \frac\pi2$ with the initial condition
\begin{align}
u(x,y,0) &= 0.5\cos(x)\cos(y) 
\end{align}
and the boundary conditions
\begin{align}
u(0,y,t) &= 0.5\cos(y)\exp(-\kappa t) \quad &\forall t\ge0, -\frac\pi2\le y\le \frac\pi2, \label{eq:2D_left_boundary} \\
u\left(\frac\pi2,y,t\right) &= t\cos y \quad &\forall t\ge0, -\frac\pi2\le y\le \frac\pi2, \label{eq:2D_right_boundary} \\
u\left(x,\pm\frac\pi2,t\right) &= 0, \quad &\forall t\ge0, 0\le x \le\frac\pi2,\label{eq:2D_top_bottom_boundary}
\end{align}
as before $\kappa=2$.

\begin{parts}
\part[12] Starting with the appropriate finite difference approximations for the derivatives derive the 2D Crank-Nicholson method, stating it's formal order of accuracy in time and space.   You should also explain how the boundary conditions \ref{eq:2D_left_boundary}--\ref{eq:2D_top_bottom_boundary} would be treated.

\droppoints
\begin{solution}
The 2D Crank-Nicholson Scheme which is first order in time and 2nd order in space, is given by
\[
\frac{u^{n+1}_{ij}-u^{n}_{ij}}{\Delta t}=
\frac\kappa2\left(\hat{\delta}^2_x+\hat{\delta}^2_y\right).\left(u_{ij}^{n+1}+u_{ij}^n\right)
\]
with the two spatial derivative approximations
\begin{align*}
\hat{\delta}^2_x.u_{ij}^n&=\frac{u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}}{\Delta x^2}
=\frac{\delta_x^2u_{ij}^n}{\Delta x^2}, \\
\hat{\delta}^2_y.u_{ij}^n&=\frac{u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}}{\Delta y^2}
=\frac{\delta_y^2u_{ij}^n}{\Delta y^2}.
\end{align*}
We need to use a linear algebra solver to solve for the unknown values of $u^{n+1}_{ij}$, so we need to determine the coefficient matrix, $A$, and the RHS vector $\vec{b}$.   Expanding the definition above we get 
\begin{align*}
\frac{u^{n+1}_{ij}-u^{n}_{ij}}{\Delta t}&=
\kappa\frac{u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}}{2\Delta x^2}
+\kappa\frac{u_{i-1j}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{i+1j}}{2\Delta x^2}\\
&+\kappa\frac{u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}}{2\Delta y^2}
+\kappa\frac{u_{ij-1}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{ij+1}}{2\Delta y^2}
\end{align*} \hfill[4]

We rearrange it to have all terms for the next time step, $t^{n+1}$, on the left hand side of the equation. We multiply the expanded Crank-Nicholson scheme by $\Delta t$ to get
\begin{align*}
u^{n+1}_{ij}-u^{n}_{ij}&=
\frac{\kappa\Delta t}{2\Delta x^2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}
+u_{i-1j}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{i+1j}\right)\\
&+\frac{\kappa\Delta t}{2\Delta y^2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}
+u_{ij-1}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{ij+1}\right).
\end{align*}
putting $u^{n+1}$ terms on the LHS and $u^n$ terms on the RHS,
\begin{align*}
u^{n+1}_{ij}&
-\frac{\kappa\Delta t}{2\Delta x^2}\left(u_{i-1j}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{i+1j}\right)
-\frac{\kappa\Delta t}{2\Delta y^2}\left(u_{ij-1}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{ij+1}\right)\\
&=u^{n}_{ij}+
\frac{\kappa\Delta t}{2\Delta x^2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}\right)
+\frac{\kappa\Delta t}{2\Delta y^2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}\right).
\end{align*}
Writing as a linear equation, we get
\[a\cdot u^{n+1}_{ij-1}+b\cdot u^{n+1}_{i-1j}+c^{n+1}_{ij}+b\cdot u^{n+1}_{i+1j}+a\cdot u^{n+1}_{ij+1}=d^{ij}\]
where 
\begin{align*}
a&=-\frac{\kappa\Delta t}{2\Delta y^2}=-\frac{r_y}{2}, \\
b&=-\frac{\kappa\Delta t}{2\Delta x^2}=-\frac{r_x}{2}, \\
c&=1+2\frac{\kappa\Delta t}{2\Delta x^2}+2\frac{\kappa\Delta t}{2\Delta y^2}=1+r_x+r_y 
\end{align*}
and
\begin{align*}
d_{ij}&=u^{n}_{ij}+
\frac{\kappa\Delta t}{2\Delta x^2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}\right)
+\frac{\kappa\Delta t}{2\Delta y^2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}\right)\\
&=u^{n}_{ij}+
\frac{r_x}{2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}\right)
+\frac{r_y}{2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}\right).
\end{align*}
with $r_x=-\frac{\kappa\Delta t}{\Delta x^2}$ and $r_y=-\frac{\kappa\Delta t}{\Delta y^2}$. \hfill[4]

All the boundary conditions (\ref{eq:2D_left_boundary}--\ref{eq:2D_top_bottom_boundary}) are Dirichlet Boundary conditions.  Where the value of u is a function of $x$, $y$ and $t$.  
\begin{itemize}
\item Ghost cells should be used to store the values of the function on the boundary. \hfill[1]
\item Condition (\ref{eq:2D_left_boundary}) specifies that the value of $u_{i-1j}$ is known so the RHS value $d_{ij}$ must have $r_x u_{i-1j}/2$ added to it. \hfill[1]
\item Condition (\ref{eq:2D_right_boundary}) specifies that the value of $u_{i+1j}$ is known so the RHS value $d_{ij}$ must have $r_x u_{i+1j}/2$ added to it. \hfill[1]
\item Since $u=0$ on the upper and lower boundaries no change is needed to the RHS value $d_{ij}$ \hfill[1]
\end{itemize}
\end{solution}

\part[15]  Briefly describe the steps needed to implement the Crank-Nicholson method using the preconditioned bi-conjugate gradient method to solve the equations, illustrating your description with elements of your \textsc{Python} code. 

\droppoints
\begin{solution}
The solution algorithm is given in Algorithm \ref{a:CN}.  The routines for mapping the coordinates, computing the LU factorisation and solving the system can be adapted from the elliptic solver in the lecture.  

\begin{algorithm}[H]
\SetAlgoLined
\KwResult{\(u_{ij}(t_1)\)} 
 Calculate the time step, $\delta t=\nu\min\left(\frac{\Delta x}{2\kappa},\frac{\Delta y}{2\kappa}\right)$\;
 Assemble the $A$ matrix with coefficients $a=-\frac{r_y}{2},\, 
 b=-\frac{r_x}{2},\, c=1+r_x+r_y$ and $d=u^{n}_{ij}+
\frac{r_x}{2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}\right)
+\frac{r_y}{2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}\right)$\;
 $t=0$\;
 \While{$t<t_1$}{
   \(dt=\min(dt,t_1-t)\)\;
   Calculate $\vec{b}$\;
   Map \(u_{ij}(t)\) onto \(\vec{x_0}\)\;
   \For{\(i=2:N_y-1\)}{
      \For{\(j=2:N_x-1\)}{
        \(k=(i-1)+(j-2)(N_y-2)\)\;
        \(b(k)=u(i,j)+\frac{r_x}{2}(u_{i-1j}-2u_{ij}+u_{i+1j})
            +\frac{r_y}{2}(u_{ij-1}-2u_{ij}+u_{ij+1})\)\;
            }
        }
    Update boundary conditions to $t=t+\delta t$\;
    Update $\vec{b}$ for grid points on the boundary.\;
    Compute the incomplete LU factorisation\;
    Solve for $A\vec{x}=\vec{b}$ using the bi-conjugate gradient solver with $L$, $U$ and an initial guess $\vec{x_0}$\;
    Map \(\vec{x}\) onto \(u_{ij}(t+\delta t)\)\;
    \(t=t+\delta t\)\;
    }
 \caption{2D Crank-Nicholson method}
 \label{a:CN}
\end{algorithm}

Alternatively bits of the Python code can be provided, the full code is:
\begin{minted}[fontsize=\footnotesize]{python}
import scipy.sparse as sps
import scipy.sparse.linalg as LA

def Crank_Nicholson(mesh,t_stop,Courant_no=5.0):
    '''Advance the solution from the current time to t=t_stop
    using the Crank-Nicholson method with the specified time
    step delta_t'''
    
    # calculate delta_t based on the Courant Number nu
    delta_t = Courant_no*min(mesh.Delta_x()**2/(2*mesh.kappa), mesh.Delta_y()**2/(2*mesh.kappa));
    maxit = int((t_stop-mesh.time)/delta_t
    out_it = maxit // 50
    
    print('Crank Nicholson with dt={:.4g} ({:d} times steps)'.format(delta_t,maxit)))
    
    # Create the A matrix using the lil format and the b vector
    # as numpy vector.
    N = (grid.Nj-2)*(grid.Ni-2)
    A_mat = sps.lil_matrix((N, N), dtype=np.float64)
    x_vec = np.zeros(N, dtype=np.float64)
    b_vec = np.zeros(N, dtype=np.float64)

    # calculate the rx and ry and the a,b and c coeeficients
    rx = mesh.kappa*delta_t/mesh.Delta_x()**2
    ry = mesh.kappa*delta_t/mesh.Delta_y()**2

    # now build the A matricies
    for i in range(1,mesh.Ni-1):
        for j in range(1,mesh.Nj-1):
            k = (i-1) + (mesh.Ni-2)*(j-1)
                
            # set the leading diagonal coefficient
            A_mat[k,k] = 1 + rx + ry
               
            # i direction
            for m in range(i-1,i+2,2):
                if not(m<1 or m>mesh.Ni-2):
                    l = (m-1) + (mesh.Ni-2)*(j-1)
                    A_mat[k,l] = -rx/2

            # j direction
            for m in range(j-1,j+2,2):
                if not(m<1 or m>mesh.Nj-2):
                    l = (i-1) + (mesh.Ni-2)*(m-1)
                    A_mat[k,l] = -ry/2
    
    # assemble the preconditioner
    ilu = LA.spilu(A_mat.tocsc(), drop_tol=1e-6, fill_factor=100)
    M_mat = LA.LinearOperator(A_mat.shape, ilu.solve)

    # set an itteration counter
    it = 0
        
    # time loop
    status = 1
    while mesh.time < t_stop:
        #progress counter
        if it % outit ==0:
            print('#',end='')
            
        # ensure we don't overshoot the stop time
        dt = min(delta_t, t_stop-mesh.time)
        
        # set the boundary conditions
        set_boundary_conditions(mesh,mesh.time)
        
        # extract the x_vector 
        x_vec = np.reshape(mesh.u[1:-1,1:-1],(N))

        # % Calculate the b vector using the values from the current time step
        for i in range(1,mesh.Ni-1):
            for j in range(1,mesh.Nj-1):
                k = (i-1) + (mesh.Ni-2)*(j-1)
                
                # b(k) calculate from the stencil for the current time step
                b_vec[k]=mesh.u[j,i] \
                    + rx*(mesh.u[j,i-1] - 2*mesh.u[j,i] + mesh.u[j,i+1])/2 \
                    + ry*(mesh.u[j-1,i] - 2*mesh.u[j,i] + mesh.u[j+1,i])/2
 
        # Now apply the boundary conditions for the next time level
        set_boundary_conditions(mesh,mesh.time+dt)

        # update the b vector to include boundary conditions
        for i in range(1,mesh.Ni-1):
            for j in range(1,mesh.Nj-1):
                k = (i-1) + (mesh.Ni-2)*(j-1)
                
                # i direction
                for m in range(i-1,i+2,2):
                    if m<1 or m>mesh.Ni-2:
                        b_vec[k] += rx * mesh.u[j,m]/2

                # j direction
                for m in range(j-1,j+2,2):
                    if m<1 or m>mesh.Nj-2:
                        b_vec[k] += ry * mesh.u[m,i]/2
                        
        # solve the matrix system
        x_vec, status = LA.bicgstab(A_mat,b_vec,x0=x_vec,M=M_mat)
            
        if status==0:
            # unpack x_vec into u
            mesh.u[1:-1,1:-1] = np.reshape(x_vec,(mesh.Ni-2,mesh.Nj-2))
        if status != 0:
            break
        
        #update everything
        mesh.time += dt
        it += 1
    
    print('.')
    if status == 0:
        return it
    else:
        return -status
\end{minted}

The students can provide a pseudo-code algorithm such as Algorithm~\ref{a:CN} or describe the changes in the text. 15 Marks are available as follows:
\begin{itemize}
    \item Using a Courant number $\nu>1$ and calculating the time step \hfill[1]
    \item Assembling the $A$ matrix \hfill[2]
    \item Map the 2D solution onto a 1D vector to calculate $b(k)$\hfill [4]
    \item Update the boundary conditions and apply them only to the $b_k$ values associated with the new time level \hfill[4]
    \item Solving the linear system using the bi-conjugate gradient solver \texttt{bicg} and the LU preconditioner \hfill[2]
    \item Providing an initial guess $x_0$ to accelerate the solver \hfill[2]
\end{itemize}
\end{solution}
\part[23] Hence, find the solution at $t=2$s.  By comparing a grid converged solution (at y=0) with the analytical solution from question 1 comment on the accuracy of the method and the computational effort needed.
\droppoints
\begin{solution}
A mesh refinement test should be performed using an appropriate metric, a grid of at least $40\times40$ should be used for the final solution.  This will take about a minute to run, assuming a Courant number of $\nu=5$.   Care must be taken to ensure that $\Delta t$ is not too large as the solution will be stable but inaccurate.

The 2D contour plot of the converged solution (with labels, caption, etc.)\hfill[3]

\centerline{\includegraphics[width=0.9\columnwidth]{cw2-solution.pdf}}

Comparison with the 1D analytical solution from Q1. Five marks are available. For marking consider the following points:
\begin{itemize}
\item Plot comparing solutions, as follows showing numerical solution as circles and analytical solution as a solid line. \hfill[2]
\centerline{\includegraphics[width=0.9\columnwidth]{cw2-centerline.pdf}}
\item In the numerical solution the boundary conditions at $y=\pm\pi/2$ and $x=0$ are all zero,  along $x=\pi/2$ the function value is $2\cos{y}$.  This means that the solution will decay non-linearly towards the $x=0$ boundary.\hfill[1]
\item We know from 1(x) that at $t=2$ the analytical solution is almost $2xt/\pi$ and so is approximately linear.
\hfill[1]
\item The 2D and 1D problems are \textbf{not} comparable, and the 1D solution \textbf{cannot} be used to validate the 2D solution. \hfill[1]
\item This could be done by changing the boundary condition in the 2D problem so that $u(\pi/2.y)$ is constant with symmetry boundaries at $y=\pm\pi/2$.
\end{itemize}

Mesh refinement study. Marks are awarded for:
\begin{itemize}
    \item using at least \textbf{four} grids \hfill[4]
    \item choosing an integrated quantity defined along the $y=0$ axis \hfill[2]
    \item quoting the apparent order of accuracy of the numerical method \hfill[2]
    \item quoting the time to solution on each grid \hfill[2]
\end{itemize}

\textbf{Computational cost of the method.}

Plotting time to solution vs number of grid points is worth [3] marks provided the grid is properly labeled.  

An explanation similar to: the cost at each time step is proportional to $N^2$ and since $\delta t \propto \Delta x$ the total time to solution will be proportional to $N^3$  \hfill[2]
\end{solution}
\end{parts}


\end{questions}


%----- Remarks ----------------------------------------------%
\newpage
\textbf{Remarks:}
\begin{itemize}
\item The differential equation
\[
\frac{\text{d}^2 u(x)}{\text{d}x^2} - \sigma^2 u(x) = 0
\]
has the equivalent solutions
\begin{align*}
u_1(x) &= A e^{\sigma x} + B e^{-\sigma x} \\
u_2(x) &= \tilde{A~} \sinh(\sigma x) + \tilde{B} \sinh(\sigma(l-x))
\end{align*}
where $l$ is an arbitrary constant.

Depending on the problem one or the other leads to an easier solution.
\item The Laplace domain function
\[
F(s) = \frac{\sinh(b\sqrt{s})}{s\sinh(a\sqrt{s})}
\]
has the inverse transform
\begin{align*}
f(t) =& \frac{b}{a} + \frac{2}{\pi} \sum_{n=1}^\infty \frac{(-1)^n}{n} e^{-n^2\pi^2 t/a^2} \sin\left(\frac{n\pi b}{a}\right)
\end{align*}

\item The Laplace domain function
\[
F(s) = \frac{\sinh(b\sqrt{s})}{s^2\sinh(a\sqrt{s})}
\]
has the inverse transform
\begin{align*}
f(t) =& \frac{bt}{a} + \frac{2a^2}{\pi^3} \sum_{n=1}^\infty \frac{(-1)^n}{n^3}\left(1 - e^{-n^2\pi^2 t/a^2}\right) \sin\left(\frac{n\pi b}{a}\right)
\end{align*}
\end{itemize}

\vskip0.1cm

\newpage
%----- Table -------------------------------------------------------%
\begin{table}[ht]
\caption{Properties of the unilateral Laplace transform} % title of Table
\centering % used for centering table
\begin{tabular}{|c|c|c|} % centered columns (4 columns)
\hline %inserts double horizontal lines
Property & $f(t), t\ge0$ & $F(s)$ \\ 
%heading
\hline 
Definition & $f(t)$ & $\int_0^\infty\exp(-st)f(t)dt$ \\ [0.3ex]
Inversion & $\frac{1}{j2\pi}\int_{c-j\infty}^{c+j\infty} \exp(st)F(s)ds$ & $F(s)$ \\ [0.3ex]
Linearity & $\alpha f(t) + \beta g(t)$ & $\alpha F(s) + \beta G(s)$ \\ [0.3ex]
Scaling & $f(\alpha t)$ & $\frac{1}{\alpha}F\left(\frac{s}{\alpha}\right)$ \\ [0.3ex]
Frequency shifting & $\exp(\alpha t)f(t)$ & $F(s-\alpha)$ \\ [0.3ex]
Time shifting & $f(t-\alpha) H(t-\alpha)$ & $\exp(-\alpha s)F(s)$ \\ [0.3ex]
First derivative & $f'(t)$ & $sF(s) - f(0)$ \\ [0.3ex]
nth derivative & $f^{(n)}(t)$ & $s^nF(s)-s^{n-1}f(0)-s^{n-2}f'(0)$ \\
      & & $-\dots-s f^{(n-2)}(0) - f^{(n-1)}(0)$  \\ [0.3ex]
Transform derivative & $t^n f(t)$ & $(-1)^n \frac{d^n F(s)}{ds^n}$ \\ [0.3ex]        
Integration & $\int_0^t f(\tau)d\tau$ & $\frac{1}{s}F(s)$  \\ [0.3ex]
\hline
\end{tabular}
\label{table:properties} % is used to refer this table in the text
\end{table}

%----- Table -------------------------------------------------------%
\begin{table}[ht]
\caption{Function transforms} % title of Table
\centering % used for centering table
\begin{tabular}{|c|c|c|c|} 
\hline %inserts double horizontal lines
Name & $f(t), t\ge0$ & $F(s)$ & Convergence region \\ [0.3ex] % inserts table
%heading
\hline % inserts single horizontal line
Unit impulse & $\delta(t)$ & $1$ & all $s$ \\ [0.3ex]
Ideal delay& $\delta(t-\alpha)$ & $\exp(-\alpha s)$ & $\Re(s)\ge\alpha$ \\ [0.3ex]
Unit step & $H(t)$ & $\frac{1}{s}$ & $\Re(s)>0$ \\ [0.3ex]
Ramp  & $t$ & $\frac{1}{s^2}$ & $\Re(s)>0$ \\ [0.3ex]
nth power  & $t^n, n=1,2,\dots$ & $\frac{n!}{s^{n+1}}$ & $\Re(s)>0$ \\ [0.3ex]
Sine & $\sin(\alpha t)$ & $\frac{\alpha}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
Cosine & $\cos(\alpha t)$ & $\frac{s}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
Hyperbolic sine & $\sinh(\alpha t)$ & $\frac{\alpha}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
Hyperbolic cosine & $\cosh(\alpha t)$ & $\frac{s}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
Exponential decay & $\exp(-\alpha t)$ & $\frac{1}{s + \alpha}$ & $\Re(s)>-\alpha$ \\ [0.3ex]
\hline %inserts single line
\end{tabular}
\label{table:functions}
\end{table}

\end{document}
