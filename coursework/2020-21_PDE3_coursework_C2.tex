 % Add 'answers' to options to get the solutions
\documentclass[answers,addpoints,12pt]{exam}	

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Coursework 2}
\footer{}{Page \thepage\ of \numpages}{\iflastpage{\textbf{END}}{\textbf{Continued}}}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_coursework_style}
\usepackage[ruled,vlined]{algorithm2e}
%\usepackage{minted}

\pointsdroppedatright 

%-------------------------------------------------------%
\begin{document}

The following coursework is to be handed in to the online dropbox by \textbf{16:00 on Thursday the 18$^{th}$ of March 2021}. This is \textbf{coursework number 2 of 2}. Each coursework is worth $20\%$ of the final mark.
\vskip0.1cm
You should submit the solution to this coursework in the form of a short report on Learn. This report must be typed, with at least 2cm margins, a font size of 11pt and line spacing of at least 1, and must not be longer than 8 pages. The title page does not count towards the page limit.

The marks for this coursework will be awarded for the correctness of the solution as well as for the clarity and logical progression of the solution. Details on laying out mathematical calculations and general guidelines for the coursework are given on Learn. 

%----- Question ----------------------------------------%
% This is the start of the questions
\begin{questions}
\question Consider the one-dimensional heat conduction equation
\begin{align}
\frac{\partial u(x,t)}{\partial t} &= \kappa \frac{\partial^2 u(x,t)}{\partial x^2} \tag{a} \\
\intertext{on the interval $-1 \le x\le 0$ for the initial condition}
u(x,0) &= (1+x)^2, \quad -1\le x \le 0, \tag{b} \\
\intertext{and the boundary conditions} 
 u(0, t)   &= 1, \quad \forall t, \tag{c} \\
 \frac{\partial u(-1, t)}{\partial x} &= 0, \quad \forall t. \tag{d} 
\end{align}
\begin{parts}
\part[8] Describe the problem which is given by the one-dimensional heat conduction equation and the initial and boundary conditions, Eqs.~(a-d). Without solving the heat equation give the steady-state solution $U$, i.e. the solution for $t\to\infty$.
\droppoints

\begin{solution}
The one-dimensional heat equation describes the heat conduction along the length of a beam of length 1. 
The beam is held at a given temperature at the right end and has no heat loss at the left end, i.e. perfect insulation. The beam has a given initial temperature profile. \hfill[2]

The right boundary is kept at $1$ and there is no heat loss at the left boundary, thus the function
\[
U(x) = 1
\]
fulfils conditions (c) and (d) and also $\nabla^2 U = 0$; thus is the steady state solution.  

Two marks each for steady state solution and mentioning that it fits the PDE and boundary conditions.\hfill[6]
\end{solution}

\part[8] Use the steady-state solution to reformulate the problem given by Eqs.~(a-d) into a problem for the transient solution $v$. \\
Remark: $u = U + v$
\droppoints

\begin{solution}
The steady-state solution is
\[
U(x) = 1
\]
and thus the initial and boundary conditions can be calculated from the relationship
\[
v(x,t) = u(x,t) - 1
\]

With this relationship the initial condition for the transient problem is given by
\begin{align}
v(x,0) &= u(x,0) - U(x) = 1 + 2x + x^2 - 1 = 2x + x^2, \quad -1\le x \le 0, \tag{e}\\
\intertext{and the boundary conditions are given by} 
 v(0, t) &= u(0,t) - U(0) =  0, \quad \forall t. \tag{f} \\
 \frac{\partial v(-1, t)}{\partial x} &= \frac{\partial u(-1, t)}{\partial x} - \frac{d U(-1)}{dx} = 0, \quad \forall t, \tag{g}
\end{align}
Two marks for the general relationship and for each of the three conditions with correct $x$ and $t$ limits. \hfill[8]
\end{solution}

\part[24] Use separation of variables to obtain the transient solution $v(x,t)$ of the heat conduction equation. Find the general solution of the problem given by Eqs.~(a-d).
\droppoints

\begin{solution}
We seek a solution of the form
\begin{align*}
v(x,t) &= e^{-\alpha t} \left(A\sin(\lambda x) + B \cos(\lambda x)\right)
\end{align*}
subject to the initial and boundary conditions~(e-g) and with $\alpha=\kappa \lambda^2$. The parameters $A$ and $B$ need to be found to fulfil these conditions. \hfill[2]

The first boundary condition (f), $v(0, t)=0$, requires that
\begin{align*}
A\sin(\lambda 0) + B\cos(\lambda 0) \overset{!}{=} 0
\end{align*}
which is only fulfilled if $B=0$. \hfill[4]

To fulfil the second boundary condition (g), $\frac{\partial v(-1, t)}{\partial x} = 0$, it is required that
\begin{align*}
\cos(-\lambda) &= 0 \\
\intertext{which is fulfilled for}
\lambda_n &= (n-0.5)\pi, \quad n=1,2,\dots
\end{align*}
\hfill[4]

Thus we get solutions of the form
\[
v_n(x,t) = e^{-\kappa \lambda_n^2 t} A_n\sin((n-0.5)\pi x), \quad n=1,2,\dots
\]
where we have used $\alpha_n = \kappa \lambda_n^2$. To fulfil the initial condition (e) we can superimpose these solutions 
\[
v(x,t) = \sum_{n=1}^\infty e^{-\kappa \lambda_n^2 t} A_n\sin((n-0.5)\pi x)
\]
By setting the superposed solution equal to the modified initial condition we can find the parameters $A_n$ from the following equation 
\[
\sum_{n=1}^\infty A_n\sin((n-0.5)\pi x) \overset{!}{=} 2x + x^2
\]
for $-1\le x \le 0$. \hfill[6]

Using the Fourier sine series expansion over the interval $-1\le x \le 0$, we get two integrals which can be solved using the list of integrals at the end of the exam paper \begin{align*}
A_n &= \frac{2}{1} \int_{-1}^0 (2x + x^2) \sin((n-0.5)\pi x) dx \\
     &= 4\int_{-1}^0 x \sin((n-0.5)\pi x) dx + 2\int_{-1}^0 x^2 \sin((n-0.5)\pi x) dx \\
     &= \frac{4}{\lambda_n^2} \left[\sin(\lambda_n x) - \lambda_n x \cos(\lambda_n x)\right]_{-1}^0 + 2\left[\frac{2x}{\lambda_n^2}\sin(\lambda_n x) - \left(\frac{x^2}{\lambda_n} - \frac{2}{\lambda_n^3}\right)\cos(\lambda_n x) \right]_{-1}^0 \\
 &= -\frac{4}{\lambda_n^2} (-1)^{n+1}
 + 2 \left[\frac{2}{\lambda_n^3} + \frac{2}{\lambda_n^2}(-1)^{n+1}\right] = \frac{4}{\lambda_n^3}
\end{align*}
for $n=1,2, \dots$. \hfill[4]

Thus the transient and general solutions are 
\begin{align*}
 v(x,t) &= \sum_{n=1}^\infty e^{-\kappa \lambda_n^2 t} \frac{4}{\lambda_n^3}\sin(\lambda_n x) \\
 u(x,t) &= 1 + \sum_{n=1}^\infty e^{-\kappa \lambda_n^2 t} \frac{4}{\lambda_n^3}\sin(\lambda_n x)
\end{align*}
for $-1\le x\le0$ and for $t\ge0$.\hfill[4]
\end{solution}
\end{parts}
%-------------------------------------------------------%

%-------------------------------------------------------%
\question[30] The \textsc{Matlab} program contained in the file \texttt{ADI.m} was written to solve the 2D transient ground water equation using the Alternating Direction Implicit (ADI) method on an $N_I\times N_J$ grid $100\text{ m}\times 100\text{ m}$.   Modify \texttt{function ADI} so that it solves the two dimensional heat conduction equation:

\begin{align}
\frac{\partial u(x,y,t)}{\partial t} &= \kappa \left(\frac{\partial^2 u(x,y,t)}{\partial x^2}+\frac{\partial^2 u(x,y,t)}{\partial y^2}\right) \tag{a}
\end{align}
on the interval $-1\le x\le0,\, -0.5\le y\le 0.5$ with the initial condition
\begin{align}
u(x,y,t=0) &= (1+x)^2(8y^3 - 4y^2+1) \tag{b} 
\end{align}
and the boundary conditions
\begin{align}
\frac{\partial u(-1,y, t)}{\partial x} &= 0, \quad &\forall t\ge0, -0.5\le y \le 0.5, \tag{c} \\
u(0.0,y,t) &= 8y^3\cos\left(\frac{\pi}{10} t\right)-4y^2+1, \quad &\forall t\ge0, -0.5<y<0.5, \tag{d} \\
u(x,-0.5,t) &= -(x+1)^2\cos\left(\frac{\pi}{10} t\right), \quad &\forall t\ge0, -1<x<0,\tag{e} \\
u(x,0.5, t) &= (x+1)^2\cos\left(\frac{\pi}{10} t\right), \quad &\forall t\ge0, -1<x<0. \tag{f} 
\end{align}

Find the solution at $t=20$s.  By comparing a grid converged solution (at y=0) with the analytical solution from question 1 comment on the accuracy of the method and the computational effort needed.
\droppoints
\begin{solution}
The driver, \texttt{ADI.m} in Matlab, needs to be modified so that it is applicable to the 2D heat conduction problem given by Equations (a) -- (f). First we realise that the PDE for the given problem is the same as the PDE for the transient ground water problem for which the program was originally developed. We only change the context of the dependent variable and of the diffusion parameter which is given by $\kappa$ in Equation (a). \hfill[2]

Next, we set the spatial grid to the interval $-1\le x\le0$ and $-0.5\le y\le 0.5$ and define the grid according to the following equations
\begin{align*}
x_j &= -1 + (j-1)\Delta x, \qquad & j=1,2,\dots,N_x+1 \\
y_i &= -0.5 + (i-1)\Delta y, \qquad & i=1,2,\dots,N_y+1
\end{align*}
where $\Delta x = \frac{1}{N_x}$ and $\Delta y=\frac{1}{N_y}$ are the grid spacing in the $x$ and $y$ direction, respectively. At the grid point $(i,j)$ the x and y coordinates are $x_j$ and $y_i$, respectively. At the discrete time $t_n$, we define $u(x_j, y_i, t_n) = u_{i,j}^n$.\hfill[2]

Set the initial conditions according to Equation~(b) which can be achieved with the following equation
\begin{align*}
u_{i,j}^0 &= (1 + x_j)^2 (8 y_i^3 - 4 y_i^2 +1)
\end{align*}
for $j=1,2,\dots,N_x+1$ and $i=1,2,\dots,N_y+1$ and where the superscript $0$ indicates $t_0=0$. \hfill[2]

The stop time of the ADI solver is set to $t_{steop}=20$. \hfill[2]

Next, we have to adjust the boundary conditions to the ones given in Equations (c) -- (f). The Neumann condition (c) at the left boundary will be discretised with the forward discretisation. The other boundaries are of the Dirichlet type and the values can be directly set according to Equations~(d) -- (f). The equation in mathematical notation are given by
\begin{align*}
u^n_{i,1} &= u^n_{i,2}, \quad \forall t_n\ge 0, i=1,2,\dots,N_y+1 \\
u^n_{i,N_x+1} &= 8 y_i^3 \cos\left(\frac{\pi}{10} t_n\right) - 4 y_i^2 +1 , \quad \forall t_n\ge 0, i=1,2,\dots,N_y+1 \\
u^n_{1,j} &= -(1 + x_j)^2 \cos\left(\frac{\pi}{10} t_n\right) , \quad \forall t_n\ge 0, j=1,2,\dots,N_x+1 \\
u^n_{N_x+1,j} &= (1 + x_j)^2 \cos\left(\frac{\pi}{10} t_n\right) , \quad \forall t_n\ge 0, j=1,2,\dots,N_x+1
\end{align*}
\hfill[2 points for each boundary condition]

A mesh refinement test should be performed using an appropriate metric, a grid of at least $40\times40$ should be used for the final solution.  This will take about a minute to run, assuming a Courant number of $\nu=0.5$. The $80\times80$ solution takes much longer! (635 seconds).

\hfill[6 points for mesh refinement study showing a grid converged solution and information on run time]

The 2D contour plot of the converged solution (with labels, caption, etc.)\hfill[3]
\centerline{\includegraphics[width=0.7\columnwidth]{CW2_2020-21_CN-2D.pdf}}


Comparison with the 1D analytical solution from Q1. Five marks are available. For marking consider the following points:
\begin{itemize}
\item Plot comparing solutions, as follows showing numerical solution as circles and analytical solution as a solid line. \hfill[2]
\centerline{\includegraphics[width=0.7\columnwidth]{CW2_2020-21_ADI-centerline.pdf}}
\item In the numerical solution the boundary conditions at $x=-1,\, y=\pm0.5$ have opposite signs.  This means that the steady state solution at $x=-1,\, y=0$ must be zero.\hfill[1]
\item In the analytical solution the symmetry boundary condition at $x=-1$ means the steady state solution must be one.\hfill[1]
\item The 2D and 1D problems are \textbf{not} comparable, and the 1D solution \textbf{cannot} be used to validate the 2D solution. \hfill[1]
\item This could be done by changing the boundary condition in the 1D problem to $u(1)=0.0$ and recomputing the analytical solution.
\end{itemize}
\end{solution}

%-------------------------------------------------------%
\question[30] Write a \textsc{Matlab} or \textsc{Python} program which also solves the 2D transient heat conduction problem in question 2 but uses the 2D Crank-Nicholson method with the preconditioned bi-conjugate gradient matrix solver to solve on the penta-diagonal sparse matrix system. 
The \texttt{bicg()} function was described in lecture 4 and the coefficients for the penta-digonal matrix were discussed in lecture 6. 
Once again find the solution at $t=20$s, and compare the required computational time and the accuracy of the method with the ADI scheme from question 2.
\droppoints
\begin{solution}
The 2D Crank-Nicholson Scheme which is first order in time and 2nd order in space, is given by
\[
\frac{u^{n+1}_{ij}-u^{n}_{ij}}{\Delta t}=
\frac\kappa2\left(\hat{\delta}^2_x+\hat{\delta}^2_y\right).\left(u_{ij}^{n+1}+u_{ij}^n\right)
\]
with the two spatial derivative approximations
\begin{align*}
\hat{\delta}^2_x.u_{ij}^n&=\frac{u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}}{\Delta x^2}
=\frac{\delta_x^2u_{ij}^n}{\Delta x^2}, \\
\hat{\delta}^2_y.u_{ij}^n&=\frac{u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}}{\Delta y^2}
=\frac{\delta_y^2u_{ij}^n}{\Delta y^2}.
\end{align*}
We need to use a linear algebra solver to solve for the unknown values of $u^{n+1}_{ij}$, so we need to determine the coefficient matrix, $A$, and the RHS vector $\vec{b}$.   Expanding the definition above we get 
\begin{align*}
\frac{u^{n+1}_{ij}-u^{n}_{ij}}{\Delta t}&=
\kappa\frac{u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}}{2\Delta x^2}
+\kappa\frac{u_{i-1j}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{i+1j}}{2\Delta x^2}\\
&+\kappa\frac{u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}}{2\Delta y^2}
+\kappa\frac{u_{ij-1}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{ij+1}}{2\Delta y^2}
\end{align*} \hfill[4]

We rearrange it to have all terms for the next timestep, $t^{n+1}$, on the left hand side of the equation. We multiply the expanded Crank-Nicholson scheme by $\Delta t$ to get
\begin{align*}
u^{n+1}_{ij}-u^{n}_{ij}&=
\frac{\kappa\Delta t}{2\Delta x^2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}
+u_{i-1j}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{i+1j}\right)\\
&+\frac{\kappa\Delta t}{2\Delta y^2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}
+u_{ij-1}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{ij+1}\right).
\end{align*}
putting $u^{n+1}$ terms on the LHS and $u^n$ terms on the RHS,
\begin{align*}
u^{n+1}_{ij}&
-\frac{\kappa\Delta t}{2\Delta x^2}\left(u_{i-1j}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{i+1j}\right)
-\frac{\kappa\Delta t}{2\Delta y^2}\left(u_{ij-1}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{ij+1}\right)\\
&=u^{n}_{ij}+
\frac{\kappa\Delta t}{2\Delta x^2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}\right)
+\frac{\kappa\Delta t}{2\Delta y^2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}\right).
\end{align*}
Writing as a linear equation, we get
\[a\cdot u^{n+1}_{ij-1}+b\cdot u^{n+1}_{i-1j}+c^{n+1}_{ij}+b\cdot u^{n+1}_{i+1j}+a\cdot u^{n+1}_{ij+1}=d^{ij}\]
where 
\begin{align*}
a&=-\frac{\kappa\Delta t}{2\Delta y^2}=-\frac{r_y}{2}, \\
b&=-\frac{\kappa\Delta t}{2\Delta x^2}=-\frac{r_x}{2}, \\
c&=1+2\frac{\kappa\Delta t}{2\Delta x^2}+2\frac{\kappa\Delta t}{2\Delta y^2}=1+r_x+r_y 
\end{align*}
and
\begin{align*}
d_{ij}&=u^{n}_{ij}+
\frac{\kappa\Delta t}{2\Delta x^2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}\right)
+\frac{\kappa\Delta t}{2\Delta y^2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}\right)\\
&=u^{n}_{ij}+
\frac{r_x}{2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}\right)
+\frac{r_y}{2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}\right).
\end{align*}
with $r_x=-\frac{\kappa\Delta t}{\Delta x^2}$ and $r_y=-\frac{\kappa\Delta t}{\Delta y^2}$. \hfill[4]

Using the ghost-cell method where the boundary conditions are stored in the halo cells $i=1$, $i=N_y+1$, $j=1$ and $j=N_x+1$ we must modify $d_{ij}$ when $i=2$, $j=2$, $i=N_y$ and $j=N_x$ since these values are not unknown. In such cases we get \hfill[2]
\begin{align*}
    d_{ij}=d_{ij}-a\cdot u_{i\pm1j}&\text { if }i=1\text{ or }i=N_y.\\
    d_{ij}=d_{ij}-b\cdot u_{ij\pm1}&\text { if }j=1\text{ or }i=N_x.
\end{align*}

The solution algorithm is given in Algorithm \ref{a:CN}.  The routines for mapping the coordinates, computing the LU factorisation and solving the system can be adapted from the elliptic solver in the lecture.  

\begin{algorithm}[H]
\SetAlgoLined
\KwResult{\(u_{ij}(t_1)\)} 
 Calculate the time step, $\delta t=\nu\min\left(\frac{\Delta x}{2\kappa},\frac{\Delta y}{2\kappa}\right)$\;
 Assemble the $A$ matrix with coefficients $a=-\frac{r_y}{2},\, 
 b=-\frac{r_x}{2},\, c=1+r_x+r_y$ and $d=u^{n}_{ij}+
\frac{r_x}{2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}\right)
+\frac{r_y}{2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}\right)$\;
 $t=0$\;
 \While{$t<t_1$}{
   \(dt=\min(dt,t_1-t)\)\;
   Calculate $\vec{b}$\;
   Map \(u_{ij}(t)\) onto \(\vec{x_0}\)\;
   \For{\(i=2:N_y-1\)}{
      \For{\(j=2:N_x-1\)}{
        \(k=(i-1)+(j-2)(N_y-2)\)\;
        \(b(k)=u(i,j)+\frac{r_x}{2}(u_{i-1j}-2u_{ij}+u_{i+1j})
            +\frac{r_y}{2}(u_{ij-1}-2u_{ij}+u_{ij+1})\)\;
            }
        }
    Update boundary conditions to $t=t+\delta t$\;
    Update $\vec{b}$ for grid points on the boundary.\;
    Compute the incomplete LU factorisation\;
    Solve for $A\vec{x}=\vec{b}$ using the bi-conjugate gradient solver with $L$, $U$ and an initial guess $\vec{x_0}$\;
    Map \(\vec{x}\) onto \(u_{ij}(t+\delta t)\)\;
    \(t=t+\delta t\)\;
    }
 \caption{2D Crank-Nicholson method}
 \label{a:CN}
\end{algorithm}

The students can provide a pseudo-code algorithm such as Algorithm~\ref{a:CN} or describe the changes in the text. 15 Marks are available as follows:
\begin{itemize}
    \item Using a Courant number $\nu>1$ and calculating the time step \hfill[1]
    \item Assembling the $A$ matrix \hfill[2]
    \item Map the 2D solution onto a 1D vector to calculate $b(k)$\hfill [4]
    \item Update the boundary conditions and apply them only to the $b_k$ values associated with the new time level \hfill[4]
    \item Solving the linear system using the bi-conjugate gradient solver \texttt{bicg} and the LU preconditioner \hfill[2]
    \item Providing an initial guess $x_0$ to accelerate the solver \hfill[2]
\end{itemize}


The new solver runs in about half the time of the original solver using a Courant number, $\nu=5$ and is even faster for higher Courant numbers.
We need to be careful as a larger Courant number will provide an inaccurate solution as the scheme is only first order in time.
Furthermore $\delta t$ must be small enough that variation in the boundary conditions are captured, using the Nyquist frequency we know that $dt\le\frac{\pi}{5}\approx0.6$ seconds. 

\begin{table}
\begin{tabular}{lllll}
Courant & Grid & dt & Iterations & Time [s] \\
\hline
5 & 40x40 & 1.563e-03 & 12800 & 14.9  \\
10 & 40x40 & 3.125e-03 & 6400 & 9.95 \\
20 & 40x40 & 9.375e-03 & 2134 & 5.23
\end{tabular}
\end{table}

The solution is very similar to that from Q2. Five marks are available for the comparison:
\begin{itemize}
    \item Choosing $\nu$ so that $\delta t<0.6$ and comment on the time to solution \hfill[2]
    \item Plots comparing ADI and CN solutions \hfill[3]
\end{itemize}

  \centerline{\includegraphics[width=0.45\columnwidth]{CW2_2020-21_CN-2D.pdf}
  \includegraphics[width=0.45\columnwidth]{CW2_2020-21_CN-2D.pdf}}
  \centerline{\includegraphics[width=0.45\columnwidth]{CW2_2020-21_ADI-centerline.pdf}
  \includegraphics[width=0.45\columnwidth]{CW2_2020-21_CN-centerline.pdf}}
  \centerline{\hfill ADI \hfill \hfill Crank-Nicholson, $\nu=20$\hfill}
\end{solution}

\end{questions}


%-------------------------------------------------------%
%\newpage
%{\large\textbf{Formula sheet}}
%\begin{itemize}
%\item The differential equation
%\[
%\frac{\text{d}^2 u(x)}{\text{d}x^2} - \sigma^2 u(x) = 0
%\]
%has the equivalent solutions
%\begin{align*}
%u_1(x) &= A e^{\sigma x} + B e^{-\sigma x} \\
%u_2(x) &= \tilde{A~} \sinh(\sigma x) + \tilde{B} \sinh(\sigma(l-x))
%\end{align*}
%Depending on the problem one or the other leads to an easier solution.
%\item The Laplace domain function
%\[
%F(s) = \frac{\sinh(b\sqrt{s})}{s\sinh(a\sqrt{s})}
%\]
%has the inverse transform
%\begin{align*}
%f(t) =& \frac{b}{a} + \frac{2}{\pi} \sum_{n=1}^\infty \frac{(-1)^n}{n} e^{-n^2\pi^2 t/a^2} \sin\left(\frac{n\pi b}{a}\right)
%\end{align*}
%
%\item The Laplace domain function
%\[
%F(s) = \frac{\sinh(b\sqrt{s})}{s^2\sinh(a\sqrt{s})}
%\]
%has the inverse transform
%\begin{align*}
%f(t) =& \frac{bt}{a} + \frac{2a^2}{\pi^3} \sum_{n=1}^\infty \frac{(-1)^n}{n^3}\left(1 - e^{-n^2\pi^2 t/a^2}\right) \sin\left(\frac{n\pi b}{a}\right)
%\end{align*}
%\end{itemize}
%
%\vskip0.1cm
%
%\newpage
%%----- Table -------------------------------------------------------%
%\begin{table}[ht]
%\caption{Properties of the unilateral Laplace transform} % title of Table
%\centering % used for centering table
%\begin{tabular}{|c|c|c|} % centered columns (4 columns)
%\hline %inserts double horizontal lines
%Property & $f(t), t\ge0$ & $F(s)$ \\ 
%%heading
%\hline 
%Definition & $f(t)$ & $\int_0^\infty\exp(-st)f(t)dt$ \\ [0.3ex]
%Inversion & $\frac{1}{j2\pi}\int_{c-j\infty}^{c+j\infty} \exp(st)F(s)ds$ & $F(s)$ \\ [0.3ex]
%Linearity & $\alpha f(t) + \beta g(t)$ & $\alpha F(s) + \beta G(s)$ \\ [0.3ex]
%Scaling & $f(\alpha t)$ & $\frac{1}{\alpha}F\left(\frac{s}{\alpha}\right)$ \\ [0.3ex]
%Frequency shifting & $\exp(\alpha t)f(t)$ & $F(s-\alpha)$ \\ [0.3ex]
%Time shifting & $f(t-\alpha) H(t-\alpha)$ & $\exp(-\alpha s)F(s)$ \\ [0.3ex]
%First derivative & $f'(t)$ & $sF(s) - f(0)$ \\ [0.3ex]
%nth derivative & $f^{(n)}(t)$ & $s^nF(s)-s^{n-1}f(0)-s^{n-2}f'(0)$ \\
%      & & $-\dots-s f^{(n-2)}(0) - f^{(n-1)}(0)$  \\ [0.3ex]
%Transform derivative & $t^n f(t)$ & $(-1)^n \frac{d^n F(s)}{ds^n}$ \\ [0.3ex]        
%Integration & $\int_0^t f(\tau)d\tau$ & $\frac{1}{s}F(s)$  \\ [0.3ex]
%\hline
%\end{tabular}
%\label{table:properties} % is used to refer this table in the text
%\end{table}
%
%%----- Table -------------------------------------------------------%
%\begin{table}[ht]
%\caption{Function transforms} % title of Table
%\centering % used for centering table
%\begin{tabular}{|c|c|c|c|} 
%\hline %inserts double horizontal lines
%Name & $f(t), t\ge0$ & $F(s)$ & Convergence region \\ [0.3ex] % inserts table
%%heading
%\hline % inserts single horizontal line
%Unit impulse & $\delta(t)$ & $1$ & all $s$ \\ [0.3ex]
%Ideal delay& $\delta(t-\alpha)$ & $\exp(-\alpha s)$ & $\Re(s)\ge\alpha$ \\ [0.3ex]
%Unit step & $H(t)$ & $\frac{1}{s}$ & $\Re(s)>0$ \\ [0.3ex]
%Ramp  & $t$ & $\frac{1}{s^2}$ & $\Re(s)>0$ \\ [0.3ex]
%nth power  & $t^n, n=1,2,\dots$ & $\frac{n!}{s^{n+1}}$ & $\Re(s)>0$ \\ [0.3ex]
%Sine & $\sin(\alpha t)$ & $\frac{\alpha}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
%Cosine & $\cos(\alpha t)$ & $\frac{s}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
%Hyperbolic sine & $\sinh(\alpha t)$ & $\frac{\alpha}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
%Hyperbolic cosine & $\cosh(\alpha t)$ & $\frac{s}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
%Exponential decay & $\exp(-\alpha t)$ & $\frac{1}{s + \alpha}$ & $\Re(s)>-\alpha$ \\ [0.3ex]
%\hline %inserts single line
%\end{tabular}
%\label{table:functions}
%\end{table}

\end{document}
