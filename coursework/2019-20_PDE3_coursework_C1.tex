 % Add 'answers' to options to get the solutions
\documentclass[answers,addpoints,12pt]{exam}	

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Coursework 1}
\footer{}{Page \thepage\ of \numpages}{\iflastpage{\textbf{END}}{\textbf{Continued}}}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_coursework_style}

%-------------------------------------------------------%
%-------------------------------------------------------%
\begin{document}

The following coursework is to be handed in to the online dropbox by \textbf{16:00 on Tuesday the 22$^{th}$ of October 2019}. This is \textbf{coursework number 1 of 2}. Each coursework is worth $20\%$ of the final mark.
\vskip0.1cm
% ---- Modify
You should submit the solution to this coursework in the form of a short report on Learn. This report must be typed, with at least 2cm margins, a font size of 11pt and line spacing of at least 1, and must not be longer than 7 pages. The title page does not count towards the page limit.

The marks for this coursework will be awarded for the correctness of the solution as well as for the clarity and logical progression of the solution. Details on laying out mathematical calculations and general guidelines for the coursework are given on Learn. 

\vskip0.1cm

%----- Question ----------------------------------------%
% This is the start of the questions
\begin{questions}
\question 

Consider the Laplace equation on the rectangle defined by $0\le x\le a$ and $-b\le y\le b$ where $a$ and $b$ are positive numbers.

The left and right boundaries of the rectangle are insulating. The top boundary is held at 0 and the bottom boundary is given by
\[
u(x, -b) = x^2, \quad 0\le x\le a.
\]

\begin{parts}
%-------------------------------------------------------%
\part[45] Calculate the analytical solution of the Laplace equation on the rectangle with the given boundary conditions. Plot the solution.

\vskip0.2cm
\textbf{Remarks:}
\begin{itemize}
\item You might need to use more than one type of the trial functions
\item Watch the video for Example 9.33 for inspiration
\end{itemize}

\begin{solution}
The Laplace equation in Cartesian coordinates is given by
\begin{align*}
\pdderiv{u}{x} + \pdderiv{u}{y} = 0
\end{align*}
We are looking for separated solutions of the form $u(x,y) = X(x) Y(y)$. To fulfil the boundary conditions, the solution at the left and right boundaries needs to be insulating. This condition can be easily fulfilled by trigonometric solutions in the $x$ direction which suggests the use of $u_1$ \hfill[3]
\begin{align*}
u_1(x,y) =& (A\sin \mu x+B\cos \mu x)\left(\tilde{C}\cosh \mu y+\tilde{D}\sinh \mu y\right) 
\end{align*}

We calculate the derivative of $u_1(x,y)$  
\begin{align*}
\pderiv{u_1}{x}(x, y) =& (A\mu\cos(\mu x) - B\mu\sin(\mu x))\left(\tilde{C}\cosh \mu y+\tilde{D}\sinh \mu y\right) 
\end{align*}
and set it to zero at the left boundary
\begin{align*}
\pderiv{u_1}{x}(x=0, y) =& (A\mu\cos(\mu 0) - B\mu\sin(\mu 0))\left(\tilde{C}\cosh \mu y+\tilde{D}\sinh \mu y\right) \overset{!}{=} 0
\end{align*}
From this it follows that $A=0$ and the solution reduces to 
\begin{align*}
u_1(x,y) =& \cos(\mu x)\left(C\cosh(\mu y) + D\sinh(\mu y)\right) 
\end{align*}
where $C=B\tilde{C}$ and $D=B\tilde{D}$.\hfill[3]

Next, we consider the right boundary
\begin{align*}
\pderiv{u_1}{x}(x=a, y) =& - \mu\sin(\mu a) \left(C\cosh(\mu y) +D\sinh(\mu y)\right) \overset{!}{=} 0
\end{align*}
This can only be fulfilled if the sine term is $0$ and thus it follows that \hfill[3]
\begin{align*}
\mu_n a &= n\pi\quad \Rightarrow\quad \mu_n = \frac{n\pi}{a}, \quad n=0,1,2,\dots 
\end{align*}
To keep the equations manageable we will use $\mu_n$ instead of the whole term.

Next, we consider the upper boundary condition where the solution is held at $0$ for all values of $x$. To handle this boundary condition at $y=b$, we mirror the solutions at the $x$ axis and shift them in the $y$ direction by $b$ which gives us the equivalent form
\begin{align*}
u_1(x, y) =& \cos(\mu_n x)\left(C_n\cosh(\mu_n (b-y)) + D_n\sinh(\mu_n (b-y))\right)
\end{align*}
where $C_n$ and $D_n$ are new, arbitrary constants and where we have replaced $\mu$ with $\mu_n$ for $n=0,1,2,\dots$. At the boundary, we need to fulfil \hfill[3]
\begin{align*}
u_1(x, y=b) =& \cos(\mu_n x)\left(C_n\cosh(\mu_n (b-b)) + D_n\sinh(\mu_n (b-b))\right) \overset{!}{=} 0
\end{align*}
from which it follows that $C_n$ needs to be zero.

To fulfil the bottom boundary condition we need to superimpose the solutions for all $\mu_n$. This is possible because we are dealing with a linear PDE. The complete solution is \hfill[3]
\[
u(x, y) = \sum_{n=0}^\infty D_n\sinh(\mu_n(b - y))\cos(\mu_n x) 
\]

To find the coefficients $D_n$ we need to set the solution at $y=-b$ equal to the boundary condition. Thus, we need to have \hfill[3]
\begin{align*}
u(x, y=-b) &= \sum_{n=0}^\infty D_n\sinh(\mu_n(b - (-b)))\cos(\mu_n x) \overset{!}{=} x^2
\end{align*}

The parameters $D_n$ can be found with the usual method of the Fourier series, as follows
\begin{align*}
D_n \sinh(2\mu_n b) &= \frac{2}{a} \int_{0}^{a} x^2 \cos(\mu_n x) dx
 \intertext{By using the formulas for integration of quadratic functions we get}
D_n \sinh(2\mu_n b) &= \frac{2}{a} \left[\frac{2x}{\mu_n^2}\cos(\mu_n x) + \left(\frac{x^2}{\mu_n} - \frac{2}{\mu_n^3}\right)\sin(\mu_n x)\right]_0^a \\
&= \frac{2}{a} \frac{2a}{\mu_n^2}\cos(\mu_n a) = \frac{4}{\mu_n^2} (-1)^n
 \intertext{and finally simplify to get the expression for the parameters $D_n$ \hfill[3]}
D_n &= (-1)^{n} \frac{4}{\mu_n^2 \sinh(2\mu_n b)}, \quad n=1,2,\dots
\end{align*}

The parameters $D_n$ only exist for $n=1,2,\dots$ and thus we need to add $u_3(x,y)$ \hfill[3]
\begin{align*}
u_3(x,y) =& (Ax+B)(Cy+D)
\end{align*}
From the left and right boundary conditions it follows that $A=0$ and from the top boundary condition 
\begin{align*}
u_3(x,b) = B(Cb+D) \overset{!}{=} 0
\end{align*}
it follows that \(Cb = -D\) so that we get
\begin{align*}
u_3(x,y) &= \tilde{C}(y - b)
\end{align*}
for \(\tilde{C} = BC\). \hfill[3]

The parameter $D_0$ in the Fourier series is given by
\begin{align*}
D_0 &= \frac{2}{a}\int_0^a x^2 dx = \frac{2}{3a}\left[x^3\right]_0^a = \frac{2a^3}{3a} = \frac{2}{3}a^2
\end{align*}
The value of $D_0$ needs to be equated with the solution $u_3$ at the bottom boundary\hfill[3]
\begin{align*}
u_3(x, -b) = -2\tilde{C}b \overset{!}{=} \frac{D_0}{2} = \frac{1}{3}a^2 \Rightarrow \tilde{C} = -\frac{1}{6}\frac{a^2}{b}
\end{align*}

Therefore, the complete solution is \hfill[3]
\begin{align}
u(x, y) =& -\frac{1}{6}\frac{a^2}{b}(y - b) \nonumber\\
 & \quad+ \sum_{n=1}^\infty (-1)^{n} \frac{4}{\mu_n^2 \sinh(2\mu_n b)} \sinh(\mu_n(b - y))\cos(\mu_n x) \nonumber\\
 & = -\frac{1}{6}\frac{a^2}{b}(y - b) \label{eq:analytical_solution}\\
 &\quad + \sum_{n=1}^\infty (-1)^{n} \frac{4a^2}{n^2\pi^2\sinh\left(2\frac{n\pi b}{a}\right)} \sinh\left(\frac{n\pi}{a}(b - y)\right)\cos\left(\frac{n\pi x}{a} \right) \nonumber
\end{align}
which can be plotted as: \hfill[3]


\includegraphics[width=0.5\columnwidth]{Cw2019_C1_analytical_contour}
\includegraphics[width=0.5\columnwidth]{Cw2019_C1_analytical}

The Fourier series term decrease as $1/n^2$ which suggests quadratic convergence. The integral error between the analytical solution~\ref{eq:analytical_solution} evaluated with 20 and 40 terms is around $10^{-5}$ which suggests that 20 terms of the series solution~\ref{eq:analytical_solution} are sufficient. \hfill[3]

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences; using proper mathematical typesetting for the equations. \hfill[6]
\end{solution}

%-------------------------------------------------------%
\part[35] From now on set $a=3$ and $b=2$. Develop a Matlab script to solve the problem numerically. Explain what you are doing, compare the solution against the analytical solution and perform a grid convergence analysis. Plot the solution. 

\vskip0.2cm
\textbf{Remarks:}
\begin{itemize}
\item While the Matlab scripts on Learn are a useful starting point, you will need to make some modifications.
\end{itemize}

\begin{solution}
To solve the Laplace equation with Matlab we need to discretise the partial differential equation. The derivatives can be discretised with the finite difference formulas for first and second derivatives from the lectures. 

We define the grid in the $x$ and $y$ direction as \hfill[3]
\begin{align*}
x_j &= (j-1)\Delta x, \qquad & j=1,2,\dots,n_x \\
y_i &= -b + (j-1)\Delta y, \qquad & i=1,2,\dots,n_y
\end{align*}
where $\Delta x$ and $\Delta y$ are the grid spacing in the $x$ and $y$ direction, respectively. At the grid point $(i,j)$ the position is given by $x_j$ and $y_i$, respectively, and we define $u(y_i, x_j) = u_{i,j}$. 

With the finite difference formulas we get 
\begin{align*}
\frac{u_{i,j+1} - 2u_{i,j} + u_{i,j-1}}{(\Delta x)^2} + \frac{u_{i+1,j} - 2u_{i,j} + u_{i-1,j}}{(\Delta y)^2} = 0
\end{align*}
Multiply this with $(\Delta x)^2$ and rearrange for $u_{i,j}$ to get
\begin{align*}
u_{i,j} \left(2 + 2\frac{(\Delta x)^2}{(\Delta y)^2}\right) =& u_{i,j+1} + u_{i,j-1} + \frac{(\Delta x)^2}{(\Delta y)^2} (u_{i+1,j} + u_{i-1,j}) 
\end{align*}
Multiply both sides of the equation with one over the factor multiplying $u_{i,j}$
\[
Ch = \frac{1}{\left(2 + 2\frac{(\Delta x)^2}{(\Delta y)^2}\right)}
\]
and bring it into the form required for the SOR method 
\begin{align}
u_{i,j}^{k+1}  = (1-\omega)u_{i,j}^{k} &+ \omega Ch \left(u_{i,j+1}^{k} + u_{i,j-1}^{k+1} + \beta^2 (u_{i+1,j}^{k} + u_{i-1,j}^{k+1}) \right)
\label{eq:uij}
\end{align}
where $\beta = \frac{\Delta x}{\Delta y}$ and $\omega$ is the relaxation factor. The superscript $k$ indicates the iteration number of the iterative SOR method. The Eq.~\ref{eq:uij} for $u_{i,j}^{k+1}$ is added into the SOR method for all internal values of $x$ and $y$, i.e. all values of $x$ and $y$ except at the boundaries. \hfill[3]

At the upper boundary, i.e. for $y= b$, we set the value of $u_{i,j}$ to zero 
\begin{align*}
u_{n_y, j} &= 0, \quad j=1,2,\dots, n_x
\end{align*}
and at the lower boundary, i.e. for $y=-b$, we set \hfill[3]
\begin{align*}
u_{1,j} &= x_j^2, \quad j=1,2,\dots, n_x
\end{align*}

At the left and right boundary the Neumann boundary conditions are discretised with the first order forward and backward difference formula, respectively, and rearranged to \hfill[3]
\begin{align*}
0 &= \left.\pderiv{u}{x}\right|_{i,1} = \frac{u_{i,2} - u_{i,1}}{\Delta x} \Rightarrow u_{i,1} = u_{i,2} \\
0 &= \left.\pderiv{u}{x}\right|_{i,n_x} = \frac{u_{i,n_x} - u_{i,n_x-1}}{\Delta x} \Rightarrow u_{i,n_x} = u_{i,n_x-1} 
\end{align*}
for $i=1,2,\dots,n_y$.

We ran the developed SOR method with a tolerance of $10^{-6}$ and compared this to a run with a tighter tolerance of $10^{-8}$. This gave us a maximum difference of around $10^{-4}$. Thus, it was decided that the tolerance of $10^{-6}$ is sufficient and all further runs used this tolerance. \hfill[3]

The following plots show the contour and surface plots for the numerical solution on a 40 by 40 grid.\hfill[3]

\includegraphics[width=0.5\columnwidth]{Cw2019_C1_numerical_contour}
\includegraphics[width=0.5\columnwidth]{Cw2019_C1_numerical}

The plots look very similar to the analytical solutions in part (a) which is confirmed by the contour and surface plots of the difference between the numerical and analytical solution for a 40 by 40 grid and 20 terms in the analytical solution. These plots show that the maximum difference for this case is about $0.3$. \hfill[3]

\includegraphics[width=0.5\columnwidth]{Cw2019_C1_Q1b_difference_contour}
\includegraphics[width=0.5\columnwidth]{Cw2019_C1_Q1b_difference}

The Matlab script RefinementAnalysis.m from the numerical lectures is used to perform the grid convergence analysis. The script is run for five grid sizes, i.e. 10x10, 20x20, 40x40, 80x80 and 160x160. \hfill[3]

The order of convergence is given as $p=1.02$. This is smaller than two, i.e. the order of the central finite difference approximation of the second derivatives, which is expected because of the first order approximation of the first derivatives on the left and right boundaries. \hfill[3]

Once the correct order of convergence is found, the grid convergence analysis shows that for all tested grids the error is proportional to $\mathcal{O}\left(dx^p\right)$. For example, the grid convergence ratio is $1.04$ which is close to $1$ on the three middle grids which indicates that the solution is grid independent for a 20x20 grid. \hfill[3]

\textbf{Remark:} Different values for the order of convergence and grid convergence can be reached for different spatial discretisations of the first spatial derivative and the Neumann boundary condition.

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[5]
\end{solution}

%-------------------------------------------------------%
\part[20] The physical system is modified by a source term in the lower half of the rectangle. Over the entire rectangle the source term is given by
\[
f(x, y) = \begin{cases}
0, \quad & 0\le y < b, 0\le x\le a \\
(b+y)y (a-x)x, \quad & -b < y\le 0, 0\le x\le a
\end{cases}
\]
Add the source term as an inhomogeneity to the Laplace equation and develop a Matlab script to solve the inhomogeneous problem numerically.

Explain what you are doing, compare the solution against the solution of the homogeneous system and perform a grid convergence analysis. Plot the solution.

\begin{solution}
In the derivation of the numerical formula in part (b) we need to replace the $0$ on the right hand side with the inhomogeneity. With the definition $f(\theta_i, r_j) = f_{i,j}$ and the results from part (b) we get \hfill[3]
\begin{align*}
\frac{u_{i,j+1} - 2u_{i,j} + u_{i,j-1}}{(\Delta x)^2} + \frac{u_{i+1,j} - 2u_{i,j} + u_{i-1,j}}{(\Delta y)^2} = f_{i,j}
\end{align*}

Multiply this with $(\Delta x)^2$ and rearrange for $u_{i,j}$ to get
\begin{align*}
u_{i,j} \left(2 + 2\frac{(\Delta x)^2}{(\Delta y)^2}\right) =& u_{i,j+1} + u_{i,j-1} + \frac{(\Delta x)^2}{(\Delta y)^2} (u_{i+1,j} + u_{i-1,j}) - (\Delta x)^2 f_{i,j}
\end{align*}
Multiply both sides of the equation with the factor $Ch$ (from part (b)) and bring it into the form required for the SOR method \hfill[3]
\begin{align*}
u_{i,j}^{k+1}  = (1-\omega)u_{i,j}^{k} &+ \omega Ch \left[u_{i,j+1}^{k} + u_{i,j-1}^{k+1} + \beta^2 (u_{i+1,j}^{k} + u_{i-1,j}^{k+1}) - (\Delta x)^2 f_{i,j}\right]
\end{align*}
where we have used $\beta = \frac{\Delta x}{\Delta y}$ and with $\omega$ the relaxation factor. The superscript $k$ indicates the iteration number of the iterative SOR method.

We run the SOR method with the same tolerance of $10^{-6}$ as used in part (b). This was deemed to be sufficient. In addition, the boundary conditions of the problem are the same as for part (b) so the only changes in the Matlab script are required for the internal values of $x$ and $y$. 
For the internal values of $x$ and $y$, we need to add the following term to the right hand side of the update equation for $u_{i,j}^{k+1}$ \hfill[3]
\[
-\omega Ch(\Delta x)^2 (b + y_i)y_i (a - x_j)x_j (1 - \textit{sign}(y_i))0.5
\]
where $\omega$ is the relaxation factor and $\textit{sign}(y)$ returns the sign of $y$, i.e. $\textit{sign}(0.5) = 1$ and $\textit{sign}(-0.7) = -1$. Alternative formulations with the Heaviside unit step function or if loops are possible.

The following plots show the contour and surface plots for the numerical solution on a 40 by 40 grid.\hfill[3]

\includegraphics[width=0.5\columnwidth]{Cw2019_C1_inhomogeneous_contour}
\includegraphics[width=0.5\columnwidth]{Cw2019_C1_inhomogeneous}

In comparison to the homogeneous system we have a negative forcing term for negative values of $y$. This term leads to an increased solution value for positive and negative values of $y$ compared to the homogeneous solution as seen in the plots. \hfill[3]

\includegraphics[width=0.5\columnwidth]{Cw2019_C1_Q1c_difference_contour}
\includegraphics[width=0.5\columnwidth]{Cw2019_C1_Q1c_difference}

The Matlab script RefinementAnalysis.m from the numerical lectures is used to perform the grid convergence analysis. The script is run for five grid sizes, i.e. 10x10, 20x20, 40x40, 80x80 and 160x160. The grid convergence analysis gives an order of convergence of $p=1.00$ which is similar to the homogeneous case. The grid convergence ratio is $1.03$ which is close to $1$ on the three middle grids which indicates that the solution is grid independent for a 20x20 grid.\hfill[3]

\textbf{Remark:} Different values for the order of convergence and grid convergence can be reached for different spatial discretisations of the first spatial derivative and the Neumann boundary condition.

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[2]
\end{solution}

\end{parts}

\end{questions}

%-------------------------------------------------------%
\newpage
{\large\textbf{Formula sheet}}

\begin{enumerate}
\item Useful integrals
\begin{align*}
\int x \sin ax \, \mathrm{d}x &= \frac{1}{a^2} \bigl ( \sin a x - a x \cos ax \bigr ) \\
\int x \cos ax \, \mathrm{d}x &= \frac{1}{a^2} \bigl ( \cos a x + a x \sin ax \bigr ) \\
\int x^2 \sin ax \, \mathrm{d}x &= \frac{2x}{a^2} \sin ax - \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \cos ax \\
\int x^2 \cos ax \, \mathrm{d}x &= \frac{2x}{a^2} \cos ax + \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \sin ax
\end{align*}
\item Trigonometric identities
\begin{align*}
\sin(a\pm b) &= \sin(a) \cos(b) \pm \cos(a) \sin(b) \\
\cos(a\pm b) &= \cos(a) \cos(b) \mp \sin(a) \sin(b) \\
2\sin(a) \cos(b) &= \sin(a+b) + \sin(a-b) \\
2\sin(a) \sin(b) &= \cos(a+b) - \cos(a-b) \\
2\cos(a) \cos(b) &= \cos(a+b) + \cos(a-b)
\end{align*}
\end{enumerate}

\end{document}
