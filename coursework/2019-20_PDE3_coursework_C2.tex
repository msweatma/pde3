 % Add 'answers' to options to get the solutions
\documentclass[addpoints,answers,12pt]{exam}	

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Coursework 2}
\footer{}{Page \thepage\ of \numpages}{\iflastpage{\textbf{END}}{\textbf{Continued}}}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_coursework_style}
\usepackage[ruled,vlined]{algorithm2e}

\pointsdroppedatright 

%-------------------------------------------------------%
\begin{document}

The following coursework is to be handed in to the online dropbox by \textbf{16:00 on Tuesday the 19$^{th}$ of November 2019}. This is \textbf{coursework number 2 of 2}. Each coursework is worth $20\%$ of the final mark.
\vskip0.1cm
You should submit the solution to this coursework in the form of a short report on Learn. This report must be typed, with at least 2cm margins, a font size of 11pt and line spacing of at least 1, and must not be longer than 7 pages. The title page does not count towards the page limit.

The marks for this coursework will be awarded for the correctness of the solution as well as for the clarity and logical progression of the solution. Details on laying out mathematical calculations and general guidelines for the coursework are given on Learn. 

%----- Question ----------------------------------------%
% This is the start of the questions
\begin{questions}
\question Consider the one-dimensional heat conduction equation
\begin{align}
\frac{\partial u(x,t)}{\partial t} &= \kappa \frac{\partial^2 u(x,t)}{\partial x^2} \tag{a} \\
\intertext{on the interval $0<x<1$ for the initial condition}
u(x,0) &= (1-x)^2, \quad 0\le x< 1, \tag{b} \\
\intertext{and the boundary conditions} 
 u(0, t)   &= 1, \quad \forall t, \tag{c} \\
 \frac{\partial u(1, t)}{\partial x} &= 0, \quad \forall t. \tag{d} 
\end{align}
\begin{parts}
\part[6] Describe the problem which is given by the one-dimensional heat conduction equation and the initial and boundary conditions, Eqs.~(a-d). Without solving the heat equation give the steady-state solution $U$, i.e. the solution for $t\to\infty$.
\droppoints

\begin{solution}
The one-dimensional heat equation describes the heat conduction along the length of a beam of length 1. The beam is held at a given temperature at the left end and has no heat loss at the right end, i.e. perfect insulation. The beam has a given initial temperature profile. \hfill[2]

The left boundary is kept at $1$ and there is no heat loss at the right boundary, thus the function
\[
U(x) = 1
\]
fulfils conditions (c) and (d) and also $\nabla^2 U = 0$; thus is the steady state solution.  \hfill[4]
\end{solution}

\part[6] Use the steady-state solution to reformulate the problem given by Eqs.~(a-d) into a problem for the transient solution $v$. \\
Remark: $u = U + v$
\droppoints

\begin{solution}
The steady-state solution is
\[
U(x) = 1
\]
and thus the initial and boundary conditions can be calculated from the relationship
\[
v(x,t) = u(x,t) - 1
\]

With this relationship the initial condition for the transient problem is given by
\begin{align}
v(x,0) &= u(x,0) - U(x) = 1 - 2x + x^2 - 1 = x^2 - 2x, \quad 0\le x< 1, \tag{e}\\
\intertext{and the boundary conditions are given by} 
 v(0, t) &= u(0,t) - U(0) =  0, \quad \forall t. \tag{f} \\
 \frac{\partial v(1, t)}{\partial x} &= \frac{\partial u(1, t)}{\partial x} - \frac{d U(1)}{dx} = 0, \quad \forall t, \tag{g}
\end{align}
Two marks for each condition with correct $x$ and $t$ limits. \hfill[6]
\end{solution}

\part[20] Use separation of variables to obtain the transient solution $v(x,t)$ of the heat conduction equation. Find the general solution of the problem given by Eqs.~(a-d).
\droppoints

\begin{solution}
We seek a solution of the form
\begin{align*}
v(x,t) &= e^{-\alpha t} \left(A\sin(\lambda x) + B \cos(\lambda x)\right)
\end{align*}
subject to the initial and boundary conditions~(e-g) and with $\alpha=\kappa \lambda^2$. The parameters $A$ and $B$ need to be found to fulfil these conditions. \hfill[2]

The first boundary condition (f), $v(0, t)=0$, requires that
\begin{align*}
A\sin(\lambda 0) + B\cos(\lambda 0) \overset{!}{=} 0
\end{align*}
which is only fulfilled if $B=0$. \hfill[2]

To fulfil the second boundary condition (g), $\frac{\partial v(1, t)}{\partial x} = 0$, it is required that
\begin{align*}
\cos(\lambda) &= 0 \\
\intertext{which is fulfilled for}
\lambda_n &= (n-0.5)\pi, \quad n=1,2,\dots
\end{align*}
\hfill[4]

Thus we get solutions of the form
\[
v_n(x,t) = e^{-\kappa \lambda_n^2 t} A_n\sin((n-0.5)\pi x), \quad n=1,2,\dots
\]
where we have used $\alpha_n = \kappa \lambda_n^2$. To fulfil the initial condition (e) we can superimpose these solutions 
\[
v(x,t) = \sum_{n=1}^\infty e^{-\kappa \lambda_n^2 t} A_n\sin((n-0.5)\pi x)
\]
and require that \hfill[4]
\[
\sum_{n=1}^\infty A_n\sin((n-0.5)\pi x) \overset{!}{=} x^2 - 2x
\]

Using the Fourier sine series expansion over the interval $0\le x \le 1$, we get two integrals which can be solved using the list of integrals at the end of the exam paper \hfill[4]
\begin{align*}
A_n &= \frac{2}{1} \int_0^1 (x^2 - 2x) \sin((n-0.5)\pi x) dx \\
     &= 2\int_0^1 x^2 \sin((n-0.5)\pi x) dx - 4\int_0^1 x \sin((n-0.5)\pi x) dx \\
     &= 2\left[\frac{2x}{\lambda_n^2}\sin(\lambda_n x) - \left(\frac{x^2}{\lambda_n} - \frac{2}{\lambda_n^3}\right)\cos(\lambda_n x) \right]_0^1 - \frac{4}{\lambda_n^2} \left[\sin(\lambda_n x) - \lambda_n x \cos(\lambda_n x)\right]_0^1 \\
 &= 2 \left[\frac{2}{\lambda_n^2}(-1)^{n+1} - \frac{2}{\lambda_n^3}\right] - \frac{4}{\lambda_n^2} (-1)^{n+1} \\
 &= -\frac{4}{\lambda_n^3}
\end{align*}

Thus the transient and general solutions are \hfill[4]
\begin{align*}
 v(x,t) &= -\sum_{n=1}^\infty e^{-\kappa \lambda_n^2 t} \frac{4}{\lambda_n^3}\sin(\lambda_n x) \\
 u(x,t) &= 1 - \sum_{n=1}^\infty e^{-\kappa \lambda_n^2 t} \frac{4}{\lambda_n^3}\sin(\lambda_n x)
\end{align*}

\end{solution}

\part[8] Assume that the $x$ direction is the radial direction of a circle and that the solution $u(x,t)$ from above is a radially scaled temperature
\[
u(x,t) = x T(x,t)
\]
State the problem, i.e. partial differential equation, initial and boundary conditions, for which $T(x,t)$ is a solution. What problems arise for this case?
\droppoints

\begin{solution}
When $u(x,t)$ is a solution of the standard one-dimensional heat conduction equation the scaled temperature 
\[
T(x,t) = \frac{u(x,t)}{x}
\]
is a solution of the spherically symmetric heat conduction equation \hfill[2]
\[
\pdderiv{T}{x} + \frac{2}{x}\pderiv{T}{x} = \frac1\kappa\pderiv{T}{x}
\]

The initial and boundary conditions are \hfill[4]
\begin{align*}
T(x,0) &= \frac{(1-x)^2}{x}, \quad 0< x< 1, \\
T(0, t) &= \frac{1}{0} \to \infty, \quad \forall t, \\
\left.\pderiv{u}{x}\right|_{x=1} = T(1,t) + \frac{\partial T(1, t)}{\partial x} &= 0, \quad \forall t. 
\end{align*}

The initial and boundary conditions for $x=0$ go to infinity. \hfill[2]

\end{solution} 
\end{parts}
%-------------------------------------------------------%

%-------------------------------------------------------%
\question[30] The \textsc{Matlab} program contained in the file \texttt{ADI.m} was written to solve the 2D transient ground water equation using the Alternating Direction Implicit (ADI) method on an $N_I\times N_J$ grid $100\text{ m}\times 100\text{ m}$.   Modify \texttt{function ADI} so that it solves the two dimensional heat conduction equation:

\begin{align}
\frac{\partial u(x,y,t)}{\partial t} &= \kappa \left(\frac{\partial^2 u(x,y,t)}{\partial x^2}+\frac{\partial^2 u(x,y,t)}{\partial y^2}\right) \tag{a}
\end{align}
on the interval $0<x<1,\, -0.5<y<0.5$ with the initial condition
\begin{align}
u(x,y,t=0) &= (1-x)^2(-8y^3\cos\left(\frac{\pi}{10} t\right)-4y^2+1) \tag{b} 
\end{align}
and the boundary conditions
\begin{align}
u(0,y,t) &= -8y^3\cos\left(\frac{\pi}{10} t\right)-4y^2+1, \quad &\forall t\ge0, -0.5<y<0.5, \tag{c} \\
\frac{\partial u(1,y, t)}{\partial x} &= 0, \quad &\forall t\ge0, -0.5<y<0.5, \tag{d} \\
u(x,-0.5,t) &= (1-x)^2\cos\left(\frac{\pi}{10} t\right), \quad &\forall t\ge0, 0<x<1,\tag{e} \\
u(x,0.5, t) &= -(1-x)^2\cos\left(\frac{\pi}{10} t\right), \quad &\forall t\ge0, 0<x<1. \tag{f} 
\end{align}

Find the solution at $t=20$s.  By comparing a grid converged solution (at y=0) with the analytical solution from part (a) comment on the accuracy of the method and the computational effort needed.
\droppoints
\begin{solution}
The driver \texttt{ADI.m} needs to be modified so that domain is the correct size and the initial conditions are set to

\begin{verbatim}
function ADI(NI,NJ)
CourantNo=0.95; % Set the Courant number
kappa=1; % set the heat transfer coefficient

% derive the grid size and line gradient
dx=1/NI; dy=1/NJ;

[X,Y]=meshgrid(0:dx:1,-0.5:dy:0.5); % setup the grid

% initialise the u array to the initial conditions
U=(1-X).^2.*(-8*Y.^3-4*Y.^2+1);

tstop=20.0;
U=ADIsolver(U,X,Y,dx,dy,tstop);
\end{verbatim}

Marks as follows:
\begin{itemize}
    \item Changing $\beta$ for $\kappa$ as we are solving the heat condition equation and not the groundwater equation.\hfill[2]
    \item setting the grid so that \(x\in[0,1]\) and \(y\in[-0.5,0.5]\).\hfill[2]
    \item Setting the initial conditions correctly so
    \[u(x,y,0) = (1-x)^2(-8y^3-4y^2+1).\]
    \hfill[2]
    \item setting the correct stop time.\hfill[2]
\end{itemize}

The next set of changes that are needed affect the boundary conditions 
\begin{verbatim}
function u=boundaryconditions(u,X,Y,t)
% this function needs to be modified so that X and Y are passed
% in as the Dirichlet boundary conditions depend
% on the coordinates.
    [m,n]=size(u);
    u(:,1)=-8*Y(:,1).^3*cos(pi*t/10)-4*Y(:,1).^2+1;
    u(1,:)=(1-X(1,:)).^2*cos(pi*t/10);
    u(m,:)=-u(1,:);
    u(:,n)=u(:,n-2); % Neumann BC
\end{verbatim}
Marks as follows:
\begin{itemize}
\item setting the Dirichlet BC on $x=0$ \[u(0,y,t) = -8y^3\cos\left(\frac{\pi}{10} t\right)-4y^2+1, \quad \forall t\ge0, -0.5<y<0.5\]
    \hfill[2]
\item setting the Dirichlet BC on $y=-0.5$ \[u(x,-0.5,t) = (1-x)^2\cos\left(\frac{\pi}{10} t\right), \quad \forall t\ge0, 0<x<1\]
    \hfill[2]
\item setting the Dirichlet BC on $y=0.5$ \[u(x,-0.5,t) = -(1-x)^2\cos\left(\frac{\pi}{10} t\right), \quad \forall t\ge0, 0<x<1\]
    \hfill[2]
\item setting the Neumann BC on $x=1$ \[u_x(1,y)=0.\]
    \hfill[2]
\end{itemize}

A mesh refinement test should be performed using an appropriate metric, a grid of at least $40\times40$ should be used for the final solution.  This will take about a minute to run, assuming a Courant number of $\nu=0.5$.  
\begin{verbatim}
>> ADI(40, 40)
ADI method for SCEE09004 coursework 2, 2019
ADI with dt=2.969e-04 (67369 iterations)
###################################################################
Done. ADI solver took 72.660 seconds (67369 iterations).
\end{verbatim}
The $80\times80$ solution takes much longer! (635 seconds).

There are 9 marks for presenting the results as follows:
\begin{itemize}
    \item Mesh refinement study showing a grid converged solution.\hfill[4]
    \item 2D contour plot of the converged solution (with labels, caption, etc.)\hfill[3]
    
    \centerline{\includegraphics[width=0.8\columnwidth]{Cw2019_C2_ADI-2D.pdf}}
    \item Information on the run time of the solution.\hfill[2]
\end{itemize}

Comparison with the 1D analytical solution from Q1. Five marks are available. For making the following points:
\begin{itemize}
    \item Plot comparing solutions, as follows showing numerical solution as circles and analytical solution as a solid line. \hfill[2]
    \centerline{\includegraphics[width=0.8\columnwidth]{Cw2019_C2_ADI-centerline.pdf}}
    \item In the numerical solution the boundary conditions at $x=1,\, y=\pm0.5$ have opposite signs.  This means that the steady state solution at $x=1,\, y=0$ must be zero.\hfill[1]
    \item In the analytical solution the symmetry boundary condition at $x=1$ means the steady state solution must be one.\hfill[1]
    \item The 2D and 1D problems are \textbf{not} comparable, and the 1D solution \textbf{cannot} be used to validate the 2D solution.  \hfill[1]
    
    This could be done by changing the boundary condition in the 1D problem to $u(1)=0.0$ and recomputing the analytical solution.
\end{itemize}
\end{solution}

%-------------------------------------------------------%
\question[30] Write a \textsc{Matlab} program which also solves the 2D transient heat conduction problem in question 2 but uses the 2D Crank-Nicholson method with the preconditioned bi-conjugate gradient matrix solver to solve on the penta-diagonal sparse matrix system. 
The \texttt{bicg()} function was described in lecture 4 and the coefficients for the penta-digonal matrix were discussed in lecture 6. 
Once again find the solution at $t=20$s, and compare the required computational time and the accuracy of the method with the ADI scheme from question 2.
\droppoints
\begin{solution}
The 2D Crank-Nicholson Scheme is
\[
\frac{u^{n+1}_{ij}-u^{n}_{ij}}{\Delta t}=
\frac\kappa2\left(\hat{\delta}^2_x+\hat{\delta}^2_y\right).\left(u_{ij}^{n+1}+u_{ij}^n\right)
\]
where 
\begin{align*}
\hat{\delta}^2_x.u_{ij}^n&=\frac{u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}}{\Delta x^2}
=\frac{\delta_x^2u_{ij}^n}{\Delta x^2}\text{, and}\\
\hat{\delta}^2_y.u_{ij}^n&=\frac{u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}}{\Delta y^2}
=\frac{\delta_y^2u_{ij}^n}{\Delta y^2}.
\end{align*}\hfill[2]

This solution is first order in time and 2nd order in space.  

We need to use a linear algebra solver to solve for the unknown values of $u^{n+1}_{ij}$, so we need to determine the coefficient matrix, $A$, and the RHS vector $\vec{b}$.   Expanding the definition above we get 
\begin{align*}
\frac{u^{n+1}_{ij}-u^{n}_{ij}}{\Delta t}&=
\kappa\frac{u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}}{2\Delta x^2}
+\kappa\frac{u_{i-1j}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{i+1j}}{2\Delta x^2}\\
&+\kappa\frac{u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}}{2\Delta y^2}
+\kappa\frac{u_{ij-1}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{ij+1}}{2\Delta y^2}
\end{align*} \hfill[2]

multiplying b.s by $\Delta t$
\begin{align*}
u^{n+1}_{ij}-u^{n}_{ij}&=
\frac{\kappa\Delta t}{2\Delta x^2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}
+u_{i-1j}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{i+1j}\right)\\
&+\frac{\kappa\Delta t}{2\Delta y^2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}
+u_{ij-1}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{ij+1}\right).
\end{align*}
putting $u^{n+1}$ terms on the LHS and $u^n$ terms on the RHS,
\begin{align*}
u^{n+1}_{ij}&
-\frac{\kappa\Delta t}{2\Delta x^2}\left(u_{i-1j}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{i+1j}\right)
-\frac{\kappa\Delta t}{2\Delta y^2}\left(u_{ij-1}^{n+1}-2u^{n+1}_{ij}+u^{n+1}_{ij+1}\right)\\
&=u^{n}_{ij}+
\frac{\kappa\Delta t}{2\Delta x^2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}\right)
+\frac{\kappa\Delta t}{2\Delta y^2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}\right).
\end{align*}
Writing as a linear equation, we get
\[a\cdot u^{n+1}_{ij-1}+b\cdot u^{n+1}_{i-1j}+c^{n+1}_{ij}+b\cdot u^{n+1}_{i+1j}+a\cdot u^{n+1}_{ij+1}=d^{ij}\]
where 
\begin{align*}
a&=-\frac{\kappa\Delta t}{2\Delta y^2}=-\frac{r_y}{2}, \\
b&=-\frac{\kappa\Delta t}{2\Delta x^2}=-\frac{r_x}{2}, \\
c&=1+2\frac{\kappa\Delta t}{2\Delta x^2}+2\frac{\kappa\Delta t}{2\Delta y^2}=1+r_x+r_y 
\end{align*}
and
\begin{align*}
d_{ij}&=u^{n}_{ij}+
\frac{\kappa\Delta t}{2\Delta x^2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}\right)
+\frac{\kappa\Delta t}{2\Delta y^2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}\right)\\
&=u^{n}_{ij}+
\frac{r_x}{2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}\right)
+\frac{r_y}{2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}\right).\\
\end{align*}
where $r_x=-\frac{\kappa\Delta t}{\Delta x^2}$ and $r_y=-\frac{\kappa\Delta t}{\Delta y^2}$. \hfill[4]

Using the ghost-cell method where the boundary conditions are stored in the halo cells $i=1$, $i=ni$, $j=1$ and $j=nj$ we must modify $d_{ij}$ when $i=2$, $j=2$, $i=ni-1$ and $j=nj-1$ since these values are not unknown.  In such cases:
\begin{align*}
    d_{ij}=d_{ij}-a\cdot u_{i\pm1j}&\text { if }i=1\text{ or }i=ni-1.\\
    d_{ij}=d_{ij}-b\cdot u_{ij\pm1}&\text { if }j=1\text{ or }i=nj-1.
\end{align*}\hfill[2]

The solution algorithm given in Algorithm \ref{a:CN}.  The routines for mapping the coordinates, computing the LU factorisation and solving the system can be adapted from the elliptic solver in the lecture.  

15 Marks are available as follows:
\begin{itemize}
    \item Using a Courant number $\nu>1$\hfill[1]
    \item Assembling the $A$ matrix \hfill[2]
    \item Computing $\b(k)$ correctly \hfill[2]
    \item Mapping the 2D solution onto a 1D vector \hfill [2]
    \item Updating the boundary conditions correctly \hfill[2]
    \item Applying the new boundary conditions only to the $b_k$ values associated with the new time level.\hfill[2]
    \item Solving the linear system using the bi-conjugate gradient solver \texttt{bicg}\hfill[1]
    \item Using the LU preconditioner \hfill[1]
    \item Providing an initial guess $x_0$ to accelerator the solver \hfill[2]
\end{itemize}

\begin{algorithm}[H]
\SetAlgoLined
\KwResult{\(u_{ij}(t_1)\)} 
 Calculate the time step, $\delta t=\nu\min\left(\frac{\Delta x}{2\kappa},\frac{\Delta y}{2\kappa}\right)$\;
 Assemble the $A$ matrix with coefficients $a=-\frac{r_y}{2},\, 
 b=-\frac{r_x}{2},\, c=1+r_x+r_y$ and $d=u^{n}_{ij}+
\frac{r_x}{2}\left(u_{i-1j}^n-2u^n_{ij}+u^n_{i+1j}\right)
+\frac{r_y}{2}\left(u_{ij-1}^n-2u^n_{ij}+u^n_{ij+1}\right)$\;
 $t=0$\;
 \While{$t<t_1$}{
   \(dt=\min(dt,t_1-t)\)\;
   Calculate $\vec{b}$\;
   Map \(u_{ij}(t)\) onto \(\vec{x_0}\)\;
   \For{\(j=2:nj-1\)}{
      \For{\(j=2:nj-1\)}{
        \(k=(i-1)+(j-2)(ni-2)\)\;
        \(b(k)=u(i,j)+\frac{r_x}{2}(u_{i-1j}-2u_{ij}+u_{i+1j})
            +\frac{r_y}{2}(u_{ij-1}-2u_{ij}+u_{ij+1})\)\;
            }
        }
    Update boundary conditions to $t=t+\delta t$\;
    Update $\vec{b}$ for grid points on the boundary.\;
    Compute the incomplete LU factorisation\;
    Solve for $A\vec{x}=\vec{b}$ using the bi-conjugate gradient solver with $L$, $U$ and an initial guess $\vec{x_0}$\;
    Map \(\vec{x}\) onto \(u_{ij}(t+\delta t)\)\;
    \(t=t+\delta t\)\;
    }
 \caption{2D Crank-Nicholson method}
 \label{a:CN}
\end{algorithm}

The new solver runs in about half the time of the original solver using a courant number,$\nu=5$ and much less for higher Courant numbers.  We need to be careful as a larger Courant number will provide an inaccurate solution as the scheme is only 1st order in time. Furthermore $\delta t$ must be small enough that variation in the boundary conditions is captured, using the Nyquist frequency we know that $dt\le\frac{\pi}{5}\approx0.6$ seconds. 

\(\nu=5\)
\begin{verbatim}
>> CrankNicholson(40, 40)
Crank-Nicholson with dt=1.563e-03 (12800 iterations)
#########################
Done. Crank-Nicholson solver took 14.900 seconds.
\end{verbatim}
\(\nu=10\)
\begin{verbatim}
>> CrankNicholson(40, 40)
Crank-Nicholson with dt=3.125e-03 (6400 iterations)
############
Done. Crank-Nicholson solver took 9.950 seconds.
\end{verbatim}
\(\nu=20\)
\begin{verbatim}
>> CrankNicholson(40, 40)
Crank-Nicholson with dt=9.375e-03 (2134 iterations)
####
Done. Crank-Nicholson solver took 5.230 seconds.
\end{verbatim}

The solution is very similar to that from Q2.  

  \centerline{\includegraphics[width=0.45\columnwidth]{Cw2019_C2_ADI-2D.pdf}
  \includegraphics[width=0.45\columnwidth]{Cw2019_C2_CN-2D.pdf}}
  \centerline{\includegraphics[width=0.45\columnwidth]{Cw2019_C2_ADI-centerline.pdf}
  \includegraphics[width=0.45\columnwidth]{Cw2019_C2_CN-centerline.pdf}}
  \centerline{\hfill ADI \hfill \hfill Crank-Nicholson, $\nu=20$\hfill}

Five marks are available for the comparison:
\begin{itemize}
    \item Choosing $\nu$ so that $\delta t<0.6$ \hfill[1]
    \item Comment on the time to solution using the method \hfill[1]
    \item Plots comparing ADI and CN solutions \hfill[3]
\end{itemize}
\end{solution}

\end{questions}


%-------------------------------------------------------%
%\newpage
%{\large\textbf{Formula sheet}}
%\begin{itemize}
%\item The differential equation
%\[
%\frac{\text{d}^2 u(x)}{\text{d}x^2} - \sigma^2 u(x) = 0
%\]
%has the equivalent solutions
%\begin{align*}
%u_1(x) &= A e^{\sigma x} + B e^{-\sigma x} \\
%u_2(x) &= \tilde{A~} \sinh(\sigma x) + \tilde{B} \sinh(\sigma(l-x))
%\end{align*}
%Depending on the problem one or the other leads to an easier solution.
%\item The Laplace domain function
%\[
%F(s) = \frac{\sinh(b\sqrt{s})}{s\sinh(a\sqrt{s})}
%\]
%has the inverse transform
%\begin{align*}
%f(t) =& \frac{b}{a} + \frac{2}{\pi} \sum_{n=1}^\infty \frac{(-1)^n}{n} e^{-n^2\pi^2 t/a^2} \sin\left(\frac{n\pi b}{a}\right)
%\end{align*}
%
%\item The Laplace domain function
%\[
%F(s) = \frac{\sinh(b\sqrt{s})}{s^2\sinh(a\sqrt{s})}
%\]
%has the inverse transform
%\begin{align*}
%f(t) =& \frac{bt}{a} + \frac{2a^2}{\pi^3} \sum_{n=1}^\infty \frac{(-1)^n}{n^3}\left(1 - e^{-n^2\pi^2 t/a^2}\right) \sin\left(\frac{n\pi b}{a}\right)
%\end{align*}
%\end{itemize}
%
%\vskip0.1cm
%
%\newpage
%%----- Table -------------------------------------------------------%
%\begin{table}[ht]
%\caption{Properties of the unilateral Laplace transform} % title of Table
%\centering % used for centering table
%\begin{tabular}{|c|c|c|} % centered columns (4 columns)
%\hline %inserts double horizontal lines
%Property & $f(t), t\ge0$ & $F(s)$ \\ 
%%heading
%\hline 
%Definition & $f(t)$ & $\int_0^\infty\exp(-st)f(t)dt$ \\ [0.3ex]
%Inversion & $\frac{1}{j2\pi}\int_{c-j\infty}^{c+j\infty} \exp(st)F(s)ds$ & $F(s)$ \\ [0.3ex]
%Linearity & $\alpha f(t) + \beta g(t)$ & $\alpha F(s) + \beta G(s)$ \\ [0.3ex]
%Scaling & $f(\alpha t)$ & $\frac{1}{\alpha}F\left(\frac{s}{\alpha}\right)$ \\ [0.3ex]
%Frequency shifting & $\exp(\alpha t)f(t)$ & $F(s-\alpha)$ \\ [0.3ex]
%Time shifting & $f(t-\alpha) H(t-\alpha)$ & $\exp(-\alpha s)F(s)$ \\ [0.3ex]
%First derivative & $f'(t)$ & $sF(s) - f(0)$ \\ [0.3ex]
%nth derivative & $f^{(n)}(t)$ & $s^nF(s)-s^{n-1}f(0)-s^{n-2}f'(0)$ \\
%      & & $-\dots-s f^{(n-2)}(0) - f^{(n-1)}(0)$  \\ [0.3ex]
%Transform derivative & $t^n f(t)$ & $(-1)^n \frac{d^n F(s)}{ds^n}$ \\ [0.3ex]        
%Integration & $\int_0^t f(\tau)d\tau$ & $\frac{1}{s}F(s)$  \\ [0.3ex]
%\hline
%\end{tabular}
%\label{table:properties} % is used to refer this table in the text
%\end{table}
%
%%----- Table -------------------------------------------------------%
%\begin{table}[ht]
%\caption{Function transforms} % title of Table
%\centering % used for centering table
%\begin{tabular}{|c|c|c|c|} 
%\hline %inserts double horizontal lines
%Name & $f(t), t\ge0$ & $F(s)$ & Convergence region \\ [0.3ex] % inserts table
%%heading
%\hline % inserts single horizontal line
%Unit impulse & $\delta(t)$ & $1$ & all $s$ \\ [0.3ex]
%Ideal delay& $\delta(t-\alpha)$ & $\exp(-\alpha s)$ & $\Re(s)\ge\alpha$ \\ [0.3ex]
%Unit step & $H(t)$ & $\frac{1}{s}$ & $\Re(s)>0$ \\ [0.3ex]
%Ramp  & $t$ & $\frac{1}{s^2}$ & $\Re(s)>0$ \\ [0.3ex]
%nth power  & $t^n, n=1,2,\dots$ & $\frac{n!}{s^{n+1}}$ & $\Re(s)>0$ \\ [0.3ex]
%Sine & $\sin(\alpha t)$ & $\frac{\alpha}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
%Cosine & $\cos(\alpha t)$ & $\frac{s}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
%Hyperbolic sine & $\sinh(\alpha t)$ & $\frac{\alpha}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
%Hyperbolic cosine & $\cosh(\alpha t)$ & $\frac{s}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
%Exponential decay & $\exp(-\alpha t)$ & $\frac{1}{s + \alpha}$ & $\Re(s)>-\alpha$ \\ [0.3ex]
%\hline %inserts single line
%\end{tabular}
%\label{table:functions}
%\end{table}

\end{document}
