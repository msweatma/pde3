 % Add 'answers' to options to get the solutions
\documentclass[answers,addpoints,12pt]{exam}	

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Coursework 1}
\footer{}{Page \thepage\ of \numpages}{\iflastpage{\textbf{END}}{\textbf{Continued}}}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_coursework_style}

%-------------------------------------------------------%
%-------------------------------------------------------%
\begin{document}

The following coursework is to be handed in to the ETO dropbox by \textbf{16:00 on Tuesday the 23$^{th}$ of October 2018}. This is \textbf{coursework number 1 of 2}. Each coursework is worth $20\%$ of the final mark.
\vskip0.1cm
% ---- Modify
You should submit the solution to this coursework in the form of a short report on Learn. This report must not be longer than 7 pages with at least 2cm margins and a font size of 11pt. The marks for this coursework will be awarded for the correctness of the solution as well as for the clarity and logical progression of the solution. Details on laying out mathematical calculations and general guidelines for the coursework are given on Learn. 

\vskip0.1cm

%----- Question ----------------------------------------%
% This is the start of the questions
\begin{questions}
\question 

Consider the Laplace equation on the rectangle defined by $a\le x\le 2a$ and $-b\le y\le b$ where $a$ and $b$ are positive numbers.

The upper and lower boundaries of the rectangle are held at $0$. The left boundary is insulating and the right boundary is given by
\[
\pderiv{u}{x}(2a, y) = 1 - y^2
\]

\begin{parts}
%-------------------------------------------------------%
\part[40] Calculate the analytical solution of the Laplace equation on the rectangle with the given boundary conditions.

\begin{solution}
The Laplace equation in Cartesian coordinates is given by
\begin{align*}
\pdderiv{u}{x} + \pdderiv{u}{y} = 0
\end{align*}
and we are looking for separated solutions of the form $u(x,y) = X(x) Y(y)$. These give us two ordinary differential equations and the solutions of these equations will depend on the value of $\lambda$. We get three trial solutions
\begin{align*}
u_1(x,y) =& (A\sin \mu x+B\cos \mu x)\left(Ce^{\mu y}+De^{-\mu y}\right)&\lambda=-\mu^2<0,\\
 =&(A\sin \mu x+B\cos \mu x)\left(\tilde{C}\cosh \mu y+\tilde{D}\sinh \mu y\right) & \\
u_2(x,y) =& \left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y)&\lambda=\mu^2>0,\\
 =& \left(\tilde{A}\cosh \mu x+\tilde{B}\sinh \mu x\right)(C\sin \mu y+ D\cos \mu y) & \\
u_3(x,y) =& (Ax+B)(Cy+D)&\lambda=0
\end{align*}
where $A, \,\tilde{A}, \, B, \,\tilde{B},\, C,\,\tilde{C},\,D$ and $\tilde{D}$ are arbitrary constants. We need to find the values of these constants so that the trial solutions fulfil the boundary conditions. 

To fulfil the boundary conditions, the solution at the upper and lower boundaries needs to be zero. This condition can be easily fulfilled by trigonometric solutions in the $y$ direction which suggests the use of $u_2$ \hfill[3]
\begin{align*}
u_2(x,y) =& \left(\tilde{A}\cosh(\mu x) + \tilde{B}\sinh(\mu x)\right)(C\sin(\mu y) + D\cos(\mu y))
\end{align*}

The flux is given by an even function in $y$ and thus the solution should only consist of cosine terms \hfill[3]
\begin{align*}
u_2(x,y) =& \left(A \cosh(\mu x) + B\sinh (\mu x)\right)\cos(\mu y))
\end{align*}
where $A=\tilde{A}D$ and $B=\tilde{B}D$.

Next, we consider the upper and lower boundary conditions where the solution is held at $0$ for all values of $x$. This condition is given by
\begin{align*}
u_2(x, \pm b) &= \left(A\cosh(\mu x) + B\sinh(\mu x)\right)\cos(\pm\mu b) \overset{!}{=} 0
\end{align*}
This can only be fulfilled if the cosine term is $0$ and thus it follows that \hfill[3]
\begin{align*}
\mu b &= (2n - 1) \frac{\pi}{2}, \quad n=1,2,\dots \\
\mu_n &= \frac{(2n-1)\pi}{2b}, \quad n=1,2,\dots 
\end{align*}
To keep the equations manageable we will use $\mu_n$ instead of the whole term.

To handle the boundary condition at $x=a$ we rewrite the solution in the equivalent form
\begin{align*}
u_2(x, y) &= \left(A_n\cosh(\mu_n(a - x)) + B_n\sinh(\mu_n(a - x))\right)\cos(\mu_n y) 
\end{align*}
where $A_n$ and $B_n$ are arbitrary constants. At the boundary $x=a$, we need to fulfil \hfill[3]
\begin{align*}
\pderiv{u_2}{x}(a, y) &= \left(-A_n\mu_n\sinh(\mu_n(a - a)) - B_n\mu_n\cosh(\mu_n(a - a))\right)\cos(\mu_n y) \overset{!}{=} 0
\end{align*}
from which it follows that $B_n$ needs to be zero and that the solution is
\begin{align*}
u_2(x, y) &= A_n\cosh(\mu_n(a - x))\cos(\mu_n y) 
\end{align*}

To fulfil the right boundary condition we need to superimpose the solutions for all $\mu_n$. This is possible because we are dealing with a linear PDE. The complete solution is \hfill[3]
\[
u(x, y ) = \sum_{n=1}^\infty A_n\cosh(\mu_n(a - x))\cos(\mu_n y) 
\]
To find the coefficients $A_n$ we need to calculate the derivative of $u$ with respect to $x$ and set it equal to the boundary condition. The derivative is given by 
\[
\pderiv{u}{x}(x, y ) = -\sum_{n=1}^\infty A_n \mu_n\sinh(\mu_n(a - x))\cos(\mu_n y) 
\]
and at the right boundary we need to have \hfill[3]
\begin{align*}
\pderiv{u}{x}(2a, y ) &= -\sum_{n=1}^\infty A_n \mu_n\sinh(\mu_n(a - 2a))\cos(\mu_n y) \overset{!}{=} 1 - y^2
\end{align*}

The parameters $A_n$ can be found with the usual method of the Fourier series, as follows
\begin{align*}
A_n &= \frac{1}{\mu_n \sinh(\mu_n a)} \frac{2}{2b} \int_{-b}^{b} (1-y^2) \cos(\mu_n y) dy = \frac{2}{b\mu_n \sinh(\mu_n a)} \int_{0}^{b} (1-y^2) \cos(\mu_n y) dy 
 \intertext{By using the formulas for integration of quadratic functions we get}
A_n &= \frac{2}{b\mu_n \sinh(\mu_n a)}\left[\frac{\sin(\mu_n y)}{\mu_n} - \frac{2y}{\mu_n^2}\cos(\mu_n y) - \left(\frac{y^2}{\mu_n} - \frac{2}{\mu_n^3}\right)\sin(\mu_n y)\right]_0^b \\
 \intertext{and we insert the boundary values to get}
 A_n &= \frac{2}{b\mu_n \sinh(\mu_n a)} \left[\sin(\mu_n b) \left(\frac{1 - b^2}{\mu_n} + \frac{2}{\mu_n^3}\right)\right]  \\ 
 \intertext{and finally simplify to get the expression for the parameters $A_n$ \hfill[3]}
A_n &= (-1)^{n+1}\frac{2}{b\mu_n \sinh(\mu_n a)} \left(\frac{1 - b^2}{\mu_n} + \frac{2}{\mu_n^3}\right)
\end{align*}

Thus the complete solution is \hfill[3]
\begin{align}
u(x, y) &= \sum_{n=1}^\infty (-1)^{n+1}\frac{2\cosh(\mu_n(a - x))}{b\mu_n \sinh(\mu_n a)} \left(\frac{1 - b^2}{\mu_n} + \frac{2}{\mu_n^3}\right)\cos(\mu_n y) 
\label{eq:analytical_solution}
\end{align}
which can be plotted as: \hfill[6]

\includegraphics[width=0.5\columnwidth]{Cw2018_C1_analytical_contour}
\includegraphics[width=0.5\columnwidth]{Cw2018_C1_analytical}

The Fourier series terms decrease as $1/n^2$ which suggests quadratic convergence. The integral error between the analytical solution~\ref{eq:analytical_solution} evaluated with 20 and 40 terms is around $10^{-14}$ which suggests that 20 terms of the series solution~\ref{eq:analytical_solution} are sufficient. \hfill[3]

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[7]
\end{solution}

%-------------------------------------------------------%
\part[35] From now on set $a=2$ and $b=1$. Develop a Matlab script to solve the problem numerically. Explain what you are doing, compare the solution against the analytical solution and perform a grid convergence analysis. Plot the solution. 

\vskip0.2cm
\textbf{Remarks:}
\begin{itemize}
\item While the Matlab scripts on Learn are a useful starting point, you will need to make some modifications.
\end{itemize}

\begin{solution}
To solve the Laplace equation with Matlab we need to discretise the partial differential equation. The derivatives can be discretised with the finite difference formulas for first and second derivatives from the lectures. 

We define the grid spacings \hfill[3]
\begin{align*}
x_j &= 1 + (j-1)\Delta x, \qquad & j=1,2,\dots,n_x \\
y_i &= (j-1)\Delta y, \qquad & i=1,2,\dots,n_y
\end{align*}
where $\Delta x$ and $\Delta y$ are the grid spacing in the $x$ and $y$ direction, respectively. At the grid point $(i,j)$ the position is given by $x_j$ and $y_i$, respectively, and we define $u(y_i, x_j) = u_{i,j}$. 

With the finite difference formulas we get 
\begin{align*}
\frac{u_{i,j+1} - 2u_{i,j} + u_{i,j-1}}{(\Delta x)^2} + \frac{u_{i+1,j} - 2u_{i,j} + u_{i-1,j}}{(\Delta y)^2} = 0
\end{align*}
Multiply this with $(\Delta x)^2$ and rearrange for $u_{i,j}$ to get
\begin{align*}
u_{i,j} \left(2 + 2\frac{(\Delta x)^2}{(\Delta y)^2}\right) =& u_{i,j+1} + u_{i,j-1} + \frac{(\Delta x)^2}{(\Delta y)^2} (u_{i+1,j} + u_{i-1,j}) 
\end{align*}
Multiply both sides of the equation with the factor
\[
Ch = \frac{1}{\left(2 + 2\frac{(\Delta x)^2}{(\Delta y)^2}\right)}
\]
and bring it into the form required for the SOR method 
\begin{align}
u_{i,j}^{k+1}  = (1-\omega)u_{i,j}^{k} &+ \omega Ch \left(u_{i,j+1}^{k} + u_{i,j-1}^{k} + \beta^2 (u_{i+1,j}^{k} + u_{i-1,j}^{k}) \right)
\label{eq:uij}
\end{align}
where $\beta = \frac{\Delta x}{\Delta y}$ and $\omega$ is the relaxation factor. The superscript $k$ indicates the iteration number of the iterative SOR method. The Eq.~\ref{eq:uij} for $u_{i,j}^{k+1}$ is added into the SOR method for all internal values of $x$ and $y$, i.e. all values of $x$ and $y$ except at the boundaries. \hfill[3]

At the upper and lower boundaries, i.e. for $y=\pm b$, we set the value of $u_{i,j}$ to zero \hfill[3]
\begin{align*}
u_{1,j} &= u_{n_y, j}= 0, \quad j=1,2,\dots, n_x
\end{align*}

At the left and right boundary the Neumann boundary conditions are discretised with the forward and backward difference formula, respectively, and rearranged to \hfill[3]
\begin{align*}
0 &= \left.\pderiv{u}{x}\right|_{i,1} = \frac{u_{i,2} - u_{i,1}}{\Delta x} \Rightarrow u_{i,1} = u_{i,2} \\
1 - y_{i}^2 &= \left.\pderiv{u}{x}\right|_{i,n_x} = \frac{u_{i,n_x} - u_{i,n_x-1}}{\Delta x} \Rightarrow u_{i,n_x} = u_{i,n_x} + \Delta x (1 - y_{i}^2) 
\end{align*}
for $i=1,2,\dots,n_y$.

We ran the developed SOR method with a tolerance of $10^{-6}$ and compared this to a run with a tighter tolerance of $10^{-8}$. This gave us a maximum difference of around $10^{-5}$. Thus, it was decided that the tolerance of $10^{-6}$ is sufficient and all further runs used this tolerance. \hfill[3]

The following plots show the contour and surface plots for the numerical solution on a 40 by 40 grid.\hfill[3]

\includegraphics[width=0.5\columnwidth]{Cw2018_C1_numerical_contour}
\includegraphics[width=0.5\columnwidth]{Cw2018_C1_numerical}

The plots look very similar to the analytical solutions in part (a) which is confirmed by the contour and surface plots of the difference between the numerical and analytical solution for a 40 by 40 grid and 20 terms in the analytical solution. These plots show that the maximum difference for this case is about $0.03$. \hfill[3]

\includegraphics[width=0.5\columnwidth]{Cw2018_C1_Q1b_difference_contour}
\includegraphics[width=0.5\columnwidth]{Cw2018_C1_Q1b_difference}

The Matlab script RefinementAnalysis.m from the numerical lectures is used to perform the grid convergence analysis. The script is run for five grid sizes, i.e. 10x10, 20x20, 40x40, 80x80 and 160x160. \hfill[3]

The order of convergence is given as $p=1.09$. This is smaller than two, i.e. the order of the central finite difference approximation of the second derivatives, which is expected because of the first order approximation of the first derivatives on the left and right boundaries. \hfill[3]

Once the correct order of convergence is found, the grid convergence analysis shows that for all tested grids the error is proportional to $\mathcal{O}\left(dx^p\right)$. For example, the grid convergence ratio is $0.986$ which is close to $1$ on the three coarsest grids which indicates that the solution is grid independent for a 10x10 grid. \hfill[3]

\textbf{Remark:} Different values for the order of convergence and grid convergence can be reached for different spatial discretisations of the first spatial derivative and the Neumann boundary condition.

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[5]
\end{solution}

%-------------------------------------------------------%
\part[25] The physical system is modified by a source term in the upper half of the rectangle. Over the entire rectangle the source term is given by
\[
f(x, y) = \begin{cases}
a^3(b - y)y, \quad & 0\le y < b, a\le x\le 2a \\
0, \quad & -b < y\le 0, a\le x\le 2a
\end{cases}
\]
Add the source term as an inhomogeneity to the Laplace equation and develop a Matlab script to solve the inhomogeneous problem numerically.

Explain what you are doing, compare the solution against the solution of the homogeneous system and perform a grid convergence analysis. Plot the solution.

\begin{solution}
In the derivation of the numerical formula in part (b) we need to replace the $0$ on the right hand side with the inhomogeneity. With the definition $f(\theta_i, r_j) = f_{i,j}$ and the results from part (b) we get \hfill[3]
\begin{align*}
\frac{u_{i,j+1} - 2u_{i,j} + u_{i,j-1}}{(\Delta x)^2} + \frac{u_{i+1,j} - 2u_{i,j} + u_{i-1,j}}{(\Delta y)^2} = f_{i,j}
\end{align*}

Multiply this with $(\Delta x)^2$ and rearrange for $u_{i,j}$ to get
\begin{align*}
u_{i,j} \left(2 + 2\frac{(\Delta x)^2}{(\Delta y)^2}\right) =& u_{i,j+1} + u_{i,j-1} + \frac{(\Delta x)^2}{(\Delta y)^2} (u_{i+1,j} + u_{i-1,j}) - (\Delta x)^2 f_{i,j}
\end{align*}
Multiply both sides of the equation with the factor $Ch$ (from part (b)) and bring it into the form required for the SOR method \hfill[3]
\begin{align*}
u_{i,j}^{k+1}  = (1-\omega)u_{i,j}^{k} &+ \omega Ch \left[u_{i,j+1}^{k} + u_{i,j-1}^{k} + \beta^2 (u_{i+1,j}^{k} + u_{i-1,j}^{k}) - (\Delta x)^2 f_{i,j}\right]
\end{align*}
where we have used $\beta = \frac{\Delta x}{\Delta y}$ and with $\omega$ the relaxation factor. The superscript $k$ indicates the iteration number of the iterative SOR method.

We run the SOR method with the same tolerance of $10^{-6}$ as used in part (b). This was deemed to be sufficient. In addition, the boundary conditions of the problem are the same as for part (b) so the only changes in the Matlab script are required for the internal values of $x$ and $y$.\hfill[3]

For the internal values of $x$ and $y$, we need to add the following term to the right hand side of the update equation for $u_{i,j}^{k+1}$ \hfill[3]
\[
-\omega Ch(\Delta x)^2 a^3(b - y_i)y_i (1 + \textit{sign}(y_i))0.5
\]
where $\omega$ is the relaxation factor and $\textit{sign}(y)$ returns the sign of $y$, i.e. $\textit{sign}(0.5) = 1$ and $\textit{sign}(-0.7) = -1$. Alternative formulations with the Heaviside unit step function or if loops are possible.

The following plots show the contour and surface plots for the numerical solution on a 40 by 40 grid.\hfill[3]

\includegraphics[width=0.5\columnwidth]{Cw2018_C1_inhomogeneous_contour}
\includegraphics[width=0.5\columnwidth]{Cw2018_C1_inhomogeneous}

In comparison to the homogeneous system we have a positive forcing term for positive values of $y$. This term leads to a reduced solution value for positive and negative values of $y$ compared to the homogeneous solution as seen in the plots. \hfill[3]

\includegraphics[width=0.5\columnwidth]{Cw2018_C1_Q1c_difference_contour}
\includegraphics[width=0.5\columnwidth]{Cw2018_C1_Q1c_difference}

The grid convergence analysis gives an order of convergence of $p=1.11$ which is similar to the homogeneous case. The grid convergence ratio is $1.01$ which is close to $1$ on the three finest grids which indicates that the solution is grid independent for a 40x40 grid.\hfill[3]

\textbf{Remark:} Different values for the order of convergence and grid convergence can be reached for different spatial discretisations of the first spatial derivative and the Neumann boundary condition.

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[4]
\end{solution}

\end{parts}


\end{questions}
\end{document}
