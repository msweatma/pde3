 % Add 'answers' to options to get the solutions
\documentclass[answers,addpoints,12pt]{exam}	

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Coursework 1}
\footer{}{Page \thepage\ of \numpages}{\iflastpage{\textbf{END}}{\textbf{Continued}}}

% Import PDE3 coursework style 
\usepackage{PDE3_coursework_style}

%-------------------------------------------------------%
%-------------------------------------------------------%
\begin{document}

The following coursework is to be handed in to the ETO online dropbox by \textbf{16:00 on Thursday the 11$^{th}$ of February 2021}. This is \textbf{coursework number 1 of 2}. Each coursework is worth $20\%$ of the final mark.
\vskip0.1cm
% ---- Modify
You should submit the solution to this coursework in the form of a short report on
Learn. This report must be typed, with at least 2cm margins, a font size of 11pt and line
spacing of at least 1, and must not be longer than 8 pages. The title page does not count
towards the page limit.

The marks for this coursework will be awarded for the correctness of the solution
as well as for the clarity and logical progression of the solution. Details on laying out
mathematical calculations and general guidelines for the coursework are given on Learn.

\vskip0.1cm

%----- Question ----------------------------------------%
% This is the start of the questions
\begin{questions}
\question[40] Consider a heat transfer pipe whose cross section is given by the following figure.
\centerline{\includegraphics[width=10cm]{Cw2020-21_C1_annulus.png}}

The temperature distribution in the blue pipe can be described by the nondimensional Laplace equation in cylindrical coordinates which is given by
\begin{align*}
\pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}

The boundary conditions on the inside and outside of the pipe are given by
\begin{align}
    \pderiv{u}{r}(1, \theta) &= -1, \quad 0\le\theta\le 2\pi \label{eq:inner_BC}\\
    u(2, \theta) &= \begin{cases}
        1, \quad & 0\le\theta<\frac{\pi}{2} \\
        0, \quad & \frac{\pi}{2}\le\theta<\frac{3\pi}{2} \\
        1, \quad & \frac{3\pi}{2}\le\theta\le 2\pi
        \end{cases} \label{eq:outer_BC}
\end{align}

\begin{parts}
%-------------------------------------------------------%
\part[4] Describe the heat transfer situation given by the boundary conditions~\ref{eq:inner_BC} and \ref{eq:outer_BC}.

\begin{solution}
The inner boundary condition~\ref{eq:inner_BC} describes a case where the heat flux at the inner boundary of the annulus is fixed at the value of $-1$. This represents a fixed heat flux into the domain because the negative slope gives a heat flux in the positive direction.

The outer boundary condition~\ref{eq:outer_BC} describes a case where the outer boundary of the annulus is held at a fixed temperature. The temperature depends on the location around the annulus. The right side is held at $1$ while the left side is held at $0$.

\hfill[2 points each for the description of the inner and outer boundary]
\end{solution}

%-------------------------------------------------------%
\part[24] Calculate the analytical solution of the Laplace equation on the annulus with the given boundary conditions.

\begin{solution}
The Laplace equation in cyclindrical coordinates is given by
\begin{align*}
\pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
and we are looking for separated solutions of the form $u(r,\theta) = R(r) \Theta(\theta)$ which give us two ordinary differential equations
\begin{align*}
-\frac{\Theta''}{\Theta} &= \frac{R'' + \frac{R'}{r}}{\frac{R}{r^2}} = \lambda
\end{align*}
The solutions of these two equations will depend on the value of $\lambda$ and we need to consider three cases. \hfill[2]

For $\lambda=0$ we get the solution
\[
u_0(r, \theta) = (a\theta + b)(C\ln(r) + D)
\]
with parameters $a$, $b$, $C$ and $D$. We know that the solution needs to be periodic in $2\pi$ and this is only the case for $a=0$. We set $b=1$ without loss of generality. 
Let us know look at the boundary conditions. At the inner boundary we need to have $\pderiv{u_0}{r}(1,\theta)=-1$ so that
\begin{align*}
\pderiv{u_0}{r}(1, \theta) &= C \frac{1}{1} \overset{!}{=} -1
\end{align*}
and from this it follows that $C=-1$. At the outer boundary we have the Dirichlet condition and we leave this until we have build the complete solution. Thus we are left with
\begin{align*}
u_0(r,\theta) = -\ln(r) + D
\end{align*}
where $D$ is a constant which needs to be determined. \hfill[4]

For $\lambda<0$ we get the combined solution
\begin{align*}
u_-(r,\theta) &= \left(A\cosh(\mu\theta) + B\sinh(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
with parameters $A$, $B$, $C$ and $D$, and where we set $\lambda = -\mu^2$. Again, this needs to be periodic in $2\pi$ which is only fulfilled for $A=B=0$. Thus the solution for $\lambda<0$ is zero. \hfill[2]

For $\lambda = \mu^2 > 0$ we get the combined solution
\begin{align*}
u_+(r,\theta) &= \left(A\cos(\mu\theta) + B\sin(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
with parameters $A$, $B$, $C$ and $D$. Again this needs to be periodic in $2\pi$ and this is the case only for $\mu=n$ with $n\in\mathbb{N}$. The inner boundary condition is already fulfilled by solution $u_0$ therefore $u_+$ needs to have a zero flux at the inter boundary.
\[
\pderiv{u_+}{r}(1, \theta) = \left(A\cos(n\theta) + B\sin(n\theta)\right) \left(-nC 1^{-n-1} + nD 1^{n-1}\right) \overset{!}{=} 0, \quad 0\le\theta\le2\pi
\]
and for $n\in\mathbb{N}$. Since the first part is not zero for all $\theta$, it follows that $C=D$. Thus the solution is given by
\[
u_+(r, \theta) = \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) \left(r^{n} + r^{-n}\right), \quad n=1,2,\dots
\]
where we have set $a_n = AC$ and $b_n = BC$. \hfill[4]

To fulfil the outer boundary condition we need to superimpose the solutions for $\lambda=0$ and $\lambda>0$. This is possible because we are dealing with a linear PDE. The complete solution is \hfill[2]
\[
u(r, \theta) = -\log(r) + D + \sum_{n=1}^\infty \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) \left(r^{n} + r^{-n}\right) 
\]

To find the coefficients $D$, $a_n$ and $b_n$ we need to set it equal to the boundary condition. At the outer boundary we need to have \hfill[2]
\begin{align*}
u(2, \theta) &= -\log(2) + D + \sum_{n=1}^\infty \left(a_n\cos(n\theta) + b_n\sin(n\theta)\right) \left(2^{n} + 2^{-n}\right) \\
 &\overset{!}{=} \begin{cases}
    1, \quad & 0\le\theta<\frac{\pi}{2} \\
    0, \quad & \frac{\pi}{2}\le\theta<\frac{3\pi}{2} \\
    1, \quad & \frac{3\pi}{2}\le\theta\le 2\pi
    \end{cases}
\end{align*}

The parameters $D$, $a_n$ and $b_n$ can be found with the usual method of the Fourier series.
The expression $-\log(2) + D$ is equivalent to $\frac{a_0}{2}$ in the Fourier series theory so that we get \hfill[2]
\begin{align*}
 2(D-\log(2)) = a_0 &= \frac{2}{2\pi} \int_{-\pi}^{\pi} u(2, \theta) d\theta = \frac{1}{\pi} \int_{-\pi/2}^{\pi/2} d\theta = 1
\end{align*}
from which it follows that $D = 0.5 + \log(2)$.

The parameter $a_n$ is given by \hfill[4]
\begin{align*}
a_n &= \frac{1}{\left(2^{n} + 2^{-n}\right)} \frac{2}{2\pi} \int_{-\pi}^{\pi} u(2, \theta) \cos(n\theta) d\theta = \frac{1}{\pi \left(2^{n} + 2^{-n}\right)} \int_{-\pi/2}^{\pi/2} \cos(n\theta) d\theta \\
 &= \frac{1}{\pi \left(2^{n} + 2^{-n}\right)} \left[\frac{\sin(n\theta)}{n} \right]_{-\pi/2}^{\pi/2} = \begin{cases}
    \frac{2}{n\pi \left(2^{n} + 2^{-n}\right)} (-1)^{(n-1)/2}, \quad & n \text{ odd} \\
    0, \quad & n \text{ even} 
    \end{cases} 
\end{align*}
The parameter $b_n$ is zero because the boundary condition is even.\hfill[2]
%\begin{align*}
%b_n &= \frac{1}{\left(2^{n} + 2^{-n}\right)} \frac{2}{2\pi} \int_{-\pi}^{\pi} u(2, \theta) \sin(n\theta) d\theta = 0
%\end{align*}

Thus, the complete solution is given by 
\begin{align*}
u(r, \theta) =& 0.5 + \log(2) - \log(r) + \sum_{n \text{ odd}} \frac{2 (-1)^{(n-1)/2}}{n\pi \left(2^{n} + 2^{-n}\right)}  \cos(n\theta)  \left(r^{n} + r^{-n}\right) \\
 =& 0.5 + \log(2) -\log(r) \\
 & + \sum_{k=1}^\infty \frac{2 (-1)^{k-1}}{(2k-1)\pi \left(2^{2k-1} + 2^{-2k+1}\right)}  \cos((2k-1)\theta)  \left(r^{2k-1} + r^{-2k+1}\right)
\end{align*}
for $1\le r \le 2$ and $0\le\theta\le 2\pi$ and with $n=2k-1$.\hfill[4]

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[4]
\end{solution}

%-------------------------------------------------------%
\part[4] Plot the analytical solution.

\vskip0.2cm
\textbf{Remarks:}
\begin{itemize}
\item Have a look at the exercise sheets and examples to get started.
\item The combined solution needs to fulfil the boundary conditions.
\end{itemize}

\begin{solution}
The Fourier series has only first order convergence. Before we plot the analytical solution, we compare the analytical solution for different cut-off points of the Fourier series. We compare a cut-off of 100, 200 and 300 with a larger cut-off of 500. The maximum difference between these solutions on an 80x80 grid are $\sim7\times10^{-2}$, $\sim7\times10^{-3}$ and $\sim9\times10^{-3}$, respectively. The difference is mainly due to the jump in the boundary condition which leads to over- and undershoots. From this we choose a cut-off of 200 for the rest of this coursework.

The solution on a 40x40 grid with a cut-off of 200 is given in the following plots. \hfill[4]

\includegraphics[width=0.5\columnwidth]{CW1_PDE3_2020-21_Analytical_solution_surf.png}
\includegraphics[width=0.5\columnwidth]{CW1_PDE3_2020-21_Analytical_solution_contour.png}
\end{solution}
\end{parts}

\question[60] Develop Matlab or Python scripts to handle the problem numerically.

\begin{parts}
%-------------------------------------------------------%
\part Develop a script in Matlab or Python to solve the Laplace equation with boundary conditions~\ref{eq:inner_BC} and \ref{eq:outer_BC}. 

In the report you need to explain what you are doing, compare the solution against the analytical solution, perform a grid convergence analysis and plot the solution.

\begin{solution}
To solve the PDE with Matlab/Python we need to discretise the equation. This solution uses the Matlab indexing starting at 1 but Python indexing would also be correct. The derivatives can be discretised with the finite difference formulas for first and second derivatives from the lectures. We define the grid spacings
\begin{align*}
r_j &= 1 + (j-1)\Delta r, \qquad & j=1,2,\dots,n_r \\
\theta_i &= (i-1)\Delta \theta, \qquad & i=1,2,\dots,n_\theta 
\end{align*}
where $\Delta r$ and $\Delta \theta$ are the grid spacing in the $r$ and $\theta$ direction, respectively. At the grid point $(i,j)$ the radius and angle are $r_j$ and $\theta_i$, respectively, and we define $u(\theta_i, r_j) = u_{i,j}$. \hfill[3]

With the finite difference formulas we get the discretisation of the Laplace equation in cylindrical coordinates
\begin{align*}
\frac{u_{i,j+1} - 2u_{i,j} + u_{i,j-1}}{(\Delta r)^2} + \frac{1}{r_j} \frac{u_{i,j+1} - u_{i,j}}{\Delta r} + \frac{1}{r_j^2} \frac{u_{i+1,j} - 2u_{i,j} + u_{i-1,j}}{(\Delta \theta)^2} = 0
\end{align*}
\hfill[3]

Multiply this with $(\Delta r)^2$ and rearrange for $u_{i,j}$ similar to the case in Cartesian coordinates to get
\begin{align*}
u_{i,j} \left(2 + \frac{\Delta r}{r_j} + 2\frac{(\Delta r)^2}{(\Delta\theta)^2 r_j^2}\right) =& u_{i,j+1}\left(1 + \frac{\Delta r}{r_j}\right) + u_{i,j-1} \\
    &+ \frac{(\Delta r)^2}{(\Delta\theta)^2}\frac{1}{r_j^2} (u_{i+1,j} + u_{i-1,j}) 
\end{align*}
Multiply both sides of the equation with the factor
\[
C = \frac{1}{\left(2 + \frac{\Delta r}{r_j} + 2\frac{(\Delta r)^2}{(\Delta\theta)^2 r_j^2}\right)}
\]
and bring it into the form required for the SOR method 
\begin{align}
u_{i,j}^{k+1}  = (1-\omega)u_{i,j}^{k} &+ \omega C \left(u_{i,j+1}^{k}\left(1 + \frac{\Delta r}{r_j}\right) + u_{i,j-1}^{k}\right. \nonumber\\
    & \quad\qquad\quad\left.+ \beta^2\frac{1}{r_j^2} (u_{i+1,j}^{k} + u_{i-1,j}^{k}) \right)
\label{eq:uij}
\end{align}
where we have used $\beta = \frac{\Delta r}{\Delta\theta}$ and with $\omega$ the relaxation factor. The superscript $k$ indicates the iteration number of the iterative SOR method. 
\hfill[3]

The Eq.~\ref{eq:uij} for $u_{i,j}^{k+1}$ is added into the SOR method for all internal values of $r$, i.e. all values of $r$ except $r=1$ and $r=2$, and all value of $\theta$. For the values $\theta=0$ and $\theta=2\pi$ we need to adjust the formula slightly so that the neighbouring angles are used, i.e. for $\theta=0$ the neighbouring angles are $\theta_+=\Delta \theta$ and $\theta_- = 2\pi-\Delta\theta$. \hfill[3]

At the inside boundary, i.e. for $r=1$, the Neumann boundary condition is discretised with the forward difference formula and rearrange to \hfill[3]
\[
u_{i, 1} = u_{i, 2} + \Delta r, \qquad i=1,2,\dots, n_\theta
\]

At the outside boundary the Dirichlet boundary condition is given by \hfill[3]
\[
u_{i,n_r} = \begin{cases}
    1, \quad & 0\le\theta_i<\frac{\pi}{2} \\
    0, \quad & \frac{\pi}{2}\le\theta_i<\frac{3\pi}{2} \\
    1, \quad & \frac{3\pi}{2}\le\theta_i\le 2\pi
    \end{cases} \qquad i=1,2,\dots, n_\theta
\]

We ran the SOR method on a 80x80 grid with tolerances of $10^{-5}$, $10^{-6}$ and $10^{-7}$, and compared it to a run with a tighter tolerance of $10^{-8}$ to get maximum differences of around $\times10^{-3}$, $\times10^{-4}$ and $\times10^{-5}$, respecively. An accuracy of $\times10^{-4}$ was deemed sufficient and thus all runs are performed with a tolerance of $10^{-6}$. \hfill[3]

The following plots show the contour and surface plots for the numerical solution on a 40 by 40 grid.\hfill[3]

\includegraphics[width=0.5\columnwidth]{CW1_PDE3_2020-21_Numerical_solution_surf.png}
\includegraphics[width=0.5\columnwidth]{CW1_PDE3_2020-21_Numerical_solution_contour.png}

The plots look very similar to the analytical solutions in part (a) which is confirmed by the contour and surface plots of the difference between the numerical and analytical solution for a 40 by 40 grid and 200 terms in the analytical solution. These plots show that the maximum difference is about $0.08$ which is mainly due to the overshoots of the analytical solution. \hfill[3]

\includegraphics[width=0.5\columnwidth]{CW1_PDE3_2020-21_Difference_solution_surf.png}
\includegraphics[width=0.5\columnwidth]{CW1_PDE3_2020-21_Difference_solution_contour.png}

The Matlab script RefinementAnalysis.m from the numerical lectures is used to perform the grid convergence analysis. We adapt the calculation of the integral metric for the case of cylindrical coordinates, i.e. multiply the integrand with the radial distance. The script is run for four grid sizes, i.e. 10x10, 20x20, 40x40 and 80x80. \hfill[3]

The order of convergence is given as $p=1.04$. This is smaller than expected because of the first order approximation of the first derivative in $r$. \hfill[3]

The grid convergence ratio is $1.004$ which is close to $1$ on the three finest grids which indicates that the solution is grid independent for the three finest grids, i.e. 20x20, 40x40 and 80x80.\hfill[3]

\textbf{Remark:} Different values for the order of convergence and grid convergence can be reached for different spatial discretisations of the first spatial derivative and the Neumann boundary condition.

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[4]
\end{solution}

%-------------------------------------------------------%
\part The physical system is modified by a source term in the left half of the annulus. Over the entire annulus the source term is given by
\[
f(r, \theta) = \begin{cases}
    0, \quad & 0\le\theta<\frac{\pi}{2} \\
    -(r-1)\cos(\theta), \quad & \frac{\pi}{2}\le\theta<\frac{3\pi}{2} \\
    0, \quad & \frac{3\pi}{2}\le\theta< 2\pi
    \end{cases} 
\]
for $1\le r \le 2$. 

Add the source term as an inhomogeneity to the Laplace equation and develop a Matlab or Python script to solve the inhomogeneous problem numerically.

In the report you need to explain what you are doing, compare the solution against the homogeneous system, perform a grid convergence analysis and plot the solution.

\vskip0.2cm
\textbf{Remarks:}
\begin{itemize}
\item Have a look at the computer labs and the associated solutions to get started.
\item While the Matlab or Python scripts on Learn can be used as a starting point, you need to make some modifications and additions.
\item It will be beneficial to work in cylindrical coordinates.
\end{itemize}

\begin{solution}
In the derivation of the numerical formula in part 2a) we need to replace the $0$ on the right hand side with the inhomogeneity. With the definition $f(\theta_i, r_j) = f_{i,j}$ and the results from part 2a) we get \hfill[3]
\begin{align*}
\frac{u_{i,j+1} - 2u_{i,j} + u_{i,j-1}}{\Delta r^2} + \frac{1}{r_j} \frac{u_{i,j+1} - u_{i,j}}{\Delta r} + \frac{1}{r_j^2} \frac{u_{i+1,j} - 2u_{i,j} + u_{i-1,j}}{\Delta \theta^2} = f_{i,j}
\end{align*}

Multiply this with $(\Delta r)^2$ and rearrange for $u_{i,j}$ similar to the case in part (b) to get 
\begin{align*}
u_{i,j} \left(2 + \frac{\Delta r}{r_j} + 2\frac{(\Delta r)^2}{(\Delta\theta)^2 r_j^2}\right) =& u_{i,j+1}\left(1 + \frac{\Delta r}{r_j}\right) + u_{i,j-1} \\
    &+ \frac{(\Delta r)^2}{(\Delta\theta)^2}\frac{1}{r_j^2} (u_{i+1,j} + u_{i-1,j}) - (\Delta r)^2 f_{i,j}
\end{align*}
Multiply both sides of the equation with the factor
\[
Ch = \frac{1}{\left(2 + \frac{\Delta r}{r_j} + 2\frac{(\Delta r)^2}{(\Delta\theta)^2 r_j^2}\right)}
\]
and bring it into the form required for the SOR method \hfill[3]
\begin{align}
u_{i,j}^{k+1}  = (1-\omega)u_{i,j}^{k} &+ \omega C \left(u_{i,j+1}^{k}\left(1 + \frac{\Delta r}{r_j}\right) + u_{i,j-1}^{k}\right. \nonumber\\
    & \quad\qquad\quad\left.+ \beta^2\frac{1}{r_j^2} (u_{i+1,j}^{k} + u_{i-1,j}^{k}) - (\Delta r)^2 f_{i,j} \vphantom{\frac12}\right) \nonumber
\label{eq:uij}
\end{align}

The boundary conditions of the problem are the same as for part (b) so the only changes in the Matlab script are required for the internal values of $r$, i.e. $1<r<2$. 
For these values we need to add the following term $g_{i,j}$ to the right hand side of the update equation for $u_{i,j}^{k+1}$ 
\[
g_{i,j} =\begin{cases}
        0, \quad & 0\le\theta_i<\frac{\pi}{2} \\
        (\Delta r)^2 \omega C (r_j-1)\cos(\theta_i), \quad & \frac{\pi}{2}\le\theta_i<\frac{3\pi}{2} \\
        0, \quad & \frac{3\pi}{2}\le\theta_i< 2\pi
        \end{cases} 
\]
where $dr$ is the grid spacing in the $r$ direction and $\omega$ the relaxation factor. Alternative formulations with the Heaviside unit step function are possible.
We run the SOR method with the same tolerance of $10^{-6}$ as used in part 2a). This was deemed to be sufficient. \hfill[3]

The following plots show the contour and surface plots for the numerical solution on a 40 by 40 grid.\hfill[3]

\includegraphics[width=0.5\columnwidth]{CW1_PDE3_2020-21_Numerical_inhomogeneous_solution_surf.png}
\includegraphics[width=0.5\columnwidth]{CW1_PDE3_2020-21_Numerical_inhomogeneous_solution_contour.png}

In comparison to the homogeneous system we have a negative forcing term for negative values of $x$ or, in other words, for $\pi/2\le\theta<3\pi/2$. This term leads to a decreased solution value for negative values of $x$ but also a very small change for positive $x$ in comparison to the homogeneous solution as seen in the plots. The contour plot of the difference shows that the contours are perpendicular to the inner boundary which shows that both solutions have the same slope there. \hfill[3]

\includegraphics[width=0.5\columnwidth]{CW1_PDE3_2020-21_Difference_inhomogeneous_solution_surf.png}
\includegraphics[width=0.5\columnwidth]{CW1_PDE3_2020-21_Difference_inhomogeneous_solution_contour.png}

The grid convergence analysis gives an order of convergence of $p=0.99$ which is close to that for the homogeneous case. The grid convergence ratio is $1.004$ which is close to $1$ on the three finest grids which indicates that the solution is grid independent for the three finest grids.\hfill[3]

\textbf{Remark:} Different values for the order of convergence and grid convergence can be reached for different spatial discretisations of the first spatial derivative and the Neumann boundary condition.

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[2]
\end{solution}

\end{parts}


\end{questions}
\end{document}
