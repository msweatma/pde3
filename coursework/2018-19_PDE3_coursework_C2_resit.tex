 % Add 'answers' to options to get the solutions
\documentclass[answers,addpoints,12pt]{exam}	

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Resit Coursework 2}
\footer{}{Page \thepage\ of \numpages}{\iflastpage{\textbf{END}}{\textbf{Continued}}}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_coursework_style}
\pointsdroppedatright 

%-------------------------------------------------------%
\begin{document}

The following resit coursework is to be handed in to the ETO online dropbox by \textbf{15:00 on Thursday the 8$^{th}$ of August 2019}. This is \textbf{resit coursework number 2 of 2}. Each coursework is worth $20\%$ of the final mark.
\vskip0.1cm
You should submit the solution to this coursework in the form of a short report on Learn. This report must not be longer than 7 pages with at least 2cm margins and a font size of 11pt. The marks for this coursework will be awarded for the correctness of the solution as well as for the clarity and logical progression of the solution. Details on laying out mathematical calculations and general guidelines for the coursework are given on Learn. 

\vskip0.1cm

%----- Question ----------------------------------------%
% This is the start of the questions
\begin{questions}
\question Consider the one-dimensional wave equation
\begin{align}
\frac{\partial^2u}{\partial t^2} &= 16 \frac{\partial^2u}{\partial x^2} & \quad  \label{eq:wave_equation_201808}\\
\intertext{for $x\in[0,\infty)$ with the initial and boundary conditions }
 u(x, 0) &= x e^{-x}, \quad \forall x, \label{eq:we_IC1_201808} \\
\left. \frac{\partial u(x, t)}{\partial t}\right|_{t=0} &= 0, \quad \forall x, \label{eq:we_IC2_201808} \\
 u(0, t) &= \sin(t), \quad \forall t, \label{eq:we_BC1_201808} \\
 \lim_{x\to\infty}u(x,t) &= 0, \quad \forall t. \label{eq:we_BC2_201808}
\end{align}

\begin{parts}
%----- Part -----------------------------------------------------------%
\part[15] The partial differential equation~(\ref{eq:wave_equation_201808}) and the boundary and initial conditions~(\ref{eq:we_IC1_201808}-\ref{eq:we_BC2_201808}) model the behaviour of a string. 

Describe the string and the physical quantity calculated by the model. Comment also on the physical dimensions of the string, its initial state and boundary conditions.
\droppoints

\begin{solution}
\begin{itemize}
\item The equation system describes the displacement of a semi-infinite string going from $0$ to infinity. \hfill[4]
\item Initially the displacement of the string is given by $xe^{-x}$ and its initial velocity is zero. \hfill[4]
\item The left side of the string at $x=0$ is moved sinusoidally while for $x\to\infty$ the string stays at rest. \hfill[4]
\end{itemize}

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[3]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[45] Apply the Laplace transform with respect to $t$ to the PDE~(\ref{eq:wave_equation_201808}) and find the general solution for $U(x,s)$ in the Laplace domain.
\droppoints

\begin{solution}
The Laplace transform of the left hand side of the partial differential equation can be found using the derivative property and on the right hand side we can swap the order of integration and differentiation. \hfill[2]

This gives us the transformed equation
\begin{align}
s^2 U(x, s) - s u(x, 0) - u'(x,0) &= 16 {\cal L} \left\{\frac{\partial^2 u(x,t)}{\partial x^2} \right\} = 16\frac{\partial^2 U(x,s)}{\partial x^2} 
\label{eq:we_transformed_201808}
\end{align}

Insert the initial conditions into Eq.~(\ref{eq:we_transformed_201808}) to get
\begin{align}
s^2 U(x, s) - sx e^{-x} &= 16 \frac{\partial^2 U(x,s)}{\partial x^2} \nonumber\\
\Rightarrow \frac{\partial^2 U(x,s)}{\partial x^2} - \frac{s^2}{16} U(x, s) &= \frac{-s}{16}x e^{-x} \label{eq:we_laplace_201808}
\end{align}
\hfill[8]

The differential equation~(\ref{eq:we_laplace_201808}) has the characteristic equation
\begin{align*}
m^2 - \frac{s^2}{16} &= 0 \\
\intertext{which has the zeros}
 m_{1,2} &= \pm \frac{s}{4} \\
\end{align*}
Thus the complimentary function is
\begin{align}
U_g(x,s) &= A(s) e^{sx/4} + B(s) e^{-sx/4} \label{eq:we_general_solution_201808}
\end{align}
where $A(s)$ and $B(s)$ are functions in the Laplace domain which we need to determine from the boundary conditions. \hfill[8]

Before we find the two functions we need to take care of the inhomogeneity of the differential equation. The trial solution for the particular integral for the inhomogeneous differential equation~(\ref{eq:we_laplace_201808}) is
\begin{align*}
U_p(x,s) &= P e^{-x} + Q x e^{-x}
\end{align*}
where the coefficients $P$ and $Q$ need to be determined. We substitute $U_p$ and its second derivative with respect to $x$ into Eq.~(\ref{eq:we_laplace_201808}) and get \hfill[8]
\begin{align*}
P e^{-x} + Q\left(x e^{-x} - 2 e^{-x}\right) - \frac{s^2}{16} \left(P e^{-x} + Q x e^{-x}\right) &= -\frac{s}{16} x e^{-x}
\end{align*}
We can arrange this in terms of $e^{-x}$ and $xe^{-x}$
\[
e^{-x}\left(P - 2 Q - \frac{s^2}{16}P \right) + xe^{-x} \left(Q - \frac{s^2}{16} Q + \frac{s}{16}\right) = 0
\]
From this we get two equations 
\begin{align*}
P\left(1-\frac{s^2}{16}\right) - 2Q &= 0 \\
Q\left(1-\frac{s^2}{16}\right) &= -\frac{s}{16}
\end{align*}

From this it follows that
\begin{align*}
Q &= \frac{s}{s^2 - 16} \\
P &= -\frac{32s}{(s^2 - 16)^2}
\end{align*}
for $\Re(s)>4$.\hfill[8]

Thus the general solution of the differential equation is of the form
\begin{align*}
U(x,s) &= A(s) e^{sx/4} + B(s) e^{-sx/4} - \frac{32s}{(s^2 - 16)^2} e^{-x} + \frac{s}{s^2 - 16} x e^{-x}
\end{align*}
\hfill[4]

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[7]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[25] Given that the general solution to the PDE~(\ref{eq:wave_equation_201808}) is
\begin{align*}
U(x,s) &= A(s) e^{sx/4} + B(s) e^{-sx/4} - \frac{32s}{(s^2 - 16)^2} e^{-x} + \frac{s}{s^2 - 16} x e^{-x}
\end{align*}
use the boundary conditions~(\ref{eq:we_BC1_201808}-\ref{eq:we_BC2_201808}) to find the particular solution. Use the inverse Laplace Transform on the particular solution to get the solution of the wave equation~(\ref{eq:wave_equation_201808}-\ref{eq:we_BC2_201808}) in the time domain.
\droppoints

\begin{solution}
From the boundary condition~(\ref{eq:we_BC2_201808}) we know that the solution is bounded. Thus we can swap the limit and the integration to get
\begin{align*}
\lim_{x\to\infty} U(x,s) &= \lim_{x\to\infty} \int_0^\infty e^{-st} u(x,t) dt = \int_0^\infty  e^{-st} \lim_{x\to\infty} u(x,t) dt = 0
\end{align*}
From this it follows that
\begin{align*}
A(s) &= 0, 
\end{align*}
because otherwise the first term on the right hand side of Eq.~(\ref{eq:we_general_solution_201808}) would go to infinity for $x\to\infty$.
\hfill[7]

The boundary condition~(\ref{eq:we_BC1_201808}) requires that
\begin{align*}
U(0,s) &= B(s) - \frac{32s}{(s^2 - 16)^2} \overset{!}{=} {\cal L} \left\{u(0,t) \right\} =  \frac{1}{s^2 + 1}, \quad \Re(s)>0,
\end{align*}
and thus
\begin{align*}
 B(s) &= \frac{1}{s^2 + 1} + \frac{32s}{(s^2 - 16)^2}
\end{align*}
\hfill[7]

From the solution in the Laplace domain 
\begin{align*}
U(x,s) &= \left(\frac{1}{s^2 + 1} + \frac{32s}{(s^2 - 16)^2}\right) e^{-sx/4} - \frac{32s}{(s^2 - 16)^2} e^{-x} + \frac{s}{s^2 - 16} x e^{-x}
\end{align*}
we can find the solution in the time domain by using the time shifting property and the ramp times hyperbolic sine
\begin{align*}
u(x,t) =& \left(\sin(t-x/4) + 4(t-x/4)\sinh[4(t-x/4)] \right) H(t-x/4) \\
   &- 4t\sinh(4t) e^{-x} H(t) + \cosh(4t) x e^{-x} H(t)
\end{align*}
\hfill[7]

Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[4]
\end{solution}

%----- Part -----------------------------------------------------------%
\part[15] Plot the time domain solution and check that the initial and boundary conditions are fulfilled.
\droppoints

\begin{solution}
Contour and surface plot \hfill[8]

Check initial and boundary conditions \hfill[4]


Clarity and layout: explanation of the steps and variables; equations as part of complete sentences. \hfill[3]
\end{solution}

\end{parts}
\end{questions}

\newpage
%----- Table -------------------------------------------------------%
\begin{table}[ht]
\caption{Properties of the unilateral Laplace transform} % title of Table
\centering % used for centering table
\begin{tabular}{|c|c|c|} % centered columns (4 columns)
\hline %inserts double horizontal lines
Property & $f(t), t\ge0$ & $F(s)$ \\ 
%heading
\hline 
Definition & $f(t)$ & $\int_0^\infty\exp(-st)f(t)dt$ \\ [0.3ex]
Inversion & $\frac{1}{j2\pi}\int_{c-j\infty}^{c+j\infty} \exp(st)F(s)ds$ & $F(s)$ \\ [0.3ex]
Linearity & $\alpha f(t) + \beta g(t)$ & $\alpha F(s) + \beta G(s)$ \\ [0.3ex]
Scaling & $f(\alpha t)$ & $\frac{1}{\alpha}F\left(\frac{s}{\alpha}\right)$ \\ [0.3ex]
Frequency shifting & $\exp(\alpha t)f(t)$ & $F(s-\alpha)$ \\ [0.3ex]
Time shifting & $f(t-\alpha) H(t-\alpha)$ & $\exp(-\alpha s)F(s)$ \\ [0.3ex]
First derivative & $f'(t)$ & $sF(s) - f(0)$ \\ [0.3ex]
nth derivative & $f^{(n)}(t)$ & $s^nF(s)-s^{n-1}f(0)-s^{n-2}f'(0)$ \\
      & & $-\dots-s f^{(n-2)}(0) - f^{(n-1)}(0)$  \\ [0.3ex]
Transform derivative & $t^n f(t)$ & $(-1)^n \frac{d^n F(s)}{ds^n}$ \\ [0.3ex]        
Integration & $\int_0^t f(\tau)d\tau$ & $\frac{1}{s}F(s)$  \\ [0.3ex]
\hline
\end{tabular}
\label{table:properties} % is used to refer this table in the text
\end{table}

%----- Table -------------------------------------------------------%
\begin{table}[ht]
\caption{Function transforms} % title of Table
\centering % used for centering table
\begin{tabular}{|c|c|c|c|} 
\hline %inserts double horizontal lines
Name & $f(t), t\ge0$ & $F(s)$ & Convergence region \\ [0.3ex] % inserts table
%heading
\hline % inserts single horizontal line
Unit impulse & $\delta(t)$ & $1$ & all $s$ \\ [0.3ex]
Ideal delay& $\delta(t-\alpha)$ & $\exp(-\alpha s)$ & $\Re(s)\ge\alpha$ \\ [0.3ex]
Unit step & $H(t)$ & $\frac{1}{s}$ & $\Re(s)>0$ \\ [0.3ex]
Ramp  & $t$ & $\frac{1}{s^2}$ & $\Re(s)>0$ \\ [0.3ex]
nth power  & $t^n, n=1,2,\dots$ & $\frac{n!}{s^{n+1}}$ & $\Re(s)>0$ \\ [0.3ex]
Sine & $\sin(\alpha t)$ & $\frac{\alpha}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
Cosine & $\cos(\alpha t)$ & $\frac{s}{s^2 + \alpha^2}$ & $\Re(s)>0$ \\ [0.3ex]
Hyperbolic sine & $\sinh(\alpha t)$ & $\frac{\alpha}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
Hyperbolic cosine & $\cosh(\alpha t)$ & $\frac{s}{s^2 - \alpha^2}$ & $\Re(s)>|\alpha|$ \\ [0.3ex]
Exponential decay & $\exp(-\alpha t)$ & $\frac{1}{s + \alpha}$ & $\Re(s)>-\alpha$ \\ [0.3ex]
\hline %inserts single line
\end{tabular}
\label{table:functions}
\end{table}
\end{document}
