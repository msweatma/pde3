\documentclass[answers,a4paper,addpoints,12pt]{exam}
% This is typeset using the EXAM LaTeX2e class by Philip Hirschhorn. 

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Laboratory 4}
% solution header

\usepackage{PDE3_tutorial}

\begin{document}

The practice questions are similar to the exercises in the textbook and in Schaum's outlines Partial Differential Equations. If you are having difficulties with these questions practice using the remaining questions from these exercise. You will get the best from this tutorial by working though these examples \textbf{before} the tutorial and asking your tutors to help you with questions with which you are having problems. \\

% This is the start of the questions
\begin{questions}
%----- Question ----------------------------------------%
\question Consider the numerical solutions for the problems from the tutorials of weeks 2 and 3.

\begin{parts}
\part Perform numerical experiments to establish the required grid size for both problems to achieve sufficient accuracy.

\begin{solution}
Before we look at the numerical validation of the implementation, let us look at the verification of the results. Plotting both the analytical and numerical solution for the last tutorials show that the solution is consistent between the two methods. 

In addition, the results seem sensible and physically sound. For example, the maxima and minima are at the domain boundaries as required for elliptical problems. The case from tutorial 2 has only one non-zero boundary while the other boundaries are held at zero. The disturbance vanishes for values of $y>1$ which is for values larger than the width in the $x$ direction. Since the Laplace equation assumes that the material is isotropic this behaviour is expected because at that point the disturbance at $y=0$ is further away than the boundaries held at zero. A similar case can be made for the case from tutorial 3 where the disturbance for positive and negative $x$ cancel each other.

Thus, we conclude that our solutions are reasonable and can now consider the question of accuracy. Here, the first question is what exactly is sufficient accuracy:
\begin{itemize}
\item Check order of accuracy is as expected
\item Check the mesh is fine enough
\item Quantify truncation errors
\end{itemize}

We use the following integral quantity to check the convergence of the method
\[
U = \int_\Omega u^2(x,y) dydx
\]

The refinement analysis from the provided script is run for grid spacings of $0.2$, $0.1$, $0.05$, $0.025$ and $0.0125$ for the problems from week 2 and 3. The output from the script for week 2 is
\begin{lstlisting}
5 grids have been used.

Grid  Spacing  Function
   1  0.013      0.035311
   2  0.025      0.036957
   3  0.050      0.040457
   4  0.100      0.048222
   5  0.200      0.065791

Order of convergence using the first three finest grids
and assuming constant grid refinement.
Order of Convergence, p = 1.088528

Richardson Extrapolation: Use above order of convergence
and first and second finest grids
Estimate to zero grid value, f_exact = 0.033850

Grid Convergence Index on fine grids. Using p=1.089 
Factor of Safety = 1.25

  Grid      Refinement
  Step      Ratio, r      GCI
  1  2      2.000000      5.171030
  2  3      2.000000     10.506889
  3  4      2.000000     21.298928
  4  5      2.000000     40.424391

Checking for asymptotic range using Eqn. 5.10.5.2.
A ratio of 1.0 indicates asymptotic range.

Grid Range   Ratio
 12 23       1.046604
 23 34       1.049049
 34 45       1.120453

--- End of Report ---
\end{lstlisting}

From the script it follows that the order of convergence is $p=1.09$ so that the method is linearly convergent. The grid refinement analysis shows that the solutions on the grids with spacings $0.1$, $0.05$, $0.025$ and $0.0125$ are mesh independent. We would use a mesh with $\Delta x = \Delta y \le 0.1$.

Here is the Matlab script for the problem from week 2:
\lstinputlisting[language=Matlab]{../scripts/Lab4_Q1a_Lab2.m}

The output from the script for week 3 is
\begin{lstlisting}
5 grids have been used.

Grid  Spacing  Function
   1  0.013     55.786096
   2  0.025     57.375566
   3  0.050     60.567057
   4  0.100     66.957557
   5  0.200     79.603924

Order of convergence using the first three finest grids
and assuming constant grid refinement.
Order of Convergence, p = 1.005684

Richardson Extrapolation: Use above order of convergence
and first and second finest grids
Estimate to zero grid value, f_exact = 54.209078

Grid Convergence Index on fine grids. Using p=1.006 
Factor of Safety = 1.25

  Grid      Refinement
  Step      Ratio, r      GCI
  1  2      2.000000      3.533627
  2  3      2.000000      6.898599
  3  4      2.000000     13.085571
  4  5      2.000000     23.423971

Checking for asymptotic range using Eqn. 5.10.5.2.
A ratio of 1.0 indicates asymptotic range.

Grid Range   Ratio
 12 23       1.028492
 23 34       1.058545
 34 45       1.121691

--- End of Report ---
\end{lstlisting}

From the script it follows that the order of convergence is $p=1.006$ so that the method is linearly convergent. The grid refinement analysis shows that the solutions on the grids with spacings $0.1$, $0.05$, $0.025$ and $0.0125$ are mesh independent. We would use a mesh with $\Delta x = \Delta y \le 0.1$.

Here is the Matlab script for the problem from week 3:
\lstinputlisting[language=Matlab]{../scripts/Lab4_Q1a_Lab3.m}
\end{solution}

\part Is there a difference in the convergence for the two problems? If so in what region of the problem domain and for what reason.

\begin{solution}
The order of convergence for both cases is around 1. However, it is slightly higher for the problem from Lab 2 because the problem from Lab 3 has the discontinuity in the boundary condition for $y=0$.
\end{solution}

\part Is there a difference if you use the analytical solution or the last numerical solution for the convergence analysis? 

\begin{solution}
In the two cases, it makes almost no difference if we use the numerical solution on the finest grid or the analytical solution because the numerical solution on the finest grid is mesh independent. 

However, we can use the analytical solution to directly check how close the numerical solution on a given grid is to the exact solution.
\end{solution}

\end{parts} 
%----- end Question ------------------------------------%

\end{questions}

%-------------------------------------------------------%
\newpage
{\large\textbf{Formula sheet}}

\begin{enumerate}
\item Useful integrals
\begin{align*}
\int x \sin ax \, \mathrm{d}x &= \frac{1}{a^2} \bigl ( \sin a x - a x \cos ax \bigr ) \\
\int x \cos ax \, \mathrm{d}x &= \frac{1}{a^2} \bigl ( \cos a x + a x \sin ax \bigr ) \\
\int x^2 \sin ax \, \mathrm{d}x &= \frac{2x}{a^2} \sin ax - \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \cos ax \\
\int x^2 \cos ax \, \mathrm{d}x &= \frac{2x}{a^2} \cos ax + \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \sin ax
\end{align*}
\item Trigonometric identities
\begin{align*}
\sin(a\pm b) &= \sin(a) \cos(b) \pm \cos(a) \sin(b) \\
\cos(a\pm b) &= \cos(a) \cos(b) \mp \sin(a) \sin(b) \\
2\sin(a) \cos(b) &= \sin(a+b) + \sin(a-b) \\
2\sin(a) \sin(b) &= \cos(a+b) - \cos(a-b) \\
2\cos(a) \cos(b) &= \cos(a+b) + \cos(a-b)
\end{align*}
\end{enumerate}

\end{document}
