\documentclass[answers,a4paper,addpoints,12pt]{exam}
% This is typeset using the EXAM LaTeX2e class by Philip Hirschhorn. 

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Laboratory 5}
% solution header

\usepackage{PDE3_tutorial}

\begin{document}

The practice questions are similar to the exercises in the textbook and in Schaum's outlines Partial Differential Equations. If you are having difficulties with these questions practice using the remaining questions from these exercise. You will get the best from this tutorial by working though these examples \textbf{before} the tutorial and asking your tutors to help you with questions with which you are having problems. \\

\vskip0.2cm

% This is the start of the questions
\begin{questions}
%----- Question ----------------------------------------%
\question Consider the Laplace equation example in cylindrical coordinates from the lecture slides.

\begin{parts}
\part Follow all the steps in the analytical solution.

\begin{solution}
The solution is
\[
u(r,\theta) = \frac{1}{2} + \sum_{k=1}^\infty \frac{2}{(2k-1)\pi a^{2k-1}} r^{2k-1}\sin((2k-1)\theta)
\]
and the following plots show the solution with 20 terms of the infinite sum.
\includegraphics[width=0.5\columnwidth]{W5_LE1_analytical_contour.png}
\includegraphics[width=0.5\columnwidth]{W5_LE1_analytical_solution.png}
\end{solution}

\part Adjust the numerical solution in Cartesian coordinates to the cylindrical coordinate system.

\begin{solution}
The Laplace transform in cylindrical coordinates is given by
\begin{align*}
\pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}

The derivatives can be discretised with the finite difference formulas for first and second derivatives 
\begin{align*}
\left.\pderiv{u}{x}\right|_{x=x_i} &= \frac{u_{i+1} - u_i}{\Delta x} \\
\left.\pdderiv{u}{x}\right|_{x=x_i} &= \frac{u_{i+1} - 2u_i + u_{i-1}}{(\Delta x)^2}
\end{align*}
With the definition
\[
u(\theta_i, r_j) = u_{i,j}
\]
and the finite difference formulas we get
\begin{align*}
\frac{u_{i,j+1} - 2u_{i,j} + u_{i,j-1}}{(\Delta r)^2} + \frac{1}{r_j} \frac{u_{i,j+1} - u_{i,j}}{\Delta r} + \frac{1}{r_j^2} \frac{u_{i+1,j} - 2u_{i,j} + u_{i-1,j}}{(\Delta \theta)^2} = 0
\end{align*}
Multiply this with $(\Delta r)^2$ and rearrange for $u_{i,j}$ similar to the case in Cartesian coordinates to get
\begin{align*}
u_{i,j} \left(2 + \frac{\Delta r}{r_j} + 2\frac{(\Delta r)^2}{(\Delta\theta)^2 r_j^2}\right) =& u_{i,j+1}\left(1 + \frac{\Delta r}{r_j}\right) + u_{i,j-1} \\
    &+ \frac{(\Delta r)^2}{(\Delta\theta)^2}\frac{1}{r_j^2} (u_{i+1,j} + u_{i-1,j}) 
\end{align*}
Multiply both sides of the equation with the factor
\[
Ch = \frac{1}{\left(2 + \frac{\Delta r}{r_j} + 2\frac{(\Delta r)^2}{(\Delta\theta)^2 r_j^2}\right)}
\]
and bring it into the form required for the SOR method
\begin{align}
u_{i,j}^{k+1}  = (1-\omega)u_{i,j}^{k} &+ \omega Ch \left(u_{i,j+1}^{k}\left(1 + \frac{\Delta r}{r_j}\right) + u_{i,j-1}^{k}\right. \nonumber\\
    & \quad\qquad\quad\left.+ \beta^2\frac{1}{r_j^2} (u_{i+1,j}^{k} + u_{i-1,j}^{k}) \right)
\label{eq:uij}
\end{align}
where we have used
\[
\beta = \frac{\Delta r}{\Delta\theta}
\]

In the Matlab script we are calculating Equation~\ref{eq:uij} for all internal values of $r$, i.e. all values of $r$ except $r=0$ and $r=a$, to find the solution inside the solution domain. Here we treat $r=0$ as a special case because Equation~\ref{eq:uij} requires us to divide by $r$ which is not defined for $r=0$. 
This part of the solution is valid for all homogeneous Laplace equations in cylindrical coordinate systems. 
The only changes for different problems are required at the boundaries which we will describe next.

To complete the numerical treatment we need to add equations for the solution at the boundaries of the domain, i.e $r=0$ and $r=a$. The outer boundary is given by a Dirichlet boundary condition and thus we set 
\[
u(i, n_r) = H(\pi - \Theta(i, n_r)), \quad i=1,2, \dots, n_{\theta}
\] 
where $H$ is the Heaviside unit step function, and $n_r$ and $n_{\theta}$ are the number of grid points in the radial and angular direction, respectively.

The solution at the origin needs to be the same for all values of $\theta$. To fulfil the energy balance at the origin, the sum of the $r$ derivatives for all angles $\theta$ need to be zero
\begin{align*}
\sum_{\theta} \pderiv{u}{r} &= \sum_{j=1}^{n_{\theta}} \frac{u_{j,2} - u_{j,1}}{\Delta r} \overset{!}{=} 0 
\end{align*}
If this weren't the case we would have a source or sink term at the origin. From this it follows that the value at the origin is given by
\begin{align*}
u_{i,1} &= \frac{1}{n_{\theta}}\sum_{j=1}^{n_{\theta}} u_{i,2}, \qquad i=1,2,\dots,n_\theta
\end{align*}
which is the average of the surrounding values. 

Furthermore, the values for $\theta=0$ are the same as the ones for $\theta=2\pi$. When we take care of these conditions we get the following solution.

\includegraphics[width=0.5\columnwidth]{W5_LE1_numerical_contour.png}
\includegraphics[width=0.5\columnwidth]{W5_LE1_numerical_solution.png}

The complete solution is shown in the following Matlab script:
\lstinputlisting[language=Matlab]{../scripts/Lab5_Q1.m}
\end{solution}

\part Compare the analytical to the numerical solution.

\begin{solution}
Compare the integral over the domain as well as the behaviour in the plots for different discretisations and numbers in the Fourier series.
\end{solution}

\part Perform the grid convergence analysis.
\begin{solution}
Use the Matlab script RefinementAnalysis.m with 3 or more different grid sizes. The Matlab output is
\begin{lstlisting}
3 grids have been used.

Grid  Spacing  Function
   1  0.628     14.938484
   2  0.314     13.875453
   3  0.157     13.312441


Order of convergence using the first three finest grids
and assuming constant grid refinement.
Order of Convergence, p = 0.916945

Richardson Extrapolation: Use above order of convergence
and first and second finest grids
Estimate to zero grid value, f_exact = 12.678499

Grid Convergence Index on fine grids. Using p=0.917 
Factor of Safety = 1.25

  Grid      Refinement
  Step      Ratio, r      GCI
  1  2      0.500000    -18.910763
  2  3      0.500000    -10.783019

Checking for asymptotic range using Eqn. 5.10.5.2.
A ratio of 1.0 indicates asymptotic range.

Grid Range   Ratio
 12 23       0.928839
\end{lstlisting}

The following figure shows the result of the Richardson extrapolation.

\includegraphics[width=0.5\columnwidth]{W5_Richardson_extrapolation.png}

Discuss the results of the grid convergence analysis.
\end{solution}
\end{parts} 
%----- end Question ------------------------------------%

\end{questions}
\end{document}
