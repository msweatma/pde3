\documentclass[answers,a4paper,addpoints,12pt]{exam}
% This is typeset using the EXAM LaTeX2e class by Philip Hirschhorn. 

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Laboratory 7}
\footer{}{Page \thepage\ of \numpages}{\iflastpage{\textbf{END}}{\textbf{Continued}}}
% solution header

\usepackage{PDE3_tutorial}

\begin{document}
% This is the start of the questions
\begin{questions}
% From exercise 9.4.3 of Advanced Modern Engineering Mathematics
%----- Question ----------------------------------------%
\question Consider the spherically symmetric form of the heat conduction equation
\begin{equation}
\pdderiv{u}{r} + \frac{2}{r}\pderiv{u}{r} = \frac1\kappa\pderiv{u}{t}
\label{eq:spherically_symmetric_heat_eq}
\end{equation}

\begin{parts}
\part Define
\[
v(r,t) = r u(r,t)
\]
and show that $v$ satisfies the standard one-dimensional heat conduction equation. What can we expect of a solution as $r\to\infty$?

\begin{solution}
If $v=ru$, then differentiating produces
\begin{align*}
\pderiv{v}{r} &= u + r\pderiv{u}{r} \\
\pdderiv{v}{r} &= 2 \pderiv{u}{r} + r\pdderiv{u}{r}
\end{align*}
and hence
\[
\pdderiv{u}{r} + \frac{2}{r} \pderiv{u}{r}  = \frac{1}{r}\pdderiv{v}{r}
\]
Inserting this into the spherically symmetric form of the heat conduction equation gives
\[
\frac{1}{r} \pdderiv{v}{r} = \frac{1}{\kappa r}\pderiv{v}{t}
\]
which after cancelling out the $r$ gives the standard one-dimensional heat conduction equation.

If $v$ remains bounded for $r\to\infty$ the solution $u\to0$ for $r\to\infty$.
\end{solution}

\part Solve the equation in the annulus $a\le r\le b$ subject to the boundary conditions
\begin{align*}
u(a,t) &= T_0, \quad & t>0 \\
u(b,t) &= 0, \quad & t>0 \\
u(r,0) &= 0, & a\le r\le b
\end{align*}

Show that the solution has the form
\[
T(r,t) = \frac{a T_0}{r} \left[\frac{b-r}{b-a} - \sum_{N=1}^\infty A_N e^{-\kappa\lambda^2 t} \sin\left(\frac{r-a}{b-a}N\pi\right) \right]
\]
where $\lambda(b-a)=N\pi$. Evaluate the Fourier coefficients $A_N$.

\begin{solution}
We solve the standard one-dimensional heat conduction equation for $v(r,t) = r u(r,t)$. Since $u=\frac{v}{r}$, the boundary conditions for $v$ are
\begin{align*}
u(a,t) &= T_0 \xrightarrow{yields} v(a,t) = aT_0, \quad & t>0 \\
u(b,t) &= 0 \xrightarrow{yields} v(b,t) = 0, \quad & t>0 \\
u(r,0) &= 0 \xrightarrow{yields} v(r,0) = 0, & a\le r\le b
\end{align*}

First, we need to find the steady state solution $V(r)$ for which $v(r,t) = V(r) + w(r,t)$. Since the inner boundary is held at $aT_0$ and the outer at $0$, we get a linearly decreasing temperature from the inner to the outer boundary
\[
V(r) = aT_0\frac{b - r}{b - a}
\]

With this steady state solution we get the following boundary conditions for the transient part $w(r,t)$
\begin{align*}
w(a,t) &= 0, \quad & t>0 \\
w(b,t) &= 0, \quad & t>0 \\
w(r,0) &= -aT_0\frac{b - r}{b - a}, & a\le r\le b
\end{align*}

From the lectures we know that the solution of the one-dimensional heat conduction equation has the form
\[
w(r, t) =\sum_{N=1}^\infty e^{-\kappa \lambda_N^2 t} \left(a_N\sin(\lambda_N (r + \phi)) + b_N\cos(\lambda_N r)\right)
\]
and that we need to find the parameters $\lambda_N$, $a_N$ and $b_N$. We have added a shift of the sine so that we can better handle the boundary conditions. We can easily check that this is also a solution to the heat conduction equation.

We set $b_N=0$ for all $N$ since there is no cosine term in the sought solution and check if we can still match all boundary conditions. To match the condition at $r=a$ we shift the sine so that its argument is zero: $\phi = - a$. 

From the second boundary condition at $r=b$ we see that 
\[
\lambda_N = \frac{N\pi}{b-a}, \quad N=1,2,\dots
\]
Thus, we get
\[
w(r, t) =\sum_{N=1}^\infty a_N e^{-\kappa \lambda_N^2 t} \sin\left(\frac{r-a}{b-a}N\pi\right)
\]
and the complete solution as
\[
u(r,t) = \frac{1}{r}(V(r) + w(r,t)) =  \frac{a T_0}{r} \left[\frac{b-r}{b-a} - \sum_{N=1}^\infty A_N e^{-\kappa\lambda^2 t} \sin\left(\frac{r-a}{b-a}N\pi\right) \right]
\]
where $a_N = -aT_0 A_N$ for all $N$.

To find the parameters $A_N$ we need to solve the Fourier problem
\begin{align*}
\frac{b-r}{b-a} &= \sum_{N=1}^\infty A_N \sin\left(\frac{r-a}{b-a}N\pi\right)
\end{align*}
The coefficients can be obtained from integration or from standard tables of Fourier series as
\[
A_N = \frac{2}{\pi N}, \quad N=1,2,3,\dots
\]
\end{solution}

\part Modify the Matlab scripts from the lectures to solve the problem numerically. 

\begin{solution}
We need to discretise the spherically symmetric heat conduction equation~\ref{eq:spherically_symmetric_heat_eq}. First, we define the grid spacings
\begin{align*}
r_i &= a + (i-1)\Delta r, \qquad & i=1,2,\dots,n_r \\
t_j &= (j-1)\Delta t, \qquad & j=1,2,\dots,n_t
\end{align*}
where $\Delta r$ and $\Delta t$ are the grid spacing in the $r$ and $t$ direction, respectively. Here, $\Delta r$ is given by
\[
\Delta r = \frac{b-a}{n_r - 1}
\]
At the grid point $(i,j)$ the position is given by $r_i$ and $t_j$, respectively, and we define $u(r_i, t_j) = u_{i,j}$. 

We discretise Equation~\ref{eq:spherically_symmetric_heat_eq} with the forward time, centred space discretisation schemes to get
\begin{align}
u_{i, j+1} &= u_{i,j} + \frac{\kappa \Delta t}{(\Delta r)^2} \left(u_{i+1,j} - 2u_{i,j} + u_{i-1,j}\right) + \frac{\kappa \Delta t}{r_i\Delta r} \left(u_{i+1,j} - u_{i-1,j}\right)
\label{eq:spherically_symmetric_discretised}
\end{align}
which needs to be solved for $i=2,3, \dots, n_r - 1$ and for $j=2,3,\dots,n_t$.

The initial values are given by
\[
u_{i,1} = 0, \quad \text{for } i=1,2,\dots, n_r
\]
and the boundary values are given by
\begin{align*}
u_{1,j} &= T_0, \quad j=1,2,\dots, n_t \\
u_{n_r,j} &= 0, \quad j=1,2,\dots, n_t 
\end{align*}
We notice that the initial and boundary conditions are not consistent. Thus, the order in which they are applied matters and will change the result slightly.

The discretisation~\ref{eq:spherically_symmetric_discretised} gives us an explicit scheme which can be solved time step by time step. From the initial conditions we calculate the solution at the next time step, i.e. for $t = \Delta t$. This is repeated until we reach the desired final time.

The following plots show the numerical and analytical solution as well as the difference between the two for the following parameters
\begin{align*}
a &= 1 \\
b &= 2 \\
T_0 &= 2 \\
\kappa &= 0.3
\end{align*}

\includegraphics[width=0.5\columnwidth]{Lab7_Q1c_numerical_solution}
\includegraphics[width=0.5\columnwidth]{Lab7_Q1c_analytical_solution}
\includegraphics[width=0.5\columnwidth]{Lab7_Q1c_difference_surface}


The following Matlab script generates the above plots.

\lstinputlisting[language=Matlab]{../scripts/Lab7_Q1c.m}
\end{solution}

\end{parts}
%----- End question ---------------------------------%

\end{questions}
\end{document}
