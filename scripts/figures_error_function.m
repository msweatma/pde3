% figures_error_function.m - Figure showing the error function

%% Plot error function
N = 100;
xaxis = [0, 3];
x = linspace(xaxis(1), xaxis(2), N);
y1 = erf(x);
y2 =  erfc(x);

figure(1)
plot(x, y1, '-', x, y2, '--', 'Linewidth', 2)
xlabel('x')
ylabel('y', 'Interpreter', 'latex')
legend('erf', 'erfc', 'Location', 'best')
set(gca,'Fontsize', 16, 'ylim', [-0 1.02], 'xlim', [xaxis(1) xaxis(2)])
saveas(gcf, '..\figures\error_function.png')

%% Plot solution to example 9.24b
N = 1000;
xaxis = [1, 5];
x = linspace(xaxis(1), xaxis(2), N);
y1 =  erfc((x - 1)./0.01)./ x;
y2 =  erfc((x - 1)./0.1) ./ x;
y3 =  erfc((x - 1)./1) ./ x;
y4 =  erfc((x - 1)./10) ./ x;


figure(2)
plot(x, y1, '-', x, y2, '--', x, y3, '-.', x, y4, ':','Linewidth', 2)
xlabel('r/a')
ylabel('$T/T_0$', 'Interpreter', 'latex')
legend('\tau=0.01', '\tau=0.1', '\tau=1', '\tau=10', 'Location', 'best')
%set(gca,'Fontsize', 16, 'ylim', [-0 1.02], 'xlim', [xaxis(1) xaxis(2)])
saveas(gcf, '..\figures\example_924_solution.png')


