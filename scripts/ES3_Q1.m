function [u, ua, X, Y, BigU, BigUExact] = ES3_Q1(nx, ny)
if nargin < 1,
    nx = 20;
    ny = 20;
end;

% Define the domain and derive the mesh
a = 1;      % x limit
xlim = [-a, a];      % x limits
ylim = [0, 3];      % y limits
dx = (xlim(2) - xlim(1))/nx;
dy = (ylim(2) - ylim(1))/ny;
fprintf('mesh (%i by %i): dx=%.4e, dy=%.4e\n', nx, ny, dx, dy);
[X,Y] = meshgrid(xlim(1):dx:xlim(2), ylim(1):dy:ylim(2));

% Set up the inital values
u = 0.0*X;

% now call the SOR solver
u = SOR_ES3(u, dx, dy, X, Y, 0.5e-6);

% compute the analytical solution
ua = analytical_solution_ES3(X, Y);

% Now compute the integral of u^2 using the exact and
% numerical solutions.
BigU=sum(sum(u.^2))*dx*dy;
BigUExact=sum(sum(ua.^2))*dx*dy;
fprintf('int U^2 dxdy is %.4e [exact %.4e]\n', BigU, BigUExact);

% finally output the solution
num_contours = 32;
figure(1)
[C,uval] = contour(X,Y,u, num_contours);
clabel(C,uval);
colormap cool;
title('Exercise sheet 3, Q1c')
xlabel('x');
ylabel('y');
saveas(gcf, '..\figures\ES3_LE1_solution_numerical1.png');

figure(2)
surf(X,Y,u);
title('Exercise sheet 3, Q1c')
xlabel('x');
ylabel('y');
saveas(gcf, '..\figures\ES3_LE1_solution_numerical2.png');
end

%% Define the SOR method
function u = SOR_ES3(u, dx, dy, X, Y,tol)
% determine the size of the problem
[ny, nx] = size(u);

% compute the optimal value of omega
lambda=(cos(pi/nx)+cos(pi/ny))^2/4;
omega=2/(1+sqrt(1-lambda));
fprintf('\nOptimal value of omega=%.4f\n',omega);

% compute the multiplication factor 1/(1+beta^2)
beta=dy/dx;
Ch=1/(2*(1+beta*beta));

% Set up the boundary conditions
T = 10;
u(1,:) = T.*sign(X(1,:)); % south BC
u(ny,:) = 0.0;  % north BC

% zero the iteration counter
it=0;

% set the error to the largest value of head
error = max([max(u), 1]);

% Now do the SOR iteration
while (error > tol)
    % save the current values
    oldu=u;
    % compute the new head value
    u(1, 1) = u(1, 2);          % Neumann BC left/west side
    u(1, nx) = u(1, nx-1);      % Neumann BC right/east side
    for i=2:ny-1
        u(i, 1) = u(i, 2);      % Neumann BC left/west side
        u(i, nx) = u(i, nx-1);  % Neumann BC right/east side
        for j=2:nx-1
            u(i,j)=(1-omega)*oldu(i,j)...
                +omega*Ch*(u(i-1,j)+u(i+1,j)...
                +beta*beta*(u(i,j-1)+u(i,j+1)));
        end
    end

    % compute the error
    error=max(max(abs(oldu(2:ny-1, 2:nx-1)-u(2:ny-1, 2:nx-1))));

    % update the iteration counter
    it=it+1;
end
fprintf('solution converged after %i iterations.\n',it);
end

%% Plotting an analytical solution to the Laplace equation on the semi-infinite
% domain
function u = analytical_solution_ES3(X, Y)

T = 10;     % Temperature at y=0
a = max(max(X));
u = zeros(size(X));
for i=0:21,
    u = u + 1/(2*i+1) .* exp(-(i+0.5)*pi/a.*Y) .* sin((i+0.5)*pi/a.*X);
end;
u = 4*T/pi * u;
% Generate the plot
surf(X,Y,u);
xlabel('x')
ylabel('y')
zlabel('u(x,y)')
title('Exercise sheet 3, Q1b')
saveas(gcf, '..\figures\ES3_LE1_solution.png')
end
