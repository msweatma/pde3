% figures_heat_steady_state.m
% Figure showing different steady state options

%% 
N = 100;
xaxis = [0, 10];
x = linspace(xaxis(1), xaxis(2), N);
y1 = 1 - 2*x/xaxis(2);
y2 = xaxis(1) + 0*x;
y3 = cos(pi*x/(xaxis(2) - xaxis(1)));
y4 = -sinh(3*(2*x - xaxis(2))/(xaxis(2) - xaxis(1)))/sinh(3);

figure(1)
plot(x, y4, '-', x, y2, '--', x, y1, '-.', x, y3, ':', 'Linewidth', 2)
xlabel('x')
ylabel('$T(x, t\to\infty)$', 'Interpreter', 'latex')
legend('Solution 1', 'Solution 2', 'Solution 3', 'Solution 4')
set(gca,'Fontsize', 16, 'ylim', [-1.1 1.1], 'xlim', [xaxis(1) xaxis(2)])
saveas(gcf, '..\figures\heat_steady_state.png')


%% Example 9.22
N = 100;
xaxis = [0, 1];
x = linspace(xaxis(1), xaxis(2), N);
y1 = x.*(2 - x);
y2 = x.*(xaxis(2) - xaxis(1));
y3 = cos(pi*x*0.5/(xaxis(2) - xaxis(1)) + 3*pi/2);
y4 = 0.5+ 0.5*sinh(3*(2*x - xaxis(2))/(xaxis(2) - xaxis(1)))/sinh(3);
y4 = x.^2;

figure(2)
plot(x, y4, '-', x, y2, '--', x, y1, '-.', x, y3, ':', 'Linewidth', 2)
xlabel('x')
ylabel('$T(x, t\to\infty)$', 'Interpreter', 'latex')
legend('Solution 1', 'Solution 2', 'Solution 3', 'Solution 4', 'location', 'northwest')
set(gca,'Fontsize', 16, 'ylim', [-0.1 1.1], 'xlim', [xaxis(1) xaxis(2)])
saveas(gcf, '..\figures\heat_steady_state2.png')
