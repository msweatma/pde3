function u = Jacobi_Dirichlet(u, dx, dy, tol)
% Use the Jacobi iteration to solve the discretisation of the Laplace 
% equation given by u with grid spacings dx and dy to a tolerance of tol.
%
% The boundary conditions of the Laplace equation are given in the top and 
% bottom rows and the left and right columns and must be of the Dirichlet
% type. For Neumann boundary conditions we need to modify the while loop.

% determine the size of the problem
[m,n] = size(u); 
% set the error to 1+tol
error = 1+tol;
% compute the multiplication factor 1/(1+beta^2)
beta = dx/dy;
Cb = 1/(2*(1+beta*beta));
% zero the iteration counter
it = 0;
% Now do the Jacobi iteration
while (error > tol) 
    % compute the new u value
    newu(2:m-1,2:n-1) = Cb*(u(1:m-2,2:n-1)+u(3:m,2:n-1)...
        + beta*beta * (u(2:m-1,1:n-2)+u(2:m-1,3:n)));
       
    % compute the error
    error = max(max(abs(newu(2:m-1,2:n-1)-u(2:m-1,2:n-1))));

    % update head(i,j)
    u(2:m-1,2:n-1) = newu(2:m-1,2:n-1); 

    % update the iteration counter
    it = it+1;
end 
fprintf('Solution converged after %i Jacobi iterations.\n', it);  
end
