function u=GaussSidel(u,dx,dy,tol)
% determine the size of the problem
[m,n]=size(u); 
% set the error to 1+tol
error=1+tol;
% compute the multiplication factor 1/(1+beta^2)
beta=dx/dy;
Cb=1/(2*(1+beta*beta));
% zero the iteration counter
it=0;
% Now do the Gauss-sidel itteration
while (error > tol) 
    % store the current solution
    oldu=u;
    % compute the new u value
    for i=2:m-1
        for j=2:n-1
            u(i,j)=Cb*(u(i-1,j)+u(i+1,j)...
                +beta*beta*(u(i,j-1)+u(i,j+1)));
        end
    end
       
    % compute the error
    error=max(max(abs(oldu(2:m-1,2:n-1)-u(2:m-1,2:n-1))));

    % update the iteration counter
    it=it+1;
end 
fprintf('solution converged after %i iterations.\n',it);  
end

