% Perform the grid convergence analysis for coursework 1 of PDE3
% Coursework 1 PDE3 course 2018-19

function Cw2018_C1_solution
% Set the grid size
nx = 10;
ny = 10;
% Loop over the different grids
f = [];
dx = [];
for i=5:-1:1,
    nx = 5 * 2^i;
    ny = 5 * 2^i;    
    % Solve the system for different grids
    [u, uq, X, Y, BigU] = solve_C1(nx, ny);
    f = [f; BigU];
    dx = [dx; 1/nx];       
end;

% Run the refinement analysis
figure(11)
RefinementAnalysis(dx, f)

%% ------------------------------------------------------ %
% Function to solve the system
function [u, ua, X, Y, BigU] = solve_C1(nx, ny)
if nargin < 1, 
    nx = 20;
    ny = 20;
end;

% Define the domain and derive the mesh 
a = 2;      % x limit
b = 1;      % y limit
xlim = [a, 2*a];    % x limits
ylim = [-b, b];     % y limits
dx = (xlim(2) - xlim(1))/nx;
dy = (ylim(2) - ylim(1))/ny;
fprintf('mesh (%i by %i): dx=%.4e, dy=%.4e\n', nx, ny, dx, dy);
[X,Y] = meshgrid(xlim(1):dx:xlim(2), ylim(1):dy:ylim(2));

% Set up the inital values
u = 0.1*X;

% compute the analytical solution
ua = analytical_solution_C1(X, Y, 20);

% Check convergence of the Fourier series solution
if 0,
    ua_more_terms = analytical_solution_C1(X, Y, 40);
    Fourier_BigU = sum(sum((ua - ua_more_terms).^2))*dx*dy
end;

%% Compute the numerical solution
inhomogeneity = 0;   % Set to 0 for homogeneous and to 1 for inhomogeneous case
u = SOR_C1(u, dx, dy, X, Y, 1e-6, inhomogeneity);

% Check tolerance of SOR
if 0,
    u2 = SOR_C1(u, dx, dy, X, Y, 1e-8, inhomogeneity);
    diff_12 = max(max(u - u2))
end;

inhomogeneity = 1;   % Set to 0 for homogeneous and to 1 for inhomogeneous case
u_inhomogeneous = SOR_C1(u, dx, dy, X, Y, 0.5e-6, inhomogeneity);

% Check that the derivative at the outer boundary is correct
if 0,
    ua_dx = (ua(:,2) - ua(:,1))/dx
    u_dx= (u(:,2) - u(:,1))/dx   
    ua_dx = (ua(:,nx) - ua(:,nx-1))/dx
    u_dx= (u(:,nx) - u(:,nx-1))/dx
end;

% Now compute the integral of u^2 using the exact and
% numerical solutions.
%u = u_inhomogeneous;   % Uncomment to check the convergence of the
% inhomogeneous case
BigU=sum(sum(u.^2))*dx*dy;
BigUExact=sum(sum(ua.^2))*dx*dy;
fprintf('int U^2 dxdy is %.4e [exact %.4e]\n', BigU, BigUExact);

% Plot output: set to 1 for plotting and 0 for no plotting
if 0,
    num_contours = 16;
    % finally output the solution
    figure(1)
    [C,uval] = contour(X,Y,u, num_contours);
    clabel(C,uval);
    colormap cool;
    title('Coursework 1, Q1b numerical contour')
    xlabel('x');
    ylabel('y');
    saveas(gcf, '..\figures\Cw2018_C1_numerical_contour.png');
    
    figure(2)
    surf(X,Y,u, 'EdgeColor','none');
    title('Coursework 1, Q1b numerical surface')
    xlabel('x');
    ylabel('y');
    saveas(gcf, '..\figures\Cw2018_C1_numerical.png');
    
    figure(3)
    [C,uval] = contour(X,Y,ua, num_contours);
    clabel(C,uval);
    colormap cool;
    title('Coursework 1, Q1a analytical contour')
    xlabel('x');
    ylabel('y');
    saveas(gcf, '..\figures\Cw2018_C1_analytical_contour.png');
    
    figure(4)
    surf(X,Y,ua, 'EdgeColor','none');
    title('Coursework 1, Q1a analytical surface')
    xlabel('x');
    ylabel('y');
    saveas(gcf, '..\figures\Cw2018_C1_analytical.png');
    
    figure(5)
    [C,uval] = contour(X,Y,u - ua, num_contours);
    clabel(C,uval);
    colormap cool;
    title('Coursework 1, Q1b numerical minus analytical contour')
    xlabel('x');
    ylabel('y');
    saveas(gcf, '..\figures\Cw2018_C1_Q1b_difference_contour.png');
    
    figure(6)
    surf(X,Y,u - ua, 'EdgeColor','none');
    title('Coursework 1, Q1b numerical minus analytical surface')
    xlabel('x');
    ylabel('y');
    saveas(gcf, '..\figures\Cw2018_C1_Q1b_difference.png');
    
    figure(7)
    [C,uval] = contour(X,Y, u_inhomogeneous, num_contours);
    clabel(C,uval);
    colormap cool;
    title('Coursework 1, Q1c inhomogeneous contour')
    xlabel('x');
    ylabel('y');
    saveas(gcf, '..\figures\Cw2018_C1_inhomogeneous_contour.png');
    
    figure(8)
    surf(X,Y, u_inhomogeneous, 'EdgeColor','none');
    title('Coursework 1, Q1c inhomogeneous surface')
    xlabel('x');
    ylabel('y');
    saveas(gcf, '..\figures\Cw2018_C1_inhomogeneous.png');
    
    figure(9)
    [C,uval] = contour(X,Y,u_inhomogeneous - u, num_contours/2);
    clabel(C,uval);
    colormap cool;
    title('Coursework 1, Q1c inhomogeneous minus homogeneous contour')
    xlabel('x');
    ylabel('y');
    saveas(gcf, '..\figures\Cw2018_C1_Q1c_difference_contour.png');
    
    figure(10)
    surf(X,Y,u_inhomogeneous - u, 'EdgeColor','none');
    title('Coursework 1, Q1c inhomogeneous minus homogeneous')
    xlabel('x');
    ylabel('y');
    saveas(gcf, '..\figures\Cw2018_C1_Q1c_difference.png');
end;

%% Define the SOR method
function u = SOR_C1(u, dx, dy, X, Y, tol, inhomogeneous)
% determine the size of the problem
[ny, nx] = size(u); 

% compute the optimal value of omega
lambda=(cos(pi/nx)+cos(pi/ny))^2/4;
omega=2/(1+sqrt(1-lambda));
fprintf('\nOptimal value of omega=%.4f\n',omega);

% compute the multiplication factor 1/(1+beta^2)
beta=dx/dy;
Ch=1/(2*(1+beta*beta));

% Set up the boundary conditions
T = 10;
u(1,:) = 0; % south BC
u(ny,:) = 0.0;  % north BC

% zero the iteration counter
it=0;

% set the error to the largest value of head
error = max([max(u), 1]);

% Now do the SOR iteration
while (error > tol) 
    % save the current values
    oldu=u;
    % compute the new head value
    u(1, 1) = u(1, 2);          % Neumann BC left/west side
    u(1, nx) = u(1, nx-1) + dx*(1 - Y(1,nx).^2);      % Neumann BC right/east side
    for i=2:ny-1
        u(i, 1) = u(i, 2);      % Neumann BC left/west side
        u(i, nx) = u(i, nx-1) + dx*(1 - Y(i,nx).^2);  % Neumann BC right/east side
        for j=2:nx-1
            u(i,j)=(1-omega)*oldu(i,j)...
                +omega*Ch*(u(i,j-1)+u(i,j+1)...
                +beta*beta*(u(i-1,j)+u(i+1,j))) ...
                - X(1,1)^3*dx^2 * omega * inhomogeneous*Ch*(Y(ny,1) - Y(i,j)).*Y(i,j) * (1 + sign(Y(i,j)))/2;
        end
    end
       
    % compute the error
    error=max(max(abs(oldu(2:ny-1, 2:nx-1)-u(2:ny-1, 2:nx-1))));

    % update the iteration counter
    it=it+1;
end 
fprintf('solution converged after %i iterations.\n',it);


%% Calculate the analytical solution to the Laplace equation 
function u = analytical_solution_C1(X, Y, terms)
a = min(min(X));
b = max(max(Y));
u = zeros(size(X));
for i=1:terms,
    mu = (2*i-1)*pi/2*b;
    B = 2 * (-1)^(i+1) * cosh(mu*(a-X))./(b*mu*sinh(mu*a)) * ((1-b^2)/mu + 2/mu^3);
    u = u + B .* cos(mu.*Y);
end;

% ------------------------------------------------------ %
% Refinement analysis function 
function RefinementAnalysis(x,f)
% perform an analysis of results obtained from a mesh refinement study.
% Several metrics are computed including;
% - order of convergence
% - Richardson extrapolation of the continum value (zero mesh spacing)
% - Grid convergence index

% how many grids have been used ?
N=size(x,1);

fprintf('%i grids have been used.\n\n',N);
fprintf('Grid  Spacing  Function\n');
for i=1:N
    fprintf('%4i %6.3f %13.6f\n',i,x(i),f(i));
end
fprintf('\n\n');

% compute the mesh spacing
r=ones(N-1);
r(1:N-1)=x(2:N)./x(1:N-1);

% Estimate the order of convergence using the first three data
% pairs and assuming that the grid refinement ratio is constant,
% r(1) = r(2).
% This is done using Eqn. 5.10.6.1 of (Roache, 1998).
p = abs(log( abs( f(3) - f(2) ) / abs( f(2) - f(1) ) )  /  log( r(1) ));

fprintf('Order of convergence using the first three finest grids\n');
fprintf('and assuming constant grid refinement.\n');
fprintf('Order of Convergence, p = %f\n\n',p);
 
%Perform Richardson extrapolation to estimate a zero grid value of f.

fexact = f(1) + ( f(1) - f(2) ) / ( r(1)^p - 1.0 );

fprintf('Richardson Extrapolation: Use above order of convergence\n');
fprintf('and first and second finest grids\n');
fprintf('Estimate to zero grid value, f_exact = %f\n\n', fexact);

% Plot the points and the estimate
plot(x,f,'o-',[0],[fexact],'^','LineWidth',2,'MarkerSize',15);
title('Richardson extrapolation');
xlabel('h');
ylabel('f');
saveas(gcf, '..\figures\Lab4_Q1_Richardson_extrapolation.png');

% Compute Grid Convergence Index (GCI) for each fine grid 
% using Eqn. 5.6.1 from Roache's book. Use factor of safety
% as recommended on page 123.
if ( N>2 ) 
   fsafe = 1.25;
else
   fsafe = 3.0;
end

for i=1:N-1
    gcif(i) = fsafe * ( abs( f(i+1) - f(i) ) / abs(f(i)) ) ...
    / ( r(i)^p - 1.0 );
end

fprintf('Grid Convergence Index on fine grids. Using p=%.3f \n',p);
fprintf('Factor of Safety = %.2f\n\n', fsafe);
fprintf('  Grid      Refinement\n');
fprintf('  Step      Ratio, r      GCI\n')
for i=1:N-1
   fprintf(' %2i %2i %13.6f %13.6f\n', i, i+1, r(i), gcif(i)*100.0);
end
fprintf('\n');

% Examine if asymptotic range has been achieved by examining
% ratio of Eqn. 5.10.5.2 of Roache's book is one.
if ( N>2 ) 
   fprintf('Checking for asymptotic range using Eqn. 5.10.5.2.\n');
   fprintf('A ratio of 1.0 indicates asymptotic range.\n\n');
   fprintf('Grid Range   Ratio\n');
   for i = 1:N-2
      ratio = r(i)^p * gcif(i) / gcif(i+1);
      fprintf(' %1i%1i %1i%1i  %13.6f\n', i, i+1, i+1, i+2, ratio);
   end
   fprintf('\n');
end 
fprintf('\n--- End of Report ---\n\n');
