function [u,ua,X,Y] = Lab3_concepTest(ni,nj)
% solve the model problem from Exercise 9.29 using the Jacobi Itteration

% derive the mesh spacing
dx=2/ni;
dy=1/nj;
fprintf('mesh (%i by %i): dx=%.4e, dy=%.4e\n',ni,nj,dx,dy);

% set up a background grid
[X,Y]=meshgrid(0:dx:2,0:dy:1);

% set up the inital values
u=0.0*X;

x = 0:dx:2;
y = 0:dy:1;
% set up the boundary conditions
u(1,:) = 1 + x; % south BC
u(nj+1,:) = 1 - x; % north BC
u(:,1) = 1.0; % west BC
u(:,ni+1) = 3 - 4*sin(3*pi*y./2 + pi);  % East BC

% now call the Gauss-Seidel solver
u=SOR(u,dx,dy,0.5e-6);

% compute the analytical solution
ua=sin(2*pi.*Y).*sinh(2*pi.*X)./sinh(4*pi);

% Now compute the integral of u^2 using the exact and
% numerical solutions.
BigU=sum(sum(u.^2))*dx*dy;
BigUExact=sum(sum(ua.^2))*dx*dy;
fprintf('int U^2 dxdy is %.4e [exact %.4e]\n',BigU,BigUExact);


% finally output the solution
figure(1)
[C,uval] = contour(X,Y,u);
clabel(C,uval)
colormap cool
xlabel('x');
ylabel('y');
zlabel('u(x,y)');
saveas(gcf, '..\figures\Laplace_concepTest_5contour.png')

figure(2)
surf(X, Y, u)
xlabel('x');
ylabel('y');
zlabel('u(x,y)');
saveas(gcf, '..\figures\Laplace_concepTest_5surf.png')
end



