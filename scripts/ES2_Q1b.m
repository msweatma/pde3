% ES2_Q1b.m
% Script plotting a solution to the Laplace equation on a square

% Set the x and y grid points and the meshgrid
N = 20;
x = linspace(0, 1, N);
y = linspace(0, 4, N);
[X,Y] = meshgrid(x, y);

% Define the function and generate the output on the meshgrid
u = @(x, y) 1/16 * (exp(-5*pi.*y) .* sin(5 * pi .* x) - 5 * exp(-3*pi.*y) .* sin(3 * pi .* x) + 10 * exp(-pi.*y) .* sin(pi .* x))
Z = u(X,Y);

% Generate the plot
surf(X,Y,Z)
xlabel('x')
ylabel('y')
zlabel('u(x,y)')
saveas(gcf, '..\figures\ES2_Q1b_solution.png')
