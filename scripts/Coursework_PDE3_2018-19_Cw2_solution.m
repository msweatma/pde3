% Script to solve the 1D heat conduction equation problem in PDE3
% Coursework 2 PDE3 course 2018-19

% Call the function
nx = 80;
[dx, BigU] = solve_problem(nx);

%% Find the analytical and numerical solution for a given number of grid points
function [dx, BigU] = solve_problem(nx);
if nargin < 1, 
    nx = 10;
end;

%% Define the grid and mesh spacing
kappa = 0.1;            % Thermal diffusivity
nu = 10.0;              % Courant number
xlim = [0.0, pi/2];     % x limits
tlim = [0, 4];          % time limits
dx = (xlim(2) - xlim(1))/nx;
fprintf('The grid contains %3i nodes (dx=%.3f).\n', nx+1, dx);

% Calculate the stable time step
dt = nu*dx^2/(2*kappa);
fprintf('dt =%.3e seconds\n', dt);

[X, T] = meshgrid(xlim(1):dx:xlim(2), tlim(1):dt:tlim(2));

%% Compute the analytical solution
ua = analytical_solution(X, T, 80, kappa);

%% Compare different analytical solutions
if 0,
    ua00 = analytical_solution(X, T, 2, kappa);
    ua0 = analytical_solution(X, T, 5, kappa);
    ua1 = analytical_solution(X, T, 10, kappa);
    ua2 = analytical_solution(X, T, 20, kappa);
    ua3 = analytical_solution(X, T, 40, kappa);
    ua4 = analytical_solution(X, T, 80, kappa);
    ua5 = analytical_solution(X, T, 160, kappa);
    ua6 = analytical_solution(X, T, 10000, kappa);

    BigUExact = sum(sum((ua0-ua00).^2./ua0.^2))*dx*dt
    BigUExact = sum(sum((ua1-ua0).^2./ua1.^2))*dx*dt
    BigUExact = sum(sum((ua2-ua1).^2./ua2.^2))*dx*dt
    BigUExact = sum(sum((ua3-ua2).^2./ua3.^2))*dx*dt
    BigUExact = sum(sum((ua4-ua3).^2./ua4.^2))*dx*dt
    BigUExact = sum(sum((ua5-ua4).^2./ua5.^2))*dx*dt
    BigUExact = sum(sum((ua6-ua5).^2./ua6.^2))*dx*dt
    max(max(abs(ua0-ua00)))
    max(max(abs(ua1-ua0)))
    max(max(abs(ua2-ua1)))
    max(max(abs(ua3-ua2)))
    max(max(abs(ua4-ua3)))
    max(max(abs(ua5-ua4)))
    max(max(abs(ua6-ua5)))
end;

%% Compute the numerical solution
N = nx+1; 
x = xlim(1):dx:xlim(2); % generate the grid
fprintf('The grid contains %3i nodes (dx=%.3f).\n', N, dx);

t = 0:dt:4.0;  % time array
U = X;
j = 1;

% Set inital condition
U(1,:) = cos(x); 

% Set the boundary conditions
U(:,1) = exp(-kappa*t);
U(:,N) = t;

% Now solve the equation using the Crank-Nicholson method
Rx = kappa*dt/(dx^2);
for time = dt:dt:4.0
    % Compile the RHS (from the old time level data)
    RHS(1:N-2) = Rx*U(j,1:N-2) + 2*(1-Rx)*U(j,2:N-1) + Rx*U(j,3:N);
    
    % Assemble the A matrix, as an (nx-1) x 3 matrix 
    Amat(1:N-2,1) = -ones(N-2,1)*(Rx); % sub diagonal
    Amat(1:N-2,2) = ones(N-2,1)*2*(1+Rx); %diagonal
    Amat(1:N-2,3) = -ones(N-2,1)*(Rx); % super diagonal

    % Left boundary condition
    RHS(1) = RHS(1) + Rx*U(j+1,1);
    % Right boundary condition
    RHS(N-2) = RHS(N-2) + Rx*U(j+1,N);

    % Solve the system
    U(j+1,2:N-1) = thomas(N-2, Amat, RHS);
    j = j+1;    % Move to next time step
end

fprintf('Done. %4i timesteps time=%.3f seconds\n',j,time);
contourf(X,T,U);

% Now compute the integral of u^2 using the exact and
% numerical solutions.
BigU = sum(sum(U.^2))*dx*dt;
BigUExact = sum(sum(ua.^2))*dx*dt;
fprintf('int U^2 dxdt is %.4e [exact %.4e]\n', BigU, BigUExact);

fprintf('Max difference between analytical and numerical solution %.4e\n', max(max(abs(U-ua))));

% Plot the solutions
num_contours = 30;
figure(1)
[C,uval] = contour(X, T, ua, num_contours);
clabel(C,uval);
colormap cool;
title('Coursework 2, Q1 analytical contour')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u(x,t)', 'Fontsize', 16)
saveas(gcf, '..\figures\Cw2018_C2_analytical_contour.png');

figure(2)
[C,uval] = contour(X, T, U, num_contours);
clabel(C,uval);
colormap cool;
title('Coursework 2, Q1 numerical contour')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u(x,t)', 'Fontsize', 16)
set(gca, 'xlim', [0 pi/2])
saveas(gcf, '..\figures\Cw2018_C2_numerical_contour.png');

figure(3)
surf(X, T, ua, 'EdgeColor', 'none');
title('Coursework 2, Q1 analytical surface')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u_a(x,t)', 'Fontsize', 16)
grid off
set(gca, 'xlim', [0 pi/2])
saveas(gcf, '..\figures\Cw2018_C2_analytical_solution.png');

figure(4)
surf(X, T, U, 'EdgeColor', 'none');
title('Coursework 2, Q1 numerical surface')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u(x,t)', 'Fontsize', 16)
set(gca, 'xlim', [0 pi/2])
saveas(gcf, '..\figures\Cw2018_C2_numerical_solution.png');

figure(5)
[C,uval] = contour(X, T, U - ua, num_contours/1);
clabel(C,uval);
colormap cool;
title('Coursework 2, Q1 numerical - analytical')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u(x,t) - u_a(x,t)', 'Fontsize', 16)
set(gca, 'xlim', [0 pi/2])
saveas(gcf, '..\figures\Cw2018_C2_difference_contour.png');

figure(6)
surf(X, T, U - ua, 'EdgeColor', 'none');
title('Coursework 2, Q1 numerical - analytical')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u(x,t) - u_a(x,t)', 'Fontsize', 16)
set(gca, 'xlim', [0 pi/2])
saveas(gcf, '..\figures\Cw2018_C2_difference_solution.png');
end

%% Analytical solution
function ua = analytical_solution(X, T, N, kappa)
ua = 0.0 * ones(size(X));
ua = 2*X.*T./pi + cos(X) .* exp(-kappa * T);
for i = 1:N,
    ua = ua + 1/(2 * pi * kappa) * (-1)^i/i^3 .* (1 - exp(-4*i^2*kappa.*T)) .* sin(2*i.*X);
end;
end

%% Thomas algorithm
function x = thomas(n, a, b)
% Thomas algorithm for tridiagonal system Ax=b
% a(:,1) is the sub-diagonal, a(:,2) is the diagonal
% and a(:,3) is the super-diagonal. 

% Forward Elimination
for i = 2:n
    em = a(i,1)/a(i-1,2);
    a(i,1) = em;
    a(i,2) = a(i,2)-em*a(i-1,3);
    b(i) = b(i)-a(i,1)*b(i-1);
end;

% Back Substitution
x(n) = b(n)/a(n,2);
for i = n-1:-1:1
    x(i) = (b(i)-a(i,3)*x(i+1))/a(i,2);
end;
end
