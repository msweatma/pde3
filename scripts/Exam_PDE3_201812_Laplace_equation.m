% Script to check the analytical solution of the Laplace equation
% PDE3 exam 201812

dx = [];
f = [];
for i=4:-1:4,
    N = 5 * 2^i;
    [dr, BigU] = solve_problem(N, N);
    dx = [dx; dr];
    f = [f; BigU];
end;
RefinementAnalysis(dx, f)

%% Find the analytical and numerical solution for a given number of grid points
function [dr, BigU] = solve_problem(nr, ntheta);
if nargin < 1, 
    nr = 10;
    ntheta = 10;
end;

save_plot = 0;
%% Define the grid and mesh spacing
a = 2*pi;      % r limit
a = 1;
b = 2;
rlim = [a, b];          % r limits
thetalim = [0, 2*pi];   % theta limits
dr = (rlim(2) - rlim(1))/nr;
dtheta = (thetalim(2) - thetalim(1))/ntheta;
fprintf('mesh (%i by %i): dx=%.4e, dy=%.4e\n', nr, ntheta, dr, dtheta);
[R, Theta] = meshgrid(rlim(1):dr:rlim(2), thetalim(1):dtheta:thetalim(2));

%% Compute the analytical solution
ua = analytical_solution_exam_201812(R, Theta, 30, a, b);

%% Compute the numerical solution
u = SOR_exam_201812(dr, dtheta, R, Theta, 0.5e-6);

% Now compute the integral of u^2 using the exact and
% numerical solutions.
BigU = sum(sum(u.^2))*dr*dtheta;
BigUExact = sum(sum(ua.^2))*dr*dtheta;
fprintf('int U^2 dxdy is %.4e [exact %.4e]\n', BigU, BigUExact);

% finally output the solution
Ncont = 30;
figure(1)
[C,uval] = contour(R.*cos(Theta), R.*sin(Theta), ua, Ncont);
clabel(C,uval);
colormap cool;
title('Exam 201812, Q1 analytical contour')
xlabel('x');
ylabel('y');
zlabel('u(x,y)')
if save_plot,
    saveas(gcf, '..\figures\exam_201812_Q1_analytical_contour.png');
end

figure(2)
[C,uval] = contour(R.*cos(Theta), R.*sin(Theta), u);
clabel(C,uval);
colormap cool;
title('Exam 201812, Q1 numerical contour')
xlabel('x');
ylabel('y');
zlabel('u(x,y)')
if save_plot,
    saveas(gcf, '..\figures\exam_201812_Q1_numerical_contour.png');
end

figure(3)
surf(R.*cos(Theta), R.*sin(Theta), ua);
title('Exam 201812, Q1 analytical')
xlabel('x');
ylabel('y');
zlabel('u(x,y)')
if save_plot,
    saveas(gcf, '..\figures\exam_201812_Q1_analytical_solution.png');
end

figure(4)
surf(R.*cos(Theta), R.*sin(Theta), u);
title('Exam 201812, Q1  numerical')
xlabel('x');
ylabel('y');
zlabel('u(x,y)')
if save_plot,
    saveas(gcf, '..\figures\exam_201812_Q1_numerical_solution.png');
end

figure(5)
surf(R.*cos(Theta), R.*sin(Theta), u - ua);
title('Exam 201812, Q1  numerical minus analytical')
xlabel('x');
ylabel('y');
zlabel('u(x,y)')
if save_plot,
    saveas(gcf, '..\figures\exam_201812_Q1_difference_solution.png');
end
end

%% Analytical solution
function ua = analytical_solution_exam_201812(R, Theta, N, a, b)
ua = -24/log(2)*(3*pi - pi^2)/12 * log(R);
ua = pi^2/(12*log(2)) * log(R);
for i=1:N,
    k = 2*i-1;
    m = 2*i;
    ua = ua + 4 * (R.^k - R.^(-k)) ./(pi *(2^k - 2^(-k))* k^3) * (-1)^(i-1) .* cos(k.*Theta);
    ua = ua + 2 * (R.^m - R.^(-m)) ./((2^m - 2^(-m))* m^2) * (-1)^(i-1) .* cos(m.*Theta);
end;
end

%% SOR solver
function [u, iter, omega] = SOR_exam_201812(dr, dtheta, R, Theta, tol)
u = 0.0 * R;    % Initial values
[ntheta, nr] = size(u);
% Set Dirichlet boundary conditions
for i=1:ntheta,
    if Theta(i,nr) <= pi/2,
        u(i, nr) = pi^2/4 - Theta(i, nr).^2; % Outside BC
    else if Theta(i,nr) >= 3*pi/2,
            u(i, nr) = pi^2/4 - (Theta(i, nr)-2*pi).^2; % Outside BC
        else
            u(i, nr) = 0;
        end
    end
end
            
% Set value at theta=0 and theta=2pi to 0.5
%u(1, nr) = 0.5;     
%u(ntheta, nr) = 0.5;

% Compute the optimal value of omega: technically only correct
% for cartesian coordinates
lambda = (cos(pi/nr) + cos(pi/ntheta))^2/4;
omega = 2/(1+sqrt(1-lambda));
% set the error to the largest value of head
error=max(max(u));

% Ratio of step sizes
beta=dr/dtheta;

% zero the iteration counter
iter = 0;

% Now do the SOR iteration
while (error > tol)
    oldu = u;       % save the current values

    % Set the inner boundary value
    for i=1:ntheta,  
        u(i,1) = 0;
    end;

    for j=2:nr-1,  % Loop over all values of r which are inside the solution domain
        Ch = 1/(2 + dr/R(1,j) + 2 * beta * beta/R(1,j)^2);
        
        % Special treatment for theta=0 due to the 2pi periodicity
        i = 1;      % Index of the angles for which Theta(i,:)=0
        u(i,j) = (1-omega) * oldu(i,j) ...
                + omega*Ch * (u(i,j+1) * (1 + dr/R(i,j)) + u(i,j-1)  ...
                + beta*beta/(R(i,j) * R(i,j)) * (u(i+1,j) + u(ntheta-1,j)));
            
        % Loop over all angles except for theta=0 and theta=2pi
        for i=2:ntheta-1,
            u(i,j) = (1-omega) * oldu(i,j) ...
                + omega*Ch * (u(i,j+1) * (1 + dr/R(1,j)) + u(i,j-1)  ...
                + beta*beta/(R(1,j) * R(1,j)) * (u(i+1,j) + u(i-1,j)));
        end;
        u(ntheta,:) = u(1,:);   % Set the theta=2pi value to the theta=0 value
    end;
    
    % Compute the error
    error = max(max(abs(oldu(2:ntheta-1, 2:nr-1)-u(2:ntheta-1, 2:nr-1))));
    % Update the iteration counter
    iter = iter + 1;
end
fprintf('solution converged after %i iterations.\n', iter);
end
