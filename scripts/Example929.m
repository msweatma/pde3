function [u_SOR, ua, X, Y] = Example929(ni, nj);
% Solve the model problem from Exercise 9.29 using the Jacobi and SOR 
% methods
% 
% The function asks for the grid size in the x and y 
% direction if both aren't given
if nargin < 2
    ni=input('Enter the grid size in the x direction: ');
    nj=input('Enter the grid size in the y direction: ');
end

% derive the mesh spacing
dx=2/ni;
dy=1/nj;
fprintf('Mesh (%i by %i): dx=%.4e, dy=%.4e\n', ni, nj, dx, dy);

% set up a background grid
[X,Y] = meshgrid(0:dx:2,0:dy:1);

% set up the inital values
u = 0.0*X;

% set up the boundary conditions
u(1,:) = 0.0;       % south BC
u(nj+1,:) = 0.0;    % north BC
u(:,1) = 0.0;       % west BC
u(:,ni+1) = sin(2*pi*Y(:,ni+1));  % east BC

% now call the Gauss-Seidel and Jacobi solvers
u_SOR = SOR_Dirichlet(u, dx, dy, 0.5e-6);
u_Jacobi = Jacobi_Dirichlet(u, dx, dy, 0.5e-6);

% compute the analytical solution
ua = sin(2*pi.*Y).*sinh(2*pi.*X)./sinh(4*pi);

% Now compute the integral of u^2 using the exact and
% numerical solutions.
BigU = sum(sum(u_SOR.^2))*dx*dy;
BigUExact = sum(sum(ua.^2))*dx*dy;
fprintf('int U^2 dxdy is %.4e [exact %.4e]\n', BigU, BigUExact);

% finally output the solution
figure(1)
[C,uval] = contour(X, Y, u_SOR);
clabel(C, uval);
colormap cool
title('Example 9.29 SOR Method');
xlabel('x (meters)');
ylabel('y (meters)');

figure(2)
[C,uval] = contour(X, Y, u_Jacobi);
clabel(C, uval);
colormap cool
title('Example 9.29 Jacobi Method');
xlabel('x (meters)');
ylabel('y (meters)');

end