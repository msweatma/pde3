% PDE3 exam 201712:
% Script to plot a series solution of the heat conduction problem
function exam_201712_heat_conduction(nx, ny)
if nargin < 1, 
    nx = 15;
    ny = 100;
end;

%% Define the grid and mesh spacing
xlim = [0.0, 1];        % x limits
ylim = [0, 10];          % y limits
dx = (xlim(2) - xlim(1))/nx;
dy = (ylim(2) - ylim(1))/ny;
fprintf('The grid contains %3i nodes (dx=%.3f).\n', nx+1, dx);
[X, Y] = meshgrid(xlim(1):dx:xlim(2), ylim(1):dy:ylim(2));

%% Compute the analytical solution
ua = analytical_exam_201712_1(X, Y, 200);

% finally output the solution
figure(1)
[C,uval] = contour(X, Y, ua);
clabel(C,uval);
colormap cool;
title('Coursework 2, Q1 analytical contour')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u(x,t)', 'Fontsize', 16)
%saveas(gcf, '..\figures\analytical_contour.png');

figure(2)
surf(X, Y, ua, 'EdgeColor', 'none');
title('Coursework 2, Q1 analytical surface')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u_a(x,t)', 'Fontsize', 16)
grid off
%saveas(gcf, '..\figures\analytical_solution.png');

figure(3)
x = xlim(1):dx:xlim(2);
plot(ua(1,:) - (1 + x - x.^2))

%% Analytical solution
function ua = analytical_exam_201712_1(X, Y, N)
ua = 1.0 * ones(size(X));
for i=1:N,
    lam = (i-0.5)*pi;
    ua = ua + exp(-0.1*lam*lam.*Y) .* (4/lam^3*(-1)^(i+1) - 2/lam^2) .* cos(lam.*X);
end;