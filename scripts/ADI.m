function ADI
disp('ADI method for the 2D transient groundwater equations');
% This program implements the ADI scheme for solving the transient ground
% water equations.  There is a basic driver which sets up the test problem
% and then calls the function ADIsolver to integrate the problem before
% producing a contour plot.
%
% (c) 2005 DM INGRAM
%
global CourantNo beta
% determine the problem size
nj=input('enter the grid size in the x direction :');
ni=input('enter the grid size in the y direction :');
length=100.0;
%
Transmisitivity=0.02; 
StorageCoeficient=0.002;
statichead=10; % 10 m head
% Compute beta
beta=StorageCoeficient/Transmisitivity;

% determine the Courant number
CourantNo=input('enter the Courant number :');

% derive the grid size and line gradient
dx=length/ni;
dy=length/nj;

% setup the grid
[X,Y]=meshgrid(0.0:dx:length,0.0:dy:length);

% initialise the head array first row set to head profile
head=ones(size(X))*statichead;

tstop=input('enter the stopping time :');

head=ADIsolver(head,dx,dy,tstop);

% finally output the solution
[C,h] = contour(X,Y,head);
clabel(C,h)
colormap cool
title('2D transient aquifer problem')
xlabel('x (meters)');
ylabel('y (meters)');

function h=boundaryconditions(h,t)
    h(1,:)=1.0;
    h(:,1)=1.0;

function h=ADIsolver(h,dx,dy,tstop)
global CourantNo beta
% Two dimensional operator split explicit time marching method.
dt=CourantNo*min([dx^2/(2*beta) dy^2/(2*beta)]);
fprintf('ADI with dt=%.3f\n',dt);

% time the inner loop
starttime=cputime;

for t=dt:dt:tstop
   h=boundaryconditions(h,t); % call a funcion which applies BC
   h=XSweep(h,dt,dx,dy);  % ADI in the X direction
   h=YSweep(h,dt,dx,dy);  % and in the Y direction
end
% stop timing and report
timetaken=cputime-starttime;
fprintf('Done. ADI solver took %.3f seconds.\n',timetaken);

function h1=XSweep(h0,dt,dx,dy)
global beta
% one dimensional ADI sweep in the x direction.
[n,m]=size(h0); % how big is the head array ?
h1=h0;

Rx=beta*dt/dx^2;
Ry=beta*dt/dy^2;

% loop over the rows
for j=2:m-1
    % first compile the RHS, from the old time level data   
    RHS(1:n-2)=h0(2:n-1,j)+...
        (Ry/2)*(h0(2:n-1,j-1)-2*h0(2:n-1,j)+h0(2:n-1,j+1));  
    RHS(1)=RHS(1)+Rx*h0(1,j)/2;
    RHS(n-2)=RHS(n-2)+Rx*h0(n,j)/2;

    % now assemble the A matrix, it is stored as an n by three matrix 
    Amat(1:n-2,1)=ones(n-2,1)*(-Rx/2);    % sub diagonal
    Amat(1:n-2,2)=ones(n-2,1)*(1+Rx); % diagonal
    Amat(1:n-2,3)=ones(n-2,1)*(-Rx/2);  % super diagonal

    % solve the system
    h1(2:n-1,j)=thomas(n-2,Amat,RHS);
end

function h1=YSweep(h0,dt,dx,dy)
global beta
% one dimensional ADI sweep in the y direction.
[n,m]=size(h0); % how big is the head array ?
h1=h0;

Rx=beta*dt/dx^2;
Ry=beta*dt/dy^2;

% loop over the columns
for i=2:n-1
    % first compile the RHS, from the old time level data   
    RHS(1:m-2)=h0(i,2:m-1)+...
        (Rx/2)*(h0(i-1,2:m-1)-2*h0(i,2:m-1)+h0(i+1,2:m-1));  
    RHS(1)=RHS(1)+Rx*h0(i,1)/2;
    RHS(m-2)=RHS(n-2)+Rx*h0(i,m)/2;

    % now assemble the A matrix, it is stored as an n by three matrix 
    Amat(1:m-2,1)=ones(m-2,1)*(-Ry/2);    % sub diagonal
    Amat(1:m-2,2)=ones(m-2,1)*(1+Ry); % diagonal
    Amat(1:m-2,3)=ones(m-2,1)*(-Ry/2);  % super diagonal

    % solve the system
    h1(i,2:m-1)=thomas(m-2,Amat,RHS);
end

function x=thomas(n,a,b)
%thomas algorithm for tridiagonal system Ax=b

%Forward Elimination
for i=2:n
    em=a(i,1)/a(i-1,2);
    a(i,1)=em;
    a(i,2)=a(i,2)-em*a(i-1,3);
    b(i)=b(i)-a(i,1)*b(i-1);
end

%Back Substitution
x(n)=b(n)/a(n,2);
for i=n-1:-1:1
    x(i)=(b(i)-a(i,3)*x(i+1))/a(i,2);
end
