% figures_Laplace_derivation.m - Figure showing the derivation of the
% Laplace equation

%% 2D domain
xbox = [0, 5];
ybox = [0, 5];
xaxis = [0, 5.5];
yaxis = [0, 5.5];

figure(1)
hold on
rectangle('Position',[xbox(1) ybox(1) xbox(2) ybox(2)], 'Linewidth', 2)

% Remove standard axes and replace with ones through (0,0)
set(gca, 'box', 'off', 'Fontsize', 14, 'Linewidth', 2, 'xtick', [], 'ytick', [])
% Generate arrows dependent on the axis size
xsize = [(xaxis(2)-xaxis(1))/25, (yaxis(2) - yaxis(1))/50];
ysize = [(xaxis(2)-xaxis(1))/50, (yaxis(2) - yaxis(1))/25];
axis([xaxis(1) xaxis(2)-ysize(1) yaxis(1) yaxis(2)-xsize(2)])   % Fix the axes sizes
patch(...
    [xaxis(2)-xsize(1), -ysize(1); xaxis(2)-xsize(1), +ysize(1); xaxis(2), yaxis(1)], ...
    [xsize(2), yaxis(2)-ysize(2); -xsize(2), yaxis(2)-ysize(2); xaxis(1), yaxis(2)], ...
    'k', 'clipping', 'off')

% Axis labels
offset = 0.3
text(-0.1, -offset, '0', 'fontweight', 'bold', 'Fontsize', 16)     
text(-offset, 0, '0', 'fontweight', 'bold', 'Fontsize', 16)    
text(xbox(2) - 0.1, -offset, 'L', 'fontweight', 'bold', 'Fontsize', 16)     
text(xaxis(1) - offset, ybox(2), 'H', 'fontweight', 'bold', 'Fontsize', 16)    
text(xaxis(2), -offset, 'x', 'fontweight', 'bold', 'Fontsize', 16)     
text(-offset, yaxis(2), 'y', 'fontweight', 'bold', 'Fontsize', 16)    

% Boundaries
text((xbox(2) - xbox(1))/1.7, ybox(1) - offset, 'T(x, 0)', 'fontweight', 'bold', 'Fontsize', 16)
text((xbox(2) - xbox(1))/1.7, ybox(2) + offset, 'T(x, H)', 'fontweight', 'bold', 'Fontsize', 16)
text(xbox(1) - 0.85, (ybox(2) - ybox(1))/1.7, 'T(0, y)', 'fontweight', 'bold', 'Fontsize', 16)
text(xbox(2) + 0.1, (ybox(2) - ybox(1))/1.7, 'T(L, y)', 'fontweight', 'bold', 'Fontsize', 16)
% Uncomment to have Neumann BC
%text(xbox(2) + 0., (ybox(2) - ybox(1))/1.7, '$$\frac{\partial T(L, y)}{\partial x}$$', 'fontweight', 'bold', 'Fontsize', 16, 'Interpreter', 'latex')
text(1.1, 1.5, 'T(x, y)?', 'fontweight', 'bold', 'Fontsize', 14)

saveas(gcf, '..\figures\Laplace_derivation.png')

%% Add small solution area
x0 = [1 2];
y0 = [1 2];

line([x0(1) x0(1)], [ybox(1) ybox(2)], 'Linestyle', '--', 'Color', 'k')
line([x0(2) x0(2)], [ybox(1) ybox(2)], 'Linestyle', '--', 'Color', 'k')
text(x0(1), -offset, 'x_0', 'fontweight', 'bold', 'Fontsize', 16)
text(x0(2), -offset, 'x_1', 'fontweight', 'bold', 'Fontsize', 16)

line([xbox(1) xbox(2)], [y0(1) y0(1)], 'Linestyle', '--', 'Color', 'k')
line([xbox(1) xbox(2)], [y0(2) y0(2)], 'Linestyle', '--', 'Color', 'k')
text(-offset, y0(1), 'y_0', 'fontweight', 'bold', 'Fontsize', 16)
text(-offset, y0(2), 'y_1', 'fontweight', 'bold', 'Fontsize', 16)

saveas(gcf, '..\figures\Laplace_derivation2.png')

%% Add heat flow
xflow = [0.5, 1, 2, 2.5]
yflow = [1.5, 1.5]

arrow([xflow(1), 1.5], [xflow(2), 1.5])
text(xflow(1)-offset-0.1, 1.2, 'Q(x_0, y)', 'fontweight', 'bold', 'Fontsize', 14)
arrow([xflow(3), 1.5], [xflow(4), 1.5])
text(xflow(4)-offset, 1.2, 'Q(x_1, y)', 'fontweight', 'bold', 'Fontsize', 14)

arrow([1.5, xflow(1)], [1.5, xflow(2)])
text(1.1, xflow(1)-0.2, 'Q(x, y_0)', 'fontweight', 'bold', 'Fontsize', 14)
arrow([1.5, xflow(3)], [1.5, xflow(4)])
text(1.1, xflow(4)+0.2, 'Q(x, y_1)', 'fontweight', 'bold', 'Fontsize', 14)

saveas(gcf, '..\figures\Laplace_derivation3.png')
