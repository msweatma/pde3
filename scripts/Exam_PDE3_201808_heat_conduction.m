% Script to plot the solution of exam 201808
function figure_exam_201808;
if nargin < 1, 
    nx = 20;
    nt = 40;
end;

%% Define the grid and mesh spacing
xlim = [0, 1];          % x limits
tlim = [0, 2];         % t limits
dx = (xlim(2) - xlim(1))/nx;
dt = (tlim(2) - tlim(1))/nt;
fprintf('mesh (%i by %i): dx=%.4e, dy=%.4e\n', nx, nt, dx, dt);
[X, T] = meshgrid(xlim(1):dx:xlim(2), tlim(1):dt:tlim(2))

%% Compute the analytical solution
ua = analytical_solution(X, T, 20);


% finally output the solution
figure(1)
[C,uval] = contour(X, T, ua);
clabel(C,uval);
colormap cool;
title('Exam analytical')
xlabel('x');
ylabel('t');
zlabel('u(x,t)')
saveas(gcf, '..\figures\exam_201808_analytical_contour.png');

figure(2)
surf(X, T, ua);
title('Exam analytical')
xlabel('x');
ylabel('t');
zlabel('u(x,t)')
saveas(gcf, '..\figures\exam_201808_analytical_solution.png');

figure(3)
x = xlim(1):dx:xlim(2);
plot(x, (1-x).^2, x, ua(1,:), '--', 'Linewidth', 2)

%% Analytical solution
function ua = analytical_solution(X, T, N)
kappa = 1;
ua = ones(size(X));
for i=1:N,
    lambda = (i-0.5)*pi;
    ua = ua - exp(-kappa*lambda^2.*T) * 4/lambda^3 .* sin(lambda .* X);
end;
