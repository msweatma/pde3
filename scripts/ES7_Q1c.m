% Script file for the solution of exercise sheet 7
clear;

Nr = 20;
dt = 0.001;
tstop = 0.1;

a = 1;
b = 2;
T0 = 2;
kappa = 0.3;
dr = (b - a)/(Nr-1);    % Mesh spacing
r = a:dr:b;     % Generate the grid
fprintf('The grid contains %3i nodes (dx=%.3f).\n', Nr+1, dr);

% Set the initial conditions
T = zeros(1, Nr);
T(1:Nr) = 0;

T_total = [];
T_analytical = [analytical_solution(0, r, 1000, a, b, kappa, T0)];
% Solve the equation using a time marching method.
for time=dt:dt:tstop,
% Set the boundary conditions
T(1) = T0;          % Dirichlet
T(Nr) = 0;          % Dirichlet

% Add current solution to the total solution array
T_analytical = [T_analytical; analytical_solution(time, r, 1000, a, b, kappa, T0)];
T_total = [T_total;T];  

T = FTCS(T, dt, r, kappa);  % Call the solver
end
T_total = [T_total;T];  

time = 0:dt:tstop;
figure(1);
surf(r, time, T_total);
title('ES7, Q1c numerical surface')
xlabel('r')
ylabel('t')
zlabel('T(r,t)')
%saveas(gcf, '..\figures\ES7_Q1c_numerical_solution.png')

figure(2);
surf(r, time, T_analytical);
title('ES7, Q1c analytical surface')
xlabel('r')
ylabel('t')
zlabel('T(r,t)')
%saveas(gcf, '..\figures\ES7_Q1c_analytical_solution.png')

figure(3);
surf(r, time, T_analytical - T_total);
title('ES7, Q1c difference surface')
xlabel('r')
ylabel('t')
zlabel('T(r,t)')
%saveas(gcf, '..\figures\ES7_Q1c_difference_surface.png')


function c1 = FTCS(c0, dt, r, kappa)
% This is the forward time centred space scheme for the
% 1D diffusion equation with kappa=1
if nargin < 4,
    kappa = 0.01;
end
dr = min(diff(r));
n = size(c0,2); % Get the size of the problem
Rr = kappa * dt/dr;
Rrr = kappa * dt/(dr^2); % Set the constant multiplier
c1 = c0;        % Set c1 to the same values as c0
% Recalculate the inner values
c1(2:n-1) = c0(2:n-1) + Rrr * (c0(1:n-2) - 2*c0(2:n-1) + c0(3:n)) + Rr * (c0(3:n) - c0(1:n-2))./r(2:n-1);
end

% Function to calculate the analytical solution
function T = analytical_solution(t, r, N, a, b, kappa, T0)
    T = (b - r)./(b - a);
    for i=1:N,
        T = T - 2/(pi*i) * exp(-kappa*(i*pi/(b-a))^2*t) * sin((r-a)/(b-a) * i * pi);
    end;
    T = T .* a*T0./r;
end

