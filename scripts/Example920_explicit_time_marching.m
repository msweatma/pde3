%function [x, T] = Example920_explicit_time_marching(Nx, dt, tstop)
% Solve the model problem from Exercise 9.20 using
% explicit time marching
%
% The function asks for the grid size in the x, the
% time step and the final time. If all three are not
% given we use the following default values.
if 1<2,%nargin < 3,
    Nx = 20;
    dt = 0.001;
    tstop = 0.1;
end;

%% Generate the grid
N = Nx + 2;         % Include ghost points (i=1 and i=N)
dx = 1.0/(Nx-1);    % Mesh spacing
x = 0.0:dx:1.0;     % Generate the grid
fprintf('The grid contains %3i nodes (dx=%.3f).\n',N-1,dx);
[X, tt] = meshgrid(x, 0:dt:tstop);

%% Set the initial conditions
T = zeros(1, N);
T(2:N-1) = sin(3*pi*x/2);

%% Solve the equation using a time marching method.
T_total = [];
T_total = [T_total; T];
for time=dt:dt:tstop,
    % Set the boundary conditions
    T(1) = 0.0;         % Dirichlet
    T(N) = T(N-2);      % No flux boundary condition
    T = FTCS(T, dt, dx);  % Call the solver
    % Add current solution to the total solution array
    T_total = [T_total; T];
end

%% Plot the solution
figure(1)
h = surf(X, tt, T_total(:, 2:end-1));
xlabel('x')
ylabel('t')
zlabel('T')
set(h,'edgecolor','none')

figure(2)
contour(X, tt, T_total(:, 2:end-1))
xlabel('x')
ylabel('t')
%end

function c1 = FTCS(c0, dt, dx, kappa)
% This is the forward time centred space scheme for 
% the 1D diffusion equation with kappa=1
if nargin < 4,
    kappa = 1.0;
end
n = size(c0,2); % Get the size of the problem
Rx = kappa * dt/(dx^2); % Set the constant multiplier
c1 = c0;
c1(2:n-1) = c0(2:n-1) + Rx * (c0(1:n-2) - 2*c0(2:n-1) + c0(3:n));
end