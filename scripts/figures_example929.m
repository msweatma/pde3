% figures_example929.m
% Script plotting a solution to the Laplace equation on a square from
% example 9.29

% Set the x and y grid points
N = 20;
x = linspace(0, 2, N);
y = linspace(0, 1, N);

% Define the function
u = @(x, y) sin(2*pi.*y) .* sinh(2*pi.*x)./ sinh(4*pi);

% Generate the plot
[X,Y] = meshgrid(x, y);
Z = u(X,Y);
surf(X,Y,Z)
xlabel('x')
ylabel('y')
zlabel('u(x,y)')
saveas(gcf, '..\figures\example_929.png')
