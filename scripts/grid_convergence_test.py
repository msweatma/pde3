import numpy as np
from refinement_analysis import refinement_analysis

# lists containing the mesh spacing, dx, and function values, f.
dx = [0.0005, 0.0010, 0.0020, 0.0038]
fun = [11.0206, 10.9435, 10.7873, 10.4832]

# compile the mesh refinement analysis
test  = refinement_analysis(dx,fun)

# report the grid convergence analysis
test.report()

# calculate the richardson extrapolation estimate of the function on a
# grid with dx=0.0 
print('f(0) = {:.6g}'.format(test.richardson()))

# produce a plot which includes the richardson extrapolation estimate
# and also has the y-axis labeled with the definition of f used in the
# refinement analysis.
test.plot(True, '$\int_\Omega T^2\ d\Omega$')

