# Python solution for coursework 1 for PDE3 2020-21
# Solution by Daniel Friedrich
# The Jupyter notebook contains the same solution

# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  

import os
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.ticker as ticker
import numpy as np
from refinement_analysis import refinement_analysis

# Analytical solution
def analytical_solution(R, Theta, n_fourier):
    ua = 1/2 + np.log(2) - np.log(R)
    for k in range(1, n_fourier):
        ua = ua + 2 * (-1)**(k-1)/((2*k-1) * np.pi * (2**(2*k-1) + 2**(-2*k+1))) * (R**(2*k-1) + R**(-2*k+1)) * np.cos((2*k-1) * Theta)

    return ua


## SOR solver for the given problem
def SOR(dr, dtheta, R, Theta, tol, inhomogeneous = 0):

    u = R - 1    # Initial values
    np.ones(np.shape(R))
    [ntheta, nr] = np.shape(R)

    # Set Dirichlet boundary conditions at outer boundary
    for i in range(0, ntheta):
        if Theta[i, 0] < 0.5*np.pi or Theta[i, 0] >= 1.5 * np.pi:
            u[i, nr-1] = 1
        else:
            u[i, nr-1] = 0

    # Compute the optimal value of omega: technically only correct
    # for cartesian coordinates
    lam = (np.cos(np.pi/nr) + np.cos(np.pi/ntheta))**2/4
    omega = 2/(1+np.sqrt(1-lam))

    # set the error to the largest value of head
    error = np.max(np.max(u))

    # Ratio of step sizes
    beta = dr/dtheta;

    # zero the iteration counter
    it = 0;

    # Now do the SOR iteration
    while (error > tol):
        oldu = np.copy(u)       # save the current values

        # Neumann BC at the inner boundary
        for i in range(0, ntheta):
            u[i, 0] = u[i,1] + dr

        for j in range(1, nr-1):       # Loop over r
            Ch = 1/(2 + 2 * beta * beta/R[1,j]**2 + dr/R[1,j])
            # Special treatment for theta=0 due to the 2pi periodicity
            i = 0

            inhom = -inhomogeneous * (R[i, j] - 1) * np.cos(Theta[i, j])
            u[i,j] = (1-omega) * oldu[i,j] + omega*Ch * (u[i,j+1] * (1 + dr/R[i,j]) + u[i,j-1] 
                                    + beta*beta/(R[i,j] * R[i,j]) * (u[i+1,j] + u[ntheta-1,j])
                                    - inhom * dr**2) 

            for i in range(1,ntheta-1):
                if Theta[i, 0] >= 0.5*np.pi and Theta[i, 0] < 1.5 * np.pi:
                    inhom = -inhomogeneous * (R[i, j] - 1) * np.cos(Theta[i, j])
                else:
                    inhom = 0

                u[i,j] = (1-omega) * oldu[i,j] + omega*Ch * (u[i,j+1] * (1 + dr/R[1,j]) + u[i,j-1] 
                                    + beta*beta/(R[1,j] * R[1,j]) * (u[i+1,j] + u[i-1,j])
                                    - inhom * dr**2) 

            u[ntheta-1, :] = u[0,:]   # Set the 2pi value to the 0 value        

        # compute the error
        error = np.max(np.max(np.abs(oldu[1:ntheta-2, 1:nr-2]-u[1:ntheta-2, 1:nr-2])))
        # update the iteration counter
        it = it + 1;

    print(f"Solution converged after {it} iterations and error {error}")

    return u 

def plot_surf(u, R, Theta, filename, title):
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # Plot the surface
    surf = ax.plot_surface(R*np.cos(Theta), R*np.sin(Theta), u, cmap=cm.coolwarm,
                        linewidth=0, antialiased=False)

    # Set view angle, labels and axis ticks
    ax.view_init(elev=20., azim=230)
    ax.set_xlabel('x axis')
    ax.set_ylabel('y axis')
    ax.set_zlabel(r'u(r,$\theta$)')
    ax.set_title(title)
    ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(1))

    # Add a color bar which maps values to colors
    fig.colorbar(surf, shrink=0.5, aspect=5)

    absolute_path = os.path.join(os.getcwd(), "..", "figures", filename)
    plt.savefig(absolute_path)
    plt.show()

def plot_contour(u, R, Theta, filename, title):
    # Contour plot
    fig = plt.figure()
    ax = fig.gca()

    cont = ax.contour(R*np.cos(Theta), R*np.sin(Theta), u, levels=16)

    ax.set_xlim(-2, 2.01)
    ax.set_ylim(-2, 2.01)
    ax.axis('equal')
    ax.set_aspect('equal', 'box')

    ax.set_title(title)
    ax.set_xlabel('x axis')
    ax.set_ylabel('y axis')
    ax.clabel(cont, inline=1, fontsize=10)

    absolute_path = os.path.join(os.getcwd(), "..", "figures", filename)
    plt.savefig(absolute_path)
    plt.show()

def solve_problem(nr, ntheta, plot_flag=False):
    """
    Function to calculate the analytical and numerical solution for a given grid.
    """
    n_fourier = 200      # Number of terms in the Fourier series
    tol = 1e-6          # Tolerance for the SOR solver

    # Define the grid and function values
    r = np.linspace(1, 2, nr, endpoint=True)
    theta = np.linspace(0, 2*np.pi, ntheta, endpoint=True)
    dr = (r[-1] - r[0])/nr
    dtheta = (theta[-1] - theta[0])/ntheta
    R, Theta = np.meshgrid(r, theta)

    # Calculate the analytical solution
    ua = 1/2 + np.log(2) - np.log(R)
    for k in range(1, n_fourier):
        ua = ua + 2 * (-1)**(k-1)/((2*k-1) * np.pi * (2**(2*k-1) + 2**(-2*k+1))) * (R**(2*k-1) + R**(-2*k+1)) * np.cos((2*k-1) * Theta)

    # Check Fourier series convergence
    if 0:
        n_fourier = 500
        ua2 = analytical_solution(R, Theta, n_fourier)

        diff_ua = np.max(np.max(np.abs(ua - ua2)))
        print("Max difference between two Fourier series cut-offs", diff_ua)

    # Calculate the homogeneous numerical solution
    u = SOR(dr, dtheta, R, Theta, tol)
    BigU = np.sum(np.sum(u**2 * R)) * dr * dtheta

    # Check tolerance of SOR
    if 0:
        tight_tol = 1e-8
        u2 = SOR(dr, dtheta, R, Theta, tight_tol);
        diff_12 = np.max(np.max(np.abs(u - u2)))
        print("Max difference between two tolerance levels", diff_12)

    # Calculate the inhomogeneous solution
    ui = SOR(dr, dtheta, R, Theta, tol, inhomogeneous=1)
    BigUi = np.sum(np.sum(ui**2 * R)) * dr * dtheta


    # Plot the solutions
    if plot_flag:
        plot_surf(ua, R, Theta, "CW1_PDE3_2020-21_Analytical_solution_surf.png", 
                    "Analytical solution")
        plot_contour(ua, R, Theta, "CW1_PDE3_2020-21_Analytical_solution_contour.png", 
                    "Analytical solution contour")
        plot_surf(u, R, Theta, "CW1_PDE3_2020-21_Numerical_solution_surf.png",
                    "Numerical solution")
        plot_contour(u, R, Theta, "CW1_PDE3_2020-21_Numerical_solution_contour.png",
                    "Numerical solution contour")              
        plot_surf(u - ua, R, Theta, "CW1_PDE3_2020-21_Difference_solution_surf.png",
                    "Numerical minus analytical solution")
        plot_contour(u - ua, R, Theta, "CW1_PDE3_2020-21_Difference_solution_contour.png",
                    "Numerical minus analytical solution contour")

        plot_surf(ui, R, Theta, "CW1_PDE3_2020-21_Numerical_inhomogeneous_solution_surf.png",
                    "Numerical inhomogeneous solution")
        plot_contour(ui, R, Theta, "CW1_PDE3_2020-21_Numerical_inhomogeneous_solution_contour.png",
                    "Numerical inhomogeneous solution contour")              
        plot_surf(ui - u, R, Theta, "CW1_PDE3_2020-21_Difference_inhomogeneous_solution_surf.png",
                    "Numerical inhomogeneous minus homogeneous solution")
        plot_contour(ui - u, R, Theta, "CW1_PDE3_2020-21_Difference_inhomogeneous_solution_contour.png",
                    "Numerical inhomogeneous minus homogeneous solution contour")

    return dr, BigU, BigUi

dx = []
f = []
fi = []
for i in range(4, 0, -1):
    N = 5 * 2**i
    print(N)
    [dr, BigU, BigUi] = solve_problem(N, N, False)
    dx.append(dr)
    f.append(BigU)
    fi.append(BigUi)

# compile the mesh refinement analysis
test  = refinement_analysis(dx, f)
testi  = refinement_analysis(dx, fi)

# report the grid convergence analysis
test.report()
testi.report()

# calculate the richardson extrapolation estimate of the function on a
# grid with dx=0.0 
print('f(0) = {:.6g}'.format(test.richardson()))

# produce a plot which includes the richardson extrapolation estimate
# and also has the y-axis labeled with the definition of f used in the
# refinement analysis.
test.plot(True, '$\int_\Omega u^2\ d\Omega$')
