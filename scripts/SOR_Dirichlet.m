function u = SOR_Dirichlet(u, dx, dz, tol)
% Use the SOR iteration to solve the discretisation of the Laplace 
% equation given by u with grid spacings dx and dy to a tolerance of tol.
%
% The boundary conditions of the Laplace equation are given in the top and 
% bottom rows and the left and right columns and must be of the Dirichlet
% type. For Neumann boundary conditions we need to modify the while loop.

% determine the size of the problem
[m,n] = size(u); 

% compute the optimal value of omega
lambda = (cos(pi/n)+cos(pi/m))^2/4;
omega = 2/(1+sqrt(1-lambda));
fprintf('\nSOR: Optimal value of omega=%.4f\n',omega);

% set the error to the largest value of head
error = max(max(u));

% compute the multiplication factor 1/(1+beta^2)
beta = dx/dz;
Ch = 1/(2*(1+beta*beta));

% zero the iteration counter
it = 0;

% Now do the SOR iteration
while (error > tol) 
    % save the current values
    oldu = u;
    % compute the new head value
    for i=2:m-1
        for j=2:n-1
            u(i,j)=(1-omega)*oldu(i,j)...
                +omega*Ch*(u(i-1,j)+u(i+1,j)...
                +beta*beta*(u(i,j-1)+u(i,j+1)));
        end
    end
       
    % compute the error
    error = max(max(abs(oldu(2:m-1,2:n-1)-u(2:m-1,2:n-1))));

    % update the iteration counter
    it = it+1;
end 
fprintf('Solution converged after %i SOR iterations.\n', it);
end