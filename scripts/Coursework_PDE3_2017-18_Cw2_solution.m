% Script to solve the 1D heat conduction equation
% Coursework 2 PDE3 course 2017-18
function C2_solution

dX = [];
f = [];
for i=2:2,
    N = 5 * 2^i;
    N = 200;
    [dx, BigU] = solve_problem(N, 2*N);
    dX = [dX; dx];
    f = [f; BigU];
end;
%RefinementAnalysis(dX, f)

%% Find the analytical and numerical solution for a given number of grid points
function [dx, BigU] = solve_problem(nx, nt);
if nargin < 1, 
    nx = 10;
    nt = 10;
end;

%% Define the grid and mesh spacing
kappa = 0.1;            % Thermal diffusivity
nu = 50.0;              % Courant number
xlim = [0.0, pi];          % x limits
tlim = [0, 4];   % time limits
dx = (xlim(2) - xlim(1))/nx;
fprintf('The grid contains %3i nodes (dx=%.3f).\n', nx+1, dx);

% calculate the stable time step
dt = nu*dx^2/(2*kappa);
fprintf('dt =%.3e seconds\n', dt);

[X, T] = meshgrid(xlim(1):dx:xlim(2), tlim(1):dt:tlim(2));

%% Compute the analytical solution
ua = analytical_solution(X, T, 80, kappa);

%% Compare different analytical solutions
if 0,
    ua00 = analytical_solution(X, T, 2, kappa);
    ua0 = analytical_solution(X, T, 5, kappa);
    ua1 = analytical_solution(X, T, 10, kappa);
    ua2 = analytical_solution(X, T, 20, kappa);
    ua3 = analytical_solution(X, T, 40, kappa);
    ua4 = analytical_solution(X, T, 80, kappa);
    ua5 = analytical_solution(X, T, 160, kappa);
    ua6 = analytical_solution(X, T, 10000, kappa);

    BigUExact = sum(sum((ua0-ua00).^2))*dx*dt
    BigUExact = sum(sum((ua1-ua0).^2))*dx*dt
    BigUExact = sum(sum((ua2-ua1).^2))*dx*dt
    BigUExact = sum(sum((ua3-ua2).^2))*dx*dt
    BigUExact = sum(sum((ua4-ua3).^2))*dx*dt
    BigUExact = sum(sum((ua5-ua4).^2))*dx*dt
    BigUExact = sum(sum((ua6-ua5).^2))*dx*dt
end;

%% Compute the numerical solution
N = nx+1; % include ghost points (i=1 and i=N)
%dx = 1.0/(Nx-1); % mesh spacing
x = xlim(1):dx:xlim(2); % generate the grid
fprintf('The grid contains %3i nodes (dx=%.3f).\n', N, dx);

t=0:dt:4.0;  % time array
U=X;

i=1;
U(1,:)=sin(x); % set inital conditions

% set the boundary condtions
U(:,1)=0.0;
U(:,N)=t;

% Now solve the equation using the Crank-Nicholson method
Rx=kappa*dt/(dx^2);
for time=dt:dt:4.0
    % compile the RHS (from the old time level data)
    RHS(1:N-2) = Rx*U(i,1:N-2) + 2*(1-Rx)*U(i,2:N-1) + Rx*U(i,3:N);
    
    % now assemble the A matrix,  as an n x 3 matrix 
    Amat(1:N-2,1) = -ones(N-2,1)*(Rx); % sub diagonal
    Amat(1:N-2,2) = ones(N-2,1)*2*(1+Rx); %diagonal
    Amat(1:N-2,3) = -ones(N-2,1)*(Rx); % super diagonal

    % boundary conditions on LHS
    Amat(1,1)=0.0; % u(1) is known and zero
    
    % boundary condition on RHS
    amat(N-2,3)=0.0; %u(N) is known and non-zero
    RHS(N-2)=RHS(N-2)+Rx*U(i+1,N);

    % solve the system
    U(i+1,2:N-1)=thomas(N-2,Amat,RHS);
    i=i+1;
end

fprintf('Done. %4i timesteps time=%.3f seconds\n',i,time);
contourf(X,T,U);

u = U;
% Now compute the integral of u^2 using the exact and
% numerical solutions.
BigU = sum(sum(u.^2))*dx*dt;
BigUExact = sum(sum(ua.^2))*dx*dt;
fprintf('int U^2 dxdt is %.4e [exact %.4e]\n', BigU, BigUExact);

% finally output the solution
figure(1)
[C,uval] = contour(X, T, ua);
clabel(C,uval);
colormap cool;
title('Coursework 2, Q1 analytical contour')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u(x,t)', 'Fontsize', 16)
saveas(gcf, '..\figures\C2_analytical_contour.png');

figure(2)
[C,uval] = contour(X, T, u);
clabel(C,uval);
colormap cool;
title('Coursework 2, Q1 numerical contour')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u(x,t)', 'Fontsize', 16)
saveas(gcf, '..\figures\C2_numerical_contour.png');

figure(3)
surf(X, T, ua, 'EdgeColor', 'none');
title('Coursework 2, Q1 analytical surface')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u_a(x,t)', 'Fontsize', 16)
grid off
saveas(gcf, '..\figures\C2_analytical_solution.png');

figure(4)
surf(X, T, u, 'EdgeColor', 'none');
title('Coursework 2, Q1 numerical surface')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u(x,t)', 'Fontsize', 16)
saveas(gcf, '..\figures\C2_numerical_solution.png');

figure(5)
[C,uval] = contour(X, T, u - ua);
clabel(C,uval);
colormap cool;
title('Coursework 2, Q1 numerical - analytical')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u(x,t) - u_a(x,t)', 'Fontsize', 16)
saveas(gcf, '..\figures\C2_difference_contour.png');

figure(6)
surf(X, T, u - ua, 'EdgeColor', 'none');
title('Coursework 2, Q1 numerical - analytical')
xlabel('x', 'Fontsize', 16);
ylabel('t', 'Fontsize', 16);
zlabel('u(x,t) - u_a(x,t)', 'Fontsize', 16)
saveas(gcf, '..\figures\C2_difference_solution.png');


%% Analytical solution
function ua = analytical_solution(X, T, N, kappa)
ua = 0.0 * ones(size(X));
ua = X.*T./pi + sin(X) .* exp(-kappa * T);
for i=1:N,
    ua = ua + 2/(pi * kappa) * (-1)^i/i^3 .* (1 - exp(-i^2*kappa.*T)) .* sin(i.*X);
end;

%% Numerical system
function c1 = FTCS(c0, dt, dx, kappa)
% This is the forward time centred space scheme for the
%1D heat conduction equation with default kappa=1
if nargin < 4, 
    kappa = 1;
end;

n = size(c0, 2); % get the size of the problem
Rx = kappa * dt/(dx^2);
c1 = c0;
c1(2:n-1) = c0(2:n-1) + Rx * (c0(1:n-2) - 2*c0(2:n-1) + c0(3:n));

%% Thomas algorithm
function x=thomas(n,a,b)
%Thomas algorithm for tridiagonal system Ax=b
%a(:,1) is the sub-diagonal, a(:,2) is the diagonal
%and a(:,3) is the super-diagonal. 

%Forward Elimination
for i=2:n
    em=a(i,1)/a(i-1,2);
    a(i,1)=em;
    a(i,2)=a(i,2)-em*a(i-1,3);
    b(i)=b(i)-a(i,1)*b(i-1);
end

%Back Substitution
x(n)=b(n)/a(n,2);
for i=n-1:-1:1
    x(i)=(b(i)-a(i,3)*x(i+1))/a(i,2);
end

