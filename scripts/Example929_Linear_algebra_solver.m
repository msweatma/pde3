% Example929_Linear_algebra_solver.m
% Linear algebra based solution method for Laplace
% equation example 9.29

%% Problem setup
% Generate the mesh
ni = 5; 
nj = 5;

% Grid Generation: derive the mesh spacing and setup the background grid
dx = 2.0/(ni-1);  
dy = 1.0/(nj-1);
N = ni*nj; % number of unknowns
fprintf('mesh (%i by %i): dx=%.4e, dy=%.4e\n',ni,nj,dx,dy);
fprintf('%i unknowns\n',N);
[X,Y] = meshgrid(-dx:dx:2+dx,-dy:dy:1+dy);

%% establish the array and specify the boundary conditions
u=zeros(nj+2,ni+2);
u(1,:)=0.0;
u(nj+2,:)=0.0;
u(:,1)=0.0;
u(:,ni+2)=sin(2*pi*Y(:,ni+1));

% calculate the beta coefficients
beta=dx/dy;
alpha1=1/(2*(1+beta*beta));
alpha2=beta^2*alpha1;
fprintf('alpha1=%.4e, alpha2=%.4e\n',alpha1,alpha2);

%% Matrix Assembly, we set up a column matrix whose elements are the non
% zero diagonals of the matrix A.  We also set up the RHS vector b.
e=ones(N,1);
% create the A matrix using spdiags, the first argument are the
% coefficients the second argument are the diagonal numbers
A=spdiags(...
    [alpha1*e alpha2*e -e alpha2*e alpha1*e],...
    [-ni -1 0 1 ni],N,N);

% now create the B vector
b=0*e;
% seting the boundary conditions on the right hand side
for j=1:nj
   i=ni;
   k=(j-1)*ni+i;
   b(k)=-alpha1*sin(2*pi*Y(j,i));
end
fprintf('Matrix assembled\n');

%% Solve the matrix system
x=pcg(A, b, 0.5e-5, 1000);

fprintf('Equations solved\n');
% unpack the solution
for j=1:nj
    for i=1:ni
        k=(j-1)*ni+i;
        u(j+1,i+1)=x(k);
    end
end
fprintf('Solution unpacked\n');

%% Plot the solution
[C,uval] = contour(X, Y, u);
clabel(C,uval)
colormap cool
title('Example 9.29: linear algebra method')
xlabel('x (meters)');
ylabel('z (meters)');
