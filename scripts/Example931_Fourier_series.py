#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plot Fourier series for PDE3 example 9.31

"""

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import itertools


# Plotting conventions
marker = itertools.cycle(('o', 'D', 's', 'p'))
farbe = itertools.cycle(('b', 'c', 'g', 'y', 'r'))
stil = itertools.cycle(('-', '--', ':'))

matplotlib.rcParams['lines.linewidth'] = 2
matplotlib.rcParams['lines.markersize'] = 6
matplotlib.rcParams['axes.labelsize'] = 16
matplotlib.rcParams['xtick.labelsize'] = 14
matplotlib.rcParams['ytick.labelsize'] = 14
matplotlib.rcParams['xtick.major.width'] = 1
matplotlib.rcParams['ytick.major.width'] = 1
matplotlib.rcParams['legend.fontsize'] = 16

dpi_size = 200

def f(t):
    # Original function
    ft = []
    for tt in t:
        if 0 <= tt < 1:
            ft.append(tt)
        else:
            ft.append(np.nan)
    return np.array(ft)

def FS_f(t):
    # Fourier series of the function
    fs = []
    y = 0
    for tt in t:
        s = 0
        for i in range(0,10):
            s = s + (-1)**i * np.sin((i+0.5)*np.pi*tt) * np.sinh((i+0.5)*np.pi*(2-y))/((2*i+1)**2*np.sinh((2*i+1)*np.pi))
        s = s * 8/np.pi**2
        fs.append(s)

    return np.array(fs)

# Define the plots
t = np.linspace(0, 1, 100)

fi, ax = plt.subplots(figsize=(6,4))

lgd = []
ax.plot(t, f(t))
lgd.append('f(t)')

ax.plot(t, FS_f(t), 'r--')
lgd.append('Fourier')

plt.legend(lgd, loc='lower right', frameon=False)

ax.spines['left'].set_position('zero')

# turn off the right spine/ticks
ax.spines['right'].set_color('none')
ax.yaxis.tick_left()

# set the y-spine
ax.spines['bottom'].set_position('zero')

# turn off the top spine/ticks
ax.spines['top'].set_color('none')
ax.xaxis.tick_bottom()

ax.set_xlabel('x')
#ax.xaxis.set_label_coords(1.05, 0.45)
ax.set_ylabel('u(x,0)')
#ax.yaxis.set_label_coords(0.4, 1.05)

ax_boundaries = [0, 1, 0, 1]
ax.axis(ax_boundaries)

name = "../Figures/Example931_Fourier_series.png"
plt.savefig(name, dpi=dpi_size)
plt.show()
