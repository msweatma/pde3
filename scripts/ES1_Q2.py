# This import registers the 3D projection, but is otherwise unused
from mpl_toolkits.mplot3d import Axes3D  

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np

# Define the grid and function values
X = np.linspace(0, 1, 100)
Y = np.linspace(0, np.pi, 100)
X, Y = np.meshgrid(X, Y)
Z = np.exp(-X) * np.sin(Y)

fig = plt.figure()
ax = fig.gca(projection='3d')

# Plot the surface
surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

# Customize the z axis
ax.set_zlim(0, 1.01)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.01f'))

ax.set_xlabel('x axis')
ax.set_ylabel('y axis')
ax.set_zlabel('u(x,y)')

# Add a color bar which maps values to colors
fig.colorbar(surf, shrink=0.5, aspect=5)

plt.savefig("./ES1-Q2.png")
plt.show()
