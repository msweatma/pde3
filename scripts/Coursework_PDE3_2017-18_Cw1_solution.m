% Script to solve the Laplace equation in cylindrical coordinates
% PDE3 course 2017-18
function C1_solution

dx = [];
f = [];
for i=4:-1:3,
    N = 5 * 2^i;
    [dr, BigU] = solve_problem(N, N);
    dx = [dx; dr];
    f = [f; BigU];
end;
figure(11)
RefinementAnalysis(dx, f)

%% Find the analytical and numerical solution for a given number of grid points
function [dr, BigU] = solve_problem(nr, ntheta);
if nargin < 1, 
    nr = 10;
    ntheta = 10;
end;

%% Define the grid and mesh spacing

% Change the next two values for the original and resit coursework
bdr = -1;    % Boundary value at r and for 0<phi<pi
a = 3;      % r limit

rlim = [1, a];          % r limits
thetalim = [0, 2*pi];   % theta limits
dr = (rlim(2) - rlim(1))/nr;
dtheta = (thetalim(2) - thetalim(1))/ntheta;
fprintf('mesh (%i by %i): dr=%.4e, dtheta=%.4e\n', nr, ntheta, dr, dtheta);
[R, Theta] = meshgrid(rlim(1):dr:rlim(2), thetalim(1):dtheta:thetalim(2));

%% Compute the analytical solution
ua = analytical_solution(R, Theta, 20, a, bdr);

%% Compute the numerical solution
inhomogeneity = 0;   % Set to 0 for homogeneous and to 1 for inhomogeneous case
u = SOR(dr, dtheta, R, Theta, bdr, 1e-6, inhomogeneity);

% Check tolerance of SOR
if 0,
    u2 = SOR(dr, dtheta, R, Theta, bdr, 1e-7, inhomogeneity);
    diff_12 = max(max(u - u2))
end;

inhomogeneity = 1;   % Set to 0 for homogeneous and to 1 for inhomogeneous case
u_inhomogeneous = SOR(dr, dtheta, R, Theta, bdr, 0.5e-6, inhomogeneity);

% Check that the derivative at the outer boundary is correct
if 0,
    ua_dr = (ua(:,nr) - ua(:,nr-1))/dr
    u_dr= (u(:,nr) - u(:,nr-1))/dr
end;

% Now compute the integral of u^2 using the exact and
% numerical solutions.
% This formula is only correct for a square, uniform grid

% u = u_inhomogeneous;   % Uncomment to check the convergence of the
% inhomogeneous case
BigU = sum(sum(u.^2))*dr*dtheta;
BigUExact = sum(sum(ua.^2))*dr*dtheta;
fprintf('int U^2 drdtheta is %.4e [exact %.4e]\n', BigU, BigUExact);

% finally output the solution
figure(1)
[C,uval] = contour(R.*cos(Theta), R.*sin(Theta), ua);
clabel(C,uval);
colormap cool;
title('Q1 analytical solution contour')
xlabel('x');
ylabel('y');
zlabel('u_a(x,y)')
saveas(gcf, '..\figures\C1_analytical_contour.png');

figure(2)
[C,uval] = contour(R.*cos(Theta), R.*sin(Theta), u);
clabel(C,uval);
colormap cool;
title('Q1 numerical solution contour')
xlabel('x');
ylabel('y');
zlabel('u(x,y)')
saveas(gcf, '..\figures\C1_numerical_contour.png');

figure(3)
surf(R.*cos(Theta), R.*sin(Theta), ua);
title('Q1 analytical solution')
xlabel('x');
ylabel('y');
zlabel('u_a(x,y)')
saveas(gcf, '..\figures\C1_analytical_solution.png');

figure(4)
surf(R.*cos(Theta), R.*sin(Theta), u);
title('Q1 numerical solution')
xlabel('x');
ylabel('y');
zlabel('u(x,y)')
saveas(gcf, '..\figures\C1_numerical_solution.png');

figure(5)
[C,uval] = contour(R.*cos(Theta), R.*sin(Theta), u - ua);
clabel(C,uval);
colormap cool;
title('Q1 numerical - analytical contour')
xlabel('x');
ylabel('y');
zlabel('u(x,y) - u_a(x,y)')
saveas(gcf, '..\figures\C1_difference_contour.png');

figure(6)
surf(R.*cos(Theta), R.*sin(Theta), u - ua);
title('Q1 numerical - analytical')
xlabel('x');
ylabel('y');
zlabel('u(x,y) - u_a(x,y)')
saveas(gcf, '..\figures\C1_difference_solution.png');

figure(7)
[C,uval] = contour(R.*cos(Theta), R.*sin(Theta), u_inhomogeneous);
clabel(C,uval);
colormap cool;
title('Q1 numerical inhomogeneous solution contour')
xlabel('x');
ylabel('y');
zlabel('u_i(x,y)')
saveas(gcf, '..\figures\C1_inhomogeneous_contour.png');

figure(8)
surf(R.*cos(Theta), R.*sin(Theta), u_inhomogeneous);
title('Q1 numerical inhomogeneous solution')
xlabel('x');
ylabel('y');
zlabel('u_i(x,y)')
saveas(gcf, '..\figures\C1_inhomogeneous_solution.png');

figure(9)
[C,uval] = contour(R.*cos(Theta), R.*sin(Theta), u_inhomogeneous - u);
clabel(C,uval);
colormap cool;
title('Q1 inhomogeneous - homogeneous contour')
xlabel('x');
ylabel('y');
zlabel('u_i(x,y) - u(x,y)')
saveas(gcf, '..\figures\C1_difference_inhomogeneous_contour.png');

figure(10)
surf(R.*cos(Theta), R.*sin(Theta), u_inhomogeneous - u);
title('Q1 inhomogeneous - homogeneous')
xlabel('x');
ylabel('y');
zlabel('u_i(x,y) - u(x,y)')
saveas(gcf, '..\figures\C1_difference_inhomogeneous.png');

%% Analytical solution
function ua = analytical_solution(R, Theta, N, a, bdr)
ua = 0.0 * ones(size(R));
ua = a * bdr/2 * log(R);  
for i=1:N,
    ua = ua + bdr * 2/((2*i-1).^2 .* pi .* (a.^(2*i-2) + a.^(-2*i))) .* (R.^(2*i-1) - R.^(-(2*i-1))) .* sin((2*i-1).*Theta);
end;

%% SOR solver
function [u, iter, omega] = SOR(dr, dtheta, R, Theta, bdr, tol, inhomogeneous)
if nargin < 7, 
    inhomogeneous = 0;
end;
u = R - 1;    % Initial values
[ntheta, nr] = size(u);
% Set Dirichlet boundary conditions
u(:, 1) = 0;      % Inside BC

% Compute the optimal value of omega: technically only correct
% for cartesian coordinates
lambda = (cos(pi/nr) + cos(pi/ntheta))^2/4;
omega = 2/(1+sqrt(1-lambda));
% set the error to the largest value of head
error=max(max(u));

% Ratio of step sizes
beta=dr/dtheta;

% zero the iteration counter
iter = 0;

% Now do the SOR iteration
while (error > tol)
    oldu = u;       % save the current values

    % Neumann BC
    j = nr; 
    for i=1:ntheta-1,
        u(i,j) = u(i,nr-1) + bdr*(sign(pi-Theta(i,j)) + 1)/2 * dr;
    end;
    i = 1;
    u(i,j) = u(i,nr-1) + (bdr)/2 * dr;       % Fix the value at theta=0
    
    for j=2:nr-1,       % Loop over r
        Ch = 1/(2 + 2 * beta * beta/R(1,j)^2 + dr/R(1,j));
        % Special treatment for theta=0 due to the 2pi periodicity
        i = 1;
        u(i,j) = (1-omega) * oldu(i,j) ...
                + omega*Ch * (u(i,j+1) * (1 + dr/R(i,j)) + u(i,j-1)  ...
                + beta*beta/(R(i,j) * R(i,j)) * (u(i+1,j) + u(ntheta-1,j))) ...
                + dr^2 * omega * inhomogeneous*Ch*(R(i,j) - 1) * sin(Theta(i,j)) * (1 - sign(pi-Theta(i,j)))/2;
        % Loop over the rest of the angles
        for i=2:ntheta-1,
            u(i,j) = (1-omega) * oldu(i,j) ...
                + omega*Ch * (u(i,j+1) * (1 + dr/R(1,j)) + u(i,j-1)  ...
                + beta*beta/(R(1,j) * R(1,j)) * (u(i+1,j) + u(i-1,j))) ...
                + dr^2 * omega * inhomogeneous*Ch*(R(i,j) - 1) * sin(Theta(i,j)) * (1 - sign(pi-Theta(i,j)))/2;
        end;
        u(ntheta,:) = u(1,:);   % Set the 2pi value to the 0 value        
    end;
    
    % compute the error
    error = max(max(abs(oldu(2:ntheta-1, 2:nr-1)-u(2:ntheta-1, 2:nr-1))));
    % update the iteration counter
    iter = iter + 1;
end
fprintf('solution converged after %i iterations.\n', iter);
