% PDE3 characteristics of example 9.11
% Generate waterfall plots for the correct and
% three wrong solutions

% Set x and time spacing
x = linspace(-5, 5, 1000);
tt = linspace(0, 5, 30);
[X, T] = meshgrid(x, tt);

Z = [];     % Correct solution
Z1 = [];    % Exponential decreasing solution
Z2 = [];    % Solution with only one characteristic
Z3 = [];    % Solution with double the energy
for i=1:size(tt,2),
    t = tt(i);
    f = F(x - t);
    g = F(x + t);
    Z = [Z; 0.5*(f + g)];
    Z1 = [Z1; 0.5*(f+g)*exp(-t*0.4)];
    Z2 = [Z2; g];
    Z3 = [Z3; (f+g)/max(f+g)];
end;

%% Plot the different options
figure(1)
waterfall(X, T, Z)
xlabel('x')
ylabel('t')
zlabel('u_2(x,t)')
set(gca,'Fontsize', 16, 'ylim', [min(tt) max(tt)], 'xlim', [min(x) max(x)])
saveas(gcf, '..\figures\example_911_correct.png')

figure(2)
waterfall(X, T, Z1)
xlabel('x')
ylabel('t')
zlabel('u_1(x,t)')
set(gca,'Fontsize', 16, 'ylim', [min(tt) max(tt)], 'xlim', [min(x) max(x)])
saveas(gcf, '..\figures\example_911_decreasing.png')

figure(3)
waterfall(X, T, Z2)
xlabel('x')
ylabel('t')
zlabel('u_3(x,t)')
set(gca,'Fontsize', 16, 'ylim', [min(tt) max(tt)], 'xlim', [min(x) max(x)])
saveas(gcf, '..\figures\example_911_f.png')

figure(4)
waterfall(X, T, Z3)
xlabel('x')
ylabel('t')
zlabel('u_4(x,t)')
set(gca,'Fontsize', 16, 'ylim', [min(tt) max(tt)], 'xlim', [min(x) max(x)])
saveas(gcf, '..\figures\example_911_f_plus_g.png')

% Initial condition function
function f = F(X)
    f = zeros(size(X));
    for i=1:size(X,2),
        x = X(i);
        if (0 <= x) && (x <= 1),
            value = 1 - x;
        elseif (-1 <= x) && (x < 0),
            value = 1 + x;
        else
            value = 0;
        end
        f(i) = value;
    end;
end