% ES1_Q1.m
% Script plotting a solution to the Laplace equation on a square

% Set the x and y grid points and the meshgrid
N = 20;
x = linspace(0, 1, N);
y = linspace(0, 1, N);
[X,Y] = meshgrid(x, y);

% Define the function and generate the output on the meshgrid
u = @(x, y) x.^4 - 6*x.^2 .* y.^2 + y.^4;
Z = u(X,Y);

% Generate the plot
surf(X,Y,Z)
xlabel('x')
ylabel('y')
zlabel('u(x,y)')
saveas(gcf, '..\figures\ES1-Q1_solution.png')
