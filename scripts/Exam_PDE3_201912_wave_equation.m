% Script to check the analytical solution of the wave equation
% PDE3 exam 201912

% Set the grid size
nx = 10;
ny = 10;
% Loop over the different grids
f = [];
dx = [];
for i=3:-1:3,
    nx = 5 * 2^i;
    ny = 5 * 2^i;
    % Solve the system for different grids
    [u, uq, X, Y, BigU] = solve_Q2(nx, ny);
    f = [f; BigU];
    dx = [dx; 1/nx];       
end;


%% ------------------------------------------------------ %
% Function to solve the system
function [u, ua, X, Y, BigU] = solve_Q2(nx, ny)
if nargin < 1, 
    nx = 20;
    ny = 20;
end;

% Define the domain and derive the mesh 
a = 4;      % x limit
b = 5;      % y limit
xlim = [0, a];    % x limits
ylim = [0, b];     % y limits
dx = (xlim(2) - xlim(1))/nx;
dy = (ylim(2) - ylim(1))/ny;
fprintf('mesh (%i by %i): dx=%.4e, dy=%.4e\n', nx, ny, dx, dy);
[X,Y] = meshgrid(xlim(1):dx:xlim(2), ylim(1):dy:ylim(2));

% Set up the inital values
u = 0.1*X;

% compute the analytical solution
ua = analytical_solution_Q2(X, Y, 40);

% Check convergence of the Fourier series solution
if 0,
    ua_more_terms = analytical_solution_C1(X, Y, 40);
    Fourier_BigU = sum(sum((ua - ua_more_terms).^2))*dx*dy
end;

BigU = sum(sum(ua.^2))*dx*dy;

% Plot output: set to 1 for plotting and 0 for no plotting
if 1,
    num_contours = 32;
    figure(1)
    [C,uval] = contour(X,Y,ua, num_contours);
    clabel(C,uval);
    colormap cool;
    title('Exam 201912, Q2 analytical contour')
    xlabel('x');
    ylabel('y');
    %saveas(gcf, '..\figures\exam_201912_Q2_analytical_contour.png');
    
    figure(2)
    surf(X,Y,ua);
    title('Exam 201912, Q2 analytical surface')
    xlabel('x');
    ylabel('y');
    %saveas(gcf, '..\figures\exam_201912_Q2_analytical.png');
end;
end

%% Calculate the analytical solution to the Laplace equation 
function u = analytical_solution_Q2(X, Y, terms)
u = zeros(size(X));
a = X(end,end);
c = 0.7;
for i=1:terms,
    k = 2*i - 1;
    u = u + (8*a^2/(k^2*pi^2) *(-1)^i + 32*a^2/(k^3*pi^3)).* cos(k * pi * c * Y/(2*a)) .* sin(k*pi/(2*a) * X);
end;
end
