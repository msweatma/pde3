% LinAlgLaplace -- use linear algebra to solve the 
% Lapace equation problem from numerical lecture 4

%% Problem setup
% Generate the mesh
N = 101;            % Number of mesh points
x = linspace(0, 200e-3, N+2);
y = linspace(0, 200e-3, N+2);

% Pre-allocate the solution array
T = zeros(N+2, N+2);

%% Define the boundary conditions
% temperature on the right hand boundary
T(:, N+2) = linspace(0, 50, N+2);
% temperature on the top boundary
T(N+2, :) = linspace(0, 50, N+2);

%% Pre-allocate sparse matrices
% The matrix has N*N rows and columns
A = sparse(N*N, N*N);
B = zeros(N*N, 1);

%% Assemble the matrix
for i=1:N
    for j=1:N
        k = (j-1)*N+i; % index to matrix rows
        A(k,k) = -1;
        for m=i-1:2:i+1
            if ((m<1) || (m>N))
                B(k) = B(k)-T(m+1,j+1)/4;
            else
                l = (j-1)*N+m;
                A(k,l) = 1/4;
            end
        end
        for n=j-1:2:j+1
            if ((n<1) || (n>N))
                B(k) = B(k)-T(i+1,n+1)/4;
            else
                l = (n-1)*N+i;
                A(k,l) = 1/4;
            end
        end
    end
end

%% Plot sparsity of the matrix
figure(1)
spy(A)

%% Solve the system using the 
% Bidirectional Conjugate Gradient solver 
% with an incomplete LU factorisation as a 
% preconditioner.
[L1, U1] = ilu(A);
[U, fl1, rr1, it1, rv1] = bicg(A, B, 1e-6, 1000, L1, U1);

%% Unpack the results: map vector U to NxN matrix
for j=1:N
    for i=1:N
        T(j+1,i+1) = U((j-1)*N+i);
    end
end
            
%% Plot the results
figure(2)
contour(x, y, T)
xlabel('x')
ylabel('y')
