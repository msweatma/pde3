function [u, ua, X, Y, BigU, BigUExact] = ES2_Q1c(nx, ny);
% Set default values if there are not enough input arguments
if nargin < 1,
    nx = 10;
    ny = 10;
end;

% Set the x and y grid points and the meshgrid
xlim = [0, 1];      % x limits
ylim = [0, 4];      % y limits
dx = xlim(2)/nx;
dy = ylim(2)/ny;
fprintf('mesh (%i by %i): dx=%.4e, dy=%.4e\n', nx, ny, dx, dy);
[X,Y] = meshgrid(0:dx:xlim(2), 0:dy:ylim(2));

% Set up the inital values
u = 0.0*X;

% Set up the boundary conditions
u(1,:) = sin(pi*X(1,:)).^5; % south BC
u(ny+1,:) = 0.0; % north BC
u(:,1) = 0; % west BC
u(:,nx+1) = 0;  % East BC

% Call the SOR solver
u = SOR_ES2(u, dx, dy, 0.5e-6);

% Compute the analytical solution
ua = 1/16 * (exp(-5*pi.*Y) .* sin(5*pi.*X) - 5 * exp(-3*pi.*Y) .* sin(3*pi.*X) + 10 * exp(-pi.*Y) .* sin(pi.*X) );

% Now compute the integral of u^2 using the exact and
% numerical solutions.
BigU=sum(sum(u.^2))*dx*dy;
BigUExact=sum(sum(ua.^2))*dx*dy;
fprintf('int U^2 dxdy is %.4e [exact %.4e]\n',BigU,BigUExact);

% Plot the solution
num_contours = 32;
figure(1)
[C,uval] = contour(X,Y,u, num_contours);
clabel(C,uval);
colormap cool
title('Exercise sheet 2, contour plot of Q1c')
xlabel('x');
ylabel('y');

figure(2)
surf(X,Y,u);
title('Exercise sheet 2, surface plot of Q1c')
xlabel('x');
ylabel('y');
end

function u = SOR_ES2(u,dx,dy,tol)
% determine the size of the problem
[ny, nx] = size(u);

% compute the optimal value of omega
lambda = (cos(pi/nx)+cos(pi/ny))^2/4;
omega = 2/(1+sqrt(1-lambda));
fprintf('\nOptimal value of omega=%.4f\n',omega);

% Set the error to the largest value of head
error = max(max(u));

% Compute the multiplication factor 1/(1+beta^2)
beta = dy/dx;
Ch = 1/(2*(1+beta*beta));

% Zero the iteration counter
it = 0;

% Now do the SOR iteration
while (error > tol)
    % Save the current values
    oldu = u;
    % Compute the new head value
    for i=2:ny-1
        for j=2:nx-1
            u(i,j) = (1-omega)*oldu(i,j)...
                + omega*Ch * (u(i-1,j)+u(i+1,j)...
                + beta*beta * (u(i,j-1)+u(i,j+1)));
        end
    end

    % Compute the error
    error = max(max(abs(oldu(2:ny-1, 2:nx-1)-u(2:ny-1, 2:nx-1))));

    % Update the iteration counter
    it = it + 1;
end
fprintf('solution converged after %i iterations.\n',it);
end
