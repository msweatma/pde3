% Script to solve the Laplace equation in cylindrical coordinates
function Lab5_Q1;

dx = [];
f = [];
for i=4:-1:1,
    N = 5 * 2^i;
    [dr, BigU] = solve_problem(N, N);
    dx = [dx; dr];
    f = [f; BigU];
end;
RefinementAnalysis(dx, f)

%% Find the analytical and numerical solution for a given number of grid points
function [dr, BigU] = solve_problem(nr, ntheta);
if nargin < 1, 
    nr = 10;
    ntheta = 10;
end;

%% Define the grid and mesh spacing
a = 2*pi;      % r limit
rlim = [0, a];          % r limits
thetalim = [0, 2*pi];   % theta limits
dr = (rlim(2) - rlim(1))/nr;
dtheta = (thetalim(2) - thetalim(1))/ntheta;
fprintf('mesh (%i by %i): dx=%.4e, dy=%.4e\n', nr, ntheta, dr, dtheta);
[R, Theta] = meshgrid(rlim(1):dr:rlim(2), thetalim(1):dtheta:thetalim(2));

%% Compute the analytical solution
ua = analytical_solution_Lab5(R, Theta, 20, a);

%% Compute the numerical solution
u = SOR_Lab5(dr, dtheta, R, Theta, 0.5e-6);

% Now compute the integral of u^2 using the exact and
% numerical solutions.
BigU = sum(sum(u.^2))*dr*dtheta;
BigUExact = sum(sum(ua.^2))*dr*dtheta;
fprintf('int U^2 dxdy is %.4e [exact %.4e]\n', BigU, BigUExact);

% finally output the solution
figure(1)
[C,uval] = contour(R.*cos(Theta), R.*sin(Theta), ua);
clabel(C,uval);
colormap cool;
title('Tutorial 5, Q1 analytical')
xlabel('x');
ylabel('y');
zlabel('u(x,y)')
saveas(gcf, '..\figures\W5_LE1_analytical_contour.png');

figure(2)
[C,uval] = contour(R.*cos(Theta), R.*sin(Theta), u);
clabel(C,uval);
colormap cool;
title('Tutorial 5, Q1 numerical')
xlabel('x');
ylabel('y');
zlabel('u(x,y)')
saveas(gcf, '..\figures\W5_LE1_numerical_contour.png');

figure(3)
surf(R.*cos(Theta), R.*sin(Theta), ua);
title('Tutorial 5, Q1 analytical')
xlabel('x');
ylabel('y');
zlabel('u(x,y)')
saveas(gcf, '..\figures\W5_LE1_analytical_solution.png');

figure(4)
surf(R.*cos(Theta), R.*sin(Theta), u);
title('Tutorial 5, Q1 numerical')
xlabel('x');
ylabel('y');
zlabel('u(x,y)')
saveas(gcf, '..\figures\W5_LE1_numerical_solution.png');

%% Analytical solution
function ua = analytical_solution_Lab5(R, Theta, N, a)
ua = 0.5 * ones(size(R));
for i=1:N,
    ua = ua + 2/((2*i-1) .* pi .* a^(2*i-1)) .* R.^(2*i-1) .* sin((2*i-1).*Theta);
end;

%% SOR solver
function [u, iter, omega] = SOR_Lab5(dr, dtheta, R, Theta, tol)
u = 0.0 * R;    % Initial values
[ntheta, nr] = size(u);
% Set Dirichlet boundary conditions
u(:, nr) = heaviside(pi - Theta(:, nr)); % Outside BC
% Set value at theta=0 and theta=2pi to 0.5
u(1, nr) = 0.5;     
u(ntheta, nr) = 0.5;

% Compute the optimal value of omega: technically only correct
% for cartesian coordinates
lambda = (cos(pi/nr) + cos(pi/ntheta))^2/4;
omega = 2/(1+sqrt(1-lambda));
% set the error to the largest value of head
error=max(max(u));

% Ratio of step sizes
beta=dr/dtheta;

% zero the iteration counter
iter = 0;

% Now do the SOR iteration
while (error > tol)
    oldu = u;       % save the current values

    % The value in the centre is the average of the surrounding values 
    s = sum(u(:,2))/ntheta;
    for i=1:ntheta,  
        u(i,1) = s;
    end;

    for j=2:nr-1,  % Loop over all values of r which are inside the solution domain
        Ch = 1/(2 + dr/R(1,j) + 2 * beta * beta/R(1,j)^2);
        
        % Special treatment for theta=0 due to the 2pi periodicity
        i = 1;      % Index of the angles for which Theta(i,:)=0
        u(i,j) = (1-omega) * oldu(i,j) ...
                + omega*Ch * (u(i,j+1) * (1 + dr/R(i,j)) + u(i,j-1)  ...
                + beta*beta/(R(i,j) * R(i,j)) * (u(i+1,j) + u(ntheta-1,j)));
            
        % Loop over all angles except for theta=0 and theta=2pi
        for i=2:ntheta-1,
            u(i,j) = (1-omega) * oldu(i,j) ...
                + omega*Ch * (u(i,j+1) * (1 + dr/R(1,j)) + u(i,j-1)  ...
                + beta*beta/(R(1,j) * R(1,j)) * (u(i+1,j) + u(i-1,j)));
        end;
        u(ntheta,:) = u(1,:);   % Set the theta=2pi value to the theta=0 value
    end;
    
    % Compute the error
    error = max(max(abs(oldu(2:ntheta-1, 2:nr-1)-u(2:ntheta-1, 2:nr-1))));
    % Update the iteration counter
    iter = iter + 1;
end
fprintf('solution converged after %i iterations.\n', iter);
