% figures_Laplace_solution_ConcepTest.m - generate plots for the lecture slides
% Generate a plot showing different solution profiles of which only one is
% correct for the Laplace equation with given boundary conditions

clear;
N = 100;
rng(4)      % Seed the random number generator to always get the same plot
x = linspace(0, 10, N)';
y = 1 + 0 * x;    % Exact values
y_1 = y + 0.5 + 0.1*rand(N,1);   % Values with error
y_2 = y + 0.7 * (rand(N, 1) - 0.5);
y_3 = y + 0.1*(rand(N,1)-0.5);
y_4 = x/10 + 0.5*rand(N,1);

xsize = 1;
ysize = 4;
%% Plot precise but not accurate
figure1=figure('Position', [100, 100, 2000, 500]);
subplot(xsize, ysize, 1);
plot(x, y, '--', x, y_1, 'x-', 'Linewidth', 2)
text(-0.9, 1, 'f_0', 'fontweight', 'bold', 'Fontsize', 14)
legend('Exact data', 'Measured data', 'Location', 'best')
xlabel('x')
ylabel('f(x)')
title('1)', 'Fontsize', 18)
set(gca,'Fontsize', 16, 'ylim', [0.5 2], 'xlim', [0 10])

%% Plot not precise but accurate
subplot(xsize, ysize, 2);
plot(x, y, '--', x, y_2, 'x-', 'Linewidth', 2)
text(-0.9, 1, 'f_0', 'fontweight', 'bold', 'Fontsize', 14)
legend('Exact data', 'Measured data', 'Location', 'best')
xlabel('x')
ylabel('f(x)')
title('2)', 'Fontsize', 18)
set(gca,'Fontsize', 16, 'ylim', [0.5 2], 'xlim', [0 10])

%% Plot precise and accurate
subplot(xsize, ysize, 3);
plot(x, y, '--', x, y_3, 'x-', 'Linewidth', 2)
text(-0.9, 1, 'f_0', 'fontweight', 'bold', 'Fontsize', 14)
legend('Exact data', 'Measured data', 'Location', 'best')
xlabel('x')
ylabel('f(x)')
title('3)', 'Fontsize', 18)
set(gca,'Fontsize', 16, 'ylim', [0.5 1.5], 'xlim', [0 10])

%% Plot not precise and not accurate
subplot(xsize, ysize, 4);
plot(x, y, '--', x, y_4, 'x-', 'Linewidth', 2)
text(-0.9, 1, 'f_0', 'fontweight', 'bold', 'Fontsize', 14)
legend('Exact data', 'Measured data', 'Location', 'best')
xlabel('x')
ylabel('f(x)')
title('4)', 'Fontsize', 18)
set(gca,'Fontsize', 16, 'ylim', [0. 1.5], 'xlim', [0 10])
saveas(gcf, '..\figures\accuracy_precision.png')
