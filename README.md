# Overview of the files and folders in the PDE3 repository

- coursework, folder with the coursework assignments since 2017-18
- exam, folder with the exams since 2017-18
- exercise_sheets, folder with exercise sheets introduced in 2020-21
- feedback, folder with the midsemester feedback response
- figures, folder with the figures for the course materials
- lectures, folder with the traditional lecture slides; superseded by the split versions in the topics folder
- notebooks, folder with Jupyter notebooks for the workshops and examples introduced in 2021-22
- scripts, folder with the Matlab scripts from pre-2021-22 and solutions for the coursework assignments
- topics, folder with the lectures split into short videos for academic year 2020-21
- tutorials, folder with the tutorial sheets from the pre-2020-21 academic years
- PDE3_assessment.org, overview file of the assessments through the years
