\documentclass[answers,a4paper,addpoints,12pt]{exam}
% This is typeset using the EXAM LaTeX2e class by Philip Hirschhorn. 

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Exercise sheet 2}
% solution header

\usepackage{PDE3_tutorial}

\begin{document}

The practice questions are similar to the exercises in the textbook and in Schaum's outlines Partial Differential Equations. If you are having difficulties with these questions practice using the remaining questions from these exercise. You will get the best from this tutorial by working though these examples \textbf{before} the tutorial and asking your tutors to help you with questions with which you are having problems. \\

% This is the start of the questions
\begin{questions}
%----- Question ----------------------------------------%
% From exercise 9.5.2 of Advanced Modern Engineering Mathematics
\question
Consider the Laplace equation
\[
0 = \grad^2u = \pdderiv{u(x,y)}{x} + \pdderiv{u(x,y)}{y} 
\]
in the region $0\le x\le1$ and $y\ge0$ and with the following boundary conditions
\begin{align}
u(0, y) &= 0, \quad y\ge0 \label{eq:LE2_BC1}\\
u(1, y) &= 0, \quad y\ge0 \label{eq:LE2_BC2}\\
u(x, y\to\infty) &\to 0, \quad 0\le x\le1 \label{eq:LE2_BC3} \\
u(x, 0) &= \sin^5(\pi x), \quad 0\le x\le1 \label{eq:LE2_BC4}
\end{align}

You might want to use the identity
\[
\sin^5\theta = \frac{1}{16}\left(\sin 5\theta - 5 \sin 3\theta + 10 \sin\theta \right)
\]

\begin{parts}
\part Use the method of separated solutions to solve the Laplace equation with the given boundary conditions.

\begin{solution}
From the lectures or the book we know that the following three functions
\begin{align*}
u_1(x,y) =& (A\sin \mu x+B\cos \mu x) \left(Ce^{\mu y}+De^{-\mu y}\right) \\
u_2(x,y) =&\left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y) \\
u_3(x,y) =& (Ax+B)(Cy+D)
\end{align*}
are possible solutions for the Laplace equation. 
We need to go through the boundary conditions to find which or which combination of these three functions is a solution to the particular problem, i.e. partial differential equation with associated boundary conditions. 
In addition, we need to use the boundary conditions to find the values for the parameters, i.e. $A$, $B$, $C$, $D$ and $\mu$, in these functions.

The first two boundary conditions \ref{eq:LE2_BC1} and \ref{eq:LE2_BC2} require that the solution has $\sin$ and $\cos$ terms. 
The $\sinh$ and $\cosh$ terms have only $1$ or $0$ roots and are thus not suitable. 
Also $u_3$ is not suitable because it won't be zero for $x=1$ and all values of $y$. 
Thus the solution needs to have the form 
\[
u_1(x,y) = (A\sin \mu x+B\cos \mu x) \left(Ce^{\mu y}+De^{-\mu y}\right)
\]

Dropping the subscript and using the first boundary condition \ref{eq:LE2_BC1} we get
\begin{align*}
u(x=0,y) &= (A\sin(\mu 0)+B\cos(\mu 0)) \left(Ce^{\mu y}+De^{-\mu y}\right) \\
 &= B \left(Ce^{\mu y}+De^{-\mu y}\right) \overset{!}{=} 0 
\end{align*}
This needs to hold for all $y\ge0$ which is only possible for $B=0$.

We can write the solution as
\[
u(x,y) = \sin(\mu x) \left(\tilde{C}e^{\mu y} + \tilde{D}e^{-\mu y}\right)
\]
where $\tilde{C} = AC$ and $\tilde{D} = AD$.

Using the second boundary condition \ref{eq:LE2_BC2} we get
\[
u(x=1,y) = \sin(\mu) \left(\tilde{C}e^{\mu y} + \tilde{D}e^{-\mu y}\right) \overset{!}{=} 0 
\]
Again this needs to hold for all $y\ge0$ which can only be fulfilled if $\sin(\mu)=0$.
We know that the sine function is zero for multiples of $\pi$ and thus that $\mu=n\pi$ where $n$ is an integer.

Using the third boundary condition \ref{eq:LE2_BC3} and the fact that $n\pi\ge0$ we can see that $\tilde{C} = 0$. If this were not the case the term $e^{n\pi y}$ would go to infinity for $y\to\infty$.

Thus the solution has the form
\begin{equation}
u(x,y) = \sin(n\pi x) \tilde{D}e^{-n\pi y}, \quad n=1,2,\dots
\label{eq:LE2_solution_form}
\end{equation}

To fulfil the fourth boundary condition \ref{eq:LE2_BC4}
\[
u(x,y=0) = \sin^5\pi x = \frac{1}{16}\left(\sin 5 \pi x - 5 \sin 3\pi x + 10 \sin\pi x \right), \quad 0\le x \le1
\]
we need to superimpose the solution \ref{eq:LE2_solution_form}
\[
u(x,y) = \sum_{n=1}^\infty \sin(n\pi x) \tilde{D_n}e^{-n\pi y} 
\]
Setting this equal to the boundary condition \ref{eq:LE2_BC4} we get
\[
u(x,y=0) = \sum_{n=1}^\infty \sin(n\pi x) \tilde{D_n}e^{-n\pi 0} \overset{!}{=}\frac{1}{16}\left(\sin 5 \pi x - 5 \sin 3\pi x + 10 \sin\pi x \right)
\]
Since the exponential term is $1$ we immediately see that most $\tilde{D}_n$ are zero except for $\tilde{D}_1 = \frac{10}{16}$, $\tilde{D}_3 = -\frac{5}{16}$ and $\tilde{D}_5 = \frac{1}{16}$.

The complete solution is
\[
u(x,y) = \frac{1}{16}\left(e^{-5\pi y}\sin 5 \pi x  - 5 e^{-3\pi y}\sin 3\pi x +e^{-\pi y} 10 \sin\pi x \right)
\]
\end{solution}

\part Use Matlab and/or Python to plot the solution.
\begin{solution}
The Matlab script is the same as for question 1 with the exception of a different function definition in line 11 and different solution domain in line 7.
\lstinputlisting[language=Matlab]{../scripts/ES2_Q1b.m}

\includegraphics[width=0.8\columnwidth,]{ES2_Q1b_solution.png}
\end{solution}

\part Modify the Matlab code for Example 9.29 (from the first lecture on numerical methods) to numerically solve the Laplace equation with the given boundary conditions.

Use $0\le y\le4$ and replace the boundary condition \ref{eq:LE2_BC3} with
\[
u(x, y=4) = 0, \quad 0\le x\le1
\]

\begin{solution}
In the script from the lecture we had to modify the size of the domain as well as the boundary conditions.
In the Matlab script included below we have included the following settings. 
In lines 8 to 14 the $x$ and $y$ limits are set and the mesh grid generated.
Line 17 defines an initial solution of the size of the mesh grid.
In lines 19 to 23 the four Dirichlet boundary conditions are set. 
Except for the south side which is given by sine waves, all other sides are set to zero.
Then we are calling the SOR method to calculate the solution inside the domain.
The loops inside the SOR method only go from $2$ to $ny-1$ and $nx-1$, respectively, because the values at the boundary, i.e. at $i=1$, $i=ny$, $j=1$ and $j=nx$ are defined by the Dirichlet boundary conditions. 
After the numerical solution we calculate the analytical solution and compare the two.

The resulting Matlab script is:
\lstinputlisting[language=Matlab]{../scripts/ES2_Q1c.m}

\end{solution}
\end{parts}
%----- end Question ------------------------------------%

\end{questions}
%-------------------------------------------------------%
\newpage
{\large\textbf{Formula sheet}}

\begin{enumerate}
\item Useful integrals
\begin{align*}
\int x \sin ax \, \mathrm{d}x &= \frac{1}{a^2} \bigl ( \sin a x - a x \cos ax \bigr ) \\
\int x \cos ax \, \mathrm{d}x &= \frac{1}{a^2} \bigl ( \cos a x + a x \sin ax \bigr ) \\
\int x^2 \sin ax \, \mathrm{d}x &= \frac{2x}{a^2} \sin ax - \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \cos ax \\
\int x^2 \cos ax \, \mathrm{d}x &= \frac{2x}{a^2} \cos ax + \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \sin ax
\end{align*}
\item Trigonometric identities
\begin{align*}
\sin(a\pm b) &= \sin(a) \cos(b) \pm \cos(a) \sin(b) \\
\cos(a\pm b) &= \cos(a) \cos(b) \mp \sin(a) \sin(b) \\
2\sin(a) \cos(b) &= \sin(a+b) + \sin(a-b) \\
2\sin(a) \sin(b) &= \cos(a+b) - \cos(a-b) \\
2\cos(a) \cos(b) &= \cos(a+b) + \cos(a-b)
\end{align*}
\end{enumerate}

\end{document}
