\documentclass[answers,a4paper,addpoints,12pt]{exam}
% This is typeset using the EXAM LaTeX2e class by Philip Hirschhorn. 

\usepackage{labelfig}
\def\epp{{\varepsilon}}
\def\half{\frac{1}{2}}
\def\u#1{\mbox{\boldmath $#1$}}
\def\d#1{\partial_#1}
\def\d#1#2{\frac{d #1}{d #2}}
\def\D#1#2{\frac{D #1}{D #2}}
\def\Dp#1#2{\frac{\partial #1}{\partial #2}}
\def\der{\partial}
\def\Fr{\hbox{Fr}}
\def\sign{\mathrm{sign}}

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Exercise sheet 6}
% solution header

\usepackage{PDE3_tutorial}

\begin{document}

The practice questions are similar to the exercises in the textbook and in Schaum's outlines Partial Differential Equations. If you are having difficulties with these questions practice using the remaining questions from these exercise. You will get the best from this seminar by working though these examples \textbf{before} the seminar and asking your tutors to help you with questions with which you are having problems. \\

%%
\begin{figure}[h!]
\begin{center}
%
%
\SetLabels
       \endSetLabels
       \strut\AffixLabels{ }
       \SetLabels
	 \L (-0.1*0.95) $(a)$\\
       \endSetLabels
       \strut\AffixLabels{
\includegraphics[width=0.42\textwidth]{./gaugingStation.png}}
\SetLabels
       \endSetLabels
       \strut\AffixLabels{ }
\SetLabels
	\L (0.0*1.0) $(b)$\\
	 \L (0.17*0.46) $x$\\
	 \L (0.06*0.77) $y$\\
	 \L (0.34*0.61) $h$\\
	 \L (0.53*0.5) $u(y,t)$\\
	\L (0.13*0.22) $\underline{g}$\\
	 \L (0.66*0.13) $\theta$\\
       \endSetLabels
       \strut\AffixLabels{
\includegraphics[width=0.5\textwidth]{./ES6_Diagram3.png}}
%
\caption{Uniform fluid flow down an inclined plane. $(a)$ a broad-crested weir in a stream used to gauge flow rates and $(b)$ an idealised schematic of a steady section.  }
%
\label{fig cylinder scaling}
%
\end{center}
\end{figure}
%%

% This is the start of the questions
\begin{questions}
%----- Question ----------------------------------------%
\question 
As in the figure above, consider a uniform layer of water, of thickness $h$, flowing down a flat plane inclined at angle $\theta$. Limiting our attention to laminar flow, for which the convective terms vanish, the Navier--Stokes equations reduce to a single PDE describing the evolution in space and time of the flow velocity $u$:
\begin{equation} \label{PDE}
	\rho \Dp{u}{t} = \eta \Dp{^2u}{y^2}+\rho g \sin\theta \, ,
\end{equation}
where $\rho$ and $\eta$ are the constant density and viscosity respectively. \\

\begin{parts}
%-------------------------------------------------------%

\part Formulate boundary conditions for $u$ under the assumptions of no slip at the base and no shear stress at the surface (\textit{hint:} the shear stress for a Newtonian fluid $\tau=\eta \partial_y u$) and use these to derive the \textit{steady} flow solutions $u_\theta(y)$

\begin{solution}
No slip along the basal plane is written
\begin{equation}
	u=0 \qquad \mbox{at} \qquad y=0\, , \label{no slip}
\end{equation}
and the stress-free surface condition implies
\begin{equation}
	\tau=0 \; \to \; \Dp{u}{y}=0 \qquad \mbox{at} \qquad y=h\, . \label{stress free}
\end{equation}
Steady solutions also satisfy the ODE
\begin{equation}
	\d{^2u_\theta}{y^2} = -\frac{\rho g}{\eta} \sin\theta \, , \label{steady ODE}
\end{equation}
due to the governing equation~\eqref{PDE}. Equations~\eqref{no slip}-\eqref{steady ODE} then have solutions
\begin{equation}
	u_\theta(y) = \frac{\rho g}{\eta}  \sin\theta \left(hy-\frac{y^2}{2}\right) \, . \label{steady}
\end{equation}
\end{solution}

%-------------------------------------------------------%

\part Now imagine that the slope angle $\theta$ changes, but that all other parameters, including the thickness, stay the same. This will naturally lead to an acceleration or deceleration towards a new steady state. Given the variable change $u=u_\theta+\hat{u}$, use the method of separated solutions to show that the general spatio-temporal solutions are of the form
\begin{equation}
	u(y,t)=u_\theta(y)+\sum_{n=0}^\infty A_n \exp(-\eta\mu_n^2 t)\sin(\sqrt{\rho}\mu_n y) \, . \label{general transient}
\end{equation}

\begin{solution}
Substituting $u=u_\theta+\hat{u}$ into the full PDE~\eqref{PDE} gives 
\begin{equation}
	\rho \Dp{\hat{u}}{t} = \eta\Dp{^2\hat{u}}{y^2} \, , \label{unsteady PDE} 
\end{equation}
because the gravitational source term and those involving $u_\theta$ cancel. This equation is analogous to the heat / diffusion equation that has been covered in lectures. 

Separation of variables suggests $\hat{u}(y,t)=Y(y)T(t)$, which, when substituted into~\eqref{unsteady PDE} yields
\begin{equation}
	\frac{T^\prime}{\eta T} = \frac{Y^{\prime\prime}}{\rho Y} \, , \label {unsteady subs} 
\end{equation}
by grouping time and spatial derivatives. Each side of this equation must equal the same constant value. Here we are looking for decaying solutions ($T^\prime<0$), which converge on new steady state flows, so we choose a strictly negative integration constant $-\mu_n^2$ so that the two sides of~\eqref{unsteady subs} give ODEs
\begin{align}
    Y^{\prime\prime} &= -\rho\mu_n^2 Y \, ,  \label{Y ODE} \\
    T^\prime &= -\eta\mu_n^2 T \, .  \label{T ODE}
\end{align}

General series solutions to these ODEs have terms 
\begin{align}
Y_n &= a_n \sin\left(\sqrt{\rho}\mu_n y\right)+b_n\cos\left(\sqrt{\rho}\mu_n y\right) \, ,  \label{Y sol} \\
T_n &= c_n \exp\left(-\eta\mu_n^2 t\right) \, .  \label{T sol}
\end{align}
However, due to the no slip condition ($Y=0$ at $y=0$) we can discard the cosine term in~\eqref{Y sol}, combine the coefficients and write the full general solution as~\eqref{general transient}.

\end{solution}

%-------------------------------------------------------%
\part For the case in which the angle changes instantly from $\theta=\theta_1$ to $\theta=\theta_2$, find explicit expressions for $\mu_n$ and $A_n$. You may find it convenient to make use of the integral
\begin{multline}
	I_n=\int_0^h \left(hy-\frac{y^2}{2}\right)\sin\left(\pi(n+1/2)\frac{y}{h}\right) dy \\=  h^3\left(\frac{8}{\left[(2n+1)\pi\right]^3}(\sin(n\pi)+1)+\frac{\sin(n\pi)}{(2n+1)\pi}\right) \, . \label{integral}
\end{multline}

\begin{solution}
To find the unknowns we need to employ the boundary conditions and the initial data. We already chose~\eqref{general transient} to satisfy no slip at the base and the stress-free surface condition~\eqref{stress free} means that 
\begin{equation}
	\sum_{n=0}^\infty \sqrt{\rho}\mu_n A_n \exp(-\eta\mu_n^2 t)\cos(\sqrt{\rho}\mu_n h) = 0 \, , 
\end{equation}
which can only be satisfied if 
\begin{equation}
	\mu_n = \frac{\pi}{h\sqrt{\rho}}(n+1/2) \, . 
\end{equation}

We now need to realise that $u_\theta(y)$ in the general solutions~\eqref{general transient} is the final velocity field, which for this example is with $\theta=\theta_2$. Setting $u=u_\theta(y;\theta=\theta_1)$ at $t=0$ gives
\begin{equation}
	\frac{\rho g}{\eta}\left[\sin\theta_1-\sin\theta_2\right]\left(hy-\frac{y^2}{2}\right) = \sum_{n=0}^\infty A_n \sin\left(\pi(n+1/2)\frac{y}{h}\right) \, .  \label{fourier}
\end{equation}

The Fourier coefficients $A_n$ in the series solution~\eqref{fourier} are found in general via
\begin{equation}
	A_n=\frac{2}{h}\int_0^h S(y) \; \sin\left(\pi(n+1/2)\frac{y}{h}\right) dy\, ,
\end{equation}
where for this example
\begin{equation}
	S(y)=\frac{\rho g}{\eta}\left[\sin\theta_1-\sin\theta_2\right]\left(hy-\frac{y^2}{2}\right) \, ,
\end{equation}
is the LHS of~\eqref{fourier}. Finally, we can make use of the exact integral~\eqref{integral} to write simply
\begin{equation}
	A_n =2\frac{I_n}{h} \frac{\rho g}{\eta} (\sin\theta_1-\sin\theta_2) \, .
\end{equation}

\end{solution}

%-------------------------------------------------------%
\end{parts}
%----- end Question ------------------------------------%

\end{questions}

%-------------------------------------------------------%
\end{document}
