\documentclass[answers,a4paper,addpoints,12pt]{exam}
% This is typeset using the EXAM LaTeX2e class by Philip Hirschhorn. 

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Exercise sheet 3}
% solution header

\usepackage{PDE3_tutorial}

\begin{document}

The practice questions are similar to the exercises in the textbook and in Schaum's outlines Partial Differential Equations. If you are having difficulties with these questions practice using the remaining questions from these exercise. You will get the best from this seminar by working though these examples \textbf{before} the seminar and asking your tutors to help you with questions with which you are having problems. \\

% This is the start of the questions
\begin{questions}
%----- Question ----------------------------------------%
% From exercise 9.5.2 of Advanced Modern Engineering Mathematics
\question 
Heat is flowing steadily in a metal plate whose shape is an infinite rectangle in the $(x,y)$ plane.
It occupies the region $-a\le x \le a$ and $y\ge0$ with a positive constant $a$.
The non-dimensional temperature at the point $(x,y)$ is denoted by $u(x,y)$.
The sides $x=\pm a$ are insulated, the non-dimensional temperature approaches zero for $y\to\infty$, while the side $y=0$ is maintained at a fixed temperature $-T$ for negative $x$ and $T$ for positive $x$. 
It is known that $u(x,y)$ satisfies the Laplace equation
\[
0 = \grad^2u = \pdderiv{u(x,y)}{x} + \pdderiv{u(x,y)}{y} 
\]
in the region $-a\le x \le a$ and $y\ge0$.

\begin{parts}
%-------------------------------------------------------%
\part Sketch the domain with the boundary conditions and write the boundary conditions in equation form.
\begin{solution}
Translating the description of the boundary conditions into equation form results in:
\begin{align}
u(x, y\to\infty) &= 0, \quad -a\le x \le a \label{eq:LE3_BC1}\\
\pderiv{u(x=\pm a, y)}{x} &= 0, \quad y\ge0,  \label{eq:LE3_BC2}\\
u(x, y=0) &= \begin{cases} -T &\mbox{if }-a\le x<0 \\ 
    T &\mbox{if }0< x\le a \end{cases} \label{eq:LE3_BC3} 
\end{align}
The Neumann boundary condition~\ref{eq:LE3_BC2} corresponds to the insulated sides of the domain: a zero flux indicates that no heat is lost through the sides.
\end{solution}

%-------------------------------------------------------%
\part Use the method of separated solutions to obtain the solution $u(x,y)$ in the form
\[
u(x,y) = \frac{4T}{\pi} \sum_{n=0}^\infty \frac{1}{2n+1} \exp\left[-\left(n+\frac{1}{2}\right)\frac{\pi y}{a}\right] \sin\left[\left(n+\frac{1}{2}\right)\frac{\pi x}{a}\right]
\]

\begin{solution}
From the lectures or the book we know that the following three functions
\begin{align*}
u_1(x,y) =& (A\sin \mu x+B\cos \mu x) \left(Ce^{\mu y}+De^{-\mu y}\right) \\
u_2(x,y) =&\left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y) \\
u_3(x,y) =& (Ax+B)(Cy+D)
\end{align*}
are possible solutions for the Laplace equation. 

The insulating boundary condition at $x\pm a$ can only be achieved by solution $u_1$. The other two solutions would require that $A=B=0$ and thus would be the trivial solution $u(x,y) = 0$.

The condition~\ref{eq:LE3_BC1} can only be fulfilled for solution $u_1$ if $C=0$; otherwise it would go off to infinity for $y\to\infty$. This leaves the solution
\[
u(x,y) = (A\sin \mu x+B\cos \mu x) De^{-\mu y}
\]

The condition~\ref{eq:LE3_BC2}
\[
\pderiv{u}{x} = 0, \quad x = \pm a
\]
can only be fulfilled if the solution contains only the sine or cosine function. The condition~\ref{eq:LE3_BC3} at $y=0$ is an odd function which suggests a solution comprised of odd function. For this reason we consider the separated solutions
\[
u(x,y) = b_\mu \exp(-\mu y) \sin(\mu x)
\]

To fulfil condition~\ref{eq:LE3_BC2} we need
\[
\mu a = \left(n+\frac{1}{2}\right)\pi, \quad n=0,1,2,\dots
\]

Thus, the solution takes the form
\[
u(x,y) = \sum_{n=0}^\infty b_n \exp\left[-\left(n+\frac{1}{2}\right)\frac{\pi y}{a}\right] \sin\left[\left(n+\frac{1}{2}\right)\frac{\pi x}{a}\right]
\]

The coefficients $b_n$ can be found by using the condition at $y = 0$
\begin{align*}
u(x,y=0) &= \sum_{n=0}^\infty b_n \exp\left[-\left(n+\frac{1}{2}\right)\frac{\pi 0}{a}\right] \sin\left[\left(n+\frac{1}{2}\right)\frac{\pi x}{a}\right] \\
&= \sum_{n=0}^\infty b_n \sin\left[\left(n+\frac{1}{2}\right)\frac{\pi x}{a}\right] \overset{!}{=}
\begin{cases} -T &\mbox{if }-a\le x<0 \\ 
    T &\mbox{if }0< x\le a \end{cases}
\end{align*}

The coefficients $b_n$ can be found from the integrals
\begin{align*}
b_n =& \frac{2}{2a}\int_{-a}^{a} sign(x)T \sin\left[\left(n+\frac{1}{2}\right)\frac{\pi x}{a}\right] dx \\
=& -\frac{2}{2a}\int_{-a}^{0} T \sin\left[\left(n+\frac{1}{2}\right)\frac{\pi x}{a}\right] dx \\
&+ \frac{2}{2a}\int_{0}^{a} T \sin\left[\left(n+\frac{1}{2}\right)\frac{\pi x}{a}\right] dx, \quad n=0,1,2,\dots
\end{align*}

We can simplify the integrals because the integrand over the interval from $-a$ to $a$ is even, and solve the resulting integral.
\begin{align*}
b_n &= \frac{2}{2a}\int_{-a}^{a} sign(x)T \sin\left[\left(n+\frac{1}{2}\right)\frac{\pi x}{a}\right] dx  = \frac{2}{a}\int_{0}^{a} T \sin\left[\left(n+\frac{1}{2}\right)\frac{\pi x}{a}\right] dx \\
 &= \frac{2T}{a} \left[\frac{-\cos\left[\left(n+\frac{1}{2}\right)\frac{\pi x}{a}\right]}{\left(n+\frac{1}{2}\right)\frac{\pi}{a}} \right]_0^a =  \frac{2T}{a} \left[0 + \frac{1}{\left(n+\frac{1}{2}\right)\frac{\pi}{a}} \right] = \frac{2T}{a} \frac{2a}{2\left(n+\frac{1}{2}\right)\pi} \\
 &= \frac{4T}{(2n + 1)\pi}, \quad n=0,1,2,\dots
\end{align*}
These parameter values match the parameters from the question text, as required.

We can plot the solution with the following Matlab script.
\lstinputlisting[language=Matlab]{../scripts/ES3_Q1.m}

\includegraphics[width=\columnwidth,]{ES3_LE1_solution.png}
\end{solution}

%-------------------------------------------------------%
\part Modify the Matlab code from the lectures to numerically solve the Laplace equation with the given boundary conditions. The main change will be the handling of the Neumann boundary condition.

Remark: Since you can't set the $y$ limit to infinity, you should experiment and set it to a value which is just large enough.

\begin{solution}
We simulate the north boundary condition, i.e. $u(x,y\to\infty)=0$, by setting the $y$ dimension much larger than the $x$ dimension. In the script $y$ is set between $0$ and $3$. The Dirichlet boundary conditions are set in lines 64 and 65 of the Matlab script. The south boundary ($i=1$) is set to $\pm T$ while the north boundary ($i=ny$) is set to zero.

The Neumann boundary conditions~\ref{eq:LE3_BC2} need to be set in the SOR solver. We can use the backward and forward difference to discretise the no flux boundary condition.
\begin{align*}
0 &= \pderiv{u(x= -a, y)}{x} \approx \frac{x_{i,1} - x_{i,2}}{\Delta x}, \quad i=1,2,\dots, ny \\
x_{i,1} &= x_{i,2} \\
0 &= \pderiv{u(x= a, y)}{x} \approx \frac{x_{i,nx} - x_{i,nx-1}}{\Delta x}, \quad i=1,2,\dots, ny \\
x_{i,nx} &= x_{i,nx-1}
\end{align*}
In the \textit{while} loop (line 74) we set $u_{i,1}=u_{i,2}$ and $u_{i,nx} = u_{i,nx-1}$ ($i=1,2,\dots, ny$) which ensures that there is no flux in the $x$ direction at the left and right boundary.

The following plots show the contours and surface of the numerical solution.
\includegraphics[width=0.5\columnwidth,]{ES3_LE1_solution_numerical1.png}
\includegraphics[width=0.5\columnwidth,]{ES3_LE1_solution_numerical2.png}

The plot was generated with the Matlab script from part (b).

\end{solution}

\end{parts}
%----- end Question ------------------------------------%

\end{questions}

%-------------------------------------------------------%
\newpage
{\large\textbf{Formula sheet}}

\begin{enumerate}
\item Useful integrals
\begin{align*}
\int x \sin ax \, \mathrm{d}x &= \frac{1}{a^2} \bigl ( \sin a x - a x \cos ax \bigr ) \\
\int x \cos ax \, \mathrm{d}x &= \frac{1}{a^2} \bigl ( \cos a x + a x \sin ax \bigr ) \\
\int x^2 \sin ax \, \mathrm{d}x &= \frac{2x}{a^2} \sin ax - \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \cos ax \\
\int x^2 \cos ax \, \mathrm{d}x &= \frac{2x}{a^2} \cos ax + \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \sin ax
\end{align*}
\item Trigonometric identities
\begin{align*}
\sin(a\pm b) &= \sin(a) \cos(b) \pm \cos(a) \sin(b) \\
\cos(a\pm b) &= \cos(a) \cos(b) \mp \sin(a) \sin(b) \\
2\sin(a) \cos(b) &= \sin(a+b) + \sin(a-b) \\
2\sin(a) \sin(b) &= \cos(a+b) - \cos(a-b) \\
2\cos(a) \cos(b) &= \cos(a+b) + \cos(a-b)
\end{align*}
\end{enumerate}

\end{document}
