\documentclass[answers,a4paper,addpoints,12pt]{exam}
% This is typeset using the EXAM LaTeX2e class by Philip Hirschhorn. 

% headers and footers - exam heading on top, page count bottom
\pagestyle{headandfoot}
\header{SCEE09004}{\textbf{Partial Differential Equations 3}}{Exercise sheet 1}
% solution header

\usepackage{PDE3_tutorial}

\begin{document}
% This is the start of the questions
\begin{questions}
%----- Question ----------------------------------------%
% From exercise 9.2.6 of Advanced Modern Engineering Mathematics
\question
Consider the the function
\[
u(x,y) = x^4 - 6x^2y^2 + y^4
\]

\begin{parts}
\part Verify that $u(x,y)$ satisfies the Laplace equation.

\begin{solution}
Calculate the first and second partial derivatives of $u(x,y)$ with respect to $x$ and $y$ to get
\begin{align}
\pderiv{u(x,y)}{x} &= 4x^3 - 12 x y^2 \nonumber\\
\pdderiv{u(x,y)}{x} &= 12x^2 - 12 y^2 \label{eq:ES1-Q1_uxx}\\
\pderiv{u(x,y)}{y} &= - 12 x^2 y + 4 y^3 \nonumber\\
\pdderiv{u(x,y)}{y} &= - 12x^2 + 12 y^2
\label{eq:ES1-Q1_uyy}
\end{align}
Inserting Eqs.~\ref{eq:ES1-Q1_uxx} and \ref{eq:ES1-Q1_uyy} into the Laplace equation gives
\[
0 = \grad^2u = \pdderiv{u(x,y)}{x} + \pdderiv{u(x,y)}{y} = 12x^2 - 12 y^2 - 12x^2 + 12 y^2 = 0
\]
thus showing that $u(x,y)$ satisfies the Laplace equation.
\end{solution}

\part Use Matlab or Python to generate a surface plot of $u(x,y)$ in the square given by $0\le x\le1$ and $0\le y\le 1$.
\begin{solution}
In the Matlab script we first define two arrays for the points along the $x$ and $y$ axis. 
This is followed by the generation of the grid. For $N=4$ we get the following arrays.
\begin{lstlisting}
x =

   0.00000   0.33333   0.66667   1.00000

y =

   0.00000   0.33333   0.66667   1.00000

X =

   0.00000   0.33333   0.66667   1.00000
   0.00000   0.33333   0.66667   1.00000
   0.00000   0.33333   0.66667   1.00000
   0.00000   0.33333   0.66667   1.00000

Y =

   0.00000   0.00000   0.00000   0.00000
   0.33333   0.33333   0.33333   0.33333
   0.66667   0.66667   0.66667   0.66667
   1.00000   1.00000   1.00000   1.00000
\end{lstlisting}
This shows that the columns of the meshgrids $X$ and $Y$ keep the $x$ values constant while varying the $y$ values.
Similarly, the rows of the meshgrids $X$ and $Y$ keep the $y$ values constant while varying the $x$ values.

Next we define the function $u(x,y)$ as an anonymous function and calculate the solution on the meshgrid.
Finally, the solution is plotted as a surface plot.
\lstinputlisting[language=Matlab]{../scripts/ES1_Q1.m}

\includegraphics[width=0.7\columnwidth,]{ES1-Q1_solution.png}
\end{solution}

\part Give the boundary conditions fulfilled by $u(x,y)$ on the boundaries of the square given by $0\le x\le1$ and $0\le y\le 1$.

\begin{solution}
By inserting the boundary values into the equation for $u(x,t)$ we get the following boundary conditions 
\begin{align*}
u(x, 0) &= x^4, \qquad &0\le x\le 1 & \to \text{bottom boundary} \\
u(x, 1) &= x^4 - 6x^2 + 1, \qquad &0\le x\le 1 & \to \text{top boundary} \\
u(0, y) &= y^4, \qquad &0\le y\le 1 & \to \text{left boundary} \\
u(1, y) &= 1 - 6y^2 + y^4, \qquad &0\le y\le 1 & \to \text{right boundary} 
\end{align*}
\end{solution}

\end{parts}
%----- end Question ------------------------------------%

%----- Question ----------------------------------------%
% Similar to Elliptic PDE Example 1
\question
Consider the the function
\[
u(x,y) = e^{-x} \sin(y)
\]

\begin{parts}
\part Verify that $u(x,y)$ satisfies the Laplace equation.

\begin{solution}
Calculate the first and second partial derivatives of $u(x,y)$ with respect to $x$ and $y$.
\begin{align}
\pderiv{u}{x} &= -e^{-x} \sin(y) \nonumber \\
\pdderiv{u}{x} &= e^{-x} \sin(y) \label{eq:ES1-Q2_uxx} \\
\pderiv{u}{y} &= e^{-x} \cos(y) \nonumber \\
\pdderiv{u}{y} &= -e^{-x} \sin(y) \label{eq:ES1-Q2_uyy}
\end{align}
Inserting Eqs.~\ref{eq:ES1-Q2_uxx} and \ref{eq:ES1-Q2_uyy} into the Laplace equation gives
\[
0 = \grad^2u = \pdderiv{u(x,y)}{x} + \pdderiv{u(x,y)}{y} = e^{-x} \sin(y) - e^{-x} \sin(y) = 0
\]
thus showing that $u(x,y)$ satisfies the Laplace equation.
\end{solution}

\part Use Matlab or Python to generate a surface plot of $u(x,y)$ in the square given by $0\le x\le1$ and $0\le y\le \pi$.

\begin{solution}
\lstinputlisting[language=Python]{../scripts/ES1_Q2.py}

\includegraphics[width=0.7\columnwidth,]{ES1-Q2.png}
\end{solution}

\part Give the boundary conditions fulfilled by $u(x,y)$ on the boundaries of the square given by $0\le x\le1$ and $0\le y\le \pi$.

\begin{solution}
By inserting the boundary values into the equation for $u(x,t)$ we get the following boundary conditions 
\begin{align*}
u(x, 0) &= 0, \qquad &0\le x\le 1 & \to \text{bottom boundary} \\
u(x, \pi) &= 0, \qquad &0\le x\le 1 & \to \text{top boundary} \\
u(0, y) &= \sin(y), \qquad &0\le y\le \pi & \to \text{left boundary} \\
u(1, y) &= e^{-1} \sin(y), \qquad &0\le y\le \pi & \to \text{right boundary} 
\end{align*}
\end{solution}
\end{parts}
%----- end Question ------------------------------------%

%----- Question ----------------------------------------%
% Extension to unknown PDEs
\question Show that the function
\[
u(x,t) = e^{-kt} \cos(mx) \cos(nt)
\]
is a solution to the partial differential equation
\[
c^2 \pdderiv{u}{x} = \pdderiv{u}{t} + 2k \pderiv{u}{t}
\]
if the constants with parameters $k$, $m$, $n$ and $c$ are related by the equation
\[
n^2 + k^2 = c^2 m^2
\]

\begin{solution}
We start by calculating the partial derivatives of $u(x,t)$ to get
\begin{align}
\pderiv{u}{t} &= -k e^{-kt} \cos(mx) \cos(nt) - n e^{-kt} \cos(mx) \sin(nt) \label{eq:ES1-Q3_ut} \\
\pdderiv{u}{t} &= k^2 e^{-kt} \cos(mx) \cos(nt) + kn e^{-kt} \cos(mx) \sin(nt) \nonumber \\
 & \quad + kn e^{-kt} \cos(mx) \sin(nt) - n^2 e^{-kt} \cos(mx) \cos(nt) \nonumber \\
 &= (k^2 - n^2) e^{-kt} \cos(mx) \cos(nt) + 2kn e^{-kt} \cos(mx) \sin(nt) \label{eq:ES1-Q3_utt} \\
\pderiv{u}{x} &= -me^{-kt} \sin(mx) \cos(nt) \nonumber \\
\pdderiv{u}{x} &= -m^2e^{-kt} \cos(mx) \cos(nt) \label{eq:ES1-Q3_uxx} 
\end{align}

Now we substitute the results from equations~\ref{eq:ES1-Q3_ut}-\ref{eq:ES1-Q3_uxx} in the partial differential equation. 

\begin{align*}
- c^2 m^2e^{-kt} \cos(mx) \cos(nt) &= (k^2 - n^2) e^{-kt} \cos(mx) \cos(nt) + 2kn e^{-kt} \cos(mx) \sin(nt) \\
 & \quad - 2k^2 e^{-kt} \cos(mx) \cos(nt) - 2k n e^{-kt} \cos(mx) \sin(nt) \\
 &= -(k^2 + n^2) e^{-kt} \cos(mx) \cos(nt)
\end{align*}
We can divide the resulting expression with $u(x,t)$ to get the required relationship between the four parameters, thus proving that $u(x,t)$ is a solution as long as the following holds
\[
c^2 m^2 = k^2 + n^2 = n^2 + k^2
\]
\end{solution}
%----- end Question ------------------------------------%
\end{questions}
\end{document}
