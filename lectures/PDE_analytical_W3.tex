\documentclass[aspectratio=169]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Properties of solutions of the Laplace equation and boundary conditions}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Recap}
% -------------------------------------------------------- %
\subsection{Laplace equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace Equation}
\begin{block}{The Laplace equation is}
\[
\grad^2u = \pdderiv{u}{x_1} + \pdderiv{u}{x_2} + \cdots + \pdderiv{u}{x_n} = 0
\]
\end{block}
\begin{itemize}
\item It is the most fundamental \textcolor{blue}{elliptic} PDE which models the steady state of a diffusion like process.
\item Used to describe the pressure in an incompressible flow, steady state diffusion and heat conduction, stress and strain, hydrostatics, electrostatics, ground water flow, etc.
\item Unlike the other PDEs the solution only involves spatial derivatives -- \textcolor{red}{there is no time dependency.}
\item Can sometimes be solved by separation of variables as shown last week
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Separated solutions}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Recipe for separated solutions}
Consider the three possible solutions 
\begin{align*}
u_1(x,y) =& (A\sin \mu x+B\cos \mu x)\left(Ce^{\mu y}+De^{-\mu y}\right)&\lambda=-\mu^2<0, \\
 =& (A\sin \mu x+B\cos \mu x)\left(\tilde{C}\cosh \mu y+\tilde{D}\sinh \mu y\right) & \\
u_2(x,y) =& \left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y)&\lambda=\mu^2>0,\\
 =& \left(\tilde{A}\cosh \mu x+\tilde{B}\sinh \mu x\right)(C\sin \mu y+ D\cos \mu y) & \\
u_3(x,y) =& (Ax+B)(Cy+D)&\lambda=0
\end{align*}
and go through the boundary conditions to
\smallskip
\begin{enumerate}
\item Eliminate basic solutions
\item Specify the permissible values of $\mu$
\item Superpose solutions to fit the initial condition
\end{enumerate}
\end{frame}

% -------------------------------------------------------- %
\section{Fourier series}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Recap of Fourier series}
\begin{itemize}
\item Was covered extensively in Engineering Mathematics 2A
\end{itemize}
\vskip0.0cm
\begin{enumerate}
\item For any periodic function $f(x)$ with period $T$ we can find coefficients $a_n$ and $b_n$ so that
\[
f(x) = \frac{1}{2} a_0 + \sum_{n=1}^\infty a_n \cos\left(\frac{2n\pi x}{T}\right) + \sum_{n=1}^\infty b_n \sin\left(\frac{2n\pi x}{T}\right)
\]
\item The coefficients of the full-range extension can be calculated by
\begin{align*}
a_0 &= \frac{2}{T} \int_{0}^{T} f(x) dx \\
a_n &= \frac{2}{T} \int_{0}^{T} f(x) \cos\left(\frac{2n\pi x}{T}\right) dx \\
b_n &= \frac{2}{T} \int_{0}^{T} f(x) \sin\left(\frac{2n\pi x}{T}\right) dx
\end{align*}
\end{enumerate}
\end{frame}

% -------------------------------------------------------- %
\subsection{Periodic extension}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Periodic extensions}
\begin{itemize}
\item Full-range periodic extension
\begin{flushright}
\includegraphics[width=7.3cm]{Glyn_Advanced_7_17}
\end{flushright}
\item Even periodic extension
\begin{flushright}
\includegraphics[width=7.3cm]{Glyn_Advanced_7_18}
\end{flushright}
\item Odd periodic extension
\begin{flushright}
\includegraphics[width=7.3cm]{Glyn_Advanced_7_19}
\end{flushright}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Half-range extension}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Half-range extensions}
\begin{itemize}
\item The half-range extensions have half the frequency: the factor $2$ is removed from the cosine/sine
\item Even periodic extension: cosine series
\[
a_n = \frac{2}{T} \int_{0}^{T} f(x) \cos\left(\frac{n\pi x}{T}\right) dx
\]
\item Odd periodic extension: sine series
\[
b_n = \frac{2}{T} \int_{0}^{T} f(x) \sin\left(\frac{n\pi x}{T}\right) dx
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.30}
% -------------------------------------------------------- %
\subsection{Problem}
% Check: x>=0???
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.30}
\begin{block}{Solve the Laplace equation $\nabla^2 u = 0$ subject to the following boundary conditions}
\begin{align*}
u(x,0) &= 0 & x\ge 0 \\
u(x,1) &= 0 & x\ge 0 \\
u(x,y) &\rightarrow 0 & x\rightarrow\infty, 0\le y\le 1 \\
u(0,y) &= 1 & 0\le y \le 1
\end{align*}
\end{block}
To satisfy the third condition we need a solution which is exponential in x, so we need to use $u_2$
\[
u_2(x,y) = \left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y)
\]
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.30}
Remove the subscript and step through the boundary conditions to find the parameters.
\[
u(x,y) = \left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y)
\]
\begin{enumerate}
\item The condition $\lim_{x\rightarrow\infty}u(x,y)=0$ is only satisfied if $A=0$ because the positive exponential is unbounded. Thus the solution simplifies to

\[
u(x,y) = e^{-\mu x}(C^*\sin \mu y+D^*\cos \mu y)
\]
where $C^*=BC$ and $D^*=BD$.
\item Now, consider the first boundary condition, $u(x,0)=0$
\begin{align*}
u(x,y=0) &= e^{-\mu x}(C^*\sin(\mu 0)+D^* \cos(\mu 0))  \\
  &= e^{-\mu x}D^* \overset{!}{=} 0
\end{align*}
which implies that $D^*=0$. 
\end{enumerate}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.30, continued}
\begin{enumerate}
\item[3.] The second boundary condition, $u(x,1)=0$ gives
\begin{align*}
u(x,y=1) &= e^{-\mu x}C^*\sin(\mu 1) \overset{!}{=} 0 \\
 & \implies\ \sin\mu=0
\end{align*}
so $\mu=n\pi,\,n=0,1,2,\ldots$
\item[4.] Because the Laplace equation is linear we can superpose the solutions, to get
\[
u(x,y) = \sum_{n=0}^\infty C^*_ne^{-n\pi x}\sin n\pi y,\quad 0\le y \le 1.
\]
\end{enumerate}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.30, continued}
\begin{enumerate}
\item[5.] From the final boundary condition, $u(0,y)=1,\ 0\le y\le1$ it follows that
\[
1 \overset{!}{=} \sum_{n=0}^\infty C^*_n\sin n\pi y,\quad 0\le y \le 1.
\]
which is a classical Fourier series problem. This is a half-range Fourier sine series with period $T=1$ , so that with the formula from earlier we get
\[
C^*_n=2\int_0^1\sin n\pi y\ \text{d}y=
\begin{cases}
4/n\pi&n\text{ odd}\\
0&n \text{ even}
\end{cases}
\]
\end{enumerate}

The complete solution is therefore
\textcolor{red}{
\[u(x,y)=\frac{4}{\pi}\sum_{n=1}^\infty \frac{1}{2n-1}e^{-2(n-1)\pi x}\sin\left((2n-1)\pi y\right)\]}
Evaluating this series at $x=0$ requires more than 30 terms to be evaluated, while at $x=1$ only one or two terms are needed.
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.30 - plot the first 4 terms of the sum}
\begin{tikzpicture}
\begin{axis}[
title={$\frac{4}{\pi}\sum_{n=1}^\infty \frac{1}{2n-1}e^{-2(n-1)\pi x}\sin\left((2n-1)\pi y\right)$},
xlabel=$x$, ylabel=$y$,
]
\addplot3[
surf,
domain=0:1, 
domain y=0:1,
]
{4/pi*(exp(-pi*x)*sin(180*y)
+exp(-3*pi*x)*sin(3*180*y)/3
+exp(-5*pi*x)*sin(5*180*y)/5
+exp(-7*pi*x)*sin(7*180*y)/7
)};
\end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\section{Boundary conditions}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Types of boundary conditions}
\begin{itemize}
\item So far we have only looked at the boundary conditions on the left
\item What do these conditions signify?
\item Can you think of any other boundary conditions?
\end{itemize}
\centerline{\includegraphics[height=0.5\textheight]{Laplace_derivation}
\pause
\includegraphics[height=0.5\textheight]{Laplace_BC}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Definitions}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Types of boundary conditions}
If $\Omega$ is the domain on which the given equation is to be solved and $\partial\Omega$ denotes its boundary, we can have
\begin{itemize}
\item Dirichlet 
\[u\text{ is specified on } \partial\Omega.\]
\item Neumann 
\[\pderiv{u}{n}\text{ is specified on }\partial\Omega.\]
\item Cauchy  
\[\text{both }u\text{ and }\pderiv{u}{n}\text{ are specified on }\partial\Omega.\]
\item Robin 
\[au+B\pderiv{u}{n}=g\text{ on }\partial\Omega.\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Mixed boundary conditions}
Two or more BC of different types are specified on $\partial\Omega$

\centerline{\includegraphics[height=0.75\textheight]{MixedBC.pdf}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Practical meaning}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Boundary conditions and physics}
Consider that the Laplace equation is used to model heat conduction in a sheet of metal, 
\begin{itemize}
\item \textcolor{blue}{Dirichlet} conditions -- the edges of the sheet are held at a specified temperature
\item \textcolor{blue}{Neumann} conditions -- the heat flux at the edges is given 
\end{itemize}
The important question is: Can a solution be found?
\vskip0.2cm
\centerline{\includegraphics[width=0.5\columnwidth]{LaplaceBC.pdf}}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest: solution shape}
\vskip0.1cm
\TopHat : Which is the solution shape for the Laplace equation with following boundary conditions?
\vskip-0.5cm
\begin{tiny}
\begin{align*}
u(x,0) &= 1 + x & 0\le x\le 2 \\
u(x,1) &= 1 - x & 0\le x\le 2 \\
\pderiv{u}{x}(2, y) &= 0, &0\le y\le 1 \\
u(0,y) &= 1 & 0\le y \le 1
\end{align*}
\end{tiny}
\begin{overprint}
\onslide<1>
\vskip-0.8cm
\begin{block}{ConcepTest: possible solutions}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_1contour}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_5contour}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_3contour}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_4contour} \\
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_1surf}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_5surf}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_3surf}
\includegraphics[width=0.2\columnwidth]{Laplace_concepTest_4surf}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\begin{enumerate}
\item Second from the left
\vskip-0.3cm
\end{enumerate}
\includegraphics[width=0.3\columnwidth]{Laplace_concepTest_5contour}
\includegraphics[width=0.3\columnwidth]{Laplace_concepTest_5surf}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\subsection{Dependence on data}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Is the PDE well-posed?}
\vskip-0.1cm
\begin{block}{The behaviour of PDEs is defined by}
\vskip-0.1cm
\begin{itemize}
\item Initial and boundary conditions
\item Coefficient functions
\item Inhomogeneous terms
\item These define the data on which the solution depends
\end{itemize}
\end{block}
\vskip-0.2cm
\begin{block}{Well-posed problem}
\vskip-0.1cm
\begin{enumerate}
\item A solution exists
\item The solution is unique
\item The solution depends continuously on the data: small changes in the data produce small changes in the solution
\end{enumerate}
\end{block}
\vskip-0.2cm
\begin{block}{Ill-posed problem}
\vskip-0.1cm
\begin{itemize}
\item Any of the requirements of well-posed problems is not fulfilled
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Appropriateness of Boundary Conditions}
\centerline{\includegraphics[width=0.85\columnwidth]{BCMatrix.pdf}}
\end{frame}

% -------------------------------------------------------- %
\section{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest: Extreme point of the solution}
\TopHat : Where can the extreme points, e.g. maximum or minimum, of the solution to the Laplace equation be located for a non-constant solution?
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item On the boundary
\item Inside the solution domain
\item Anywhere
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\begin{block}{Solution}
\begin{enumerate}
\item On the boundary
\begin{itemize}
\item Assume there is a maximum temperature on the inside
\item Heat flows from hot to cold places
\item This heat maximum would dissipate
\item This is called the maximum principle which holds for all non-constant solutions of the Laplace equation
\end{itemize}
\end{enumerate}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\section{Next lecture}
% -------------------------------------------------------- %
\begin{frame}
\frametitle[squezze]{Any questions?}
\vskip0.2cm
\begin{block}{Preparation for the next class}
\begin{itemize}
\item Topic: \textbf{Poisson equation and different coordinate systems}
\item Read sections 9.5 in Advanced Modern Engineering Mathematics before next lecture
\item Check the \WeeklyTarget
\item Tell me on \TopHat
\begin{itemize}
\item What did you find confusing or difficult about what you read? 
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\item Exercises 9.5.2 in Advanced Modern Engineering Mathematics to strengthen material from this lecture
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}