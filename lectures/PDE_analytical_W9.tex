\documentclass{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Hyperbolic problems - Separated solutions}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Recap}
% -------------------------------------------------------- %
\subsection{d'Alembert solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{d'Alembert solution in $(x,t,u)$ space}
The solution of the wave equation for an initial velocity, $\pderiv{u(x,0)}{t}=G(x),$ and an initial displacement, $u(x,0)=F(x),\quad \forall x,$ is
\[
u(x,t) =\frac12\left[F(x+ct)+F(x-ct)\right]+\frac1{2c}\int_{x-ct}^{x+ct} G(z)\,\text{d}z
\]
\centering
\includegraphics[width=7cm]{Ex9-11}
\end{frame}

% -------------------------------------------------------- %
\section{Method of characteristics}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Domains of dependence and influence}
\centerline{\includegraphics[width=\columnwidth]{domainofdependence}}

\begin{itemize}
\item Dependence --- the solution at $P$ is determined by the initial conditions on the boundary $AB$. 
\item Influence --- the initial conditions on $AB$ influence the shaded area. 

\textcolor{red}{Data outside of this region cannot be influenced}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Procedure}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Method of characteristics 1/2}
\begin{itemize}
\item We can find the solution at the point $P$ as
\[
u(P) = f(B)+g(A)
\]
because $f$ is constant along the \textcolor{red}{characteristic} $BP$ and $g$ is constant along the \textcolor{red}{characteristic} $AP$.  
\item The problem is now to compute $f(B)$ and $g(A)$ on the $t=0$ boundary.
\item Typical conditions along $t=0$ are $u(x,0)=F(x)$ and $\pderiv{u(x,0)}{t}=G(x)$ 
\item Use $u(x,t) = f(x+ct) + g(x-ct)$ and calculate
{\small\begin{align*}
c\pderiv{u}{x}+\pderiv{u}{t} &= cf^\prime(x+ct) + cg^\prime(x-ct) + cf^\prime(x+ct) - cg^\prime(x-ct) \\
&= 2cf^\prime(x+ct) \text{ and similarly} \\
c\pderiv{u}{x} - \pderiv{u}{t} &= 2cg^\prime(x-ct)
\end{align*}}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Method of characteristics 2/2}
\begin{itemize}
\item On $t=0$ we know that $\pderiv{u}{x}=F^\prime(x)$ and $\pderiv{u}{t}=G(x)$, so
\begin{align*}
cF^\prime(x)+G(x) &= 2cf^\prime(x) \\
cF^\prime(x)-G(x) &= 2cg^\prime(x)
\end{align*}
\item Since $F$ and $G$ are given we can compute
\begin{align*}
f^\prime(x) &= \frac12\left[F^\prime(x)+\frac{G(x)}{c}\right] \\
g^\prime(x) &= \frac12\left[F^\prime(x)-\frac{G(x)}{c}\right] 
\end{align*}
\item So $f(x)$ and $g(x)$ can be found by integration.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.12}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.12: wave equation with $c=1$}
\begin{center}
\includegraphics[width=5.8cm]{GlynJames_9_14}
\end{center}
\begin{align*}
u(x,0) &= V(x) =1, \quad \forall x > 0 \\
\pderiv{u(x,0)}{t} &= 0, \quad\forall x > 0 \\
u(0, t) &= 0, \quad t>0
\end{align*}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.12 continued}
\begin{itemize}
\item Find $f$ and $g$ for the general solution
\[
u(x,t) = f(x-t) + g(x+t)
\]
where $c=1$ due to the slope of the characteristics
\item For $x>t$ we get
\begin{align*}
f(x) = g(x) &= \frac{1}{2} V(x) \\
u(x,t) &= \frac{1}{2}\left[V(x+t) + V(x-t)\right] 
\end{align*}
\item For $x<t$ we need to take the boundary condition at $x=0$ into account
\begin{align*}
0 = u(0,t) &= f(-t) + g(t) \Rightarrow f(-z) = -g(z) = -\frac{1}{2} V(z)\\
u(x,t) &= \frac{1}{2}\left[V(x+t) - V(t-x)\right]
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Comments}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Comments}
\begin{itemize}
\item For non-constant coefficient equations the characteristics lines will be curved.
\item Intersecting characteristics may lead to shocks in the solution.
\item The method of characteristics is the basis of both space marching schemes for solving hyperbolic equations and for  semi-Lagrangian methods.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Formal classification}
% -------------------------------------------------------- %
\subsection{Procedure}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Formal Classification of second order PDEs}
\begin{itemize}
\item Given the general second-order PDE
\[
Au_{xx}+2Bu_{xy}+Cu_{yy}+Du_x+Eu_y+F = 0
\]
where $A,\, B,\, C,\ldots$ are constants. 
\item We can make the change of variable to use characteristics $r=ax+y$ and $s=x+by$
\item We obtain (see section 9.8.1)
\[
(A+2bB+b^2C)\left[u_{ss}+\frac{AC-B^2}{(A+Bb)^2}u_{rr}\right]+\ldots = 0
\]
\item The $\ldots$ indicate low order derivatives
\item The behaviour of the PDE depends on the sign of $AC-B^2$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Elliptic equations}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Classification: elliptic equation}
\begin{itemize}
\item $AC-B^2>0$ --- Elliptic equation
\item Writing $\lambda^2=(AC-B)^2/(A+bB)^2$ we obtain
\[
\alpha(u_{ss}+\lambda^2u_{rr})+\ldots = 0
\] 
\item This can be expressed as
\[
u_{ss}+u_{qq}
\]
where $q=\frac{r}\lambda$
\item This is Laplace's Equation
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Parabolic equations}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Classification: parabolic equation}
\begin{itemize}
\item $AC-B^2=0$ --- Parabolic equation
\item Since $AC-B^2=0$ we have 
\[
u_{ss}+\ldots = 0
\]
\item This is very similar to the heat conduction/diffusion equation
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Hyperbolic equations}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Classification: hyperbolic equation}
\begin{itemize}
\item  $AC-B^2<0$ --- Hyperbolic equation
\item Writing $-\mu^2=(AC-B)^2/(A+bB)^2$ we obtain
\[
\alpha(u_{ss}-\mu^2u_{rr})+\ldots = 0
\]
\item This can be expressed as
\[
u_{ss}-u_{tt} = 0
\]
where $t=\frac{r}\mu$
\item This is the wave equation
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Separated Solutions}
% -------------------------------------------------------- %
\subsection{Motivation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Vibrating string}
\includegraphics[width=\columnwidth]{VibratingString03-400x220}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Remember the fundamental mode of a vibrating string}
\[
u(x,t) = u_0 \cos\frac{\pi c t}{L} \sin\frac{\pi x}{L}
\]

\begin{tikzpicture}[scale=0.93]
 \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black,domain=0:1, samples=101] {sin(x*180)};
  \addplot[blue, dashed, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(45)};
  \addplot[cyan, loosely dotted, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(90)};
  \addplot[green, dashdotted, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(135)};
  \addplot[red, dotted, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(180)};
  \legend{$t=0.00$s,$t=0.25$s,$t=0.50$s,$t=0.75$s,$t=1.00$s}
 \end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\subsection{Procedure}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Separation of Variables}
\begin{block}{Solve the PDE by seeking solutions of the form}
\[
u(x,y) = X(x)Y(y)
\]
where $X(x)$ and $Y(y)$ are single functions of single variables.
\end{block}
\begin{itemize}
\item We can sometimes find $X$ and $Y$ as solutions of \textcolor{blue}{ordinary differential equations}.
These are often much easier to solve than PDEs and it \textcolor{red}{may} be possible to build up full solutions of the PDE in terms of $X$ and $Y$.
\item This can be applied to many types of PDE and has already been used for
\begin{itemize}
\item Laplace equation
\item Heat conduction/diffusion equation
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Vibrating string}
\begin{itemize}
\item We are looking for solutions of the wave equation, 
\[
\frac1{c^2}\pdderiv{u}{t} = \pdderiv{u}{x},
\]
of the form of either 
\[
u = v(x)\sin c\lambda t \text{ or } u = v(x)\cos c\lambda t
\]
\item Substituting into the wave equation we obtain the ODE,
\[
\frac{1}{v}\frac{\text{d}^2v}{\text{d}x^2}=-\lambda^2
\]
\item This is a simple harmonic equation with solutions,
\[
v = \sin\lambda x\text{ or } v = \cos\lambda x
\] 
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}[label=separated_solution]
\frametitle{Vibrating string continued}
Using  
\[
v(x) = \sin\lambda x\text{ and } v(x) = \cos\lambda x
\]
we can build up the general solution using a linear combination of the four basic solutions
\begin{enumerate}
\item \[u_1(x,t) = \cos\lambda ct\sin\lambda x\]
\item \[u_2(x,t) = \cos\lambda ct\cos\lambda x\]
\item \[u_3(x,t) = \sin\lambda ct\sin\lambda x\]
\item \[u_4(x,t) = \sin\lambda ct\cos\lambda x\]
\end{enumerate}

\hyperlink{example_9_14}{\beamergotobutton{Example 9.14 case (i)}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Recipe}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Recipe for separated solutions}
\begin{itemize}
\item Consider the four basic solutions 
\begin{enumerate}
\item $u_1(x,t) = \cos\lambda ct\sin\lambda x$
\item $u_2(x,t) = \cos\lambda ct\cos\lambda x$
\item $u_3(x,t) = \sin\lambda ct\sin\lambda x$
\item $u_4(x,t) = \sin\lambda ct\cos\lambda x$
\end{enumerate}
\item Go through the initial and boundary conditions to
\begin{enumerate}
\item Eliminate basic solutions
\item Specify the permissible values of $\lambda$
\item Superpose solutions to fit the initial condition
\end{enumerate}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.14 case (ii)}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.14 case (ii): solve the wave equation}
Solve the wave equation
\[
\frac1{c^2}\pdderiv{u}{t} = \pdderiv{u}{x},
\]
for a string stretched between $x=0$ and $x=l$ subject to the boundary conditions
\begin{align*}
u(0,t) &= u(l,t)=0  &\forall t\ge0 \\
\pderiv{u(x,0)}{t} &= 0  &0\le x\le l \\
u(x,0) &= F(x) &0\le x\le l 
\end{align*}
where
\[
F(x) = \begin{cases}
x & 0\le x \le \frac{l}{2} \\
l-x & \frac{l}{2} \le x \le l
\end{cases}
\]
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.14 case (ii)}
\begin{itemize}
\item By inspection neither solutions \textcolor{blue}{2} or \textcolor{blue}{4} will satisfy the first boundary condition, 
\[
u(0,t) = 0\ \forall t\ge0. 
\] 
because the $\cos(0)=1$
\item Solutions \textcolor{blue}{1} and \textcolor{blue}{3} will satisfy the boundary condition, 
\[
u(l,t) = 0\ \forall t\ge0, 
\]
provided
\[
\sin \lambda l = 0,\text{ or }\lambda l=n\pi,\ n=1,\, 2,\, 3,\, \dots 
\]
\item This means the solution takes the form of either
\begin{align*}
u_1(x,t) &= \cos\frac{nc\pi t}{l}\sin\frac{n\pi x}{l}, \quad\text{ or } \\
u_3(x,t) &= \sin\frac{nc\pi t}{l}\sin\frac{n\pi x}{l}, \quad \ n=1,\, 2,\, 3,\, \dots 
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.14 case (ii) continued}
\begin{itemize}
\item Of these only
\[
u_1(x,t) = \cos\frac{nc\pi t}{l}\sin\frac{n\pi x}{l}, \quad n=1,\, 2,\, 3,\, \dots \]
will satisfy the initial condition
\[
\pderiv{u(x,0)}{t}=0
\]
\item \textcolor{red}{But, this will not, in general, satisfy the initial condition
\[
u(x,0) = F(x)
\]
}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.14 case (ii) continued}
\begin{itemize}
\item Fortunately we are dealing with the \textcolor{blue}{linear wave equation} so we can \textcolor{red}{superpose solutions}, i.e. the sum of these solutions is also a solution.  
\item So we have
\[
u(x,t) = \sum_{n=1}^\infty b_n\cos\frac{nc\pi t}{l}\sin\frac{n\pi x}{l}
\]
\item The initial condition $u(x,0)=F(x)$ requires that the following holds
\[
\sum_{n=1}^\infty b_n\sin\frac{n\pi x}{l} \overset{!}{=}\begin{cases}
x & 0\le x \le \frac{l}{2} \\
l-x & \frac{l}{2} \le x \le l
\end{cases}
\]
\item The values of $b_n$ can be found with the methods from Fourier series 
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.14 case (ii) continued}
\begin{itemize}
\item Using the Fourier series sine expansion
\begin{align*}
b_n &= \frac{2}{l}\int_0^l F(x)\sin\frac{n\pi x}{l}\text{d}x \\
&= \frac{2}{l}\int_0^\frac{l}{2}x\sin\frac{n\pi x}{l}\text{d}x+\frac{2}{l}\int_\frac{l}{2}^l (l-x)\sin\frac{n\pi x}{l}\text{d}x \\
&= \frac{4l}{\pi^2n^2}\sin \frac12n\pi, \quad n=1,2,3,\dots \\
&= \frac{4l}{\pi^2 (2k-1)^2} (-1)^{k+1}, \quad k=1,2,3,\dots
\end{align*}
\item The complete solution is therefore
\[
u(x,t) = \frac{4l}{\pi^2}\sum_{k=1}^\infty\frac{(-1)^{k+1}}{(2k-1)^2}\cos\left(\frac{(2k-1)c\pi t}{l}\right)\sin\left(\frac{(2k-1)\pi x}{l}\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.14 case (ii) solution - 6 terms of the series}
\begin{itemize}
\item Six terms of the series solution for six different times
\end{itemize}
\begin{tikzpicture}[scale=0.95]
 \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black] coordinates {(0,0) (0.5,0.5) (1,0)};
%
  \addplot[blue, dotted, line width=1.5,domain=0:1, samples=101] {4/(pi*pi)*(cos(180/3*0.2)*sin(180*x)
  -cos(180*0.2)*sin(3*180*x)/9+cos(5*180/3*0.2)*sin(5*180*x)/25-cos(7*180/3*0.2)*sin(7*180*x)/49)
  +cos(9*180/3*0.2)*sin(9*180*x)/81-cos(11*180/3*0.2)*sin(11*180*x)/121};
%
  \addplot[cyan, dashed, line width=1.5,domain=0:1, samples=101] {4/(pi*pi)*(cos(180/3*0.4)*sin(180*x)
  -cos(180*0.4)*sin(3*180*x)/9+cos(5*180/3*0.4)*sin(5*180*x)/25-cos(7*180/3*0.4)*sin(7*180*x)/49)
  +cos(9*180/3*0.4)*sin(9*180*x)/81-cos(11*180/3*0.4)*sin(11*180*x)/121};
%
  \addplot[magenta, dashdotted, line width=1.5,domain=0:1, samples=101] {4/(pi*pi)*(cos(180/3*0.6)*sin(180*x)
  -cos(180*0.6)*sin(3*180*x)/9+cos(5*180/3*0.6)*sin(5*180*x)/25-cos(7*180/3*0.6)*sin(7*180*x)/49)
  +cos(9*180/3*0.6)*sin(9*180*x)/81-cos(11*180/3*0.6)*sin(11*180*x)/121};
%
 \addplot[red, loosely dotted, line width=1.5,domain=0:1, samples=101] {4/(pi*pi)*(cos(180/3*0.8)*sin(180*x)
  -cos(180*0.8)*sin(3*180*x)/9+cos(5*180/3*0.8)*sin(5*180*x)/25-cos(7*180/3*0.8)*sin(7*180*x)/49)
  +cos(9*180/3*0.8)*sin(9*180*x)/81-cos(11*180/3*0.8)*sin(11*180*x)/121};
%
 \addplot[green, densely dotted, line width=1.5,domain=0:1, samples=101] {4/(pi*pi)*(cos(180/3*1.0)*sin(180*x)
  -cos(180*1.0)*sin(3*180*x)/9+cos(5*180/3*1.0)*sin(5*180*x)/25-cos(7*180/3*1.0)*sin(7*180*x)/49)
  +cos(9*180/3*1.0)*sin(9*180*x)/81-cos(11*180/3*1.0)*sin(11*180*x)/121};

  \legend{$t=0.0$,$t=0.2$,$t=0.4$,$t=0.6$,$t=0.8$,$t=1.0$}
 \end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Convergence at $t=0$}
\begin{tikzpicture}[scale=0.95]
 \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black] coordinates {(0.3,0.3) (0.5,0.5) (0.7,0.3)};
%=0.3:0:7
  \addplot[blue, dotted, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)};
%
  \addplot[cyan, dashed, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9};
%
  \addplot[magenta, dashdotted, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9+cos(5*180/3*0.0)*sin(5*180*x)/25};
%
 \addplot[red, loosely dotted, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9+cos(5*180/3*0.0)*sin(5*180*x)/25-cos(7*180/3*0.0)*sin(7*180*x)/49)};
%
 \addplot[green, densely dotted, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9+cos(5*180/3*0.0)*sin(5*180*x)/25-cos(7*180/3*0.0)*sin(7*180*x)/49)
  +cos(9*180/3*0.0)*sin(9*180*x)/81};
%
\addplot[brown, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9+cos(5*180/3*0.0)*sin(5*180*x)/25-cos(7*180/3*0.0)*sin(7*180*x)/49)
  +cos(9*180/3*0.0)*sin(9*180*x)/81-cos(11*180/3*0.0)*sin(11*180*x)/121};

  \legend{Exact, 1 term, 2 terms, 3 terms, 4 terms, 5 terms, 6 terms}
 \end{axis}
\end{tikzpicture}
\begin{itemize}
\item The series solution at $t=0$ for different numbers of terms in the series solution
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Comments}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Comments}
\begin{itemize}
\item The convergence of the Fourier series may be very slow so we could have to evaluate a lot of terms of the series.
\item This can make an analytical solution of this type less useful than we might like.
\item Finding separated solutions of the wave equation requires \textcolor{red}{judicious use of known solutions} and \textcolor{blue}{careful fitting of the boundary conditions}. 
\item Extension to other coordinate systems and other PDEs is possible.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest: separated solution}
\TopHat : Which are the possible solutions for the wave equation with the following initial and boundary conditions?
\begin{align*}
u(x,0)&=x & 0\le x\le 2\pi, \\
u_t(x,0)&=0 & 0\le x\le 2\pi,\\
u(0,t)&=0&t>0,\\
u(\pi,t)&=0&t>0.
\end{align*}
\vskip-0.5cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item $u_1=\cos\lambda ct\sin\lambda x$
\item $u_2=\cos\lambda ct\cos\lambda x$
\item $u_3=\sin\lambda ct\sin\lambda x$
\item $u_4=\sin\lambda ct\cos\lambda x$
\end{enumerate}
\end{block}
\vskip-0.2cm
\iftoggle{conceptest}{% Show solution
\onslide<2>
\begin{block}{Solution: }
\begin{enumerate}
\item[1.] $u_1=\cos\lambda ct\sin\lambda x$
\end{enumerate}
\end{block}
}{% Don't show solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\section{Next lecture}
% -------------------------------------------------------- %
\begin{frame}
\frametitle[squezze]{Any questions?}
\vskip0.2cm
\begin{block}{Preparation for the next class}
\begin{itemize}
\item Topic: \textbf{Laplace transform solution and general considerations}
\item Read section 9.8 in Advanced Modern Engineering Mathematics before next lecture
\item Tell me on \TopHat
\begin{itemize}
\item What did you find confusing or difficult about what you read? 
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\item Exercises 9.3.4 in Advanced Modern Engineering Mathematics to strengthen material from this lecture
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

% -------------------------------------------------------- %
\section{Example 9.13}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}[label=example_9_13]
\frametitle{Example 9.13}
\begin{itemize}
\item Consider the partial differential equation
\[
0 = u_{xx}+2u_{xt}+2\alpha u_{tt}
\]
\item The roots of the polynomial
\[
1+2a+2a^2\alpha = 0
\] 
determine the character of the PDE 
\item If the roots are real and distinct the PDE is \textcolor{red}{hyperbolic}.
\item If the root is real and simultaneous the PDE is \textcolor{red}{parabolic}.
\item If the roots are complex the PDE is \textcolor{red}{elliptic}.
\item For $\alpha=\frac{3}{8}$ we get roots $a_1=-2$ and $a_2=-\frac{2}{3}$ and the solution
\[
u(x,t) = f(x-2t) + g\left(x-\frac{2}{3}t\right)
\]
\end{itemize}

\hyperlink{example_9_13_full}{\beamergotobutton{Full example}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}[label=example_9_13_full]
\frametitle{Example 9.13}
\textbf{Problem:} Find the characteristics of the equation
\[
0 = u_{xx}+2u_{xt}+2\alpha u_{tt}.
\]
Study the case were $\alpha=\frac38$ with the boundary conditions
\begin{align*}
\pderiv{u(x,0)}{t} &= 0, \quad \forall x\ge 0 \\
u(x,0) &= \begin{cases}
1, \quad & 0<x<1 \\
0, \quad & x \ge 0
\end{cases} \\
u(0,t) &= 0, \quad \forall t \\
\pderiv{u(0,t)}{x} &= 0, \quad \forall t
\end{align*}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.13 -- Finding the characteristics}
\begin{itemize}
\item Since all the coefficients are constant we know the characteristics will be straight lines with solutions of the form 
\[
u = u(x+at)
\]
\item Putting $z=x+at$ and writing $u^\prime=\frac{\text{d}u}{\text{d}z}$, etc. we obtain
\[
0 = u_{xx}+2u_{xt}+2\alpha u_{tt}=(1+2a+2a^2\alpha)u^{\prime\prime}
\]
\item From this we get 
\[
a = \frac{-1\pm\sqrt{1-2\alpha}}{2\alpha}.
\]
\item For there to be \textcolor{red}{two characteristics} we need two real, distinct roots, so $\alpha<\frac12$.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}[label=example_9_13_cont]
\frametitle{Example 9.13 -- continued}
\begin{itemize}
\item If $\alpha=\frac38$ there are roots $a_1=-2$ and $a_2=-\frac23$, so the solution has the form
\[
u = f(x-2t)+g\left(x-\frac{2t}3\right)
\] 
\item The first two boundary conditions,
\begin{align*}
\pderiv{u(x,0)}{t} &= 0, \quad \forall x\ge 0 \text { and}\\
u(x,0) &= F(x) = \begin{cases}
1, \quad & 0<x<1 \\
0, & x \ge 0
\end{cases} 
\end{align*}
give
\[
\left.\begin{array}{r}
0 = \pderiv{u(x,0)}{t}=-2f^\prime(x)-\frac23 g^\prime(x) \\
F(x) = u(x,0) = f(x)+g(x)
\end{array}\right\}, \quad \forall x\ge0
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.13 -- continued}
\begin{itemize}
\item Taking $f(0)=g(0)=0$ we can integrate the first equation and solve for $f(x)$ and $g(x)$ on the $t=0$ boundary giving
\[f(x)=-\frac12F(x),\, g(x)=\frac32F(x), \quad \forall x\ge0\]
\item The second two boundary conditions 
\begin{align*}
u(0,t) &= 0, \quad \forall t \\
\pderiv{u(0,t)}{x} &= 0, \quad\forall t
\end{align*}
state that on the $x=0$ line we get
\[
f(z) = g(z) = 0, \quad \forall z<0
\]
\end{itemize}
\hyperlink{example_9_13}{\beamergotobutton{Back}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.13 -- solution}
Because $f(x)$ and $g(x)$ are constant along their respective characteristics the solution in the +ve quadrant of the $x-t$ plane is
\vskip0.1cm
{\centering\includegraphics[width=10cm]{EX9-13}}

\hyperlink{example_9_13}{\beamergotobutton{Back}}
\end{frame}

% -------------------------------------------------------- %
\section{Complete example 9.14 case (i)}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}[label=example_9_14]
\frametitle{Example 9.14 case (i): displacement of a string}
\textbf{Problem:} Solve the wave equation
\[
\frac1{c^2}\pdderiv{u}{t}=\pdderiv{u}{x},
\]
for a string stretched between $x=0$ and $x=l$ subject to the boundary conditions
\begin{align*}
u(0,t) &= u(l,t)=0,  &\forall t\ge0, \\
\pderiv{u(x,0)}{t} &= 0,  &0\le x\le l,\\
u(x,0) &= \sin\frac{\pi x}{l}+\frac14\sin\frac{3\pi x}{l}, &0\le x\le l.
\end{align*}
\begin{itemize}
\item The solution is of the form
\[
u(x,t) = u_0 \cos\frac{\pi c t}{L} \sin\frac{\pi x}{L}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.14 case (i) solution}
\begin{itemize}
\item Fortunately we are dealing with the \textcolor{blue}{linear wave equation} so we can \textcolor{red}{superpose solutions}, i.e. the sum of these solutions is also a solution.   
\item So we have
\[
u(x,t) = \sum_{n=1}^\infty b_n\cos\frac{nc\pi t}{l}\sin\frac{n\pi x}{l}
\]
\item The initial condition $u(x,0)=\sin\frac{\pi x}{l}+\frac14\sin\frac{3\pi x}{l}$ requires that the following holds
\[
\sum_{n=1}^\infty b_n\sin\frac{n\pi x}{l} \overset{!}{=} \sin\frac{\pi x}{l}+\frac14\sin\frac{3\pi x}{l}
\]
\item Clearly $b_1=1,\, b_2=0,\, b_3=\frac14,\, b_4=b_5=\dots=0$
\item Giving the solution
\[
u(x,t) = cos\frac{c\pi t}{l}\sin\frac{\pi x}{l}+\frac14cos\frac{3c\pi t}{l}\sin\frac{3\pi x}{l}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.14 case (i) solution plot}
\begin{tikzpicture}[scale=0.95]
 \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black,domain=0:1, samples=101] {cos(180/3*0.0)*sin(180*x)+cos(180*0.0)*sin(3*180*x)/4};
  \addplot[blue, dotted, line width=1.5,domain=0:1, samples=101] {cos(180/3*0.4)*sin(180*x)+cos(180*0.4)*sin(3*180*x)/4};
  \addplot[cyan, dashed, line width=1.5,domain=0:1, samples=101] {cos(180/3*1.2)*sin(180*x)+cos(180*1.2)*sin(3*180*x)/4};
  \addplot[magenta, dashdotted, line width=1.5,domain=0:1, samples=101] {cos(180/3*2.0)*sin(180*x)+cos(180*2.0)*sin(3*180*x)/4};

  \legend{$t=0.0$,$t=0.4$,$t=1.2$,$t=2.0$}
 \end{axis}
\end{tikzpicture}
\vskip0.0cm
\hyperlink{separated_solution}{\beamergotobutton{Back}}
\end{frame}

\appendixend

\end{document}