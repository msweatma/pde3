\documentclass{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Introduction to hyperbolic problems and d'Alembert solution}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Recap}
% -------------------------------------------------------- %
\subsection{Spherically symmetric heat conduction equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Scaled temperature for spherically symmetric systems }
\begin{itemize}
\item The radially symmetric heat conduction/diffusion equation is
\[
\frac{1}{r^2}\frac{\partial}{\partial r}\left(r^2\pderiv{T}{r}\right) = \frac{1}{\kappa}\pderiv{T}{t}
\]
\item Scale the temperature with the radius
\[
T(r,t) = \frac{\theta(r,t)}{r}
\]
\item This gives the equation for $\theta(r,t)$ as
\[
\pdderiv{\theta}{r}=\frac{1}{\kappa}\pderiv{\theta}{t}
\]
\item We can solve this using the methods previously described but care must be taken when \textcolor{red}{$r=0$}.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Wave equation}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Wave equation}
\centerline{\includegraphics[width=0.9\columnwidth]{VibratingString03-400x220}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Derivation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Derivation of the wave equation 1/4}
Consider an element $\Delta s$ of a string with displacement $u(x,t)$.
\vskip0.1cm
\centerline{\includegraphics[width=\columnwidth]{StringElement}}
\vskip0.1cm
Applying Newton's 3rd law to a section $\Delta s$ of the string, we get
\[\underbrace{T\sin(\psi+\Delta\psi)-T\sin\psi}_{\text{net force in the $u$ direction}}
=\underbrace{\rho\Delta s}_{\text{mass}} \times
\underbrace{\pdderiv{u}{t}}_\text{acceleration}\]
\vskip0.1cm
\begin{itemize}
\item $\psi(x,t)$ is the angle between the string and the $x$ axis
\item $T(x,t)$ is the tension in the string
\item $\rho$ is the mass density
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Derivation of the wave equation 2/4}
\begin{itemize}
\item We assume that
\begin{itemize}
\item the string moves only vertically, and
\item the vibrations are only small, i.e. $|\psi(x,t)|\ll 1$
\end{itemize}
\vskip0.2cm
\item With these assumptions we get
\begin{itemize}
\item $\sin\psi\approx\psi$
\item $\cos\psi\approx1$
\item $\tan\psi = \frac{\sin\psi}{\cos\psi}\approx \psi$
\item String length $\Delta s$ is approximated by $\Delta x$
\[
\Delta s= \sqrt{(\Delta x)^2 + (\Delta u)^2} = \Delta x\sqrt{1+\left(\frac{\Delta u}{\Delta x}\right)^2}\approx \Delta x
\]
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Derivation of the wave equation 3/4}
\begin{itemize}
\item We can use the Taylor series around $\psi$
\begin{align*}
\sin(\psi+\Delta\psi) = \sin(\psi) + \Delta \psi \cos(\psi) + \mathcal{O}(\Delta \psi^2)
\end{align*}
\item By neglecting second order and higher terms we can rewrite the governing equation as
\[
T\sin\psi+T\cos\psi\Delta\psi-T\sin\psi = \rho\Delta x\pdderiv{u}{t}
\]
\item By dividing by $\Delta x$ and letting $\Delta x\to0$ we get
\[
T\cos\psi\pderiv{\psi}{x}=\rho \pdderiv{u}{t}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Derivation of the wave equation 4/4}
\begin{itemize}
\item For the small oscillations of the string, we have $\cos\psi\approx1$ and the gradient of the string is
\[
\pderiv{u}{x} = \tan\psi\approx\psi.
\]
\item So the governing equations is
\[
T\pdderiv{u}{x} = \rho\pdderiv{u}{t}
\]
\end{itemize}
\vskip0.1cm
\begin{block}{One dimensional wave equation}
\[
\frac{1}{c^2}\pdderiv{u}{t}=\pdderiv{u}{x}
\]
where $c^2=T/\rho$
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{General form}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{The wave equation}
\vskip0.1cm
\centerline{\includegraphics[width=0.8\columnwidth]{StringElement}}
\vskip-0.2cm
\[\frac{1}{c^2}\pdderiv{u}{t}=\pdderiv{u}{x}+\pdderiv{u}{y}+\pdderiv{u}{z}=\grad^2u.\]
\vskip-0.1cm
\begin{itemize}
\item It can model sound waves, shock wave, electromagnetic waves, water waves, a plucked string, etc.
\item The one dimensional form was discovered by the French scientist Jean-Baptiste le Rond d'Alembert (1717--1783)
\item The equation is said to be \textcolor{red}{hyperbolic} in character and has the nasty property that discontinuities in $u$, or its derivative, $\frac{\partial u}{\partial x}$,  will propagate in time.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Vibrating string}
% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fundamental mode of a vibrating string 1/2}
\textbf{Problem:} Show that
\[u(x,t)=u_0\sin\frac{\pi x}{L}\cos\frac{\pi c t}{L}\]
satisfies both the wave equation and the initial conditions
\[
u(x,0)=u_0\sin\frac{\pi x}{L}\text{ and }
\left.\pderiv{u}{t}\right|_{t=0}=0
\]
\begin{itemize}
\item Differentiate $u(x,t)$ with respect to $t$ to get
\[u_t(x,t)=\pderiv{u}{t}=-\frac{u_0\pi c}{L}\sin\frac{\pi x}{L}\sin\frac{\pi c t}{L}\]
\item Clearly $u_t(x,0)=0$ and $u(x,0)$ satisfy the initial conditions.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fundamental mode of a vibrating string 2/2}
\begin{itemize}
\item We compute the 2nd partial derivatives of $u(x,t)$,
\[
u_{xx}(x,t)=\pdderiv{u}{x} = -\frac{u_0\pi^2}{L^2}\sin\frac{\pi x}{L}\cos\frac{\pi c t}{L}
\]
\[
u_{tt}(x,t)=\pdderiv{u}{t} = -\frac{u_0\pi^2c^2}{L^2}\sin\frac{\pi x}{L}\cos\frac{\pi c t}{L}
\]
\item Clearly we have
\[
u_{tt} = c^2u_{xx}
\]
so the one dimensional wave equation is satisfied.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Plot of the fundamental mode of a vibrating string with $c=1,\,L=1,u_0=1$}
\begin{tikzpicture}[scale=0.93]
 \begin{axis}[legend style={draw=none},
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black,domain=0:1, samples=101] {sin(x*180)};
  \addplot[blue, dotted, line width=1.5, domain=0:1, samples=101]{sin(180*x)*cos(45)};
  \addplot[cyan, dashed, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(90)};
  \addplot[green,dashdotted, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(135)};
  \addplot[red,loosely dotted, line width=1.5,domain=0:1, samples=101]{sin(180*x)*cos(180)};
  \legend{$t=0.00$s,$t=0.25$s,$t=0.50$s,$t=0.75$s,$t=1.00$s}
 \end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\section{Linear advection}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Linear advection problem}
\textbf{Problem:} Show that
\[
u(x,t) = a\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right]
\]
satisfies the wave equation.
\vskip0.1cm
\begin{itemize}
\item Differentiate $u(x,t)$ twice with respect to $x$
{\small
\begin{align*}
u_x(x,t) &= \frac{-2a(x-ct)}{h^2}\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right] \\
u_{xx}(x,t) &= \frac{-2a}{h^2}\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right]
+\frac{4a(x-ct)^2}{h^4}\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right]
\end{align*}
}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution for linear advection, continued}
\begin{itemize}
\item Twice differentiating $u(x,t)$ with respect to $t$ gives us
{\small
\begin{align*}
u_t(x,t) &= \frac{2a(x-ct)c}{h^2}\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right] \\
u_{tt}(x,t) &= \frac{-2a c^2}{h^2}\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right]
+\frac{4a(x-ct)^2c^2}{h^4}\exp\left[-\frac{1}{h^2}\left(x - ct\right)^2\right]
\end{align*}
}
\item Comparing $u_{tt}$ and $u_{xx}$ it is clear that
\[
u_{tt} = c^2u_{xx}
\]
\item Thus, this is also a solution of the one dimensional wave equation.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Plot of the solution}
\begin{tikzpicture}[scale=0.95]
 \begin{axis}[legend style={draw=none},
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black,domain=-2:10, samples=101] {exp(-x^2)};
  \addplot[blue,dotted, line width=1.5,domain=-2:10, samples=101] {exp(-(x-2)^2)};
  \addplot[cyan,dashed, line width=1.5,domain=-2:10, samples=101] {exp(-(x-4)^2)};
  \addplot[magenta,loosely dotted, line width=1.5,domain=-2:10, samples=101] {exp(-(x-8)^2)};

  \legend{$t=0$,$t=\frac{2h}{c}$,$t=\frac{4h}{c}$,$t=\frac{8h}{c}$}
 \end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{A geometric solution using the $x-t$ diagram}
Instead of plotting the solution $u(x,t)$, we can plot the location of the start, end and peak of the curve, against time.
\vskip0.2cm
\begin{tikzpicture}
 \begin{groupplot}[group style={group size=1 by 2},
             height=0.4\textheight,width=0.95\columnwidth,
             axis lines=left,xlabel=$x$]
  \nextgroupplot[ylabel=$t$]
  \addplot[blue, domain=0:8] {x};
  \addplot[magenta,loosely dotted, line width=1.5, domain=2:10] {x-2};
  \addplot[magenta,loosely dotted, line width=1.5, domain=-2:6] {x+2};
  \addplot[black, domain=-2:10]{6};
  \addplot[magenta,loosely dotted, line width=1.5] coordinates {(4,0) (4,6)};
  \addplot[magenta,loosely dotted, line width=1.5] coordinates {(8,0) (8,6)};
  \addplot[blue] coordinates {(6,0) (6,6)};

   \nextgroupplot[ylabel=$u$, ymax=1]
  \addplot[red,domain=-2:10, samples=101, ylabel=$u$] {exp(-(x-6)^2)};
  \addplot[magenta,loosely dotted, line width=1.5] coordinates {(4,0) (4,1)};
  \addplot[magenta,loosely dotted, line width=1.5] coordinates {(8,0) (8,1)};
  \addplot[blue] coordinates {(6,0) (6,1)};
 \end{groupplot}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\section{Characteristics}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Characteristics}
\begin{itemize}
\item The linear advection problem shows that \textcolor{red}{information} propagates at the speed $c$
\item Clearly $c=\pm\sqrt{c^2}$ so we can have left or right travelling waves!
\end{itemize}
\vskip0.2cm
\centerline{\resizebox{0.8\columnwidth}{!}{
\begin{tikzpicture}
  \begin{axis}[legend style={draw=none},
     legend pos=outer north east,
     xlabel=$x/c$,ylabel=$t$]
  \addplot[magenta, dashed, line width=1.5, domain=0:5] {x};
  \addplot[cyan, line width=1.5, domain=-5:0] {-x};
  \legend{$t=0+x/c$,$t=0-x/c$}
\end{axis}
\end{tikzpicture}
}}
\end{frame}

% -------------------------------------------------------- %
\subsection{General solution}
% -------------------------------------------------------- %
\begin{frame}[label=general_solution]
\frametitle{General solution of the wave equation 1/2}
\begin{itemize}
\item This classical solution of the \textcolor{blue}{one-dimensional wave equation} is obtained by changing the coordinate system to one based on the \textcolor{red}{characteristics}, namely,
\begin{align*}
r&=x+ct \text{, }\quad s=x-ct.
\end{align*}
\item Applying the multivariable chain rule
\begin{align*}
u_{xx} &= u_{rr}+2u_{rs}+u_{ss} \\
\intertext{and}
u_{tt} &= c^2\left(u_{rr}-2u_{rs}+u_{ss}\right).
\end{align*}
\item So the wave equation becomes
\[
4c^2u_{rs} = 0 \Rightarrow u_{rs} = 0
\]
\end{itemize}
\hyperlink{general_solution_chain_rule}{\beamergotobutton{Chain rule details}}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{General solution of the wave equation 2/2}
\begin{itemize}
\item Integrating, once, with respect to $s$ gives
\[
u_r = \int u_{rs} ds = \int 0 ds = \theta(r)
\]
where $\theta(r)$ is an arbitrary function of $r$.
\item Now, integrating with respect to $r$ we obtain
\[
u = f(r)+g(s)
\]
\item Substituting for $r$ and $s$ gives the general solution of the wave equation,
\[
u = f(x+ct)+g(x-ct)
\]
where $f$ and $g$ are arbitrary functions.
\item The functions $f$ and $g$ depend only on one parameter
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Example 9.11}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.11}
\begin{block}{Solve the 1D wave equation}
\[
\frac{1}{c^2}\pdderiv{u}{t} = \pdderiv{u}{x}
\]
subject to the initial conditions
\begin{align*}
\pderiv{u(x,0)}{t} &= 0 \qquad  \forall x, \\
u(x,0) = F(x) &=
\begin{cases}
1-x & 0\le x \le 1 \\
1+x & -1\le x \le 0 \\
0 & \text{otherwise}
\end{cases}
\end{align*}
\end{block}
\begin{itemize}
\item The PDE represents, for example, an infinite string
\item Initially the string is displaced according to $F(x)$ but is at rest
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution 1/2}
\begin{itemize}
\item We know the general solution is
\[
u(x,t) = f(x+ct)+g(x-ct)
\]
\item Take the derivative with respect to $t$
\[
\pderiv{u(x,t)}{t} = c f'(x+ct) - c g'(x-ct)
\]
\item Set $t=0$ and apply the derivative initial condition
\[
0 = \pderiv{u(x,0)}{t} = cf^\prime(x)-cg^\prime(x), \quad \forall x
\]
\item Integrate to get
\[
f(x)-g(x) = K,
\]
where $K$ is an arbitrary constant.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution 2/2}
\begin{itemize}
\item Rearrange for $g(x)$ to get
\[
g(x) = f(x) - K
\]
\item Substitute $g(x)$ in the general solution to get
\[
u(x,t) = f(x+ct)+f(x-ct) - K
\]
\item Similarly, applying the Dirichlet condition gives
\[
F(x) = u(x,0) = 2f(x) - K
\]
\item Clearly, we get
\begin{align*}
u(x,t) &= \frac{1}{2} F(x+ct) + \frac{1}{2}K + \frac{1}{2} F(x-ct) + \frac{1}{2}K - K \\
&= \frac12F(x+ct) + \frac12F(x-ct)
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest: solution shape}
\TopHat : Which plot shows the correct solution behaviour?
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\includegraphics[width=0.35\columnwidth]{example_911_decreasing}
\includegraphics[width=0.35\columnwidth]{example_911_correct}\\
\includegraphics[width=0.35\columnwidth]{example_911_f}
\includegraphics[width=0.35\columnwidth]{example_911_f_plus_g}
\end{block}
\vskip-0.2cm
\iftoggle{conceptest}{% Show solution
\onslide<2>
\begin{block}{Solution: }
\includegraphics[width=0.5\columnwidth]{example_911_correct}
\begin{itemize}
\item This solution has \textbf{two} travelling waves, one propagating to the left and one to the right.
\item The waves propagate without loss of amplitude or distortion and shape discontinuities are not smoothed out.
\end{itemize}
\end{block}
\onslide<3>
\begin{block}{Solution: }
\vskip0.3cm
\centering
\includegraphics[width=0.9\columnwidth]{Ex9-11}
\end{block}
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution as an $x-t$ diagram}
\begin{tikzpicture}
  \begin{axis}[xlabel=$x$,ylabel=$t$]
  \addplot[cyan, dotted, line width=1.5, domain=0:5] {x};
  \addplot[cyan, dotted, line width=1.5, domain=-5:0] {-x};
  \addplot[blue, line width=1.5, domain=-1:4]{x+1};
  \addplot[red, dashed, line width=1.5, domain=1:6]{x-1};
  \addplot[blue, line width=1.5, domain=-6:-1]{-x-1};
  \addplot[red, dashed, line width=1.5, domain=-4:1]{-x+1};
\end{axis}
\end{tikzpicture}
\begin{itemize}
\item This plot is for $c=1$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{d'Alembert solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{d'Alembert solution 1/2}
Consider the solution of the wave equation
\[
u(x,t) = f(x+ct) + g(x-ct)
\]
\vskip-0.2cm
\begin{enumerate}
\item with an initial velocity,
\[
\pderiv{u(x,0)}{t} = G(x), \forall x \text{ and}
\]
\item and an initial displacement,
\[
u(x,0) = F(x),\quad \forall x.
\]
\end{enumerate}
\textbf{Solution:}
\begin{itemize}
\item Calculate the derivative of $u(x,t)$ and set $t=0$ so that condition 1 gives
\(c\left[f^\prime(x)-g^\prime(x)\right] = G(x)\)
\item Integrate this to get \(\left[f(x)-g(x)\right] =\int_0^x G(z)\,\text{d}z+Kc\)
\item Replace $u(x,t)$ in condition 2 to get \(f(x) + g(x) = F(x)\)
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{d'Alembert solution 2/2}
\begin{itemize}
\item This gives the following linear equation system
\begin{align*}
f(x) - g(x) &= \int_0^x G(z)\,\text{d}z+Kc \\
f(x) + g(x) &= F(x)
\end{align*}
\item This can be solved for $f(x)$ and $g(x)$ to get
\begin{align*}
f(x) &= \frac12F(x)+\frac{1}{2c}\int_0^xG(z)\,\text{d}z+\frac12K \\
g(x) &= \frac12F(x)-\frac{1}{2c}\int_0^xG(z)\,\text{d}z-\frac12K
\end{align*}
\vskip-0.8cm
\item Combining these gives the
\begin{block}{d'Alembert solution}
\[
u(x,t) = \frac12\left[F(x+ct)+F(x-ct)\right] + \frac1{2c}\int_{x-ct}^{x+ct} G(z)\,\text{d}z
\]
\end{block}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest: d'Alembert solution}
\TopHat : Pick all true statements.
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item The d'Alembert solution can be used for any $x$ interval
\item The d'Alembert solution can only be used for $-\infty<x<\infty$
\item The d'Alembert solution can be used for $0\le x<\infty$
\item The wave equation needs only initial conditions
\item The wave equation needs initial and boundary conditions
\item The wave equation needs only initial conditions in unbounded spatial domains
\end{enumerate}
\end{block}
\vskip-0.2cm
\iftoggle{conceptest}{% Show solution
\onslide<2>
\begin{block}{Solution: }
\begin{enumerate}
\item[2.] The d'Alembert solution can only be used for $-\infty<x<\infty$
\item[6.] The wave equation needs only initial conditions in unbounded spatial domains
\end{enumerate}
\end{block}
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\section{Next lecture}
% -------------------------------------------------------- %
\begin{frame}
\frametitle[squezze]{Any questions?}
\vskip0.2cm
\begin{block}{Preparation for the next class}
\begin{itemize}
\item Topic: \textbf{Characteristics and separated solution}
\item Read sections 9.3.3 and 9.8 in Advanced Modern Engineering Mathematics before next lecture
\item Tell me on \TopHat
\begin{itemize}
\item What did you find confusing or difficult about what you read?
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\item Exercises 9.3.4 in Advanced Modern Engineering Mathematics to strengthen material from this lecture
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin
% -------------------------------------------------------- %
\section{Characteristics}
% -------------------------------------------------------- %
\subsection{Chain rule}
% -------------------------------------------------------- %
\begin{frame}[label=general_solution_chain_rule]
\frametitle[squezze]{General solution chain rule}
\begin{itemize}
\item Remember the new coordinates $r=x+ct \text{, }\quad s=x-ct$
\item Applying the multivariable chain rule to the first derivatives
\begin{align*}
\pderiv{u}{x} &= \pderiv{u}{r}\pderiv{r}{x} + \pderiv{u}{s}\pderiv{s}{x} = \pderiv{u}{r} + \pderiv{u}{s} \\
\pderiv{u}{t} &= \pderiv{u}{r}\pderiv{r}{t} + \pderiv{u}{s}\pderiv{s}{t} = c\pderiv{u}{r} - c\pderiv{u}{s}
\end{align*}
\item Apply the chain rule for the second derivatives
{\small
\begin{align*}
\pdderiv{u}{x} &= \pderiv{}{x}\left(\pderiv{u}{r} + \pderiv{u}{s}\right) = \pdderiv{u}{r} \pderiv{r}{x} + \frac{\partial^2 u}{\partial r \partial s}\pderiv{s}{x} + \frac{\partial^2 u}{\partial s \partial r}\pderiv{r}{x} + \pdderiv{u}{s}\pderiv{s}{x} \\
&= \pdderiv{u}{r} + 2\frac{\partial^2 u}{\partial r \partial s} + \pdderiv{u}{s} = u_{rr}+2u_{rs}+u_{ss} \\
\pdderiv{u}{t} &= c\pderiv{}{t}\left(\pderiv{u}{r} - \pderiv{u}{s}\right)
 = c\left(\pdderiv{u}{r} \pderiv{r}{t} + \frac{\partial^2 u}{\partial r \partial s}\pderiv{s}{t} - \frac{\partial^2 u}{\partial s \partial r}\pderiv{r}{t} - \pdderiv{u}{s}\pderiv{s}{t}\right) \\
 &= c^2\left(\pdderiv{u}{r} - 2\frac{\partial^2 u}{\partial r \partial s} + \pdderiv{u}{s}\right) = c^2\left(u_{rr} - 2u_{rs} + u_{ss} \right)
\end{align*}}
\end{itemize}
\hyperlink{general_solution}{\beamergotobutton{Back}}
\end{frame}

\appendixend

\end{document}