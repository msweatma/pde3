\documentclass{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Hyperbolic equations - Laplace transform method and boundary conditions}

% -------------------------------------------------------- %
\begin{document}
%\frame{\titlepage}

% -------------------------------------------------------- %
\section{Recap}
% -------------------------------------------------------- %
\subsection{Characteristics}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Domains of dependence and influence}
\centerline{\includegraphics[width=\columnwidth]{domainofdependence}}

\begin{itemize}
\item Dependence --- the solution at $P$ is determined by the initial conditions on the boundary $AB$.
\item Influence --- the initial conditions on $AB$ influence the shaded area.

\textcolor{red}{Data outside of this region cannot be influenced}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Classification}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Formal Classification of second order PDEs}
\begin{itemize}
\item Given the general second-order PDE
\[
Au_{xx}+2Bu_{xy}+Cu_{yy}+Du_x+Eu_y+F = 0
\]
where $A,\, B,\, C,\ldots$ are constants.
\item We can make the change of variable to use characteristics $r=ax+y$ and $s=x+by$ to obtain
\[
(A+2bB+b^2C)\left[u_{ss}+\frac{AC-B^2}{(A+Bb)^2}u_{rr}\right]+\ldots = 0
\]
\item The behaviour of the PDE depends on the sign of $AC-B^2$
\begin{itemize}
\item If $AC-B^2<0$ the PDE is \textcolor{red}{hyperbolic}.
\item If $AC-B^2=0$ the PDE is \textcolor{red}{parabolic}.
\item If $AC-B^2>0$ the PDE is \textcolor{red}{elliptic}.
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Separated solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Convergence at $t=0$}
\vskip0.1cm
\begin{tikzpicture}[scale=0.85]
 \begin{axis}[legend style={draw=none},
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black] coordinates {(0.3,0.3) (0.5,0.5) (0.7,0.3)};
%=0.3:0:7
  \addplot[blue, dotted, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)};
%
  \addplot[cyan, dashed, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9};
%
  \addplot[magenta, dashdotted, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9+cos(5*180/3*0.0)*sin(5*180*x)/25};
%
 \addplot[red, loosely dotted, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9+cos(5*180/3*0.0)*sin(5*180*x)/25-cos(7*180/3*0.0)*sin(7*180*x)/49)};
%
 \addplot[green, densely dotted, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9+cos(5*180/3*0.0)*sin(5*180*x)/25-cos(7*180/3*0.0)*sin(7*180*x)/49)
  +cos(9*180/3*0.0)*sin(9*180*x)/81};
%
\addplot[brown, line width=1.5,domain=0.3:0.7, samples=101] {4/(pi*pi)*(cos(180/3*0.0)*sin(180*x)
  -cos(180*0.0)*sin(3*180*x)/9+cos(5*180/3*0.0)*sin(5*180*x)/25-cos(7*180/3*0.0)*sin(7*180*x)/49)
  +cos(9*180/3*0.0)*sin(9*180*x)/81-cos(11*180/3*0.0)*sin(11*180*x)/121};

  \legend{Exact, 1 term, 2 terms, 3 terms, 4 terms, 5 terms, 6 terms}
 \end{axis}
\end{tikzpicture}
\vskip-0.1cm
\[
u(x,t) = \frac{4l}{\pi^2}\sum_{k=1}^\infty\frac{(-1)^{k+1}}{(2k-1)^2}\cos\left(\frac{(2k-1)c\pi t}{l}\right)\sin\left(\frac{(2k-1)\pi x}{l}\right)
\]
\end{frame}

% -------------------------------------------------------- %
\subsection{Convergence}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Series convergence}
\begin{itemize}
\item For the heat conduction equation we had two solution methods
\item Separated solutions example
\begin{align*}
u(x,t) &= U(x) + v(x,t) \\
  &= x + \frac8{\pi^3}\sum_{i=1}^n\frac1{(2n-1)^3}e^{-(2n-1)^2 \kappa\pi^2t}\sin(2n-1)\pi x
\end{align*}
\item Laplace transform method example
\begin{align*}
u(x,t) =& \frac{2xt}{\pi} + \frac{0.5}{\pi\kappa} \sum_{n=1}^\infty \frac{(-1)^n}{n^3}\left(1 - e^{-4n^2\kappa t}\right) \sin\left(2n x\right) + \cos(x) e^{-\kappa t}
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Laplace transform method}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace Transforms}
\begin{itemize}
\item For linear problems with $t=0\rightarrow\infty$, for example the wave equation, Laplace transforms provide a formal method of solution.
\item Take the Laplace transform with respect to $t$ to reduce the PDE to an ODE in $x$
\item Following the procedure for solving ODEs we need the Laplace transforms of
\[
\pderiv{u}{x},\,\pderiv{u}{t},\,\pdderiv{u}{x},\text{ and }\pdderiv{u}{t}
\]
for the function $u(x,t),\ t\ge0.$
\item The main difficulty is the final inversion.
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Transform the derivatives}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Partial derivative transforms for $u(x,t),\,t\ge0$}
The Laplace transforms of the partial derivatives of the function
\[
u(x,t),\,t\ge0
\]
are given by
\begin{align*}
\mathcal{L}\left\{\pderiv{u}{t}\right\} &= sU(x,s)-u(x,0)\\
\mathcal{L}\left\{\pdderiv{u}{t}\right\} &= s^2U(x,s)-su(x,0)-u_t(x,0) \\
\mathcal{L}\left\{\pderiv{u}{x}\right\} &= \int_0^\infty e^{-st}\pderiv{u}{x}\text{ d}t =\frac{\text{d}}{\text{d}x}\int_0^\infty e^{-st}u(x,t)\text{ d}t = \frac{\text{d}}{\text{d}x}U(x,s)\\
\mathcal{L}\left\{\pdderiv{u}{x}\right\}&=\frac{\text{d}^2U(x,s)}{\text{d}x^2}
\end{align*}
where $U(x,s)$ is the Laplace transform of $u(x,t)$ with respect to $t$
\end{frame}

% -------------------------------------------------------- %
\subsection{Wave equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace transform of the wave equation}
\begin{itemize}
\item Consider the 1D linear wave equation
\[
c^2\pdderiv{u(x,t)}{x} = \pdderiv{u(x,t)}{t}
\]
\item Subject to the initial conditions
\[
u(x,0) = f(x)\text{ and } \pderiv{u(x,0)}{t} = g(x)
\]
\item Apply the Laplace transform with respect to $t$ to both sides to get
\[
c^2\frac{\text{d}^2U(x,s)}{\text{d}x^2} = s^2U(x,s)-g(x)-sf(x)
\]
\item This can be solved for the boundary conditions on $x$ and the solution inverted to find $u(x,t)$.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.16}
% -------------------------------------------------------- %
\subsection{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest: find the ODE in the Laplace domain}
\TopHat : Apply the Laplace transform  with respect to time to the 1D wave equation for a semi-infinite string, $0\le x<\infty$, with the following initial and boundary conditions
{\small
\begin{align}
u(x,0) &= 0 &\forall x\ge0 \label{eq:wave_916_IC1} \\
u'(x,0) = \pderiv{u(x,0)}{t} &= xe^{-\frac{x}{a}} & \forall x\ge0 \label{eq:wave_916_IC2}\\
u(0,t) &= 0 & \forall t\ge0 \label{eq:wave_916_BC1} \\
\lim_{x\rightarrow\infty}u(x,t) &= 0 & \forall t\ge0 \label{eq:wave_916_BC2}
\end{align}}
\vskip-0.8cm
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item \(\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s}{c^2}U(x,s) = \frac{-1}{c^2}xe^{-\frac{x}{a}}\)
\item \(\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s^2}{c^2}U(x,s) = \frac{-1}{c^2}xe^{-\frac{x}{a}}\)
\item \(\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s^2}{c^2}U(x,s) = \frac{-s}{c^2}xe^{-\frac{x}{a}}\)
\item \(\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s}{c^2}U(x,s) = \frac{-s}{c^2}xe^{-\frac{x}{a}}\)
\end{enumerate}
\end{block}
\vskip-0.2cm
\iftoggle{conceptest}{% Show solution
\onslide<2>
\begin{block}{Solution: }
\begin{itemize}
\item Taking the Laplace transform with respect to time and substituting the initial conditions~\ref{eq:wave_916_IC1} and \ref{eq:wave_916_IC2} we get
{\small
\[
c^2\frac{\text{d}^2U(x,s)}{\text{d}x^2} = s^2U(x,s) - su(x,0) - u'(x,0) =s^2U(x,s)-xe^{-\frac{x}{a}}
\]}
\item Rearrange to standard form: \(\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s^2}{c^2}U(x,s) = \frac{-1}{c^2}xe^{-\frac{x}{a}}\)
\end{itemize}
\end{block}
}{% Don't show solution
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.16 solution}
\begin{itemize}
\item The characteristic polynomial of the ODE has the zeros $\lambda_{1,2}=\pm\frac{s}{c}$
\item Thus, the complementary function is
\[
U_c(x,s)=A(s) e^{\frac{sx}{c}} + B(s) e^{\frac{-sx}{c}}
\]
where $A(s)$ and $B(s)$ are arbitrary functions of $s$
\item Next find the particular integral for the right hand side
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.16 solution continued}
\begin{itemize}
\item We seek a particular integral of the ODE of the form
\[
U_p(x,s) = \alpha xe^{-\frac{x}{a}} + \beta e^{-\frac{x}{a}}
\]
\item Calculate the second derivative
\begin{align*}
\frac{dU_p}{dx} &= \alpha e^{-\frac{x}{a}} - \frac{\alpha}{a} xe^{-\frac{x}{a}} - \frac{\beta}{a}e^{-\frac{x}{a}} \\
\frac{d^2U_p}{dx^2 } &= -\frac{\alpha}{a} e^{-\frac{x}{a}} - \frac{\alpha}{a} e^{-\frac{x}{a}} + \frac{\alpha}{a^2} xe^{-\frac{x}{a}} + \frac{\beta}{a^2}e^{-\frac{x}{a}}
\end{align*}
\item Insert this into the differential equation in the Laplace domain
\begin{align*}
e^{-\frac{x}{a}}\left[\frac{\beta}{a^2} - \frac{2\alpha}{a}\right] + \frac{\alpha}{a^2} x e^{-\frac{x}{a}} - \frac{s^2}{c^2} \left[\alpha xe^{-\frac{x}{a}} + \beta e^{-\frac{x}{a}}\right] &= \frac{-1}{c^2}x e^{-\frac{x}{a}}
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.16 solution continued}
\begin{itemize}
\item Collect the terms
\begin{align*}
e^{-\frac{x}{a}}\left[\frac{\beta}{a^2} - \frac{2\alpha}{a} - \frac{s^2}{c^2}\beta\right] + x e^{-\frac{x}{a}}\left[\frac{\alpha}{a^2} - \frac{\alpha s^2}{c^2} + \frac{1}{c^2}\right] &= 0
\end{align*}
\item Solve for $\alpha$
\[
\frac{\alpha}{a^2} - \frac{\alpha s^2}{c^2} = -\frac{1}{c^2} \implies \alpha = \frac{1}{s^2 - \frac{c^2}{a^2}}
\]
\item Use $\alpha$ to solve for $\beta$
\begin{align*}
\beta \left(\frac{1}{a^2} - \frac{s^2}{c^2}\right) - \frac{2\alpha}{a} &= 0 \\
\beta \frac{c^2 - s^2 a^2}{a^2 c^2} &= \frac{2}{a}\frac{1}{s^2 - \frac{c^2}{a^2}} \\
\beta &= \frac{2}{a}\frac{1}{s^2 - \frac{c^2}{a^2}} \frac{a^2 c^2}{c^2 - s^2 a^2} = -\frac{\frac{2 c^2}{a}}{\left(s^2 - \frac{c^2}{a^2}\right)^2}
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.16 solution continued}
\begin{itemize}
\item With the parameters $\alpha$ and $\beta$ we obtain the general solution
\[
U(x,s)=A(s) e^{\frac{sx}{c}} + B(s) e^{\frac{-sx}{c}} + \frac{e^{-\frac{x}{a}}}{s^2 - \frac{c^2}{a^2}}
\left[x - \frac{2\frac{c^2}{a}}{s^2 - \frac{c^2}{a^2}}\right]
\]
where $A(s)$ and $B(s)$ are arbitrary functions which we need to find from the boundary conditions.
\item We can find $A(s)$ and $B(s)$ by using the transformed boundary conditions~\ref{eq:wave_916_BC1} and \ref{eq:wave_916_BC2}
\begin{align*}
u(0,t)=0&\implies U(0,s)=0 \\
\lim_{x\rightarrow\infty}u(x,t)=0&\implies\lim_{x\rightarrow\infty}U(x,s)=0
\end{align*}
\item Clearly $\lim_{x\rightarrow\infty}U(x,s) = 0 \implies A(s) =0$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.16 solution continued}
\begin{itemize}
\item Considering the boundary condition at $x=0$, we get
\[
U(0,s) = B(s)e^{\frac{-s0}{c}} + \frac{e^{-\frac{0}{a}}}{s^2 - \frac{c^2}{a^2}}
\left[0 - \frac{2\frac{c^2}{a}}{s^2 - \frac{c^2}{a^2}}\right] \overset{!}{=} 0
\]
\item Solving this equation for $B(s)$ it follows that
\begin{align*}
0  &= B(s) - \frac{2\frac{c^2}{a}}{\left(s^2 - \frac{c^2}{a^2}\right)^2} \\
\implies B(s) &= \frac{2\frac{c^2}{a^2}}{\left(s^2 - \frac{c^2}{a}\right)^2}
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.16 solution continued}
\begin{itemize}
\item The complete solution in the Laplace domain is
\[
U(x,s) = \frac{2\frac{c^2}{a}}{\left(s^2 - \frac{c^2}{a^2}\right)^2}e^{-\frac{sx}{c}} + \frac{e^{-\frac{x}{a}}}{s^2 - \frac{c^2}{a^2}}\left[x - \frac{2\frac{c^2}{a}}{s^2 - \frac{c^2}{a^2}}\right]
\]
\item Fortunately this can be inverted (using the standard tables)
\item We use the time shift theorem
\[
\mathcal{L}\left\{f(t-\alpha) H(t-\alpha)\right\} = \exp(-\alpha s)F(s)
\]
\item and the functions transforms
\begin{align*}
\mathcal{L}\left\{\sinh\omega t\right\}&=\frac{\omega}{s^2-\omega^2} \\
\mathcal{L}\left\{\cosh\omega t\right\}&=\frac{s}{s^2-\omega^2} \\
\mathcal{L}\left\{\frac{\omega t\cosh\omega t-\sinh\omega t}{2\omega^3}\right\}&=\frac{1}{(s^2-\omega^2)^2}
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.16 solution continue}
\begin{block}{To obtain the solution}
\begin{align*}
u(x,t) &= \frac{a}{c}\left[(ct-x)\cosh\left(\frac{ct-x}{a}\right)H(ct-x)-cte^{-\frac{x}{a}}\cosh\left(\frac{ct}{a}\right) H(t)\right] \\
 &+ \frac{a}{c}\left[(e^{-\frac{x}{a}}(x+a)\sinh\left(\frac{ct}{a}\right)H(t) - a\sinh\left(\frac{ct-x}{a}\right)H(ct-x)\right]
\end{align*}
where $H(t)$ is the Heaviside step function.
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.16 solution, $a=1$ and $c=1$}
\begin{tikzpicture}
\begin{axis}[legend style={draw=none},
legend pos=outer north east,
xlabel=$x$,ylabel=$u$]
% t=0.0
\addplot[black,domain=0:10, samples=201] {0.0};
% t=0.5
\addplot[cyan, dotted, line width=1.5,domain=0:10, samples=201] {(0.5-x)*cosh(0.5-x)*(0.5+0.5*tanh(0.5-x*10))-0.5*exp(-x)*cosh(0.5)
  +exp(-x)*(x+1)*sinh(0.5)-sinh(0.5-x)*(0.5+0.5*tanh(0.5-x*10))};
% t=1.0
\addplot[blue, dashed, line width=1.5,domain=0:10, samples=201] {(1.0-x)*cosh(1.0-x)*(0.5+0.5*tanh(1.0-x*10))-1.0*exp(-x)*cosh(1.0)
  +exp(-x)*(x+1)*sinh(1.0)-sinh(1.0-x)*(0.5+0.5*tanh(1.0-x*10))};
% t=2.0
\addplot[red, dashdotted, line width=1.5,domain=0:10, samples=201] {(2.0-x)*cosh(2.0-x)*(0.5+0.5*tanh(2.0-x*10))-2.0*exp(-x)*cosh(2.0)
  +exp(-x)*(x+1)*sinh(2.0)-sinh(2.0-x)*(0.5+0.5*tanh(2.0-x*10))};

\legend{$t=0.0$,$t=0.5$,$t=1.0$,$t=2.0$}
\end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\section{Comments on Laplace transform method}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Concluding remarks}
\begin{itemize}
\item Laplace transforms can be a useful method for finding solutions to PDEs.
\item The inverse transforms, if it exist, can often be very complicated so the use of a computer algebra system is recommended.
\item Once this is done finding a simplified form of the solution needs considerable expertise.
\item Extension to other coordinate systems and other PDEs is possible.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Boundary conditions}
% -------------------------------------------------------- %
\subsection{Different types}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Boundary conditions}
If $\Omega$ is the domain on which the given equation is to be solved and $\partial\Omega$ denotes its boundary, we can have
\begin{itemize}
\item Cauchy
\[
\text{both }u\text{ and }\pderiv{u}{n}\text{ are specified on }\partial\Omega.
\]
\item Dirichlet
\[
u\text{ is specified on } \partial\Omega.
\]
\item Neumann
\[
\pderiv{u}{n}\text{ is specified on }\partial\Omega.
\]
\item Robin
\[
au+B\pderiv{u}{n}=g\text{ on }\partial\Omega.
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Mixed boundary conditions}
Two or more BC of different types are specified on $\partial\Omega$
\centerline{\includegraphics[height=0.8\textheight]{MixedBC}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Relation to reality}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Boundary conditions and physics}
If we consider the wave equation being used to model a vibrating string,
\begin{itemize}
\item the ends of the string are likely to be fixed in place -- \textcolor{blue}{Dirichlet} conditions
\item it seems natural for the initial displacement and the velocity of the string  to be specified -- \textcolor{blue}{Cauchy condition.}
\end{itemize}
There are alternatives....
\begin{itemize}
\item we could also fix the gradient at the ends of the string using \textcolor{blue}{Neumann} conditions, or
\item the initial position of the string could be known (from a photograph) and its position at a later time (from a second exposure) giving \textcolor{blue}{Dirichlet} conditions.
\end{itemize}
The important question is can a solution be found?
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Uniqueness}
The vibrating string problem (Example 9.1) has the solution
\[
u = u_0\sin\frac{\pi x}{L}\cos\frac{\pi ct}{L}.
\]
If we know its position at $t=L/2c$ and $t=3L/2c$ can we find a solution?
\[
\text{At }t = \frac{L}{2c},\  \cos\frac{\pi ct}{L} = \cos\frac{\pi}{2} = 0\implies u(x)=0\ \forall x
\]
\[
\text{At }t = \frac{3L}{2c},\  \cos\frac{\pi ct}{L}=\cos\frac{3\pi}{2}=0\implies u(x)=0\ \forall x
\]
So in both cases the string is horizontal.  One solution is that $u_0=0$ and the string is stationary, but many other solutions are possible ---  Here the boundary conditions \textcolor{red}{do not provide a unique solution}
and so cannot be used.
\end{frame}

% -------------------------------------------------------- %
\subsection{Wave equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Wave equation boundary conditions}
\centerline{\includegraphics[width=\columnwidth]{WaveBC}}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Appropriateness of Boundary Conditions}
\centerline{\includegraphics[width=0.95\columnwidth]{BCMatrix.pdf}}
\end{frame}

% -------------------------------------------------------- %
\section{Fun with PDEs}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fun with PDEs}
\centerline{\includegraphics[width=0.95\columnwidth]{FEM.pdf}}
Stress and strain analysis  in Finite Element Methods for Solids and Structures 4 (CIVE10022) and The Finite Element Method 5 (CIVE11029).
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fun with PDEs}
\centerline{\includegraphics[width=\columnwidth]{DeLaval.pdf}}
Solution of the Navier-Stokes equations with commercial software in Computational Fluid Dynamics 5 (MECE11004).
\end{frame}

% -------------------------------------------------------- %
\section{Exam}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Exam}
\begin{itemize}
\item 2 hour closed book exam on the 17th of December
\item Two questions each for the analytical and numerical part of the course
\item You need to solve 3 of these 4 questions
\item The questions for the analytical part will be similar to those from previous years and to questions from
\begin{itemize}
\item Engineering Mathematics 2A and 2B (for the PDE material)
\item Mathematics for Science and Engineering 2A and 2B
\item Mathematics for Electrical and Mechanical Engineers
\item Mathematics for Chemical Engineers
\end{itemize}
\item The numerical part could among others contain
\begin{itemize}
\item Derivation of discretised equations
\item Description and discussion on how you would implement them
\item Analysis and discussion of simulation results, e.g. of plots and grid refinement
\item Example questions to be uploaded to Learn soon
\end{itemize}
\item Good luck!
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}