\documentclass{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Introduction to parabolic problems}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Heat equation}
% -------------------------------------------------------- %
\subsection{Introduction}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Heat conduction or diffusion equation}
\centerline{\includegraphics[width=0.6\columnwidth]{heated_rod.jpg}}
\begin{itemize}
\item Calculate heat loss through a building envelope
\item Analyse experiments to find the thermal conductivity of a new insulating material
\item Evaluate the temperature profile in an internal combustion engine
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Example}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example solution of the heat conduction equation}
\begin{block}{Show that}
\[
T(x,t) = \frac1{\sqrt{t}}\exp\left(-\frac{x^2}{4\kappa t}\right)
\]
is a solution of the one dimensional heat condition equation.
\end{block}
{\small
By calculating the partial derivatives
\begin{align*}
T_t(x,t) &= \frac{-1}{2t^{3/2}}\exp\left(\frac{-x^2}{4\kappa t}\right)
+\frac{1}{\sqrt{t}}\frac{x^2}{4\kappa t^2}\exp\left(\frac{-x^2}{4\kappa t}\right) \\
T_x(x,t) &= \frac{1}{\sqrt{t}}\frac{-2x}{4\kappa t}\exp\left(\frac{-x^2}{4\kappa t}\right) \\
T_{xx}(x,t) &= \frac{-1}{2\kappa t^{3/2}}\exp\left(\frac{-x^2}{4\kappa t}\right)
+ \frac{1}{\sqrt{t}} \frac{x^2}{4\kappa^2 t^2}\exp\left(\frac{-x^2}{4\kappa t}\right)
\end{align*}}
We can now easily show that $T_t=\kappa T_{xx}$
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Diffusion -- Reduced temperature at $t=\frac{L^2}{4\kappa}$ }
\begin{tikzpicture}
  \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$\frac{T}{\sqrt{4\kappa}}$]
  \addplot[black,domain=-2:2, samples=101] {1/sqrt(0.1^2)*exp(-x^2/0.1^2)};
  \addplot[blue, dotted, line width=1.5,domain=-2:2, samples=101] {1/sqrt(0.2^2)*exp(-x^2/0.2^2)};
  \addplot[cyan, dashed, line width=1.5,domain=-2:2, samples=101] {1/sqrt(0.3^2)*exp(-x^2/0.3^2)};
  \addplot[magenta, dashdotted, line width=1.5,domain=-2:2, samples=101] {1/sqrt(0.5^2)*exp(-x^2/0.5^2)};
  \addplot[red, loosely dotted, line width=1.5,domain=-2:2, samples=101] {1/sqrt(1^2)*exp(-x^2/1^2)};

  \legend{$L=0.1$,$L=0.2$,$L=0.3$,$L=0.5$,$L=1.0$}
\end{axis}
   \end{tikzpicture} 
\end{frame}

% -------------------------------------------------------- %
\subsection{Derivation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Problem statement}
\centering
\includegraphics[width=0.8\columnwidth]{HeatFlowElement.pdf}
\medskip
\begin{itemize}
\item Consider the heat balance in the element
\item $Q(x,t)$ comes in and $Q(x+\Delta x,t)$ leaves
\item The net increase at time $t$ is given by balancing the energy flows
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Energy balance}
\begin{minipage}[c]{0.3\columnwidth}
\includegraphics[height=0.9\columnwidth,angle=90]{HeatFlowElement.pdf}
\end{minipage}%
\begin{minipage}[c]{0.7\columnwidth}
The net increase per unit time is
\[
A c\rho\pderiv{T}{t}\Delta x = A \left( Q(x,t)-Q(x+\Delta x,t) \right)
\]
Here
\begin{itemize}
\item $c$ is the specific heat capacity in J$\,$kg$^{-1}\,$K$^{-1}$
\item $\rho$ is the density in kg$\,$m$^{-3}$
\item $\Delta x$ is the length of the unit in m
\item $A$ is the crosssection in m$^2$
\end{itemize}
Check the units
\begin{align*}
\text{m}^2\frac{\text{J}}{\text{kg}\,\text{K}} \frac{\text{kg}}{\text{m}^3}\frac{\text{K}}{\text{s}}\text{m} &= \text{m}^2  \left[Q\right] \\
\frac{\text{J}}{\text{m}^2\,\text{s}} = \frac{\text{W}}{\text{m}^2} &=  \left[Q\right]
\end{align*}
\end{minipage}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Apply Fourier's law}
\begin{minipage}[c]{0.3\columnwidth}
\includegraphics[height=0.9\columnwidth,angle=90]{HeatFlowElement.pdf}
\end{minipage}%
\begin{minipage}[c]{0.7\columnwidth}
Divide by $A$ and $\Delta x$, and let $\lim \Delta x\rightarrow 0$,
\[
c\rho\pderiv{T}{t} =-\pderiv{Q}{x}.
\]
Applying Fourier's law 
\[
Q=-k\pderiv{T}{x}
\]
with the thermal conductivity $k$ in W$\,$m$^{-1}\,$K$^{-1}$, we obtain
\[
\pderiv{T}{t}=\kappa\pdderiv{T}{x}
\]
where $\kappa=k/c\rho$ is the \textbf{thermal diffusivity} with units m$^{2}\,$s$^{-1}$.
\end{minipage}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Nondimensionalisation}
\vskip0.cm
We pick characteristic length and time to nondimensionalise:
\begin{itemize}
\item Characteristic length: pick the length of the rod $L$: \[x = L \chi\]
\item Characteristic time: pick the rod length squared divided by the thermal diffusivity
\[
t = \frac{L^2}{\kappa}\tau
\]
\item From this it follows
\begin{align*}
\frac{\kappa}{L^2}\pderiv{T}{\tau} = \pderiv{T}{t} &= \kappa\pdderiv{T}{x} = \frac{\kappa}{L^2}\pdderiv{T}{\chi} \\
\pderiv{T}{\tau} &= \pdderiv{T}{\chi}
\end{align*}
\item This nondimensionalisation removed the only parameter
\end{itemize}

\end{frame}

% -------------------------------------------------------- %
\subsection{General form}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Heat conduction or diffusion equation}
The heat conduction equation is
\[
\frac1\kappa\pderiv{T}{t} = \grad^2T = \pdderiv{T}{x_1} + \cdots + \pdderiv{T}{x_n}
\]
for $n$ space dimensions.
\medskip
\begin{itemize}
\item This equation is \textcolor{red}{parabolic} in nature and given enough time (and constant boundary conditions) the solution will converge to a steady state.
\item If there are no time-varying inputs this has the steady state solution, $U$, satisfying the Laplace equation
\[ 
\nabla^2U = 0
\]
and the boundary conditions.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Thermal properties}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Thermal conductivity}
\begin{itemize}
\item Vacuum insulation panels $k\approx0.007$ in W$\,$m$^{-1}\,$K$^{-1}$
\item Gases: $k\approx0.005 - 0.2$ in W$\,$m$^{-1}\,$K$^{-1}$
\begin{itemize}
\item CO$_2$ $0.0146\,$W$\,$m$^{-1}\,$K$^{-1}$
\item Hydrogen  $0.168\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\item Insulators: $k\approx0.02 - 0.7$ in W$\,$m$^{-1}\,$K$^{-1}$
\begin{itemize}
\item Styrofoam $0.033\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\item Liquids: $k\approx0.2 - 8$ in W$\,$m$^{-1}\,$K$^{-1}$
\begin{itemize}
\item Water $0.58\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\item Non-metallic solids: $k\approx0.4 - 60$ in W$\,$m$^{-1}\,$K$^{-1}$
\begin{itemize}
\item Ice $2.18\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\item Alloys: $k\approx15 - 150$ in W$\,$m$^{-1}\,$K$^{-1}$
\begin{itemize}
\item Chrome nickel steel $16.3\,$W$\,$m$^{-1}\,$K$^{-1}$
\item Aluminium bronze  $121\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\item Metals: $k\approx50 - 429$ in W$\,$m$^{-1}\,$K$^{-1}$
\begin{itemize}
\item Silver $429\,$W$\,$m$^{-1}\,$K$^{-1}$
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Diffusion equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Diffusion equation}
\centerline{\includegraphics[width=0.75\columnwidth]{Blausen_0315_Diffusion.png}}
\begin{itemize}
\item A similar argument using \textbf{Fick's law} for diffusion instead of \textbf{Fourier's law} leads to an identical results with a different definition of $\kappa$
\item Here $\kappa$ is the mass diffusivity again with units m$^{2}\,$s$^{-1}$
\item Application examples
\begin{itemize}
\item Diffusion of dopants in microfabrication
\item Mixing of two fluids
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest: heat equation solution}
\TopHat : Consider a perfectly insulated rod with the conditions
\begin{itemize}
\item Both ends are held at fixed temperatures: $T(0,t) = 1$ and $T(L,t) = -1$
\item Initially the wire is at a given temperature $T(x,0) = \cos(\pi x/L)$ for $0\le x\le L$
\end{itemize}
What is the solution of this problem for $t\to\infty$?
\begin{overprint}
\onslide<1>
\vskip-0.4cm
\begin{block}{ConcepTest: possible solutions}
\centerline{\includegraphics[width=6cm]{heat_steady_state}}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\begin{enumerate}
\item Solution 3 is correct: at $t\to\infty$ the initial disturbance will have dispersed and the solution will be a linear temperature profile between the two boundary values
\end{enumerate}
\vskip-0.2cm
\centerline{\includegraphics[width=4.2cm]{heat_steady_state}}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\subsection{General solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{General solution}
\begin{itemize}
\item If there are no time varying inputs, i.e. boundary conditions or source terms, the solution will approach a steady state
\item In this case, we can write the general solution as
\[
u = U + v
\]
where $v$ also satisfies both the PDE and the boundary conditions on $u-U$.
\item Essentially 
\begin{itemize}
\item $U$ is the steady state solution and,
\item $v$ is the transient (time-varying) part
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Transient solution}
\begin{itemize}
\item Since we know that
\[
\lim_{t\rightarrow\infty} u = U
\]
it follows
\[
\lim_{t\rightarrow\infty}v=0
\]
\item The boundary conditions of the transient problem are given by
\begin{align*}
v(x_0, t) &= u(x_0, t) - U(x_0), &\forall t>0 \\
\left.\pderiv{v}{x}\right|_{x=x_0} &= \left.\pderiv{u}{x}\right|_{x=x_0} - \left.\pderiv{U}{x}\right|_{x=x_0}, &\forall t>0 \\
\end{align*}
\item For example, for constant Dirichlet boundary conditions we get
\[
v(x_0, t) = 0
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Separated solutions}
% -------------------------------------------------------- %
\subsection{Idea}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Separation of Variables}
Solve the PDE by seeking solutions of the form
\[
T(x,t) = r(t)w(x)
\]
where $r(t)$ and $w(x)$ are single functions of single variables.
\vskip0.1cm
Substituting this into the heat equation
\[
\frac{1}{\kappa}\pderiv{T}{t} = \pdderiv{T}{x}
\]
gives
\[
\frac{1}{\kappa} \frac{dr(t)}{dt} w(x) = r(t) \frac{\text{d}^2w}{\text{d}x^2}
\]
or
\[
\frac{1}{\kappa}\frac1r\frac{\text{d}r}{\text{d}t} = \frac1w\frac{\text{d}^2w}{\text{d}x^2} = \mu
\]
\medskip
Here $\mu$ is independent of both $x$ and $t$.
\end{frame}

% -------------------------------------------------------- %
\subsection{Derivation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Two simple ODEs}
Since the two equations are independent, we can split them into two ODEs 
\[
\frac{\text{d}r}{\text{d}t} = \mu\kappa r \text{ and }\frac{\text{d}^2w}{\text{d}x^2} = \mu w
\]
The type of the solution depends on the sign of $\mu$, there are a variety of possibilities. Let us first look at the solutions for $w(x)$:
\begin{align*}
w(x) =& A \cos(\lambda x) + B \sin(\lambda x) & \mu = -\lambda^2 < 0,\\
w(x) =& A e^{\lambda x} + B e^{-\lambda x} & \mu = \lambda^2>0,\\
w(x) =& (Ax+B) & \mu=0
\end{align*}
For $r(t)$ we get the solution
\[
r(t) = C e^{\mu\kappa t}
\]
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Combined solution}
\begin{itemize}
\item We get the combined solution
\[
v(x, t) = r(t) w(x) = C e^{\mu\kappa t} w(x)
\]
\item This fits with the physics which suggests a solution which decays in time
\item To achieve this we need $\mu\kappa<0$ from which it follows that $\mu < 0$
\item This is the case because 
\[
\kappa = k/c\rho > 0
\]
\item Thus we get
\[
v(x,t) = e^{-\lambda^2\kappa t} \left(A\sin\lambda x + B\cos\lambda x\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{General form}
% -------------------------------------------------------- %
\begin{frame}[label=general_form]
\frametitle{General form of the separated solution}
\begin{itemize}
\item Setting $\alpha = \lambda^2\kappa$ we get
\[
v(x,t) = e^{-\alpha t} \left(A\sin\lambda x+B\cos\lambda x\right)
\]
\item This is a solution of the heat conduction equation for all values of $\lambda$, $A$ and $B$
\item Superimposed solution, as previously we can build up a solution to satisfy all the boundary conditions
\textcolor{red}{
\[
v(x, t) =\sum_{n=1}^\infty e^{-\alpha_n t} \left(A_n\sin\lambda_n x+B_n\cos\lambda_n x\right)
\]}
\end{itemize}
\hyperlink{example_920}{\beamergotobutton{Example 9.20}} \\
\hyperlink{example_921}{\beamergotobutton{Example 9.21}}
\end{frame}

% -------------------------------------------------------- %
\section{Comments}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Concluding remarks}
\begin{itemize}
\item Separated solutions can provide a useful approach to solving the one dimensional heat conduction or diffusion equation.
\item As before finding separated solutions requires \textcolor{red}{judicious use of known solutions} and \textcolor{blue}{careful fitting of the boundary conditions}.  
\item The starting point must always be to find the \textcolor{red}{steady state solution} and to subtract this from the dependent variable.
\item Many of the Fourier series obtained can be slow to converge and a large number of terms may need to be evaluated.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Next lecture}
% -------------------------------------------------------- %
\begin{frame}
\frametitle[squezze]{Any questions?}
\vskip0.2cm
\begin{block}{Preparation for the next class}
\begin{itemize}
\item Topic: \textbf{Laplace transform method and more examples}
\item Read section 9.4 in Advanced Modern Engineering Mathematics before next lecture
\item Tell me on \TopHat
\begin{itemize}
\item What did you find confusing or difficult about what you read? 
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\item Exercises 9.4.3 in Advanced Modern Engineering Mathematics to strengthen material from this lecture
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

% -------------------------------------------------------- %
\section{Example 9.20}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}[label=example_920]
\frametitle{Example 9.20}
Solve the one dimensional heat conduction equation
\[
\pderiv{T}{t} = \kappa\pdderiv{T}{x}
\]
subject to the boundary conditions
\begin{align*}
T(0,t)&=0&\forall t>0\\
\left.\pderiv{T}{x}\right|_{x=l}&=0 &\forall t>0\\
T(x,0)&=T_0\sin\frac{3\pi x}{2l} & 0\le x \le l
\end{align*}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.20}
\begin{itemize}
\item  The boundary conditions are constant and there are no source terms so the system approaches a steady state solution. 
\item The left boundary is set at $T=0$ so that the steady state solution is 
\[
\lim_{t\rightarrow\infty} T(x,t)=0,\   0\le x \le l
\]
\item So we consider solutions of the form
\[
T = e^{-\alpha t} \left(A\sin\lambda x+B\cos\lambda x\right).
\]
\item The first boundary condition 
\[
T(0, t) = e^{-\alpha t} \left(A\sin(\lambda 0) + B\cos(\lambda 0)\right) \overset{!}{=} 0
\]
which implies $B=0$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.20 continued}
\begin{itemize}
\item For the second boundary condition we need
\[
\left.\pderiv{T}{x}\right|_{x=l} = Ae^{-\alpha t}\lambda\cos\lambda l \overset{!}{=} 0
\]
So
\[
\cos\lambda l=0
\]
or 
\[
\text{ or }\lambda l=\left(n+\frac12\right)\pi,\ n=0,1,2,3,\ldots
\]
\item Leading to the solution
\[
T = Ae^{-\alpha t}\sin\left[\left(n+\frac12\right)\pi\frac{x}{l}\right]
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.20 continued}
\begin{itemize}
\item Applying the initial condition
\[
T(x, t=0) = Ae^{0}\sin\left[\left(n+\frac12\right)\pi\frac{x}{l}\right] \overset{!}{=} T_0\sin\frac{3\pi x}{2l}
\]
gives $n=1$ and $A=T_0$
\item So the solution is
\[
T(x,t) = T_0\exp\left(-\frac{9\kappa\pi^2 t}{4l^2}\right)\sin\frac{3\pi x}{2l}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.20 solution, $\kappa=1$ and $l=1$}
\begin{tikzpicture}
  \begin{axis}[legend style={draw=none}, 
  xlabel=$x/l$,ylabel=$T/T_0$]
% t=0.0
 \addplot[black,line width=1.5, domain=0:1, samples=201] {exp(-9*3.14159^2*0.0/4)*sin(540*x/2)};
% t=0.01
  \addplot[cyan,dashed, domain=0:1, samples=201] {exp(-9*3.14159^2*0.01/4)*sin(540*x/2)};
% t=0.05
  \addplot[blue,dotted, line width=1.5, domain=0:1, samples=201] {exp(-9*3.14159^2*0.05/4)*sin(540*x/2)};
% t=0.1
  \addplot[red,dashed, line width=1.5, domain=0:1, samples=201] {exp(-9*3.14159^2*0.1/4)*sin(540*x/2)};

  \legend{$t=0.00$,$t=0.01$,$t=0.05$,$t=0.10$}
\end{axis}
\end{tikzpicture}
\vskip0.1cm
\hyperlink{general_form}{\beamergotobutton{Back to general solution form}}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.21}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}[label=example_921]
\frametitle{Example 9.21}
Solve the one dimensional heat condition equation
\[
\pderiv{T}{t} = \kappa\pdderiv{T}{x}
\]
subject to the boundary conditions
\begin{align*}
T(0,t)&=0&\forall t\ge0\\
T(l,t)&=0&\forall t\ge0\\
T(x,0)&=T_0\left(\frac12-\frac{x}{l}\right)&0\le x \le l
\end{align*}
\begin{itemize}
\item There is a discontinuity in the initial and boundary conditions.
\item The boundary conditions are constant and there are no source terms so the system approaches a steady state solution. 
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.21}
\begin{itemize}
\item As before we are seeking solutions of the form
\[
T(x,t) = e^{-\alpha t} \left(A\sin\lambda x+B\cos\lambda x\right),
\]
\item The left boundary condition is
\[
T(0,t) = e^{-\alpha t} \left(A\sin(\lambda 0) + B\cos(\lambda 0)\right) \overset{!}{=} 0
\]
which is only fulfilled for $B=0$
\item The second condition is
\[
T(l,t) =  e^{-\alpha t} A\sin(\lambda l) \overset{!}{=} 0
\]
which is only fulfilled for
\begin{align*}
\sin\lambda l=0,\text{ or }\lambda l&=n\pi,\ n=1,2,3,\ldots \\
\lambda &= \frac{n\pi}{l}
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.21 continued}
\begin{itemize}
\item Recalling that $\lambda^2=\alpha/\kappa$ we are seeking solutions of the form
\[
T(x,t) = A\exp\left(-\frac{\kappa n^2\pi^2 t}{l^2}\right)\sin\frac{n\pi x}{l},\ n=1,2,3,\ldots
\]
\item This time we cannot satisfy the initial conditions from a single solution, so we need to use superposition
\[
T(x,t) = \sum_{n=1}^\infty A_n\exp\left(-\frac{\kappa n^2\pi^2 t}{l^2}\right)\sin\frac{n\pi x}{l}
\]
\item Applying the initial condition we get
\[
T(x, t=0) = \sum_{n=1}^\infty A_n\sin\frac{n\pi x}{l} \overset{!}{=} T_0\left(\frac12-\frac{x}{l}\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.21 continued}
\begin{itemize}
\item Using the half-range Fourier sine series we find the coefficients $A_n$
\[
A_n = \frac{2T_0}{l}\int_0^l \left(\frac12-\frac{x}{l}\right)\sin\frac{n\pi x}{l}\text{ d}x=
\begin{cases}
0 & \text{odd }n \\
\frac{2T_0}{n\pi} & \text{even }n
\end{cases}
\]
\item The following integrals can be useful in calculating Fourier series coefficients
\begin{align*}
\int x\sin(ax)dx &= \frac{1}{a^2}\left(\sin(ax) - ax \cos(ax)\right) \\
\int x\cos(ax)dx &= \frac{1}{a^2}\left(\cos(ax) + ax \sin(ax)\right) 
\end{align*}
\item Similar relations exist for $x^2$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.21 continued}
\begin{itemize}
\item Substituting the $A_n$ back into the solution and using $2m=n$ we get
\textcolor{red}{
\[
T(x,t) = \frac{T_0}{\pi} \sum_{m=1}^\infty \frac1{m}\exp\left(-m^2\frac{4\kappa \pi^2}{l^2} t\right)\sin\left(\frac{2m\pi x}{l}\right)
\]}
\item We can plot the solution at the scaled time, 
\[
\tau = \frac{4\kappa\pi^2}{l^2} t,
\] 
using the dimensionless temperature $T/T_0$ and the dimensionless length $x/l$.  
\item We are only interested in the solution over the interval, $0\le x \le l$ and although $T$ can be computed for values of $x$ outside this range it has no physical meaning.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.21 solutions at $\tau = \frac{4\kappa\pi^2}{l^2}t$}
\begin{tikzpicture}
  \begin{axis}[legend style={draw=none}, 
  xlabel=$x/l$,ylabel=$T/T_0$]
% t=0.0
 \addplot[black,domain=0:1, samples=3] {0.5-x};
% t=0.5
  \addplot[cyan,dashed, domain=0:1, samples=201] 
  {(exp(-0.5)*sin(360*x)+exp(-4*0.5)*sin(2*360*x)/2+exp(-9*0.5)*sin(3*360*x)/3+exp(-16*0.5)*sin(4*360*x)/4)
  /3.141592};
% t=1.0
   \addplot[blue,dotted, line width=1.5,domain=0:1, samples=201] 
  {(exp(-1.0)*sin(360*x)+exp(-4*1.0)*sin(2*360*x)/2+exp(-9*1.0)*sin(3*360*x)/3+exp(-16*1.0)*sin(4*360*x)/4)
  /3.141592};
% t=1.5
  \addplot[red,dashed, line width=1.5,domain=0:1, samples=201] 
  {(exp(-1.5)*sin(360*x)+exp(-4*1.5)*sin(2*360*x)/2+exp(-9*1.5)*sin(3*360*x)/3+exp(-16*1.5)*sin(4*360*x)/4)
  /3.141592};

  \legend{$\tau=0.0$,$\tau=0.5$,$\tau=1.0$,$\tau=1.5$}
\end{axis}
   \end{tikzpicture}
\begin{itemize}
\item Showing 4 terms of the Fourier series
\end{itemize}
\vskip0.1cm
\hyperlink{general_form}{\beamergotobutton{Back to general solution form}}
\end{frame}

\appendixend

\end{document}