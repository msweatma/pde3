\documentclass[aspectratio=169]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Poisson equation and other coordinate systems}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Overview}
% -------------------------------------------------------- %
\begin{frame}[label=overview]
\frametitle{Overview}
\begin{block}{In this lecture}
\begin{itemize}
\item Complex variable solutions
\item Poisson equation
\item Other coordinate systems
\end{itemize}
\end{block}
\vskip0.2cm
\begin{block}{Appendix}
\begin{itemize}
\item Engineering example: linear wave theory \hyperlink{linearwavetheory}{\beamergotobutton{Linear wave theory}}
\item Example 9.31: slides for the Media Hopper video \hyperlink{example931}{\beamergotobutton{Example 9.31}}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\section{Complex variable solution}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Complex variable solutions}
\begin{itemize}
\item The real and imaginary parts of a \textcolor{blue}{differentiable} function, $f(z)$, of the complex variable $z=x+jy$ automatically satisfy the Laplace equation.
\item It is this method that was used to find the potential flow solutions needed to design airfoils before the advent of the digital computer. 
\item Though we also have to use the Joukowski transformation to map a unit circle onto the airfoil surface (see \url{www.grc.nasa.gov/WWW/k-12/airplane/map.html}).  
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Joukowski transform}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Joukowski transform}
\centerline{\includegraphics[width=0.3\columnwidth]{Joukowsky_transform1}
\includegraphics[width=0.6\columnwidth]{Joukowsky_transform2}}
The transform $z=\zeta+\frac1\zeta$ maps a circle $\zeta=\chi + i\eta$ onto an airfoil $z=x+iy$
\begin{align*}
z &= x+iy = \zeta + \frac1\zeta = \chi + i \eta + \frac{1}{\chi + i \eta}  \\
 & = \chi + i \eta + \frac{\chi - i \eta}{\chi^2 + \eta^2} \\
 & = \frac{\chi(\chi^2 + \eta^2 + 1)}{\chi^2 + \eta^2} + i \frac{\eta(\chi^2 + \eta^2 - 1)}{\chi^2 + \eta^2} 
\end{align*}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.32}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.32}
\begin{block}{If $f(z)=\phi(x,y)+j\psi(x,y)$ is a complex function of the complex variable $z=x+jy$}
\begin{itemize}
\item Verify that $\phi$ and $\psi$ satisfy the Laplace equation for the case where $f(z)=z^2$
\item Sketch the contours of $\phi$ and $\psi$
\end{itemize}
\end{block}
We have,
\[f(z)=z^2=x^2-y^2+j2xy\]
Now
\begin{align*}
\phi &= \Re(f(z))=x^2-y^2 \\
\psi & =\Im(f(z))=2xy
\end{align*}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.32}
Differentiating $\phi$ w.r.t. $x$  gives
\[\pderiv{\phi}{x}=2x\text{ and }\pdderiv{\phi}{x}=2\]
Differentiating $\phi$ w.r.t. $y$  gives
\[\pderiv{\phi}{y}=-2y\text{ and }\pdderiv{\phi}{y}=-2\]
The Laplacian is
\[\nabla^2\phi=\pdderiv{\phi}{x}+\pdderiv{\phi}{y}=2-2=0\]
So Laplace's equation is satisfied for $\phi$
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.32, continued}
Differentiating $\psi$ w.r.t. $x$  gives
\[\pderiv{\psi}{x}=2y\text{ and }\pdderiv{\psi}{x}=0\]
Differentiating $\phi$ w.r.t. $y$  gives
\[\pderiv{\psi}{y}=2x\text{ and }\pdderiv{\psi}{u}=0.\]
The Laplacian is
\[\nabla^2\psi=\pdderiv{\psi}{x}+\pdderiv{\psi}{y}=0+0=0,\]
So Laplace's equation is also satisfied for $\psi$
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.32, contour plot}
\vskip-0.3cm
\centerline{\includegraphics[width=0.8\columnwidth]{Ex9-32.pdf}}
\vskip-0.5cm
\begin{itemize}
\item $\psi=\text{const}$ give streamlines of flow into a corner
\item $\phi=\text{const}$ intersect with the streamlines at right angles
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Poisson equation}
% -------------------------------------------------------- %
\begin{frame}\frametitle{Poisson equation}
\begin{block}{The Poisson equation is}
\[ 
\nabla^2 u = f
\]
where \(u=u(x,y,z)\) and \(f=f(x,y,z)\)
\end{block}
\begin{itemize}
\item It is the Laplace equation with a scalar \textcolor{red}{Source} or \textcolor{blue}{Sink} term. 
\item Source terms allow the addition or removal of $u$ inside the solution domain and are a very important part of modelling real engineering problems.
\item Source terms can occur in all types of PDE, but normally make the problem much harder to solve.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.33}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}\frametitle{Example 9.33}
\begin{block}{Solve the Poisson equation}
\[\pdderiv{u}{x}+\pdderiv{u}{y}=-\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}\]
over the rectangle $0\le x\le a,\,0\le y\le b$ given the boundary conditions 
\begin{align*}
u(0,y)&=0&0\le y\le b\\
u(a,y)&=f(y)&0\le y\le b\\
\pderiv{u(x,0)}{y}&=0&0\le x\le a\\
\pderiv{u(x,b)}{y}&=0&0\le x\le a
\end{align*}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}\frametitle{Example 9.33}
\begin{itemize}
\item The approach here is to find a \textcolor{red}{particular integral} to eliminate the RHS, then to compute new boundary conditions and then solve the residual Laplace equation. 
\item In the present case we choose
\[
U=K\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}
\]
\item Substituting into the Poisson equation
\[\grad^2U=-K\left(\frac{\pi^2}{a^2}+\frac{\pi^2}{b^2}\right)\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}\]
and hence
\[K^{-1}=\pi^2\left(\frac1{a^2}+\frac{1}{b^2}\right)\]
\item We now write $u=U+v$ so that $v$ satisfies $\grad^2v=0$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest: Boundary conditions of the Poisson equation}
\TopHat : What are the boundary conditions for $\grad^2v=0$?
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item The same as for $\grad^2u=0$
\item Boundary conditions for $u$ multiplied by $K$
\item Boundary conditions for $u$ minus $K$
\item No idea
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\begin{block}{Solution}
\begin{itemize}
\item In this case the modified boundary conditions remain the same
\begin{align*}
v(0,y)&=0&0\le y\le b\\
v(a,y)&=f(y)&0\le y\le b\\
\pderiv{v(x,0)}{y}&=0&0\le x\le a\\
\pderiv{v(x,b)}{y}&=0&0\le x\le a
\end{align*}
\item Why are the boundary conditions the same?
\end{itemize}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution continued}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.33, continued}
We now have a \textsl{standard} Laplace equation problem which can be solved by separation of variables.
Choosing 
\[v(x,y) =\left(A\cosh \mu x+B\sinh \mu x\right)(C\sin \mu y+D\cos \mu y)\]
and applying the first three boundary conditions we  obtain the usual series solution
\[v(x,y)=\frac12 A_0x+\sum_{n=1}^\infty A_n\sinh\frac{n\pi x}{b}\cos\frac{n\pi y}{b}\]
where $A_n = BD$ for the different $\mu_n$.

Applying $v(a,y)=f(y)$ we have
\[f(y)=\frac12A_0x+\sum_{n=1}^\infty \left(A_n\sinh \frac{a}{b}n\pi\right)\cos\frac{n\pi y}{b}\]
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.33, continued}
We must now solve the general Fourier problem to find the unknown coefficients, using the usual methods we have
\begin{align*}
a_0&=A_0a=\frac2b\int_0^bf(y)\text{ d}y\\
a_n&=A_n\sinh\frac{a}{b}n\pi=\frac2b\int_0^bf(y)\cos\frac{n\pi y}{b}\text{ d}y&n=1,2,3,\ldots
\end{align*}
For the function $f(y)=y$ we get
\textcolor{red}{
\[
u(x,y) =\frac{\sin\frac{\pi x}{a}\cos\frac{\pi y}{b}}{\pi^2\left(\frac1{a^2}+\frac1{b^2}\right)}
+\frac{a_0x}{a}
+\sum_{n=0}^\infty \frac{a_n}{\sinh\frac{a}{b}n\pi}\sinh\frac{n\pi x}{b}\cos\frac{n\pi y}{b}
\]}
\end{frame}

% -------------------------------------------------------- %
\section{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest: Extreme point of the Poisson equation}
\TopHat : Where can the extreme points, e.g. maximum or minimum, of the solution to the Poisson equation be located?
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item On the boundary
\item Inside the solution domain
\item Anywhere
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\begin{block}{Solution}
\begin{enumerate}
\item[3.] Anywhere
\begin{itemize}
\item A heat source can lead to higher temperatures on the inside
\item A heat sink can lead to lower temperatures on the inside
\end{itemize}
\end{enumerate}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\section{Laplace equation in cylindrical coordinates}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Coordinate systems}
\begin{itemize}
\item It is often easier to express problems in coordinate systems different from the standard Cartesian coordinates
\item The integration problems in EM2B made extensive use of cylindrical and polar coordinates
\end{itemize}
\centerline{\includegraphics[height=5cm]{Glyn_Advanced_3_38}
\includegraphics[height=5cm]{Glyn_Advanced_3_39}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Derive cylindrical Laplace equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{2D cylindrical coordinates}
The coordinates are given by
\begin{align*}
x &= r \cos(\theta) \\
y &= r \sin(\theta) \\
r &= \sqrt{x^2 + y^2} \\
\theta &= \tan^{-1}\left(\frac{y}{x}\right)
\end{align*}
The Cartesian Laplace equation is
\[
\grad^2u = \pdderiv{u}{x} + \pdderiv{u}{y} = 0
\]
where $x= x(r,\theta)$ and $y = y(r, \theta)$. With
\[
u(x,y) = u(r, \theta)
\]
we get
\[
\grad^2 u(r, \theta) = 0
\]
\end{frame}

% -------------------------------------------------------- %
\begin{frame}[label=cylindrical_equations]
\frametitle{Laplace equation in cylindrical coordinates}
With the chain rule 
\[
\pderiv{u}{x} = \pderiv{u}{r}\pderiv{r}{x} + \pderiv{u}{\theta}\pderiv{\theta}{x}
\]
and by using the identity
\[
r^2 = x^2 + y^2 
\]
we can calculate the partial derivatives involving the coordinates $x$, $y$, $r$ and $\theta$. For example
\[
\pderiv{r^2}{x} = \pderiv{(x^2+y^2)}{x} \Rightarrow 2r\pderiv{r}{x} = 2x \Rightarrow \pderiv{r}{x} = \frac{x}{r} 
\]
\vskip0.2cm
Combining everything we get the Laplace equation in cylindrical coordinates
\begin{align*}
\pdderiv{u}{x} + \pdderiv{u}{y} &= \pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
\hyperlink{derive_cylindrical_equations}{\beamergotobutton{Complete derivation of cylindrical equations}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example}
Consider the Laplace equation in cylindrical coordinates
\begin{align*}
\pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
with the boundary conditions
\begin{align*}
u(a,\theta) &= 1, \quad 0\le\theta<\pi \\
u(a,\theta) &= 0, \quad \pi\le\theta<2\pi
\end{align*}
\begin{itemize}
\item The domain is a circle with radius $a$
\item The temperature at the top boundary is set to 1
\item The temperature at the bottom boundary is set to 0
\end{itemize}

\end{frame}
% -------------------------------------------------------- %
\subsection{Separated solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Separated solution in 2D cylindrical coordinates}
Consider the Laplace equation in cylindrical coordinates
\begin{align*}
\pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
and search for separated solutions
\begin{align*}
u(r,\theta) &= R(r) \Theta(\theta) \\
\frac{d^2 R}{dr^2} \Theta + \frac{1}{r} \frac{dR}{dr}\Theta + \frac{R}{r^2}\frac{d^2\Theta}{d\theta^2} &= 0 \\
\intertext{We separate the terms depending on $r$ and $\theta$ to get}
-\frac{\Theta''}{\Theta} &= \frac{R'' + \frac{R'}{r}}{\frac{R}{r^2}} = \lambda
\end{align*}
\vskip-0.3cm
\begin{itemize}
\item Consider three cases similar to the separated solutions in Cartesian coordinates
\item Gets more complicated if we have variation also in the $z$ direction
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Case $\lambda=0$}
We get the two solutions
\begin{align*}
\Theta(\theta) &= A\theta + B \\
R(r) &= C\ln(r) + D \\
u(r,\theta) &= (A\theta + B) (C\ln(r) + D)
\end{align*}
\begin{itemize}
\item The solution has to be periodic in $2\pi$ $\Rightarrow A=0$
\item The solution needs to be finite for $r\to0\Rightarrow C=0$
\item From this it follows that the solution is a constant
\[
u(r,\theta) = BD = k = \text{constant}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Case $\lambda<0$}
We get the two solutions with $\lambda = -\mu^2$
\begin{align*}
\Theta(\theta) &= A\cosh(\mu\theta) + B\sinh(\mu\theta) \\
R(r) &= C r^{-\mu} + D r^{\mu} \\
u(r,\theta) &= \left(A\cosh(\mu\theta) + B\sinh(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
\begin{itemize}
\item The solution has to be periodic in $\theta $ with period $2\pi$ 
\item From this it follows that $A=B=0$ and thus
\[
u(r,\theta) = 0
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Case $\lambda>0$}
We get the two solutions with $\lambda = \mu^2$
\begin{align*}
\Theta(\theta) &= A\cos(\mu\theta) + B\sin(\mu\theta) \\
R(r) &= C r^{-\mu} + D r^{\mu} \\
u(r,\theta) &= \left(A\cos(\mu\theta) + B\sin(\mu\theta)\right) \left(C r^{-\mu} + D r^{\mu}\right)
\end{align*}
\begin{itemize}
\item This is periodic in $\theta $ with period $2\pi$ for $\mu=n$ with $n\in\mathbb{N}$
\item To remain finite as $r\to0$ we need $C=0$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Combined solution}
\begin{itemize}
\item Thus the solution is
\[
u(r,\theta) = k + \sum_{n=1}^\infty r^{n}\left(A_n\cos(n\theta) + B_n\sin(n\theta)\right)
\]
\item Apply the boundary condition
\[
u(a,\theta) = k + \sum_{n=1}^\infty a^{n}\left(A_n\cos(n\theta) + B_n\sin(n\theta)\right) = \begin{cases}
1, \quad & 0\le\theta<\pi \\
0, \quad & \pi\le\theta<2\pi
\end{cases}
\]
\end{itemize}
Solve with the Fourier series
\begin{itemize}
\item Solve for the constant term
\[
a_0 = \frac{2}{2\pi}\int_0^{2\pi} u(a, \theta)d\theta = 1 \Rightarrow k = \frac{1}{2}a_0 = \frac{1}{2}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fourier series coefficients}
\begin{align*}
a_0 &= \frac{1}{\pi} \int_{0}^{\pi} 1 d\theta \\
A_n &= \frac{1}{a^n \pi} \int_{0}^{\pi} 1\cos\left(n\theta\right) d\theta = 0\\
B_n &= \frac{1}{a^n \pi} \int_{0}^{\pi} 1\sin\left(n\theta\right) d\theta = \begin{cases}
\frac{2}{a^n \pi n}, \quad & n \text{ odd} \\
0, \quad & n \text{ even}
\end{cases}
\end{align*}

\begin{itemize}
\item Thus the solution is
\[
u(r,\theta) = \frac{1}{2} + \sum_{k=1}^\infty \frac{2}{(2k-1)\pi a^{2k-1}} r^{2k-1}\sin((2k-1)\theta)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Laplace transform}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace transform method}
\begin{itemize}
\item Laplace transforms are not a natural solution method for the Laplace equation!
\item The Laplace equation has no time derivative, so unlike the wave equation and the heat/diffusion equation it cannot be solved using Laplace transforms.
\item Even for a semi-infinite domain where one space dimension is used as 'time' the Laplace transform method is unsuitable (more information about this later)
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Next lecture}
% -------------------------------------------------------- %
\begin{frame}
\frametitle[squezze]{Any questions?}
\vskip0.2cm
\begin{block}{Preparation for the next class}
\begin{itemize}
\item Topic: \textbf{Introduction and solution of parabolic equations}
\item Read sections 9.2.2 and 9.4 in Advanced Modern Engineering Mathematics before next lecture
\item Tell me on \TopHat
\begin{itemize}
\item What did you find confusing or difficult about what you read? 
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\item Exercises 9.5.2 and 9.5.4 in Advanced Modern Engineering Mathematics to strengthen material from this lecture
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

% -------------------------------------------------------- %
\section{Engineering example}
% -------------------------------------------------------- %
\begin{frame}[label=linearwavetheory]
\frametitle{Linear wave problem}
\centering\includegraphics[width=0.8\columnwidth]{techet_linear_wave_problem}
\begin{itemize}
\item The linear wave problem can be described by the Laplace equation
\[
\grad^2\varphi = \pdderiv{\varphi}{x} + \pdderiv{\varphi}{z} = 0
\]
\item $\varphi$ is the velocity potential
\item With appropriate boundary conditions
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Linear wave problem boundary conditions}
\vskip-0.1cm
\includegraphics[width=0.5\columnwidth]{techet_linear_wave_problem}
\begin{itemize}
\item Bottom boundary condition
\[
\pderiv{\varphi}{z} = 0, \quad z = -H
\]
\item Free Surface Dynamic Boundary Condition
\[
\eta = \frac{1}{g} \left(\pderiv{\varphi}{t}\right), \quad z=0
\]
\item Free Surface Kinematic Boundary Condition
\[
\pdderiv{\varphi}{t} + g \pderiv{\varphi}{z} = 0
\]
\end{itemize}
\vskip-0.2cm
\hyperlink{overview}{\beamergotobutton{Back}}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.31}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}[label=example931]
\frametitle{Example 9.31}
\begin{block}{Solve the Laplace equation $\nabla^2 u = 0$ subject to the following boundary conditions.}
\begin{align*}
u(x,0)&=x&0\le x\le 1 \\
u(x,2)&=0&0\le x\le 1 \\
u(0,y)&=0&0\le y\le 2 \\
\pderiv{u(1,y)}{x}&=0&0\le y \le 2
\end{align*}
\end{block}
This problem requires \textcolor{red}{zeros} on $x=0$ and a \textcolor{blue}{zero derivative} on $x=1$,
suggesting that trigonometric solutions in $x$ and exponential solutions in $y$ may be appropriate. So we try
\[u=(A\sin\mu x+B\cos\mu x)(C\cosh \mu y+D\sinh \mu y)\]
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.31}
\[
u(x,y) =(A\sin\mu x+B\cos\mu x)(C\cosh \mu y+D\sinh \mu y)
\]
Now the third condition, $u(0,y)=0\ \implies\ B=0,$ so we have
\[u(x,y)=(C^*\cosh \mu y+D^*\sinh \mu y)\sin\mu x\]
The fourth condition, 
\[
\pderiv{u(1,y)}{x} = 0 \ \implies\ \cos\mu=0
\]
So $\mu=(n+\frac12)\pi\ n=0,1,2,3,4,\ldots$ giving
\begin{align*}
u(x,y) =& \left(C^*\cosh\left(n+\frac12\right)\pi y +D^*\sinh    \left(n+\frac12\right)\pi y \right) \\
 & \sin\left(n+\frac12\right)\pi x, \quad n=0,1,2,3,\ldots
\end{align*}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of  example 9.31, continued}
To satisfy the second boundary condition, $u(x,2)=0$ it is better to write the solution as
\begin{align*}
u(x,y) &= \sin\left(n+\frac12\right)\pi x \\
&\left[E\cosh \left(\left(n+\frac12\right)\pi \left(2-y\right)\right)
+F\sinh\left(\left(n+\frac12\right)\pi \left(2-y\right)\right)\right]
\end{align*}
Here we have shifted and mirrored the part of the solution in $y$. It is easy to show that this is also a solution to the Laplace equation.

To satisfy the second boundary condition we require $E=0$. Giving, using superposition, the basic solution
\[
u(x,y)=\sum_{n=0}^\infty F_n\sin\left(\left(n+\frac12\right)\pi x\right)
\sinh\left(\left(n+\frac12\right)\pi \left(2-y\right)\right)
\]
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.31, continued}
Finally, the first boundary condition $u(x,0)=x$, gives the Fourier problem
\[
x = \sum_{n=0}^\infty F_n\sinh\left(\left(2n+1\right)\pi \right)
\sin\left(\left(n+\frac12\right)\pi x\right).
\]
So that 
\begin{align*}
\frac12F_n\sinh(2n+1)\pi &= \int_0^1x\sin\left(n+\frac12\right)\pi x\text{ d}x \\
&= \frac{\sin\left(\left(n+\frac12\right)\pi\right)}{\pi^2\left(n+\frac12\right)^2} = (-1)^n \frac{4}{\pi^2 (2n+1)^2}
\end{align*}
So the complete solution is
\textcolor{red}{
\[
u(x,y) = \frac8{\pi^2}\sum_{n=0}^\infty (-1)^n
\frac{\sin\left(\frac{(2n+1)}{2}\pi x\right) \sinh\left(\frac{(2n+1)}{2}\pi(2-y)\right)}{(2n+1)^2\sinh\left((2n+1)\pi\right)}
\]
}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.31 - plot the first 4 terms of the sum}
\begin{tikzpicture}[scale=0.95]
\begin{axis}[
title={$\frac8{\pi^2}\sum_{n=0}^\infty (-1)^n
\frac{\sin\left(\frac{(2n+1)}{2}\pi x\right) \sinh\left(\frac{(2n+1)}{2}\pi(2-y)\right)}{(2n+1)^2\sinh\left((2n+1)\pi\right)}$},
xlabel=$x$, ylabel=$y$, zlabel=$u$,
]
\addplot3[
surf,
domain=0:1, 
domain y=0:2,
]
{8/pi*(sin(90*x)*sinh(pi*(2-y)/2)/sinh(pi)
-sin(180*3/2*x)*sinh(3*pi/2*(2-y))/(9*sinh(3*pi))
+sin(180*5/2*x)*sinh(5*pi/2*(2-y))/(25*sinh(5*pi))
-sin(180*7/2*x)*sinh(7*pi/2*(2-y))/(49*sinh(7*pi))
)};
\end{axis}
\end{tikzpicture}
\vskip-0.2cm
\hyperlink{overview}{\beamergotobutton{Back}}
\end{frame}

% -------------------------------------------------------- %
\section{Derive cylindrical equations}
% -------------------------------------------------------- %
\begin{frame}[label=derive_cylindrical_equations]
\frametitle{Derivatives in cylindrical coordinates}
With the chain rule 
\[
\pderiv{u}{x} = \pderiv{u}{r}\pderiv{r}{x} + \pderiv{u}{\theta}\pderiv{\theta}{x}
\]
we can calculate the second derivatives
\begin{align*}
\pdderiv{u}{x} =& \pderiv{u}{r}\pdderiv{r}{x} + \pderiv{}{x}\left(\pderiv{u}{r}\right)\pderiv{r}{x}
   +\pderiv{u}{\theta}\pdderiv{\theta}{x} + \pderiv{}{x}\left(\pderiv{u}{\theta}\right)\pderiv{\theta}{x}
\end{align*}
The terms with the brackets require again the application of the chain rule
\begin{align*}
\pderiv{}{x}\left(\pderiv{u}{r}\right) &= \pderiv{}{r}\left(\pderiv{u}{r}\right)\pderiv{r}{x} + \pderiv{}{\theta}\left(\pderiv{u}{r}\right)\pderiv{\theta}{x} = 
\pdderiv{u}{r}\pderiv{r}{x} + \frac{\partial^2 u}{\partial\theta\partial r} \pderiv{\theta}{x} \\
\pderiv{}{x}\left(\pderiv{u}{\theta}\right) &= \pderiv{}{r}\left(\pderiv{u}{\theta}\right)\pderiv{r}{x} + \pderiv{}{\theta}\left(\pderiv{u}{\theta}\right)\pderiv{\theta}{x} = \frac{\partial^2 u}{\partial r\partial \theta} \pderiv{r}{x} +  \pdderiv{u}{\theta}\pderiv{\theta}{x}
\end{align*}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Partial derivatives of the different coordinates}
We can calculate the partial derivatives involving the coordinates $x$, $y$, $r$ and $\theta$ by using the identity
\[
r^2 = x^2 + y^2 
\]
\vskip-0.5cm
\begin{align}
\pderiv{r^2}{x} = \pderiv{(x^2+y^2)}{x} \Rightarrow 2r\pderiv{r}{x} = 2x &\Rightarrow \pderiv{r}{x} = \frac{x}{r} \\
\pdderiv{r}{x} = \pderiv{}{x}\left(\frac{x}{r}\right) \Rightarrow \pdderiv{r}{x} = \pderiv{}{x}\left(\frac{x}{\sqrt{x^2 + y^2}}\right) &\Rightarrow \pdderiv{r}{x} = \frac{y^2}{r^3} \\
\pderiv{\theta}{x} = \pderiv{}{x} \left(\arctan\left(\frac{y}{x}\right)\right) \Rightarrow \pderiv{\theta}{x} = - \frac{1}{1+\left(\frac{y}{x}\right)^2} \frac{y}{x^2}
&\Rightarrow \pderiv{\theta}{x} = - \frac{y}{r^2} \\
\pdderiv{\theta}{x} = \pderiv{}{x}\left(- \frac{y}{r^2}\right) \Rightarrow \pdderiv{\theta}{x} = \pderiv{}{x}\left(\frac{-y}{x^2 + y^2}\right)
&\Rightarrow \pdderiv{\theta}{x} = \frac{2xy}{r^4}
\end{align}
Here we have used
\[
\frac{d}{dt} \arctan(t) = \frac{1}{1+t^2}
\]
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Combine all the parts}
Combining all the parts we get
\begin{align*}
\pdderiv{u}{x} &= \pdderiv{u}{r} \frac{x^2}{r^2} + \pderiv{u}{r}\frac{y^2}{r^3} + \frac{\partial^2 u}{\partial r\partial \theta} \frac{-2xy}{r^3} + \pderiv{u}{\theta}\frac{2xy}{r^4} + \pdderiv{u}{\theta}\frac{y^2}{r^4} \\
\pdderiv{u}{y} &= \pdderiv{u}{r} \frac{y^2}{r^2} + \pderiv{u}{r}\frac{x^2}{r^3} + \frac{\partial^2 u}{\partial r\partial \theta} \frac{2xy}{r^3} - \pderiv{u}{\theta}\frac{2xy}{r^4} + \pdderiv{u}{\theta}\frac{x^2}{r^4}
\end{align*}
so that by adding the two equations we get
\begin{align*}
\pdderiv{u}{x} + \pdderiv{u}{y} &= \pdderiv{u}{r} + \frac{1}{r}\pderiv{u}{r} + \frac{1}{r^2}\pdderiv{u}{\theta} = 0
\end{align*}
which is the Laplace equation in 2D cylindrical coordinates.
\vskip0.2cm
\hyperlink{cylindrical_equations}{\beamergotobutton{Back to cylindrical equations}}
\end{frame}

\appendixend

\end{document}