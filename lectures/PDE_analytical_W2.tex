\documentclass[aspectratio=169]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Introduction to elliptic equations}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Laplace Equation}
% -------------------------------------------------------- %
\subsection{General form}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace Equation}
\begin{block}{The Laplace equation is}
\[
\grad^2 u(x,y) =0
\]
\end{block}
\begin{itemize}
\item It was first studied by Pierre-Simon Laplace (1749--1827), of whom you have already heard!
\item It is an \textcolor{red}{elliptic} equation which models the steady state of a diffusion like process.  
We can use it to describe the pressure in an incompressible flow, steady state diffusion and heat conduction, stress and strain, etc.
\item It is closely related to the Poisson equation \[\grad^2u=f(x,y)\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Example}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Show the stream function satisfies the Laplace equation}
\[
\psi(x,y) = Uy\left(1-\frac{a^2}{x^2+y^2}\right)
\]
\vskip0.1cm
Calculate the partial derivatives
{\small
\begin{align*}
\psi_x(x,y) &= \frac{2xyUa^2}{(x^2+y^2)^2}\\
\psi_y(x,y) &= U - \frac{Ua^2}{x^2+y^2} + \frac{2y^2Ua^2}{(x^2+y^2)^2} \\
\psi_{xx}(x,y) &= \frac{2yUa^2}{(x^2+y^2)^2} - \frac{8x^2yUa^2}{(x^2+y^2)^3} \\
\psi_{yy}(x,y) &= \frac{6yUa^2}{(x^2+y^2)^2} - \frac{8y^3Ua^2}{(x^2+y^2)^3}
\end{align*}}
and substitute into the Laplace Equation gives
{\small
\[
\grad^2\psi(x,y) =\frac{8yUa^2}{(x^2+y^2)^2}-\frac{8y(x^2+y^2)Ua^2}{(x^2+y^2)^3}=0
\]}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Contours of $\psi(x,y) =Uy\left(1-\frac{a^2}{x^2+y^2}\right)$}
\includegraphics[width=0.9\columnwidth,]{StreamLines.pdf}
\begin{itemize}
\item The streamlines are contours of $\psi$.
\item The streamlines show the flow of an inviscid, irrotational fluid past a cylinder placed in a uniform stream.
\item In this plot, for clarity, $\psi$ has been clipped below $-0.2$ so the singularity inside the cylinder, at (0,0), is not visible.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Derivation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Problem statement}
\begin{itemize}
\item Consider a flat, rectangular sheet of thermally conductive material
\item The left side is heated while the other sides are keep cold
\item There is no heat transfer in the z direction
\item There is no source or sink inside the sheet
\item Find the steady state temperature distribution in the sheet
\end{itemize}
\centerline{\includegraphics[width=0.45\columnwidth]{Laplace_derivation.png}}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Temperature in a small element}
\centerline{\includegraphics[width=0.5\columnwidth]{Laplace_derivation3.png}}
\begin{itemize}
\item Consider the small element between $x_0$, $x_1$, $y_0$ and $y_1$
\item There are four heat flow terms at its boundaries
\item The temperature inside is not changing over time, i.e. $\pderiv{T}{t}=0$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Assumptions}
\begin{itemize}
\item Heat flows from hot regions to cold regions
\item The rate at which heat flows through a plane section drawn in a body is proportional to its area and to the temperature gradient normal to the section
\item The quantity of heat in a body is proportional to its mass, specific heat capacity and to its temperature
\item Fourier's law for the conduction of heat is
\[
Q = -k \pderiv{T}{x}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Derivation 1/2}
\centerline{\includegraphics[width=0.4\columnwidth]{Laplace_derivation3.png}}
The heat flows across the boundaries need to balance because $T$ doesn't change with time
\[
0 = \left[Q(x_0,y) - Q(x_1,y)\right] (y_1 - y_0) + \left[Q(x,y_0) - Q(x,y_1)\right] (x_1 - x_0)
\]
Divide by $(x_1 - x_0)(y_1 - y_0)$
\[
0 = \frac{Q(x_0,y) - Q(x_1,y)}{x_1 - x_0} + \frac{Q(x,y_0) - Q(x,y_1)}{y_1 - y_0}
\]
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Derivation 2/2}
\vskip-0.2cm
\centerline{\includegraphics[width=0.4\columnwidth]{Laplace_derivation3.png}}
Reduce the square size so that $x_1 - x_0\to0$ and $y_1 - y_0\to0$
\[
0 = -\pderiv{Q}{x} - \pderiv{Q}{y}
\]
Apply Fourier's law and divide by $k$ to get 
\[
0 = \pdderiv{T}{x} + \pdderiv{T}{y} = \grad^2 T
\]
\end{frame}

% -------------------------------------------------------- %
\subsection{Example}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace equation}
\centerline{\includegraphics[width=0.7\columnwidth]{laplace_solution.png}}
\end{frame}

% -------------------------------------------------------- %
\section{Example}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{First solution}
Consider the function
\[
u(x,y) = a(x^2 - y^2) + bxy + c
\]
with constants $a, b, c$. 
The partial derivatives are
\begin{align*}
\pderiv{u}{x} &= 2ax + by \qquad &\pderiv{u}{y} &= -2ay + bx \\
\pdderiv{u}{x} &= 2a \qquad &\pdderiv{u}{y} &= -2a \\
\frac{\partial^2 u}{\partial x \partial y} &= b \qquad &\frac{\partial^2 u}{\partial y \partial x} &= b
\end{align*}
\vskip-0.1cm
\begin{itemize}
\item $u$ is a solution to the Laplace equation
\item The order of integration makes no difference if the function is smooth enough
\item True for most functions in engineering
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Second solution}
Consider the function
\[
v(x,y) = e^{-y} \cos(x)
\]
The partial derivatives are
\begin{align*}
\pderiv{v}{x} &= -e^{-y} \sin(x) \qquad &\pderiv{v}{y} &= -e^{-y} \cos(x) \\
\pdderiv{v}{x} &= -e^{-y} \cos(x) \qquad &\pdderiv{v}{y} &= e^{-y} \cos(x)
\end{align*}
\vskip-0.1cm
\begin{itemize}
\item $v(x,y)$ is also a solution of the Laplace equation
\item The superposition of $u$ and $v$ is also a solution
\[
w(x,y) = u(x,y) + v(x,y) = a(x^2 - y^2) + bxy + c + e^{-y} \cos(x)
\]
\item The particular solution for a given problem is determined by the boundary conditions
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
%----- Frame -------------------------------------------%
\begin{frame}{ConcepTest: boundary conditions}
\TopHat : Which boundary condition does the solution 
\[
v(x,y) = e^{-y} \cos(x)
\]
of the Laplace equation on $0\le x\le 1$ and $0\le y \le 1$ fulfil?
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item $u(1,y) = e^{-y}\cos(x)$ on $0\le y\le 1$
\item $u(1,y) = e^{-y}\cos(1)$ on $0\le y\le 1$
\item $u(1,y) = e^{-1}\cos(x)$ on $0\le y\le 1$
\item $u(1,y) = e^{-1}\cos(1)$ on $0\le y\le 1$
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\begin{block}{Solution}
\begin{itemize}
\item[2.] $u(1,y) = e^{-y}\cos(1)$ on $0\le y\le 1$
\end{itemize}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\section{Separated solutions}
% -------------------------------------------------------- %
\subsection{Idea}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Separation of Variables}
\begin{block}{Solve the PDE by seeking solutions of the form}
\[u(x,y)=X(x)Y(y)\]
where $X(x)$ and $Y(y)$ are single functions of single variables.
\end{block}
\begin{itemize}
\item We can sometimes find $X$ and $Y$ as solutions of \textcolor{blue}{ordinary differential equations}.
\item These are often much easier to solve than PDEs and it \textcolor{red}{may} be possible to build up full solutions of the PDE in terms of $X$ and $Y$.
\item This can be applied to many types of PDE.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Application to Laplace equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Separated solutions}
We seek solutions to the 2D Laplace equation of the form
\[u(x,y) =X(x)Y(y)\]
substituting this (and applying the product rule) gives
\[Y\frac{\text{d}^2X}{\text{d}x^2}+X\frac{\text{d}^2Y}{\text{d}y^2}=0\]
or
\[
\frac1X\frac{\text{d}^2X}{\text{d}x^2}=-\frac1Y\frac{\text{d}^2Y}{\text{d}y^2}=\lambda.
\]
Here $\lambda$ is independent of both $x$ and $y$
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Two simple harmonic ODEs 1/2}
Since the two equations are independent, we can split them into two ODEs of the simple harmonic type
\[
\frac{\text{d}^2X}{\text{d}x^2}=\lambda X\text{ and }\frac{\text{d}^2Y}{\text{d}y^2}=-\lambda Y.
\]
The type of the solution depends on the sign of $\lambda$, there are a variety of possibilities. Let us first look at the solutions for $X$:
\begin{align*}
X(x) =& A \cos(\mu x) + B \sin(\mu x) & \lambda=-\mu^2<0,\\
X(x) =& A e^{\mu x} + B e^{-\mu x} & \lambda=\mu^2>0,\\
X(x) =& (Ax+B) & \lambda=0
\end{align*}
The order of the first and second solution is swapped for $Y$ due to the minus sign.
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Two simple harmonic ODEs 2/2}
Since the two equations are independent, we can split them into two ODEs of the simple harmonic type
\[
\frac{\text{d}^2X}{\text{d}x^2}=\lambda X\text{ and }\frac{\text{d}^2Y}{\text{d}y^2}=-\lambda Y.
\]
By combining the solutions for $X$ and $Y$ we get
\begin{align*}
u=&(A\sin \mu x+B\cos \mu x)\left(Ce^{\mu y}+De^{-\mu y}\right)&\lambda=-\mu^2<0,\\
 =&(A\sin \mu x+B\cos \mu x)\left(\tilde{C}\cosh \mu y+\tilde{D}\sinh \mu y\right) & \\
u=&\left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y)&\lambda=\mu^2>0,\\
 =&\left(\tilde{A}\cosh \mu x+\tilde{B}\sinh \mu x\right)(C\sin \mu y+ D\cos \mu y) & \\
u=&(Ax+B)(Cy+D)&\lambda=0
\end{align*}
where $A,\, B,\, C,$ and $D$ are arbitrary constants.
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.29}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.29}
\begin{block}{Solve the Laplace equation $\nabla^2 u = 0$ subject to the following boundary conditions.}
\begin{align*}
u(x,0)&=0&0\le x\le 2 \\
u(x,1)&=0&0\le x\le 2 \\
u(0,y)&=0&0\le y \le 1\\
u(2,y)&=a\sin 2\pi y&0\le y \le 1
\end{align*}
\end{block}
\begin{itemize}
\item The last boundary condition is zero at the corners
\item The boundary conditions are consistent
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Recipe}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Recipe for separated solutions}
Consider the three possible solutions 
\begin{align*}
u_1(x,y) =& (A\sin \mu x+B\cos \mu x)\left(Ce^{\mu y}+De^{-\mu y}\right)&\lambda=-\mu^2<0, \\
 =& (A\sin \mu x+B\cos \mu x)\left(\tilde{C}\cosh \mu y+\tilde{D}\sinh \mu y\right) & \\
u_2(x,y) =& \left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y)&\lambda=\mu^2>0,\\
 =& \left(\tilde{A}\cosh \mu x+\tilde{B}\sinh \mu x\right)(C\sin \mu y+ D\cos \mu y) & \\
u_3(x,y) =& (Ax+B)(Cy+D)&\lambda=0
\end{align*}
and go through the boundary conditions to
\smallskip
\begin{enumerate}
\item Eliminate basic solutions
\item Specify the permissible values of $\mu$
\item Superpose solutions to fit the initial condition
\end{enumerate}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.29}
To satisfy the first two conditions we need solutions which are zero for varying $x$ and for two values of $y$. Thus, we need solutions of the form 
\[
u_2(x,y) = \left(Ae^{\mu x}+Be^{-\mu x}\right)(C\sin \mu y+D\cos \mu y)
\]

The first boundary condition, $u(x,0)=0$, gives
\[0=\left(Ae^{\mu x}+Be^{-\mu x}\right)D,\quad (0\le x \le 2)\]
Thus $D=0$ and we have
\[
u(x,y) =\left(A^*e^{\mu x}+B^*e^{-\mu x}\right)\sin \mu y
\] 
where $A^*=AC$ and $B^*=BC$.
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.29, continued}
Applying the second boundary condition, $u(x,1)=0$ we get
\[
0=\left(A^*e^{\mu x}+B^*e^{-\mu x}\right)\sin \mu,\quad (0\le x \le 2)
\] 
so $\sin\mu=0$ and we need $\mu=n\pi$ where $n$ is an integer.
\medskip
The third boundary condition, $u(0,y)=0$, gives
\[
(A^*+B^*)\sin n\pi y=0
\] 
which implies $A^*=-B^*$ giving
\begin{align*}
u(x,y) &= A^*\left(e^{n\pi x}-e^{-n\pi x}\right)\sin n\pi y\\
  &= 2A^*\sinh n\pi x\sin n\pi y
\end{align*}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution of example 9.29, continued}
The final boundary condition gives
\[
2A^*\sinh 2n\pi \sin n\pi y=a\sin 2\pi y,\quad (0\le y \le 1).
\]
So $n=2$ and $a=2A^*\sinh 4\pi$ So the solution is
\textcolor{red}{
\[
u(x,y) =a\sin2\pi y\frac{\sinh 2\pi x}{\sinh 4\pi}
\]
}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.29}
\begin{tikzpicture}
\begin{axis}[
title={$\sin2\pi y\frac{\sinh 2\pi x}{\sinh 4\pi}$},
xlabel=$x$, ylabel=$y$,
]
\addplot3[
surf,
domain=0:2, 
domain y=0:1,
]
{sin(360*y)*sinh(2*pi*x)/sinh(4*pi)};
\end{axis}
\end{tikzpicture}
\end{frame}

% ----------------------------------------------------- %
\section{Nondimensionalisation}
%-------------------------------------------------------%
\subsection{Example}
% ----------------------------------------------------- %
\begin{frame}{Do the equations make sense?}
Consider the last example in terms of heat conduction
\[
\nabla^2 u = 0
\]
where the variable $u$ is a temperature, i.e. $u=T$.
\medskip
\begin{itemize}
\item There are no parameters
\item The solution depends only on the boundary conditions
\item Are the given boundary conditions reasonable for heat conduction?
\begin{align*}
T(x,0)&=0&0\le x\le 2 \\
T(x,1)&=0&0\le x\le 2 \\
T(0,y)&=0&0\le y \le 1\\
T(2,y)&=a\sin 2\pi y&0\le y \le 1
\end{align*}
\vskip-0.2cm
\pause\item A temperature of $0$ on the boundary is very low
\item Maybe the equation is given in nondimensional form
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Procedure}
% ----------------------------------------------------- %
\begin{frame}{Procedure}
\begin{itemize}
\item Identify a characteristic value for each dependent and independent variable
\item Replace each dependent and independent variable with a variable scaled with the characteristic value
\item Rewrite equations in terms of the nondimensional variables
\end{itemize}
\vskip-0.1cm
\begin{block}{Example}
\begin{itemize}
\item In the dimensional form the temperature at the bottom, top and left boundary can be used as the characteristic value: call it $T_{ref}$
\item Set $\tilde{T}  = T T_{ref} + T_{ref}$ or 
\[
T = \frac{\tilde{T} - T_{ref}}{T_{ref}}
\]
\item $T$ varies around $0$ and $\tilde{T}$ varies around $T_{ref}$
\item Substitute this into the Laplace equation
\end{itemize}
\end{block}
\end{frame}

% ----------------------------------------------------- %
\begin{frame}{Nondimensionalisation continued}
Consider the derivatives $\pderiv{T}{x}$ and $\pderiv{T}{x}$
\begin{itemize}
\item Set $T=\frac{\tilde{T} - T_{ref}}{T_{ref}}$ with $T_{ref}=const$
\item Then we get
\vskip-0.6cm
\begin{align*}
\pderiv{T}{x} &= \frac1{T_{ref}}\pderiv{\tilde{T}}{x} \\
\pdderiv{T}{x} &= \frac1{T_{ref}}\pdderiv{\tilde{T}}{x}
\end{align*}
\item Substitute into the Laplace equation
\begin{align*}
&\nabla^2 T = \pdderiv{T}{x} + \pdderiv{T}{y} = \frac1{T_{ref}} \pdderiv{\tilde{T}}{x} + \frac1{T_{ref}} \pdderiv{\tilde{T}}{y} = 0 \\
\Rightarrow & \pdderiv{\tilde{T}}{x} + \pdderiv{\tilde{T}}{y} = 0 
\end{align*}
\end{itemize}
\end{frame}

% ----------------------------------------------------- %
\begin{frame}{Nondimensionalisation continued}
\begin{itemize}
\item The boundary conditions in the dimensional form are
\begin{align*}
\tilde{T}(x,0) &= T_{ref} & 0\le x\le 2 \\
\tilde{T}(x,1) &= T_{ref} & 0\le x\le 2 \\
\tilde{T}(0,y) &= T_{ref} & 0\le y \le 1\\
\tilde{T}(2,y) &= T_{ref}a\sin 2\pi y + T_{ref} & 0\le y \le 1
\end{align*}
\end{itemize}
\smallskip
\begin{block}{Remarks}
\begin{itemize}
\item We didn't nondimensionalise the independent variables
\item Not applicable here but nondimensionalisation can reduce the number of parameters
\item This makes it easier to compare numerical experiments
\item We will come back to this later
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
%----- Frame -------------------------------------------%
\begin{frame}{ConcepTest: nondimensionalisation}
\TopHat : What are suitable characteristic values for the nondimensionalisation of the independent variables for $0\le x\le 2$ and $0\le y \le 1$?
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item $x_{ref} = 1$ and $y_{ref} = 1$
\item $x_{ref} = 2$ and $y_{ref} = 2$
\item $x_{ref} = 1$ and $y_{ref} = 2$
\item $x_{ref} = 2$ and $y_{ref} = 1$
\item Some other characteristic values
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\begin{block}{Solution}
\begin{itemize}
\item All options are possible
\item Option 1. and 2. are the most suitable
\item Option 3. and 4. will result in a scale factor between the different terms in the Laplace equation
\end{itemize}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\section{Next lecture}
% -------------------------------------------------------- %
\begin{frame}
\frametitle[squezze]{Any questions?}
\vskip0.2cm
\begin{block}{Preparation for the next class}
\begin{itemize}
\item Topic: \textbf{Properties of solutions of the Laplace equation and boundary conditions}
\item Read section 9.5 in Advanced Modern Engineering Mathematics before next lecture
\item Check the \WeeklyTarget
\item Tell me on \TopHat
\begin{itemize}
\item What did you find confusing or difficult about what you read? 
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\item Exercises 9.5.2 in Advanced Modern Engineering Mathematics to strengthen material from this lecture
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}