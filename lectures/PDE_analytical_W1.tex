\documentclass[aspectratio=169]{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Introduction}
\author[Daniel Friedrich]{David Ingram and Daniel Friedrich}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Introduction}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Vibrating string}
\centerline{\includegraphics[width=0.85\columnwidth]{VibratingString03-400x220.jpg}}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{The wave equation}
\begin{block}{One dimensional wave equation}
\[\frac{1}{c^2}\pdderiv{u}{t}=\pdderiv{u}{x}\]
where $c^2=T/\rho$ where $T$ is the tension and $\rho$ is the mass per unit length.
\end{block}
\begin{itemize}
\item It can model sound waves, shock waves, electromagnetic waves, water waves, a plucked string, etc.
\item The one dimensional form was discovered by the French scientist Jean-Baptiste le Rond d'Alembert (1717--1783)
\item The equation is said to be \textcolor{red}{hyperbolic} in character and has the nasty property that discontinuities in $u$, or its derivative, $\frac{\partial u}{\partial x}$, will propagate in time.
\end{itemize}
\end{frame}

%-------------------------------------------------------%
\subsection{Linear advection}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Linear advection}
\begin{block}{Show that}
\[u(x,t)=a\exp\left[-\left(\frac{x}{h}-\frac{c t}{h}\right)^2\right]\]
satisfies the wave equation.
\end{block}
\vskip0.1cm
Differentiating $u$ twice with respect to $x$ and $t$
{\small
\begin{align*}
u_{xx}&=\frac{-2a}{h^2}\exp\left[-\left(\frac{x}{h}-\frac{c t}{h}\right)^2\right]
+\frac{4a(x-ct)^2}{h^4}\exp\left[-\left(\frac{x}{h}-\frac{c t}{h}\right)^2\right] \\
u_{tt}&=\frac{-2a c^2}{h^2}\exp\left[-\left(\frac{x}{h}-\frac{c t}{h}\right)^2\right]
+\frac{4a(x-ct)^2c^2}{h^4}\exp\left[-\left(\frac{x}{h}-\frac{c t}{h}\right)^2\right]
\end{align*}
}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Plot of the linear advection solution}
\begin{tikzpicture}[scale=1]
 \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$u$]
  \addplot[black,domain=-2:10, samples=101] {exp(-x^2)};
  \addplot[blue, dotted, line width=1.5,domain=-2:10, samples=101] {exp(-(x-2)^2)};
  \addplot[cyan,dashed, line width=1.5,domain=-2:10, samples=101] {exp(-(x-4)^2)};
  \addplot[magenta,loosely dotted, line width=1.5,domain=-2:10, samples=101] {exp(-(x-8)^2)};

  \legend{$t=0$,$t=\frac{2h}{c}$,$t=\frac{4h}{c}$,$t=\frac{8h}{c}$}
 \end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Linear Advection with a discontinuity}
\centerline{\includegraphics[width=0.58\columnwidth]{LW-tvd.pdf}}
\vskip-0.2cm
\begin{itemize}
\item Shocks or discontinuities pose problems for numerical solvers
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution methods: analytical vs. numerical}
\begin{block}{Analytical solutions}
\begin{itemize}
\item Exact and explicit solution
\item Fast to evaluate the solution behaviour for changes in the parameters: e.g. changes in tension
\item Gives insight into the solution behaviour
\item Only available for special and often simple cases
\end{itemize}
\end{block}
\vskip0.0cm
\begin{block}{Numerical solutions}
\begin{itemize}
\item Available for any case (limited by available computational resources)
\item Might take significant time to compute
\item Only gives the solution for one set of parameters and boundary conditions
\item Needs to be evaluated for every set of parameters
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\section{What are PDEs?}
% -------------------------------------------------------- %
\subsection{ODE}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Ordinary differential equations}
\centerline{\includegraphics[angle=180,width=0.56\columnwidth]{HydraulicDamper.jpg}} \medskip
 For this hydraulic damper, the resistance is proportional to the velocity of compression, $v$.  If a motion of a mass, $m$, is being arrested by the damper, we can write $$\frac{\mathrm{d}v(t)}{\mathrm{d}t}=-\lambda v(t),\,v(0)=U,$$ where  $\lambda=k/m$, $v(t)$ is the speed of the mass at time $t$, $U$ is its initial velocity and $k$ is the damping coefficient. \\ [6pt]
\begin{itemize}
\item What is the order of the ODE?
\item What are the independent and dependent variables?
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Analytical solution}
\begin{itemize}
\item Let us calculate the analytical solution for the hydraulic damper
\item This was covered in Engineering Mathematics 2A
\item Please revise the material from EM2A if you are not familiar with this
\end{itemize}
\begin{eqnarray*}
\mbox{General solution of} && \frac{\mathrm{d}v}{\mathrm{d}t}=-\lambda v\\
\mbox{Separating variables} && \int\frac{\mathrm{d}v}{v}=\int-\lambda\,\mathrm{d}t,\\
\mbox{Integrating} && \ln{v}+D=-\lambda t,\\
\mbox{Take antilogs} && ve^D=e^{-\lambda t},\\
\mbox{So } && v=Ce^{-\lambda t}
\end{eqnarray*}
Now $v(0)=U$, so the particular solution is:
\[
v=Ue^{-\lambda t}
\]
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Plot of the solution}
\begin{tikzpicture}
 \begin{axis}[ 
    axis lines=left,
    scaled ticks=false,
    xlabel={$t$ [s]},
    ylabel={$v/U$ [--]}
 ] 
 \addplot[green, dashed, domain=0:5, samples=201, line width=1.5] {e^(-0.5*x)};
 \addplot[red, domain=0:5, samples=201, line width=1.5] {e^(-x)}; 
 \addplot[blue, dotted, domain=0:5, samples=201, line width=1.5] {e^(-2*x)};
 \legend{$\lambda=0.5$,$\lambda=1.0$, $\lambda=2.0$}
 \end{axis}
\end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\subsection{PDE vs ODE}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{PDE vs ODE}
\vskip-0.1cm
\begin{block}{More than one independent variable}
\vskip-0.1cm
\begin{itemize}
\item The wave equation depends on time and space
\item Stationary problems with multiple space dimensions 
\end{itemize}
\end{block}
\vskip-0.1cm
\begin{block}{Partial derivatives}
\vskip-0.1cm
\begin{itemize}
\item[ODE] Derivative 
\[
\frac{\mathrm{d}f}{\mathrm{d}t} = f' = \dot{f} 
\]
\item[PDE] Partial derivative
\begin{align*}
\pderiv{f(t,x)}{t} &= f_{t}(t,x) = \partial_{t} f(t,x) \\
\pderiv{f(t,x)}{x} &= f_{x}(t,x) = \partial_{x} f(t,x) \\
\pdderiv{f(t,x)}{x} &= f_{xx}(t,x) = \partial_{xx} f(t,x)
\end{align*}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{PDE vs ODE continued}
\vskip-0.1cm
\begin{block}{Existence of a solution}
\vskip-0.1cm
\begin{itemize}
\item[ODE] Existence and uniqueness theorems exist
\item[PDE] No guarantees of existence
\end{itemize}
\end{block}
\vskip-0.1cm
\begin{block}{Initial and boundary conditions}
\vskip-0.1cm
\begin{itemize}
\item Solution behaviour for many PDEs depends on the conditions at the boundaries and how the system is initialised 
\item It is often easy to find a solution of the PDE
\item Much harder to find a solution fitting the initial and boundary conditions
\end{itemize}
\end{block}
\begin{block}{Illustration of the solution much harder}
\vskip-0.1cm
\begin{enumerate}
\item[ODE] Time-value plot
\item[PDE] Multiple space plus time dimensions
\end{enumerate}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
%----- Frame -------------------------------------------%
\begin{frame}{ConcepTest: order and independent variables}
\TopHat : What is the order and what are the independent variables of the following PDE?
\[
\pdderiv{x}{y} = \pderiv{x}{t} + \sin(t)
\]
\begin{overprint}
\onslide<1>
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item Order 1, $x$ and $t$
\item Order 1, $x$ and $y$
\item Order 1 and $t$
\item Order 2, $x$ and $t$
\item Order 2, $x$ and $y$
\item Order 2, $y$ and $t$
\item Order 2 and $t$
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\begin{block}{Solution}
\begin{enumerate}
\item[6.] Order 2, $y$ and $t$
\end{enumerate}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\section{Examples}
% -------------------------------------------------------- %
\subsection{Mechanical engineering}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{PDEs in mechanical engineering}
Many mechanical engineering problems are governed by partial differential equations (PDEs), i.e. equations involving partial derivatives. In my research work I use advection-diffusion-reaction equations.
\begin{columns}
\column{.6\textwidth}
\includegraphics[width=6.4cm]{columnPlusPellet.pdf} 
\column{.43\textwidth}
\includegraphics[width=4.3cm]{CCSi_CO2_concentration.pdf} 
\end{columns}
\medskip
Other examples are
\begin{itemize}
\item Equations of fluid flow
\item Heat conduction
\item Wave propagation
\item Stress analysis
\item Mass, energy and momentum balances
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Fluid flow}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fluid flow equations}
\begin{block}{Incompressible Euler equations}
\begin{align*}
\nabla.\vect{u}&=0 \\
\pderiv{\vect{u}}{t}+ \underbrace{\vect{u}.\grad\vect{u}}_\text{advection}&=-\grad s
\end{align*}
where $\vect{u}$ is the velocity and $s=p/\rho_0$ is the specific pressure.
\end{block}
in 1755 Euler wrote ``\emph{If difficulties remain they will not be in the field of mechanics,
but entirely in the field of analysis}''
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Fluid flow equations 2}
He was wrong and didn't know about viscosity in fluids, so in 1822 Claude Louis Marie Henri Navier and Sir George Gabriel Stokes derived the 
\begin{block}{Incompressible Navier-Stokes equations}
\vskip-0.4cm
\begin{align*}
\nabla.\vect{u}&=0 \\
\pderiv{\vect{u}}{t}+
\underbrace{\vect{u}.\grad\vect{u}}_\text{advection}
-\underbrace{\nu\grad^2\vect{u}}_\text{diffusion}
&=-\grad s+\vect{g}
\end{align*}
where $\nu=\mu/\rho_0$ is the kinematic viscosity.
\end{block}
Analytical solutions can only be found for simple cases such as laminar flow in infinitely long straight pipes:

\centering
\includegraphics[width=5.9cm]{PhD_transportEffects.png}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Applications of the Navier-Stokes equations}
Analytical solutions cannot be found for real problems (we can for laminar flow in an infinitely long straight pipe),
so we use numerical methods and massive computers to obtain...

\centering\includegraphics[width=0.7\columnwidth]{weather} \\
 
Iso-bars from the BBC Weather forecast 3-Jan-2015
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Applications of the Navier-Stokes equations}
\centering
\includegraphics[width=0.8\columnwidth]{SeaGenSim.jpg} \\
 
Flow passed the SeaGen turbine 
 
(CFD simulation by Dr Angus Creech, IES)
\end{frame}

% -------------------------------------------------------- %
\section{Basic PDE types}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Three basic PDE types}
In this part of the course we will consider analytical solutions for three basic types of PDE
\begin{enumerate}
\item The Laplace equation which is elliptic
\item The heat conduction or diffusion equation which is parabolic
\item The wave equation which is hyperbolic
\end{enumerate}
\bigskip
These three different types of equations
\begin{itemize}
\item Appear in many areas of engineering
\item Have different solution properties and behaviours
\item Require different boundary conditions
\item Require different numerical solution methods
\item Any second order linear PDE can be reduced to one of these types
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Wave equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Wave equation}
\vskip0.1cm
\centerline{\includegraphics[width=0.6\columnwidth]{VibratingString03-400x220.jpg}}
\smallskip
\begin{block}{The general form of the wave equation is}
\vskip-0.1cm
\[\frac{1}{c^2}\pdderiv{u}{t}=\pdderiv{u}{x}+\pdderiv{u}{y}+\pdderiv{u}{z}=\grad^2u.\]
\end{block}
\end{frame}

%-------------------------------------------------------%
\subsection{Example}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Megatsunami propagation in a fjord}

\includegraphics[width=0.45\columnwidth]{coastline-60.pdf}
\includegraphics[width=0.45\columnwidth]{coastline-120.pdf}

Landslide generated megatsunami in a fjord similar to Lituya Bay in Alaska. Where in 1958 a landslide in the Gilbert inlet caused a wave of up to 91m high.  Based on a survivors evidence the wave travelled at about 120 mph!
\end{frame}

% -------------------------------------------------------- %
\section{Heat conduction or diffusion equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Heat-conduction or diffusion equation}
\centerline{\includegraphics[width=0.45\columnwidth]{heated_rod.jpg}}
\begin{itemize}
\item General equation
\[
\frac1\kappa\pderiv{T}{t}=\grad^2T
\]
\item This equation is \textcolor{red}{parabolic} in nature and given enough time (and constant boundary conditions) the solution will converge to a steady state.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Example}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example: point heat source}
\begin{itemize}
\item Assume that we have a rod extending to plus and minus infinity
\item At time zero this is heated to infinity at the origin
\item The temperature in the rod can be described by
\[
T(x,t) = \frac1{\sqrt{t}}\exp\left(-\frac{x^2}{4\kappa t}\right)
\]
\item This functions satisfies the heat conduction equation
\begin{itemize}
\item Calculate the second derivatives with respect to \(x\) and the first derivative with respect to \(t\)
\item Insert these into the heat conduction equation
\item Check that the equation is fulfilled
\end{itemize}
\item This solution is approximately valid for many point heat sources in sufficiently long rods
\end{itemize}

\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Diffusion -- Reduced temperature at $t=\frac{L^2}{4\kappa}.$ }
\begin{tikzpicture}
  \begin{axis}[legend style={draw=none}, 
  legend pos=outer north east,
  xlabel=$x$,ylabel=$\frac{T}{\sqrt{4\kappa}}$]
  \addplot[black,domain=-2:2, samples=101] {1/sqrt(0.1^2)*exp(-x^2/0.1^2)};
  \addplot[blue, dotted, line width=1.5,domain=-2:2, samples=101] {1/sqrt(0.2^2)*exp(-x^2/0.2^2)};
  \addplot[cyan, dashed, line width=1.5,domain=-2:2, samples=101] {1/sqrt(0.3^2)*exp(-x^2/0.3^2)};
  \addplot[magenta, dashdotted, line width=1.5,domain=-2:2, samples=101] {1/sqrt(0.5^2)*exp(-x^2/0.5^2)};
  \addplot[red, loosely dotted, line width=1.5,domain=-2:2, samples=101] {1/sqrt(1^2)*exp(-x^2/1^2)};

  \legend{$L=0.1$,$L=0.2$,$L=0.3$,$L=0.5$,$L=1.0$}
\end{axis}
   \end{tikzpicture} 
\end{frame}

% -------------------------------------------------------- %
\section{Laplace Equation}
% -------------------------------------------------------- %
\subsection{General form}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Laplace Equation}
\begin{block}{The Laplace equation is}
\[\grad^2u=0.\]
\end{block}
\begin{itemize}
\item It was first studied by Pierre-Simon Laplace (1749--1827), of whom you have already heard!
\item It is an \textcolor{red}{elliptic} equation which models the steady state of a diffusion like process.  
We can use it to describe the pressure in an incompressible flow, steady state diffusion and heat conduction, stress and strain, etc.
\item It is closely related to the Poisson equation \[\grad^2u=f(x,y).\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Example}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Inviscid, irrotational flow}
\begin{itemize}
\item Used in subsonic aerodynamics
\item Can be used to calculate lift on aerofoils
\item One solution to the Laplace equation is the stream function
\[
\psi(x,y) = Uy\left(1-\frac{a^2}{x^2+y^2}\right)
\]
\item This functions satisfies the Laplace equation
\begin{itemize}
\item Calculate the second derivatives with respect to \(x\) and \(y\)
\item Insert these into the Laplace equation
\item Check that they add up to zero
\end{itemize}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Contours of $\psi=Uy\left(1-\frac{a^2}{x^2+y^2}\right)$}
\includegraphics[width=\columnwidth,]{StreamLines.pdf}
\begin{itemize}
\item The streamlines are contours of $\psi$.
\item The streamlines show the flow of an inviscid, irrotational fluid past a cylinder placed in a uniform stream.
\item In this plot, for clarity, $\psi$ has been clipped below $-0.2$ so the singularity inside the cylinder, at (0,0), is not visible.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Next lecture}
% -------------------------------------------------------- %
\begin{frame}
\frametitle[squezze]{Any questions?}
\vskip0.2cm
\begin{block}{Preparation for the next class}
\begin{itemize}
\item Topic: \textbf{Introduction to the Laplace equation}
\item Read sections 9.2.3 and 9.5 in Advanced Modern Engineering Mathematics before next lecture
\item Check the \WeeklyTarget
\item Tell me on \TopHat
\begin{itemize}
\item What did you find confusing or difficult about what you read? 
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\item Exercises 9.2.6 in Advanced Modern Engineering Mathematics to strengthen material from this lecture
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}