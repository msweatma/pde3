\documentclass{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Heat transfer examples and Laplace transform method}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Recap}
% -------------------------------------------------------- %
\subsection{General form}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Heat conduction or diffusion equation}
The heat conduction equation is
\[
\frac1\kappa\pderiv{T}{t} = \grad^2T = \pdderiv{T}{x_1} + \cdots + \pdderiv{T}{x_n}
\]
for $n$ space dimensions.
\medskip
\begin{itemize}
\item This equation is \textcolor{red}{parabolic} in nature and given enough time (and constant boundary conditions) the solution will converge to a steady state.
\item If there are no time-varying inputs this has the steady state solution, $U$, satisfying the Laplace equation
\[ 
\nabla^2U = 0
\]
and the boundary conditions.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Boundary conditions}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Types of boundary conditions}
If $\Omega$ is the domain on which the given equation is to be solved and $\partial\Omega$ denotes its boundary, we can have
\begin{itemize}
\item Dirichlet 
\[u\text{ is specified on } \partial\Omega.\]
\item Neumann 
\[\pderiv{u}{n}\text{ is specified on }\partial\Omega.\]
\item Cauchy  
\[\text{both }u\text{ and }\pderiv{u}{n}\text{ are specified on }\partial\Omega.\]
\item Robin 
\[au+B\pderiv{u}{n}=g\text{ on }\partial\Omega.\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Heat conduction equation boundary conditions}
\centerline{\includegraphics[width=\columnwidth]{DiffusionBC.pdf}}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Appropriateness of Boundary Conditions}
\centerline{\includegraphics[width=0.95\columnwidth]{BCMatrix.pdf}}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.22}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}[label=example_922]
\frametitle{Example 9.22}
Solve the one dimensional heat condition equation
\[
\pderiv{u}{t}=\kappa\pdderiv{u}{x}
\]
subject to the boundary conditions
\begin{align*}
u(0,t) &= 0 & \forall t\ge0\\
u(1,t) &= 1 & \forall t\ge0\\
u(x,0) &= x(2-x) & 0\le x \le 1
\end{align*}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest: heat equation solution}
\TopHat : What is the steady state solution of this problem for $t\to\infty$?
\begin{overprint}
\onslide<1>
\vskip-0.4cm
\begin{block}{ConcepTest: possible solutions}
\centerline{\includegraphics[width=6cm]{heat_steady_state2}}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\begin{enumerate}
\item Solution 2 is correct: at $t\to\infty$ the initial disturbance will have dispersed and the solution will be a linear temperature profile between the two boundary values
\end{enumerate}
\vskip-0.2cm
\centerline{\includegraphics[width=4.2cm]{heat_steady_state2}}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}{ConcepTest: transient solution initial condition}
\TopHat : What is the initial condition for the transient solution?
\begin{itemize}
\item The steady state solution is $U=x$ 
\item This satisfies the first two boundary conditions and also $\nabla^2 U=0$
\item Writing $u=U+v$ we know that $v$ needs to satisfy the PDE, but with new boundary conditions
\begin{align*}
u(0,t) &=0 &\implies v(0,t) &= u(0,t) - U(0) = 0 \\
u(1,t) &= 1 &\implies v(1,t) &= u(1,t) - U(1) = 0 \\
u(x,0) &= x(2-x) &\implies v(x,0) &= ?
\end{align*}
\end{itemize}
\begin{overprint}
\onslide<1>
\vskip-0.4cm
\begin{block}{ConcepTest: possible solutions}
\vskip-0.2cm
\begin{enumerate}
\item \(v(x,0) = x - 2x^2\)
\item \(v(x,0) = 2x\)
\item \(v(x,0) = x - x^2\)
\item \(v(x,0) = - x^2\)
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\begin{enumerate}
\item Solution 3 is correct: 
\begin{align*}
u(x,0) &= x(2-x) &\implies v(x,0) &= x(2-x) - U(x) \\
& & &= 2x-x^2 - x = x - x^2
\end{align*}
\end{enumerate}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.22 continued}
\begin{itemize}
\item As before we are seeking solutions of the form
\[
v(x,t) = e^{-\alpha t} \left(A\sin\lambda x+B\cos\lambda x\right)
\]
\item The first boundary condition
\[
v(x=0, t) = e^{-\alpha t} \left(A\sin(\lambda 0) + B\cos(\lambda 0)\right) \overset{!}{=} 0
\]
is only fulfilled for $B=0$
\item The second boundary condition 
\[
v(x=1, t) = e^{-\alpha t} A\sin(\lambda 1)  \overset{!}{=} 0
\]
is only fulfilled for $\sin\lambda=0$ which gives
\[
\lambda=n\pi,\quad n=1,2,3,\ldots
\]
\item To fulfil the initial condition we need to superpose the solution for all $n=1,2,3,\dots$
\[
v(x,t) = \sum_{n=1}^\infty a_n e^{-\alpha_n t} \sin(n \pi x)
\]
\end{itemize}
\end{frame}


% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.22 continued}
\begin{itemize}
\item The initial condition requires
\[
v(x,t=0) = \sum_{n=1}^\infty a_n e^{-\alpha_n 0}\sin( n\pi x) \overset{!}{=} x - x^2
\]
\item Determining the Fourier coefficients in the normal 
\[
a_n = 2\int_0^1\left(x-x^2\right)\sin( n\pi x) \text{ d}x
\]
\item The following integrals help in calculating Fourier series coefficients
\begin{align*}
\int x^2\sin(ax)dx &= \frac{2x}{a^2}\sin(ax) - \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \cos(ax) \\
\int x^2\cos(ax)dx &=  \frac{2x}{a^2}\cos(ax) + \left(\frac{x^2}{a} - \frac{2}{a^3}\right) \sin(ax)
\end{align*}

\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.22 continued}
\begin{itemize}
\item With this we can determine the Fourier coefficients 
\begin{align*}
a_n &= 2\int_0^1\left(x-x^2\right)\sin( n\pi x) \text{ d}x \\
&= 2\int_0^1 x\sin( n\pi x) \text{ d}x - 2\int_0^1 x^2\sin( n\pi x) \text{ d}x \\
&= \begin{cases}
\frac{8}{n^3 \pi^3}, & \text{odd } n \\
0, &\text{even }n
\end{cases}
\end{align*}
\item Using $\alpha_n = \kappa\lambda_n^2$ we get the complete solution
\textcolor{red}{
\begin{align*}
u(x,t) &= U(x) + v(x,t) \\
  &= x + \frac8{\pi^3}\sum_{k=1}^\infty\frac1{(2k-1)^3}e^{-(2k-1)^2 \kappa\pi^2t}\sin\left((2k-1)\pi x\right)
\end{align*}
}
where $n=2k-1$ to cover only the odd values of $n$
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.22 solutions at $\tau=\kappa\pi^2t$}

\begin{tikzpicture}
  \begin{axis}[legend style={draw=none}, 
  legend pos=south east,
  xlabel=$x$,ylabel=$u$]
% t=0.0
 \addplot[black,domain=0:1, samples=201] {x*(2-x)};
% t=0.2
  \addplot[cyan, dotted, line width=1.5, domain=0:1, samples=201] 
  {x+8*(exp(-0.2)*sin(180*x)+exp(-9*0.2)*sin(3*180*x)/27+exp(-25*0.2)*sin(5*180*x)/125)/3.141592^3};
% t=0.6
   \addplot[blue, dashed, line width=1.5, domain=0:1, samples=201] 
  {x+8*(exp(-0.6)*sin(180*x)+exp(-9*0.6)*sin(3*180*x)/27+exp(-25*0.6)*sin(5*180*x)/125)/3.141592^3};
% t=1.0
   \addplot[red, dashdotted, line width=1.5, domain=0:1, samples=201] 
  {x+8*(exp(-1.0)*sin(180*x)+exp(-9*1.0)*sin(3*180*x)/27+exp(-25*1.0)*sin(5*180*x)/125)/3.141592^3};

  \legend{$\tau=0.0$,$\tau=0.2$,$\tau=0.6$,$\tau=1.0$}
\end{axis}
   \end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\section{Laplace transform method}
% -------------------------------------------------------- %
\subsection{Diffusion example}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Diffusion equation}
Consider the one dimensional diffusion equation is
\[ 
\pderiv{u}{t} = \kappa \pdderiv{u}{x}, \qquad \forall x,\, t\ge 0
\]
\begin{itemize}
\item The concentration $u(x,t)$ needs to remain bounded
\item The boundary and initial conditions
\begin{align*}
u(x,0) &= 0, \quad & \forall x \\
u(a,t) &= T\delta(t), & \forall t
\end{align*}
\item Here $\delta(t)$ is the Dirac delta function which can be thought of as
\[
\delta(t) = \begin{cases}
\infty, & t=0 \\
0, & \text{else}
\end{cases}
\]
\item Solve the equation using the Laplace transform method
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Recap: Laplace transform}
%----- Frame -------------------------------------------%
\begin{frame}
\frametitle[squezze]{Recap: Laplace transform definition}
\vskip-0.1cm
\begin{block}{Definition of the Laplace transform}
The Laplace transform of a function $f(t)$ is defined as
\[
 {\cal L} \left\{ f(t) \right\} = F(s) = \int_0^\infty f(t) e^{-st}dt
\]
for those $s\in\mathbb{C}$ for which the integral exists.
\end{block}
\vskip0.3cm
\begin{block}{Remarks}
\vskip-0.1cm
\begin{itemize}
\item Transforms a function $f(t)$ with a real argument $t\ge0$ to a complex valued function $F(s)$ with complex argument $s$
\item Laplace transform of the Dirac delta function is
\[
\mathcal{L}\left\{T\delta(t)\right\} = T
\]
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Partial derivative transforms}
The Laplace transforms of the partial derivatives of the function
\[
u(x,t),\,t\ge0
\]
are given by
\begin{align*}
\mathcal{L}\left\{\pderiv{u}{x}\right\} &= \frac{\text{d}}{\text{d}x}U(x,s) \\
\mathcal{L}\left\{\pdderiv{u}{x}\right\} &= \frac{\text{d}^2U(x,s)}{\text{d}x^2} \\
\mathcal{L}\left\{\pderiv{u}{t}\right\} &= sU(x,s)-u(x,0)\\
\mathcal{L}\left\{\pdderiv{u}{t}\right\} &= s^2U(x,s)-su(x,0)-u_t(x,0)
\end{align*}
where $U(x,s)$ is the Laplace transform of $u(x,t)$ with respect to $t$
\end{frame}

%----- Frame -------------------------------------------%
\begin{frame}\frametitle{Laplace transform to solve differential equations}
\vskip-0.1cm
\begin{block}{Laplace transform method}
\begin{enumerate}
\item Transform the differentiation in the time domain into an algebraic operation in the Laplace domain
\item The solution in the Laplace domain describes the Laplace transform of the system response
\item Get the time response by the inverse Laplace transform
\end{enumerate}
\end{block}
\vskip0.0cm
\begin{block}{Remarks}
\begin{itemize}
\item Ordinary differential equations are reduced to algebraic equations
\item The initial conditions are automatically included: complete solution to the differential equation
\item One of the derivatives of partial differential equations is removed
\end{itemize}
\end{block}
\vskip0.1cm
\end{frame}

% -------------------------------------------------------- %
\section{Laplace transform example}
% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution by Laplace transforms}
\begin{itemize}
\item Applying the Laplace transform to the diffusion equation we get
\[
sU(x,s)-u(x,0)=\kappa \frac{\text{d}^2U(x,s)}{\text{d}x^2}
\]
\item Insert the first boundary condition to get
\[
\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s}{\kappa}U=0
\]
\item This ODE can be solved by finding the characteristic polynomial 
\[
m^2 - \frac{s}{\kappa} = 0
\]
and remembering the ODE solution methods
\[
U(x,s) = A(s)\exp\left(x\sqrt{\frac{s}{\kappa}}\right) + B(s)\exp\left(-x\sqrt{\frac{s}{\kappa}}\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution by Laplace transforms, continued}
\begin{itemize}
\item The Laplace domain solution needs to remain bounded because the time domain solution remains bounded
\item To fulfil this we need to split the Laplace domain solution into two parts: one for $x<a$ and one for $x>a$
\item Since these two solutions need to be bounded we need to choose
\begin{align*}
U(x,s) &= B(s)\exp\left(-x\sqrt{\frac{s}{\kappa}}\right) &\forall x\ge a \\
U(x,s) &= A(s)\exp\left(x\sqrt{\frac{s}{\kappa}}\right) & \forall x<a
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution by Laplace transforms, continued}
\begin{itemize}
\item Using the second boundary conditions 
\[
U(a,s) = B(s)\exp\left(-a\sqrt{\frac{s}{\kappa}}\right) \overset{!}{=} \mathcal{L}\left\{T\delta(t)\right\} = T
\]
we can find $B$
\[
B(s) = T\exp\left(a\sqrt{\frac{s}{\kappa}}\right)
\]
\item And similar for $A$ 
\[
U(a,s) = A(s)\exp\left(a\sqrt{\frac{s}{\kappa}}\right) \overset{!}{=} \mathcal{L}\left\{T\delta(t)\right\} = T
\]
so
\[
A(s) = T\exp\left(-a\sqrt{\frac{s}{\kappa}}\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution by Laplace transforms, continued}
\begin{itemize}
\item Using the functions of $A(s)$ and $B(s)$ we get
\begin{align*}
U(x,s) &= T\exp\left(-(x-a)\sqrt{\frac{s}{\kappa}}\right), &\forall x\ge a\text{ and} \\
U(x,s) &= T\exp\left(-(a-x)\sqrt{\frac{s}{\kappa}}\right), &\forall x<a
\end{align*}
\item This can be inverted by looking at extensive tables of Laplace transforms
\item From these we find that
\[
\mathcal{L}^{-1}\left\{e^{-b\sqrt{s}}\right\} = \frac{b}{2\sqrt{\pi}}t^{-\frac32}e^{-\frac{b^2}{4t}}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solution by Laplace transforms, continued}
\begin{itemize}
\item Using this
\[
\mathcal{L}^{-1}\left\{e^{-b\sqrt{s}}\right\} = \frac{b}{2\sqrt{\pi}}t^{-\frac32}e^{-\frac{b^2}{4t}}
\]
with $b= \frac{x-a}{\sqrt{\kappa}}$ we get
\[
u(x,t) = \frac{T\left(x-a\right)}{2\sqrt{\pi\kappa}}t^{-\frac32}e^{-\frac{(x-a)^2}{4\kappa t}},\, t>0, \forall x\ge a
\]
and for $b= \frac{a-x}{\sqrt{\kappa}}$ we get
\[
u(x,t) = \frac{T\left(a-x\right)}{2\sqrt{\pi\kappa}}t^{-\frac32}e^{-\frac{(x-a)^2}{4\kappa t}},\, t>0, \forall x< a
\]
\item So that the full solution is
\[
u(x,t) = \frac{T\left|x-a\right|}{2\sqrt{\pi\kappa}}t^{-\frac32}e^{-\frac{(x-a)^2}{4\kappa t}},\, t>0
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.23 solution, $\kappa=1,\,a=1$}
\begin{tikzpicture}
  \begin{axis}[legend style={draw=none}, 
  xlabel=$x$,ylabel=$u/T$]
% t=0.5
 \addplot[black,domain=-3:10, samples=201] {abs(x-1)/(2*sqrt(3.14159))*0.5^(-1.5)*exp(-(x-1)^2/(4*0.5)};
% t=1.0
 \addplot[blue, dotted, line width=1.5, domain=-3:10, samples=201] {abs(x-1)/(2*sqrt(3.14159))*1.0^(-1.5)*exp(-(x-1)^2/(4*1.0)};
% t=2.0
 \addplot[cyan, dashed, line width=1.5, domain=-3:10, samples=201] {abs(x-1)/(2*sqrt(3.14159))*2.0^(-1.5)*exp(-(x-1)^2/(4*2.0)};
% t=5.0
 \addplot[red, dashdotted, line width=1.5, domain=-3:10, samples=201] {abs(x-1)/(2*sqrt(3.14159))*5.0^(-1.5)*exp(-(x-1)^2/(4*5.0)};

  \legend{$t=0.5$,$t=1.0$,$t=2.0$,$t=5.0$}
\end{axis}
   \end{tikzpicture}
\end{frame}

% -------------------------------------------------------- %
\section{Next lecture}
% -------------------------------------------------------- %
\begin{frame}
\frametitle[squezze]{Any questions?}
\vskip0.2cm
\begin{block}{Preparation for the next class}
\begin{itemize}
\item Topic: \textbf{Parabolic equations in spherical coordinates and introduction to hyperbolic equations}
\item Read sections 9.2.1 and 9.3 in Advanced Modern Engineering Mathematics before next lecture
\item Tell me on \TopHat
\begin{itemize}
\item What did you find confusing or difficult about what you read? 
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}