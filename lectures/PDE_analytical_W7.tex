\documentclass{beamer}
\mode<presentation>

% Define toggles to show solutions
\usepackage{etoolbox}
\newtoggle{conceptest}
\toggletrue{conceptest}
%\togglefalse{conceptest}

% Import Numerical Methods for Chemical Engineers style 
\usepackage{PDE3_style}

% -------------------------------------------------------- %
% Course/lecture title information
\subtitle{Heat conduction continued}

% -------------------------------------------------------- %
\begin{document}
\frame{\titlepage}

% -------------------------------------------------------- %
\section{Recap}
% -------------------------------------------------------- %
\subsection{Laplace transform method}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Solving the heat conduction with the Laplace transform}
\begin{itemize}
\item Applying the Laplace transform with respect to $t$ to the parabolic equation
\[
sU(x,s)-u(x,0)=\kappa \frac{\text{d}^2U(x,s)}{\text{d}x^2}
\]
\item Insert the boundary condition at $t=0$ to get
\[
\frac{\text{d}^2U(x,s)}{\text{d}x^2} - \frac{s}{\kappa}U = \frac{u(x,t=0)}{\kappa} 
\]
\item Use the characteristic polynomial to find the complimentary function
\[
U_c(x,s) = A(s)\exp\left(x\sqrt{\frac{s}{\kappa}}\right) + B(s)\exp\left(-x\sqrt{\frac{s}{\kappa}}\right)
\]
\item Find a particular integral satisfying the inhomogeneity
\item Use the inverse Laplace transform to find the time domain solution from the particular solution
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest: Laplace transform}
\TopHat : What is the Laplace transform with respect to $x$ of the 2D Laplace equation 
\[
0 = \pdderiv{u}{x} + \pdderiv{u}{y} = \grad^2 u
\]
\begin{overprint}
\onslide<1>
\vskip-0.4cm
\begin{block}{ConcepTest: possible solutions}
\begin{enumerate}
\item $\pdderiv{U(x,s)}{x} + s^2 U(x,s) - s u(0, y) - u'(0,y) = 0$
\item $s^2 U(s,y) - s u(0, y) - u'(0,y) + s^2 U(x,s) - s u(x, 0) - u'(x,0) = 0$
\item $s^2 U(s,y) - s u(0, y) - u'(0,y) + \pdderiv{U(s,y)}{y} = 0$
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\begin{enumerate}
\item The correct solution is
\[
s^2 U(s,y) - s u(0, y) - u'(0,y) + \pdderiv{U(s,y)}{y} = 0
\]
\end{enumerate}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\section{Spherical coordinates}
% -------------------------------------------------------- %
\subsection{Overview}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Spherical coordinates}
\begin{itemize}
\item So far we have restricted solutions to one dimensional rods and pipes. 
\item We can rewrite the PDEs in spherical $(r, \varphi,\nu)$ and cylindrical polar $(r,\varphi, z)$ form and find the solution in other shaped regions.
\end{itemize}
\centerline{\includegraphics[width=0.65\columnwidth]{coordinates-spherical.png}}
\end{frame}

% -------------------------------------------------------- %
\subsection{Symmetry}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Spherical symmetry}
\begin{itemize}
\item Let us consider a radially symmetric, spherical region and solution
\item The solution is the same for all values of $\varphi\in[0, 2\pi]$ and $\nu\in[0,\pi]$
\item The solution now depends only on \textcolor{blue}{time} and \textcolor{red}{distance from the origin} 
\item So we are solving for $u(r,t)$
\item Using Pythagoras (the radius is the Euclidean distance from the origin) we know
\[
r^2=x^2+y^2+z^2
\]
so
\[
2r\pderiv{r}{x}=2x
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Transform from Cartesian to spherical coordinates}
\begin{itemize}
\item We want to find the Laplacian with no variation due to $\theta$ and $\phi$
\[
\nabla^2 f=\pdderiv{f}{x}+\pdderiv{f}{y}+\pdderiv{f}{z},\text{ where }f=f(r,t),
\]
\item Use the chain rule with $x = r \cos(\theta)$ and the result from the last slide
\[
\pderiv{f}{x}=\pderiv{f}{r}\pderiv{r}{x}=\frac{x}{r}\pderiv{f}{r}
\]
\item Apply the chain rule again for the 2nd derivative
\begin{align*}
\pdderiv{f}{x}&=\frac{\partial}{\partial x}\left(\frac{x}{r}\pderiv{f}{r}\right) \\
&=\frac{1}{r}\pderiv{f}{r}-\frac{x}{r^3}\left(x\pderiv{f}{r}\right)+\frac{x^2}{r^2}\pdderiv{f}{r}
\end{align*}
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Transform from Cartesian to spherical coordinates}
\begin{itemize}
\item A similar process gives the $y$ and $z$ derivatives so the Laplacian becomes
\begin{align*}
\nabla^2f&=\frac{3}{r}\pderiv{f}{r}-\frac{x^2+y^2+z^2}{r^3}\pderiv{f}{r}+\frac{x^2+y^2+z^2}{r^2}\pdderiv{f}{r}\\
&=\frac{2}{r}\pderiv{f}{r}+\pdderiv{f}{r}=\frac{1}{r^2}\frac{\partial}{\partial r}\left(r^2\pderiv{f}{r}\right).
\end{align*}
\item In the previous step we have used $r^2 = x^2+y^2+z^2$
\item So the radially symmetric heat conduction/diffusion equation is
\[
\frac{1}{r^2}\frac{\partial}{\partial r}\left(r^2\pderiv{T}{r}\right) = \frac{1}{\kappa}\pderiv{T}{t}
\]
\item The time derivative stays the same as it is not affected by the change of coordinate system
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Scale the temperature with the radius}
\begin{itemize}
\item Scale the temperature with the radius $T=\theta(r,t)/{r}$ 
\item This changes the space derivative to
\[
\pderiv{T}{r} = \pderiv{\frac{\theta}{r}}{r} = \frac{1}{r}\pderiv{\theta}{r} - \frac{\theta}{r^2}
\]
\item Insert this into the heat conduction equation in spherical coordinates to get
\[
\frac{1}{r^2}\frac{\partial}{\partial r}\left(-\theta+r\pderiv{\theta}{r}\right) = \frac{1}{r\kappa}\pderiv{\theta}{t}
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Scale the temperature with the radius}
\begin{itemize}
\item Differentiating once more gives,
\[
\frac{1}{r}\left(-\pderiv{\theta}{r}+\pderiv{\theta}{r}+ r\pdderiv{\theta}{r}\right) = \frac{1}{\kappa}\pderiv{\theta}{t}
\]
\item So the equation for $\theta(r,t)$ is
\[
\pdderiv{\theta}{r}=\frac{1}{\kappa}\pderiv{\theta}{t}
\]
\item We can solve this using the methods previously described but care must be taken when \textcolor{red}{$r=0$}.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Example 9.24}
% -------------------------------------------------------- %
\subsection{Problem}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.24(b)}
Solve the radially symmetric heat conduction equation
\[\frac{1}{r^2}\frac{\partial}{\partial r}\left(r^2\pderiv{T}{r}\right)=\frac{1}{\kappa}\pderiv{T}{t}\]
in the region $r>a$ subject to the boundary conditions
\begin{align*}
T(a,t) &= T_0 & \forall t\ge0 \\
\lim_{r\rightarrow\infty} T(r,t) &= 0 & \forall t\ge0 \\
T(r,0) &= 0 & \forall r>a
\end{align*}
\begin{itemize}
\item $T_0$ is a constant temperature
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{ConcepTest}
% -------------------------------------------------------- %
\begin{frame}{ConcepTest: boundary conditions of scaled temperature}
\TopHat : Find the boundary conditions for $\theta(r,t) = r T(r,t)$?
\begin{itemize}
\item Consider $\theta(r,t) = r T(r,t)$ and solve the transformed heat equation
\[
\pdderiv{\theta}{r}=\frac{1}{\kappa}\pderiv{\theta}{t}
\]
\item We need to adjust the boundary conditions for $\theta$
\begin{align*}
T(a,t) &= T_0 \implies & \theta(a,t) &= ? & \forall t\ge0 \\
\lim_{r\rightarrow\infty} T(r,t) &< \text{const} \implies & \lim_{r\rightarrow\infty} \theta(r,t) &<\text{const} & \forall t\ge0 \\
T(r,0) &= 0 \implies & \theta(r,0) &= 0 & \forall r>a
\end{align*}
\end{itemize}
\begin{overprint}
\onslide<1>
\vskip-0.4cm
\begin{block}{ConcepTest: possible solutions}
\vskip-0.2cm
\begin{enumerate}
\item \(\theta(a,t) = \frac{T_0}{a}, \quad \forall t\ge0\)
\item \(\theta(a,t) = T_0, \quad \forall t\ge0\)
\item \(\theta(a,t) = a^2T_0, \quad \forall t\ge0\)
\item \(\theta(a,t) = aT_0, \quad \forall t\ge0\)
\end{enumerate}
\end{block}
\iftoggle{conceptest}{% Show ConcepTest solution
\onslide<2>
\vskip-0.4cm
\begin{block}{Solution}
\begin{enumerate}
\item Solution 4 is correct: 
\begin{align*}
\theta(r,t) &= r T(r,t) & \\
\theta(a,t) &= a T(a,t) = a T_0 & \forall t\ge0
\end{align*}
\end{enumerate}
\end{block}
}{% Don't show ConcepTest solution 
\vskip5cm
}
\end{overprint}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.24(b) continued}
\vskip-0.1cm
\begin{itemize}
\item Similar to the previous Laplace transform example, so we can write 
\[
\overline{\theta}(r,s) = A(s)\exp\left(r\sqrt{\frac{s}{\kappa}}\right) + B(s)\exp\left(-r\sqrt{\frac{s}{\kappa}}\right)
\]
\item Since $\lim_{r\rightarrow\infty} \theta(r,t)$ is bounded it follows that the Laplace transform also needs to be bounded
\item This is only given for $A(s) = 0$
\item Transforming the boundary condition at $r=a$ gives
\[
\overline{\theta}(a,s) = a\frac{T_0}{s},
\]
\item This needs to be equal to
\[
\overline{\theta}(a,s) = a\frac{T_0}{s} \overset{!}{=} B(s)\exp\left(-a\sqrt{\frac{s}{\kappa}}\right)
\]
\item From this it follows that
\[
B(s) = \frac{aT_0}{s}\exp\left(a\sqrt{\frac{s}{\kappa}}\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.24(b) continued}
\begin{itemize}
\item The solution of the transformed equation is therefore
\[
\overline{\theta}(r,s) = \frac{aT_0}{s}\exp\left(-(r-a)\sqrt{\frac{s}{\kappa}}\right)
\]
\item Finding the inverse transform for 
\[
\overline{\theta}(r,s) = \frac{aT_0}{s}\exp\left(-(r-a)\sqrt{\frac{s}{\kappa}}\right)
\]
is not easy.  
\item We need extensive tables of Laplace transforms or computer algebra tools.  
\item The inverse  is
\[
\theta(r,t) = aT_0\ \text{erfc}\left(\frac12\frac{r-a}{\sqrt{\kappa t}}\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\subsection{Error function}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{The error function}
\begin{block}{The error function $\text{erf}(z)$ is given by}
\[\text{erf}(z)=\frac2{\sqrt{\pi}}\int_0^z e^{-\nu^2}\text{ d}\nu\]
\end{block}
\begin{block}{The complementary error function $\text{erfc}(z)$ is given by}
\[\text{erfc}(z)=1-\text{erf}(z)\]
\end{block}
\begin{itemize}
\item Both commonly occur in the solution of heat conduction and diffusion problems (and are widely used in statistics).
\item Evaluating $\text{erf}(z)$ is tricky but either tables or approximations can be used.
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{The error function and the complementary error function for $x>0$}
\includegraphics[width=0.8\columnwidth]{error_function}
\end{frame}

% -------------------------------------------------------- %
\subsection{Solution plot}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.24(b) continued}
\begin{itemize}
\item The solution of the scaled temperature $\theta$ is 
\[
\theta(r,t) = aT_0\ \text{erfc}\left(\frac12\frac{r-a}{\sqrt{\kappa t}}\right)
\]
\item Recall the scaling of the temperature
\[
\theta(r,t) = r T(r,t)
\]
\item We get the solution in terms of temperature
\[ 
T(r,t) = \frac{aT_0}{r}\ \text{erfc}\left(\frac12\frac{r-a}{\sqrt{\kappa t}}\right)
\]
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\begin{frame}
\frametitle{Example 9.24(b) solution at $\tau=\frac{4\kappa t}{a^2}=0.01,\, 0.1,\, 1\text{ and }10$}
\includegraphics[width=0.85\columnwidth]{example_924_solution}
\end{frame}

% -------------------------------------------------------- %
\section{Remarks about the heat conduction equation}
% -------------------------------------------------------- %
\begin{frame}
\frametitle{Remarks}
\begin{itemize}
\item Further examples are given in section 9.4.3 in the text book and in Schaum's outline series
\item Additional worked examples are in the tutorial problems
\end{itemize}
\end{frame}

% -------------------------------------------------------- %
\section{Next lecture}
% -------------------------------------------------------- %
\begin{frame}
\frametitle[squezze]{Any questions?}
\vskip0.2cm
\begin{block}{Preparation for the next class}
\begin{itemize}
\item Topic: \textbf{Introduction to the wave equation and solution with the method of d'Alembert}
\item Read sections 9.3.1 and 9.3.2 in Advanced Modern Engineering Mathematics before next lecture
\item Tell me on \TopHat
\begin{itemize}
\item What did you find confusing or difficult about what you read? 
\item If you didn't find anything confusing, what did you find interesting or what do you wonder about.
\end{itemize}
\item Exercises 9.2.6 in Advanced Modern Engineering Mathematics to strengthen material from this lecture
\end{itemize}
\end{block}
\end{frame}

% -------------------------------------------------------- %
\appendix
\appendixbegin

\appendixend

\end{document}